/* eslint-disable @typescript-eslint/naming-convention */
declare global {
  interface GlobalEventHandlersEventMap {
    // This is required for each event that needs specific types, i.e. DragEvent, instead of the default basic Event
    // in Angular templates.
    // Angular might one day support better types for the EventManagerPlugin API, but who knows.
    // https://github.com/angular/angular/issues/40778#issuecomment-776829440
    // https://github.com/angular/angular/issues/40553
    'dragenter.outside-zone': DragEvent;
    'dragover.outside-zone': DragEvent;
    'dragleave.outside-zone': DragEvent;
    'dragstart.outside-zone': DragEvent;
    'mousedown.outside-zone': MouseEvent;
    'mousemove.outside-zone': MouseEvent;
  }
}

export {};
