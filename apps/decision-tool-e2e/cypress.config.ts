import { defineConfig } from 'cypress';
import { nxE2EPreset } from '@nx/cypress/plugins/cypress-preset';

const cypressJsonConfig = {
  fileServerFolder: '.',
  fixturesFolder: './src/fixtures',
  video: true,
  videosFolder: '../../dist/cypress/apps/decision-tool-e2e/videos',
  screenshotsFolder: '../../dist/cypress/apps/decision-tool-e2e/screenshots',
  chromeWebSecurity: false,
  viewportWidth: 1920,
  viewportHeight: 1080,
  specPattern: 'src/{e2e,integration}/**/*.cy.{js,jsx,ts,tsx}',
  supportFile: 'src/support/e2e.ts',
  experimentalSessionAndOrigin: true,
  defaultCommandTimeout: 20000,
};
export default defineConfig({
  e2e: {
    ...nxE2EPreset(__dirname),
    ...cypressJsonConfig,
  },
});
