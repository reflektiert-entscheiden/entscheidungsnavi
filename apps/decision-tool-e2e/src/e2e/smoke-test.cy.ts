describe('smoke-test', () => {
  it('should begin on start page', () => {
    cy.visit('/');
    cy.get('[data-cy=start_container]').should('exist');
  });
});
