describe('Global Variables', { testIsolation: false }, () => {
  before(() => {
    cy.visit('/');
    cy.loadAlex();
    cy.getBySel('step-impactModel').click();
  });

  it('opens the global variables modal', () => {
    openGlobalVariablesModal();
  });

  it('should add variable', () => {
    cy.getBySel('add-variable').click();

    cy.getBySel('variable-name').click({ force: true });
    cy.getBySel('variable-name').type('Test1');

    cy.getBySel('variable-value').click({ force: true });
    cy.getBySel('variable-value').type('5');

    cy.getBySel('save-variables').click();
    cy.waitForModalToClose();
  });

  it('should not add duplicate variable', () => {
    openGlobalVariablesModal();
    cy.getBySel('add-variable').click();

    cy.getBySel('variable-name').last().click({ force: true });
    cy.getBySel('variable-name').last().type('Test1');

    cy.getBySel('save-variables').should('be.disabled');

    cy.getBySel('delete-variable').last().click();
  });

  it('should not add variables matching Indicator names', () => {
    cy.getBySel('add-variable').click();

    cy.getBySel('variable-name').last().click({ force: true });
    cy.getBySel('variable-name').last().type('Ind1');

    cy.getBySel('save-variables').should('be.disabled');

    cy.getBySel('delete-variable').last().click();
  });

  it('should not add variables matching Weight names', () => {
    cy.getBySel('add-variable').click();

    cy.getBySel('variable-name').last().click({ force: true });
    cy.getBySel('variable-name').last().type('c');

    cy.getBySel('save-variables').should('be.disabled');

    cy.getBySel('delete-variable').last().click();

    cy.getBySel('discard-changes').click();
    cy.getBySel('confirm-button').click();
    cy.waitForModalToClose();
  });

  it('should remove variable', () => {
    openGlobalVariablesModal();

    cy.getBySel('delete-variable').click();
    cy.getBySel('save-variables').click();

    openGlobalVariablesModal();

    cy.getBySel('variable-name').should('not.exist');
    cy.getBySel('dt-modal-title-close').click();
    cy.waitForModalToClose();
  });

  it('should add variable with value', () => {
    openGlobalVariablesModal();

    // Add Variable with Value 5
    cy.getBySel('add-variable').click();

    cy.getBySel('variable-name').click({ force: true });
    cy.getBySel('variable-name').type('Test1');

    cy.getBySel('variable-value').click({ force: true });
    cy.getBySel('variable-value').type('5');

    cy.getBySel('save-variables').click();
    cy.waitForModalToClose();

    // Check if Variable is added with proper value by using it in custom aggregation formula
    cy.getBySel('objective-scale-info').first().click();
    cy.getBySel('aggregation-type-selection').click();
    cy.getBySel('custom-aggreation-formula').click();
    cy.getBySel('custom-aggregation-formula-input').click({ force: true });
    cy.getBySel('custom-aggregation-formula-input').type('Test1');

    cy.getBySel('calculate-limits-automatically').find('button').click();
    cy.getBySel('calculate-limits-automatically').find('button').should('have.attr', 'aria-checked', 'true');

    cy.getBySel('custom-aggregation-auto-min').should('have.value', '5');
    cy.getBySel('custom-aggregation-auto-max').should('have.value', '5');

    cy.getBySel('custom-aggregation-formula-input').click({ force: true });
    cy.getBySel('custom-aggregation-formula-input').type(' + Ind1');

    cy.getBySel('custom-aggregation-auto-min').should('have.value', '10');
    cy.getBySel('custom-aggregation-auto-max').should('have.value', '6');

    cy.getBySel('save-scale').click();
    cy.waitForModalToClose();

    // First matrix cell should contain 7
    cy.getBySel('valid').first().should('contain.text', '7');
  });

  it('should not delete used variable', () => {
    openGlobalVariablesModal();

    cy.getBySel('delete-variable').should('be.disabled');
  });

  it('should show objective scales using variable', () => {
    cy.getBySel('used-in-trigger').click();

    cy.getBySel('used-in-popover').should('exist');

    cy.getBySel('used-in-objective').contains('Möglichst hohes eigenes Wohlempfinden');
  });

  it('should not add duplicate variable if one is used', () => {
    cy.getBySel('add-variable').click();

    cy.getBySel('variable-name').last().click({ force: true });
    cy.getBySel('variable-name').last().type('Test1');

    cy.getBySel('save-variables').should('be.disabled');

    cy.getBySel('delete-variable').last().click();

    cy.getBySel('save-variables').click();
  });

  it('should retain disabled variables', () => {
    openGlobalVariablesModal();

    cy.getBySel('variable-name').should('exist');
  });
});

function openGlobalVariablesModal() {
  cy.getBySel('navline-element-global-variables').click();
  cy.findDtModal().should('exist');
}
