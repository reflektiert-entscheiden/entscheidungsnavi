import { DecisionData } from '@entscheidungsnavi/decision-data';

// This checks whether all ways of adding an objective in the navi calls the event in decision-data
describe('objective-event', function () {
  beforeEach(() => {
    cy.visit('/');
    cy.loadAlex();
    cy.window().then(window => {
      expect(window['DecisionData']).to.exist;
      cy.window().its('DecisionData').as('decisionData');
    });
  });
  it('should not fire while loading', function () {
    const decisionData: DecisionData = this.decisionData;

    const objectiveAddedListener = cy.stub();

    decisionData.objectiveAdded$.subscribe(objectiveAddedListener);

    cy.loadAlex();
    cy.get('[data-cy=step-objectives]').click();
    expect(objectiveAddedListener).to.not.be.called;
  }),
    it('should fire in the hierarchy', function () {
      const decisionData: DecisionData = this.decisionData;

      const objectiveAddedListener = cy.stub();

      decisionData.objectiveAdded$.subscribe(objectiveAddedListener);

      cy.get('[data-cy=step-objectives]').click();
      cy.get('dt-hierarchy-node').first().findBySel('hierarchy-node-wrapper').click();
      cy.get('body').type('{rightArrow}');
      cy.get('body').type('{enter}');
      cy.then(() => {
        expect(objectiveAddedListener).to.be.called;
      });
    });

  it('should fire in the list', function () {
    const decisionData: DecisionData = this.decisionData;

    const objectiveAddedListener = cy.stub();

    decisionData.objectiveAdded$.subscribe(objectiveAddedListener);

    cy.get('[data-cy=step-objectives]').click();
    cy.selectOptionFromDtSelect('Zielhierarchie', 'Liste');
    cy.get('[data-cy=add-objective]').click();
    cy.then(() => {
      expect(objectiveAddedListener).to.be.called;
    });
  });

  it('should fire in broadcast transfer', function () {
    const decisionData: DecisionData = this.decisionData;

    const objectiveAddedListener = cy.stub();

    decisionData.objectiveAdded$.subscribe(objectiveAddedListener);

    const channel = new BroadcastChannel('dt');

    cy.get('[data-cy=step-objectives]').click();
    cy.selectOptionFromDtSelect('Zielhierarchie', 'Liste');
    cy.then(() => {
      channel.postMessage({
        type: 'objective',
        data: JSON.stringify(decisionData.objectives[0]),
      });
    });
    cy.get('[data-cy=accept-transfer]').click();
    cy.then(() => {
      expect(objectiveAddedListener).to.be.called;
    });
  });
});
