describe('load-project', function () {
  it('should load alex project', function () {
    cy.visit('/');
    cy.loadAlex();
    cy.getBySel('project_name').contains('Alex').should('exist');
  });
});
