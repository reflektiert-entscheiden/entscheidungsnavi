describe('Online project management', () => {
  beforeEach(() => {
    cy.createAndLoginUser('e2e-test-user@entscheidungsnavi.de', '12345678');
    cy.fixture('alex.json').then(alexJson => {
      cy.uploadProject('Alex e2e-Testprojekt', JSON.stringify(alexJson));
    });
  });

  afterEach(() => {
    // Log back in, in case the user was logged out
    cy.createAndLoginUser('e2e-test-user@entscheidungsnavi.de', '12345678');
    cy.deleteCurrentUser('12345678');
  });

  it('allows loading, editing, and restoring an online project', () => {
    cy.visit('/');
    cy.getBySel('project_mgmt').click();
    cy.getBySel('online-projects-button').not('.disabled').click();
    cy.getBySel('project-list').findBySel('project-name').contains('Alex').click();

    cy.get('dt-project-area').within(() => {
      cy.getBySel('project_name').should('contain.text', 'Alex e2e-Testprojekt');
      // The project is saved
      cy.get('mat-icon').contains('cloud_done').should('exist');
    });

    cy.getBySel('step-decisionStatement').click();
    // Switching steps should not trigger a save
    cy.get('dt-project-area mat-icon').contains('cloud_upload').should('not.exist');

    // Modifying the decision statement triggers saving, which then succeeds
    cy.getBySel('statement-input').type('{selectall}Die neue Entscheidungsfrage von Alex');
    cy.getBySel('statement-input').blur();
    cy.get('dt-project-area mat-icon').contains('cloud_upload').should('exist');
    cy.get('dt-project-area mat-icon').contains('cloud_done', { timeout: 10_000 }).should('exist');

    cy.getBySel('project-history-button').click();
    cy.getBySel('project-history-list').children().should('have.length.at.least', 2);

    cy.getBySel('project-history-list').children().last().click();
    cy.get('dt-project-area mat-icon').contains('cloud_off').should('exist');

    cy.getBySel('restore-project-button').click();
    cy.getBySel('restore-project-button').should('not.exist');
    cy.get('dt-project-area mat-icon').contains('cloud_done').should('exist');

    cy.getBySel('statement-input').should(
      'have.text',
      // eslint-disable-next-line max-len
      'Wie kann ich mein Leben in den nächsten drei Jahren in beruflicher Sicht so gestalten, dass ich eine möglichst hohe Lebensqualität erreiche?',
    );

    cy.getBySel('project-history-list').children().last().click();
    cy.getBySel('project-history-list')
      .children()
      .then(children => {
        cy.wrap(children[2]).click();
      });
    cy.getBySel('statement-input').should('have.text', 'Die neue Entscheidungsfrage von Alex');

    cy.getBySel('revert-to-newest-button').click();
    cy.getBySel('revert-to-newest-button').should('not.exist');
    cy.get('dt-project-area mat-icon').contains('cloud_done', { timeout: 10_000 }).should('exist');

    cy.getBySel('project_mgmt').click();
    cy.getBySel('close-project-button').click();
    cy.url().should('include', '/start');
  });

  it('should allow saving and overwriting projects online', () => {
    cy.visit('/');
    cy.loadAlex();

    cy.getBySel('project_mgmt').click();
    cy.getBySel('save-as-button').should('not.have.class', 'disabled');
    cy.getBySel('save-as-button').click();

    cy.getBySel('project-name-input').type('{selectall}Neues Projekt von lokal');
    cy.getBySel('save-as-confirm-button').click();
    cy.getBySel('save-as-confirm-button').should('not.exist');
    cy.getBySel('dt-modal-title-close').click();
    cy.findDtModal().should('not.exist');
    cy.get('dt-project-area mat-icon').contains('cloud_done').should('exist');

    cy.getBySel('project_mgmt').click();
    cy.getBySel('save-as-button').click();
    cy.getBySel('project-list').findBySel('project-name').contains('Alex e2e-Testprojekt').click();
    cy.getBySel('project-name-input').should('have.value', 'Alex e2e-Testprojekt');

    cy.getBySel('save-as-confirm-button').click();
    cy.findDtModal().findBySel('confirm-button').click();
    cy.getBySel('save-as-confirm-button').should('not.exist');
    cy.getBySel('dt-modal-title-close').click();
    cy.findDtModal().should('not.exist');
    cy.get('dt-project-area mat-icon').contains('cloud_done').should('exist');

    cy.getBySel('project_mgmt').click();
    cy.getBySel('online-projects-button').not('.disabled').click();
    cy.getBySel('project-list').findBySel('project-name').should('have.length', 2);
  });

  describe('with a loaded project', () => {
    beforeEach(() => {
      cy.visit('/');

      cy.getBySel('project_mgmt').click();
      cy.getBySel('online-projects-button').not('.disabled').click();
      cy.getBySel('project-list').findBySel('project-name').contains('Alex').click();

      cy.getBySel('step-decisionStatement').click();
      cy.get('dt-project-area mat-icon').contains('cloud_done').should('exist');
      cy.window().its('ProjectType').should('equal', 'online');
    });

    it('should convert to a local project when the user looses their session during saving', () => {
      cy.clearAllCookies();

      cy.getBySel('statement-input').type('{selectall}Die neue Entscheidungsfrage von Alex');
      cy.getBySel('statement-input').blur();
      cy.get('dt-project-area mat-icon').contains('cloud_upload').should('exist');

      cy.window().its('ProjectType', { timeout: 10_000 }).should('equal', 'local');
      cy.findDtModal().should('exist');
    });

    it('should show an error when saving has failed', () => {
      let shouldIntercept = true;
      cy.intercept('/api/projects/**', req => {
        if (shouldIntercept) req.destroy();
      });

      cy.getBySel('statement-input').type('{selectall}Die neue Entscheidungsfrage von Alex');
      cy.getBySel('statement-input').blur();
      cy.get('dt-project-area mat-icon').contains('cloud_upload').should('exist');

      cy.getBySel('autosave-error-icon', { timeout: 10_000 }).should('exist');
      cy.getBySel('autosave-failed-snackbar').should('exist');

      cy.then(() => {
        shouldIntercept = false;
      });

      cy.get('dt-project-area mat-icon').contains('cloud_done', { timeout: 15_000 }).should('exist');
      cy.getBySel('autosave-failed-snackbar').should('not.exist');
    });
  });
});
