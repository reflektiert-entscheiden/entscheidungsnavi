describe('Starter mode', () => {
  it('allows creating a project start to finish', () => {
    cy.visit('/');

    cy.getBySel('new-project-button').click();
    cy.getBySel('starter-mode-button').click();
    cy.getBySel('create-project-button').click();

    // Decision statement
    cy.getBySel('starter-step-decisionStatement').click();
    cy.getBySel('starter-finish-button').should('be.disabled');

    cy.getBySel('statement-input').type('Eine Entscheidungsfrage');

    cy.getBySel('starter-finish-button').click();

    // Objectives
    cy.getBySel('starter-step-objectives').click();
    cy.getBySel('starter-finish-button').should('be.disabled');

    cy.getBySel('objective-name-input').type('Ziel 1');

    cy.getBySel('add-objective-button').click();
    cy.getBySel('objective-name-input').should('have.length', 2);
    cy.getBySel('objective-name-input').last().type('Ziel 2');

    cy.getBySel('add-objective-button').click();
    cy.getBySel('objective-name-input').last().type('Ziel 3');
    cy.getBySel('delete-objective-button').last().click();
    cy.getBySel('objective-name-input').should('have.length', 2);

    cy.getBySel('starter-finish-button').click();

    // Alternatives
    cy.getBySel('starter-step-alternatives').click();
    cy.getBySel('starter-finish-button').should('be.disabled');

    cy.getBySel('alternative-name-input').type('Alternative 1');

    cy.getBySel('add-alternative-button').click();
    cy.getBySel('alternative-name-input').should('have.length', 2);
    cy.getBySel('alternative-name-input').last().type('Alternative 2');

    cy.getBySel('add-alternative-button').click();
    cy.getBySel('alternative-name-input').last().type('Alternative 3');
    cy.getBySel('delete-alternative-button').last().click();
    cy.getBySel('alternative-name-input').should('have.length', 2);

    cy.getBySel('starter-finish-button').click();

    // Impact model
    cy.getBySel('starter-step-impactModel').click();
    cy.getBySel('starter-finish-button').should('be.disabled');

    cy.getBySel('outcome-button').each((outcome, index) => {
      cy.wrap(outcome).click();
      cy.get('.mat-mdc-menu-item').eq(index).click();
    });
    cy.get('.unset[data-cy=outcome-button]').should('not.exist');

    cy.getBySel('starter-finish-button').click();

    // Objective weighting
    cy.getBySel('starter-step-results').click();
    cy.getBySel('starter-finish-button').should('be.disabled');

    cy.getBySel('objective-weight-slider').first().find('input').invoke('val', 8).trigger('change');
    cy.getBySel('objective-weight-slider').last().find('input').invoke('val', 2).trigger('change');

    cy.getBySel('starter-finish-button').click();

    // Finish project
    cy.getBySel('starter-step-finishProject').click();

    cy.get('dt-chart-ranking').should('exist');
  });

  it('allows switching to educational', () => {
    cy.visit('/');
    cy.loadFixtureProject('starter-project.json');

    cy.getBySel('mode-switch-button').click();
    cy.getBySel('mode-switch-menu').find('mat-radio-button').contains('Educational').click();
    cy.getBySel('confirm-leave-starter').click();

    cy.location().its('pathname').should('equal', '/results');
    cy.window().its('DecisionData').its('projectMode').should('equal', 'educational');
  });

  it('allows switching to professional', () => {
    cy.visit('/');
    cy.loadFixtureProject('starter-project.json');

    cy.getBySel('mode-switch-button').click();
    cy.getBySel('mode-switch-menu').find('mat-radio-button').contains('Professional').click();
    cy.getBySel('confirm-leave-starter').click();

    cy.location().its('pathname').should('equal', '/professional/evaluate-and-decide');
    cy.window().its('DecisionData').its('projectMode').should('equal', 'professional');
  });
});
