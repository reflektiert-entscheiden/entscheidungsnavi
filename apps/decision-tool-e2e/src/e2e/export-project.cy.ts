const downloadsFolder = Cypress.config('downloadsFolder');

describe('export project from management', function () {
  beforeEach(() => {
    cy.visit('/');
    cy.loadAlex();
    cy.getBySel('project_mgmt').click();
    cy.getBySel('export-project-button').click();
  });

  it('should export alex project as json', function () {
    cy.getBySel('export-json-button').click();
    cy.readFile(`${downloadsFolder}/Alex (Fallstudie zur Job- und Lebensplanung).json`).its('exportVersion').should('be.a', 'string');
  });

  it('should export alex project as pdf', function () {
    cy.getBySel('export-pdf-button').click();
    cy.getBySel('generate-pdf-button').click();
    cy.getBySel('download-pdf-button').click();
    cy.readFile(`${downloadsFolder}/Alex (Fallstudie zur Job- und Lebensplanung).pdf`);
  });

  it('should export alex project as excel', function () {
    cy.getBySel('export-excel-button').click();
    cy.readFile(`${downloadsFolder}/Alex (Fallstudie zur Job- und Lebensplanung).xlsx`);
  });
});

describe('impact model export', function () {
  it('should export alex project impact model as excel', function () {
    cy.visit('/');
    cy.loadAlex();

    cy.getBySel(`step-impactModel`).click();
    cy.getBySel('excel-button').click();
    cy.getBySel('export-button').click();
    cy.readFile(`${downloadsFolder}/Alex (Fallstudie zur Job- und Lebensplanung)-Wirkungsmodell.xlsx`);
  });
});
