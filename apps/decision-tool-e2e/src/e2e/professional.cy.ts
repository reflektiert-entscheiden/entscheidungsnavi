import { DecisionData } from '@entscheidungsnavi/decision-data';

export type ToolForStructureAndEstimate =
  | 'sae-overview'
  | 'sae-assumptions'
  | 'sae-objective-list'
  | 'sae-hierarchy'
  | 'sae-checklists'
  | 'sae-alternative-list'
  | 'sae-weak-points'
  | 'sae-objective-focused-search'
  | 'sae-lever-method'
  | 'sae-influence-factors';

export type ToolForEvaluateAndDecide =
  | 'ead-overview'
  | 'ead-utility-functions'
  | 'ead-objective-weighting'
  | 'ead-sensitivity-analysis'
  | 'ead-pros-and-cons'
  | 'ead-robustness-check'
  | 'ead-risk-comparison'
  | 'ead-objective-weight-analysis'
  | 'ead-cost-utility-analysis'
  | 'ead-utility-based-tornado-diagram';

const STRUCTURE_AND_ESTIMATE_TOOL_NAMES = {
  'sae-overview': 'Übersicht',
  'sae-assumptions': 'Annahmen und Abgrenzungen',
  'sae-objective-list': 'Listenansicht',
  'sae-alternative-list': 'Übersicht',
  'sae-hierarchy': 'Hierarchie',
  'sae-checklists': 'Beispiele und Vorschläge',
  'sae-weak-points': 'Schwachpunktanalyse',
  'sae-objective-focused-search': 'Zielfokussierte Suche',
  'sae-lever-method': 'Stellhebelmethode',
  'sae-influence-factors': 'Einflussfaktoren',
};

const EVALUATE_AND_DECIDE_TOOL_NAMES = {
  'ead-overview': 'Übersicht',
  'ead-utility-functions': 'Nutzenfunktionen',
  'ead-objective-weighting': 'Zielgewichtung',
  'ead-sensitivity-analysis': 'Sensitivitätsanalyse',
  'ead-pros-and-cons': 'Pros und Kontras',
  'ead-robustness-check': 'Robustheitstest',
  'ead-risk-comparison': 'Risikovergleich',
  'ead-objective-weight-analysis': 'Zielgewichtsanalyse',
  'ead-cost-utility-analysis': 'Kosten-Nutzen-Analyse',
  'ead-utility-based-tornado-diagram': 'Tornadodiagramm',
};

describe('professional', function () {
  function switchToStructureAndEstimateTool(tool: ToolForStructureAndEstimate, switchTab = true) {
    if (switchTab) {
      cy.getBySel('professional-nav').then(professionalNav => {
        if (professionalNav.hasClass('evaluate-and-decide')) {
          // switch to structure and estimate if we are in evaluate and decide
          cy.getBySel('phase-evaluate-and-decide').click();
        }
      });
    }

    if (tool === 'sae-overview' || tool === 'sae-assumptions' || tool === 'sae-influence-factors') {
      cy.getBySel(tool).click();
    } else {
      const toolToCategoryName = {
        'sae-objective-list': 'category-sae-objective-list-sae-hierarchy-sae-checklists',
        'sae-hierarchy': 'category-sae-objective-list-sae-hierarchy-sae-checklists',
        'sae-checklists': 'category-sae-objective-list-sae-hierarchy-sae-checklists',
        'sae-weak-points': 'category-sae-alternative-list-sae-weak-points-sae-objective-focused-search-sae-lever-method',
        'sae-objective-focused-search': 'category-sae-alternative-list-sae-weak-points-sae-objective-focused-search-sae-lever-method',
        'sae-lever-method': 'category-sae-alternative-list-sae-weak-points-sae-objective-focused-search-sae-lever-method',
        'sae-alternative-list': 'category-sae-alternative-list-sae-weak-points-sae-objective-focused-search-sae-lever-method',
      };
      const categoryName = toolToCategoryName[tool];

      cy.getBySel(categoryName).click();
      cy.getBySel(tool).click();
      cy.getBySel('dt-pf-tool-active-tool').should('have.text', ' ' + STRUCTURE_AND_ESTIMATE_TOOL_NAMES[tool]);
    }
  }

  function switchToEvaluateAndDecideTool(tool: ToolForEvaluateAndDecide, switchTab = true) {
    if (switchTab) {
      cy.getBySel('professional-nav').then(professionalNav => {
        if (!professionalNav.hasClass('evaluate-and-decide')) {
          // switch to evaluate and decide if we are in structure and estimate
          cy.getBySel('phase-structure-and-estimate').click();
        }
      });
    }

    if (tool === 'ead-overview' || tool === 'ead-utility-functions' || tool === 'ead-objective-weighting') {
      cy.getBySel(tool).click();
      return;
    }

    if (
      tool === 'ead-sensitivity-analysis' ||
      tool === 'ead-pros-and-cons' ||
      tool === 'ead-robustness-check' ||
      tool === 'ead-risk-comparison' ||
      tool === 'ead-objective-weight-analysis' ||
      tool === 'ead-cost-utility-analysis' ||
      tool === 'ead-utility-based-tornado-diagram'
    ) {
      const s =
        'category-ead-sensitivity-analysis-ead-pros-and-cons-ead-robustness-check-ead-risk-comparison-' +
        'ead-objective-weight-analysis-ead-cost-utility-analysis-ead-utility-based-tornado-diagram';
      cy.getBySel(s).click();
      cy.getBySel(tool).click();
      cy.getBySel('dt-pf-tool-active-tool').should('have.text', ' ' + EVALUATE_AND_DECIDE_TOOL_NAMES[tool]);
    }
  }

  beforeEach(() => {
    cy.visit('/');
    cy.loadAlex();
    cy.window().then(window => {
      expect(window['DecisionData']).to.exist;
      cy.window().its('DecisionData').as('decisionData');
    });

    cy.getBySel('mode-switch-button').click();
    cy.getBySel('mode-switch-menu').get('mat-radio-button').contains('Professional').click({ force: true });
  });

  it('should switch to professional', function () {
    cy.get('dt-professional-ead-main').should('exist');
  });

  describe('structure-and-estimate', () => {
    it("should switch to tool 'overview'", function () {
      switchToStructureAndEstimateTool('sae-overview');
      cy.getBySel('structure-and-estimate-decision-statement').should('have.class', 'statement-wrapper').should('have.prop', 'tagName');
      cy.getBySel('structure-and-estimate-impact-model').should('have.prop', 'tagName').should('eq', 'dt-impact-matrix'.toUpperCase());
    });

    it("should switch to tool 'assumptions'", function () {
      switchToStructureAndEstimateTool('sae-assumptions');
      cy.getBySel('structure-and-estimate-tool-assumptions-decision-statement')
        .should('have.prop', 'tagName')
        .should('eq', 'dt-decision-statement'.toUpperCase());
    });

    it("should switch to tool 'objective-list'", function () {
      switchToStructureAndEstimateTool('sae-objective-list');
      cy.getBySel('structure-and-estimate-tool-objective-list-objective-list')
        .should('have.prop', 'tagName')
        .should('eq', 'dt-objective-list'.toUpperCase());
    });

    it("should switch to tool 'hierarchy'", function () {
      switchToStructureAndEstimateTool('sae-hierarchy');

      cy.getBySel('structure-and-estimate-tool-hierarchy-hierarchy')
        .should('have.prop', 'tagName')
        .should('eq', 'dt-objective-hierarchy'.toUpperCase());

      cy.getBySel('structure-and-estimate-tool-hierarchy-hierarchy').findBySel('hierarchy-help').click();

      // cy.get('mat-dialog-container').find('dt-aspect-hierarchy-shortcuts').should('exist');
      cy.findDtModal().within(() => {
        cy.getBySel('dt-modal-title-close').click();
      });
      // todo: back to educational
    });

    it("should switch to tool 'checklist'", function () {
      switchToStructureAndEstimateTool('sae-checklists');

      cy.getBySel('structure-and-estimate-tool-checklists-checklist')
        .should('have.prop', 'tagName')
        .should('eq', 'dt-objective-hint-4'.toUpperCase());
    });

    it("should switch to tool 'alternative list'", function () {
      switchToStructureAndEstimateTool('sae-alternative-list');

      cy.getBySel('structure-and-estimate-tool-alternative-list-alternatives')
        .should('have.prop', 'tagName')
        .should('eq', 'dt-alternatives'.toUpperCase());
    });

    it("should switch to tool 'weak-points'", function () {
      switchToStructureAndEstimateTool('sae-weak-points');

      cy.getBySel('structure-and-estimate-tool-weak-points-alternative-list')
        .should('have.prop', 'tagName')
        .should('eq', 'dt-alternative-hint-2'.toUpperCase());
    });

    it("should switch to tool 'objective-focused-search'", function () {
      switchToStructureAndEstimateTool('sae-objective-focused-search');

      cy.getBySel('structure-and-estimate-tool-objective-focused-search-alternative-list')
        .should('have.prop', 'tagName')
        .should('eq', 'dt-alternative-hint-3'.toUpperCase());
    });

    it("should switch to tool 'lever-method'", function () {
      switchToStructureAndEstimateTool('sae-lever-method');

      cy.getBySel('structure-and-estimate-tool-lever-method-alternative-list')
        .should('have.prop', 'tagName')
        .should('eq', 'dt-alternative-hint-5'.toUpperCase());
    });

    it("should switch to tool 'influence-factors'", function () {
      switchToStructureAndEstimateTool('sae-influence-factors');

      cy.getBySel('structure-and-estimate-tool-influence-factors-list')
        .should('have.prop', 'tagName')
        .should('eq', 'dt-influence-factors'.toUpperCase());
    });

    it('should switch between tools', function () {
      /*
        How we display alternative and objective counts:
                cy.getBySel('alternative-container')
                  .should('have.length', f + p + c)
                                         ^   ^   ^
                                         |   |   |
                           from fixture -*   |   |
          from previous tool under test -----*   |
           from current tool under test ---------*
      */
      const decisionData: DecisionData = this.decisionData;

      // Lever method
      {
        switchToStructureAndEstimateTool('sae-lever-method');

        // Should add alternative 'foo'.
        cy.getBySel('add-new-configuration').click();
        cy.getBySel('configuration-box').last().type('foo');
        cy.getBySel('configuration-box').last().type('{enter}');
        cy.then(() => expect(decisionData.alternatives[decisionData.alternatives.length - 1].name).to.equal('foo'));

        // Should add alternative 'bar'.
        cy.getBySel('add-new-configuration').click();
        cy.getBySel('configuration-box').last().type('bar');
        cy.getBySel('configuration-box').last().type('{enter}');
        cy.then(() => expect(decisionData.alternatives[decisionData.alternatives.length - 1].name).to.equal('bar'));

        // Should delete alternative 'bar'.

        cy.get('dt-alternative-list dt-object-box').last().findBySel('delete-button').click();

        cy.confirmPopover();
        cy.get('dt-alternative-list dt-object-box').should('have.length', 6 + 1);
        cy.then(() => {
          expect(decisionData.alternatives.map(alternative => alternative.name)).to.not.contain('bar');
        });

        // Should expand the last inputbox to (temporarily) rename the last alternative 'foo'.
        cy.get('dt-alternative-list dt-object-box')
          .last()
          .within(() => {
            cy.getBySel('expand-button').click();

            cy.getBySel('name-input').type('{selectall}foofoo');
            cy.then(() => {
              expect(decisionData.alternatives[decisionData.alternatives.length - 1].name).to.equal('foofoo');
            });

            cy.getBySel('name-input').type('{selectall}foo');
            cy.then(() => {
              expect(decisionData.alternatives[decisionData.alternatives.length - 1].name).to.equal('foo');
            });
          });
      }
      // New alternatives: 'foo' (lever method)

      // Objective-focused search
      {
        switchToStructureAndEstimateTool('sae-objective-focused-search', false);

        // Should add alternative 'foobar'.
        cy.getBySel('add-alternative-input').type('foobar');
        cy.getBySel('add-alternative-button').click();
        cy.then(() => expect(decisionData.alternatives[decisionData.alternatives.length - 1].name).to.equal('foobar'));

        // Should add alternative 'baz'.
        cy.getBySel('add-alternative-input').type('baz');
        cy.getBySel('add-alternative-button').click();
        cy.then(() => expect(decisionData.alternatives[decisionData.alternatives.length - 1].name).to.equal('baz'));

        // Should delete alternative 'foobar' which is the second to last alternative.

        cy.get('dt-alternative-list dt-object-box').eq(-2).findBySel('delete-button').click();
        cy.confirmPopover();

        cy.get('dt-alternative-list dt-object-box')
          .should('have.length', 6 + 1 + 1)
          .then(() => {
            expect(decisionData.alternatives.map(alternative => alternative.name)).to.not.contain('foobar');
          });

        // Should rename last alternative 'baz'.
        cy.get('dt-alternative-list dt-object-box')
          .last()
          .within(() => {
            cy.getBySel('expand-button').click();
            cy.getBySel('name-input').type('{selectall}bazbaz');
            cy.then(() => {
              expect(decisionData.alternatives[decisionData.alternatives.length - 1].name).to.equal('bazbaz');
            });
            cy.getBySel('name-input').type('{selectall}baz');
            cy.then(() => {
              expect(decisionData.alternatives[decisionData.alternatives.length - 1].name).to.equal('baz');
            });
          });
      }
      // New alternatives: 'foo' (lever method), 'baz' (objective-focused search)

      // Weak points
      {
        switchToStructureAndEstimateTool('sae-weak-points', false);

        cy.getBySel('dt-alternative-hint2-steps').within(() => {
          // Should add alternative 'qux' via weak point analysis.
          cy.getBySel('alternative-selection-button').click();
          cy.document().find('.mat-mdc-menu-content .mat-mdc-menu-item').first().click();
          cy.getBySel('objective-selection-button').click();
          cy.document().find('.mat-mdc-menu-content .mat-mdc-menu-item').first().click();
          cy.getBySel('alternative-name-input').type('qux');
          cy.getBySel('alternative-add-button').click();
          cy.then(() => expect(decisionData.alternatives[decisionData.alternatives.length - 1].name).to.equal('qux'));

          // Should add alternative 'quux' via weak point analysis.
          cy.getBySel('alternative-selection-button').click();
          cy.document().find('.mat-mdc-menu-content .mat-mdc-menu-item').first().click();
          cy.getBySel('objective-selection-button').click();
          cy.document().find('.mat-mdc-menu-content .mat-mdc-menu-item').first().click();
          cy.getBySel('alternative-name-input').type('quux');
          cy.getBySel('alternative-add-button').click();
          cy.then(() => expect(decisionData.alternatives[decisionData.alternatives.length - 1].name).to.equal('quux'));
        });

        cy.get('dt-alternative-list').within(() => {
          // Should delete alternative 'qux'.
          cy.get('dt-object-box').last().findBySel('delete-button').click();
          cy.confirmPopover();
          cy.get('dt-object-box')
            .should('have.length', 6 + 2 + 1)
            .then(() => {
              expect(decisionData.alternatives[decisionData.alternatives.length - 1].name).to.equal('qux');
            });

          // Should add alternative 'quuxquux' via add button in the alternative list.
          cy.getBySel('alternative-add-button').click();
          cy.get('dt-object-box').last().findBySel('name-input').type('{selectall}quuxquux');
          cy.then(() => {
            expect(decisionData.alternatives[decisionData.alternatives.length - 1].name).to.equal('quuxquux');
          });

          // Should rename alternative 'quuxquux' into 'quux'.
          cy.get('dt-object-box').last().findBySel<HTMLTextAreaElement>('name-input').type('{selectall}quux');
          cy.then(() => {
            expect(decisionData.alternatives[decisionData.alternatives.length - 1].name).to.equal('quux');
          });
        });
      }
      // New alternatives: 'foo' (lever method), 'baz' (objective-focused search), 'qux' (weak points), 'quux' (weak points)

      // Alternative list
      {
        switchToStructureAndEstimateTool('sae-alternative-list', false);

        cy.get('dt-alternative-list').within(() => {
          // Should delete alternative 'quux'.
          cy.get('dt-object-box').last().findBySel('delete-button').click();
          cy.confirmPopover();

          cy.get('dt-object-box')
            .should('have.length', 6 + 4 - 1)
            .then(() => {
              expect(decisionData.alternatives.map(alternative => alternative.name)).to.not.contain('quux');
            });

          // Should add an alternative that...
          cy.getBySel('alternative-add-button').click();

          cy.get('dt-object-box')
            .last()
            .within(() => {
              // ...should be named 'corgecorge' to be than...
              cy.getBySel('name-input').type('corgecorge');
              cy.then(() => {
                expect(decisionData.alternatives[decisionData.alternatives.length - 1].name).to.equal('corgecorge');
              });

              // ...renamed to 'corge'.
              cy.getBySel('name-input').type('{selectall}corge');
              cy.then(() => {
                expect(decisionData.alternatives[decisionData.alternatives.length - 1].name).to.equal('corge');
              });
            });
        });
      }
      // New alternatives: 'foo' (lever method), 'baz' (objective-focused search), 'qux' (weak points), 'corge' (alternative list)

      // Influence factors
      switchToStructureAndEstimateTool('sae-influence-factors', false);

      // Assumptions
      switchToStructureAndEstimateTool('sae-assumptions', false);

      // Overview
      {
        const initialStatement = this.decisionData.decisionStatement.statement;

        switchToStructureAndEstimateTool('sae-overview', false);

        cy.getBySel('structure-and-estimate-decision-statement').within(() => {
          // Should stay with the old decision statement when we do not blur/commit.
          cy.getBySel('statement-input').type('{selectall}Professional rocks!');
          cy.then(() => expect(decisionData.decisionStatement.statement).to.equal(initialStatement));

          // Should commit the decision statement when we blur.
          cy.getBySel('statement-input').blur();
          cy.then(() => expect(decisionData.decisionStatement.statement).to.equal('Professional rocks!'));

          // Should stay with the old decision statement when we leave blank the field.
          cy.getBySel('statement-input').type('{selectall}{backspace}');
          cy.getBySel('statement-input').blur();
          cy.then(() => expect(decisionData.decisionStatement.statement).to.equal('Professional rocks!'));
        });

        cy.getBySel('structure-and-estimate-impact-model').within(() => {
          // Should add an objective (checked in the next chain).
          cy.getBySel('add-objective-button').click();

          cy.getBySel('objective-name-field').findBySel('name-input').last().as('nameinput');
          cy.get('@nameinput').click();
          cy.get('@nameinput').within($objectiveNameTextField => {
            // Should name the new objective 'grault'.
            cy.wrap($objectiveNameTextField).type('{selectall}grault');
            cy.wrap($objectiveNameTextField).blur();
            cy.then(() => expect(decisionData.objectives[decisionData.objectives.length - 1].name).to.equal('grault'));

            // Should reset to default objective name when we leave blank the field.
            cy.wrap($objectiveNameTextField).focus();
            cy.wrap($objectiveNameTextField).type('{selectall}{backspace}');
            cy.wrap($objectiveNameTextField).blur();
            cy.then(() =>
              expect(decisionData.objectives[decisionData.objectives.length - 1].name).to.equal('Ziel ' + decisionData.objectives.length),
            );
          });

          // Should add an alternative (checked in the next chain).
          cy.getBySel('add-alternative-button').click();

          cy.getBySel('alternative-name-field').findBySel('name-input').last().as('nameinput');
          cy.get('@nameinput').click();
          cy.get('@nameinput').within($alternativeNameTextField => {
            // Should name the new alternative 'garply'.
            cy.wrap($alternativeNameTextField).type('{selectall}garply');
            cy.wrap($alternativeNameTextField).blur();
            cy.then(() => expect(decisionData.alternatives[decisionData.alternatives.length - 1].name).to.equal('garply'));

            // Should reset to the default value when we leave blank the field.
            cy.wrap($alternativeNameTextField).focus();
            cy.wrap($alternativeNameTextField).type('{selectall}{backspace}');
            cy.wrap($alternativeNameTextField).blur();
            cy.then(() =>
              expect(decisionData.alternatives[decisionData.alternatives.length - 1].name).to.equal(
                'Alternative ' + decisionData.alternatives.length,
              ),
            );
          });

          // Should add and delete an objective called 'graultgarply'.
          cy.getBySel('add-objective-button').click();
          cy.getBySel('objective-name-field').last().click();
          cy.getBySel('objective-name-field').last().type('{selectall}graultgarply');
          cy.getBySel('objective-actions').last().trigger('mouseenter');
          // Escape the within, because the cdk-overlay of the popover is outside of it
          cy.document().within(() => {
            cy.getBySel('delete-objective-button').click();
          });
          cy.confirmPopover();
          cy.getBySel('objective-name-field')
            .should('have.length', 6 + 0 + (1 + 1 - 1))
            .then(() => {
              expect(decisionData.objectives.map(objective => objective.name)).to.not.contain('graultgarply');
            });

          // Should add and delete an objective called 'garplygrault'.
          cy.getBySel('add-alternative-button').click();
          cy.getBySel('alternative-name-field').last().click();
          cy.getBySel('alternative-name-field').last().type('{selectall}garplygrault');
          cy.getBySel('alternative-box').last().trigger('mouseover');
          // Escape the within, because the cdk-overlay of the popover is outside of it
          cy.document().within(() => {
            cy.getBySel('delete-alternative-button').click();
          });
          cy.confirmPopover();
          cy.getBySel('alternative-name-field')
            .should('have.length', 6 + 4 + (1 + 1 - 1))
            .then(() => {
              expect(decisionData.alternatives.map(alternatives => alternatives.name)).to.not.contain('garplygrault');
            });

          // Should interpolate the outcome cells when transitioning into the next tab.
          cy.getBySel('outcome').findBySel('invalid').should('exist');
          // Since .findBySel returns an undefined subject for some reason fallback to .find.
          cy.document().its('body').find('[data-cy=phase-structure-and-estimate]').click();
          cy.document().its('body').find('[data-cy=phase-evaluate-and-decide]').click();
          cy.getBySel('outcome').findBySel('invalid').should('not.exist');
        });
      }
      /*
        New alternatives: 'foo' (lever method),
        'baz' (objective-focused search),
        'qux' (weak points),
        'corge' (alternative list),
        'garply' (overview)
       */
      // New objectives: 'grault' (overview)

      // Hierarchy
      switchToStructureAndEstimateTool('sae-hierarchy', false);

      // Checklists

      // TODO: add proper e2e tests for "checklists"
      /*
        New alternatives:
        'foo' (lever method),
        'baz' (objective-focused search),
        'qux' (weak points),
        'corge' (alternative list),
        'garply' (overview)
      */
      // New objectives: 'grault' (overview)

      switchToStructureAndEstimateTool('sae-objective-list', false);
    });
  });

  describe('evaluate-and-decide', () => {
    it("should switch to tool 'overview'", function () {
      cy.getBySel('chart-ranking-container').should('have.attr', 'dtExplainable');
      cy.get('dt-objective-weighting-overview').should('have.attr', 'dtExplainable');
    });

    it("should switch to tool 'overview' via tool selection", function () {
      switchToEvaluateAndDecideTool('ead-overview');
      cy.getBySel('chart-ranking-container').should('have.attr', 'dtExplainable');
      cy.get('dt-objective-weighting-overview').should('have.attr', 'dtExplainable');
    });

    it("should switch to tool 'utility-functions'", function () {
      switchToEvaluateAndDecideTool('ead-utility-functions');

      // Should switch back and forth (one round) between the main and detailed view.
      // Should do that with the select and the select should show the right active objective.
      cy.get('dt-utility-function-navigation').selectOptionFromDtSelect('Übersicht', 'Möglichst hohes eigenes Wohlempfinden');

      cy.selectOptionFromDtSelect('Möglichst hohes eigenes Wohlempfinden', 'Übersicht');
    });

    it("should switch to tool 'objective-weighting'", function () {
      switchToEvaluateAndDecideTool('ead-objective-weighting');

      // Should switch back and forth (one round) between the main and detailed view.
      // Should do that with the select and the select should show the right active objective.
      cy.get('dt-objective-weighting-navigation').selectOptionFromDtSelect(
        'Übersicht',
        'Möglichst umfängliche Freude an meinen Leidenschaften',
      );

      cy.get('dt-objective-weighting-navigation').selectOptionFromDtSelect(
        'Möglichst umfängliche Freude an meinen Leidenschaften',
        'Übersicht',
      );
    });

    it("should switch to tool 'sensitivity-analysis'", function () {
      switchToEvaluateAndDecideTool('ead-sensitivity-analysis');
    });

    it("should switch to tool 'pros-and-cons'", function () {
      switchToEvaluateAndDecideTool('ead-pros-and-cons');

      cy.getDtSelect('Netzdiagramm (absolut)').should('exist');

      cy.get('dt-polar-chart').findBySel('polar-chart-dimension-label').should('have.length', 6);

      cy.selectOptionFromDtSelect('Netzdiagramm (absolut)', 'Balkendiagramm (relativ)');

      cy.get('dt-pro-and-contra').within(() => {
        cy.get('dt-pro-contra-bar-chart').within(() => {
          cy.getBySel('pro-contra-grid-container').should('exist');
        });
      });
    });

    it("should switch to tool 'robustness-check'", function () {
      switchToEvaluateAndDecideTool('ead-utility-functions');

      cy.get('dt-utility-function-main-element-numerical-or-indicator').first().findBySel('precision-input').type('{selectall}0,5');

      switchToEvaluateAndDecideTool('ead-robustness-check', false);

      cy.get('dt-robustness-check').get('dt-robustness-check-chart').should('exist');
    });

    it("should switch to tool 'cost-utility-analysis'", function () {
      switchToEvaluateAndDecideTool('ead-cost-utility-analysis', false);

      cy.get('dt-cost-utility-analysis').get('.chart-wrapper').should('exist');
    });

    it("should switch to tool 'utility-based-tornado-diagram'", function () {
      switchToEvaluateAndDecideTool('ead-utility-based-tornado-diagram', false);

      cy.get('dt-utility-based-tornado-diagram').get('.chart-container').should('exist');
    });
  });
});
