import { DecisionData } from '@entscheidungsnavi/decision-data';

describe('time-tracking', function () {
  beforeEach(function () {
    cy.visit('/');
    cy.loadAlex();
    cy.window().then(window => {
      expect(window['DecisionData']).to.exist;
      cy.window().its('DecisionData').as('decisionData');

      const decisionData: DecisionData = window['DecisionData'];

      // Reset All Timers to 0
      Array.from(Object.entries(decisionData.timeRecording.timers)).forEach(
        ([_, timerList]) =>
          timerList?.forEach(timer => {
            timer.activeTime = 0;
            timer.totalTime = 0;
          }),
      );
    });
  });

  it('tracks-time', function () {
    const decisionData: DecisionData = this.decisionData;

    expect(decisionData.timeRecording.timers.decisionStatement[0].activeTime).to.be.eq(0);

    cy.getBySel('step-decisionStatement').click();
    cy.getBySel('substep-0').click();

    // eslint-disable-next-line cypress/no-unnecessary-waiting
    cy.wait(1000).getBySel('substep-1').click();
    cy.then(() => {
      expect(decisionData.timeRecording.timers.decisionStatement[0].activeTime).to.be.within(0.5, 1.5);
    });
  });

  it('detects-changes', function () {
    const decisionData: DecisionData = this.decisionData;

    cy.getBySel('step-alternatives').click();

    // eslint-disable-next-line cypress/no-unnecessary-waiting
    cy.wait(1000).then(() => {
      expect(decisionData.timeRecording.timers.alternatives[7].activeTime).to.be.eq(0);
      cy.getBySel('alternative-add-button').click();

      cy.then(() => {
        const alternativeTime = () => decisionData.timeRecording.timers.alternatives[7].activeTime;
        cy.wrap(alternativeTime, { timeout: 10000 }).should(time => expect(time()).to.not.be.eq(0));
      });
    });
  });
});
