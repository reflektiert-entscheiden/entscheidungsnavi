describe('Hierarchy', () => {
  beforeEach(() => {
    cy.visit('/');
    cy.loadAlex();

    cy.window().its('DecisionData').as('decisionData');
  });

  describe('ObjectiveHierarchy', () => {
    beforeEach(() => {
      cy.getBySel('step-objectives').click();
    });

    it('should allow selecting and deleting multiple elements', function () {
      cy.get('dt-hierarchy-toolbar').should('exist');
      cy.get('dt-hierarchy-toolbar').findBySel('focused-elements-indicator').should('not.exist');

      getHierarchyNode('Möglichst umfängliche Freude an meinen Leidenschaften').click();
      cy.get('dt-hierarchy-toolbar').findBySel('focused-elements-indicator').should('contain.text', '1 Element');

      getHierarchyNode('Qualifikationen').click({ ctrlKey: true });
      cy.get('dt-hierarchy-toolbar').findBySel('focused-elements-indicator').should('contain.text', '2 Elemente');

      cy.get('dt-hierarchy-toolbar').findBySel('toolbar-delete-button').click();
      cy.getBySel('delete-objective-modal').findBySel('confirm-delete-button').click();
      cy.waitForModalToClose();

      cy.get('dt-hierarchy-toolbar').findBySel('focused-elements-indicator').should('not.exist');
      getHierarchyNode('Möglichst umfängliche Freude an meinen Leidenschaften').should('not.exist');
      getHierarchyNode('Qualifikationen').should('not.exist');

      cy.then(() => {
        expect(this.decisionData.objectives).to.have.length(5);
      });
    });

    it('should allow adding, renaming, and deleting individual elements', function () {
      getHierarchyNode('Möglichst umfängliche Freude an meinen Leidenschaften').within(() => {
        cy.root().click();
        cy.getBySel('add-button-opener').click();
        cy.get('.add.bottom').click();
      });

      cy.get('body').type('New objective{enter}');

      cy.then(() => {
        expect(this.decisionData.objectives).to.have.length(7);
        expect(this.decisionData.objectives[3].name).to.equal('New objective');
      });

      getHierarchyNode('New objective').findBySel('delete-button').click();

      cy.then(() => {
        expect(this.decisionData.objectives).to.have.length(6);
        expect(this.decisionData.objectives[3].name).not.to.equal('New objective');
      });
    });

    it('should allow moving elements', function () {
      getHierarchyNode('Möglichst viele und wertvolle soziale Bindungen').drag(
        getHierarchyNode('Möglichst umfängliche Freude an meinen Leidenschaften'),
        { offsetX: 0, offsetY: 0 },
      );

      cy.then(() => {
        expect(this.decisionData.objectives[2].name).to.equal('Möglichst viele und wertvolle soziale Bindungen');
        expect(this.decisionData.objectives[3].name).to.equal('Möglichst umfängliche Freude an meinen Leidenschaften');
      });

      getHierarchyNode('Möglichst ausgeprägtes Gefühl der eigenen Kompetenz').drag(getHierarchyNode('finanzieller Puffer'), {
        offsetX: 0,
        offsetY: 20,
      });
      cy.getBySel('delete-objective-modal').findBySel('confirm-delete-button').click();
      cy.waitForModalToClose();

      cy.then(() => {
        expect(this.decisionData.objectives).to.have.length(5);
        expect(this.decisionData.objectives[4].aspects.children[2].value.name).to.equal(
          'Möglichst ausgeprägtes Gefühl der eigenen Kompetenz',
        );
      });
    });

    it('should allow dragging to and from trash and brainstorming', function () {
      cy.getBySel('trash-bin-box').click();
      cy.getBySel('brainstorming-box').click();

      cy.getBySel('trash-bin-list').find('li').first().drag(cy.getBySel('brainstorming-box'));
      cy.getBySel('trash-bin-list').find('li').first().drag(getHierarchyNode('Möglichst viele und wertvolle soziale Bindungen'));

      getHierarchyNode('Partner').first().drag(cy.getBySel('brainstorming-box'));

      cy.then(() => {
        expect(this.decisionData.objectives).to.have.length(7);
        expect(this.decisionData.objectives[3].name).to.equal('Finanzielle Sicherheit');

        expect(this.decisionData.objectiveAspects.listOfAspects).to.have.length(3);
        expect(this.decisionData.objectiveAspects.listOfAspects[0].name).to.equal('Partner');
        expect(this.decisionData.objectiveAspects.listOfAspects[1].name).to.equal('mehr Zeit für den Partner');
        expect(this.decisionData.objectiveAspects.listOfAspects[2].name).to.equal('Kontakt zu altem Chef stärken');

        expect(this.decisionData.objectives[4].aspects.children).to.have.length(2);

        expect(this.decisionData.objectiveAspects.listOfDeletedAspects[0].name).to.equal('Kompetenzgefühl');
      });
    });
  });

  describe('ObjectiveHierarchy <=> SimpleHierarchy', () => {
    beforeEach(() => {
      cy.intercept('/api/quickstart/tags', { body: [] });
      cy.intercept('/api/quickstart/hierarchies?*', { fixture: 'quickstart-hierarchies.json' });

      cy.getBySel('step-objectives').click();
      cy.getBySel('substep-3').click();

      cy.getBySel('quickstart-hierarchy-option').click();
    });

    it('allows dragging from simple to objective hierarchy', function () {
      cy.get('dt-simple-hierarchy')
        .findBySel('hierarchy-node-wrapper')
        .filter(`:contains("Kosten")`)
        .drag(getHierarchyNode('Möglichst viele und wertvolle soziale Bindungen'));
      cy.confirmPopover();

      cy.then(() => {
        expect(this.decisionData.objectives[3].name).to.equal('Kosten');
        expect(this.decisionData.objectives[3].aspects.children).to.have.length(4);
        expect(this.decisionData.objectives[3].aspects.children[0].value.name).to.equal('Lebenshaltungskosten');
      });
    });
  });
});

function getHierarchyNode(withText: string) {
  return cy.get('dt-objective-hierarchy').findBySel('hierarchy-node-wrapper').filter(`:contains("${withText}")`);
}
