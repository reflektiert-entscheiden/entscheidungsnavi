// ***********************************************
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************

// eslint-disable-next-line @typescript-eslint/no-namespace
declare namespace Cypress {
  // eslint-disable-next-line @typescript-eslint/no-unused-vars,@typescript-eslint/naming-convention
  interface Chainable<Subject> {
    getBySel<T extends Node = HTMLElement>(
      selector: string,
      options?: Partial<Loggable & Timeoutable & Withinable & Shadow>,
    ): Chainable<JQuery<T>>;

    findBySel<T extends Node = HTMLElement>(selector: string, options?: Partial<Loggable & Timeoutable & Shadow>): Chainable<JQuery<T>>;

    findDtModal(): Chainable<JQuery>;

    // Commands for `dt-select`.

    getDtSelect<T extends Node = HTMLElement>(
      optionSelected: string,
      options?: Partial<Loggable & Timeoutable & Shadow>,
    ): Chainable<JQuery<T>>;

    getOptionFromDtSelect<T extends Node = HTMLElement>(optionText: string): Chainable<JQuery<T>>;

    selectOptionFromDtSelect(optionSelected: string, optionText: string): void;

    // API commands

    createAndLoginUser(email: string, password: string): void;

    uploadProject(name: string, data: string): void;

    deleteCurrentUser(password: string): void;

    // Other commands

    loadFixtureProject(fixtureName: string): void;

    loadAlex(): void;

    waitForMenuToClose(): Chainable<JQuery>;

    waitForModalToClose(): Chainable<JQuery>;

    confirmPopover(): void;

    drag<T extends Node = HTMLElement>(
      target: Chainable<JQuery<T>>,
      options?: Partial<TriggerOptions & ObjectLike>,
    ): Cypress.Chainable<JQuery<T>>;
  }
}

// adapted from https://docs.cypress.io/guides/references/best-practices#Real-World-Example
Cypress.Commands.add('getBySel', (selector, ...args) => {
  return cy.get(`[data-cy=${selector}]`, ...args);
});

Cypress.Commands.add('findBySel', { prevSubject: true }, (subject, selector, ...args) => {
  return subject.find(`[data-cy=${selector}]`, ...args);
});

Cypress.Commands.add('findDtModal', () => {
  return cy.document().its('body').find('dt-modal-content');
});

// Commands for `dt-select`.

Cypress.Commands.add('getDtSelect', optionSelected => {
  return cy.get(`dt-select[data-cy="${optionSelected}"]`).should('have.length', 1).first();
});

Cypress.Commands.add('getOptionFromDtSelect', { prevSubject: true }, (subject, optionText) => {
  expect(subject.prop('tagName')).to.be.equal('DT-SELECT', 'You can only use this command with `dt-select` as a subject.');
  const currentlySelectedOption = subject.attr('data-cy');
  expect(currentlySelectedOption).not.to.be.empty;

  return cy.get(`mat-selection-list[data-cy="dt-select-${currentlySelectedOption}"]`).find('mat-list-option').contains(optionText);
});

Cypress.Commands.add('selectOptionFromDtSelect', (optionSelected, optionText) => {
  cy.getDtSelect(optionSelected).click();
  cy.getDtSelect(optionSelected).getOptionFromDtSelect(optionText).click();
  cy.waitForMenuToClose();
});

// API commands

Cypress.Commands.add('createAndLoginUser', (email, password) => {
  cy.request('GET', '/api/auth/csrf-token').then(response => {
    const headers = { 'x-csrf-token': response.body.token };
    const body = { email, password };

    // If the user already exists, the first request fails. This is fine.
    cy.request({ method: 'POST', url: '/api/users', body, headers, failOnStatusCode: false });
    cy.request({ method: 'POST', url: '/api/auth/login', body, headers });
  });
});

Cypress.Commands.add('uploadProject', (name, data) => {
  cy.request('GET', '/api/auth/csrf-token').then(response => {
    const headers = { 'x-csrf-token': response.body.token };
    const body = { name, data };

    cy.request({ method: 'POST', url: '/api/projects', body, headers });
  });
});

Cypress.Commands.add('deleteCurrentUser', password => {
  cy.request('GET', '/api/auth/csrf-token').then(response => {
    const headers = { 'x-csrf-token': response.body.token };
    cy.request({ method: 'DELETE', url: '/api/user', body: { password }, headers });
  });
});

// Other commands.

Cypress.Commands.add('loadFixtureProject', (fixtureName: string) => {
  cy.getBySel('project_mgmt').click();

  cy.fixture(fixtureName).then(fixtureJson => {
    cy.getBySel<HTMLInputElement>('project_file_input').then(function (el) {
      const blob = new Blob([JSON.stringify(fixtureJson)], { type: 'application/json' });
      const file = new File([blob], fixtureName, { type: 'application/json' });
      const list = new DataTransfer();

      list.items.add(file);
      const myFileList = list.files;

      el[0].files = myFileList;
      el[0].dispatchEvent(new Event('change', { bubbles: true }));
    });
  });
});

Cypress.Commands.add('loadAlex', () => {
  cy.loadFixtureProject('alex.json');
});

Cypress.Commands.add('waitForMenuToClose', () => {
  cy.get('.mat-mdc-menu-panel').should('not.exist');
});

Cypress.Commands.add('waitForModalToClose', () => {
  cy.get('.mdc-dialog').should('not.exist');
});

Cypress.Commands.add('confirmPopover', () => {
  cy.getBySel('confirm-popover-confirm-button', { withinSubject: null }).click();
});

Cypress.Commands.add('drag', { prevSubject: true }, (subject, target, options) => {
  const dataTransfer = new DataTransfer();

  cy.wrap(subject).trigger('dragstart', { dataTransfer });
  return target.trigger('drop', { dataTransfer, ...options });
});
