// ***********************************************************
// This is a great place to put global configuration and
// behavior that modifies Cypress.
//
// You can read more here:
// https://on.cypress.io/configuration
// ***********************************************************

import './commands';
import '@percy/cypress';

Cypress.on('uncaught:exception', err => !err.message.includes('ResizeObserver loop'));

afterEach(() => {
  cy.get('[data-cy=navi_error]').should('have.css', 'display', 'none');
  cy.window().its('dt.lastError').should('be.null');
});
