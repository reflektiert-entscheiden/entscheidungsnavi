import { ConfigService } from '@nestjs/config';
import { Test } from '@nestjs/testing';
import { MongoMemoryServer } from 'mongodb-memory-server';
import mongoose from 'mongoose';
import request from 'supertest';
import { NestExpressApplication } from '@nestjs/platform-express';
import session from 'express-session';
import passport from 'passport';
import { noop } from 'lodash';
import { AppModule } from '../src/app/app.module';
import { loginUser, USERS } from './test-helper';

jest.setTimeout(30000);

describe('Events (e2e)', () => {
  let app: NestExpressApplication;
  let mongod: MongoMemoryServer;

  let eventManagerCookie: string[];
  let basicUserCookie: string[];
  let basicUserCookie2: string[];
  let adminUserCookie: string[];

  beforeAll(async () => {
    mongod = await MongoMemoryServer.create();
    const uri = await mongod.getUri('navi');

    await mongoose.connect(uri);

    const module = await Test.createTestingModule({
      imports: [AppModule],
    })
      .overrideProvider(ConfigService)
      .useValue({
        get: (key: string) => {
          switch (key) {
            case 'NODE_ENV':
              return 'test';
            case 'BASE_URL':
              return 'http://dev.entscheidungsnavi.de';
            case 'MONGODB_URI':
              return uri;
            default:
              return null;
          }
        },
      })
      .compile();

    app = module
      .createNestApplication<NestExpressApplication>()
      .setGlobalPrefix('/api')
      .use(session({ secret: 'testingsecret', resave: false, saveUninitialized: false }))
      .use(passport.initialize())
      .use(passport.session());

    // Override default session middlewares
    app.get(AppModule).configure = noop;

    await app.init();

    // Setup some users
    const { db } = mongoose.connection;
    await db.collection('users').insertMany([USERS.user, USERS.eventManager, USERS.admin, USERS.user2]);

    // Log the users in
    basicUserCookie = await loginUser(app, 'user');
    basicUserCookie2 = await loginUser(app, 'user2');
    adminUserCookie = await loginUser(app, 'admin');
    eventManagerCookie = await loginUser(app, 'eventManager');
  });

  afterAll(async () => {
    await app.close();
    await mongoose.disconnect();
    await mongod.stop();
  });

  beforeEach(async () => {
    // Clear the events table
    await mongoose.connection.db.collection('events').deleteMany({});
  });

  it('requires the event-manager role for event creation', async () => {
    await request(app.getHttpServer()).post('/api/events').send({ name: 'new event' }).set('Cookie', basicUserCookie).expect(403);
  });

  it('retrieves events for non event-manager editors', async () => {
    await request(app.getHttpServer())
      .post('/api/events')
      .send({ name: 'new event', editors: [{ email: 'user@entscheidungsnavi.de' }], viewers: [] })
      .set('Cookie', eventManagerCookie)
      .expect(201);

    const result = (await request(app.getHttpServer()).get('/api/events').set('Cookie', basicUserCookie).expect(200)).body;
    expect(result).toHaveLength(1);
  });

  it('retrieves events for non event-manager viewers', async () => {
    await request(app.getHttpServer())
      .post('/api/events')
      .send({ name: 'new event', editors: [], viewers: [{ email: 'user@entscheidungsnavi.de' }] })
      .set('Cookie', eventManagerCookie)
      .expect(201);

    const result = (await request(app.getHttpServer()).get('/api/events').set('Cookie', basicUserCookie).expect(200)).body;
    expect(result).toHaveLength(1);
  });

  it('does not retrieve events for non event-manager users who are neither editors nor viewers', async () => {
    await request(app.getHttpServer())
      .post('/api/events')
      .send({ name: 'new event', editors: [], viewers: [] })
      .set('Cookie', eventManagerCookie)
      .expect(201);

    const result = (await request(app.getHttpServer()).get('/api/events').set('Cookie', basicUserCookie).expect(200)).body;
    expect(result).toHaveLength(0);
  });

  it('create event, get events, update event, get event', async () => {
    await request(app.getHttpServer()).post('/api/events').send({ name: 'new event' }).set('Cookie', eventManagerCookie).expect(201);

    const events = (await request(app.getHttpServer()).get('/api/events').set('Cookie', eventManagerCookie).expect(200)).body;
    expect(events.length).toBe(1);
    expect(events[0].name).toBe('new event');

    await request(app.getHttpServer())
      .patch(`/api/events/${events[0].id}`)
      .send({ name: 'updated' })
      .set('Cookie', eventManagerCookie)
      .expect(200);

    const event = (await request(app.getHttpServer()).get(`/api/events/${events[0].id}`).set('Cookie', eventManagerCookie).expect(200))
      .body;
    expect(event.name).toBe('updated');
  });

  it('creates an event and changes its permission', async () => {
    await request(app.getHttpServer()).post('/api/events').send({ name: 'new event' }).set('Cookie', eventManagerCookie).expect(201);

    const events = (await request(app.getHttpServer()).get('/api/events').set('Cookie', eventManagerCookie).expect(200)).body;
    expect(events.length).toBe(1);

    await request(app.getHttpServer())
      .patch(`/api/events/${events[0].id}`)
      .send({ owner: { email: 'non-existent@entscheidungsnavi.de' } })
      .set('Cookie', eventManagerCookie)
      .expect(404);

    await request(app.getHttpServer())
      .patch(`/api/events/${events[0].id}`)
      .send({ owner: { email: 'user@entscheidungsnavi.de' } })
      .set('Cookie', eventManagerCookie)
      .expect(200);

    // The manager has lost access
    expect((await request(app.getHttpServer()).get('/api/events').set('Cookie', eventManagerCookie).expect(200)).body.length).toBe(0);
  });

  it('retrieves all registrations of an event and get the submission of a participant for an event', async () => {
    const eventCode = 'testcode';
    // create event
    await request(app.getHttpServer())
      .post('/api/events')
      .send({ name: 'new event', code: eventCode })
      .set('Cookie', eventManagerCookie)
      .expect(201);

    const event = (await request(app.getHttpServer()).get('/api/events').set('Cookie', eventManagerCookie).expect(200)).body;

    // join event
    await request(app.getHttpServer())
      .post('/api/user/event-registrations')
      .set('Cookie', basicUserCookie)
      .send({ code: eventCode })
      .expect(201);
    await request(app.getHttpServer())
      .post('/api/user/event-registrations')
      .set('Cookie', adminUserCookie)
      .send({ code: eventCode })
      .expect(201);
    await request(app.getHttpServer())
      .post('/api/user/event-registrations')
      .set('Cookie', eventManagerCookie)
      .send({ code: eventCode })
      .expect(201);

    // get all registrations
    // only event manager is authorized to retrieve all registrations
    await request(app.getHttpServer())
      .get(`/api/events/${event[0].id}/registrations`)
      .set('Cookie', basicUserCookie)
      .send({ eventId: event[0].id })
      .expect(403);

    const eventRegistrationList = (
      await request(app.getHttpServer())
        .get(`/api/events/${event[0].id}/registrations`)
        .set('Cookie', eventManagerCookie)
        .send({ eventId: event[0].id })
        .expect(200)
    ).body;

    expect(eventRegistrationList.items).toHaveLength(3);

    expect(eventRegistrationList.count).toEqual(eventRegistrationList.items.length);

    // make a submission for the basic user so we can retrieve it
    const questionnaireResponses = [
      ['345678', 3, 45, 1],
      [5, 2, 4],
    ];
    const freeText = 'Here is my free text (:';

    const eventSubmissionForBasicUser = {
      questionnaireResponses: questionnaireResponses,
      projectData: "{version: '0.0.0'}",
      freeText: freeText,
    };

    const eventRegistrationIdForBasicUser = eventRegistrationList.items[2].id;

    // update the free text, submitted project and questionnaire responses for the basic user
    await request(app.getHttpServer())
      .patch(`/api/user/event-registrations/${eventRegistrationIdForBasicUser}`)
      .set('Cookie', basicUserCookie)
      .send(eventSubmissionForBasicUser)
      .expect(200);

    await request(app.getHttpServer())
      .get(`/api/events/${event[0].id}/registrations/${eventRegistrationIdForBasicUser}`)
      .set('Cookie', basicUserCookie2)
      .expect(403);

    // retrieve the new event registration and extract the submission
    const eventRegistrationForBasicUser = (
      await request(app.getHttpServer())
        .get(`/api/events/${event[0].id}/registrations/${eventRegistrationIdForBasicUser}`)
        .set('Cookie', eventManagerCookie)
        .expect(200)
    ).body;

    const retrievedEventSubmissionForBasicUser = {
      projectData: eventRegistrationForBasicUser.projectData,
      freeText: eventRegistrationForBasicUser.freeText,
      questionnaireResponses: eventRegistrationForBasicUser.questionnaireResponses,
    };

    expect(retrievedEventSubmissionForBasicUser.projectData).toEqual(eventSubmissionForBasicUser.projectData);
    expect(retrievedEventSubmissionForBasicUser.questionnaireResponses).toEqual(eventSubmissionForBasicUser.questionnaireResponses);
    expect(retrievedEventSubmissionForBasicUser.freeText).toEqual(eventSubmissionForBasicUser.freeText);
  });
});
