import { ConfigService } from '@nestjs/config';
import { Test } from '@nestjs/testing';
import { MongoMemoryReplSet } from 'mongodb-memory-server';
import request from 'supertest';
import mongoose, { Types } from 'mongoose';
import { NestExpressApplication } from '@nestjs/platform-express';
import passport from 'passport';
import session from 'express-session';
import { noop, omit } from 'lodash';
import { QuickstartQuestionDto, QuickstartQuestionWithCountsDto, QuickstartTagDto } from '@entscheidungsnavi/api-client';
import { AppModule } from '../src/app/app.module';
import { CreateQuickstartQuestionDto } from '../src/app/quickstart/dto/quickstart-question.dto';
import { getSessionCookies, loginUser, USERS } from './test-helper';

jest.setTimeout(30000);

describe('Backend (e2e)', () => {
  let app: NestExpressApplication;
  let mongod: MongoMemoryReplSet;

  beforeAll(async () => {
    mongod = await MongoMemoryReplSet.create({ replSet: { name: 'ra0' } });
    await mongod.waitUntilRunning();
    const uri = mongod.getUri('navi');

    await mongoose.connect(uri);

    const module = await Test.createTestingModule({
      imports: [AppModule],
    })
      .overrideProvider(ConfigService)
      .useValue({
        get: (key: string) => {
          switch (key) {
            case 'NODE_ENV':
              return 'test';
            case 'BASE_URL':
              return 'http://dev.entscheidungsnavi.de';
            case 'MONGODB_URI':
              return uri;
            case 'SESSION_SECRET':
              return 'somesecret';
            default:
              return null;
          }
        },
      })
      .compile();

    app = module
      .createNestApplication<NestExpressApplication>()
      .setGlobalPrefix('/api')
      .use(session({ secret: 'testingsecret', resave: false, saveUninitialized: false }))
      .use(passport.initialize())
      .use(passport.session());

    // Override default session middlewares
    app.get(AppModule).configure = noop;

    await app.init();
  });

  afterAll(async () => {
    await app.close();
    await mongoose.disconnect();
    await mongod.stop();
  });

  beforeEach(async () => {
    // Clear the database
    const { db } = mongoose.connection;
    await db.collection('users').deleteMany({});
    await db.collection('projects').deleteMany({});
  });

  it('create user, login, create project, load project, delete project', async () => {
    // Create user
    await request(app.getHttpServer())
      .post('/api/users')
      .send({
        email: 'test@entscheidungsnavi.de',
        password: '12345678',
      })
      .expect(201);

    // Login as said user
    const cookies = getSessionCookies(
      await request(app.getHttpServer())
        .post('/api/auth/login')
        .send({ email: 'test@entscheidungsnavi.de', password: '12345678' })
        .expect(200),
    );

    // Create project
    const projectToCreate = { name: 'new project', data: JSON.stringify({ version: '6.0.0' }) };
    const project = (await request(app.getHttpServer()).post('/api/projects').send(projectToCreate).set('Cookie', cookies).expect(201))
      .body;

    // Get project
    const fetchedProject = (await request(app.getHttpServer()).get(`/api/projects/${project.id}`).set('Cookie', cookies).expect(200)).body;
    expect(fetchedProject).toMatchObject(projectToCreate);

    // Delete project
    await request(app.getHttpServer()).delete(`/api/projects/${project.id}`).set('Cookie', cookies).expect(200);

    // Ensure project is deleted
    await request(app.getHttpServer()).get(`/api/projects/${project.id}`).set('Cookie', cookies).expect(404);
  });

  describe('User', () => {
    let cookies: string[];

    beforeEach(async () => {
      await mongoose.connection.db.collection('users').insertOne(USERS.user);
      cookies = await loginUser(app, 'user');
    });

    it('fetches the user', async () => {
      const result = await request(app.getHttpServer()).get('/api/user').set('Cookie', cookies).expect(200);
      expect(result.body).toHaveProperty('email', 'user@entscheidungsnavi.de');
    });

    it('fails unauthenticated user fetching', async () => {
      await request(app.getHttpServer()).get('/api/user').expect(401);
    });

    it('updates the user', async () => {
      await request(app.getHttpServer()).patch('/api/user').send({ email: 'test@entscheidungsnavi.de' }).set('Cookie', cookies).expect(200);
      const result = await request(app.getHttpServer()).get('/api/user').set('Cookie', cookies).expect(200);
      expect(result.body).toHaveProperty('email', 'test@entscheidungsnavi.de');
    });

    it('fails unauthenticated user update', () => {
      return request(app.getHttpServer()).patch('/api/user').send({ name: 'new name' }).expect(401);
    });
  });

  describe('Projects', () => {
    let cookies1: string[];
    let cookies2: string[];
    const projectId = new Types.ObjectId();

    beforeEach(async () => {
      // Create and login two users
      await mongoose.connection.db.collection('users').insertMany([USERS.user, USERS.user2]);
      cookies1 = await loginUser(app, 'user');
      cookies2 = await loginUser(app, 'user2');

      // Create a project for user
      await mongoose.connection.db.collection('projects').insertOne({
        _id: projectId,
        name: 'new project',
        data: JSON.stringify({ version: '6.0.0' }),
        userId: USERS.user._id,
      });
    });

    it('retrieves project', async () => {
      const result = await request(app.getHttpServer()).get(`/api/projects/${projectId}`).set('Cookie', cookies1).expect(200);
      expect(result.body).toHaveProperty('name', 'new project');
    });

    it('fails retrieving unowned project', () => {
      return request(app.getHttpServer()).get(`/api/projects/${projectId}`).set('Cookie', cookies2).expect(404);
    });

    it('fails retrieving project unauthenticated', () => {
      return request(app.getHttpServer()).get(`/api/projects/${projectId}`).expect(401);
    });

    it('allows project creation', async () => {
      const creationResult = await request(app.getHttpServer())
        .post('/api/projects')
        .send({ name: 'test', data: JSON.stringify({ version: '6.0' }) })
        .set('Cookie', cookies2)
        .expect(201);
      expect(creationResult.body).toHaveProperty('id');

      const result = await request(app.getHttpServer()).get(`/api/projects/${creationResult.body.id}`).set('Cookie', cookies2).expect(200);
      expect(result.body).toHaveProperty('name', 'test');
    });

    it('fails unauthenticated project creation', () => {
      return request(app.getHttpServer())
        .post('/api/projects')
        .send({ name: 'test', data: JSON.stringify({ version: '6.0' }) })
        .expect(401);
    });

    it('allows updating projects', async () => {
      await request(app.getHttpServer()).patch(`/api/projects/${projectId}`).send({ name: 'new name' }).set('Cookie', cookies1).expect(200);

      const result = await request(app.getHttpServer()).get(`/api/projects/${projectId}`).set('Cookie', cookies1).expect(200);
      expect(result.body).toHaveProperty('name', 'new name');
    });

    it('fails unauthenticated project update', () => {
      return request(app.getHttpServer()).patch(`/api/projects/${projectId}`).send({ name: 'new name' }).expect(401);
    });

    it('fails updating unowned project', () => {
      return request(app.getHttpServer())
        .patch(`/api/projects/${projectId}`)
        .send({ name: 'new name' })
        .set('Cookie', cookies2)
        .expect(404);
    });
  });

  describe('Quickstart', () => {
    let quickstartManagerCookies: string[];
    let userCookies: string[];

    beforeEach(async () => {
      await mongoose.connection.db.collection('users').insertMany([USERS.user, USERS.quickstartManager, USERS.admin]);
      userCookies = await loginUser(app, 'user');
      quickstartManagerCookies = await loginUser(app, 'quickstartManager');
    });

    it('create tag, create project with tag, get project, delete tag, get project, delete project', async () => {
      // Create tag
      const tag = (
        await request(app.getHttpServer())
          .post('/api/quickstart/tags')
          .send({ name: { en: 'tag name', de: 'tag name' } })
          .set('Cookie', quickstartManagerCookies)
          .expect(201)
      ).body;

      // Create project
      const project = (
        await request(app.getHttpServer())
          .post('/api/quickstart/projects')
          .send({ name: 'new project', data: JSON.stringify({ version: '6.0.0' }), tags: [tag.id] })
          .set('Cookie', quickstartManagerCookies)
          .expect(201)
      ).body;

      // Fetch project with tag
      const fetchedProjectWithTag = (
        await request(app.getHttpServer()).get(`/api/quickstart/projects/${project.id}`).set('Cookie', quickstartManagerCookies).expect(200)
      ).body;
      expect(fetchedProjectWithTag).toMatchObject({
        name: 'new project',
        data: JSON.stringify({ version: '6.0.0' }),
        tags: [tag.id],
      });

      // Delete tag (this should delete the tag from the project)
      await request(app.getHttpServer()).delete(`/api/quickstart/tags/${tag.id}`).set('Cookie', quickstartManagerCookies).expect(200);

      // Fetch project without tag
      const fetchedProjectWithoutTag = (
        await request(app.getHttpServer()).get(`/api/quickstart/projects/${project.id}`).set('Cookie', quickstartManagerCookies).expect(200)
      ).body;
      expect(fetchedProjectWithoutTag).toMatchObject({
        name: 'new project',
        data: JSON.stringify({ version: '6.0.0' }),
        tags: [],
      });

      // Delete project
      await request(app.getHttpServer())
        .delete(`/api/quickstart/projects/${project.id}`)
        .set('Cookie', quickstartManagerCookies)
        .expect(200);

      // Make sure there are no projects or tags
      const projects = (await request(app.getHttpServer()).get('/api/quickstart/projects').expect(200)).body;
      expect(projects).toEqual([]);
      const tags = (await request(app.getHttpServer()).get('/api/quickstart/tags').expect(200)).body;
      expect(tags).toEqual([]);
    });

    it('create value, track stats, get value, delete value', async () => {
      const value = (
        await request(app.getHttpServer())
          .post('/api/quickstart/values')
          .send({ name: { en: 'new value', de: 'new value' } })
          .set('Cookie', quickstartManagerCookies)
          .expect(201)
      ).body;

      await request(app.getHttpServer())
        .post('/api/quickstart/values/metrics')
        .send({ values: [{ id: value.id, score: 1 }] })
        .expect(201);

      const fetchedValues = (
        await request(app.getHttpServer())
          .get('/api/quickstart/values?includeStats=true')
          .set('Cookie', quickstartManagerCookies)
          .expect(200)
      ).body;

      expect(fetchedValues).toHaveLength(1);
      expect(fetchedValues[0].accumulatedScore).toBe(1);
      expect(fetchedValues[0].countTracked).toBe(1);
      expect(fetchedValues[0].countInTop5).toBe(1);
      expect(fetchedValues[0].countAssigned).toBe(1);

      await request(app.getHttpServer()).delete(`/api/quickstart/values/${value.id}`).set('Cookie', quickstartManagerCookies).expect(200);

      expect(
        (
          await request(app.getHttpServer())
            .get('/api/quickstart/values?includeStats=true')
            .set('Cookie', quickstartManagerCookies)
            .expect(200)
        ).body,
      ).toHaveLength(0);
    });

    it('creates question, retrieves all questions, updates question, assigns question, retrieves question, deletes question', async () => {
      const questionToCreateDto: CreateQuickstartQuestionDto = {
        name: {
          de: 'german question name',
          en: 'english question name',
        },
        tags: [],
      };

      const userCookies = await loginUser(app, 'user');
      const quickstartManagerCookies = await loginUser(app, 'quickstartManager');

      // 1: Create question
      await request(app.getHttpServer()).post('/api/quickstart/questions').send(questionToCreateDto).expect(401);
      await request(app.getHttpServer()).post('/api/quickstart/questions').set('Cookie', userCookies).send(questionToCreateDto).expect(403);
      await request(app.getHttpServer())
        .post('/api/quickstart/questions')
        .set('Cookie', quickstartManagerCookies)
        .send(questionToCreateDto)
        .expect(201);

      // 2: Retrieve all questions without counts
      const createdQuestionWithoutCounts: QuickstartQuestionDto = (
        await request(app.getHttpServer()).get('/api/quickstart/questions?includeCounts=false').expect(200)
      ).body[0];

      expect(omit(createdQuestionWithoutCounts, ['id', 'createdAt', 'updatedAt'])).toEqual(questionToCreateDto);

      // 3: Retrieve all questions with counts
      await request(app.getHttpServer()).get('/api/quickstart/questions?includeCounts=true').expect(403);
      await request(app.getHttpServer()).get('/api/quickstart/questions?includeCounts=true').set('Cookie', userCookies).expect(403);

      const createdQuestionWithCounts: QuickstartQuestionWithCountsDto = (
        await request(app.getHttpServer())
          .get('/api/quickstart/questions?includeCounts=true')
          .set('Cookie', quickstartManagerCookies)
          .expect(200)
          .expect(res => {
            expect(res.body).toHaveLength(1);
          })
      ).body[0];

      expect(createdQuestionWithCounts).toEqual({
        ...createdQuestionWithoutCounts,
        countAssigned: 0,
      });

      // 4: Update question (new name and tags)
      const tag: QuickstartTagDto = (
        await request(app.getHttpServer())
          .post('/api/quickstart/tags')
          .send({ name: { en: 'tag name', de: 'tag name' } })
          .set('Cookie', quickstartManagerCookies)
          .expect(201)
      ).body;

      const questionToUpdateDto: CreateQuickstartQuestionDto = {
        name: {
          de: 'german question name updated',
          en: 'english question name updated',
        },
        tags: [tag.id],
      };

      await request(app.getHttpServer())
        .patch(`/api/quickstart/questions/${createdQuestionWithCounts.id}`)
        .send(questionToUpdateDto)
        .expect(401);
      await request(app.getHttpServer())
        .patch(`/api/quickstart/questions/${createdQuestionWithCounts.id}`)
        .set('Cookie', userCookies)
        .send(questionToUpdateDto)
        .expect(403);
      const updatedQuestionDto = (
        await request(app.getHttpServer())
          .patch(`/api/quickstart/questions/${createdQuestionWithCounts.id}`)
          .set('Cookie', quickstartManagerCookies)
          .send(questionToUpdateDto)
          .expect(200)
      ).body;

      expect(omit(updatedQuestionDto, ['updatedAt'])).toEqual(
        omit(
          {
            ...createdQuestionWithoutCounts,
            ...updatedQuestionDto,
          },
          ['updatedAt'],
        ),
      );

      // 5. Assign question
      const assignQuestion = async () =>
        await request(app.getHttpServer()).post(`/api/quickstart/questions/${createdQuestionWithCounts.id}/count`).expect(201);
      await Promise.all([assignQuestion(), assignQuestion(), assignQuestion()]);

      // 6. Retrieve question with counts (from list and single endpoint)
      const assignedQuestionWithCountsFromListEndpoint: QuickstartQuestionWithCountsDto = (
        await request(app.getHttpServer())
          .get(`/api/quickstart/questions?includeCounts=true`)
          .set('Cookie', quickstartManagerCookies)
          .expect(200)
          .expect(res => expect(res.body).toHaveLength(1))
      ).body[0];

      const assignedQuestionFromSingleEndpoint: QuickstartQuestionWithCountsDto = (
        await request(app.getHttpServer())
          .get(`/api/quickstart/questions/${createdQuestionWithCounts.id}`)
          .set('Cookie', userCookies)
          .expect(200)
      ).body;

      expect(assignedQuestionWithCountsFromListEndpoint.countAssigned).toBe(3);
      expect(assignedQuestionWithCountsFromListEndpoint).toEqual(assignedQuestionFromSingleEndpoint);

      // 7. Delete question
      await request(app.getHttpServer()).delete(`/api/quickstart/questions/${createdQuestionWithCounts.id}`).expect(401);
      await request(app.getHttpServer())
        .delete(`/api/quickstart/questions/${createdQuestionWithCounts.id}`)
        .set('Cookie', userCookies)
        .expect(403);
      await request(app.getHttpServer())
        .delete(`/api/quickstart/questions/${createdQuestionWithCounts.id}`)
        .set('Cookie', quickstartManagerCookies)
        .expect(200);

      await request(app.getHttpServer())
        .get(`/api/quickstart/questions?includeCounts=true`)
        .set('Cookie', quickstartManagerCookies)
        .expect(200)
        .expect(res => expect(res.body).toHaveLength(0));
      await request(app.getHttpServer())
        .get(`/api/quickstart/questions/${createdQuestionWithCounts.id}`)
        .set('Cookie', quickstartManagerCookies)
        .expect(404);
    });

    it('fails unauthenticated quickstart project creation', () => {
      return request(app.getHttpServer())
        .post('/api/quickstart/projects')
        .send({
          name: 'projectname',
          data: '',
        })
        .expect(401);
    });

    it('fails quickstart project creation without appropriate role', () => {
      return request(app.getHttpServer())
        .post('/api/quickstart/projects')
        .send({ name: 'projectname', data: '' })
        .set('Cookie', userCookies)
        .expect(403);
    });

    it('fails unauthenticated quickstart tag creation', () => {
      return request(app.getHttpServer())
        .post('/api/quickstart/tags')
        .send({ name: { en: 'projectname' } })
        .expect(401);
    });

    it('fails quickstart tag creation without appropriate role', () => {
      return request(app.getHttpServer())
        .post('/api/quickstart/tags')
        .send({ name: { en: 'projectname' } })
        .set('Cookie', userCookies)
        .expect(403);
    });
  });
});
