import { ConfigService } from '@nestjs/config';
import * as Sentry from '@sentry/nestjs';
import { type EnvironmentType } from './app/app.module';

// eslint-disable-next-line @typescript-eslint/no-var-requires
const { version } = require('../../../package.json');

const configService = new ConfigService();

Sentry.init({
  dsn: configService.get<string>('SENTRY_DSN'),
  release: version,
  environment: configService.get<EnvironmentType>('ENVIRONMENT_TYPE'),
  tracesSampleRate: 0.1,
  beforeSend: event => {
    // Make sure there are no passwords in the transmitted data. To this end, we look for the key password
    // in the request body and delete the whole body if it exists.
    const data = event.request?.data;

    if (typeof data !== 'string' || data.toLowerCase().includes('password')) {
      delete event.request.data;
    }

    return event;
  },
});
