import { Worksheet } from 'exceljs';
import { sum, truncate } from 'lodash';

export interface Column<T> {
  name: string;
  data: (data: T) => any;
}

export class TableBuilder<T> {
  private columns: Column<T>[] = [];
  private groupCount = 0;

  constructor(private ws: Worksheet) {}

  private addMainHeader(startCol: number, width: number, text: string, row = 2) {
    if (width <= 0) {
      return;
    }

    this.ws.mergeCells(row, startCol, row, startCol + width - 1);
    const cell = this.ws.getCell(row, startCol);
    cell.value = text;
    cell.font = { bold: true, size: 13, color: { argb: 'FFF8931E' } };
    cell.alignment = { horizontal: 'center', vertical: 'middle' };
    cell.fill = { type: 'pattern', pattern: 'solid', fgColor: { argb: this.groupCount % 2 === 0 ? 'FF3B4C5B' : 'FF626F7B' } };
  }

  addGroupSequence(mainHeader: string, groups: Array<[string, Column<T>[]]>) {
    this.addMainHeader(this.columns.length + 1, sum(groups.map(g => g[1].length)), mainHeader, 1);
    groups.forEach(([header, cols]) => this.addGroup(header, cols));
  }

  addGroup(header: string, cols: Column<T>[]) {
    this.addMainHeader(this.columns.length + 1, cols.length, header);
    this.addColumns(...cols);
    this.groupCount++;
  }

  addColumns(...cols: Column<T>[]) {
    this.columns.push(...cols);
  }

  buildTable(name: string, data: T[], addFilters = true) {
    return this.ws.addTable({
      name: name,
      ref: 'A3',
      headerRow: true,
      style: { showRowStripes: true },
      columns: this.columns.map(column => ({ name: sanitizeExcelString(column.name), filterButton: addFilters })),
      rows: data.map(row => this.columns.map(column => this.sanitizeValue(column.data(row)))),
    });
  }

  private sanitizeValue(value: any) {
    if (typeof value === 'string') {
      return sanitizeExcelString(value);
    }

    return value;
  }
}

export function sanitizeExcelString(str: string) {
  // According to OWASP, the following characters should be sanitized from strings
  // https://owasp.org/www-community/attacks/CSV_Injection
  while (str.length > 0 && ['=', '+', '-', '@', '\t', '\r'].includes(str.charAt(0))) {
    str = str.slice(1);
  }

  // Excel allows a maximum of 32767 characters per cell
  return truncate(str, { length: 32767 });
}
