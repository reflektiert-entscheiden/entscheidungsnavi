import { CallHandler, ConflictException, ExecutionContext, Injectable, mixin, NestInterceptor } from '@nestjs/common';
import { catchError, throwError } from 'rxjs';

/**
 * An interceptor that transforms a mongo unique constraint violation error into
 * a HTTP Conflict exception.
 *
 * @param errorMessage - Message body of the error
 */
export function UniqueConstraintViolationInterceptor(errorMessage?: string) {
  @Injectable()
  class InterceptorMixin implements NestInterceptor {
    intercept(_context: ExecutionContext, next: CallHandler<any>) {
      return next.handle().pipe(
        catchError(err => {
          if (err instanceof Error && err.name === 'MongoServerError' && (err as any).code === 11000) {
            return throwError(
              () =>
                new ConflictException({
                  statusCode: 409,
                  message: errorMessage,
                  error: 'Conflict',
                  duplicateKey: Object.keys((err as any).keyValue)[0],
                }),
            );
          }

          return throwError(() => err);
        }),
      );
    }
  }

  return mixin(InterceptorMixin);
}
