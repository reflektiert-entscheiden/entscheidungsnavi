import { Statistics } from '@entscheidungsnavi/api-types';
import { Injectable } from '@nestjs/common';
import { ProjectsService } from '../projects/projects.service';
import { UsersService } from '../users/users.service';

@Injectable()
export class StatisticsService {
  constructor(
    private usersService: UsersService,
    private projectsService: ProjectsService,
  ) {}

  async getStatistics(): Promise<Statistics> {
    return {
      projectCount: await this.projectsService.countProjects(),
      userCount: await this.usersService.countUsers(),
    };
  }
}
