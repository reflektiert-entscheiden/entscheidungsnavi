import { forwardRef, Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import PassportLocalMongoose from 'passport-local-mongoose';
import { User, UserSchema } from '../schemas/user.schema';
import { ProjectsModule } from '../projects/projects.module';
import { UsersService } from './users.service';

@Module({
  imports: [
    MongooseModule.forFeatureAsync([
      {
        name: User.name,
        useFactory: () =>
          UserSchema.plugin(PassportLocalMongoose, { usernameField: 'email', limitAttempts: true, usernameCaseInsensitive: true }),
      },
    ]),
    forwardRef(() => ProjectsModule),
  ],
  controllers: [],
  providers: [UsersService],
  exports: [UsersService, MongooseModule],
})
export class UsersModule {}
