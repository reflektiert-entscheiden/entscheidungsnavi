import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { PassportLocalDocument, PassportLocalModel, Types } from 'mongoose';
import { escapeRegExp } from 'lodash';
import { User, UserDocument } from '../schemas/user.schema';

@Injectable()
export class UsersService {
  constructor(@InjectModel(User.name) private userModel: PassportLocalModel<UserDocument & PassportLocalDocument>) {}

  async findOne(id: Types.ObjectId) {
    return await this.userModel.findById(id).exec();
  }

  async findByEmail(email: string) {
    return await this.userModel.findByUsername(email, false).exec();
  }

  async findManyByEmail(emails: string[]) {
    const emailRegexes = emails.map(email => new RegExp(`^${escapeRegExp(email)}$`, 'i'));
    return await this.userModel.find({ email: emailRegexes }).exec();
  }

  /**
   * Pass on the authenticate method from 'passport-local-mongoose'
   */
  authenticate(username: string, password: string) {
    return this.userModel.authenticate()(username, password);
  }

  async setPassword(id: Types.ObjectId, password: string) {
    // We assume that this user exists here
    const userDocument = await this.findOne(id);
    await userDocument.setPassword(password);
    await userDocument.save();
  }

  async countUsers() {
    return await this.userModel.countDocuments().exec();
  }
}
