import { Tree } from '@entscheidungsnavi/tools';
import { Hierarchy, flattenHierarchy, unflattenHierarchy } from './flattened-hierarchy';

describe('flattenHierarchy()', () => {
  it('is reversible', () => {
    const trivialHierarchy: Hierarchy = new Tree({ name: { de: 'ein name', en: 'a name' } }, []);
    expect(unflattenHierarchy(flattenHierarchy(trivialHierarchy))).toEqual(trivialHierarchy);
  });

  it('handles children', () => {
    const hierarchy: Hierarchy = new Tree({ name: { de: 'ein name', en: 'a name' } }, [
      new Tree({ name: { de: 'kind 1', en: 'child 1' } }, [
        new Tree({ name: { de: '000', en: '000' } }),
        new Tree({ name: { de: '001', en: '001' } }),
      ]),
      new Tree({ name: { de: 'kind 2', en: 'child 2' } }, [
        new Tree({ name: { de: '010', en: '010' } }),
        new Tree({ name: { de: '011', en: '011' } }),
      ]),
    ]);
    expect(unflattenHierarchy(flattenHierarchy(hierarchy))).toEqual(hierarchy);
  });
});
