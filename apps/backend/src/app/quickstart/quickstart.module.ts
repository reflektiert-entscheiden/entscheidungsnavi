import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { QuickstartProject, QuickstartProjectSchema } from '../schemas/quickstart-project.schema';
import { QuickstartTag, QuickstartTagSchema } from '../schemas/quickstart-tag.schema';
import { QuickstartValue, QuickstartValueSchema } from '../schemas/quickstart-value.schema';
import { QuickstartQuestion, QuickstartQuestionSchema } from '../schemas/quickstart-question.schema';
import { QuickstartHierarchy, QuickstartHierarchySchema } from '../schemas/quickstart-hierarchy.schema';
import { QuickstartTagsService } from './quickstart-tags.service';
import { QuickstartController } from './quickstart.controller';
import { QuickstartProjectsService } from './quickstart-projects.service';
import { QuickstartValuesService } from './quickstart-values.service';
import { QuickstartHierarchiesService } from './quickstart-hierarchies.service';
import { QuickstartQuestionsService } from './quickstart-questions.service';

@Module({
  imports: [
    MongooseModule.forFeature([
      {
        name: QuickstartProject.name,
        schema: QuickstartProjectSchema,
      },
      {
        name: QuickstartTag.name,
        schema: QuickstartTagSchema,
      },
      {
        name: QuickstartValue.name,
        schema: QuickstartValueSchema,
      },
      {
        name: QuickstartQuestion.name,
        schema: QuickstartQuestionSchema,
      },
      {
        name: QuickstartHierarchy.name,
        schema: QuickstartHierarchySchema,
      },
    ]),
  ],
  controllers: [QuickstartController],
  providers: [
    QuickstartProjectsService,
    QuickstartTagsService,
    QuickstartValuesService,
    QuickstartQuestionsService,
    QuickstartHierarchiesService,
  ],
})
export class QuickstartModule {}
