import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model, Types } from 'mongoose';
import { plainToClass } from 'class-transformer';
import { omit } from 'lodash';
import {
  GroupedQuickstartObjectiveSortBy,
  QuickstartHierarchyFilter,
  QuickstartHierarchySortBy,
  QuickstartObjectiveFilter,
  QuickstartObjectiveGroupBy,
  QuickstartObjectiveSortBy,
  SortDirection,
} from '@entscheidungsnavi/api-types';
import { QuickstartHierarchy, QuickstartHierarchyDocument, QuickstartHierarchyNode } from '../schemas/quickstart-hierarchy.schema';
import { QuickstartHierarchyListDto } from './dto/quickstart-hierarchy.dto';
import { flattenHierarchy, unflattenHierarchy } from './flattened-hierarchy';
import { CreateQuickstartHierarchyDto } from './dto/create-quickstart-hierarchy.dto';
import { UpdateQuickstartHierarchyDto } from './dto/update-quickstart-hierarchy.dto';
import { QuickstartObjectiveListDto } from './dto/quickstart-objective.dto';
import { QuickstartHierarchyListWithStatsDto, QuickstartHierarchyWithStatsDto } from './dto/quickstart-hierarchy-stats.dto';
import { GroupedQuickstartObjectiveListDto, GroupedQuickstartObjectiveListWithStatsDto } from './dto/quickstart-objective-grouped.dto';
import { QuickstartObjectiveListWithStatsDto } from './dto/quickstart-objective-stats.dto';

function toDto(doc: QuickstartHierarchyDocument) {
  return doc
    ? plainToClass(QuickstartHierarchyWithStatsDto, {
        ...doc.toObject({ getters: true }),
        tree: unflattenHierarchy(doc.tree),
        totalAccumulatedScore: doc.tree.reduce((acc, node) => acc + node.accumulatedScore, 0),
      })
    : null;
}

@Injectable()
export class QuickstartHierarchiesService {
  constructor(@InjectModel(QuickstartHierarchy.name) private hierarchyModel: Model<QuickstartHierarchy>) {}

  async create(hierarchy: CreateQuickstartHierarchyDto) {
    return toDto(
      await this.hierarchyModel.create({
        ...hierarchy,
        tree: flattenHierarchy(hierarchy.tree),
      }),
    );
  }

  async update(id: Types.ObjectId, hierarchy: UpdateQuickstartHierarchyDto) {
    const doc = await this.hierarchyModel.findById(id);
    if (doc == null) throw new NotFoundException();

    if (hierarchy.name) {
      doc.name = hierarchy.name;
    }

    if (hierarchy.tags) {
      doc.set(
        'tags',
        hierarchy.tags.map(s => new Types.ObjectId(s)),
      );
    }

    if (hierarchy.tree) {
      // Take the tree structure and the overwritten properties from the update query
      // and backfill them with the data from the database.
      const entriesById: Record<string, QuickstartHierarchyNode> = {};
      doc.tree.forEach(entry => (entriesById[entry.id] = entry));

      const flattenedHierarchy = flattenHierarchy(hierarchy.tree).map(entry => {
        const updateEntry = entry.id && entry.id in entriesById ? { ...entriesById[entry.id], ...entry } : entry;
        return omit(updateEntry, 'id'); // Drop 'id' and only keep '_id'
      });

      doc.set('tree', flattenedHierarchy);
    }

    await doc.save();

    return toDto(doc);
  }

  async getHierarchy(id: Types.ObjectId) {
    return toDto(await this.hierarchyModel.findById(id));
  }

  async delete(id: Types.ObjectId) {
    const result = await this.hierarchyModel.findByIdAndDelete(id);
    if (result == null) throw new NotFoundException();
  }

  async incrementTrackingCounter(objectiveIds: Types.ObjectId[]) {
    // We split the increment across all respective objectives
    const result = await this.hierarchyModel
      .updateMany(
        { 'tree._id': { $in: objectiveIds } },
        { $inc: { 'tree.$[element].accumulatedScore': 1 / objectiveIds.length } },
        { arrayFilters: [{ 'element._id': { $in: objectiveIds } }] },
      )
      .exec();

    if (result.modifiedCount === 0) throw new NotFoundException();
  }

  async listHierarchies(
    filter: QuickstartHierarchyFilter,
    sort: { by: QuickstartHierarchySortBy; direction: SortDirection },
    pagination: { limit: number; offset: number },
    withStats: boolean,
  ) {
    const query = this.hierarchyModel.aggregate().collation({ locale: 'en' });

    if (filter.tags) {
      query.match({ tags: { $all: filter.tags.map(tag => new Types.ObjectId(tag)) } });
    }

    query.match({
      // We assume that filter.query is already escaped for use in a RegEx
      $or: [
        {
          'name.de': { $regex: filter.nameQuery ?? '', $options: 'i' },
        },
        {
          'name.en': { $regex: filter.nameQuery ?? '', $options: 'i' },
        },
      ],
    });

    const sortMap: { [key in QuickstartHierarchySortBy]: string } = {
      nameDe: 'name.de',
      nameEn: 'name.en',
      createdAt: 'createdAt',
      totalAccumulatedScore: 'totalAccumulatedScore',
    };
    const sortDirection = sort.direction === 'asc' ? 1 : -1;

    const [result] = await query
      .facet({
        items: [
          {
            $addFields: {
              totalAccumulatedScore: { $sum: '$tree.accumulatedScore' },
            },
          },
          {
            $sort: { [sortMap[sort.by]]: sortDirection },
          },
          {
            $skip: pagination.offset,
          },
          {
            $limit: pagination.limit,
          },
        ],
        metadata: [{ $count: 'count' }],
        tags: [{ $unwind: '$tags' }, { $group: { _id: null, tags: { $addToSet: '$tags' } } }],
      })
      .exec();

    return plainToClass(withStats ? QuickstartHierarchyListWithStatsDto : QuickstartHierarchyListDto, {
      items: result.items.map((item: QuickstartHierarchy) => {
        const hydratedDoc = this.hierarchyModel.hydrate(item);

        return {
          ...hydratedDoc.toObject({ getters: true }),
          tree: unflattenHierarchy(hydratedDoc.tree),
        };
      }),
      count: result.items.length > 0 ? result.metadata[0].count : 0,
      activeTags: result.tags[0]?.tags ?? [],
    });
  }

  async listObjectives(
    filter: QuickstartObjectiveFilter,
    sort: { by: QuickstartObjectiveSortBy; direction: SortDirection },
    pagination: { limit: number; offset: number },
    withStats: boolean,
  ) {
    const query = this.hierarchyModel.aggregate().collation({ locale: 'en' });

    if (filter.tags) {
      query.match({ tags: { $all: filter.tags.map(tag => new Types.ObjectId(tag)) } });
    }

    query.unwind('$tree');

    if (filter.query) {
      query.match({
        // We assume that filter.query is already escaped for use in a RegEx
        $or: [
          {
            'tree.name.de': { $regex: filter.query ?? '', $options: 'i' },
          },
          {
            'tree.name.en': { $regex: filter.query ?? '', $options: 'i' },
          },
        ],
      });
    }

    switch (filter.level) {
      case 'only-fundamental':
        query.match({ 'tree.level': 1 });
        break;
      case 'only-aspects':
        query.match({ 'tree.level': { $gt: 1 } });
        break;
      default:
        query.match({ 'tree.level': { $gt: 0 } });
        break;
    }

    const sortMap: { [key in QuickstartObjectiveSortBy]: string } = {
      nameDe: 'tree.name.de',
      nameEn: 'tree.name.en',
      accumulatedScore: 'tree.accumulatedScore',
      level: 'tree.level',
    };
    const sortDirection = sort.direction === 'asc' ? 1 : -1;

    const [result] = await query
      .facet({
        items: [
          { $sort: { [sortMap[sort.by]]: sortDirection } },
          {
            $project: {
              id: '$tree._id',
              hierarchyId: '$_id',
              hierarchyName: '$name',
              name: '$tree.name',
              comment: '$tree.comment',
              scaleComment: '$tree.scaleComment',
              level: '$tree.level',
              tags: '$tags',
              accumulatedScore: '$tree.accumulatedScore',
            },
          },
          {
            $skip: pagination.offset,
          },
          {
            $limit: pagination.limit,
          },
        ],
        metadata: [{ $count: 'count' }],
        tags: [{ $unwind: '$tags' }, { $group: { _id: null, tags: { $addToSet: '$tags' } } }],
      })
      .exec();

    return plainToClass(withStats ? QuickstartObjectiveListWithStatsDto : QuickstartObjectiveListDto, {
      items: result.items,
      count: result.items.length > 0 ? result.metadata[0].count : 0,
      activeTags: result.tags[0]?.tags ?? [],
    });
  }

  async listGroupedObjectives(
    filter: QuickstartObjectiveFilter,
    sort: { by: GroupedQuickstartObjectiveSortBy; direction: SortDirection },
    pagination: { limit: number; offset: number },
    withStats: boolean,
    groupBy: QuickstartObjectiveGroupBy,
  ) {
    const query = this.hierarchyModel.aggregate().collation({ locale: 'en' });

    if (filter.tags) {
      query.match({ tags: { $all: filter.tags.map(tag => new Types.ObjectId(tag)) } });
    }

    query.unwind('$tree');

    if (filter.query) {
      // We assume that filter.query is already escaped for use in a RegEx
      query.match(
        groupBy === 'nameDe'
          ? { 'tree.name.de': { $regex: filter.query ?? '', $options: 'i' } }
          : { 'tree.name.en': { $regex: filter.query ?? '', $options: 'i' } },
      );
    }

    switch (filter.level) {
      case 'only-fundamental':
        query.match({ 'tree.level': 1 });
        break;
      case 'only-aspects':
        query.match({ 'tree.level': { $gt: 1 } });
        break;
      default:
        query.match({ 'tree.level': { $gt: 0 } });
        break;
    }

    query
      .project({
        id: '$tree._id',
        hierarchyId: '$_id',
        hierarchyName: '$name',
        name: '$tree.name',
        comment: '$tree.comment',
        scaleComment: '$tree.scaleComment',
        level: '$tree.level',
        tags: '$tags',
        accumulatedScore: '$tree.accumulatedScore',
      })
      .group({
        _id: groupBy === 'nameDe' ? '$name.de' : '$name.en',
        items: { $push: '$$ROOT' },
        itemCount: { $count: {} },
        accumulatedScore: { $sum: '$accumulatedScore' },
      });

    const sortMap: { [key in GroupedQuickstartObjectiveSortBy]: string } = {
      name: '_id',
      itemCount: 'itemCount',
      accumulatedScore: 'accumulatedScore',
    };
    const sortDirection = sort.direction === 'asc' ? 1 : -1;

    const [result] = await query
      .facet({
        items: [
          { $sort: { [sortMap[sort.by]]: sortDirection } },
          {
            $skip: pagination.offset,
          },
          {
            $limit: pagination.limit,
          },
        ],
        metadata: [{ $count: 'count' }],
        tags: [{ $unwind: '$items' }, { $unwind: '$items.tags' }, { $group: { _id: null, tags: { $addToSet: '$items.tags' } } }],
      })
      .exec();

    return plainToClass(withStats ? GroupedQuickstartObjectiveListWithStatsDto : GroupedQuickstartObjectiveListDto, {
      items: result.items,
      count: result.items.length > 0 ? result.metadata[0].count : 0,
      activeTags: result.tags[0]?.tags ?? [],
    });
  }
}
