import { PartialType } from '@nestjs/mapped-types';
import { CreateQuickstartTagDto } from './create-quickstart-tag.dto';

export class UpdateQuickstartTagDto extends PartialType(CreateQuickstartTagDto) {}
