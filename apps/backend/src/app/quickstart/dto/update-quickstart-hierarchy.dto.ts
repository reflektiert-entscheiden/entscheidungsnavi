import { PartialType, PickType } from '@nestjs/mapped-types';
import { Type } from 'class-transformer';
import { IsOptional, IsMongoId, IsDefined, ValidateNested } from 'class-validator';
import { LocalizedStringDto } from '../../common/localized-string.dto';
import { QuickstartHierarchyDto } from './quickstart-hierarchy.dto';

class UpdateHierarchyTreeValueDto {
  @IsOptional()
  @IsMongoId()
  @Type(() => String)
  id?: string;

  @IsOptional()
  @ValidateNested()
  @Type(() => LocalizedStringDto)
  name?: LocalizedStringDto;

  @IsDefined()
  @ValidateNested()
  @Type(() => LocalizedStringDto)
  comment?: LocalizedStringDto;

  @IsDefined()
  @ValidateNested()
  @Type(() => LocalizedStringDto)
  scaleComment?: LocalizedStringDto;
}

export class UpdateQuickstartHierarchyTreeDto {
  @IsDefined()
  @ValidateNested()
  @Type(() => UpdateHierarchyTreeValueDto)
  value: UpdateHierarchyTreeValueDto;

  @IsDefined()
  @ValidateNested({ each: true })
  @Type(() => UpdateQuickstartHierarchyTreeDto)
  children: UpdateQuickstartHierarchyTreeDto[];
}

export class UpdateQuickstartHierarchyDto extends PickType(PartialType(QuickstartHierarchyDto), ['name', 'tags']) {
  @IsOptional()
  @ValidateNested()
  @Type(() => UpdateQuickstartHierarchyTreeDto)
  tree?: UpdateQuickstartHierarchyTreeDto;
}
