import { QuickstartQuestion, QuickstartQuestionWithStats } from '@entscheidungsnavi/api-types';
import { Exclude, Expose, Type } from 'class-transformer';
import { IsDefined, IsMongoId, ValidateNested } from 'class-validator';
import { PartialType, PickType } from '@nestjs/mapped-types';
import { LocalizedStringDto } from '../../common/localized-string.dto';

@Exclude()
export class QuickstartQuestionDto implements QuickstartQuestion {
  @Expose()
  @Type(() => String)
  id: string;

  @Expose()
  @IsDefined()
  @ValidateNested()
  @Type(() => LocalizedStringDto)
  name: LocalizedStringDto;

  @Expose()
  @IsDefined()
  @Type(() => String)
  @IsMongoId({ each: true })
  tags: string[];

  @Expose()
  createdAt: Date;

  @Expose()
  updatedAt: Date;
}

@Exclude()
export class QuickstartQuestionWithStatsDto extends QuickstartQuestionDto implements QuickstartQuestionWithStats {
  @Expose()
  countAssigned: number;
}

export class CreateQuickstartQuestionDto extends PickType(QuickstartQuestionDto, ['name', 'tags']) {}

export class UpdateQuickstartQuestionDto extends PartialType(CreateQuickstartQuestionDto) {}
