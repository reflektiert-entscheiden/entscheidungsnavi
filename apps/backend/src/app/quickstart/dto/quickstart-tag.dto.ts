import { Exclude, Expose, Type } from 'class-transformer';
import { IsDefined, IsInt, IsOptional, ValidateNested } from 'class-validator';
import { QuickstartTag } from '@entscheidungsnavi/api-types';
import { LocalizedStringDto } from '../../common/localized-string.dto';

@Exclude()
export class QuickstartTagDto implements QuickstartTag {
  @Expose()
  @Type(() => String)
  id: string;

  @IsDefined()
  @ValidateNested()
  @Type(() => LocalizedStringDto)
  @Expose()
  name: LocalizedStringDto;

  @IsOptional()
  @IsInt()
  @Expose()
  weight?: number;

  @Expose()
  createdAt: Date;

  @Expose()
  updatedAt: Date;
}
