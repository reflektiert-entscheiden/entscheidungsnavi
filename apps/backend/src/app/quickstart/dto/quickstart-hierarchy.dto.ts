import { Exclude, Expose, Type } from 'class-transformer';
import { IsDefined, IsEnum, IsMongoId, IsOptional, ValidateNested } from 'class-validator';
import {
  QUICKSTART_HIERARCHY_SORT_BY,
  QuickstartHierarchy,
  QuickstartHierarchyList,
  QuickstartHierarchySort,
  QuickstartHierarchySortBy,
  SORT_DIRECTIONS,
  SortDirection,
} from '@entscheidungsnavi/api-types';
import { LocalizedStringDto } from '../../common/localized-string.dto';

@Exclude()
export class QuickstartHierarchyTreeValueDto {
  @Type(() => String)
  @Expose()
  id: string;

  @Type(() => LocalizedStringDto)
  @Expose()
  name: LocalizedStringDto;

  @IsDefined()
  @ValidateNested()
  @Type(() => LocalizedStringDto)
  @Expose()
  comment: LocalizedStringDto;

  @IsDefined()
  @ValidateNested()
  @Type(() => LocalizedStringDto)
  @Expose()
  scaleComment: LocalizedStringDto;
}

@Exclude()
export class QuickstartHierarchyTreeDto {
  @Type(() => QuickstartHierarchyTreeValueDto)
  @Expose()
  value: QuickstartHierarchyTreeValueDto;

  @Type(() => QuickstartHierarchyTreeDto)
  @Expose()
  children: QuickstartHierarchyTreeDto[];
}

@Exclude()
export class QuickstartHierarchyDto implements QuickstartHierarchy {
  @Type(() => String)
  @Expose()
  id: string;

  @IsDefined()
  @ValidateNested()
  @Type(() => LocalizedStringDto)
  @Expose()
  name: LocalizedStringDto;

  @Expose()
  @Type(() => String)
  @IsMongoId({ each: true })
  tags: string[];

  @Expose()
  @Type(() => QuickstartHierarchyTreeDto)
  tree: QuickstartHierarchyTreeDto;

  @Expose()
  createdAt: Date;

  @Expose()
  updatedAt: Date;
}

@Exclude()
export class QuickstartHierarchyListDto implements QuickstartHierarchyList {
  @Expose()
  @Type(() => QuickstartHierarchyDto)
  items: QuickstartHierarchyDto[];

  @Expose()
  @Type(() => String)
  activeTags: string[];

  @Expose()
  count: number;
}

export class QuickstartHierarchySortDto implements Partial<QuickstartHierarchySort> {
  @IsOptional()
  @IsEnum(QUICKSTART_HIERARCHY_SORT_BY)
  sortBy?: QuickstartHierarchySortBy;

  @IsOptional()
  @IsEnum(SORT_DIRECTIONS)
  sortDirection?: SortDirection;
}
