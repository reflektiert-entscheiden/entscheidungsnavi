import { QuickstartValueTracking } from '@entscheidungsnavi/api-types';
import { Type } from 'class-transformer';
import { IsDefined, IsMongoId, IsNumber, Max, Min, ValidateNested } from 'class-validator';

class TrackingEntryDto {
  @IsMongoId()
  id: string;

  @IsNumber()
  @Min(0)
  @Max(1)
  score: number;
}

export class QuickstartValueTrackingDto implements QuickstartValueTracking {
  @IsDefined()
  @Type(() => TrackingEntryDto)
  @ValidateNested({ each: true })
  values: TrackingEntryDto[];
}
