import { Type } from 'class-transformer';
import { IsMongoId } from 'class-validator';
import { Types } from 'mongoose';

export class TrackQuickstartObjectivesDto {
  @Type(() => Types.ObjectId)
  @IsMongoId({ each: true })
  objectiveIds: Types.ObjectId[];
}
