import { QuickstartProject } from '@entscheidungsnavi/api-types';
import { IsNotMongoId } from '@entscheidungsnavi/tools';
import { Exclude, Expose, Type } from 'class-transformer';
import { IsBoolean, IsDefined, IsMongoId, IsNotEmpty, IsOptional, IsString, Validate } from 'class-validator';

@Exclude()
export class QuickstartProjectDto implements QuickstartProject {
  @Expose()
  @Type(() => String)
  id: string;

  @IsNotEmpty()
  @IsString()
  @Expose()
  name: string;

  @IsDefined()
  @Expose()
  data: any;

  @IsDefined()
  @Expose()
  @IsBoolean()
  visible = false;

  @IsOptional()
  @Expose()
  @Type(() => String)
  @IsMongoId({ each: true })
  tags: string[];

  @IsOptional()
  @Validate(IsNotMongoId)
  @IsNotEmpty()
  @Expose()
  shareToken?: string;

  @Expose()
  shareUrl: string;

  @Expose()
  createdAt: Date;

  @Expose()
  updatedAt: Date;
}
