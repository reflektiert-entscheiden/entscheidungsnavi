import { PickType } from '@nestjs/mapped-types';
import { Type } from 'class-transformer';
import { IsDefined, ValidateNested } from 'class-validator';
import { LocalizedStringDto } from '../../common/localized-string.dto';
import { QuickstartHierarchyDto } from './quickstart-hierarchy.dto';

class CreateHierarchyTreeValueDto {
  @IsDefined()
  @ValidateNested()
  @Type(() => LocalizedStringDto)
  name: LocalizedStringDto;

  @IsDefined()
  @ValidateNested()
  @Type(() => LocalizedStringDto)
  comment: LocalizedStringDto;

  @IsDefined()
  @ValidateNested()
  @Type(() => LocalizedStringDto)
  scaleComment: LocalizedStringDto;
}

class CreateHierarchyTreeDto {
  @IsDefined()
  @ValidateNested()
  @Type(() => CreateHierarchyTreeValueDto)
  value: CreateHierarchyTreeValueDto;

  @IsDefined()
  @ValidateNested({ each: true })
  @Type(() => CreateHierarchyTreeDto)
  children: CreateHierarchyTreeDto[];
}

export class CreateQuickstartHierarchyDto extends PickType(QuickstartHierarchyDto, ['name', 'tags']) {
  @IsDefined()
  @ValidateNested()
  @Type(() => CreateHierarchyTreeDto)
  tree: CreateHierarchyTreeDto;
}
