import { ClassConstructor, plainToClass } from 'class-transformer';
import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model, Types, UpdateQuery } from 'mongoose';
import { QuickstartValue, QuickstartValueDocument } from '../schemas/quickstart-value.schema';
import {
  CreateQuickstartValueDto,
  QuickstartValueDto,
  QuickstartValueWithStatsDto,
  UpdateQuickstartValueDto,
} from './dto/quickstart-value.dto';
import { QuickstartValueTrackingDto } from './dto/quickstart-value-tracking.dto';

function toDto(
  doc: QuickstartValueDocument,
  target: ClassConstructor<QuickstartValueDto> | ClassConstructor<QuickstartValueWithStatsDto> = QuickstartValueWithStatsDto,
) {
  return plainToClass(target, doc);
}

@Injectable()
export class QuickstartValuesService {
  constructor(@InjectModel(QuickstartValue.name) private quickstartValueModel: Model<QuickstartValueDocument>) {}

  async create(tag: CreateQuickstartValueDto) {
    return toDto(await this.quickstartValueModel.create(tag));
  }

  async list(withStats: boolean) {
    const docs = await this.quickstartValueModel.aggregate([
      // Compute an average weight and sort by it
      {
        $set: {
          averageScore: {
            $cond: {
              if: { $ne: ['$countTracked', 0] },
              then: { $divide: ['$accumulatedScore', '$countTracked'] },
              else: 0,
            },
          },
          id: {
            $toString: '$_id',
          },
        },
      },
      {
        $sort: { averageScore: -1 },
      },
    ]);
    return docs.map(doc => toDto(doc, withStats ? QuickstartValueWithStatsDto : QuickstartValueDto));
  }

  async findById(id: Types.ObjectId) {
    return toDto(await this.quickstartValueModel.findById(id).exec());
  }

  async collectMetrics(update: QuickstartValueTrackingDto) {
    // Sort the entries by score descending. Highest score first.
    update.values.sort((a, b) => (b.score ?? 0) - (a.score ?? 0));

    // Update every entry in this list
    const promises = update.values.map((value, valueIndex) => {
      const update: UpdateQuery<QuickstartValueDocument> = {
        $inc: { countTracked: 1 },
      };
      if (value.score > 0) {
        update.$inc.countAssigned = 1;
        update.$inc.accumulatedScore = value.score;

        if (valueIndex < 5) {
          update.$inc.countInTop5 = 1;
        }
      }
      return this.quickstartValueModel.updateOne({ _id: new Types.ObjectId(value.id) }, update);
    });

    await Promise.all(promises);
  }

  async update(id: Types.ObjectId, tag: UpdateQuickstartValueDto) {
    const result = await this.quickstartValueModel.findByIdAndUpdate(id, tag, { new: true }).exec();
    return toDto(result);
  }

  async remove(id: Types.ObjectId) {
    const result = await this.quickstartValueModel.deleteOne({ _id: id }).exec();
    if (result.deletedCount === 0) {
      throw new NotFoundException();
    }
  }
}
