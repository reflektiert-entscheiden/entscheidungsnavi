import { QuickstartHierarchyNode as ApiQuickstartHierarchyNode } from '@entscheidungsnavi/api-types';
import { ITree } from '@entscheidungsnavi/tools';
import { omit } from 'lodash';
import { QuickstartHierarchyNode } from '../schemas/quickstart-hierarchy.schema';

export type Hierarchy = ITree<Partial<ApiQuickstartHierarchyNode>>;

/**
 * A flattened hierarchy is the hierarchy tree serialized into a list. This removes the deep nesting.
 *
 * @example
 * A tree is flattened as follows. Original tree:
 * ```
 * {
 *   value: <v1>,
 *   children: [
 *      { value: <v2>, children: [
 *        { value: <v4>, children: [] }
 *      ]},
 *      { value: <v3>, children: []}
 *   ]
 * }
 * ```
 * Flattened tree:
 * ```
 * [
 *   { ...<v1>, level: 0 },
 *   { ...<v2>, level: 1 },
 *   { ...<v4>, level: 2 },
 *   { ...<v3>, level: 1 }
 * ]
 * ```
 */
export type FlattenedHierarchy = Array<Partial<QuickstartHierarchyNode>>;

/**
 * Turns a tree into a flattened tree.
 *
 * @param tree - The tree to flatten
 * @returns The flattened tree
 */
export function flattenHierarchy(tree: Hierarchy): FlattenedHierarchy {
  const flattenNode = (node: Hierarchy, level: number): FlattenedHierarchy => {
    const children = node.children.flatMap(child => flattenNode(child, level + 1)) ?? [];
    return [{ ...node.value, level }, ...children];
  };

  return flattenNode(tree, 0);
}

/**
 * Turns a flattened tree back into a normal tree.
 *
 * @param tree - The flattened tree
 * @returns The unflattened tree
 */
export function unflattenHierarchy(tree: FlattenedHierarchy): Hierarchy {
  const parseNode = (node: Partial<QuickstartHierarchyNode>, children: FlattenedHierarchy): Hierarchy => {
    const tree: ITree<Partial<ApiQuickstartHierarchyNode>> = { value: omit(node, 'level'), children: [] };

    // Find the direct children, i.e. the nodes with level === node.level + 1.
    // The first child is ALWAYS a direct child by definition.
    let currentChildIndex = 0;

    while (currentChildIndex < children.length) {
      let nextChildIndex = currentChildIndex + 1;

      // Find the next direct child
      while (nextChildIndex < children.length && children[nextChildIndex].level !== node.level + 1) {
        nextChildIndex++;
      }

      tree.children.push(parseNode(children[currentChildIndex], children.slice(currentChildIndex + 1, nextChildIndex)));
      currentChildIndex = nextChildIndex;
    }

    return tree;
  };

  return parseNode(tree[0], tree.slice(1));
}
