import { ClassConstructor, plainToClass } from 'class-transformer';
import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model, Types, UpdateQuery } from 'mongoose';
import { QuickstartQuestion, QuickstartQuestionDocument } from '../schemas/quickstart-question.schema';
import {
  CreateQuickstartQuestionDto,
  QuickstartQuestionDto,
  QuickstartQuestionWithStatsDto,
  UpdateQuickstartQuestionDto,
} from './dto/quickstart-question.dto';

function toDto(
  doc: QuickstartQuestionDocument,
  target: ClassConstructor<QuickstartQuestionDto> | ClassConstructor<QuickstartQuestionWithStatsDto> = QuickstartQuestionWithStatsDto,
) {
  return plainToClass(target, doc);
}

@Injectable()
export class QuickstartQuestionsService {
  constructor(@InjectModel(QuickstartQuestion.name) private quickstartQuestionModel: Model<QuickstartQuestionDocument>) {}

  async create(question: CreateQuickstartQuestionDto) {
    return toDto(await this.quickstartQuestionModel.create(question));
  }

  async list(withCounts: boolean) {
    const docs = await this.quickstartQuestionModel.find().sort({ countAssigned: -1 });
    return docs.map(doc => toDto(doc, withCounts ? QuickstartQuestionWithStatsDto : QuickstartQuestionDto));
  }

  async findById(id: Types.ObjectId) {
    return toDto(await this.quickstartQuestionModel.findById(id).exec());
  }

  async handleQuestionAssignment(id: Types.ObjectId) {
    const incrementQuestionAssignmentCountUpdate: UpdateQuery<QuickstartQuestionDocument> = {
      $inc: { countAssigned: 1 },
    };

    return this.quickstartQuestionModel.updateOne({ _id: new Types.ObjectId(id) }, incrementQuestionAssignmentCountUpdate);
  }

  async update(id: Types.ObjectId, question: UpdateQuickstartQuestionDto) {
    const result = await this.quickstartQuestionModel.findByIdAndUpdate(id, question, { new: true }).exec();
    return toDto(result);
  }

  async removeTagReferences(tagId: Types.ObjectId) {
    await this.quickstartQuestionModel.updateMany({}, { $pull: { tags: tagId } }).exec();
  }

  async remove(id: Types.ObjectId) {
    const result = await this.quickstartQuestionModel.deleteOne({ _id: id }).exec();
    if (result.deletedCount === 0) {
      throw new NotFoundException();
    }
  }
}
