import { NestExpressApplication } from '@nestjs/platform-express';
import { Test } from '@nestjs/testing';
import { MongoMemoryReplSet } from 'mongodb-memory-server';
import mongoose, { Types } from 'mongoose';
import { MongooseModule } from '@nestjs/mongoose';
import { Tree } from '@entscheidungsnavi/tools';
import { NotFoundException } from '@nestjs/common';
import {
  QuickstartHierarchy,
  QuickstartHierarchyNode,
  QuickstartHierarchyNodeSchema,
  QuickstartHierarchySchema,
} from '../schemas/quickstart-hierarchy.schema';
import { QuickstartHierarchiesService } from './quickstart-hierarchies.service';
import { UpdateQuickstartHierarchyTreeDto } from './dto/update-quickstart-hierarchy.dto';

jest.setTimeout(30000);

describe('QuickstartHierarchiesService', () => {
  let app: NestExpressApplication;
  let mongod: MongoMemoryReplSet;
  let service: QuickstartHierarchiesService;

  beforeAll(async () => {
    mongod = await MongoMemoryReplSet.create({ replSet: { name: 'ra0' } });
    await mongod.waitUntilRunning();

    const uri = await mongod.getUri('navi');

    await mongoose.connect(uri);

    const module = await Test.createTestingModule({
      imports: [
        MongooseModule.forRoot(uri),
        MongooseModule.forFeature([
          {
            name: QuickstartHierarchy.name,
            schema: QuickstartHierarchySchema,
          },
          {
            name: QuickstartHierarchyNode.name,
            schema: QuickstartHierarchyNodeSchema,
          },
        ]),
      ],
      providers: [QuickstartHierarchiesService],
    }).compile();

    app = module.createNestApplication<NestExpressApplication>();
    service = app.get(QuickstartHierarchiesService);

    await app.init();
  });

  afterAll(async () => {
    await app.close();
    await mongoose.disconnect();
    await mongod.stop();
  });

  const mockHierarchy = {
    _id: new Types.ObjectId('64cbaff1d364150ba5ba4823'),
    name: {
      en: 'Hierarchy',
      de: 'Hierarchie',
    },
    tags: [] as string[],
    tree: [
      {
        name: {
          en: 'Wurzel',
          de: 'Root',
        },
        comment: {
          en: '{"ops":[]}',
          de: '{"ops":[]}',
        },
        scaleComment: {
          en: '{"ops":[]}',
          de: '{"ops":[]}',
        },
        level: 0,
        accumulatedScore: 1,
        _id: new Types.ObjectId('64cbaff1d364150ba5ba4824'),
      },
      {
        name: {
          en: 'Child 1',
          de: 'Kind 1',
        },
        comment: {
          en: '{"ops":[]}',
          de: '{"ops":[]}',
        },
        scaleComment: {
          en: '{"ops":[]}',
          de: '{"ops":[]}',
        },
        level: 1,
        accumulatedScore: 2,
        _id: new Types.ObjectId('64cbaff1d364150ba5ba4825'),
      },
      {
        name: {
          en: 'Aspect 1',
          de: 'Aspekt 1',
        },
        comment: {
          en: '{"ops":[]}',
          de: '{"ops":[]}',
        },
        scaleComment: {
          en: '{"ops":[]}',
          de: '{"ops":[]}',
        },
        level: 2,
        accumulatedScore: 3,
        _id: new Types.ObjectId('64cbaff1d364150ba5ba4826'),
      },
    ],
    createdAt: new Date('2023-08-03T13:47:29.030Z'),
    updatedAt: new Date('2023-08-03T14:39:04.595Z'),
    __v: 0,
  };

  beforeEach(async () => {
    await mongoose.connection.db.collection('quickstarthierarchies').deleteMany({});
    await mongoose.connection.db.collection('quickstarthierarchies').insertOne(mockHierarchy);
  });

  describe('create()', () => {
    it('creates a hierarchy', async () => {
      const result = await service.create({
        name: { de: 'Neue Hierarchie', en: 'New hierarchy' },
        tags: [],
        tree: new Tree({
          name: { de: 'Wurzel', en: 'Root' },
          comment: { de: 'Leer', en: 'empty' },
          scaleComment: { de: 'Leer', en: 'empty' },
        }),
      });

      expect(result.id).toBeDefined();
      expect(result.tree).toMatchObject({
        value: { name: { de: 'Wurzel', en: 'Root' }, comment: { de: 'Leer', en: 'empty' }, accumulatedScore: 0 },
        children: [],
      });
      expect(result.totalAccumulatedScore).toBe(0);

      expect(await mongoose.connection.db.collection('quickstarthierarchies').findOne({ _id: new Types.ObjectId(result.id) })).toBeTruthy();
    });
  });

  describe('update()', () => {
    it('updates name and tags', async () => {
      const result = await service.update(new Types.ObjectId('64cbaff1d364150ba5ba4823'), {
        name: { de: 'Neuer Name', en: 'New name' },
        tags: ['62b1cad7c004d74ba1f68109'],
      });
      expect(result.name).toEqual({ de: 'Neuer Name', en: 'New name' });

      const document = await mongoose.connection.db
        .collection('quickstarthierarchies')
        .findOne({ _id: new Types.ObjectId('64cbaff1d364150ba5ba4823') });

      expect(document.name).toEqual({ de: 'Neuer Name', en: 'New name' });
      expect(document.tags).toEqual([new Types.ObjectId('62b1cad7c004d74ba1f68109')]);
      expect(document.tree).toEqual(mockHierarchy.tree);
    });

    it('allows replacing the tree', async () => {
      const tree: UpdateQuickstartHierarchyTreeDto = {
        value: {
          name: { de: 'Neue Wurzel', en: 'New root' },
          comment: { de: 'Kommentar', en: 'Comment' },
          scaleComment: { de: 'Kommentar', en: 'Comment' },
        },
        children: [],
      };

      const result = await service.update(new Types.ObjectId('64cbaff1d364150ba5ba4823'), { tree });

      expect(result.tree).toMatchObject(tree);
      // The tree has been replaced with a new one
      expect(result.tree.value.id).not.toBe(mockHierarchy.tree[0]._id.toString());
      expect(result.tree.value.accumulatedScore).toBe(0);
      expect(result.tree.children).toHaveLength(0);

      const document = await mongoose.connection.db
        .collection('quickstarthierarchies')
        .findOne({ _id: new Types.ObjectId('64cbaff1d364150ba5ba4823') });

      expect(document.tree).toMatchObject([
        {
          name: { de: 'Neue Wurzel', en: 'New root' },
          comment: { de: 'Kommentar', en: 'Comment' },
          scaleComment: { de: 'Kommentar', en: 'Comment' },
        },
      ]);
    });

    it('allows updating the tree', async () => {
      const treeUpdate: UpdateQuickstartHierarchyTreeDto = {
        value: { id: '64cbaff1d364150ba5ba4824', name: { de: 'Alte Wurzel', en: 'Old root' } },
        children: [],
      };

      const result = await service.update(new Types.ObjectId('64cbaff1d364150ba5ba4823'), { tree: treeUpdate });
      expect(result.tree).toMatchObject(treeUpdate);

      const document = await mongoose.connection.db
        .collection('quickstarthierarchies')
        .findOne({ _id: new Types.ObjectId('64cbaff1d364150ba5ba4823') });

      expect(document.tree).toHaveLength(1);
      expect(document.tree[0]).toMatchObject({
        name: { de: 'Alte Wurzel', en: 'Old root' },
        comment: {
          en: '{"ops":[]}',
          de: '{"ops":[]}',
        },
        level: 0,
        accumulatedScore: 1,
        _id: new Types.ObjectId('64cbaff1d364150ba5ba4824'),
      });
    });

    it('throws on unknown id', async () => {
      await expect(service.update(new Types.ObjectId('64cbaff1d364150ba5ba4827'), {})).rejects.toThrowError(NotFoundException);
    });
  });

  describe('getHierarchy()', () => {
    it('returns the unflattened hierarchy', async () => {
      const result = await service.getHierarchy(new Types.ObjectId('64cbaff1d364150ba5ba4823'));

      expect(result).toEqual({
        id: '64cbaff1d364150ba5ba4823',
        name: {
          en: 'Hierarchy',
          de: 'Hierarchie',
        },
        tags: [],
        createdAt: new Date('2023-08-03T13:47:29.030Z'),
        updatedAt: new Date('2023-08-03T14:39:04.595Z'),
        totalAccumulatedScore: 6,
        tree: {
          value: {
            name: {
              en: 'Wurzel',
              de: 'Root',
            },
            comment: {
              en: '{"ops":[]}',
              de: '{"ops":[]}',
            },
            scaleComment: {
              en: '{"ops":[]}',
              de: '{"ops":[]}',
            },
            accumulatedScore: 1,
            id: '64cbaff1d364150ba5ba4824',
          },
          children: [
            {
              value: {
                name: {
                  en: 'Child 1',
                  de: 'Kind 1',
                },
                comment: {
                  en: '{"ops":[]}',
                  de: '{"ops":[]}',
                },
                scaleComment: {
                  en: '{"ops":[]}',
                  de: '{"ops":[]}',
                },
                accumulatedScore: 2,
                id: '64cbaff1d364150ba5ba4825',
              },
              children: [
                {
                  value: {
                    name: {
                      en: 'Aspect 1',
                      de: 'Aspekt 1',
                    },
                    comment: {
                      en: '{"ops":[]}',
                      de: '{"ops":[]}',
                    },
                    scaleComment: {
                      en: '{"ops":[]}',
                      de: '{"ops":[]}',
                    },
                    accumulatedScore: 3,
                    id: '64cbaff1d364150ba5ba4826',
                  },
                  children: [],
                },
              ],
            },
          ],
        },
      });
    });

    it('returns null on unknown id', async () => {
      await expect(service.getHierarchy(new Types.ObjectId('64cbaff1d364150ba5ba4827'))).resolves.toBeNull();
    });
  });

  describe('delete()', () => {
    it('deletes the hierarchy', async () => {
      await service.delete(new Types.ObjectId('64cbaff1d364150ba5ba4823'));

      const document = await mongoose.connection.db
        .collection('quickstarthierarchies')
        .findOne({ _id: new Types.ObjectId('64cbaff1d364150ba5ba4823') });

      expect(document).toBeNull();
    });

    it('throws on unknown id', async () => {
      await expect(service.delete(new Types.ObjectId('64cbaff1d364150ba5ba4827'))).rejects.toThrowError(NotFoundException);
    });
  });

  describe('incrementTrackingCounter()', () => {
    it('increases the counter for a single objective', async () => {
      await service.incrementTrackingCounter([new Types.ObjectId('64cbaff1d364150ba5ba4825')]);

      const document = await mongoose.connection.db
        .collection('quickstarthierarchies')
        .findOne({ _id: new Types.ObjectId('64cbaff1d364150ba5ba4823') });

      expect(document.tree[1].accumulatedScore).toBe(3);
    });

    it('increases the counter for multiple objectives', async () => {
      await service.incrementTrackingCounter([
        new Types.ObjectId('64cbaff1d364150ba5ba4825'),
        new Types.ObjectId('64cbaff1d364150ba5ba4826'),
      ]);

      const document = await mongoose.connection.db
        .collection('quickstarthierarchies')
        .findOne({ _id: new Types.ObjectId('64cbaff1d364150ba5ba4823') });

      expect(document.tree[0].accumulatedScore).toBe(1);
      expect(document.tree[1].accumulatedScore).toBe(2.5);
      expect(document.tree[2].accumulatedScore).toBe(3.5);
    });

    it('throws on unknown id', async () => {
      await expect(service.incrementTrackingCounter([new Types.ObjectId('64cbaff1d364150ba5ba4827')])).rejects.toThrowError(
        NotFoundException,
      );
    });
  });

  describe('Data fetching', () => {
    const mockHierarchy2 = {
      _id: new Types.ObjectId('64cbaff1d364150ba5ba4830'),
      name: {
        en: 'A hierarchy',
        de: 'Eine Hierarchie',
      },
      tags: ['64cbaff1d364150ba5ba4841'],
      tree: [
        {
          name: {
            en: 'Wurzel',
            de: 'Root',
          },
          comment: {
            en: '{"ops":[]}',
            de: '{"ops":[]}',
          },
          scaleComment: {
            en: '{"ops":[]}',
            de: '{"ops":[]}',
          },
          level: 0,
          accumulatedScore: 5,
          _id: new Types.ObjectId('64cbaff1d364150ba5ba4831'),
        },
        {
          name: {
            en: 'A child node',
            de: 'Ein Kindknoten',
          },
          comment: {
            en: '{"ops":[]}',
            de: '{"ops":[]}',
          },
          scaleComment: {
            en: '{"ops":[]}',
            de: '{"ops":[]}',
          },
          level: 1,
          accumulatedScore: 3,
          _id: new Types.ObjectId('64cbaff1d364150ba5ba4832'),
        },
        {
          name: {
            en: 'A second-level aspect',
            de: 'Ein Aspekt auf zweiter Ebene',
          },
          comment: {
            en: '{"ops":[]}',
            de: '{"ops":[]}',
          },
          scaleComment: {
            en: '{"ops":[]}',
            de: '{"ops":[]}',
          },
          level: 2,
          accumulatedScore: 4,
          _id: new Types.ObjectId('64cbaff1d364150ba5ba4833'),
        },
      ],
      createdAt: new Date('2023-08-03T13:47:29.030Z'),
      updatedAt: new Date('2023-08-03T14:39:04.595Z'),
      __v: 0,
    };

    beforeEach(async () => {
      await mongoose.connection.db.collection('quickstarthierarchies').insertOne(mockHierarchy2);
    });

    describe('listHierarchies()', () => {
      it('allows sorting by "totalAccumulatedScore"', async () => {
        const resultAsc = await service.listHierarchies(
          {},
          { by: 'totalAccumulatedScore', direction: 'asc' },
          { limit: 100, offset: 0 },
          false,
        );
        expect(resultAsc.items).toMatchObject([{ id: '64cbaff1d364150ba5ba4823' }, { id: '64cbaff1d364150ba5ba4830' }]);

        const resultDesc = await service.listHierarchies(
          {},
          { by: 'totalAccumulatedScore', direction: 'desc' },
          { limit: 100, offset: 0 },
          false,
        );
        expect(resultDesc.items).toMatchObject([{ id: '64cbaff1d364150ba5ba4830' }, { id: '64cbaff1d364150ba5ba4823' }]);
      });

      it('allows sorting by "nameDe"', async () => {
        const resultAsc = await service.listHierarchies({}, { by: 'nameDe', direction: 'asc' }, { limit: 100, offset: 0 }, false);
        expect(resultAsc.items).toMatchObject([{ id: '64cbaff1d364150ba5ba4830' }, { id: '64cbaff1d364150ba5ba4823' }]);

        const resultDesc = await service.listHierarchies({}, { by: 'nameDe', direction: 'desc' }, { limit: 100, offset: 0 }, false);
        expect(resultDesc.items).toMatchObject([{ id: '64cbaff1d364150ba5ba4823' }, { id: '64cbaff1d364150ba5ba4830' }]);
      });

      it('filters by name', async () => {
        const result = await service.listHierarchies(
          { nameQuery: 'eine' },
          { by: 'nameDe', direction: 'asc' },
          { limit: 100, offset: 0 },
          false,
        );
        expect(result.items).toHaveLength(1);
        expect(result.items).toMatchObject([{ id: '64cbaff1d364150ba5ba4830' }]);
      });

      it('paginates', async () => {
        const result = await service.listHierarchies({}, { by: 'nameDe', direction: 'asc' }, { limit: 1, offset: 1 }, false);

        expect(result.count).toBe(2);
        expect(result.items).toHaveLength(1);
        expect(result.items).toMatchObject([{ id: '64cbaff1d364150ba5ba4823' }]);
      });

      it('returns well-formed hierarchies without stats', async () => {
        const result = await service.listHierarchies({}, { by: 'nameDe', direction: 'asc' }, { limit: 100, offset: 0 }, false);

        expect(result.items[0]).toEqual({
          id: '64cbaff1d364150ba5ba4830',
          name: {
            en: 'A hierarchy',
            de: 'Eine Hierarchie',
          },
          tags: ['64cbaff1d364150ba5ba4841'],
          tree: {
            value: {
              name: {
                en: 'Wurzel',
                de: 'Root',
              },
              comment: {
                en: '{"ops":[]}',
                de: '{"ops":[]}',
              },
              scaleComment: {
                en: '{"ops":[]}',
                de: '{"ops":[]}',
              },
              id: '64cbaff1d364150ba5ba4831',
            },
            children: [
              {
                value: {
                  name: {
                    en: 'A child node',
                    de: 'Ein Kindknoten',
                  },
                  comment: {
                    en: '{"ops":[]}',
                    de: '{"ops":[]}',
                  },
                  scaleComment: {
                    en: '{"ops":[]}',
                    de: '{"ops":[]}',
                  },
                  id: '64cbaff1d364150ba5ba4832',
                },
                children: [
                  {
                    value: {
                      name: {
                        en: 'A second-level aspect',
                        de: 'Ein Aspekt auf zweiter Ebene',
                      },
                      comment: {
                        en: '{"ops":[]}',
                        de: '{"ops":[]}',
                      },
                      scaleComment: {
                        en: '{"ops":[]}',
                        de: '{"ops":[]}',
                      },
                      id: '64cbaff1d364150ba5ba4833',
                    },
                    children: [],
                  },
                ],
              },
            ],
          },
          createdAt: new Date('2023-08-03T13:47:29.030Z'),
          updatedAt: new Date('2023-08-03T14:39:04.595Z'),
        });
      });

      it('returns well-formed hierarchies with stats', async () => {
        const result = await service.listHierarchies({}, { by: 'nameDe', direction: 'asc' }, { limit: 100, offset: 0 }, true);

        expect(result.items[0]).toEqual({
          id: '64cbaff1d364150ba5ba4830',
          name: {
            en: 'A hierarchy',
            de: 'Eine Hierarchie',
          },
          tags: ['64cbaff1d364150ba5ba4841'],
          totalAccumulatedScore: 12,
          tree: {
            value: {
              name: {
                en: 'Wurzel',
                de: 'Root',
              },
              comment: {
                en: '{"ops":[]}',
                de: '{"ops":[]}',
              },
              scaleComment: {
                en: '{"ops":[]}',
                de: '{"ops":[]}',
              },
              accumulatedScore: 5,
              id: '64cbaff1d364150ba5ba4831',
            },
            children: [
              {
                value: {
                  name: {
                    en: 'A child node',
                    de: 'Ein Kindknoten',
                  },
                  comment: {
                    en: '{"ops":[]}',
                    de: '{"ops":[]}',
                  },
                  scaleComment: {
                    en: '{"ops":[]}',
                    de: '{"ops":[]}',
                  },
                  accumulatedScore: 3,
                  id: '64cbaff1d364150ba5ba4832',
                },
                children: [
                  {
                    value: {
                      name: {
                        en: 'A second-level aspect',
                        de: 'Ein Aspekt auf zweiter Ebene',
                      },
                      comment: {
                        en: '{"ops":[]}',
                        de: '{"ops":[]}',
                      },
                      scaleComment: {
                        en: '{"ops":[]}',
                        de: '{"ops":[]}',
                      },
                      accumulatedScore: 4,
                      id: '64cbaff1d364150ba5ba4833',
                    },
                    children: [],
                  },
                ],
              },
            ],
          },
          createdAt: new Date('2023-08-03T13:47:29.030Z'),
          updatedAt: new Date('2023-08-03T14:39:04.595Z'),
        });
      });
    });

    describe('listObjectives()', () => {
      it('allows sorting by "accumulatedScore"', async () => {
        const resultAsc = await service.listObjectives(
          { level: 'only-fundamental' },
          { by: 'accumulatedScore', direction: 'asc' },
          { limit: 100, offset: 0 },
          false,
        );
        expect(resultAsc.items).toMatchObject([{ id: '64cbaff1d364150ba5ba4825' }, { id: '64cbaff1d364150ba5ba4832' }]);

        const resultDesc = await service.listObjectives(
          { level: 'only-fundamental' },
          { by: 'accumulatedScore', direction: 'desc' },
          { limit: 100, offset: 0 },
          false,
        );
        expect(resultDesc.items).toMatchObject([{ id: '64cbaff1d364150ba5ba4832' }, { id: '64cbaff1d364150ba5ba4825' }]);
      });

      it('paginates', async () => {
        const result = await service.listObjectives({ query: 'Ein' }, { by: 'nameDe', direction: 'asc' }, { limit: 1, offset: 1 }, false);

        expect(result.count).toBe(2);
        expect(result.items).toHaveLength(1);
        expect(result.items).toMatchObject([{ id: '64cbaff1d364150ba5ba4832' }]);
      });

      it('returns well-formed objectives without stats', async () => {
        const result = await service.listObjectives(
          { query: 'Ein Kindknoten' },
          { by: 'nameDe', direction: 'asc' },
          { limit: 100, offset: 0 },
          false,
        );

        expect(result.count).toBe(1);
        expect(result.items[0]).toEqual({
          name: {
            en: 'A child node',
            de: 'Ein Kindknoten',
          },
          comment: {
            en: '{"ops":[]}',
            de: '{"ops":[]}',
          },
          scaleComment: {
            en: '{"ops":[]}',
            de: '{"ops":[]}',
          },
          hierarchyId: '64cbaff1d364150ba5ba4830',
          hierarchyName: {
            en: 'A hierarchy',
            de: 'Eine Hierarchie',
          },
          tags: ['64cbaff1d364150ba5ba4841'],
          level: 1,
          id: '64cbaff1d364150ba5ba4832',
        });
      });

      it('returns stats', async () => {
        const result = await service.listObjectives(
          { query: 'Ein Kindknoten' },
          { by: 'nameDe', direction: 'asc' },
          { limit: 100, offset: 0 },
          true,
        );

        expect(result.count).toBe(1);
        expect(result.items[0]).toHaveProperty('accumulatedScore', 3);
      });
    });

    describe('listGroupedObjectives()', () => {
      beforeEach(async () => {
        await mongoose.connection.db.collection('quickstarthierarchies').insertOne({
          _id: new Types.ObjectId('64cbaff1d364150ba5ba4810'),
          name: {
            en: 'Hierarchy',
            de: 'Hierarchie',
          },
          tags: [] as string[],
          tree: [
            {
              name: {
                en: 'Wurzel',
                de: 'Root',
              },
              comment: {
                en: '{"ops":[]}',
                de: '{"ops":[]}',
              },
              scaleComment: {
                en: '{"ops":[]}',
                de: '{"ops":[]}',
              },
              level: 0,
              accumulatedScore: 1,
              _id: new Types.ObjectId('64cbaff1d364150ba5ba4811'),
            },
            {
              name: {
                en: 'Child 1',
                de: 'Kind 1',
              },
              comment: {
                en: '{"ops":[]}',
                de: '{"ops":[]}',
              },
              scaleComment: {
                en: '{"ops":[]}',
                de: '{"ops":[]}',
              },
              level: 1,
              accumulatedScore: 20,
              _id: new Types.ObjectId('64cbaff1d364150ba5ba4812'),
            },
          ],
        });
      });

      it('groups objectives with identical name', async () => {
        const result = await service.listGroupedObjectives(
          { query: 'Kind 1', level: 'only-fundamental' },
          { by: 'name', direction: 'asc' },
          { limit: 100, offset: 0 },
          false,
          'nameDe',
        );

        expect(result.count).toBe(1);
        expect(result.items[0].name).toBe('Kind 1');
        expect(result.items[0].items).toHaveLength(2);
      });
    });
  });
});
