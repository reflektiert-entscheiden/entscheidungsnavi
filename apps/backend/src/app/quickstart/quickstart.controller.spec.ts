import { Role } from '@entscheidungsnavi/api-types';
import {
  CanActivate,
  ClassSerializerInterceptor,
  ExecutionContext,
  INestApplication,
  Injectable,
  mixin,
  ValidationPipe,
} from '@nestjs/common';
import { APP_INTERCEPTOR, APP_PIPE } from '@nestjs/core';
import { Test } from '@nestjs/testing';
import { Types } from 'mongoose';
import request from 'supertest';
import { find } from 'lodash';
import { NextFunction, Request, Response } from 'express';
import { LoginGuard } from '../auth/login.guard';
import { QuickstartProjectsService } from './quickstart-projects.service';
import { QuickstartTagsService } from './quickstart-tags.service';
import { QuickstartController } from './quickstart.controller';
import { QuickstartProjectDto } from './dto/quickstart-project.dto';
import { QuickstartValuesService } from './quickstart-values.service';
import { QuickstartHierarchiesService } from './quickstart-hierarchies.service';
import { QuickstartQuestionsService } from './quickstart-questions.service';

jest.setTimeout(30000);

const userId = new Types.ObjectId();
let isQuickstartManager: boolean;

jest.mock('../auth/roles.guard.ts', () => ({
  RolesGuard: jest.fn().mockImplementation((_roles: Role[]) => {
    @Injectable()
    class RolesGuardMock implements CanActivate {
      canActivate(context: ExecutionContext) {
        const req = context.switchToHttp().getRequest();
        req.user = { _id: userId, roles: ['admin'] };
        return true;
      }
    }

    return mixin(RolesGuardMock);
  }),
}));

describe('QuickstartController', () => {
  let app: INestApplication;
  let updateProject: jest.Mock;
  let createProject: jest.Mock;
  let findById: jest.Mock;
  let findByIdIfVisible: jest.Mock;
  let listAll: jest.Mock;
  let listVisible: jest.Mock;
  let updateTag: jest.Mock;
  let createTag: jest.Mock;
  let listValues: jest.Mock;
  let updateValue: jest.Mock;
  let listHierarchies: jest.Mock;
  let listObjectives: jest.Mock;
  let listGroupedObjectives: jest.Mock;

  beforeEach(async () => {
    updateProject = jest.fn();
    createProject = jest.fn();
    findById = jest.fn();
    findByIdIfVisible = jest.fn();
    listAll = jest.fn();
    listVisible = jest.fn();
    updateTag = jest.fn();
    createTag = jest.fn();
    listValues = jest.fn();
    updateValue = jest.fn();
    listHierarchies = jest.fn();
    listObjectives = jest.fn();
    listGroupedObjectives = jest.fn();

    isQuickstartManager = false;

    const module = await Test.createTestingModule({
      controllers: [QuickstartController],
      providers: [
        {
          provide: QuickstartProjectsService,
          useValue: {
            update: updateProject,
            create: createProject,
            findById: findById,
            findByIdIfVisible: findByIdIfVisible,
            listAll: listAll,
            listVisible: listVisible,
          },
        },
        { provide: QuickstartTagsService, useValue: { update: updateTag, create: createTag } },
        { provide: QuickstartValuesService, useValue: { list: listValues, update: updateValue } },
        { provide: QuickstartQuestionsService, useValue: {} },
        { provide: QuickstartHierarchiesService, useValue: { listHierarchies, listObjectives, listGroupedObjectives } },
        {
          provide: APP_PIPE,
          useValue: new ValidationPipe({ whitelist: true, transform: true, transformOptions: { exposeUnsetFields: false } }),
        },
        { provide: APP_INTERCEPTOR, useValue: ClassSerializerInterceptor },
      ],
    })
      .overrideGuard(LoginGuard)
      .useValue({ canActivate: () => true })
      .compile();

    app = module.createNestApplication();

    // For the purpose of this controller, we are always authenticated
    app.use((request: Request, _response: Response, next: NextFunction) => {
      request.isAuthenticated = (() => true) as any;
      if (isQuickstartManager) {
        request.user = { _id: userId, roles: ['quickstart-manager'] } as any;
      } else {
        request.user = { _id: userId, roles: [] } as any;
      }

      next();
    });

    await app.init();
  });

  afterEach(async () => {
    await app.close();
  });

  describe('GET /quickstart/projects', () => {
    const allProjects: QuickstartProjectDto[] = [
      {
        id: new Types.ObjectId().toString(),
        name: 'visible project',
        data: '',
        visible: true,
        tags: [],
        createdAt: new Date(),
        updatedAt: new Date(),
        shareUrl: 'http://example.com',
      },
      {
        id: new Types.ObjectId().toString(),
        name: 'invisible project',
        data: '',
        visible: false,
        tags: [],
        createdAt: new Date(),
        updatedAt: new Date(),
        shareUrl: 'http://example.com',
      },
    ];

    const visibleProjects = allProjects.filter(project => project.visible);

    beforeEach(() => {
      listAll.mockResolvedValue(allProjects);
      listVisible.mockResolvedValue(visibleProjects);
    });

    it('returns all projects when prompted', async () => {
      await request(app.getHttpServer())
        .get('/quickstart/projects?showInvisible=true')
        .expect(200)
        .expect(response => {
          expect(response.body).toHaveLength(allProjects.length);
          response.body.forEach((returnedProject: QuickstartProjectDto, index: number) => {
            expect(returnedProject.id).toEqual(allProjects[index].id);
          });
        });
    });
    it('returns visible projects by default', async () => {
      await request(app.getHttpServer())
        .get('/quickstart/projects')
        .expect(200)
        .expect(response => {
          expect(response.body).toHaveLength(visibleProjects.length);
          response.body.forEach((returnedProject: QuickstartProjectDto, index: number) => {
            expect(returnedProject.id).toEqual(visibleProjects[index].id);
          });
        });
    });
  });

  describe('POST /quickstart/projects', () => {
    const createdProjectId = new Types.ObjectId();
    const createdProject = { id: createdProjectId, name: 'some name', data: '' };
    const createdProjectDto = { ...createdProject, id: createdProjectId.toString() };

    beforeEach(() => {
      createProject.mockResolvedValue(createdProject);
    });

    it('creates a project', async () => {
      const requestProject = { name: 'test', data: '{}' };

      await request(app.getHttpServer()).post('/quickstart/projects').send(requestProject).expect(201).expect(createdProjectDto);

      expect(createProject.mock.calls[0][0]).toMatchObject(requestProject);
    });

    it('fails on empty name', () => {
      return request(app.getHttpServer()).post('/quickstart/projects').send({ name: '', data: '{}' }).expect(400);
    });

    it('fails on missing data', () => {
      return request(app.getHttpServer()).post('/quickstart/projects').send({ name: '' }).expect(400);
    });
  });

  describe('GET /quickstart/projects/:id', () => {
    const visibleProject: QuickstartProjectDto = {
      id: new Types.ObjectId().toString(),
      name: 'visible project',
      data: '',
      visible: true,
      tags: [],
      createdAt: new Date(),
      updatedAt: new Date(),
      shareUrl: 'http://example.com',
    };

    const hiddenProject: QuickstartProjectDto = {
      id: new Types.ObjectId().toString(),
      name: 'invisible project',
      data: '',
      visible: false,
      tags: [],
      createdAt: new Date(),
      updatedAt: new Date(),
      shareUrl: 'http://example.com',
    };

    const allProjects = [visibleProject, hiddenProject];

    beforeEach(() => {
      findById.mockImplementation((id: Types.ObjectId) => find(allProjects, { id: id.toString() }));
      findByIdIfVisible.mockImplementation((id: Types.ObjectId) => find(allProjects, { id: id.toString(), visible: true }));
    });

    it('returns visible project', async () => {
      await request(app.getHttpServer())
        .get(`/quickstart/projects/${visibleProject.id}`)
        .expect(200)
        .expect(response => {
          expect(response.body.id).toEqual(visibleProject.id);
        });
    });

    it('returns hidden project', async () => {
      await request(app.getHttpServer())
        .get(`/quickstart/projects/${hiddenProject.id}`)
        .expect(200)
        .expect(response => {
          expect(response.body.id).toEqual(hiddenProject.id);
        });
    });
  });

  describe('PATCH /quickstart/projects/:id', () => {
    let targetProjectId: Types.ObjectId;

    beforeEach(() => {
      updateProject.mockResolvedValue({});
      targetProjectId = new Types.ObjectId();
    });

    it('updates a project correctly', async () => {
      const update = { name: 'new name', data: 'mock data', tags: [new Types.ObjectId().toString()] };

      await request(app.getHttpServer()).patch(`/quickstart/projects/${targetProjectId.toString()}`).send(update).expect(200);

      expect(updateProject.mock.calls.length).toBe(1);
      expect(updateProject.mock.calls[0][0]).toEqual(targetProjectId);
      expect(updateProject.mock.calls[0][1]).toMatchObject(update);
    });

    it('fails on invalid tag id', async () => {
      const update = { tags: ['invalid'] };

      await request(app.getHttpServer()).patch(`/quickstart/projects/${targetProjectId.toString()}`).send(update).expect(400);

      expect(updateProject.mock.calls.length).toBe(0);
    });
  });

  describe('POST /quickstart/tags', () => {
    const createdTagId = new Types.ObjectId();
    const createdTag = { id: createdTagId, name: { en: 'english name', de: 'deutscher name' } };
    const createdTagDto = { ...createdTag, id: createdTagId.toString() };

    beforeEach(() => {
      createTag.mockResolvedValue(createdTag);
    });

    it('creates a tag', async () => {
      const requestTag = { name: { en: 'test', de: 'test' } };

      await request(app.getHttpServer()).post('/quickstart/tags').send(requestTag).expect(201).expect(createdTagDto);

      expect(createTag.mock.calls[0][0]).toMatchObject(requestTag);
    });

    it('fails on invalid name', () => {
      return request(app.getHttpServer()).post('/quickstart/projects').send({ name: '' }).expect(400);
    });

    it('requires german and english names', () => {
      return request(app.getHttpServer())
        .post('/quickstart/projects')
        .send({ name: { en: 'test' } })
        .expect(400);
    });
  });

  describe('PATCH /quickstart/tags/:id', () => {
    let targetTagId: Types.ObjectId;

    beforeEach(() => {
      updateTag.mockResolvedValue({});
      targetTagId = new Types.ObjectId();
    });

    it('updates a tag correctly', async () => {
      const update = { name: { en: 'new name', de: 'neuer name' } };

      await request(app.getHttpServer()).patch(`/quickstart/tags/${targetTagId.toString()}`).send(update).expect(200);

      expect(updateTag.mock.calls.length).toBe(1);
      expect(updateTag.mock.calls[0][0]).toEqual(targetTagId);
      expect(updateTag.mock.calls[0][1]).toMatchObject(update);
    });

    it('fails on invalid name', async () => {
      const update = { name: {} };

      await request(app.getHttpServer()).patch(`/quickstart/projects/${targetTagId.toString()}`).send(update).expect(400);

      expect(updateTag.mock.calls.length).toBe(0);
    });
  });

  describe('GET /quickstart/values', () => {
    const values = [{ name: { de: 'abc', en: 'abc' }, countInTop5: 1 }];

    beforeEach(() => {
      listValues.mockResolvedValue(values);
    });

    it('lists values without stats', async () => {
      const { body } = await request(app.getHttpServer()).get('/quickstart/values').expect(200);
      expect(body).toEqual(values);

      expect(listValues).toHaveBeenCalledWith(false);
    });

    it('denies stats to non-quickstart-managers', async () => {
      await request(app.getHttpServer()).get('/quickstart/values?includeStats=true').expect(403);
      expect(listValues).not.toHaveBeenCalled();
    });

    it('shows stats to quickstart-managers', async () => {
      isQuickstartManager = true;
      const { body } = await request(app.getHttpServer()).get('/quickstart/values?includeStats=true').expect(200);

      expect(listValues).toHaveBeenCalledWith(true);
      expect(body).toEqual(values);
    });
  });

  describe('GET /hierarchies', () => {
    const hierarchies = {
      items: [
        {
          id: '64cacc6bbcdd8c207719c0c2',
          tree: {
            value: {
              id: '64cacc6bbcdd8c207719c0c3',
            },
          },
        },
      ],
      count: 1,
    };

    beforeEach(() => {
      listHierarchies.mockResolvedValue(hierarchies);
    });

    it('lists hierarchies without stats', async () => {
      const { body } = await request(app.getHttpServer()).get('/quickstart/hierarchies').expect(200);
      expect(body).toEqual(hierarchies);

      expect(listHierarchies).toHaveBeenCalledTimes(1);
      expect(listHierarchies.mock.calls[0][3]).toBe(false);
    });

    it('denies stats to non-quickstart-managers', async () => {
      await request(app.getHttpServer()).get('/quickstart/hierarchies?includeStats=true').expect(403);
      expect(listHierarchies).not.toHaveBeenCalled();
    });

    it('shows stats to quickstart-managers', async () => {
      isQuickstartManager = true;
      const { body } = await request(app.getHttpServer()).get('/quickstart/hierarchies?includeStats=true').expect(200);

      expect(listHierarchies).toHaveBeenCalledTimes(1);
      expect(listHierarchies.mock.calls[0][3]).toBe(true);
      expect(body).toEqual(hierarchies);
    });

    it('escapes the query string correctly', async () => {
      const filter = { nameQuery: 'abcd [ ] * + ? { } . ( ) ^ $ | - \\' };

      await request(app.getHttpServer()).get('/quickstart/hierarchies?includeStats=false').query(filter).expect(200);

      expect(listHierarchies.mock.calls[0][0]).toMatchObject({
        nameQuery: 'abcd \\[ \\] \\* \\+ \\? \\{ \\} \\. \\( \\) \\^ \\$ \\| - \\\\',
      });
    });
  });

  describe('GET /objectives', () => {
    const objectives = {
      items: [
        {
          id: '64cacc6bbcdd8c207719c0c4',
          hierarchyId: '64cacc6bbcdd8c207719c0c2',
        },
      ],
      count: 1,
    };

    beforeEach(() => {
      listObjectives.mockResolvedValue(objectives);
    });

    it('lists hierarchies without stats', async () => {
      const { body } = await request(app.getHttpServer())
        .get('/quickstart/objectives?sortBy=accumulatedScore&sortDirection=desc')
        .expect(200);
      expect(body).toEqual(objectives);

      expect(listObjectives).toHaveBeenCalledTimes(1);
      expect(listObjectives.mock.calls[0][3]).toBe(false);
    });

    it('denies stats to non-quickstart-managers', async () => {
      await request(app.getHttpServer())
        .get('/quickstart/objectives?sortBy=accumulatedScore&sortDirection=desc&includeStats=true')
        .expect(403);
      expect(listObjectives).not.toHaveBeenCalled();
    });

    it('shows stats to quickstart-managers', async () => {
      isQuickstartManager = true;
      const { body } = await request(app.getHttpServer())
        .get('/quickstart/objectives?sortBy=accumulatedScore&sortDirection=desc&includeStats=true')
        .expect(200);

      expect(listObjectives).toHaveBeenCalledTimes(1);
      expect(listObjectives.mock.calls[0][3]).toBe(true);
      expect(body).toEqual(objectives);
    });

    it('escapes the query string correctly', async () => {
      const filter = { query: 'abcd [ ] * + ? { } . ( ) ^ $ | - \\' };

      await request(app.getHttpServer())
        .get('/quickstart/objectives?sortBy=accumulatedScore&sortDirection=desc&includeStats=false')
        .query(filter)
        .expect(200);

      expect(listObjectives.mock.calls[0][0]).toMatchObject({ query: 'abcd \\[ \\] \\* \\+ \\? \\{ \\} \\. \\( \\) \\^ \\$ \\| - \\\\' });
    });

    it('groups when required', async () => {
      listGroupedObjectives.mockResolvedValue(objectives);

      await request(app.getHttpServer())
        .get('/quickstart/objectives?sortBy=accumulatedScore&sortDirection=desc&includeStats=false&groupBy=nameDe')
        .expect(200);

      expect(listGroupedObjectives).toHaveBeenCalled();
      expect(listObjectives).not.toHaveBeenCalled();
    });
  });
});
