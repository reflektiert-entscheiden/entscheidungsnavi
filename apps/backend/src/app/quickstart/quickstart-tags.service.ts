import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { plainToClass } from 'class-transformer';
import { Model, Types } from 'mongoose';
import { QuickstartTag, QuickstartTagDocument } from '../schemas/quickstart-tag.schema';
import { CreateQuickstartTagDto } from './dto/create-quickstart-tag.dto';
import { QuickstartTagDto } from './dto/quickstart-tag.dto';
import { UpdateQuickstartTagDto } from './dto/update-quickstart-tag.dto';
import { QuickstartProjectsService } from './quickstart-projects.service';
import { QuickstartQuestionsService } from './quickstart-questions.service';

export function toDto(doc: QuickstartTagDocument) {
  return plainToClass(QuickstartTagDto, doc);
}

@Injectable()
export class QuickstartTagsService {
  constructor(
    @InjectModel(QuickstartTag.name) private quickstartTagModel: Model<QuickstartTagDocument>,
    private quickstartProjectsService: QuickstartProjectsService,
    private quickstartQuestionsService: QuickstartQuestionsService,
  ) {}

  async create(tag: CreateQuickstartTagDto) {
    return toDto(await this.quickstartTagModel.create(tag));
  }

  async list() {
    const docs = await this.quickstartTagModel.find().sort({ _id: 1 }).exec();
    return docs.map(toDto);
  }

  async findById(id: Types.ObjectId) {
    return toDto(await this.quickstartTagModel.findById(id).exec());
  }

  async update(id: Types.ObjectId, tag: UpdateQuickstartTagDto) {
    const result = await this.quickstartTagModel.findByIdAndUpdate(id, tag, { new: true }).exec();
    return toDto(result);
  }

  async remove(id: Types.ObjectId) {
    await this.quickstartProjectsService.removeTagReferences(id);
    await this.quickstartQuestionsService.removeTagReferences(id);

    const result = await this.quickstartTagModel.deleteOne({ _id: id }).exec();
    if (result.deletedCount === 0) {
      throw new NotFoundException();
    }
  }
}
