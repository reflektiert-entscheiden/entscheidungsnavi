import { Language } from '@entscheidungsnavi/api-types';
import { Injectable, Logger } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { createTransport, Transporter } from 'nodemailer';
import Mail from 'nodemailer/lib/mailer';

@Injectable()
export class EmailService {
  private readonly logger = new Logger(EmailService.name);
  private transport: Transporter;
  private baseUrl: string;

  constructor(configService: ConfigService) {
    this.baseUrl = configService.get<string>('BASE_URL');

    const host = configService.get<string>('MAIL_HOST'),
      login = configService.get<string>('MAIL_USER'),
      password = configService.get<string>('MAIL_PASSWORD'),
      from = configService.get<string>('MAIL_FROM');

    const logEmail: Mail.PluginFunction = (mail, callback) => {
      this.logger.verbose(`Sending E-Mail`);
      this.logger.verbose(`
- From: ${mail.data.from}
- To: ${mail.data.to}
- Subject: ${mail.data.subject}
- Content: ${mail.data.html ?? mail.data.text}
      `);
      callback();
    };

    if (host && login && password && from) {
      this.transport = createTransport(
        {
          host,
          auth: {
            user: login,
            pass: password,
          },
        },
        { from },
      ).use('compile', logEmail);
    } else {
      this.logger.warn('SMTP credentials missing or incomplete. No actual Emails will be sent out.');
      this.transport = createTransport({
        jsonTransport: true,
      }).use('compile', logEmail);
    }
  }

  async sendConfirmationMail(email: string, token: string, lang: Language) {
    if (!this.transport) {
      return;
    }

    if (lang === 'de') {
      await this.transport.sendMail({
        to: email,
        subject: 'Verifizierungscode für das Entscheidungsnavi',
        // eslint-disable-next-line max-len
        text: `Vielen Dank für Deine Registrierung beim Entscheidungsnavi (${this.baseUrl}).

Dein Aktivierungscode lautet: ${token}. Der Code ist 15 Minuten gültig.`,
        html: `Vielen Dank für Deine Registrierung beim Entscheidungsnavi (<a href="${this.baseUrl}">${this.baseUrl}</a>).
<br><br>
Dein Aktivierungscode lautet <b>${token}</b>. Der Code ist 15 Minuten gültig.`,
      });
    } else {
      await this.transport.sendMail({
        to: email,
        subject: 'Verification code for the Entscheidungsnavi',
        // eslint-disable-next-line max-len
        text: `Thank you for registering for the Entscheidungsnavi (${this.baseUrl}).

Your activation code is: ${token}. The code is valid for 15 minutes.`,
        html: `Thank you for registering at <a href="${this.baseUrl}">${this.baseUrl}</a>.
<br><br>
Your activation code is <b>${token}</b>. The code is valid for 15 minutes.`,
      });
    }
  }

  async sendResetMail(email: string, token: string, lang: Language) {
    if (!this.transport) {
      return;
    }

    const resetUrl = `${this.baseUrl}/reset/${token}`;
    if (lang === 'de') {
      await this.transport.sendMail({
        to: email,
        subject: 'Passwort zurücksetzen',
        text: `Du hast das Zurücksetzen Deines Passworts beantragt.
Besuche dazu bitte folgende Seite: ${resetUrl}`,
        html: `Du hast das Zurücksetzen Deines Passworts beantragt.
Besuche dazu bitte folgende Seite: <a href="${resetUrl}">${resetUrl}</a>`,
      });
    } else {
      await this.transport.sendMail({
        to: email,
        subject: 'Reset password',
        text: `You have requested a password reset.
To continue, please open the following page: ${resetUrl}`,
        html: `You have requested a password reset.
To continue, please open the following page: <a href="${resetUrl}">${resetUrl}</a>`,
      });
    }
  }

  async sendKlugEmail(email: string, token: string, pdfExport: string) {
    if (!this.transport) {
      return;
    }

    const naviUrl = `${this.baseUrl}/start?klugToken=${token}`;

    await this.transport.sendMail({
      to: email,
      subject: 'Dein KLUGentscheiden Projekt für das Entscheidungsnavi',
      text: `Hallo,
schön, dass Du weiter an der Lösung Deines Entscheidungsproblems im 
Entscheidungsnavi arbeiten möchtest. Das Entscheidungsnavi ist ein frei im 
Internet verfügbares Tool, was von der RWTH Aachen University entwickelt 
wurde, vom Förderverein Reflektiert Entscheiden e. V. (https://www.reflektiert-entscheiden.de) unterstützt wird 
und jedem Interessierten frei zur Verfügung steht.
\n
Dein Klug-Projekt kannst Du jederzeit unter dem folgenden Link im Entscheidungsnavi aufrufen: ${naviUrl}.
Dort kannst Du weiter an Deinem Projekt 
arbeiten. Um Deine Daten weiter zu sichern, kannst Du Dich im Entscheidungsnavi 
registrieren und in Deinem persönlichen Account das Projekt speichern.
\n
Viel Spaß und gute Entscheidungen wünscht Dir
\n
Das Team vom Entscheidungsnavi
\n
\n
P.S: Das beigefügte PDF ist Dein bisheriges Ergebnis aus deinem KLUGentscheiden-
Projekt mit dem Token ${token}`,
      html: `Hallo,
<br>
<br>
schön, dass Du weiter an der Lösung Deines Entscheidungsproblems im 
Entscheidungsnavi arbeiten möchtest. Das Entscheidungsnavi ist ein frei im 
Internet verfügbares Tool, was von der RWTH Aachen University entwickelt wurde, 
vom Förderverein Reflektiert Entscheiden e.&nbsp;V. (<a href = "https://www.reflektiert-entscheiden.de">www.reflektiert-entscheiden.de</a>) 
unterstützt wird und jedem Interessierten frei zur Verfügung steht.
<br>
<br>
Dein Klug-Projekt kannst Du jederzeit <a href = "${naviUrl}">hier</a> im Entscheidungsnavi aufrufen. 
Dort kannst Du weiter an Deinem Projekt 
arbeiten. Um Deine Daten weiter zu sichern, kannst Du Dich im Entscheidungsnavi 
registrieren und in Deinem persönlichen Account das Projekt speichern.
<br>
<br>
Viel Spaß und gute Entscheidungen wünscht Dir
<br>
<br>
Das Team vom Entscheidungsnavi
<br>
<br>
<br>
P.S: Das beigefügte PDF ist Dein bisheriges Ergebnis aus deinem KLUGentscheiden-
Projekt mit dem Token ${token}`,
      attachments: [{ filename: 'Klug Ergebnis.pdf', content: pdfExport, encoding: 'base64' }],
    });
  }

  async sendTeamInviteMail(email: string, inviterName: string, inviteToken: string, teamName: string, lang: Language) {
    if (!this.transport) {
      return;
    }

    const teamJoinUrl = `${this.baseUrl}/start?joinTeam=${inviteToken}`;

    if (lang === 'de') {
      await this.transport.sendMail({
        to: email,
        subject: `Einladung zum Team ${teamName}`,
        text: `Du wurdest von ${inviterName} zu einem Team Projekt im Entscheidungsnavi eingeladen.
        Besuche zum Beitreten bitte folgende Seite: ${teamJoinUrl}`,
        html: `Du wurdest von ${inviterName} zu einem Team Projekt im Entscheidungsnavi eingeladen.
Besuche zum Beitreten bitte folgende Seite: <a href="${teamJoinUrl}">${teamJoinUrl}</a>`,
      });
    } else {
      await this.transport.sendMail({
        to: email,
        subject: `Invitation to the team ${teamName}`,
        text: `You have been invited by ${inviterName} to join a team project in the Entscheidungsnavi.
        Please visit the following page to join: ${teamJoinUrl}`,
        html: `You have been invited by ${inviterName} to join a team project in the Entscheidungsnavi.
Please visit the following page to join: <a href="${teamJoinUrl}">${teamJoinUrl}</a>`,
      });
    }
  }
}
