import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { EmailService } from './email.service';

@Module({
  providers: [EmailService, ConfigModule],
  exports: [EmailService],
})
export class EmailModule {}
