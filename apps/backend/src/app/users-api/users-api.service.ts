import crypto from 'crypto';
import { ConflictException, GoneException, Injectable, NotFoundException, UnauthorizedException } from '@nestjs/common';
import { InjectConnection, InjectModel } from '@nestjs/mongoose';
import { PassportLocalModel, PassportLocalDocument, FilterQuery, Types, Model, Connection } from 'mongoose';
import { plainToClass } from 'class-transformer';
import { Language, Role, UserSortBy } from '@entscheidungsnavi/api-types';
import { SortDirection } from '@angular/material/sort';
import { User, UserDocument } from '../schemas/user.schema';
import { EventRegistrationsService } from '../events/event-registrations.service';
import { ProjectsService } from '../projects/projects.service';
import { EmailConfirmation, EmailConfirmationDocument } from '../schemas/email-confirmation.schema';
import { EmailService } from '../email/email.service';
import { UsersService } from '../users/users.service';
import { TeamsService } from '../team/teams.service';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { UserListDto } from './dto/user-list.dto';
import { UserDto } from './dto/user.dto';
import { UsersFilterDto } from './dto/users-filter.dto';

export function toDto(doc: UserDocument) {
  return plainToClass(UserDto, doc, { exposeDefaultValues: true });
}

@Injectable()
export class UsersApiService {
  constructor(
    @InjectModel(User.name) private userModel: PassportLocalModel<UserDocument & PassportLocalDocument>,
    @InjectModel(EmailConfirmation.name) private emailConfirmationModel: Model<EmailConfirmationDocument>,
    private usersService: UsersService,
    private projectsService: ProjectsService,
    private eventRegistrationsService: EventRegistrationsService,
    private emailService: EmailService,
    private teamsService: TeamsService,
    @InjectConnection() private connection: Connection,
  ) {}

  async create(createUserDto: CreateUserDto, lang: Language) {
    const { password, ...userData } = createUserDto;
    const user = await new this.userModel(userData);

    let registeredUser;
    try {
      registeredUser = await this.userModel.register(user, password);
    } catch (error: unknown) {
      if (error instanceof Error && error.name === 'UserExistsError') {
        throw new ConflictException();
      }

      throw error;
    }

    await this.startEmailConfirmation(registeredUser, lang);
    return toDto(registeredUser);
  }

  async updateRoles(id: Types.ObjectId, roles: Role[]) {
    const res = await this.userModel.updateOne({ _id: id }, { roles }).exec();
    if (res.matchedCount === 0) {
      throw new NotFoundException();
    }
  }

  async deleteOne(id: Types.ObjectId, password: string) {
    const user = await this.usersService.findOne(id);
    const authResult = await user.authenticate(password);

    if (authResult.error) {
      throw new UnauthorizedException();
    }

    await this.deleteOneAdmin(id);
  }

  async deleteOneAdmin(id: Types.ObjectId) {
    await this.connection.transaction(async session => {
      await this.eventRegistrationsService.deleteAllUnsubmittedRegistrations(id, session);
      await this.projectsService.deleteAllForUser(id, session);
      const wasRemovedFromTeams = await this.teamsService.removeFromAllTeams(id, session);

      if (!wasRemovedFromTeams) {
        throw new ConflictException({ error: 'user-is-team-owner' });
      }
    });

    const result = await this.userModel.deleteOne({ _id: id }).exec();

    if (result.deletedCount === 0) {
      throw new NotFoundException();
    }
  }

  async findUsers(filters: UsersFilterDto, sort: { column: UserSortBy; direction: SortDirection }, limit?: number, offset?: number) {
    const f: FilterQuery<UserDocument & PassportLocalDocument> = {};
    if (filters.query) {
      // We assume that filters.query is already escaped for use in a RegEx
      f.$or = [{ name: { $regex: filters.query, $options: 'i' } }, { email: { $regex: filters.query, $options: 'i' } }];
    }
    if (filters.roles) {
      f.roles = { $all: filters.roles };
    }
    if (filters.emailConfirmed != null) {
      f.emailConfirmed = filters.emailConfirmed || { $ne: true };
    }

    const query = this.userModel.find(f);
    const totalCount = await query.clone().countDocuments(); // Count ignoring pagination

    query
      .sort({ [sort.column]: sort.direction === 'asc' ? 1 : -1 })
      .sort({ _id: 1 })
      .skip(offset ?? 0)
      .limit(limit ?? 100);

    const items = (await query.exec()).map(toDto);

    return plainToClass(UserListDto, { items, count: totalCount }, { exposeDefaultValues: true });
  }

  async getOne(id: Types.ObjectId) {
    const user = await this.usersService.findOne(id);
    return user ? toDto(user) : null;
  }

  /**
   * Used when a user updates their own profile.
   *
   * This cannot update a users roles.
   */
  async updateSelf(id: Types.ObjectId, update: UpdateUserDto, lang: Language) {
    const userDocument = await this.usersService.findOne(id);
    if (userDocument == null) {
      throw new NotFoundException();
    }

    const { email, name, password, oldPassword } = update;

    if (email != null) {
      userDocument.email = email;
      userDocument.emailConfirmed = false;
    }
    if (name !== undefined) {
      // Allow emptying the name field by setting null
      userDocument.name = name;
    }

    if (email != null) {
      // Start a new eail confirmation if email has changed
      await this.startEmailConfirmation(userDocument, lang);
    }

    if (password != null) {
      if (oldPassword == null) {
        throw new UnauthorizedException();
      }
      try {
        await userDocument.changePassword(oldPassword, password);
      } catch {
        throw new UnauthorizedException();
      }
    }

    await userDocument.save();
  }

  async requestEmailConfirmation(userId: Types.ObjectId, lang: Language) {
    const user = await this.usersService.findOne(userId);
    await this.startEmailConfirmation(user, lang);
  }

  async confirmEmail(userId: Types.ObjectId, token: string) {
    const confirmation = await this.emailConfirmationModel.findOneAndDelete({ userId, token }).exec();
    if (confirmation == null) {
      throw new NotFoundException();
    }

    await this.userModel.updateOne({ _id: userId }, { emailConfirmed: true }).exec();
  }

  private async startEmailConfirmation(user: UserDocument, lang: Language) {
    if (user.emailConfirmed) {
      throw new GoneException();
    }

    const token = crypto.randomInt(1000000).toString().padStart(6, '0');
    await this.emailConfirmationModel.updateOne({ userId: new Types.ObjectId(user.id) }, { token }, { upsert: true }).exec();
    await this.emailService.sendConfirmationMail(user.email, token, lang);
  }
}
