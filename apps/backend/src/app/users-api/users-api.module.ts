import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import PassportLocalMongoose from 'passport-local-mongoose';
import { AuthModule } from '../auth/auth.module';
import { EmailModule } from '../email/email.module';
import { ProjectsModule } from '../projects/projects.module';
import { EmailConfirmation, EmailConfirmationSchema } from '../schemas/email-confirmation.schema';
import { User, UserSchema } from '../schemas/user.schema';
import { EventsModule } from '../events/events.module';
import { UsersModule } from '../users/users.module';
import { TeamsModule } from '../team/teams.module';
import { UserController } from './user.controller';
import { UsersController } from './users.controller';
import { UsersApiService } from './users-api.service';

@Module({
  imports: [
    MongooseModule.forFeatureAsync([
      {
        name: User.name,
        useFactory: () => UserSchema.plugin(PassportLocalMongoose, { usernameField: 'email', limitAttempts: true }),
      },
    ]),
    MongooseModule.forFeature([
      {
        name: EmailConfirmation.name,
        schema: EmailConfirmationSchema,
      },
    ]),
    EmailModule,
    UsersModule,
    AuthModule,
    ProjectsModule,
    EventsModule,
    TeamsModule,
  ],
  controllers: [UsersController, UserController],
  providers: [UsersApiService],
  exports: [],
})
export class UsersApiModule {}
