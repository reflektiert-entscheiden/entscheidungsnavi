import { UserList } from '@entscheidungsnavi/api-types';
import { Type } from 'class-transformer';
import { UserDto } from './user.dto';

export class UserListDto implements UserList {
  @Type(() => UserDto)
  items: UserDto[];
  count: number;
}
