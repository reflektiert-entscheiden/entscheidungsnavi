import { ClassSerializerInterceptor, INestApplication, ValidationPipe } from '@nestjs/common';
import { APP_INTERCEPTOR, APP_PIPE } from '@nestjs/core';
import { Test } from '@nestjs/testing';
import { Types } from 'mongoose';
import request from 'supertest';
import { NextFunction, Request, Response } from 'express';
import { ProjectsService } from '../projects/projects.service';
import { LoginGuard } from '../auth/login.guard';
import { UserController } from './user.controller';
import { UsersApiService } from './users-api.service';

const userId = new Types.ObjectId();

describe('UserController', () => {
  let app: INestApplication;
  let getUser: jest.Mock;
  let updateSelf: jest.Mock;

  beforeEach(async () => {
    getUser = jest.fn();
    updateSelf = jest.fn();

    const module = await Test.createTestingModule({
      controllers: [UserController],
      providers: [
        { provide: UsersApiService, useValue: { getOne: getUser, updateSelf } },
        { provide: ProjectsService, useValue: {} },
        {
          provide: APP_PIPE,
          useValue: new ValidationPipe({ whitelist: true, transform: true, transformOptions: { exposeUnsetFields: false } }),
        },
        { provide: APP_INTERCEPTOR, useValue: ClassSerializerInterceptor },
      ],
    })
      .overrideGuard(LoginGuard)
      .useValue({ canActivate: () => true })
      .compile();

    app = module.createNestApplication();
    app.use((req: Request, _res: Response, next: NextFunction) => {
      req.user = { _id: userId } as any;
      req.isAuthenticated = (() => true) as any;
      next();
    });
    await app.init();
  });

  afterEach(async () => {
    await app.close();
  });

  describe('GET /user', () => {
    it('returns user', () => {
      const user = { id: userId, email: 'test@entscheidungsnavi.de', emailConfirmed: false };
      getUser.mockResolvedValueOnce(user);

      return request(app.getHttpServer())
        .get('/user')
        .expect({ ...user, id: userId.toString() });
    });
  });

  describe('PATCH /user', () => {
    it('updates the user', async () => {
      const user = { email: 'new@entscheidungsnavi.de' };
      await request(app.getHttpServer()).patch('/user').send(user).expect(200);

      expect(updateSelf.mock.calls.length).toBe(1);
      expect(updateSelf.mock.calls[0][1]).toEqual(user);
    });

    it('fails on invalid email', async () => {
      await request(app.getHttpServer()).patch('/user').send({ email: 'thisisinvalid' }).expect(400);
      expect(updateSelf.mock.calls.length).toBe(0);
    });

    it('fails on short password', async () => {
      await request(app.getHttpServer()).patch('/user').send({ password: '1234' }).expect(400);
      expect(updateSelf.mock.calls.length).toBe(0);
    });

    it('fails on empty name', async () => {
      await request(app.getHttpServer()).patch('/user').send({ name: '' }).expect(400);
      expect(updateSelf.mock.calls.length).toBe(0);
    });
  });
});
