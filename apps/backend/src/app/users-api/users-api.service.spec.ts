import { getConnectionToken, getModelToken } from '@nestjs/mongoose';
import { Test } from '@nestjs/testing';
import { plainToClass } from 'class-transformer';
import { Types } from 'mongoose';
import { ProjectsService } from '../projects/projects.service';
import { User } from '../schemas/user.schema';
import { EventRegistrationsService } from '../events/event-registrations.service';
import { UsersService } from '../users/users.service';
import { EmailService } from '../email/email.service';
import { EmailConfirmation } from '../schemas/email-confirmation.schema';
import { TeamsService } from '../team/teams.service';
import { UpdateUserDto } from './dto/update-user.dto';
import { UsersApiService } from './users-api.service';

describe('UsersApiService', () => {
  let usersApiService: UsersApiService;
  let findUserById: jest.Mock;
  let updateEmailConfirmation: jest.Mock;
  let sendConfirmationMail: jest.Mock;
  let deleteUnsubmittedEventRegistrations: jest.Mock;
  let deleteUserProjects: jest.Mock;
  let removeUserFromTeams: jest.Mock;

  let userExec: jest.Mock;
  let userCount: jest.Mock;
  let userFind: jest.Mock;
  let userSort: jest.Mock;

  beforeEach(async () => {
    findUserById = jest.fn();
    updateEmailConfirmation = jest.fn();
    sendConfirmationMail = jest.fn();
    deleteUnsubmittedEventRegistrations = jest.fn();
    deleteUserProjects = jest.fn();
    removeUserFromTeams = jest.fn();

    userFind = jest.fn().mockReturnThis();
    userSort = jest.fn().mockReturnThis();
    userExec = jest.fn();
    userCount = jest.fn();

    const module = await Test.createTestingModule({
      providers: [
        {
          provide: getModelToken(User.name),
          useValue: {
            find: userFind,
            sort: userSort,
            skip: jest.fn().mockReturnThis(),
            limit: jest.fn().mockReturnThis(),
            deleteOne: jest.fn().mockReturnThis(),
            exec: userExec,
            countDocuments: userCount,
            clone: jest.fn().mockReturnThis(),
          },
        },
        {
          provide: getModelToken(EmailConfirmation.name),
          useValue: { updateOne: updateEmailConfirmation },
        },
        { provide: EmailService, useValue: { sendConfirmationMail } },
        { provide: ProjectsService, useValue: { deleteAllForUser: deleteUserProjects } },
        { provide: EventRegistrationsService, useValue: { deleteAllUnsubmittedRegistrations: deleteUnsubmittedEventRegistrations } },
        { provide: UsersService, useValue: { findOne: findUserById } },
        { provide: TeamsService, useValue: { removeFromAllTeams: removeUserFromTeams } },
        { provide: getConnectionToken('Database'), useValue: { transaction: async (toCall: () => Promise<void>) => await toCall() } },
        UsersApiService,
      ],
    }).compile();

    usersApiService = module.get(UsersApiService);
  });

  describe('updateSelf()', () => {
    const userDocumentMock = {
      email: 'test@entscheidungsnavi.de',
      emailConfirmed: true,
      name: 'some name',
      changePassword: jest.fn(),
      save: jest.fn(),
    };
    // Changes are made to this document
    let liveDocument: any;

    beforeEach(() => {
      liveDocument = { ...userDocumentMock };
      findUserById.mockResolvedValue(liveDocument);
      updateEmailConfirmation.mockReturnValue({ exec: jest.fn().mockResolvedValue({}) });
    });

    it('ignores empty values', async () => {
      await usersApiService.updateSelf(new Types.ObjectId(), plainToClass(UpdateUserDto, {}), 'de');

      expect(liveDocument).toStrictEqual(userDocumentMock);
      expect(userDocumentMock.save.mock.calls.length).toBe(1);
      expect(userDocumentMock.changePassword.mock.calls.length).toBe(0);
    });

    it('changes password', async () => {
      await usersApiService.updateSelf(
        new Types.ObjectId(),
        plainToClass(UpdateUserDto, { oldPassword: '12345678', password: 'abcdefgh' }),
        'de',
      );

      expect(userDocumentMock.changePassword.mock.calls.length).toBe(1);
      expect(userDocumentMock.changePassword.mock.calls[0][0]).toBe('12345678');
      expect(userDocumentMock.changePassword.mock.calls[0][1]).toBe('abcdefgh');
    });

    it('throws on missing oldPassword', async () => {
      await expect(
        usersApiService.updateSelf(new Types.ObjectId(), plainToClass(UpdateUserDto, { password: 'abcdefgh' }), 'de'),
      ).rejects.toThrow();
    });

    it('sets email', async () => {
      await usersApiService.updateSelf(new Types.ObjectId(), plainToClass(UpdateUserDto, { email: 'new@entscheidungsnavi.de' }), 'de');

      expect(liveDocument.email).toBe('new@entscheidungsnavi.de');
      expect(liveDocument.emailConfirmed).toBe(false);

      expect(updateEmailConfirmation.mock.calls.length).toBe(1);
      expect(sendConfirmationMail.mock.calls.length).toBe(1);
    });

    it('sets name', async () => {
      await usersApiService.updateSelf(new Types.ObjectId(), plainToClass(UpdateUserDto, { name: 'new name' }), 'de');

      expect(liveDocument.name).toBe('new name');
    });

    it('allows clearing the name', async () => {
      await usersApiService.updateSelf(new Types.ObjectId(), plainToClass(UpdateUserDto, { name: null }), 'de');

      expect(liveDocument.name).toBeNull();
    });
  });

  describe('findUsers()', () => {
    beforeEach(() => {
      userExec.mockResolvedValue([]);
      userCount.mockResolvedValue(0);
    });

    it('assembles the filter correctly', async () => {
      await usersApiService.findUsers({ query: 'abcd', emailConfirmed: true, roles: ['admin'] }, { column: 'name', direction: 'asc' });

      expect(userFind.mock.calls.length).toBe(1);
      expect(userFind.mock.calls[0][0]).toEqual({
        $or: [{ name: { $regex: 'abcd', $options: 'i' } }, { email: { $regex: 'abcd', $options: 'i' } }],
        roles: { $all: ['admin'] },
        emailConfirmed: true,
      });
    });

    it('returns correct values', async () => {
      const items = [{ name: 'abcd' }];
      userExec.mockResolvedValue(items);
      userCount.mockResolvedValue(1);

      const result = await usersApiService.findUsers({}, { column: 'name', direction: 'asc' });

      expect(result).toMatchObject({ items, count: 1 });
    });

    it('handles missing filters', async () => {
      await usersApiService.findUsers({}, { column: 'name', direction: 'asc' });

      expect(userFind.mock.calls[0][0]).toEqual({});
    });

    it('sorts by name descending', async () => {
      await usersApiService.findUsers({}, { column: 'name', direction: 'desc' });

      expect(userSort.mock.calls.length).toBe(2);
      expect(userSort.mock.calls[0][0]).toEqual({ name: -1 });
      expect(userSort.mock.calls[1][0]).toEqual({ _id: 1 });
    });
  });

  describe('deleteOneAdmin', () => {
    beforeEach(() => {
      deleteUnsubmittedEventRegistrations.mockResolvedValue(null);
      deleteUserProjects.mockResolvedValue(null);
      removeUserFromTeams.mockResolvedValue(true);
      userExec.mockResolvedValue({ deletedCount: 1 });
    });

    it('deletes projects, event registrations & removes user from teams', async () => {
      await usersApiService.deleteOneAdmin(new Types.ObjectId());

      expect(deleteUnsubmittedEventRegistrations.mock.calls.length).toBe(1);
      expect(deleteUserProjects.mock.calls.length).toBe(1);
      expect(removeUserFromTeams.mock.calls.length).toBe(1);
    });

    it('throw when user is owner of teams', async () => {
      removeUserFromTeams.mockResolvedValue(false);

      await expect(usersApiService.deleteOneAdmin(new Types.ObjectId())).rejects.toThrow();
    });

    it('throw when nothing was deleted', async () => {
      userExec.mockResolvedValue({ deletedCount: 0 });

      await expect(usersApiService.deleteOneAdmin(new Types.ObjectId())).rejects.toThrow();
    });
  });
});
