import { Language } from '@entscheidungsnavi/api-types';
import {
  Body,
  Controller,
  Delete,
  ForbiddenException,
  Get,
  Param,
  Patch,
  Post,
  Query,
  Req,
  UseGuards,
  UseInterceptors,
} from '@nestjs/common';
import { Types } from 'mongoose';
import { RolesGuard } from '../auth/roles.guard';
import { Lang } from '../common/language.decorator';
import { PaginationParams } from '../common/pagination-params';
import { ParseMongoIdPipe } from '../common/parse-mogo-id.pipe';
import { ProjectsService } from '../projects/projects.service';
import { NotFoundInterceptor } from '../common/not-found.interceptor';
import { AuthenticatedRequest } from '../auth/login.guard';
import { CreateUserDto } from './dto/create-user.dto';
import { UserSortDto } from './dto/user-sort.dto';
import { UsersFilterDto } from './dto/users-filter.dto';
import { UpdateUserRolesDto } from './dto/update-user-roles.dto';
import { UsersApiService } from './users-api.service';

@Controller('users')
export class UsersController {
  constructor(
    private usersApiService: UsersApiService,
    private projectsService: ProjectsService,
  ) {}

  @Post()
  async create(@Body() user: CreateUserDto, @Lang() lang: Language) {
    return await this.usersApiService.create(user, lang);
  }

  @UseGuards(RolesGuard('admin'))
  @Get()
  async list(
    @Query() filters: UsersFilterDto,
    @Query() { sortBy, sortDirection }: UserSortDto,
    @Query() { limit, offset }: PaginationParams,
  ) {
    return await this.usersApiService.findUsers(filters, { column: sortBy ?? 'name', direction: sortDirection ?? 'asc' }, limit, offset);
  }

  @UseGuards(RolesGuard('admin'))
  @UseInterceptors(NotFoundInterceptor)
  @Get(':id')
  async getOne(@Param('id', ParseMongoIdPipe) id: Types.ObjectId) {
    return await this.usersApiService.getOne(id);
  }

  @UseGuards(RolesGuard('admin'))
  @Get(':id/projects')
  async getProjects(@Param('id', ParseMongoIdPipe) id: Types.ObjectId) {
    return await this.projectsService.list(id);
  }

  /**
   * Currently, this only allows admins to modify user roles. They can not modify anything else
   * about other users.
   */
  @UseGuards(RolesGuard('admin'))
  @Patch(':id')
  async update(@Req() req: AuthenticatedRequest, @Param('id', ParseMongoIdPipe) id: Types.ObjectId, @Body() update: UpdateUserRolesDto) {
    if (req.user._id.equals(id) && !update.roles.includes('admin')) {
      // Admins can not remove their own admin role
      throw new ForbiddenException();
    }

    await this.usersApiService.updateRoles(id, update.roles);
  }

  @UseGuards(RolesGuard('admin'))
  @Delete(':id')
  async deleteOne(@Param('id', ParseMongoIdPipe) id: Types.ObjectId) {
    await this.usersApiService.deleteOneAdmin(id);
  }
}
