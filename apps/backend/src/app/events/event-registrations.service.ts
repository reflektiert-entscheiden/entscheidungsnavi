import { BadRequestException, ConflictException, Injectable, NotFoundException } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { plainToClass } from 'class-transformer';
import { ClientSession, Model, Types } from 'mongoose';
import { EventRegistration, EventRegistrationDocument } from '../schemas/events/event-registration.schema';
import { Event } from '../schemas/events/event.schema';
import { EventRegistrationDto } from './dto/event-registration.dto';
import { UpdateEventRegistrationDto } from './dto/update-event-registration.dto';
import { EventsService } from './events.service';
import { validateQuestion } from './validate-question';

function toDto(doc: Omit<EventRegistrationDocument, 'event'> & { event?: Event }) {
  return plainToClass(EventRegistrationDto, doc);
}

@Injectable()
export class EventRegistrationsService {
  constructor(
    private eventsService: EventsService,
    @InjectModel(EventRegistration.name) private eventRegistrationModel: Model<EventRegistrationDocument>,
  ) {}

  async listSubmissions(userId: Types.ObjectId) {
    const result = await this.eventRegistrationModel.find({ user: userId }, '-projectData').populate<{ event: Event }>('event').exec();

    // Remove unreleased questionnaires
    result.forEach(submission => {
      const event = submission.event;
      if (!event.questionnaireReleased) {
        event.questionnaire = null;
      }
    });

    return result.map(toDto);
  }

  async registerForEvent(userId: Types.ObjectId, code: string) {
    const event = await this.eventsService.findByCode(code);
    if (event == null) {
      throw new NotFoundException();
    }

    try {
      await this.eventRegistrationModel.create({ user: userId, event: new Types.ObjectId(event.id) });
    } catch {
      throw new ConflictException();
    }
  }

  async findOne(userId: Types.ObjectId, registrationId: Types.ObjectId) {
    return await this.eventRegistrationModel.findOne({ _id: registrationId, user: userId }).exec();
  }

  async updateSubmission(userId: Types.ObjectId, registrationId: Types.ObjectId, update: UpdateEventRegistrationDto) {
    const oldSubmission = await this.findOne(userId, registrationId);
    if (oldSubmission == null) {
      throw new NotFoundException();
    }

    if (oldSubmission.submitted) {
      throw new BadRequestException('Submission cannot be changed after it has been finalized');
    }

    const currentSubmission = await this.eventRegistrationModel
      .findOneAndUpdate<Omit<EventRegistrationDocument, 'event'>>({ _id: registrationId, user: userId }, update, {
        new: true,
        upsert: true,
        fields: '-event -projectData',
      })
      .exec();

    return toDto(currentSubmission);
  }

  async submit(userId: Types.ObjectId, registrationId: Types.ObjectId) {
    const submission = await this.findOne(userId, registrationId);
    if (submission == null) {
      throw new NotFoundException();
    }

    if (submission.submitted) {
      throw new ConflictException('This submission is already finalized');
    }

    const event = await this.eventsService.findById(submission.event as Types.ObjectId);

    // Check if we are within start and end date
    if (event.startDate && event.startDate > new Date()) {
      throw new BadRequestException('The submission period has not started yet');
    }
    if (event.endDate && event.endDate < new Date()) {
      throw new BadRequestException('The submission period has ended');
    }

    // Validate the questionnaire
    if (!event.questionnaireReleased) {
      throw new BadRequestException('Cannot submit until the questionnaire is released');
    }

    if (event.questionnaire.length !== submission.questionnaireResponses.length) {
      throw new BadRequestException('questionnaireResponses contains the wrong number of pages');
    }

    for (let pageIndex = 0; pageIndex < event.questionnaire.length; pageIndex++) {
      const page = event.questionnaire[pageIndex],
        responses = submission.questionnaireResponses[pageIndex];

      if (page.entries.length !== responses.length) {
        throw new BadRequestException(`Questionnaire page ${pageIndex + 1} has an incorrect number of entries`);
      }

      for (let questionIndex = 0; questionIndex < page.entries.length; questionIndex++) {
        const question = page.entries[questionIndex],
          response = responses[questionIndex];

        if (!validateQuestion(question, response)) {
          throw new BadRequestException(`Response for question ${questionIndex + 1} on page ${pageIndex + 1} is invalid`);
        }
      }
    }

    // Validate the free text
    if (event.freeTextConfig.enabled && event.freeTextConfig.minLength > submission.freeText?.length) {
      throw new BadRequestException('The free text does not fulfill the requirements');
    }

    // Make sure we have a project
    if (submission.projectData == null) {
      throw new BadRequestException('Can only submit with a project');
    }

    // Submit
    await this.eventRegistrationModel.updateOne({ _id: registrationId, user: userId }, { submitted: true }).exec();
  }

  /**
   * Allow the user to leave an event only if they have not submitted yet.
   */
  async leaveEvent(userId: Types.ObjectId, registrationId: Types.ObjectId) {
    const event = await this.findOne(userId, registrationId);
    if (event == null) {
      throw new NotFoundException();
    }
    if (event.submitted) {
      throw new ConflictException('an event cannot be left after submission');
    }
    await event.deleteOne();
  }

  async deleteAllUnsubmittedRegistrations(userId: Types.ObjectId, session: ClientSession) {
    const result = await this.eventRegistrationModel.deleteMany({ user: userId, submitted: false }, { session }).exec();
    return result.deletedCount;
  }
}
