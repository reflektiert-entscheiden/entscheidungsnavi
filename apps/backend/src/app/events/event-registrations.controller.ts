import { Body, Controller, Delete, Get, Param, Patch, Post, Req, UseGuards } from '@nestjs/common';
import { Types } from 'mongoose';
import { ParseMongoIdPipe } from '../common/parse-mogo-id.pipe';
import { AuthenticatedRequest, LoginGuard } from '../auth/login.guard';
import { JoinEventDto } from './dto/join-event.dto';
import { UpdateEventRegistrationDto } from './dto/update-event-registration.dto';
import { EventRegistrationsService } from './event-registrations.service';

@UseGuards(LoginGuard)
@Controller('user/event-registrations')
export class EventRegistrationsController {
  constructor(private eventRegistrationsService: EventRegistrationsService) {}

  @Get()
  async listEvents(@Req() req: AuthenticatedRequest) {
    return await this.eventRegistrationsService.listSubmissions(req.user._id);
  }

  @Post()
  async joinEvent(@Req() req: AuthenticatedRequest, @Body() body: JoinEventDto) {
    await this.eventRegistrationsService.registerForEvent(req.user._id, body.code);
  }

  @Get(':id')
  async getOneEvent(@Req() req: AuthenticatedRequest, @Param('id', ParseMongoIdPipe) registrationId: Types.ObjectId) {
    return await this.eventRegistrationsService.findOne(req.user._id, registrationId);
  }

  @Patch(':id')
  async updateEvent(
    @Req() req: AuthenticatedRequest,
    @Param('id', ParseMongoIdPipe) id: Types.ObjectId,
    @Body() update: UpdateEventRegistrationDto,
  ) {
    return await this.eventRegistrationsService.updateSubmission(req.user._id, id, update);
  }

  @Post(':id/submit')
  async submit(@Req() req: AuthenticatedRequest, @Param('id', ParseMongoIdPipe) registrationId: Types.ObjectId) {
    await this.eventRegistrationsService.submit(req.user._id, registrationId);
  }

  @Delete(':id')
  async leaveEvent(@Req() req: AuthenticatedRequest, @Param('id', ParseMongoIdPipe) registrationId: Types.ObjectId) {
    await this.eventRegistrationsService.leaveEvent(req.user._id, registrationId);
  }
}
