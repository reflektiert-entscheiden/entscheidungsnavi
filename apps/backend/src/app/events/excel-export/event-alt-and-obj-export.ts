import { Workbook, Worksheet } from 'exceljs';
import { max } from 'lodash';
import { EventAltAndObjExportAlternativesAttribute, EventAltAndObjExportObjectivesAttribute } from '@entscheidungsnavi/api-types';
import {
  applyWeightsToUtilityMatrix,
  getAlternativeUtilities,
  getIndicatorUtilityFunction,
  getNumericalUtilityFunction,
  getOutcomeUtilityFunction,
  getVerbalUtilityFunction,
  iterateUserDefinedScenarios,
  Objective,
  ObjectiveType,
  SimpleUserDefinedInfluenceFactor,
} from '@entscheidungsnavi/decision-data';
import { assertUnreachable } from '@entscheidungsnavi/tools/assert-unreachable';
import { normalizeDiscrete } from '@entscheidungsnavi/tools';
import { TableBuilder } from '../../common/excel/table-builder';
import { EventDocument } from '../../schemas/events/event.schema';
import { EventSubmission } from './event-columns';
import { createErrorsWorksheet, createMetadataWorksheet, loadProjectData, PopulatedEventRegistration } from './event-export-helper';

/**
 * Generate an Alternatives/Objectives Excel Export of the corresponding event submissions.
 *
 * @param eventSubmissions - All submissions that are part of this export.
 *                           Only submitted registrations must be included. `user` must be populated.
 * @param alternativeAttributes - Settings describing the columns in the Alternatives Sheet.
 * @param objectiveAttributes - Settings describing the columns in the Objectives Sheet.
 * @returns The Excel workbook
 */
export async function createEventAltAndObjExport(
  events: EventDocument[],
  eventSubmissions: PopulatedEventRegistration[],
  alternativeAttributes: EventAltAndObjExportAlternativesAttribute[],
  objectiveAttributes: EventAltAndObjExportObjectivesAttribute[],
) {
  // Create the workbook
  const wb = new Workbook();
  wb.created = new Date();

  // Import all decision data objects
  const [loadedProjectSubmissions, importErrors] = loadProjectData(eventSubmissions);

  // Create a worksheet with the errors, if applicable
  if (importErrors.length > 0) {
    createErrorsWorksheet(wb, importErrors);
  }

  createMetadataWorksheet(
    wb,
    events,
    eventSubmissions,
    null,
    [].concat(alternativeAttributes).concat(objectiveAttributes),
    importErrors.length,
  );

  if (alternativeAttributes.includes('alternativesSheet')) {
    const alternativesSheet = wb.addWorksheet('Alternativen', { properties: { defaultColWidth: 15 } });
    createAlternativeExportWorksheet(alternativesSheet, loadedProjectSubmissions, alternativeAttributes);
  }

  if (objectiveAttributes.includes('objectivesSheet')) {
    const objectivesSheet = wb.addWorksheet('Ziele', { properties: { defaultColWidth: 15 } });
    createObjectiveExportWorksheet(objectivesSheet, loadedProjectSubmissions, objectiveAttributes);
  }

  return wb;
}

type ExtendedEventSubmission = EventSubmission & { submissionIndex: number; rowIndex: number };

function createObjectiveExportWorksheet(
  ws: Worksheet,
  eventSubmissions: EventSubmission[],
  objectiveAttributes: EventAltAndObjExportObjectivesAttribute[],
) {
  const tableBuilder = new TableBuilder<ExtendedEventSubmission>(ws);

  const eventIds = eventSubmissions.map(eventSubmission =>
    [eventSubmission.id].concat(new Array(eventSubmission.projectData.objectives.length - 1).fill('')),
  );
  const userIds = eventSubmissions.map(eventSubmission =>
    [eventSubmission.user?.id ?? 'Benutzer gelöscht'].concat(new Array(eventSubmission.projectData.objectives.length - 1).fill('')),
  );
  const objectiveCounts = eventSubmissions.map(eventSubmission =>
    [eventSubmission.projectData.objectives.length].concat(new Array(eventSubmission.projectData.objectives.length - 1).fill('')),
  );
  const objectiveIndices = eventSubmissions.map(eventSubmission => eventSubmission.projectData.objectives.map((_, i) => i + 1));
  const objectiveNames = eventSubmissions.map(eventSubmission => eventSubmission.projectData.objectives.map(objective => objective.name));

  // default columns (always there)
  tableBuilder.addGroup('Standardinformationen', [
    { name: 'Veranstaltung ID', data: data => eventIds[data.submissionIndex][data.rowIndex] },
    { name: 'Benutzer ID', data: data => userIds[data.submissionIndex][data.rowIndex] },
    { name: 'Anzahl', data: data => objectiveCounts[data.submissionIndex][data.rowIndex] },
    { name: 'Nr.', data: data => objectiveIndices[data.submissionIndex][data.rowIndex] },
    { name: 'Name', data: data => objectiveNames[data.submissionIndex][data.rowIndex] },
  ]);

  // optional columns (depending on objectiveAttributes)
  if (objectiveAttributes.includes('scaleInfo')) {
    const scaleValues = eventSubmissions.map(eventSubmission => {
      return eventSubmission.projectData.objectives.map(objective => {
        return getObjectiveTypeAndLimits(objective);
      });
    });
    tableBuilder.addGroup('Skaleninformationen', [
      { name: 'Typ (v/n/i)', data: data => scaleValues[data.submissionIndex][data.rowIndex].type },
      { name: 'von', data: data => scaleValues[data.submissionIndex][data.rowIndex].from },
      { name: 'bis', data: data => scaleValues[data.submissionIndex][data.rowIndex].to },
      { name: 'Einheit', data: data => scaleValues[data.submissionIndex][data.rowIndex].unit },
      { name: 'Indikatorformel', data: data => scaleValues[data.submissionIndex][data.rowIndex].formula },
    ]);
  }

  if (objectiveAttributes.includes('weightInfo')) {
    const weightValues = eventSubmissions.map(eventSubmission => {
      return eventSubmission.projectData.objectives.map((_, objectiveIdx) => {
        return [
          eventSubmission.projectData.weights.tradeoffObjectiveIdx === objectiveIdx ? 'x' : '',
          eventSubmission.projectData.weights.getWeightValues()[objectiveIdx],
          normalizeDiscrete(eventSubmission.projectData.weights.getWeightValues(), 1, 0.0000000001)[objectiveIdx],
          eventSubmission.projectData.weights.getWeights()[objectiveIdx].precision,
        ];
      });
    });
    tableBuilder.addGroup('Zielgewichtinformationen', [
      { name: 'Vergleichsziel Zielgewichtung', data: data => weightValues[data.submissionIndex][data.rowIndex][0] },
      { name: 'Zielgewicht', data: data => weightValues[data.submissionIndex][data.rowIndex][1] },
      { name: 'Zielgewicht normiert', data: data => weightValues[data.submissionIndex][data.rowIndex][2] },
      { name: 'Präzision Zielgewicht', data: data => weightValues[data.submissionIndex][data.rowIndex][3] },
    ]);
  }

  if (objectiveAttributes.includes('utilityInfo')) {
    const maxNumberOfOptions = max(
      eventSubmissions.map(submission => max(submission.projectData.objectives.map(objective => objective.verbalData.options.length ?? 0))),
    );
    const utilityValues = eventSubmissions.map(eventSubmission => {
      return eventSubmission.projectData.objectives.map(objective => {
        const precisions = getPrecisions(objective);
        const result = [getC(objective), precisions.c, precisions.verbal];
        if (objective.isVerbal) {
          result.push(objective.verbalData.options.length);
          objective.verbalData.utilities.forEach((utility, utilityIdx) => {
            result.push(objective.verbalData.options[utilityIdx]);
            result.push(utility);
          });
        }
        return result;
      });
    });
    const columns = [
      { name: 'c', data: (data: ExtendedEventSubmission) => utilityValues[data.submissionIndex][data.rowIndex][0] },
      { name: 'Präzision c', data: (data: ExtendedEventSubmission) => utilityValues[data.submissionIndex][data.rowIndex][1] },
      { name: 'Präzision verbal', data: (data: ExtendedEventSubmission) => utilityValues[data.submissionIndex][data.rowIndex][2] },
      { name: 'Anzahl Ausprägungen', data: (data: ExtendedEventSubmission) => utilityValues[data.submissionIndex][data.rowIndex][3] },
    ];
    for (let optionIdx = 0; optionIdx < maxNumberOfOptions; optionIdx++) {
      columns.push({
        name: 'Ausprägung ' + (optionIdx + 1) + ' Name',
        data: (data: ExtendedEventSubmission) => {
          const currentColumnIndex = 4 + optionIdx * 2;
          return utilityValues[data.submissionIndex][data.rowIndex][currentColumnIndex] ?? '';
        },
      });
      columns.push({
        name: 'Ausprägung ' + (optionIdx + 1) + ' Nutzen',
        data: (data: ExtendedEventSubmission) => {
          const currentColumnIndex = 5 + optionIdx * 2;
          return utilityValues[data.submissionIndex][data.rowIndex][currentColumnIndex] ?? '';
        },
      });
    }
    tableBuilder.addGroup('Nutzenfunktioninformationen', columns);
  }

  return tableBuilder.buildTable(
    'Ziele',
    eventSubmissions.flatMap((eventSubmission, submissionIndex) =>
      eventSubmission.projectData.objectives.map((_, rowIndex) => ({ ...eventSubmission, submissionIndex, rowIndex })),
    ),
    false,
  );
}

function createAlternativeExportWorksheet(
  ws: Worksheet,
  eventSubmissions: EventSubmission[],
  alternativeAttributes: EventAltAndObjExportAlternativesAttribute[],
) {
  const tableBuilder = new TableBuilder<ExtendedEventSubmission>(ws);

  const eventIds = eventSubmissions.map(eventSubmission =>
    [eventSubmission.id].concat(new Array(eventSubmission.projectData.alternatives.length - 1).fill('')),
  );
  const userIds = eventSubmissions.map(eventSubmission =>
    [eventSubmission.user?.id ?? 'Benutzer gelöscht'].concat(new Array(eventSubmission.projectData.alternatives.length - 1).fill('')),
  );
  const alternativeCounts = eventSubmissions.map(eventSubmission =>
    [eventSubmission.projectData.alternatives.length].concat(new Array(eventSubmission.projectData.alternatives.length - 1).fill('')),
  );
  const alternativeIndices = eventSubmissions.map(eventSubmission => eventSubmission.projectData.alternatives.map((_, i) => i + 1));
  const alternativeNames = eventSubmissions.map(eventSubmission =>
    eventSubmission.projectData.alternatives.map(alternative => alternative.name),
  );

  // default columns (always there)
  tableBuilder.addGroup('Standardinformationen', [
    { name: 'Veranstaltung ID', data: data => eventIds[data.submissionIndex][data.rowIndex] },
    { name: 'Benutzer ID', data: data => userIds[data.submissionIndex][data.rowIndex] },
    { name: 'Anzahl', data: data => alternativeCounts[data.submissionIndex][data.rowIndex] },
    { name: 'Nr.', data: data => alternativeIndices[data.submissionIndex][data.rowIndex] },
    { name: 'Name', data: data => alternativeNames[data.submissionIndex][data.rowIndex] },
  ]);

  // optional columns (depending on alternativeAttributes)
  if (alternativeAttributes.includes('defaultAnalysis')) {
    const analysisValues = eventSubmissions.map(eventSubmission => {
      const c = getCs(eventSubmission.projectData.objectives);
      const utilityVerbal = getUtilityVerbal(eventSubmission.projectData.objectives);
      return setRankingsWithUtilities(eventSubmission, c, utilityVerbal);
    });

    tableBuilder.addGroup('Standardauswertung', [
      { name: 'Rang', data: data => analysisValues[data.submissionIndex][data.rowIndex][0] + 1 },
      { name: 'Nutzenerwartungswert', data: data => analysisValues[data.submissionIndex][data.rowIndex][1] },
    ]);
  }

  if (alternativeAttributes.includes('linearNumericalAnalysis')) {
    const analysisValues = eventSubmissions.map(eventSubmission => {
      const c = new Array(eventSubmission.projectData.objectives.length).fill(0);
      const utilityVerbal = getUtilityVerbal(eventSubmission.projectData.objectives);
      return setRankingsWithUtilities(eventSubmission, c, utilityVerbal);
    });

    tableBuilder.addGroup('Auswertung c=0', [
      { name: 'Rang c=0', data: data => analysisValues[data.submissionIndex][data.rowIndex][0] + 1 },
      { name: 'Nutzenerwartungswert c=0', data: data => analysisValues[data.submissionIndex][data.rowIndex][1] },
    ]);
  }

  if (alternativeAttributes.includes('linearVerbalAnalysis')) {
    const analysisValues = eventSubmissions.map(eventSubmission => {
      const c = getCs(eventSubmission.projectData.objectives);
      const utilityVerbal = getLinearUtilityVerbal(eventSubmission.projectData.objectives);
      return setRankingsWithUtilities(eventSubmission, c, utilityVerbal);
    });

    tableBuilder.addGroup('Auswertung linear (verbale Skalen)', [
      { name: 'Rang linear', data: data => analysisValues[data.submissionIndex][data.rowIndex][0] + 1 },
      { name: 'Nutzenerwartungswert linear', data: data => analysisValues[data.submissionIndex][data.rowIndex][1] },
    ]);
  }

  if (alternativeAttributes.includes('linearBothAnalysis')) {
    const analysisValues = eventSubmissions.map(eventSubmission => {
      const c = new Array(eventSubmission.projectData.objectives.length).fill(0);
      const utilityVerbal = getLinearUtilityVerbal(eventSubmission.projectData.objectives);
      return setRankingsWithUtilities(eventSubmission, c, utilityVerbal);
    });

    tableBuilder.addGroup('Auswertung c=0 und linear', [
      { name: 'Rang c=0 und linear', data: data => analysisValues[data.submissionIndex][data.rowIndex][0] + 1 },
      { name: 'Nutzenerwartungswert c=0 und linear', data: data => analysisValues[data.submissionIndex][data.rowIndex][1] },
    ]);
  }

  if (alternativeAttributes.includes('linearMostImportantAnalyses')) {
    tableBuilder.addGroup('Auswertung (das n-te wichtigste Ziel ist linear)', setImportantRankingColumns(eventSubmissions, false, false));
  }

  if (alternativeAttributes.includes('linearMostImportantAccumulatedAnalyses')) {
    tableBuilder.addGroup(
      'Auswertung (die ersten n wichtigsten Ziele sind linear)',
      setImportantRankingColumns(eventSubmissions, true, false),
    );
  }

  if (alternativeAttributes.includes('linearLeastImportantAnalyses')) {
    tableBuilder.addGroup('Auswertung (das n-te unwichtigste Ziel ist linear)', setImportantRankingColumns(eventSubmissions, false, true));
  }

  if (alternativeAttributes.includes('linearLeastImportantAccumulatedAnalyses')) {
    tableBuilder.addGroup(
      'Auswertung (die ersten n unwichtigsten Ziele sind linear)',
      setImportantRankingColumns(eventSubmissions, true, true),
    );
  }

  return tableBuilder.buildTable(
    'Alternativen',
    eventSubmissions.flatMap((eventSubmission, submissionIndex) =>
      eventSubmission.projectData.alternatives.map((_, rowIndex) => ({ ...eventSubmission, submissionIndex, rowIndex })),
    ),
    false,
  );
}

// ascOrDesc = 1 means ascending order, ascOrDesc = -1 means descending order
function setImportantRankingColumns(eventSubmissions: EventSubmission[], isAccumulated: boolean, isAsc: boolean) {
  const ascOrDesc = isAsc ? 1 : -1;
  const importantWord = isAsc ? 'unwichtig' : 'wichtig';
  const importantObjective = isAccumulated ? importantWord + 'e Ziele' : importantWord + 'es Ziel';
  let maxObjectives = 0;
  const analysisValues = eventSubmissions.map(eventSubmission => {
    const objectives = eventSubmission.projectData.objectives;
    const weightsAsc = eventSubmission.projectData.weights.getWeights().map((w, objectiveIdx) => {
      return { objectiveIdx, value: w.value };
    });
    weightsAsc.sort((a, b) => {
      if (a.value > b.value) {
        return ascOrDesc;
      } else if (a.value < b.value) {
        return -ascOrDesc;
      } else {
        return 0;
      }
    });
    const analysisValuesColumn = [];

    maxObjectives = Math.max(maxObjectives, objectives.length);
    for (let importanceIdx = 0; importanceIdx < objectives.length; importanceIdx++) {
      const c = getCs(objectives);
      const utilityVerbal = getUtilityVerbal(objectives);
      // loop goes from 0 to objectiveIdx (isAccumulated = true) or executes exactly once (isAccumulated = false)
      for (let currentImportanceIdx = isAccumulated ? 0 : importanceIdx; currentImportanceIdx < importanceIdx + 1; currentImportanceIdx++) {
        // linearize least important objective
        const currentObjectiveIdx = weightsAsc[currentImportanceIdx].objectiveIdx;
        if (objectives[currentObjectiveIdx].isVerbal) {
          utilityVerbal[currentObjectiveIdx] = objectives[currentObjectiveIdx].verbalData.options.map(
            (_, idx) => idx * (100 / (objectives[currentObjectiveIdx].verbalData.options.length - 1)),
          );
        } else {
          c[currentObjectiveIdx] = 0;
        }
      }
      analysisValuesColumn.push(setRankingsWithUtilities(eventSubmission, c, utilityVerbal));
    }
    return analysisValuesColumn;
  });

  const columns = [];
  const pointForNonAccumulated = isAccumulated ? '' : '.';
  for (let objectiveIdx = 0; objectiveIdx < maxObjectives; objectiveIdx++) {
    columns.push({
      name: 'Rang ' + (objectiveIdx + 1) + pointForNonAccumulated + ' ' + importantObjective + ' linear',
      data: (data: ExtendedEventSubmission) => {
        if (analysisValues[data.submissionIndex][objectiveIdx] == null) {
          return '';
        }
        return analysisValues[data.submissionIndex][objectiveIdx][data.rowIndex][0] + 1;
      },
    });
    columns.push({
      name: 'NEW ' + (objectiveIdx + 1) + pointForNonAccumulated + ' ' + importantObjective + ' linear',
      data: (data: ExtendedEventSubmission) => {
        if (analysisValues[data.submissionIndex][objectiveIdx] == null) {
          return '';
        }
        return analysisValues[data.submissionIndex][objectiveIdx][data.rowIndex][1];
      },
    });
  }
  return columns;
}

function getObjectiveTypeAndLimits(objective: Objective) {
  if (objective.isNumerical) {
    return {
      type: 'n',
      from: objective.numericalData.from,
      to: objective.numericalData.to,
      unit: objective.numericalData.unit,
      formula: '',
    };
  } else if (objective.isVerbal) {
    return {
      type: 'v',
      from: objective.verbalData.options[0],
      to: objective.verbalData.options[objective.verbalData.options.length - 1],
      unit: '',
      formula: '',
    };
  } else {
    return {
      type: 'i',
      from: objective.indicatorData.worstValue,
      to: objective.indicatorData.bestValue,
      unit: objective.indicatorData.aggregatedUnit,
      formula: objective.indicatorData.useCustomAggregation ? objective.indicatorData.customAggregationFormula : 'additive Formel',
    };
  }
}

function setRankingsWithUtilities(submission: EventSubmission, c: number[], utilityVerbal: number[][]) {
  const analysisValues = computeUtilities(submission, c, utilityVerbal).map((utilitySum, objectiveIdx) => {
    return [objectiveIdx, utilitySum];
  });
  const analysisValuesAsc = [...analysisValues].sort((a, b) => Math.sign(b[1] - a[1]));
  const rankingsWithUtilities: [number, number][] = analysisValues.map((analysisValue, objectiveIdx) => {
    return [analysisValuesAsc.map(row => row[0]).indexOf(objectiveIdx), analysisValue[1]];
  });
  return rankingsWithUtilities;
}

function computeUtilities(submission: EventSubmission, c: number[], utilityVerbal: number[][]) {
  const objectiveUf = submission.projectData.objectives.map((objective, objectiveIdx) => {
    switch (objective.objectiveType) {
      case ObjectiveType.Numerical: {
        return getNumericalUtilityFunction(c[objectiveIdx], objective.numericalData.from, objective.numericalData.to);
      }
      case ObjectiveType.Indicator: {
        const aggregationFunction = objective.indicatorData.aggregationFunction;

        const automaticLimits = objective.indicatorData.useCustomAggregation && objective.indicatorData.automaticCustomAggregationLimits;
        const worst = automaticLimits
          ? aggregationFunction(objective.indicatorData.indicators.map(indicator => [indicator.min]))
          : objective.indicatorData.defaultAggregationWorst;
        const best = automaticLimits
          ? aggregationFunction(objective.indicatorData.indicators.map(indicator => [indicator.max]))
          : objective.indicatorData.defaultAggregationBest;

        return getIndicatorUtilityFunction(c[objectiveIdx], aggregationFunction, worst, best);
      }
      case ObjectiveType.Verbal: {
        return getVerbalUtilityFunction(utilityVerbal[objectiveIdx]);
      }
      default:
        assertUnreachable(objective.objectiveType);
    }
  });

  const outcomeUf = submission.projectData.outcomes.map(ocsForAlternative =>
    ocsForAlternative.map((outcome, objectiveIndex) => {
      if (outcome.influenceFactor instanceof SimpleUserDefinedInfluenceFactor) {
        const probabilities = outcome.influenceFactor.states.map(s => s.probability / 100);
        return getOutcomeUtilityFunction(objectiveUf[objectiveIndex], (callback, outcomeValues) =>
          iterateUserDefinedScenarios(outcomeValues, probabilities, callback),
        );
      } else {
        return getOutcomeUtilityFunction(objectiveUf[objectiveIndex], (callback, outcomeValues) =>
          outcome.influenceFactor.iterateScenarios(outcomeValues, callback, true),
        );
      }
    }),
  );

  const weightedUtilityMatrix = submission.projectData.alternatives.map((_, alternativeIndex) =>
    submission.projectData.outcomes[alternativeIndex].map((outcome, objectiveIndex) => {
      return outcomeUf[alternativeIndex][objectiveIndex](outcome.values);
    }),
  );

  applyWeightsToUtilityMatrix(weightedUtilityMatrix, submission.projectData.weights.getWeightValues());

  return getAlternativeUtilities(weightedUtilityMatrix);
}

function getCs(objectives: Objective[]) {
  return objectives.map(objective => getC(objective));
}

function getC(objective: Objective) {
  if (objective.isNumerical) {
    return objective.numericalData.utilityfunction.c;
  }
  if (objective.isIndicator) {
    return objective.indicatorData.utilityfunction.c;
  }
  return null;
}

function getPrecisions(objective: Objective) {
  if (objective.isNumerical) {
    return { c: objective.numericalData.utilityfunction.precision, verbal: '' };
  }
  if (objective.isVerbal) {
    return { c: '', verbal: objective.verbalData.precision };
  }
  if (objective.isIndicator) {
    return { c: objective.indicatorData.utilityfunction.precision, verbal: '' };
  }
  return null;
}

function getUtilityVerbal(objectives: Objective[]) {
  return objectives.map(objective => (objective.isVerbal ? objective.verbalData.utilities.slice() : null));
}

function getLinearUtilityVerbal(objectives: Objective[]) {
  return objectives.map(objective => {
    if (!objective.isVerbal) return null;
    return objective.verbalData.options.map((_, idx) => idx * (100 / (objective.verbalData.options.length - 1)));
  });
}
