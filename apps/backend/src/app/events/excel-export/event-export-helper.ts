import { Workbook } from 'exceljs';
import { Types } from 'mongoose';
import { readTextWithMetadata } from '@entscheidungsnavi/decision-data/export';
import { EventAltAndObjExportAlternativesAttribute, EventAltAndObjExportObjectivesAttribute } from '@entscheidungsnavi/api-types';
import { EventRegistrationDocument } from '../../schemas/events/event-registration.schema';
import { User } from '../../schemas/user.schema';
import { EventDocument } from '../../schemas/events/event.schema';
import { EventSubmission } from './event-columns';
import { EventExcelExportParameters } from './event-default-export';

interface ImportError {
  submissionId: string;
  userId: string;
  userEmail: string;
  errorMessage: string;
}

// Event submissions with populated user
export type PopulatedEventRegistration = Omit<EventRegistrationDocument, 'user' | 'event'> & { user: User; event: Types.ObjectId };

export function createMetadataWorksheet(
  wb: Workbook,
  events: EventDocument[],
  eventSubmissions: PopulatedEventRegistration[],
  standardParameters: EventExcelExportParameters,
  altAndObjParameters: (EventAltAndObjExportAlternativesAttribute | EventAltAndObjExportObjectivesAttribute)[],
  importErrorCount: number,
) {
  const metaWs = wb.addWorksheet('Metadaten', { properties: { defaultColWidth: 20 } });
  metaWs.addRow(['Entscheidungsnavi Export Metadaten']);
  metaWs.getRow(1).font = { bold: true };
  metaWs.addRow(['Exportdatum', new Date()]);
  events.forEach((event, index) =>
    metaWs.addRow([
      index === 0 ? 'Veranstaltungen' : '',
      event.name,
      event.id,
      eventSubmissions.filter(submission => submission.event.equals(event._id)).length,
    ]),
  );
  metaWs.addRow(['Anzahl Abgaben', '', '', eventSubmissions.length]);
  if (standardParameters != null) {
    metaWs.addRow([
      'Exportierte Attribute',
      standardParameters.attributes.length === 0 ? 'Keine' : standardParameters.attributes.join(', '),
    ]);
    metaWs.addRow(['Limit Anzahl Alternativen', standardParameters.alternativeLimit ?? 'dynamisch']);
    metaWs.addRow(['Limit Anzahl Ziele', standardParameters.objectiveLimit ?? 'dynamisch']);
    metaWs.addRow(['Limit Anzahl Einflussfaktoren', standardParameters.influenceFactorLimit ?? 'dynamisch']);
  } else if (altAndObjParameters != null) {
    metaWs.addRow(['Exportierte Attribute', altAndObjParameters.length === 0 ? 'Keine' : altAndObjParameters.join(', ')]);
  }
  metaWs.addRow(['Anzahl Importfehler', importErrorCount]);
  return metaWs;
}

export function createErrorsWorksheet(wb: Workbook, importErrors: ImportError[]) {
  const errorWs = wb.addWorksheet('Importfehler', { properties: { defaultColWidth: 20 } });
  errorWs.addRow(['Importfehler']);
  errorWs.getRow(1).font = { bold: true };
  errorWs.columns = [
    { header: 'Abgabe ID', key: 'submissionId' },
    { header: 'Benutzer ID', key: 'userId' },
    { header: 'Benutzer Email', key: 'userEmail' },
    { header: 'Fehlermeldung', key: 'errorMessage' },
  ];
  errorWs.addRows(importErrors);
  return errorWs;
}

export function loadProjectData(eventSubmissions: PopulatedEventRegistration[]) {
  const importErrors: ImportError[] = [];
  const loadedEventSubmissions: EventSubmission[] = [];

  eventSubmissions.forEach(submission => {
    try {
      const { data: projectData, projectName } = readTextWithMetadata(submission.projectData);
      // We convert the documents to plain objects and load project data
      loadedEventSubmissions.push({
        ...submission,
        projectData,
        projectName,
      });
    } catch (e) {
      importErrors.push({
        submissionId: submission.id,
        userId: submission.user?.id,
        userEmail: submission.user?.email,
        errorMessage: e instanceof Error ? e.message : String(e),
      });
    }
  });

  return [loadedEventSubmissions, importErrors] as const;
}
