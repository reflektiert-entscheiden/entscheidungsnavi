import { ConfigService } from '@nestjs/config';
import { getModelToken } from '@nestjs/mongoose';
import { Test } from '@nestjs/testing';
import { Types } from 'mongoose';
import { EventRegistration } from '../schemas/events/event-registration.schema';
import { Event } from '../schemas/events/event.schema';
import { UsersService } from '../users/users.service';
import { EventsService } from './events.service';

describe('EventsService', () => {
  let eventsService: EventsService;
  let eventsExec: jest.Mock;
  let eventsDeleteOne: jest.Mock;
  let submissionsDeleteMany: jest.Mock;
  let findUserByEmail: jest.Mock;
  let findManyUsersByEmail: jest.Mock;

  beforeEach(async () => {
    eventsExec = jest.fn();
    eventsDeleteOne = jest.fn();
    submissionsDeleteMany = jest.fn();
    findUserByEmail = jest.fn();
    findManyUsersByEmail = jest.fn();

    const module = await Test.createTestingModule({
      providers: [
        {
          provide: getModelToken(Event.name),
          useValue: {
            deleteOne: eventsDeleteOne,
            findOne: jest.fn().mockReturnThis(),
            populate: jest.fn().mockReturnThis(),
            save: jest.fn().mockReturnThis(),
            exec: eventsExec,
            updateMany: jest.fn().mockReturnThis(),
          },
        },
        {
          provide: getModelToken(EventRegistration.name),
          useValue: { deleteMany: submissionsDeleteMany, exec: jest.fn().mockResolvedValue(null) },
        },
        {
          provide: ConfigService,
          useValue: { get: jest.fn().mockReturnValue('') },
        },
        {
          provide: UsersService,
          useValue: {
            findByEmail: findUserByEmail,
            findManyByEmail: findManyUsersByEmail,
          },
        },
        EventsService,
      ],
    }).compile();

    eventsService = module.get(EventsService);
  });

  describe('update()', () => {
    it('accepts an update from the owner', async () => {
      const ownerId = new Types.ObjectId();

      eventsExec.mockResolvedValue({
        questionnaireReleased: false,
        freeTextConfig: {},
        owner: ownerId,
        editors: [],
        viewers: [],
        save: jest.fn().mockReturnThis(),
        populate: jest.fn().mockReturnThis(),
      });

      const result = await eventsService.update(ownerId, new Types.ObjectId(), { startDate: new Date() });
      expect(result).toBeTruthy();
    });

    it('accepts an update from an editor', async () => {
      const editorId = new Types.ObjectId();

      eventsExec.mockResolvedValue({
        questionnaireReleased: false,
        freeTextConfig: {},
        owner: new Types.ObjectId(),
        editors: [editorId],
        viewers: [],
        save: jest.fn().mockReturnThis(),
        populate: jest.fn().mockReturnThis(),
      });

      const result = await eventsService.update(editorId, new Types.ObjectId(), { startDate: new Date() });
      expect(result).toBeTruthy();
    });

    it('throws when the user is listed only as a viewer', async () => {
      const viewerId = new Types.ObjectId();

      eventsExec.mockResolvedValue({
        questionnaireReleased: false,
        freeTextConfig: {},
        owner: new Types.ObjectId(),
        editors: [],
        viewers: [viewerId],
        save: jest.fn().mockReturnThis(),
        populate: jest.fn().mockReturnThis(),
      });

      await expect(eventsService.update(new Types.ObjectId(), new Types.ObjectId(), { questionnaire: [] })).rejects.toThrow();
    });

    it('throws when the user has no permission', async () => {
      eventsExec.mockResolvedValue({
        questionnaireReleased: false,
        freeTextConfig: {},
        owner: new Types.ObjectId(),
        editors: [],
        viewers: [],
        save: jest.fn().mockReturnThis(),
        populate: jest.fn().mockReturnThis(),
      });

      await expect(eventsService.update(new Types.ObjectId(), new Types.ObjectId(), { questionnaire: [] })).rejects.toThrow();
    });

    it('throws when an editor tries to modify permissions', async () => {
      const editorId = new Types.ObjectId();

      eventsExec.mockResolvedValue({
        questionnaireReleased: false,
        freeTextConfig: {},
        owner: new Types.ObjectId(),
        editors: [editorId],
        viewers: [],
        save: jest.fn().mockReturnThis(),
        populate: jest.fn().mockReturnThis(),
      });
      findUserByEmail.mockResolvedValue({ id: new Types.ObjectId() });
      findManyUsersByEmail.mockResolvedValue([{ id: new Types.ObjectId() }]);

      await expect(
        eventsService.update(editorId, new Types.ObjectId(), { owner: { email: 'test@entscheidungsnavi.de' } }),
      ).rejects.toThrow();
      await expect(
        eventsService.update(editorId, new Types.ObjectId(), { editors: [{ email: 'test@entscheidungsnavi.de' }] }),
      ).rejects.toThrow();
    });

    it('throws when a released questionnaire is modified', async () => {
      eventsExec.mockResolvedValue({
        questionnaireReleased: true,
        freeTextConfig: {},
        save: jest.fn().mockReturnThis(),
        populate: jest.fn().mockReturnThis(),
      });

      await expect(eventsService.update(new Types.ObjectId(), new Types.ObjectId(), { questionnaire: [] })).rejects.toThrow();
    });
  });

  describe('deleteOne()', () => {
    beforeEach(() => {
      eventsExec.mockResolvedValue({ deletedCount: 1, acknowledged: true });
      submissionsDeleteMany.mockReturnThis();
      eventsDeleteOne.mockReturnThis();
    });

    it('deletes events and submissions', async () => {
      await eventsService.deleteOne(new Types.ObjectId(), new Types.ObjectId());

      expect(submissionsDeleteMany).toBeCalled();
      expect(eventsDeleteOne).toBeCalled();
    });
  });
});
