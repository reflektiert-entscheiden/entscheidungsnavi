import {
  EventAltAndObjExportRequest,
  EventDefaultExportRequest,
  EventProjectsExportRequest,
  EventRegistrationSortBy,
} from '@entscheidungsnavi/api-types';
import { BadRequestException, ForbiddenException, Injectable, NotFoundException } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { InjectModel } from '@nestjs/mongoose';
import { ClassConstructor, plainToClass } from 'class-transformer';
import { uniq } from 'lodash';
import { FilterQuery, Model, PassportLocalDocument, Types } from 'mongoose';
import { SortDirection } from '@angular/material/sort';
import { EventRegistration, EventRegistrationDocument } from '../schemas/events/event-registration.schema';
import { Event, EventDocument } from '../schemas/events/event.schema';
import { User, UserDocument } from '../schemas/user.schema';
import { UsersService } from '../users/users.service';
import { CreateEventDto } from './dto/create-event.dto';
import { EventDto, EventPermissionDto, EventWithStatsDto } from './dto/event.dto';
import { UpdateEventDto } from './dto/update-event.dto';
import { createDefaultExport } from './excel-export/event-default-export';
import { createProjectsExport } from './projects-export/projects-export';
import { createEventAltAndObjExport } from './excel-export/event-alt-and-obj-export';
import { EventRegistrationDto } from './dto/event-registration.dto';
import { EventRegistrationListDto } from './dto/event-registration-list.dto';
import { EventRegistrationFilterDto } from './dto/event-registration-filter.dto';

function toDto(
  doc: Omit<EventDocument, 'owner' | 'editors' | 'viewers'> & {
    owner: Pick<User, 'email'>;
    editors: Pick<User, 'email'>[];
    viewers: Pick<User, 'email'>[];
  },
  target: ClassConstructor<EventDto> | ClassConstructor<EventWithStatsDto> = EventDto,
) {
  return plainToClass(target, doc);
}

@Injectable()
export class EventsService {
  private baseUrl: string;

  constructor(
    configService: ConfigService,
    private usersService: UsersService,
    @InjectModel(Event.name) private eventModel: Model<EventDocument>,
    @InjectModel(EventRegistration.name) private eventRegistrationModel: Model<EventRegistrationDocument>,
  ) {
    this.baseUrl = configService.get<string>('BASE_URL');

    this.migrate();
  }

  private async migrate() {
    // Add requireDataUsageAuthorization to all events that do not have it
    await this.eventModel.updateMany(
      { requireDataUsageAuthorization: { $exists: false } },
      { $set: { requireDataUsageAuthorization: true } },
    );
  }

  async checkIfEventExists(eventId: Types.ObjectId) {
    const event = await this.eventModel.findById(eventId, 'name');
    return event != null;
  }

  async checkIfUserCanViewEvent(userId: Types.ObjectId, eventId: Types.ObjectId) {
    const event = await this.findById(eventId);
    return event.editors.includes(userId) || event.viewers.includes(userId) || event.owner.equals(userId);
  }

  async getOneEventRegistration(userId: Types.ObjectId, eventId: Types.ObjectId, registrationId: Types.ObjectId) {
    if (!(await this.checkIfEventExists(eventId))) throw new NotFoundException();

    const eventRegistration = await this.eventRegistrationModel
      .findOne({ event: eventId, _id: registrationId })
      .populate({
        path: 'event',
        select: 'name',
      })
      .populate({
        path: 'user',
        select: ['id', 'name', 'email'],
      })
      .exec();

    if (!userId.equals(eventRegistration.user._id) && !(await this.checkIfUserCanViewEvent(userId, eventId))) {
      throw new ForbiddenException('only the event owner/editors or the submission owner can make this request');
    }

    return plainToClass(EventRegistrationDto, eventRegistration);
  }

  async getEventRegistrations(
    userId: Types.ObjectId,
    eventId: Types.ObjectId,
    filters: EventRegistrationFilterDto,
    sort: { column: EventRegistrationSortBy; direction: SortDirection },
    limit?: number,
    offset?: number,
  ) {
    if (!(await this.checkIfEventExists(eventId))) throw new NotFoundException();
    if (!(await this.checkIfUserCanViewEvent(userId, eventId))) {
      throw new ForbiddenException('only the event owner/editors of this event can make this request');
    }

    const userFilter: FilterQuery<UserDocument & PassportLocalDocument> = {};
    const eventFilter = { _id: eventId };
    const eventRegistrationFilter: FilterQuery<EventRegistrationDocument & PassportLocalDocument> = {
      event: eventId,
    };

    if (filters.query) {
      // We assume that filters.query is already escaped for use in a RegEx
      userFilter.$or = [{ email: { $regex: filters.query, $options: 'i' } }];
    }

    if (filters.projectSubmitted != null) {
      eventRegistrationFilter.submitted = filters.projectSubmitted;
    }

    // extract sorting in correct format
    const finalSort: Record<string, 1 | -1> = {};

    if (sort.column === 'email') {
      finalSort[`user.${sort.column}`] = sort.direction === 'asc' ? 1 : -1;
    } else {
      finalSort[sort.column] = sort.direction === 'asc' ? 1 : -1;
    }

    const setIdExpression = {
      id: {
        $toString: '$_id',
      },
    };

    // collect all event registrations that match the event ID, the filter query (email)
    // apply sorting and limit/offset (pagination from Cockpit)
    const filteredEventRegistrationsStage = [
      { $unset: ['projectData', 'questionnaireResponses', 'freeText'] },
      { $set: setIdExpression },
      {
        $lookup: {
          from: 'events',
          localField: 'event',
          foreignField: '_id',
          as: 'event',
          pipeline: [{ $match: eventFilter }, { $set: setIdExpression }, { $project: { name: 1, id: 1 } }],
        },
      },
      {
        $lookup: {
          from: 'users',
          localField: 'user',
          foreignField: '_id',
          as: 'user',
          pipeline: [{ $match: userFilter }, { $set: setIdExpression }, { $project: { name: 1, email: 1, id: 1 } }],
        },
      },
      { $unwind: '$user' },
      { $sort: finalSort },
      { $skip: offset ?? 0 },
      { $limit: limit ?? 100 },
    ];

    // retrieve the total count of event registrations that match the event ID and the filter query (pagination is ignored)
    const countStage = [
      {
        $lookup: {
          from: 'users',
          localField: 'user',
          foreignField: '_id',
          as: 'user',
          pipeline: [{ $match: userFilter }],
        },
      },
      { $unwind: '$user' },
      { $count: 'count' },
    ];

    const eventRegistrationAndCountAggregation = (
      await this.eventRegistrationModel
        .aggregate([
          {
            $match: eventRegistrationFilter,
          },
          {
            $facet: {
              registrations: filteredEventRegistrationsStage,
              count: countStage,
            },
          },
        ])
        .exec()
    )[0];

    // event registrations that match the filter, limited by limit and offset
    const eventRegistrations = eventRegistrationAndCountAggregation.registrations;
    // count of all event registrations that match the filter
    const numberOfEventRegistrations =
      eventRegistrationAndCountAggregation.registrations.length > 0 ? eventRegistrationAndCountAggregation.count[0].count : 0;

    return plainToClass(EventRegistrationListDto, { items: eventRegistrations, count: numberOfEventRegistrations });
  }

  async create(userId: Types.ObjectId, event: CreateEventDto) {
    const { owner, editors, viewers, ...remainingEvent } = event;

    const doc = await this.eventModel.create({
      ...remainingEvent,
      owner: owner != null ? (await this.loadUserIdsFromPermissions([owner]))[0] : userId,
      editors: editors != null ? await this.loadUserIdsFromPermissions(editors) : undefined,
      viewers: viewers != null ? await this.loadUserIdsFromPermissions(viewers) : undefined,
    });

    return toDto(
      await doc.populate<{ owner: Pick<User, 'email'>; editors: Pick<User, 'email'>[]; viewers: Pick<User, 'email'>[] }>([
        { path: 'owner', select: 'email' },
        { path: 'editors', select: 'email' },
        { path: 'viewers', select: 'email' },
      ]),
    );
  }

  async update(userId: Types.ObjectId, eventId: Types.ObjectId, update: UpdateEventDto) {
    // Get the current event
    const event = await this.findById(eventId);
    if (event == null) {
      throw new NotFoundException();
    }

    // Ensure the user has sufficient permissions
    if (update.owner != null || update.editors != null) {
      if (!userId.equals(event.owner as Types.ObjectId)) {
        throw new ForbiddenException('only the owner can change permissions');
      }
    } else {
      if (!event.editors.some(editor => userId.equals(editor as Types.ObjectId)) && !userId.equals(event.owner as Types.ObjectId)) {
        throw new ForbiddenException('only the owner or editors may modify an event');
      }
    }

    // The user is trying to unrelease the questionnaire
    if (event.questionnaireReleased && update.questionnaireReleased === false) {
      throw new BadRequestException('The questionnaire cannot be unreleased');
    }

    // The user is trying to modify the questionnaire after it was released
    if (event.questionnaireReleased && update.questionnaire) {
      throw new BadRequestException('The questionnaire cannot be changed after it has been released');
    }

    // Perform the update
    const { owner, editors, viewers, ...remainingUpdate } = update;
    Object.assign(event, remainingUpdate);

    // Handle changes in permissions
    if (update.owner != null) {
      event.owner = (await this.loadUserIdsFromPermissions([owner]))[0];
    }
    if (update.editors != null) {
      event.editors = await this.loadUserIdsFromPermissions(editors);
    }
    if (update.viewers != null) {
      event.viewers = await this.loadUserIdsFromPermissions(viewers);
    }

    await event.save();

    return toDto(
      await event.populate<{ owner: Pick<User, 'email'>; editors: Pick<User, 'email'>[]; viewers: Pick<User, 'email'>[] }>([
        { path: 'owner', select: 'email' },
        { path: 'editors', select: 'email' },
        { path: 'viewers', select: 'email' },
      ]),
    );
  }

  /**
   * Loads exactly one user ID for every entry in {@link permissions}.
   */
  private async loadUserIdsFromPermissions(permissions: EventPermissionDto[]): Promise<Types.ObjectId[]> {
    // Make sure there are no duplicates in the editors
    const emails = uniq(permissions.map(permission => permission.email));
    const userIds = (await this.usersService.findManyByEmail(emails)).map(user => user._id);

    if (userIds.length !== emails.length) throw new NotFoundException('some emails do not belong to a user');

    return userIds;
  }

  async listEvents(userId: Types.ObjectId) {
    const docs = await this.eventModel
      .aggregate([
        {
          $match: { $or: [{ owner: userId }, { editors: userId }, { viewers: userId }] },
        },
        {
          $lookup: {
            from: 'eventregistrations',
            localField: '_id',
            foreignField: 'event',
            as: 'registrationCount',
          },
        },
        {
          $lookup: {
            from: 'eventregistrations',
            localField: '_id',
            foreignField: 'event',
            pipeline: [{ $match: { submitted: true } }],
            as: 'submissionCount',
          },
        },
        {
          $set: {
            registrationCount: { $size: '$registrationCount' },
            submissionCount: { $size: '$submissionCount' },
            id: {
              $toString: '$_id',
            },
          },
        },
        {
          $sort: { createdAt: -1 },
        },
      ])
      .exec();
    return (
      await this.eventModel.populate<{ owner: Pick<User, 'email'>; editors: Pick<User, 'email'>[]; viewers: Pick<User, 'email'>[] }>(docs, [
        { path: 'owner', select: 'email' },
        { path: 'editors', select: 'email' },
        { path: 'viewers', select: 'email' },
      ])
    ).map(doc => {
      if (doc.viewers == null) doc.viewers = [];
      return toDto(doc, EventWithStatsDto);
    });
  }

  async getOne(userId: Types.ObjectId, eventId: Types.ObjectId) {
    const doc = await this.eventModel
      .findOne({ _id: eventId, $or: [{ owner: userId }, { editors: userId }, { viewers: userId }] })
      .populate<{ owner: Pick<User, 'email'>; editors: Pick<User, 'email'>[]; viewers: Pick<User, 'email'>[] }>([
        { path: 'owner', select: 'email' },
        { path: 'editors', select: 'email' },
        { path: 'viewers', select: 'email' },
      ])
      .exec();
    return toDto(doc);
  }

  async findById(eventId: Types.ObjectId) {
    return await this.eventModel.findOne({ _id: eventId }).exec();
  }

  async findByCode(code: string) {
    return await this.eventModel.findOne({ code: code.toLowerCase() }).exec();
  }

  async deleteOne(userId: Types.ObjectId, eventId: Types.ObjectId) {
    const res = await this.eventModel.deleteOne({ _id: eventId, $or: [{ owner: userId }, { editors: userId }] }).exec();

    if (res.deletedCount === 0) {
      throw new NotFoundException();
    }

    // Also delete all submissions
    await this.eventRegistrationModel.deleteMany({ event: eventId }).exec();
  }

  async generateDefaultExport(userId: Types.ObjectId, request: EventDefaultExportRequest) {
    if (request.eventIds.length > 1 && request.attributes.includes('submissionQuestionnaire')) {
      throw new BadRequestException('Can only export questionnaire when exporting a single event');
    }

    const events = await this.eventModel
      .find({
        _id: { $in: request.eventIds.map(id => new Types.ObjectId(id)) },
        $or: [{ owner: userId }, { editors: userId }, { viewers: userId }],
      })
      .exec();

    if (events.length !== request.eventIds.length) {
      throw new NotFoundException('Some events could not be found. This might be due to lacking permissions.');
    }

    const submissions = await this.eventRegistrationModel
      .find({ event: { $in: events.map(event => event._id) }, submitted: true })
      .populate<{ user: User; event: Types.ObjectId }>('user')
      .sort({ _id: 1 })
      .exec();

    return await createDefaultExport(events, submissions, { ...request, naviBaseUrl: this.baseUrl });
  }

  async generateAltAndObjExport(userId: Types.ObjectId, request: EventAltAndObjExportRequest) {
    if (request.eventIds.length === 0) {
      throw new BadRequestException('Can only export template when exporting at least one event');
    }

    const events = await this.eventModel
      .find({
        _id: { $in: request.eventIds.map(id => new Types.ObjectId(id)) },
        $or: [{ owner: userId }, { editors: userId }, { viewers: userId }],
      })
      .exec();

    if (events.length !== request.eventIds.length) {
      throw new NotFoundException('Some events could not be found. This might be due to lacking permissions.');
    }

    const submissions = await this.eventRegistrationModel
      .find({ event: { $in: events.map(event => event._id) }, submitted: true })
      .populate<{ user: User; event: Types.ObjectId }>('user')
      .sort({ _id: 1 })
      .exec();

    return await createEventAltAndObjExport(events, submissions, request.alternativeAttributes, request.objectiveAttributes);
  }

  async generateProjectsExport(userId: Types.ObjectId, request: EventProjectsExportRequest) {
    const eventIds = request.eventIds.map(id => new Types.ObjectId(id));

    // We need to load the events to check user permissions
    const eventCount = await this.eventModel
      .find({
        _id: { $in: eventIds },
        $or: [{ owner: userId }, { editors: userId }, { viewers: userId }],
      })
      .countDocuments();

    if (eventCount !== request.eventIds.length) {
      throw new NotFoundException('Some events could not be found. This might be due to lacking permissions.');
    }

    const submissions = await this.eventRegistrationModel.find({ event: { $in: eventIds }, submitted: true }).exec();
    return await createProjectsExport(submissions);
  }
}
