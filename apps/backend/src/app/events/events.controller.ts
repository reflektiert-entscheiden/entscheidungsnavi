import {
  Body,
  Controller,
  Delete,
  Get,
  Header,
  HttpCode,
  Param,
  Patch,
  Post,
  Query,
  Req,
  Res,
  StreamableFile,
  UseGuards,
  UseInterceptors,
} from '@nestjs/common';
import { Types } from 'mongoose';
import { Response } from 'express';
import { RolesGuard } from '../auth/roles.guard';
import { NotFoundInterceptor } from '../common/not-found.interceptor';
import { ParseMongoIdPipe } from '../common/parse-mogo-id.pipe';
import { UniqueConstraintViolationInterceptor } from '../common/unique-constraint-violation.interceptor';
import { AuthenticatedRequest, LoginGuard } from '../auth/login.guard';
import { PaginationParams } from '../common/pagination-params';
import { CreateEventDto } from './dto/create-event.dto';
import { EventDefaultExportRequestDto } from './dto/event-default-export-request.dto';
import { UpdateEventDto } from './dto/update-event.dto';
import { EventsService } from './events.service';
import { EventProjectsExportRequestDto } from './dto/event-projects-export-request.dto';
import { EventAltAndObjDtoExportRequest } from './dto/event-alt-and-obj-export-request.dto';
import { EventRegistrationFilterDto } from './dto/event-registration-filter.dto';
import { EventRegistrationSortDto } from './dto/event-registration-sort.dto';

@Controller('events')
@UseGuards(LoginGuard)
export class EventsController {
  constructor(private eventsService: EventsService) {}

  @Get()
  async list(@Req() request: AuthenticatedRequest) {
    return await this.eventsService.listEvents(request.user._id);
  }

  @UseInterceptors(UniqueConstraintViolationInterceptor())
  @Post()
  @UseGuards(RolesGuard('event-manager'))
  async create(@Req() request: AuthenticatedRequest, @Body() createEventDto: CreateEventDto) {
    return await this.eventsService.create(request.user._id, createEventDto);
  }

  @Post('default-export')
  @HttpCode(200)
  async generateDefaultExport(@Req() request: AuthenticatedRequest, @Body() body: EventDefaultExportRequestDto, @Res() res: Response) {
    const wb = await this.eventsService.generateDefaultExport(request.user._id, body);
    const buffer = await wb.xlsx.writeBuffer();

    res.setHeader('Content-Type', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    res.setHeader('Content-Disposition', 'attachment;filename=data.xlsx');

    res.write(buffer);
    res.end();
  }

  @Post('alternatives-and-objectives-export')
  @HttpCode(200)
  async generateAltAndObjExport(@Req() request: AuthenticatedRequest, @Body() body: EventAltAndObjDtoExportRequest, @Res() res: Response) {
    const wb = await this.eventsService.generateAltAndObjExport(request.user._id, body);
    const buffer = await wb.xlsx.writeBuffer();

    res.setHeader('Content-Type', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    res.setHeader('Content-Disposition', 'attachment;filename=data.xlsx');

    res.write(buffer);
    res.end();
  }

  @Post('projects-export')
  @Header('Content-Type', 'application/zip')
  @Header('Content-Disposition', 'attachment;filename=data.zip')
  @HttpCode(200)
  async generateProjectsExport(@Req() request: AuthenticatedRequest, @Body() body: EventProjectsExportRequestDto) {
    const zip = await this.eventsService.generateProjectsExport(request.user._id, body);
    return new StreamableFile(zip);
  }

  @UseInterceptors(NotFoundInterceptor)
  @Get(':eventId')
  async getOne(@Req() request: AuthenticatedRequest, @Param('eventId', ParseMongoIdPipe) id: Types.ObjectId) {
    return await this.eventsService.getOne(request.user._id, id);
  }

  @UseInterceptors(UniqueConstraintViolationInterceptor())
  @Patch(':eventId')
  async update(
    @Req() request: AuthenticatedRequest,
    @Param('eventId', ParseMongoIdPipe) id: Types.ObjectId,
    @Body() updateEventDto: UpdateEventDto,
  ) {
    return this.eventsService.update(request.user._id, id, updateEventDto);
  }

  @Delete(':eventId')
  async del(@Req() request: AuthenticatedRequest, @Param('eventId', ParseMongoIdPipe) id: Types.ObjectId) {
    await this.eventsService.deleteOne(request.user._id, id);
  }

  @Get(':eventId/registrations')
  async getEventRegistrations(
    @Req() request: AuthenticatedRequest,
    @Param('eventId', ParseMongoIdPipe) eventId: Types.ObjectId,
    @Query() filters: EventRegistrationFilterDto,
    @Query() { sortBy, sortDirection }: EventRegistrationSortDto,
    @Query() { limit, offset }: PaginationParams,
  ) {
    return await this.eventsService.getEventRegistrations(
      request.user._id,
      eventId,
      filters,
      { column: sortBy ?? 'updatedAt', direction: sortDirection ?? 'desc' },
      limit,
      offset,
    );
  }

  @Get(':eventId/registrations/:registrationId')
  async getOneEventRegistration(
    @Req() request: AuthenticatedRequest,
    @Param('eventId', ParseMongoIdPipe) eventId: Types.ObjectId,
    @Param('registrationId', ParseMongoIdPipe) registrationId: Types.ObjectId,
  ) {
    return await this.eventsService.getOneEventRegistration(request.user._id, eventId, registrationId);
  }
}
