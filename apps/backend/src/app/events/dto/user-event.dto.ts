import { UserEvent } from '@entscheidungsnavi/api-types';
import { Exclude, Expose, Type } from 'class-transformer';
import { IsBoolean } from 'class-validator';
import { EventFreeTextConfigDto } from './event-free-text-config.dto';
import { EventProjectRequirementsDto } from './event-project-requirements.dto';
import { QuestionnairePageDto } from './event-questionnaire.dto';

@Exclude()
export class UserEventDto implements UserEvent {
  @Expose()
  id: string;

  @Expose()
  name: string;

  @Expose()
  startDate?: Date;

  @Expose()
  endDate?: Date;

  @Expose()
  @Type(() => EventProjectRequirementsDto)
  projectRequirements: EventProjectRequirementsDto;

  @Expose()
  @Type(() => QuestionnairePageDto)
  questionnaire?: QuestionnairePageDto[];

  @Expose()
  @Type(() => EventFreeTextConfigDto)
  freeTextConfig: EventFreeTextConfigDto;

  @Expose()
  @IsBoolean()
  requireDataUsageAuthorization: boolean;
}
