import {
  EventAltAndObjExportAlternativesAttribute,
  EventAltAndObjExportRequest,
  EVENT_ALT_AND_OBJ_EXPORT_ALTERNATIVES_ATTRIBUTES,
  EventAltAndObjExportObjectivesAttribute,
  EVENT_ALT_AND_OBJ_EXPORT_OBJECTIVES_ATTRIBUTES,
} from '@entscheidungsnavi/api-types';
import { IsEnum, IsMongoId } from 'class-validator';

export class EventAltAndObjDtoExportRequest implements EventAltAndObjExportRequest {
  @IsMongoId({ each: true })
  eventIds: string[];

  @IsEnum(EVENT_ALT_AND_OBJ_EXPORT_ALTERNATIVES_ATTRIBUTES, { each: true })
  alternativeAttributes: EventAltAndObjExportAlternativesAttribute[];

  @IsEnum(EVENT_ALT_AND_OBJ_EXPORT_OBJECTIVES_ATTRIBUTES, { each: true })
  objectiveAttributes: EventAltAndObjExportObjectivesAttribute[];
}
