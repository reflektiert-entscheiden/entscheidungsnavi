import { IntersectionType, PartialType, PickType } from '@nestjs/mapped-types';
import { EventDto } from './event.dto';

export class CreateEventDto extends IntersectionType(
  PickType(EventDto, ['name'] as const),
  PartialType(
    PickType(EventDto, [
      'owner',
      'editors',
      'viewers',
      'code',
      'startDate',
      'endDate',
      'projectRequirements',
      'questionnaire',
      'questionnaireReleased',
      'freeTextConfig',
      'requireDataUsageAuthorization',
    ] as const),
  ),
) {}
