import {
  BaseQuestionnaireEntry,
  QuestionnaireEntryType,
  QUESTIONNAIRE_ENTRY_TYPES,
  NumberQuestionEntry,
  OptionsQuestionEntry,
  TextQuestionEntry,
  OptionsQuestionDisplayType,
  OPTIONS_QUESTION_DISPLAY_TYPES,
  QuestionnairePage,
  FormFieldQuestionEntry,
  TableQuestionEntry,
  TextBlockEntry,
  TEXT_BLOCK_TYPES,
  TextBlockType,
} from '@entscheidungsnavi/api-types';
import { Exclude, Expose, Type } from 'class-transformer';
import { IsEnum, IsNumber, IsOptional, IsString, ValidateNested } from 'class-validator';

@Exclude()
export abstract class AbstractQuestionnaireEntryDto implements BaseQuestionnaireEntry {
  @Expose()
  @IsEnum(QUESTIONNAIRE_ENTRY_TYPES)
  entryType: QuestionnaireEntryType;
}

@Exclude()
export class TextBlockEntryDto extends AbstractQuestionnaireEntryDto implements TextBlockEntry {
  override entryType: 'textBlock';

  @Expose()
  @IsString()
  text: string;

  @Expose()
  @IsEnum(TEXT_BLOCK_TYPES)
  type: TextBlockType;
}

@Exclude()
abstract class AbstractFormFieldEntryDto extends AbstractQuestionnaireEntryDto implements FormFieldQuestionEntry {
  @Expose()
  @IsString()
  question: string;

  @Expose()
  @IsString()
  label: string;
}

@Exclude()
export class TextQuestionEntryDto extends AbstractFormFieldEntryDto implements TextQuestionEntry {
  override entryType: 'textQuestion';

  @Expose()
  @IsOptional()
  @IsNumber()
  minLength?: number;

  @Expose()
  @IsOptional()
  @IsNumber()
  maxLength?: number;

  @Expose()
  @IsOptional()
  @IsString()
  pattern?: string;
}

@Exclude()
export class NumberQuestionEntryDto extends AbstractFormFieldEntryDto implements NumberQuestionEntry {
  override entryType: 'numberQuestion';

  @Expose()
  @IsOptional()
  @IsNumber()
  min?: number;

  @Expose()
  @IsOptional()
  @IsNumber()
  max?: number;

  @Expose()
  @IsOptional()
  @IsNumber()
  step?: number;
}

@Exclude()
export class OptionsQuestionEntryDto extends AbstractFormFieldEntryDto implements OptionsQuestionEntry {
  override entryType: 'optionsQuestion';

  @Expose()
  @IsString({ each: true })
  options: string[];

  @Expose()
  @IsEnum(OPTIONS_QUESTION_DISPLAY_TYPES)
  displayType: OptionsQuestionDisplayType;
}

@Exclude()
export class TableQuestionEntryDto extends AbstractQuestionnaireEntryDto implements TableQuestionEntry {
  override entryType: 'tableQuestion';

  @Expose()
  @IsString({ each: true })
  options: string[];

  @Expose()
  @IsString()
  baseQuestion: string;

  @Expose()
  @IsString({ each: true })
  subQuestions: string[];
}

@Exclude()
export class QuestionnairePageDto implements QuestionnairePage {
  @Expose()
  @ValidateNested({ each: true })
  @Type(() => AbstractQuestionnaireEntryDto, {
    keepDiscriminatorProperty: true,
    discriminator: {
      property: 'entryType',
      subTypes: [
        { value: TextBlockEntryDto, name: 'textBlock' },
        { value: TextQuestionEntryDto, name: 'textQuestion' },
        { value: NumberQuestionEntryDto, name: 'numberQuestion' },
        { value: OptionsQuestionEntryDto, name: 'optionsQuestion' },
        { value: TableQuestionEntryDto, name: 'tableQuestion' },
      ],
    },
  })
  entries: (TextBlockEntryDto | TextQuestionEntryDto | NumberQuestionEntryDto | OptionsQuestionEntryDto | TableQuestionEntryDto)[];
}
