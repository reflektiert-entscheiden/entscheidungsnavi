import {
  SortDirection,
  SORT_DIRECTIONS,
  EventRegistrationSort,
  EVENT_REGISTRATION_SORT_BY,
  EventRegistrationSortBy,
} from '@entscheidungsnavi/api-types';
import { IsEnum, IsOptional } from 'class-validator';

export class EventRegistrationSortDto implements EventRegistrationSort {
  @IsOptional()
  @IsEnum(EVENT_REGISTRATION_SORT_BY)
  sortBy?: EventRegistrationSortBy;

  @IsOptional()
  @IsEnum(SORT_DIRECTIONS)
  sortDirection?: SortDirection;
}
