import { EventRegistrationFilter } from '@entscheidungsnavi/api-types';
import { Transform } from 'class-transformer';
import { IsBoolean, IsOptional, IsString } from 'class-validator';
import { escapeRegExp } from 'lodash';

export class EventRegistrationFilterDto implements EventRegistrationFilter {
  @IsOptional()
  @IsString()
  @Transform(({ value }) => escapeRegExp(value))
  query?: string;

  @IsOptional()
  @IsBoolean()
  @Transform(({ value }) => value === 'true')
  projectSubmitted?: boolean;
}
