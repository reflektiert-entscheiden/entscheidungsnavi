import { getConnectionToken, getModelToken } from '@nestjs/mongoose';
import { Test } from '@nestjs/testing';
import { plainToClass } from 'class-transformer';
import { Types } from 'mongoose';
import { applyPatch, createPatch, hashCode } from '@entscheidungsnavi/tools';
import { ConflictException, NotFoundException, PayloadTooLargeException } from '@nestjs/common';
import { repeat } from 'lodash';
import { Project } from '../schemas/project.schema';
import { ProjectHistoryEntry } from '../schemas/project-history.schema';
import { UpdateProjectDto } from './dto/update-project.dto';
import { ProjectsService } from './projects.service';
import { compressPatch, decompressPatch } from './history-helper';

describe('ProjectsService', () => {
  let projectsService: ProjectsService;
  let findProject: jest.Mock;
  let findOneProject: jest.Mock;
  let updateOneProject: jest.Mock;
  let findOneAndUpdateProject: jest.Mock;
  let createHistoryEntry: jest.Mock;

  beforeEach(async () => {
    findProject = jest.fn();
    findOneProject = jest.fn();
    updateOneProject = jest.fn();
    findOneAndUpdateProject = jest.fn();
    createHistoryEntry = jest.fn();

    const module = await Test.createTestingModule({
      providers: [
        {
          provide: getModelToken(Project.name),
          useValue: {
            find: findProject,
            findOne: jest.fn().mockReturnValue({ exec: findOneProject }),
            updateOne: updateOneProject,
            findOneAndUpdate: findOneAndUpdateProject,
          },
        },
        {
          provide: getModelToken(ProjectHistoryEntry.name),
          useValue: {
            create: createHistoryEntry,
          },
        },
        {
          provide: getConnectionToken(),
          useValue: { transaction: (fn: () => Promise<any>) => fn() },
        },
        ProjectsService,
      ],
    }).compile();

    projectsService = module.get(ProjectsService);
  });

  describe('list()', () => {
    it('should return projects for user', async () => {
      const projectId1 = new Types.ObjectId(),
        projectId2 = new Types.ObjectId(),
        userId = new Types.ObjectId(),
        date = new Date('04 Dec 1995 00:12:00 GMT');

      findProject.mockReturnValue({
        sort: jest.fn().mockReturnThis(),
        exec: jest.fn().mockResolvedValueOnce([
          {
            id: projectId1,
            userId,
            name: 'project 1',
            createdAt: date,
            updatedAt: date,
            populate: jest.fn().mockReturnThis(),
          },
          {
            id: projectId2,
            userId,
            name: 'p2',
            createdAt: date,
            updatedAt: date,
            populate: jest.fn().mockReturnThis(),
          },
        ]),
      });

      await expect(projectsService.list(userId)).resolves.toEqual([
        { id: projectId1.toString(), name: 'project 1', createdAt: date, updatedAt: date, userId: userId.toString() },
        { id: projectId2.toString(), name: 'p2', createdAt: date, updatedAt: date, userId: userId.toString() },
      ]);

      expect(findProject.mock.calls[0][0]).toHaveProperty('userId', userId);
    });
  });

  describe('update()', () => {
    let pId: Types.ObjectId, uId: Types.ObjectId, hId: Types.ObjectId;

    beforeEach(() => {
      pId = new Types.ObjectId();
      uId = new Types.ObjectId();
      hId = new Types.ObjectId();

      findOneProject.mockResolvedValue({
        _id: pId,
        userId: uId,
        name: 'old name',
        data: 'dummy data',
        history: [hId],
        updatedAt: new Date(),
      });
      findOneAndUpdateProject.mockReturnValue({
        exec: jest.fn().mockResolvedValue({
          _id: pId,
          userId: uId,
          name: 'new name',
          populate: jest.fn().mockReturnThis(),
        }),
      });
    });

    it('updates the name', async () => {
      await projectsService.update(uId, pId, { name: 'new name' });

      expect(findOneAndUpdateProject.mock.calls[0][1]).toStrictEqual({
        $set: {
          name: 'new name',
        },
        $unset: {},
      });
    });

    it('allows replacing the project data', async () => {
      const newHistoryId = new Types.ObjectId();
      createHistoryEntry.mockResolvedValue([
        {
          _id: newHistoryId,
        },
      ]);

      await projectsService.update(uId, pId, { data: 'new data' });

      expect(createHistoryEntry).toHaveBeenCalledTimes(1);
      // Check if the patch is valid
      expect(applyPatch(decompressPatch(createHistoryEntry.mock.calls[0][0][0].compressedPatch), 'new data')).toBe('dummy data');

      expect(findOneAndUpdateProject.mock.calls[0][1]).toStrictEqual({
        $set: {
          data: 'new data',
        },
        $unset: {
          dataSaveType: 1,
        },
        $push: {
          history: newHistoryId,
        },
      });
    });

    it('allows patching the project data', async () => {
      const patch = createPatch('dummy data', 'new data');

      const newHistoryId = new Types.ObjectId();
      createHistoryEntry.mockResolvedValue([
        {
          _id: newHistoryId,
        },
      ]);

      await projectsService.update(uId, pId, { dataPatch: patch, oldDataHash: hashCode('dummy data'), dataSaveType: 'restore-backup' });

      expect(findOneAndUpdateProject.mock.calls[0][1]).toStrictEqual({
        $set: { data: 'new data', dataSaveType: 'restore-backup' },
        $unset: {},
        $push: { history: newHistoryId },
      });

      expect(createHistoryEntry).toHaveBeenCalledTimes(1);

      // Check if the created patch if valid
      const patchResult = applyPatch(decompressPatch(createHistoryEntry.mock.calls[0][0][0].compressedPatch), 'new data');
      expect(patchResult).toEqual('dummy data');
      expect(createHistoryEntry.mock.calls[0][0][0].saveType).toBeFalsy();
    });

    it('handles race conditions when updating projects', async () => {
      // The project is not found
      findOneAndUpdateProject.mockReturnValue({
        exec: jest.fn().mockResolvedValue(null),
      });
      createHistoryEntry.mockResolvedValue([
        {
          _id: new Types.ObjectId(),
        },
      ]);

      await expect(projectsService.update(uId, pId, { data: 'new data' })).rejects.toThrowError(ConflictException);

      expect(createHistoryEntry).toHaveBeenCalledTimes(1);
    });

    it('rejects if the project is too large', async () => {
      createHistoryEntry.mockResolvedValue([
        {
          _id: new Types.ObjectId(),
        },
      ]);

      await expect(projectsService.update(uId, pId, { data: repeat('a', 10e6) })).resolves.not.toThrow();
      await expect(projectsService.update(uId, pId, { data: repeat('a', 10e6 + 1) })).rejects.toThrowError(PayloadTooLargeException);
    });

    it('ignores null/undefined fields', async () => {
      await projectsService.update(new Types.ObjectId(), new Types.ObjectId(), plainToClass(UpdateProjectDto, { name: null, data: null }));

      expect(findOneAndUpdateProject.mock.calls[0][1]).toStrictEqual({ $set: {}, $unset: {} });
    });
  });

  describe('getDataFromHistory()', () => {
    let pId: Types.ObjectId, uId: Types.ObjectId, historyEntries: any[], historyEntryIds: Types.ObjectId[];

    beforeEach(() => {
      pId = new Types.ObjectId();
      uId = new Types.ObjectId();

      historyEntries = [
        { _id: new Types.ObjectId(), compressedPatch: compressPatch(createPatch('dummy data', 'temporary data')) },
        { _id: new Types.ObjectId(), compressedPatch: compressPatch(createPatch('temporary data', 'some more data')) },
        { _id: new Types.ObjectId(), compressedPatch: compressPatch(createPatch('some more data', 'the final data')) },
      ];
      historyEntryIds = historyEntries.map(entry => entry._id);

      findOneProject.mockResolvedValue({
        _id: pId,
        userId: uId,
        name: 'old name',
        data: 'dummy data',
        history: historyEntryIds,
        populate: jest.fn().mockResolvedValue({
          history: historyEntries,
        }),
      });
    });

    it('applies patches sequentially', async () => {
      await expect(projectsService.getDataFromHistory(uId, pId, historyEntryIds[2])).resolves.toEqual('the final data');
    });

    it('fails if no patches can be found', async () => {
      await expect(projectsService.getDataFromHistory(uId, pId, new Types.ObjectId())).rejects.toThrowError(NotFoundException);
    });
  });
});
