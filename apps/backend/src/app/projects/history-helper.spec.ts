import { compressPatch, decompressPatch } from './history-helper';

describe('compressPatch()/decompressPatch()', () => {
  it('is a reversible operation', () => {
    const source = 'some random text';
    const result = decompressPatch(compressPatch(source));

    expect(result).toEqual(source);
  });
});
