import { ProjectHistoryEntry, ProjectSaveType } from '@entscheidungsnavi/api-types';
import { Exclude, Expose, Type } from 'class-transformer';

@Exclude()
export class ProjectHistoryEntryDto implements ProjectHistoryEntry {
  @Expose()
  @Type(() => String)
  id: string;

  @Expose()
  saveType?: ProjectSaveType;

  @Expose()
  versionTimestamp: Date;
}
