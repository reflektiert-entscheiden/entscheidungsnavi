import { PickType } from '@nestjs/mapped-types';
import { ProjectWithDataDto } from './project.dto';

export class CreateProjectDto extends PickType(ProjectWithDataDto, ['name', 'data'] as const) {}
