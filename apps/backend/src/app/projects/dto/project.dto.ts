import { Exclude, Expose, Type } from 'class-transformer';
import { IsNotEmpty, IsString } from 'class-validator';
import { Project as ApiProject, ProjectWithData } from '@entscheidungsnavi/api-types';

@Exclude()
export class ProjectDto implements ApiProject {
  @Expose()
  @Type(() => String)
  id: string;

  @IsNotEmpty()
  @IsString()
  @Expose()
  name: string;

  @Expose()
  @Type(() => String)
  userId: string;

  @Expose()
  shareToken?: string;

  @Expose()
  createdAt: Date;

  @Expose()
  updatedAt: Date;
}

@Exclude()
export class ProjectWithDataDto extends ProjectDto implements ProjectWithData {
  @Expose()
  @IsString()
  data: string;
}
