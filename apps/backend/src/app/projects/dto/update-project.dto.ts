import { PartialType } from '@nestjs/mapped-types';
import { IsEnum, IsNumber, IsOptional, IsString, ValidateIf } from 'class-validator';
import { PROJECT_SAVE_TYPES, ProjectSaveType } from '@entscheidungsnavi/api-types';
import { CreateProjectDto } from './create-project.dto';

export class UpdateProjectDto extends PartialType(CreateProjectDto) {
  @IsOptional()
  @IsString()
  dataPatch?: string;

  // We need the hash when we are patching
  @ValidateIf(o => o.dataPatch != null)
  @IsNumber()
  oldDataHash?: number;

  @IsOptional()
  @IsEnum(PROJECT_SAVE_TYPES)
  dataSaveType?: ProjectSaveType;
}
