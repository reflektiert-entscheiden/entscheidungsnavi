import { randomBytes as randomBytesCallback } from 'crypto';
import { promisify } from 'util';
import { BadRequestException, ConflictException, Injectable, NotFoundException, PayloadTooLargeException } from '@nestjs/common';
import { InjectConnection, InjectModel } from '@nestjs/mongoose';
import { plainToClass } from 'class-transformer';
import { ClientSession, Connection, Model, Types, UpdateQuery } from 'mongoose';
import { applyPatch, createPatch, hashCode, textSize } from '@entscheidungsnavi/tools';
import { PROJECT_SIZE_LIMIT } from '@entscheidungsnavi/api-types';
import { Project, ProjectDocument } from '../schemas/project.schema';
import { ProjectHistoryEntry, ProjectHistoryEntryDocument } from '../schemas/project-history.schema';
import { CreateProjectDto } from './dto/create-project.dto';
import { ProjectDto, ProjectWithDataDto } from './dto/project.dto';
import { UpdateProjectDto } from './dto/update-project.dto';
import { compressPatch, decompressPatch } from './history-helper';
import { ProjectHistoryEntryDto } from './dto/project-history-entry.dto';

const randomBytes = promisify(randomBytesCallback);

function toDto(doc: ProjectDocument, withData?: false): ProjectDto;
function toDto(doc: ProjectDocument, withData: true): ProjectWithDataDto;
function toDto(doc: ProjectDocument, withData = false) {
  return plainToClass(withData ? ProjectWithDataDto : ProjectDto, doc);
}

@Injectable()
export class ProjectsService {
  constructor(
    @InjectModel(Project.name) private projectModel: Model<ProjectDocument>,
    @InjectModel(ProjectHistoryEntry.name) private projectHistoryEntryModel: Model<ProjectHistoryEntryDocument>,
    @InjectConnection() private connection: Connection,
  ) {}

  async create(userId: Types.ObjectId, project: CreateProjectDto, teamId?: Types.ObjectId, session?: ClientSession) {
    return toDto((await this.projectModel.create([{ ...project, userId, team: teamId }], { session }))[0]);
  }

  async update(userId: Types.ObjectId, projectId: Types.ObjectId, update: Readonly<UpdateProjectDto>) {
    const project = await this.projectModel.findOne({ userId, _id: projectId }).exec();
    if (project == null) {
      throw new NotFoundException();
    }

    let dataUpdate: string;

    // Parse the data update from the request
    if (update.data != null) {
      dataUpdate = update.data;
    } else if (update.dataPatch != null) {
      const oldDataHash = hashCode(project.data);

      if (oldDataHash !== update.oldDataHash) {
        throw new ConflictException(project.updatedAt.toISOString());
      }

      try {
        dataUpdate = applyPatch(update.dataPatch, project.data);
      } catch {
        throw new BadRequestException('the given data patch is invalid');
      }
    }

    // Perform the actual update in a transaction
    let updateResult: ProjectDocument;
    await this.connection.transaction(async session => {
      const query: UpdateQuery<ProjectDocument> = { $set: {}, $unset: {} };

      if (update.name != null) {
        query.$set.name = update.name;
      }

      if (dataUpdate) {
        // 10 MB limit
        if (textSize(dataUpdate) > PROJECT_SIZE_LIMIT) {
          throw new PayloadTooLargeException('projects may be at most 10MB in size');
        }

        const patchString = createPatch(dataUpdate, project.data);
        const compressedPatch = compressPatch(patchString);

        const [newHistoryEntry] = await this.projectHistoryEntryModel.create(
          [
            {
              compressedPatch,
              saveType: project.dataSaveType,
              versionTimestamp: project.updatedAt,
            },
          ],
          { session },
        );

        query.$set.data = dataUpdate;
        query.$push = { history: newHistoryEntry._id };

        if (update.dataSaveType == null) {
          query.$unset.dataSaveType = 1;
        } else {
          query.$set.dataSaveType = update.dataSaveType;
        }
      }

      // Only update if the project was not updated in the meantime. This prevents a race condition.
      updateResult = await this.projectModel
        .findOneAndUpdate({ userId, _id: projectId, updatedAt: project.updatedAt }, query, { new: true, session })
        .exec();

      if (updateResult == null) {
        // The project was modified in the meantime -> abort
        const modifiedProject = await this.projectModel.findOne({ userId, _id: projectId }, { updatedAt: 1 }).exec();
        throw new ConflictException(modifiedProject.updatedAt.toISOString());
      }
    });

    return toDto(updateResult);
  }

  async list(userId: Types.ObjectId) {
    // Do not load the data field for list queries
    const docs = await this.projectModel.find({ userId }, '-data -history').sort({ _id: 1 }).exec();
    return docs.map(doc => toDto(doc));
  }

  async findById(userId: Types.ObjectId, projectId: Types.ObjectId) {
    return toDto(await this.projectModel.findOne({ userId, _id: projectId }).exec(), true);
  }

  async findByIdAdmin(projectId: Types.ObjectId) {
    return toDto(await this.projectModel.findOne({ _id: projectId }).exec(), true);
  }

  async findByShareToken(shareToken: string) {
    return toDto(await this.projectModel.findOne({ shareToken }).exec(), true);
  }

  async getHistory(userId: Types.ObjectId, projectId: Types.ObjectId) {
    const project = await this.projectModel
      .findOne({ _id: projectId, userId })
      .populate<{ history: ProjectHistoryEntryDocument[] }>('history', '-compressedPatch')
      .exec();

    if (project == null) {
      throw new NotFoundException('project not found');
    }

    return project.history.map(entry => plainToClass(ProjectHistoryEntryDto, entry));
  }

  async getDataFromHistory(userId: Types.ObjectId, projectId: Types.ObjectId, historyEntryId: Types.ObjectId) {
    const project = await this.projectModel.findOne({ userId, _id: projectId }).exec();
    if (project == null) {
      throw new NotFoundException('project not found');
    }

    // Ensure the newest history entry is first
    project.history.reverse();

    // Cut off all entries past the one we need
    const index = project.history.findIndex(entry => entry.equals(historyEntryId));
    if (index === -1) {
      throw new NotFoundException('history entry not found');
    }
    project.history = project.history.slice(0, index + 1);

    const populatedProject = await project.populate<{ history: ProjectHistoryEntryDocument[] }>('history');

    // Apply all patches
    let data = project.data;
    for (const historyEntry of populatedProject.history) {
      const patchText = decompressPatch(historyEntry.compressedPatch);
      data = applyPatch(patchText, data);
    }

    return data;
  }

  async deleteOne(userId: Types.ObjectId, projectId: Types.ObjectId) {
    const res = await this.projectModel.deleteOne({ userId, _id: projectId }).exec();
    if (res.deletedCount === 0) {
      throw new NotFoundException();
    }
  }

  async deleteAllForUser(userId: Types.ObjectId, session: ClientSession) {
    await this.projectModel.deleteMany({ userId: { $eq: userId } }, { session }).exec();
  }

  async countProjects() {
    return await this.projectModel.countDocuments().exec();
  }

  async enableShare(userId: Types.ObjectId, projectId: Types.ObjectId) {
    const project = await this.projectModel.findOne({ userId, _id: projectId }, 'shareToken').exec();

    if (!project) {
      throw new NotFoundException();
    }

    if (!project.shareToken) {
      project.shareToken = await this.generateShareToken();
      await project.save();
    }

    return project.shareToken;
  }

  async disableShare(userId: Types.ObjectId, projectId: Types.ObjectId) {
    const updateResult = await this.projectModel.updateOne({ userId, _id: projectId }, { $unset: { shareToken: '' } }).exec();

    if (updateResult.matchedCount === 0) {
      throw new NotFoundException();
    }
  }

  private async generateShareToken() {
    const randomBuf = await randomBytes(48);
    return randomBuf.toString('base64').replace(/\//g, '_').replace(/\+/g, '-');
  }
}
