import { PaginationParams } from '@entscheidungsnavi/api-types';
import { Body, Controller, Delete, Get, HttpCode, Param, Patch, Post, Query, Res, UseGuards, UseInterceptors } from '@nestjs/common';
import { Response } from 'express';
import { RolesGuard } from '../auth/roles.guard';
import { NotFoundInterceptor } from '../common/not-found.interceptor';
import { UniqueConstraintViolationInterceptor } from '../common/unique-constraint-violation.interceptor';
import { KlugProjectFilterDto } from './dto/klug-project-filter.dto';
import { KlugProjectSortDto } from './dto/klug-project-sort.dto';
import { KlugService } from './klug.service';
import { CreateKlugProjectManagerDto } from './dto/create-klug-project-manager.dto';
import { UpdateKlugProjectManagerDto } from './dto/update-klug-project-manager.dto';

@UseGuards(RolesGuard('klug-manager'))
@Controller('klug-manager')
export class KlugManagerController {
  constructor(private klugService: KlugService) {}

  @UseInterceptors(NotFoundInterceptor)
  @UseInterceptors(UniqueConstraintViolationInterceptor('A project with this token already exists'))
  @Patch('projects/:token')
  async updateKlugProject(@Param('token') token: string, @Body() update: UpdateKlugProjectManagerDto) {
    return await this.klugService.updateProjectByTokenManager(token, update);
  }

  @Delete('projects/:token')
  async deleteKlugProject(@Param('token') token: string) {
    return await this.klugService.deleteProject(token);
  }

  @Post('projects')
  @UseInterceptors(UniqueConstraintViolationInterceptor('A project with this token already exists'))
  async createKlugProject(@Body() create: CreateKlugProjectManagerDto) {
    return await this.klugService.create(create.token, create.isOfficial, create.readonly);
  }

  @Get('projects')
  async listKlugProjects(
    @Query() filters: KlugProjectFilterDto,
    @Query() { sortBy, sortDirection }: KlugProjectSortDto,
    @Query() { limit, offset }: PaginationParams,
  ) {
    return await this.klugService.findKlugProjects(
      filters,
      { column: sortBy ?? 'createdAt', direction: sortDirection ?? 'asc' },
      limit,
      offset,
    );
  }

  @Post('excel-export')
  @HttpCode(200)
  async generateExcelExport(@Query() filters: KlugProjectFilterDto, @Res() res: Response) {
    const wb = await this.klugService.generateExcelExport(filters);
    const buffer = await wb.xlsx.writeBuffer();

    res.setHeader('Content-Type', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    res.setHeader('Content-Disposition', 'attachment;filename=data.xlsx');

    res.write(buffer);
    res.end();
  }
}
