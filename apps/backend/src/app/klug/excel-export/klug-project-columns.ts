import { DecisionData } from '@entscheidungsnavi/decision-data';
import { Worksheet } from 'exceljs';
import { range } from 'lodash';
import { KlugPairComparisons } from '@entscheidungsnavi/decision-data/klug';
import {
  addAlternativeExplanationColumns,
  addAlternativeUtilityValues,
  addModelNames,
  addObjectiveExplanationColumns,
  addObjectiveWeightColumns,
} from '../../common/excel/project-columns';
import { TableBuilder } from '../../common/excel/table-builder';
import { KlugProject } from '../../schemas/klug-project.schema';

export type KlugLoadedProject = Omit<KlugProject, 'data'> & {
  projectData: DecisionData;
  projectName: string;
  pairComparisons?: KlugPairComparisons;
};

export function createKlugExportBuilder(
  ws: Worksheet,
  parameters: {
    alternativeCount: number;
    objectiveCount: number;
  },
) {
  const builder = new TableBuilder<KlugLoadedProject>(ws);

  builder.addColumns(
    { name: 'Token', data: s => s.token },
    { name: 'Erstellzeitpunkt', data: s => s.updatedAt },
    { name: 'Entscheidungsfrage', data: s => s.projectData.decisionStatement.statement },
  );

  addModelNames(builder, {
    alternativeCount: parameters.alternativeCount,
    objectiveCount: parameters.objectiveCount,
    influenceFactorCount: 0,
  });

  addObjectiveExplanationColumns(builder, parameters.objectiveCount);
  addAlternativeExplanationColumns(builder, parameters.alternativeCount);

  addObjectiveWeightColumns(builder, parameters.objectiveCount);
  addAlternativeUtilityValues(builder, parameters.alternativeCount);

  builder.addGroup(
    'Paarvergleiche',
    range(1, parameters.objectiveCount).flatMap(columnIndex =>
      range(columnIndex).map(rowIndex => {
        return {
          name: `Paar Z${rowIndex + 1}/Z${columnIndex + 1}`,
          data: s => s.pairComparisons?.[rowIndex]?.[columnIndex] ?? '',
        };
      }),
    ),
  );

  return builder;
}
