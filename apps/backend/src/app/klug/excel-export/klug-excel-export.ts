import { Workbook } from 'exceljs';
import { readText } from '@entscheidungsnavi/decision-data/export';
import { KLUG_EXTRA_DATA_FIELD, KlugData, KlugPairComparisons } from '@entscheidungsnavi/decision-data/klug';
import { KlugProjectDocument } from '../../schemas/klug-project.schema';
import { createKlugExportBuilder, KlugLoadedProject } from './klug-project-columns';

interface ImportError {
  token: string;
  errorMessage: string;
}

export async function createKlugExcelExport(projects: KlugProjectDocument[]) {
  // Import all decision data objects
  const [loadedProjects, importErrors] = loadProjectData(projects);

  const maxAlternativeCount = Math.max(...loadedProjects.map(s => s.projectData.alternatives.length), 0);
  const maxObjectiveCount = Math.max(...loadedProjects.map(s => s.projectData.objectives.length), 0);

  // Create the workbook
  const wb = new Workbook();
  wb.created = new Date();

  // Create a worksheet with the errors, if applicable
  if (importErrors.length > 0) {
    createErrorsWorksheet(wb, importErrors);
  }

  // Create the actual export worksheet, if applicable
  createExportWorksheet(wb, loadedProjects, { alternativeCount: maxAlternativeCount, objectiveCount: maxObjectiveCount });

  return wb;
}

function loadProjectData(projects: KlugProjectDocument[]) {
  const importErrors: ImportError[] = [];
  const loadedEventSubmissions: KlugLoadedProject[] = [];

  projects.forEach(project => {
    try {
      const projectData = readText(project.data);
      let pairComparisons: KlugPairComparisons;

      if (KLUG_EXTRA_DATA_FIELD in projectData.extraData) {
        pairComparisons = (projectData.extraData[KLUG_EXTRA_DATA_FIELD] as KlugData).pairComparisons;
      }

      loadedEventSubmissions.push({
        ...project.toObject({ getters: true }),
        projectName: project.token,
        projectData,
        pairComparisons,
      });
    } catch (e) {
      importErrors.push({
        token: project.token,
        errorMessage: e instanceof Error ? e.message : String(e),
      });
    }
  });

  return [loadedEventSubmissions, importErrors] as const;
}

function createErrorsWorksheet(wb: Workbook, importErrors: ImportError[]) {
  const errorWs = wb.addWorksheet('Importfehler', { properties: { defaultColWidth: 20 } });
  errorWs.addRow(['Importfehler']);
  errorWs.getRow(1).font = { bold: true };
  errorWs.columns = [
    { header: 'Token', key: 'token' },
    { header: 'Fehlermeldung', key: 'errorMessage' },
  ];
  errorWs.addRows(importErrors);
  return errorWs;
}

function createExportWorksheet(
  wb: Workbook,
  projects: KlugLoadedProject[],
  parameters: {
    alternativeCount: number;
    objectiveCount: number;
  },
) {
  const exportWs = wb.addWorksheet('Export', { properties: { defaultColWidth: 20 } });
  const table = createKlugExportBuilder(exportWs, parameters).buildTable('Export', projects);
  table.commit();
}
