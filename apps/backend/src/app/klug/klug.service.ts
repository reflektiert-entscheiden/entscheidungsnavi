import { randomBytes as randomBytesCallback } from 'crypto';
import { promisify } from 'util';
import { KlugProjectSortBy, SortDirection } from '@entscheidungsnavi/api-types';
import { createKlugPDFExport } from '@entscheidungsnavi/decision-data/klug';
import { ForbiddenException, Injectable, NotFoundException } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { HttpService } from '@nestjs/axios';
import { plainToClass } from 'class-transformer';
import { FilterQuery, Model } from 'mongoose';
import { firstValueFrom, map, of, timeout } from 'rxjs';
import pdfMake from 'pdfmake/build/pdfmake';
import vfsFonts from 'pdfmake/build/vfs_fonts';
import { KlugProject, KlugProjectDocument } from '../schemas/klug-project.schema';
import { EmailService } from '../email/email.service';
import { KlugProjectFilterDto } from './dto/klug-project-filter.dto';
import { KlugProjectListDto } from './dto/klug-project-list.dto';
import { KlugProjectDto } from './dto/klug-project.dto';
import { UpdateKlugProjectDto } from './dto/update-klug-project.dto';
import { createKlugExcelExport } from './excel-export/klug-excel-export';
import { KlugFinishProjectDto } from './dto/finish-project.dto';
import { UpdateKlugProjectManagerDto } from './dto/update-klug-project-manager.dto';

const randomBytes = promisify(randomBytesCallback);

export function toDto(doc: KlugProjectDocument) {
  return plainToClass(KlugProjectDto, doc);
}

type Writeable<T> = { -readonly [P in keyof T]: T[P] };

@Injectable()
export class KlugService {
  constructor(
    @InjectModel(KlugProject.name) private klugProjectModel: Model<KlugProjectDocument>,
    private http: HttpService,
    private emailService: EmailService,
  ) {}

  async create(token: string, isOfficial: boolean, readonly = false) {
    const projectData: Partial<KlugProject> = { token, isOfficial, readonly };
    if (!isOfficial) {
      // 30 Tage später
      projectData.expiresAt = new Date(Date.now() + 1000 * 60 * 60 * 24 * 30);
    }

    const doc = await this.klugProjectModel.create(projectData);
    return toDto(doc);
  }

  async isValidKlugToken(token: string) {
    return await firstValueFrom(
      this.http.get(`https://klug-plattform.org/check-token/${token}`).pipe(
        timeout({ first: 10000, with: () => of(null) }),
        map(response => response?.status === 200),
      ),
    );
  }

  async findProjectByToken(token: string, implicitKlugProjectCreation = true) {
    const existingProject = toDto(await this.klugProjectModel.findOne({ token }).exec());

    if (existingProject) {
      return existingProject;
    }

    if (implicitKlugProjectCreation && (await this.isValidKlugToken(token))) {
      const createdProject = await this.create(token, true);

      return createdProject;
    }

    return null;
  }

  async getLatestPDFExportForProject(token: string) {
    const project = await this.klugProjectModel.findOne({ token }).exec();

    if (!project || !project.lastPdfExport) {
      return null;
    }

    return project.lastPdfExport;
  }

  async updateProjectByToken(token: string, update: UpdateKlugProjectDto) {
    const project = await this.klugProjectModel.findOne({ token }).exec();
    if (project.readonly) {
      throw new ForbiddenException('Project is set to readonly');
    }
    const result = await this.klugProjectModel.findOneAndUpdate({ token }, { $set: update }, { new: true }).exec();
    return toDto(result);
  }

  async updateProjectByTokenManager(token: string, update: UpdateKlugProjectManagerDto) {
    const result = await this.klugProjectModel.findOneAndUpdate({ token }, { $set: update }, { new: true }).exec();

    return toDto(result);
  }

  async deleteProject(token: string) {
    const result = await this.klugProjectModel.deleteOne({ token }).exec();

    if (result.deletedCount === 0) {
      throw new NotFoundException();
    }
  }

  async findKlugProjects(
    filters: KlugProjectFilterDto,
    sort: { column: KlugProjectSortBy; direction: SortDirection },
    limit?: number,
    offset?: number,
  ) {
    const documents = await this.findKlugProjectDocuments(filters, sort, limit, offset);

    return plainToClass(KlugProjectListDto, { items: documents.items.map(toDto), count: documents.count });
  }

  async generateExcelExport(filters: KlugProjectFilterDto) {
    const projects = await this.findKlugProjectDocuments(filters, { column: 'createdAt', direction: 'asc' }, Number.MAX_SAFE_INTEGER);

    return await createKlugExcelExport(projects.items);
  }

  async generateUnofficialProject() {
    const randomBuf = await randomBytes(48);

    let foundUnusedToken = false;
    let generatedToken = '';

    while (!foundUnusedToken) {
      generatedToken = randomBuf.toString('base64').replace(/\//g, '_').replace(/\+/g, '-').slice(0, 6);

      foundUnusedToken = (await this.findProjectByToken(generatedToken, false)) == null;
    }

    return await this.create(generatedToken, false);
  }

  async finishProject(token: string, body: KlugFinishProjectDto) {
    const project = await this.findProjectByToken(token);

    if (project == null) {
      throw new NotFoundException();
    }

    if (!project.finished) {
      await this.klugProjectModel.findOneAndUpdate({ token }, { finished: true });
    }

    if (body.pdfExportImages) {
      const pdfExport = await new Promise<string>(resolve => {
        const document = createKlugPDFExport(token, body.pdfExportImages);
        const pdf = pdfMake.createPdf(document, undefined, undefined, vfsFonts.vfs);
        pdf.getBase64((base64: string) => resolve(base64));
      });

      await this.klugProjectModel.findByIdAndUpdate(project.id, {
        lastPdfExport: Buffer.from(pdfExport, 'base64'),
      });

      if (body.email) {
        this.emailService.sendKlugEmail(body.email, token, pdfExport);
      }
    }
  }

  private async findKlugProjectDocuments(
    filters: KlugProjectFilterDto,
    sort: { column: KlugProjectSortBy; direction: SortDirection },
    limit?: number,
    offset?: number,
  ) {
    const f: FilterQuery<Writeable<KlugProjectDocument>> = {};
    if (filters.isOfficial != null) {
      f.isOfficial = filters.isOfficial;
    }
    if (filters.finished != null) {
      f.finished = filters.finished;
    }
    const createdAt: Partial<Record<'$gte' | '$lte', Date>> = {};
    if (filters.startDate) {
      createdAt.$gte = filters.startDate;
    }
    if (filters.endDate) {
      createdAt.$lte = filters.endDate;
    }

    if (createdAt.$gte || createdAt.$lte) {
      f.createdAt = createdAt;
    }

    const query = this.klugProjectModel.find(f);
    const totalCount = await query.clone().countDocuments(); // Count ignoring pagination

    query
      .sort({ [sort.column]: sort.direction === 'asc' ? 1 : -1 })
      .sort({ _id: 1 })
      .skip(offset ?? 0)
      .limit(limit ?? 100);

    const items = await query.exec();

    return { items, count: totalCount };
  }
}
