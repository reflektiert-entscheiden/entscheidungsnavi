import { KlugProjectList } from '@entscheidungsnavi/api-types';
import { Exclude, Expose, Type } from 'class-transformer';
import { KlugProjectDto } from './klug-project.dto';

@Exclude()
export class KlugProjectListDto implements KlugProjectList {
  @Expose()
  @Type(() => KlugProjectDto)
  items: KlugProjectDto[];

  @Expose()
  count: number;
}
