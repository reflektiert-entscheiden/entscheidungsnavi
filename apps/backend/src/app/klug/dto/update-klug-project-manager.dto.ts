import { PartialType } from '@nestjs/mapped-types';
import { CreateKlugProjectManagerDto } from './create-klug-project-manager.dto';

export class UpdateKlugProjectManagerDto extends PartialType(CreateKlugProjectManagerDto) {}
