import { PickType } from '@nestjs/mapped-types';
import { KlugProjectDto } from './klug-project.dto';

export class CreateKlugProjectManagerDto extends PickType(KlugProjectDto, ['token', 'readonly', 'isOfficial']) {}
