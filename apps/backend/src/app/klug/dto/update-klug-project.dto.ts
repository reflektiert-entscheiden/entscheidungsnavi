import { PickType } from '@nestjs/mapped-types';
import { KlugProjectDto } from './klug-project.dto';

export class UpdateKlugProjectDto extends PickType(KlugProjectDto, ['data']) {}
