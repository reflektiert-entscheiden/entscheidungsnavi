import { Language } from '@entscheidungsnavi/api-types';
import { HttpException, Injectable, Logger, NotFoundException } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { EmailService } from '../email/email.service';
import { PasswordReset, PasswordResetDocument } from '../schemas/password-reset.model';
import { UsersService } from '../users/users.service';

@Injectable()
export class AuthService {
  private readonly logger = new Logger(AuthService.name);

  constructor(
    @InjectModel(PasswordReset.name) private passwordResetModel: Model<PasswordResetDocument>,
    private usersService: UsersService,
    private emailService: EmailService,
  ) {}

  async requestPasswordReset(email: string, lang: Language) {
    const user = await this.usersService.findByEmail(email);
    if (user == null) {
      throw new NotFoundException();
    }

    // Deactivate all previous reset links
    const updateResult = await this.passwordResetModel.updateMany({ userId: user._id }, { active: false }).exec();
    // We allow at most 5 reset requests per user per day
    if (updateResult.matchedCount >= 5) {
      this.logger.log(`Passwort reset limit for user ${user.email} exceeded`);
      throw new HttpException('Too many requests for this user', 429);
    }

    const reset = await this.passwordResetModel.create({ userId: user._id });
    await this.emailService.sendResetMail(user.email, reset.token, lang);
  }

  async checkResetToken(token: string) {
    const reset = await this.passwordResetModel.findOne({ token, active: true }).exec();
    if (reset == null) {
      throw new NotFoundException();
    }
  }

  async resetPassword(token: string, password: string) {
    const reset = await this.passwordResetModel.findOneAndUpdate({ token, active: true }, { active: false }).exec();
    if (reset == null) {
      throw new NotFoundException();
    }

    await this.usersService.setPassword(reset.userId, password);
  }
}
