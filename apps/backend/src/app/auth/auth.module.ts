import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { PasswordReset, PasswordResetSchema } from '../schemas/password-reset.model';
import { EmailModule } from '../email/email.module';
import { UsersModule } from '../users/users.module';
import { AuthController } from './auth.controller';
import { AuthService } from './auth.service';
import { LocalAuthGuard } from './local-auth.guard';
import { LocalStrategy } from './local.strategy';
import { LocalSerializer } from './local.serializer';
import { LoginGuard } from './login.guard';

@Module({
  imports: [
    MongooseModule.forFeature([
      {
        name: PasswordReset.name,
        schema: PasswordResetSchema,
      },
    ]),
    UsersModule,
    EmailModule,
  ],
  controllers: [AuthController],
  providers: [LocalStrategy, LocalAuthGuard, LocalSerializer, LoginGuard, AuthService],
  exports: [LoginGuard],
})
export class AuthModule {}
