import { ConfigService } from '@nestjs/config';
import { Test } from '@nestjs/testing';
import { MongoMemoryServer } from 'mongodb-memory-server';
import request from 'supertest';
import mongoose from 'mongoose';
import { NestExpressApplication } from '@nestjs/platform-express';
import { getSessionCookies, USERS } from '../../../test/test-helper';
import { AppModule } from '../app.module';

jest.setTimeout(30000);

async function getCsrfToken(app: NestExpressApplication, cookies?: string[]) {
  const result = await request(app.getHttpServer())
    .get('/api/auth/csrf-token')
    .set('Cookie', cookies ?? []);
  const sessionCookies = getSessionCookies(result);
  const csrfToken = result.body.token;

  return { sessionCookies, csrfToken };
}

/**
 * This test builds the whole application including all modules and deps.
 */
describe('AuthModule', () => {
  let app: NestExpressApplication;
  let mongod: MongoMemoryServer;

  beforeAll(async () => {
    mongod = await MongoMemoryServer.create();
    const uri = await mongod.getUri('navi');

    await mongoose.connect(uri);

    const module = await Test.createTestingModule({
      imports: [AppModule],
    })
      .overrideProvider(ConfigService)
      .useValue({
        get: (key: string) => {
          switch (key) {
            case 'NODE_ENV':
              return 'test';
            case 'BASE_URL':
              return 'http://dev.entscheidungsnavi.de';
            case 'MONGODB_URI':
              return uri;
            case 'SESSION_SECRET':
              return 'badpassword';
            default:
              return null;
          }
        },
      })
      .compile();

    app = await module.createNestApplication<NestExpressApplication>().setGlobalPrefix('/api').init();
  });

  beforeEach(async () => {
    // Clear and recreate the users. Otherwise, we may run into rate limiting with failed login attempts.
    const { db } = mongoose.connection;
    await db.collection('users').deleteMany({});
    await db.collection('users').insertOne(USERS.user);
  });

  afterAll(async () => {
    await app.close();
    await mongoose.disconnect();
    await mongod.stop();
  });

  it('returns a csrf token', async () => {
    const result = await request(app.getHttpServer()).get('/api/auth/csrf-token').expect(200);
    expect(result.body).toHaveProperty('token');
    expect(result.body.token).toBeTruthy();
  });

  it('blocks access to the user when not logged in', async () => {
    await request(app.getHttpServer()).get('/api/user').expect(401);
  });

  describe('Login', () => {
    it('fails without csrf token', async () => {
      await request(app.getHttpServer())
        .post('/api/auth/login')
        .send({ email: 'user@entscheidungsnavi.de', password: '12345678' })
        .expect(403);
    });

    it('succeeds with csrf token', async () => {
      const { sessionCookies, csrfToken } = await getCsrfToken(app);

      const result = await request(app.getHttpServer())
        .post('/api/auth/login')
        .send({ email: 'user@entscheidungsnavi.de', password: '12345678' })
        .set('x-csrf-token', csrfToken)
        .set('Cookie', sessionCookies)
        .expect(200);
      const newCookies = getSessionCookies(result);

      const userResult = await request(app.getHttpServer()).get('/api/user').set('Cookie', newCookies).expect(200);
      expect(userResult).toHaveProperty('body.email', 'user@entscheidungsnavi.de');
    });

    it('fails on wrong password', async () => {
      const { sessionCookies, csrfToken } = await getCsrfToken(app);

      await request(app.getHttpServer())
        .post('/api/auth/login')
        .send({ email: 'user@entscheidungsnavi.de', password: '11111111' })
        .set('x-csrf-token', csrfToken)
        .set('Cookie', sessionCookies)
        .expect(401);
    });

    it('invalidates the old csrf token', async () => {
      const { sessionCookies, csrfToken } = await getCsrfToken(app);

      const loginResult = await request(app.getHttpServer())
        .post('/api/auth/login')
        .send({ email: 'user@entscheidungsnavi.de', password: '12345678' })
        .set('Cookie', sessionCookies)
        .set('x-csrf-token', csrfToken)
        .expect(200);
      const newCookies = loginResult.headers['set-cookie'];

      // Make sure we are logged in properly
      const userResult = await request(app.getHttpServer()).get('/api/user').set('Cookie', newCookies).expect(200);
      expect(userResult).toHaveProperty('body.email', 'user@entscheidungsnavi.de');

      // We should need a new csrf token at this point, because logging in or out invalidates it
      await request(app.getHttpServer())
        .post('/api/auth/logout')
        .send({ email: 'user@entscheidungsnavi.de', password: '12345678' })
        .set('Cookie', newCookies)
        .set('x-csrf-token', csrfToken)
        .expect(403);
    });
  });

  describe('Logout', () => {
    let csrfToken: string;
    let sessionCookies: string[];

    beforeEach(async () => {
      ({ sessionCookies, csrfToken } = await getCsrfToken(app));

      const response = await request(app.getHttpServer())
        .post('/api/auth/login')
        .send({ email: 'user@entscheidungsnavi.de', password: '12345678' })
        .set('Cookie', sessionCookies)
        .set('x-csrf-token', csrfToken);
      sessionCookies = getSessionCookies(response);

      ({ csrfToken } = await getCsrfToken(app, sessionCookies));
    });

    it('revokes access', async () => {
      await request(app.getHttpServer()).post('/api/auth/logout').set('Cookie', sessionCookies).set('x-csrf-token', csrfToken).expect(200);
      await request(app.getHttpServer()).get('/api/user').expect(401);
    });

    it('invalidates the csrf token', async () => {
      await request(app.getHttpServer()).post('/api/auth/logout').set('Cookie', sessionCookies).set('x-csrf-token', csrfToken).expect(200);

      // We cannot login since the csrf token is expired
      await request(app.getHttpServer())
        .post('/api/auth/login')
        .send({ email: 'user@entscheidungsnavi.de', password: '12345678' })
        .set('Cookie', sessionCookies)
        .set('x-csrf-token', csrfToken)
        .expect(403);
    });
  });
});
