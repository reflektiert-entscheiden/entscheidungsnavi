import { CsrfToken, Language } from '@entscheidungsnavi/api-types';
import { Body, Controller, Get, HttpCode, HttpStatus, Param, Post, Put, Req, UseGuards } from '@nestjs/common';
import { Throttle, seconds } from '@nestjs/throttler';
import { Request } from 'express';
import { Lang } from '../common/language.decorator';
import { AuthService } from './auth.service';
import { RequestResetDto } from './dto/request-reset.dto';
import { ResetPasswordDto } from './dto/reset-password.dto';
import { LocalAuthGuard } from './local-auth.guard';
import { AuthenticatedRequest, LoginGuard } from './login.guard';
import { CSRF } from './csrf';

@Controller('auth')
export class AuthController {
  constructor(private authService: AuthService) {}

  @HttpCode(HttpStatus.OK)
  @UseGuards(LocalAuthGuard)
  @Post('login')
  login() {
    // The guard has already logged us in. There is nothing we need to do at this point.
  }

  @HttpCode(HttpStatus.OK)
  @UseGuards(LoginGuard)
  @Post('logout')
  logout(@Req() req: AuthenticatedRequest) {
    return new Promise<void>((resolve, reject) => {
      req.session.destroy(err => {
        if (err) reject(err);
        else resolve();
      });
    });
  }

  @Get('csrf-token')
  getCsrfToken(@Req() req: Request): CsrfToken {
    return { token: CSRF.generateToken(req) };
  }

  /**
   * Allow at most 10 password reset requests per hour.
   */
  @Throttle({ default: { limit: 10, ttl: seconds(60 * 60) } })
  @Post('reset')
  async requestReset(@Body() req: RequestResetDto, @Lang() lang: Language) {
    await this.authService.requestPasswordReset(req.email, lang);
  }

  @Get('reset/token/:token')
  async checkPasswordReset(@Param('token') token: string) {
    await this.authService.checkResetToken(token);
  }

  @Put('reset/token/:token')
  async resetPassword(@Body() req: ResetPasswordDto, @Param('token') token: string) {
    await this.authService.resetPassword(token, req.password);
  }
}
