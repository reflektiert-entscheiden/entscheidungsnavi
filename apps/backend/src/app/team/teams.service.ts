import { BadRequestException, ConflictException, ForbiddenException, Injectable, NotFoundException } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import {
  Language,
  TEAM_EDIT_RIGHT_DURATION,
  TaskWrapperDto,
  TeamCommentDto,
  TeamDto,
  TeamEditTokenDto,
  TeamInviteInfoDto,
  TeamMemberDto,
  TeamProjectInfoDto,
  TeamProjectInfoWithRoleDto,
  TeamRoles,
  TeamTaskSingletonMap,
  doesTeamMemberHaveEditorRights,
  doesTeamMemberHaveOwnerRights,
} from '@entscheidungsnavi/api-types';
import { dataToText, readText } from '@entscheidungsnavi/decision-data/export';
import { v4 as uuidv4 } from 'uuid';
import { plainToClass, plainToInstance } from 'class-transformer';
import { ClientSession, Model, Types } from 'mongoose';
import { DecisionData, TeamCalc, isValidImportance } from '@entscheidungsnavi/decision-data';
import { EmailService } from '../email/email.service';
import { Team, TeamDocument } from '../schemas/team/team.schema';
import { UsersService } from '../users/users.service';
import { TeamTask } from '../schemas/team/tasks/team-task.schema';
import { UpdateTeamDto } from './dto/update-team.dto';
import { UpdateTeamMemberDto } from './dto/update-team-member.dto';
import { UpdateTeamTaskDto } from './dto/tasks/update-task.dto';
import { UpdateImportanceDto } from './dto/update-importance.dto';
import { CreateVirtualMemberDto } from './dto/create-virtual-member.dto';
import { AssignTaskDto } from './dto/tasks/assign-task.dto';
import { CreateTeamDto } from './dto/create-team.dto';

// eslint-disable-next-line @typescript-eslint/no-var-requires
const { version } = require('../../../../../package.json');

@Injectable()
export class TeamsService {
  constructor(
    @InjectModel(Team.name) private teamModel: Model<TeamDocument>,
    private usersService: UsersService,
    private emailService: EmailService,
  ) {
    this.migrate();
  }

  private async migrate() {
    // give all screws an uuid if they dont have one
    await this.teamModel
      .find({
        mainProjectData: { $exists: true },
      })
      .cursor()
      .eachAsync(async team => {
        try {
          const mainProject = readText(team.mainProjectData);

          mainProject.hintAlternatives.screws.forEach(screw => {
            if (!screw.uuid) {
              screw.uuid = uuidv4();
            }
          });

          team.mainProjectData = dataToText(mainProject, version);
          await team.save();
        } catch (e) {
          console.log('Error while migrating screws', e);
        }
      });
  }

  async removeFromAllTeams(userId: Types.ObjectId, session: ClientSession) {
    const userOwnerTeams = await this.getProjectsForUser(userId, true);
    if (userOwnerTeams.length > 0) {
      return false;
    }

    await this.teamModel.updateMany(
      { 'members.user': userId },
      {
        $set: {
          'members.$.user': null,
        },
      },
      { session },
    );

    return true;
  }

  async create(creatorUserId: Types.ObjectId, create: CreateTeamDto) {
    return plainToClass(
      TeamProjectInfoDto,
      await this.teamModel.create({
        name: create.name,
        members: [
          {
            user: creatorUserId,
            importance: [100],
            role: 'owner',
          },
        ],
        mainProjectData: create.data,
      }),
    );
  }

  async deleteTeam(teamId: Types.ObjectId, accessorUserId: Types.ObjectId) {
    const { team, accessorMember } = await this.accessTeamUsingOwnerLike(teamId, accessorUserId);

    if (accessorMember.role !== 'owner') {
      throw new ForbiddenException();
    }

    await this.teamModel.deleteOne({ _id: team._id }).exec();
  }

  async getProjectsForUser(userId: Types.ObjectId, onlyWhereUserIsOwner = false) {
    let userTeams = await this.teamModel.find({ 'members.user': userId }, { 'members.$': 1, createdAt: 1, updatedAt: 1, name: 1 });
    if (onlyWhereUserIsOwner) {
      userTeams = userTeams.filter(team => team.members.some(member => member.user?.equals(userId) && member.role === 'owner'));
    }

    const userTeamsWithRole = userTeams.map(team => {
      const userMemberRole = team.members.find(member => member.user?.equals(userId)).role;
      return plainToClass(TeamProjectInfoWithRoleDto, { ...team.toObject({ getters: true }), userMemberRole });
    });

    return userTeamsWithRole;
  }

  async getEditToken(teamId: Types.ObjectId, accessorUserId: Types.ObjectId) {
    const { team } = await this.accessTeamUsingMember(teamId, accessorUserId, ['editToken']);

    return plainToClass(TeamEditTokenDto, team.editToken);
  }

  async findById(id: Types.ObjectId, accessorUserId: Types.ObjectId) {
    const { team, accessorMember } = await this.accessTeamUsingMember(id, accessorUserId);

    return this.toTeamDto(team, accessorMember._id);
  }

  async createVirtualMember(teamId: Types.ObjectId, accessorUserId: Types.ObjectId, dto: CreateVirtualMemberDto) {
    const { team } = await this.accessTeamUsingOwnerLike(teamId, accessorUserId);

    const memberId = new Types.ObjectId();
    const newTeam = await this.teamModel.findOneAndUpdate(
      { _id: team._id },
      {
        $push: {
          members: {
            _id: memberId,
            name: dto.name,
            importance: team.importanceShareNames.map(_ => 0),
          },
        },
      },
      { new: true },
    );

    return plainToClass(TeamMemberDto, newTeam.members.id(memberId));
  }

  async createInvitedMember(teamId: Types.ObjectId, accessorUserId: Types.ObjectId, email: string, lang: Language) {
    const { team, accessorMember } = await this.accessTeamUsingOwnerLike(teamId, accessorUserId);

    if (team.members.some(member => member.invite?.email === email)) {
      throw new ConflictException('An invitation has already been sent to this email address.');
    }

    const user = await this.usersService.findByEmail(email);
    if (user && team.members.some(member => member.user && member.user.equals(user._id))) {
      throw new ConflictException('A user with this email address already exists.');
    }

    const accessorUser = await this.usersService.findOne(accessorMember.user);

    const inviteToken = uuidv4();
    const memberId = new Types.ObjectId();
    const newTeam = await this.teamModel.findOneAndUpdate(
      { _id: team._id },
      {
        $push: {
          members: {
            _id: memberId,
            importance: team.importanceShareNames.map(_ => 0),
            invite: {
              email,
              inviterName: accessorUser.name ?? accessorUser.email,
              inviteToken,
            },
          },
        },
      },
      { new: true },
    );
    this.emailService.sendTeamInviteMail(email, accessorUser.name ?? accessorUser.email, inviteToken, team.name, lang);

    return plainToClass(TeamMemberDto, newTeam.members.id(memberId));
  }

  async resendInvite(teamId: Types.ObjectId, memberId: Types.ObjectId, accessorUserId: Types.ObjectId, lang: Language) {
    const { team } = await this.accessTeamUsingOwnerLike(teamId, accessorUserId);
    const invitee = team.members.id(memberId);
    const { email, inviterName, inviteToken } = invitee.invite;

    this.emailService.sendTeamInviteMail(email, inviterName, inviteToken, team.name, lang);
  }

  async deleteTeamMember(teamId: Types.ObjectId, memberId: Types.ObjectId, accessorUserId: Types.ObjectId) {
    const { team } = await this.accessTeamUsingOwnerLike(teamId, accessorUserId);

    const memberToDelete = team.members.id(memberId);
    if (memberToDelete.role === 'owner') {
      throw new ConflictException('Cannot remove the owner of the team');
    }

    await this.teamModel.findOneAndUpdate(
      {
        _id: team._id,
      },
      {
        $pull: {
          members: {
            _id: memberId,
          },
          comments: {
            authorMember: memberId,
          },
        },
      },
      { new: true },
    );
  }

  async joinUsingToken(userId: Types.ObjectId, token: string) {
    const team = await this.findTeamUsingToken(token);

    const invitee = team.members.find(member => member.invite && member.invite.inviteToken === token);

    if (team.members.some(teamMember => teamMember.user?.equals(userId))) {
      throw new ConflictException('Provided user is already part of the team');
    }

    const newTeam = await this.teamModel.findOneAndUpdate(
      { _id: team._id, 'members._id': invitee._id },
      {
        $set: {
          'members.$.user': userId,
          'members.$.invite': null,
        },
      },
      { new: true, projection: ['id', 'name', 'createdAt', 'updatedAt'] },
    );

    return plainToInstance(TeamProjectInfoDto, newTeam);
  }

  async leaveTeam(teamId: Types.ObjectId, accessorUserId: Types.ObjectId) {
    const { team, accessorMember } = await this.accessTeamUsingMember(teamId, accessorUserId);

    if (accessorMember.role === 'owner') {
      const membersWithUserAccounts = team.members.filter(member => member.user);

      if (membersWithUserAccounts.length === 1) {
        throw new ConflictException('Cannot turn the last real member of a team into a virtual member');
      } else {
        const otherMembersOrderedByRole = membersWithUserAccounts
          .filter(member => !member._id.equals(accessorMember._id))
          .sort((a, b) => TeamRoles.indexOf(a.role) - TeamRoles.indexOf(b.role));

        const newOwner = otherMembersOrderedByRole[0];

        newOwner.role = 'owner';
      }
    }

    const leavingUser = await this.usersService.findOne(accessorMember.user);

    accessorMember.user = null;
    accessorMember.name = leavingUser.name;

    await team.save();
  }

  async comment(teamId: Types.ObjectId, accessorUserId: Types.ObjectId, objectId: string, content: string) {
    const { team, accessorMember } = await this.accessTeamUsingMember(teamId, accessorUserId);

    const commentId = new Types.ObjectId();

    const newTeam = await this.teamModel.findOneAndUpdate(
      { _id: team._id },
      {
        $push: {
          comments: {
            _id: commentId,
            authorMember: accessorMember._id,
            content,
            objectId,
          },
        },
        // Push commentId into each members unreadComments array, dont push into the accessorMember array
        $addToSet: {
          'members.$[m].unreadComments': commentId,
        },
      },
      { new: true, projection: 'comments', arrayFilters: [{ 'm._id': { $ne: accessorMember._id } }] },
    );

    return plainToInstance(TeamCommentDto, newTeam.comments);
  }

  async getComments(teamId: Types.ObjectId, accessorUserId: Types.ObjectId, forObject?: string) {
    const { team, accessorMember } = await this.accessTeamUsingMember(teamId, accessorUserId);

    const relevantComments = forObject ? team.comments.filter(comment => comment.objectId === forObject) : team.comments;

    if (forObject) {
      await this.teamModel.updateOne(
        {
          _id: team._id,
          'members._id': accessorMember._id,
        },
        {
          $pull: {
            'members.$.unreadComments': {
              $in: relevantComments.map(comment => comment.id),
            },
          },
        },
      );
    }

    return relevantComments.map(comment => plainToClass(TeamCommentDto, comment));
  }

  async deleteComment(teamId: Types.ObjectId, accessorUserId: Types.ObjectId, commentId: Types.ObjectId) {
    const { team, accessorMember } = await this.accessTeamUsingMember(teamId, accessorUserId);

    const comment = team.comments.id(commentId);

    if (!comment) {
      throw new NotFoundException({ error: 'comment-not-found' });
    }

    if (!(comment.authorMember.equals(accessorMember._id) || doesTeamMemberHaveOwnerRights(accessorMember))) {
      throw new ForbiddenException();
    }

    const newTeam = await this.teamModel.findOneAndUpdate(
      { _id: team._id },
      {
        $pull: {
          comments: {
            _id: commentId,
          },
        },
      },
      { new: true, projection: 'comments' },
    );

    return plainToInstance(TeamCommentDto, newTeam.comments);
  }

  async getInviteInfoFromToken(token: string) {
    const team = await this.findTeamUsingToken(token);

    const invitee = team.members.find(member => member.invite?.inviteToken === token);

    if (!invitee) {
      return null;
    }

    return plainToClass(TeamInviteInfoDto, { teamName: team.name, inviterName: invitee.invite.inviterName });
  }

  async updateTeam(teamId: Types.ObjectId, update: UpdateTeamDto, accessorUserId: Types.ObjectId) {
    const { team, accessorMember } = await this.accessTeamUsingMember(teamId, accessorUserId);

    const accessorHasEditorRights = doesTeamMemberHaveEditorRights(accessorMember);
    const accessorHasOwnerRights = doesTeamMemberHaveOwnerRights(accessorMember);

    if (!accessorHasEditorRights && update.whiteboard != null) {
      throw new ForbiddenException();
    }

    if (
      !accessorHasOwnerRights &&
      (update.canTakeEditRightFrom != null ||
        update.editRightExpiresFor != null ||
        update.lockedOpinionTasks != null ||
        update.lockedWeightTasks != null)
    ) {
      throw new ForbiddenException();
    }

    const editTokenInfo = (await this.toTeamDto(team, null)).editTokenInfo;
    const accessorHasValidEditToken = editTokenInfo.state === 'valid' && editTokenInfo.holder.id === accessorMember.id;
    if (!accessorHasValidEditToken && update.mainProjectData != null) {
      throw new ForbiddenException();
    }

    const set: Record<string, unknown> = {};

    if (update.whiteboard != null) {
      set.whiteboard = update.whiteboard;
    }

    if (update.canTakeEditRightFrom != null) {
      set.canTakeEditRightFrom = update.canTakeEditRightFrom;
    }

    if (update.editRightExpiresFor != null) {
      set.editRightExpiresFor = update.editRightExpiresFor;
    }

    if (update.lockedOpinionTasks != null) {
      set.lockedOpinionTasks = update.lockedOpinionTasks;
    }

    if (update.lockedWeightTasks != null) {
      set.lockedWeightTasks = update.lockedWeightTasks;
    }

    if (update.name != null) {
      set.name = update.name;
    }

    if (update.mainProjectData) {
      set.mainProjectData = update.mainProjectData;
      set['editToken.expiresAt'] = new Date(Date.now() + TEAM_EDIT_RIGHT_DURATION);
    }

    await this.teamModel.updateOne({ _id: team._id }, { $set: set }, { new: true }).exec();
  }

  async updateTeamMember(
    teamId: Types.ObjectId,
    memberId: Types.ObjectId,
    update: UpdateTeamMemberDto,
    accessorUserId: Types.ObjectId,
    lang: Language,
  ) {
    const { team, accessorMember } = await this.accessTeamUsingOwnerLike(teamId, accessorUserId);

    if (update.role === 'owner' && accessorMember.role !== 'owner') {
      throw new ForbiddenException();
    }

    const memberToUpdate = team.members.id(memberId);

    if (update.isVirtual === false) {
      throw new BadRequestException('Cannot update a member to be non-virtual');
    }

    if (!memberToUpdate) {
      throw new NotFoundException();
    }

    if (update.role === 'owner' && !memberToUpdate.user) {
      throw new BadRequestException('Cannot update a virtual member to be the owner');
    }

    const set: Record<string, unknown> = {};

    if (update.isVirtual && memberToUpdate.user) {
      await this.leaveTeam(teamId, memberToUpdate.user);
    }

    if (update.role) {
      set['members.$.role'] = update.role;
    }

    if (update.email && !memberToUpdate.invite) {
      throw new BadRequestException('Cannot update email of a normal member');
    }

    if (update.email && team.members.some(member => member.invite?.email === update.email)) {
      throw new ConflictException('An invitation has already been sent to this email address.');
    }

    if (update.email) {
      const user = await this.usersService.findByEmail(update.email);
      if (user && team.members.some(member => member.user && member.user.equals(user._id))) {
        throw new ConflictException('A user with this email address already exists.');
      }
    }

    if (update.email) {
      set['members.$.invite.email'] = update.email;
    }

    await this.teamModel
      .updateOne(
        { _id: team._id, 'members._id': memberId },
        {
          $set: set,
        },
      )
      .exec();

    if (update.role === 'owner' && !accessorMember._id.equals(memberId)) {
      await this.teamModel.updateOne(
        { _id: team._id, 'members._id': accessorMember._id },
        {
          $set: {
            'members.$.role': 'co-owner',
          },
        },
      );
    }

    if (update.email) {
      this.resendInvite(teamId, memberId, accessorUserId, lang);
    }
  }

  async updateTask(
    teamId: Types.ObjectId,
    memberId: Types.ObjectId,
    taskId: Types.ObjectId,
    update: UpdateTeamTaskDto,
    accessorUserId: Types.ObjectId,
  ) {
    const { team, accessorMember } = await this.accessTeamUsingMember(teamId, accessorUserId);

    const targetMember = team.members.find(member => member._id.equals(memberId));

    const doesAccessorHaveOwnerRights = doesTeamMemberHaveOwnerRights(accessorMember);

    if (!doesAccessorHaveOwnerRights && !targetMember._id.equals(accessorMember._id)) {
      throw new ForbiddenException("You can't update tasks of other members unless you have owner rights.");
    }

    const targetTask = targetMember.tasks.id(taskId);

    if (!targetTask) {
      throw new NotFoundException();
    }

    if (targetTask.submitted && update.data) {
      throw new ConflictException('Cannot update the data of a submitted task');
    }

    if (targetTask.submitted && update.submitted === false) {
      throw new ConflictException('Cannot unsubmit a submitted task');
    }

    if (targetTask.applied && update.applied === false) {
      throw new ConflictException('Cannot unapply an applied task');
    }

    if (update.weights && team.lockedWeightTasks && !doesAccessorHaveOwnerRights) {
      throw new ConflictException('Objective Weights tasks are currently in readonly mode');
    }

    if (update.opinions && team.lockedOpinionTasks && !doesAccessorHaveOwnerRights) {
      throw new ConflictException('Opinion tasks are currently in readonly mode');
    }

    const set: Record<string, unknown> = {};

    if (update.data != null) {
      set['data'] = update.data;
    }

    if (update.submitted != null) {
      set['submitted'] = update.submitted;
    }

    if (update.applied != null) {
      set['applied'] = update.applied;
    }

    if (update.weights != null) {
      set['weights'] = update.weights;
    }

    if (update.opinions != null) {
      set['opinions'] = update.opinions;
    }

    // update the task
    await this.teamModel.updateOne({ _id: team._id }, [
      {
        $set: {
          members: {
            $map: {
              input: '$members',
              as: 'm',
              in: {
                $mergeObjects: [
                  '$$m',
                  {
                    tasks: {
                      $map: {
                        input: '$$m.tasks',
                        as: 't',
                        in: {
                          $cond: [
                            {
                              $and: [{ $eq: ['$$t.type', targetTask.type] }, { $eq: ['$$t._id', taskId] }],
                            },
                            {
                              $mergeObjects: ['$$t', set],
                            },
                            '$$t',
                          ],
                        },
                      },
                    },
                  },
                ],
              },
            },
          },
        },
      },
    ]);
  }

  async assignTask(teamId: Types.ObjectId, memberId: Types.ObjectId, dto: AssignTaskDto, accessorUserId: Types.ObjectId) {
    const task = dto.task;
    const { team } = await this.accessTeamUsingOwnerLike(teamId, accessorUserId);

    const member = team.members.id(memberId);

    if (!member) {
      throw new NotFoundException();
    }

    const isSingletonTask = TeamTaskSingletonMap[task.type];

    if (isSingletonTask && member.tasks.some(assignedTask => assignedTask.type === task.type)) {
      throw new ConflictException('Cannot assign a singleton task to a member that already has that task');
    }

    const newTaskId = new Types.ObjectId();

    const taskData: Record<string, unknown> = {
      _id: newTaskId,
      type: task.type,
    };

    if (task.type === 'objective-scale') {
      taskData['objectiveUUID'] = task.objectiveUUID;
    }

    if (task.type === 'objective-scale') {
      const mainProject = readText(team.mainProjectData);

      const objectiveUUID = task.objectiveUUID;

      const objective = mainProject.objectives.find(o => o.uuid === objectiveUUID);

      if (!objective) {
        throw new BadRequestException('objective-not-found');
      }

      const taskDecisionData = new DecisionData();
      taskDecisionData.addObjective(objective);

      taskData['data'] = dataToText(taskDecisionData, version);
    } else if (task.type === 'objective-brainstorming') {
      taskData['data'] = dataToText(new DecisionData(), version);
    }

    const newTeam = await this.teamModel.findOneAndUpdate(
      { _id: team._id, 'members._id': memberId },
      {
        $push: {
          'members.$.tasks': taskData,
        },
      },
      { new: true, projection: ['members'] },
    );

    return this.toTaskDto(newTeam.members.id(memberId).tasks.id(newTaskId));
  }

  async deleteTask(teamId: Types.ObjectId, memberId: Types.ObjectId, taskId: Types.ObjectId, accessorUserId: Types.ObjectId) {
    const { team } = await this.accessTeamUsingOwnerLike(teamId, accessorUserId);

    const member = team.members.id(memberId);

    if (!member) {
      throw new NotFoundException();
    }

    const task = member.tasks.id(taskId);

    if (!task) {
      throw new NotFoundException();
    }

    await this.teamModel.updateOne(
      { _id: team._id, 'members._id': memberId },
      {
        $pull: {
          'members.$.tasks': {
            _id: taskId,
          },
        },
      },
    );
  }

  async takeEditToken(teamId: Types.ObjectId, accessorUserId: Types.ObjectId) {
    const { team, accessorMember } = await this.accessTeamUsingEditor(teamId, accessorUserId);

    const teamDto = await this.toTeamDto(team, null);

    const editTokenInfo = teamDto.editTokenInfo;

    if (!teamDto.canMemberTakeEditToken(accessorMember).canTake) {
      throw new ForbiddenException('Cannot take edit token from current holder');
    }

    const takingTheTokenFromSomeone = editTokenInfo.state === 'valid';

    // If the token expired or we are taking it from someone
    if (editTokenInfo.state === 'expired' || takingTheTokenFromSomeone) {
      const tokenEndDate = editTokenInfo.state === 'expired' ? editTokenInfo.expiredAt : new Date();

      await this.teamModel.updateOne(
        {
          _id: team._id,
        },
        {
          $set: {
            'accessLog.editToken.$[log].to': tokenEndDate,
          },
        },
        {
          arrayFilters: [{ 'log.to': null }],
        },
      );
    }

    await team.updateOne({
      $push: {
        'accessLog.editToken': {
          member: accessorMember._id,
          from: new Date(),
          to: null,
        },
      },
    });

    const newTeam = await this.teamModel.findOneAndUpdate(
      { _id: team._id },
      {
        $set: {
          editToken: {
            holder: accessorMember._id,
            expiresAt: new Date(Date.now() + TEAM_EDIT_RIGHT_DURATION),
          },
        },
      },
      { populate: 'editToken', new: true },
    );

    return plainToInstance(TeamEditTokenDto, newTeam.editToken);
  }

  async returnEditToken(teamId: Types.ObjectId, accessorUserId: Types.ObjectId) {
    const { team, accessorMember } = await this.accessTeamUsingMember(teamId, accessorUserId);

    if (!team.editToken?.holder.equals(accessorMember._id)) {
      throw new ForbiddenException();
    }

    await this.teamModel.updateOne(
      { _id: team._id, 'editToken.holder': accessorMember._id, 'accessLog.editToken': { $exists: true } },
      {
        $set: {
          'accessLog.editToken.$[log].to': new Date(),
        },
      },
      { arrayFilters: [{ 'log.to': null }] },
    );

    await this.teamModel.updateOne(
      { _id: team._id, 'editToken.holder': accessorMember._id },
      {
        $set: {
          editToken: null,
        },
      },
    );
  }

  async updateImportance(teamId: Types.ObjectId, update: UpdateImportanceDto, accessorUserId: Types.ObjectId) {
    const { team, accessorMember } = await this.accessTeamUsingOwnerLike(teamId, accessorUserId);

    const validateUpdate = isValidImportance(
      team.members.length,
      update.memberImportanceValues,
      update.importanceShares,
      update.importanceShareNames,
    );

    if (!validateUpdate[0]) {
      throw new BadRequestException(validateUpdate[1]);
    }

    team.importanceShares = update.importanceShares;
    team.importanceShareNames = update.importanceShareNames;

    team.members.forEach((member, memberIndex) => {
      member.importance = update.memberImportanceValues[memberIndex];
    });

    await team.save();

    return this.toTeamDto(team, accessorMember._id);
  }

  async accessTeamUsingMember(teamId: Types.ObjectId, memberUserId: Types.ObjectId, projection?: string[]) {
    if (projection) {
      projection.push('members.user');
      projection.push('members.isEditor');
      projection.push('owner');
      projection.push('members._id');
    }

    const team = await this.teamModel.findById(teamId, projection);

    if (!team) {
      throw new NotFoundException();
    }

    const accessorMember = team.members.find(member => member.user?.equals(memberUserId));
    if (!accessorMember) {
      throw new ForbiddenException();
    }

    return { team, accessorMember };
  }

  private async accessTeamUsingEditor(teamId: Types.ObjectId, editorUserId: Types.ObjectId, projection?: string[]) {
    const { team, accessorMember } = await this.accessTeamUsingMember(teamId, editorUserId, projection);

    if (accessorMember.role === 'user') {
      throw new ForbiddenException();
    }

    return { team, accessorMember };
  }

  private async accessTeamUsingOwnerLike(teamId: Types.ObjectId, ownerUserId: Types.ObjectId, projection?: string[]) {
    const { team, accessorMember } = await this.accessTeamUsingMember(teamId, ownerUserId, projection);

    if (!doesTeamMemberHaveOwnerRights(accessorMember)) {
      throw new ForbiddenException();
    }

    return { team, accessorMember };
  }

  private async findTeamUsingToken(inviteToken: string) {
    const team = await this.teamModel.findOne({
      'members.invite.inviteToken': inviteToken,
    });

    if (!team) {
      throw new NotFoundException();
    }

    return team;
  }

  private async toTeamDto(doc: TeamDocument, forMember: Types.ObjectId) {
    const dto = plainToClass(TeamDto, await doc.populate([{ path: 'members.user', select: ['id', 'name', 'email'] }]), {
      groups: forMember != null ? this.getExposeGroupsFor(doc, forMember) : [...TeamRoles],
      exposeUnsetFields: false,
    });

    this.includeAggregatedProject(dto);

    return dto;
  }

  private getExposeGroupsFor(doc: TeamDocument, accessorMemberId: Types.ObjectId) {
    const groups = [accessorMemberId.toString()];

    const member = doc.members.find(member => member._id.equals(accessorMemberId));

    const roleIndex = TeamRoles.indexOf(member.role);

    groups.push(...TeamRoles.filter((_, index) => index >= roleIndex));

    return groups;
  }

  private includeAggregatedProject(dto: TeamDto) {
    if (!dto.mainProjectData) {
      return dto;
    }

    const teamCalc = new TeamCalc(dto);

    // Version from package.json
    dto.mainProjectData = dataToText(teamCalc.aggregatedProject, version);

    return dto;
  }

  private toTaskDto(task: TeamTask) {
    return plainToInstance(TaskWrapperDto, { task }).task;
  }
}
