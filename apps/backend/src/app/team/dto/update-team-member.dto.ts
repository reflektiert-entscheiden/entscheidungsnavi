import { TeamMemberDto } from '@entscheidungsnavi/api-types';
import { PartialType, PickType } from '@nestjs/mapped-types';
import { IsBoolean, IsOptional, IsString } from 'class-validator';

export class UpdateTeamMemberDto extends PartialType(PickType(TeamMemberDto, ['role'] as const)) {
  @IsOptional()
  @IsBoolean()
  isVirtual?: boolean;

  @IsOptional()
  @IsString()
  email?: string;
}
