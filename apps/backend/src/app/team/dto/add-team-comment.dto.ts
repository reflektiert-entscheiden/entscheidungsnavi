import { TeamCommentDto } from '@entscheidungsnavi/api-types';
import { PickType } from '@nestjs/mapped-types';

export class AddTeamCommentDto extends PickType(TeamCommentDto, ['objectId', 'content']) {}
