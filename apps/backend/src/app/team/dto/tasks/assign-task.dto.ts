import { Type } from 'class-transformer';
import { ValidateNested } from 'class-validator';
import { PickType } from '@nestjs/mapped-types';
import {
  AbstractTeamTaskDto,
  TeamTaskObjectiveBrainstormingDto,
  TeamTaskObjectiveScaleDto,
  TeamTaskObjectiveWeightsDto,
  TeamTaskOpinionDto,
} from '@entscheidungsnavi/api-types';

class AssignTeamTaskObjectiveScaleDto extends PickType(TeamTaskObjectiveScaleDto, ['objectiveUUID', 'type']) {}
class AssignTeamTaskObjectiveBrainstormingDto extends PickType(TeamTaskObjectiveBrainstormingDto, ['type']) {}
class AssignTeamTaskObjectiveWeightsDto extends PickType(TeamTaskObjectiveWeightsDto, ['type']) {}
class AssignTeamTaskOpinionDto extends PickType(TeamTaskOpinionDto, ['type']) {}

type AssignTeamTaskDto =
  | AssignTeamTaskObjectiveScaleDto
  | AssignTeamTaskObjectiveBrainstormingDto
  | AssignTeamTaskObjectiveWeightsDto
  | AssignTeamTaskOpinionDto;

export class AssignTaskDto {
  @ValidateNested()
  @Type(() => AbstractTeamTaskDto, {
    keepDiscriminatorProperty: true,
    discriminator: {
      property: 'type',
      subTypes: [
        { value: AssignTeamTaskObjectiveScaleDto, name: 'objective-scale' },
        { value: AssignTeamTaskObjectiveBrainstormingDto, name: 'objective-brainstorming' },
        { value: AssignTeamTaskObjectiveWeightsDto, name: 'objective-weights' },
        { value: AssignTeamTaskOpinionDto, name: 'opinion' },
      ],
    },
  })
  task: AssignTeamTaskDto;
}
