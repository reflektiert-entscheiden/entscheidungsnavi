import { PartialType, PickType } from '@nestjs/mapped-types';
import { IsObject, IsOptional, IsString } from 'class-validator';
import { AbstractTeamTaskDto, AlternativeOpinions } from '@entscheidungsnavi/api-types';

export class UpdateTeamTaskDto extends PartialType(PickType(AbstractTeamTaskDto, ['submitted', 'applied'] as const)) {
  @IsOptional()
  @IsString()
  data: string;

  @IsOptional()
  @IsObject()
  weights: Record<string, number>;

  @IsOptional()
  @IsObject()
  opinions: AlternativeOpinions;
}
