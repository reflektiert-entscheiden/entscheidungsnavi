import { TeamDto } from '@entscheidungsnavi/api-types';
import { PartialType, PickType } from '@nestjs/mapped-types';

export class UpdateTeamDto extends PartialType(
  PickType(TeamDto, [
    'whiteboard',
    'canTakeEditRightFrom',
    'editRightExpiresFor',
    'mainProjectData',
    'lockedWeightTasks',
    'lockedOpinionTasks',
    'name',
  ] as const),
) {}
