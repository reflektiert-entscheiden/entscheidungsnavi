import { IsArray, IsNumber, IsString, Validate, ValidatorConstraint, ValidatorConstraintInterface } from 'class-validator';

@ValidatorConstraint({ name: 'IsNonEmpty2DNumberArray', async: false })
export class IsNonEmpty2DNumberArray implements ValidatorConstraintInterface {
  errors: string[] = [];

  validate(value: unknown) {
    this.errors = [];

    if (!Array.isArray(value)) {
      this.errors.push('value should be an array');
      return false;
    }

    if (value.length === 0) {
      this.errors.push('value should not be empty');
      return false;
    }

    if (value.some(v => !Array.isArray(v) || v.length === 0 || v.some(vv => typeof vv !== 'number'))) {
      this.errors.push('value should be a 2D number array');
      return false;
    }

    return true;
  }

  defaultMessage() {
    return this.errors.join('; ');
  }
}

export class UpdateImportanceDto {
  @IsArray()
  @IsNumber({}, { each: true })
  importanceShares: number[];

  @IsArray()
  @IsString({ each: true })
  importanceShareNames: string[];

  @Validate(IsNonEmpty2DNumberArray)
  memberImportanceValues: number[][];
}
