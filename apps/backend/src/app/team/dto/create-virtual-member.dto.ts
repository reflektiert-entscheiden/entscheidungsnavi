import { IsNotEmpty, IsString } from 'class-validator';

export class CreateVirtualMemberDto {
  @IsString()
  @IsNotEmpty()
  name: string;
}
