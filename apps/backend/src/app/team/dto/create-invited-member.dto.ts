import { IsNotEmpty, IsString } from 'class-validator';

export class CreateInvitedMemberDto {
  @IsString()
  @IsNotEmpty()
  email: string;
}
