import {
  Alternative,
  CompositeUserDefinedInfluenceFactor,
  UserDefinedInfluenceFactor,
  DecisionData,
  InfluenceFactor,
  Objective,
  Outcome,
  PredefinedInfluenceFactor,
  SimpleUserDefinedInfluenceFactor,
  Screw,
} from '@entscheidungsnavi/decision-data';
import { assertUnreachable } from '@entscheidungsnavi/tools/assert-unreachable';
import { ConflictException, NotFoundException } from '@nestjs/common';
import { v4 as uuidv4 } from 'uuid';

export function transferBetweenProjects(decisionDataSource: DecisionData, decisionDataDestination: DecisionData, objectId: string) {
  const { object: objectToTransfer, index: objectToTransferIndex } = decisionDataSource.findObject(objectId);

  if (!objectToTransfer) {
    throw new NotFoundException();
  }

  objectToTransfer.uuid = uuidv4();

  if (objectToTransfer instanceof Objective) {
    getOrCreateMatchingObjective(decisionDataDestination, objectToTransfer);
    for (const alternativeRow of decisionDataSource.outcomes) {
      transferOutcome(decisionDataSource, decisionDataDestination, alternativeRow[objectToTransferIndex]);
    }
  } else if (objectToTransfer instanceof Alternative) {
    getOrCreateMatchingAlternative(decisionDataDestination, objectToTransfer);
    for (const outcomeToTransfer of decisionDataSource.outcomes[objectToTransferIndex]) {
      transferOutcome(decisionDataSource, decisionDataDestination, outcomeToTransfer);
    }
  } else if (objectToTransfer instanceof SimpleUserDefinedInfluenceFactor) {
    decisionDataDestination.addInfluenceFactor(objectToTransfer);
  } else if (objectToTransfer instanceof CompositeUserDefinedInfluenceFactor) {
    objectToTransfer.baseFactors = [
      getOrCreateMatchingInfluenceFactor(decisionDataDestination, objectToTransfer.baseFactors[0]),
      getOrCreateMatchingInfluenceFactor(decisionDataDestination, objectToTransfer.baseFactors[1]),
    ];
    decisionDataDestination.addInfluenceFactor(objectToTransfer);
  } else if (objectToTransfer instanceof Outcome) {
    const error = transferOutcome(decisionDataSource, decisionDataDestination, objectToTransfer);

    if (error) {
      throw new ConflictException(error);
    }
  } else if (objectToTransfer instanceof Screw) {
    decisionDataDestination.hintAlternatives.screws.push(objectToTransfer);
  } else {
    assertUnreachable(objectToTransfer);
  }
}

function transferOutcome(
  decisionDataSource: DecisionData,
  decisionDataDestination: DecisionData,
  outcome: Outcome,
): 'missing-alternative' | 'missing-objective' | undefined {
  const { alternativeIndex, objectiveIndex } = getIndicesOfOutcome(decisionDataSource, outcome);

  const targetAlternativeIndex = getMatchingAlternative(decisionDataDestination, decisionDataSource.alternatives[alternativeIndex]);

  if (targetAlternativeIndex < 0) {
    return 'missing-alternative' as const;
  }

  const targetObjectiveIndex = getMatchingObjective(decisionDataDestination, decisionDataSource.objectives[objectiveIndex]);

  if (targetObjectiveIndex < 0) {
    return 'missing-objective' as const;
  }

  const otherOutcome = decisionDataDestination.outcomes[targetAlternativeIndex][targetObjectiveIndex];
  const originalUUID = otherOutcome.uuid;

  otherOutcome.copyBack(outcome);

  otherOutcome.uuid = originalUUID;

  if (otherOutcome.influenceFactor) {
    const targetInfluenceFactor = getOrCreateMatchingInfluenceFactor(decisionDataDestination, outcome.influenceFactor);

    otherOutcome.setInfluenceFactor(targetInfluenceFactor);
  }
}

function getIndicesOfOutcome(decisionData: DecisionData, outcome: Outcome) {
  for (let alternativeIndex = 0; alternativeIndex < decisionData.alternatives.length; alternativeIndex++) {
    for (let objectiveIndex = 0; objectiveIndex < decisionData.outcomes[alternativeIndex].length; objectiveIndex++) {
      if (decisionData.outcomes[alternativeIndex][objectiveIndex] === outcome) {
        return { alternativeIndex, objectiveIndex };
      }
    }
  }

  return null;
}

function getMatchingAlternative(decisionData: DecisionData, baseAlternative: Alternative) {
  for (let alternativeIndex = 0; alternativeIndex < decisionData.alternatives.length; alternativeIndex++) {
    if (decisionData.alternatives[alternativeIndex].name === baseAlternative.name) {
      return alternativeIndex;
    }
  }

  return -1;
}

function getOrCreateMatchingAlternative(decisionData: DecisionData, baseAlternative: Alternative) {
  const existing = getMatchingAlternative(decisionData, baseAlternative);

  if (existing >= 0) {
    return existing;
  }

  decisionData.addAlternative({ alternative: baseAlternative });

  return decisionData.alternatives.length - 1;
}

function getOrCreateMatchingObjective(decisionData: DecisionData, baseObjective: Objective) {
  const existing = getMatchingObjective(decisionData, baseObjective);

  if (existing >= 0) {
    return existing;
  }

  decisionData.addObjective(baseObjective);

  return decisionData.objectives.length - 1;
}

function getMatchingObjective(decisionData: DecisionData, baseObjective: Objective) {
  for (let objectiveIndex = 0; objectiveIndex < decisionData.objectives.length; objectiveIndex++) {
    const objective = decisionData.objectives[objectiveIndex];
    if (objective.name === baseObjective.name && objective.hasSameScaleAs(baseObjective)) {
      return objectiveIndex;
    }
  }

  return -1;
}

function getOrCreateMatchingInfluenceFactor(
  decisionData: DecisionData,
  baseInfluenceFactor: UserDefinedInfluenceFactor,
): UserDefinedInfluenceFactor;
function getOrCreateMatchingInfluenceFactor(decisionData: DecisionData, baseInfluenceFactor: InfluenceFactor): InfluenceFactor;
function getOrCreateMatchingInfluenceFactor(decisionData: DecisionData, baseInfluenceFactor: InfluenceFactor): InfluenceFactor {
  if (baseInfluenceFactor instanceof PredefinedInfluenceFactor) {
    return baseInfluenceFactor;
  } else if (baseInfluenceFactor instanceof SimpleUserDefinedInfluenceFactor) {
    const matchingIF = decisionData.influenceFactors.find(
      influenceFactor =>
        influenceFactor instanceof SimpleUserDefinedInfluenceFactor &&
        influenceFactor.name === baseInfluenceFactor.name &&
        influenceFactor.states.length === baseInfluenceFactor.states.length &&
        influenceFactor.states.every(
          (state, index) =>
            state.name === baseInfluenceFactor.states[index].name && state.probability === baseInfluenceFactor.states[index].probability,
        ) &&
        // The user-defined influence factor MAY NOT BE part of a composite user-defined influence factor
        !decisionData.influenceFactors.some(
          customIF => customIF instanceof CompositeUserDefinedInfluenceFactor && customIF.baseFactors.includes(influenceFactor),
        ),
    );

    if (matchingIF) {
      return matchingIF;
    } else {
      decisionData.addInfluenceFactor(baseInfluenceFactor);
      return baseInfluenceFactor;
    }
  } else if (baseInfluenceFactor instanceof CompositeUserDefinedInfluenceFactor) {
    const firstBase = getOrCreateMatchingInfluenceFactor(decisionData, baseInfluenceFactor.baseFactors[0]),
      secondBase = getOrCreateMatchingInfluenceFactor(decisionData, baseInfluenceFactor.baseFactors[1]);

    const matchingIF = decisionData.influenceFactors.find(
      influenceFactor =>
        influenceFactor instanceof CompositeUserDefinedInfluenceFactor &&
        influenceFactor.name === baseInfluenceFactor.name &&
        influenceFactor.baseFactors[0] === firstBase &&
        influenceFactor.baseFactors[1] === secondBase &&
        // The influence factor MAY NOT BE part of a composite user-defined influence factor
        !decisionData.influenceFactors.some(
          customIF => customIF instanceof CompositeUserDefinedInfluenceFactor && customIF.baseFactors.includes(influenceFactor),
        ),
    );

    if (matchingIF) {
      return matchingIF;
    } else {
      baseInfluenceFactor.baseFactors = [firstBase, secondBase];
      decisionData.addInfluenceFactor(baseInfluenceFactor);
      return baseInfluenceFactor;
    }
  } else {
    assertUnreachable(baseInfluenceFactor);
  }
}
