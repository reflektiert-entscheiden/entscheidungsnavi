import { Language } from '@entscheidungsnavi/api-types';
import { Body, Controller, Delete, Get, Param, Patch, Post, Query, Req, UseGuards, UseInterceptors } from '@nestjs/common';
import { Types } from 'mongoose';
import { Lang } from '../common/language.decorator';
import { NotFoundInterceptor } from '../common/not-found.interceptor';
import { ParseMongoIdPipe } from '../common/parse-mogo-id.pipe';
import { AuthenticatedRequest, LoginGuard } from '../auth/login.guard';
import { AddTeamCommentDto } from './dto/add-team-comment.dto';
import { CreateTeamDto } from './dto/create-team.dto';
import { TeamsService } from './teams.service';
import { UpdateTeamDto } from './dto/update-team.dto';
import { UpdateTeamMemberDto } from './dto/update-team-member.dto';
import { UpdateTeamTaskDto } from './dto/tasks/update-task.dto';
import { UpdateImportanceDto } from './dto/update-importance.dto';
import { CreateVirtualMemberDto } from './dto/create-virtual-member.dto';
import { AssignTaskDto } from './dto/tasks/assign-task.dto';
import { CreateInvitedMemberDto } from './dto/create-invited-member.dto';

@UseGuards(LoginGuard)
@Controller('teams')
export class TeamsController {
  constructor(private teamService: TeamsService) {}

  @Post()
  async create(@Req() request: AuthenticatedRequest, @Body() create: CreateTeamDto) {
    return await this.teamService.create(request.user._id, create);
  }

  @Get()
  async list(@Req() request: AuthenticatedRequest, @Query('onlyWhereOwner') onlyWhereUserIsOwner?: boolean) {
    return await this.teamService.getProjectsForUser(request.user._id, onlyWhereUserIsOwner);
  }

  @Delete(':id')
  async delete(@Req() request: AuthenticatedRequest, @Param('id', ParseMongoIdPipe) teamId: Types.ObjectId) {
    return await this.teamService.deleteTeam(teamId, request.user._id);
  }

  @UseInterceptors(NotFoundInterceptor)
  @Get(':id')
  async getOne(@Req() request: AuthenticatedRequest, @Param('id', ParseMongoIdPipe) teamId: Types.ObjectId) {
    return await this.teamService.findById(teamId, request.user._id);
  }

  @Post(':id/members/virtual')
  async createVirtualMember(
    @Req() request: AuthenticatedRequest,
    @Param('id', ParseMongoIdPipe) teamId: Types.ObjectId,
    @Body() dto: CreateVirtualMemberDto,
  ) {
    return await this.teamService.createVirtualMember(teamId, request.user._id, dto);
  }

  @Post(':id/members/invited')
  async createInvitedMember(
    @Req() request: AuthenticatedRequest,
    @Param('id', ParseMongoIdPipe) teamId: Types.ObjectId,
    @Body() dto: CreateInvitedMemberDto,
    @Lang() lang: Language,
  ) {
    return await this.teamService.createInvitedMember(teamId, request.user._id, dto.email, lang);
  }

  @Post(':id/members/:memberId/resend')
  async resendInvite(
    @Req() request: AuthenticatedRequest,
    @Param('id', ParseMongoIdPipe) teamId: Types.ObjectId,
    @Param('memberId', ParseMongoIdPipe) memberId: Types.ObjectId,
    @Lang() lang: Language,
  ) {
    await this.teamService.resendInvite(teamId, memberId, request.user._id, lang);
  }

  @UseInterceptors(NotFoundInterceptor)
  @Get('members/invited/:token')
  async getInviteInfo(@Param('token') token: string) {
    return await this.teamService.getInviteInfoFromToken(token);
  }

  @Post('members/invited/:token')
  async join(@Req() request: AuthenticatedRequest, @Param('token') token: string) {
    return await this.teamService.joinUsingToken(request.user._id, token);
  }

  @Post(':teamId/leave')
  async leave(@Req() request: AuthenticatedRequest, @Param('teamId', ParseMongoIdPipe) teamId: Types.ObjectId) {
    return await this.teamService.leaveTeam(teamId, request.user._id);
  }

  @Get(':teamId/comments')
  async getComments(
    @Req() request: AuthenticatedRequest,
    @Param('teamId', ParseMongoIdPipe) teamId: Types.ObjectId,
    @Query('forObject') objectId?: string,
  ) {
    return await this.teamService.getComments(teamId, request.user._id, objectId);
  }

  @Post(':teamId/comments')
  async comment(
    @Req() request: AuthenticatedRequest,
    @Param('teamId', ParseMongoIdPipe) teamId: Types.ObjectId,
    @Body() comment: AddTeamCommentDto,
  ) {
    return await this.teamService.comment(teamId, request.user._id, comment.objectId, comment.content);
  }

  @Delete(':teamId/comments/:commentId')
  async deleteComment(
    @Req() request: AuthenticatedRequest,
    @Param('teamId', ParseMongoIdPipe) teamId: Types.ObjectId,
    @Param('commentId', ParseMongoIdPipe) commentId: Types.ObjectId,
  ) {
    return await this.teamService.deleteComment(teamId, request.user._id, commentId);
  }

  @Patch(':teamId')
  async updateTeam(
    @Req() request: AuthenticatedRequest,
    @Param('teamId', ParseMongoIdPipe) teamId: Types.ObjectId,
    @Body() update: UpdateTeamDto,
  ) {
    return await this.teamService.updateTeam(teamId, update, request.user._id);
  }

  @Post(':teamId/editToken/take')
  async takeEditToken(@Req() request: AuthenticatedRequest, @Param('teamId', ParseMongoIdPipe) teamId: Types.ObjectId) {
    return await this.teamService.takeEditToken(teamId, request.user._id);
  }

  @Delete(':teamId/editToken')
  async returnEditToken(@Req() request: AuthenticatedRequest, @Param('teamId', ParseMongoIdPipe) teamId: Types.ObjectId) {
    return await this.teamService.returnEditToken(teamId, request.user._id);
  }

  @Patch(':teamId/members/:memberId')
  async updateMember(
    @Req() request: AuthenticatedRequest,
    @Param('teamId', ParseMongoIdPipe) teamId: Types.ObjectId,
    @Param('memberId', ParseMongoIdPipe) memberId: Types.ObjectId,
    @Body() update: UpdateTeamMemberDto,
    @Lang() lang: Language,
  ) {
    return await this.teamService.updateTeamMember(teamId, memberId, update, request.user._id, lang);
  }

  @Delete(':teamId/members/:memberId')
  async deleteMember(
    @Req() request: AuthenticatedRequest,
    @Param('teamId', ParseMongoIdPipe) teamId: Types.ObjectId,
    @Param('memberId', ParseMongoIdPipe) memberId: Types.ObjectId,
  ) {
    return await this.teamService.deleteTeamMember(teamId, memberId, request.user._id);
  }

  @Post(':teamId/members/:memberId/tasks')
  async assignTask(
    @Req() request: AuthenticatedRequest,
    @Param('teamId', ParseMongoIdPipe) teamId: Types.ObjectId,
    @Param('memberId', ParseMongoIdPipe) memberId: Types.ObjectId,
    @Body() task: AssignTaskDto,
  ) {
    return await this.teamService.assignTask(teamId, memberId, task, request.user._id);
  }

  @Patch(':teamId/members/:memberId/tasks/:taskId')
  async updateTask(
    @Req() request: AuthenticatedRequest,
    @Param('teamId', ParseMongoIdPipe) teamId: Types.ObjectId,
    @Param('memberId', ParseMongoIdPipe) memberId: Types.ObjectId,
    @Param('taskId', ParseMongoIdPipe) taskId: Types.ObjectId,
    @Body() update: UpdateTeamTaskDto,
  ) {
    return await this.teamService.updateTask(teamId, memberId, taskId, update, request.user._id);
  }

  @Delete(':teamId/members/:memberId/tasks/:taskId')
  async deleteTask(
    @Req() request: AuthenticatedRequest,
    @Param('teamId', ParseMongoIdPipe) teamId: Types.ObjectId,
    @Param('memberId', ParseMongoIdPipe) memberId: Types.ObjectId,
    @Param('taskId', ParseMongoIdPipe) taskId: Types.ObjectId,
  ) {
    return await this.teamService.deleteTask(teamId, memberId, taskId, request.user._id);
  }

  @Post(':teamId/importance')
  async updateImportance(
    @Req() request: AuthenticatedRequest,
    @Param('teamId', ParseMongoIdPipe) teamId: Types.ObjectId,
    @Body() update: UpdateImportanceDto,
  ) {
    return await this.teamService.updateImportance(teamId, update, request.user._id);
  }

  @Get(':teamId/editToken')
  async getEditToken(@Req() request: AuthenticatedRequest, @Param('teamId', ParseMongoIdPipe) teamId: Types.ObjectId) {
    return await this.teamService.getEditToken(teamId, request.user.id);
  }
}
