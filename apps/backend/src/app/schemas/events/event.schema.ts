import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { HydratedDocument, Schema as MongooseSchema, Types } from 'mongoose';
import { QuestionnairePage, QuestionnairePageSchema } from './event-questionnaire.schema';
import {
  EventFreeTextConfig,
  EventFreeTextConfigSchema,
  EventProjectRequirements,
  EventProjectRequirementsSchema,
} from './event-types.schema';

export type EventDocument = HydratedDocument<Event>;

@Schema({ timestamps: true })
export class Event {
  readonly id: string;

  @Prop({ required: true, unique: true })
  name: string;

  @Prop({
    lowercase: true,
    trim: true,
    minlength: 3,
    // Unique if not null: https://stackoverflow.com/a/43804473
    index: {
      unique: true,
      partialFilterExpression: { code: { $type: 'string' } },
    },
    default: null,
  })
  code: string;

  @Prop({ required: true, ref: 'User', type: MongooseSchema.Types.ObjectId })
  owner: Types.ObjectId;

  @Prop({ required: true, type: [{ type: MongooseSchema.Types.ObjectId, ref: 'User' }], default: (): Types.ObjectId[] => [] })
  editors: Types.ObjectId[];

  @Prop({ required: true, type: [{ type: MongooseSchema.Types.ObjectId, ref: 'User' }], default: (): Types.ObjectId[] => [] })
  viewers: Types.ObjectId[];

  @Prop()
  startDate?: Date;

  @Prop()
  endDate?: Date;

  @Prop({ required: true, type: EventProjectRequirementsSchema, default: () => ({}) })
  projectRequirements: EventProjectRequirements;

  @Prop({
    required: true,
    type: [QuestionnairePageSchema],
    default: (): QuestionnairePage[] => [],
  })
  questionnaire: QuestionnairePage[];

  @Prop({ required: true, default: false })
  questionnaireReleased: boolean;

  @Prop({ required: true, type: EventFreeTextConfigSchema, default: () => ({}) })
  freeTextConfig: EventFreeTextConfig;

  @Prop({ required: true, default: true, type: Boolean })
  requireDataUsageAuthorization: boolean;

  readonly createdAt: Date;
  readonly updatedAt: Date;
}

export const EventSchema = SchemaFactory.createForClass(Event);
