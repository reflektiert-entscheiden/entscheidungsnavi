import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { HydratedDocument, Schema as MongooseSchema, Types } from 'mongoose';

export type EventRegistrationDocument = HydratedDocument<EventRegistration>;

@Schema({ timestamps: true })
export class EventRegistration {
  readonly id: string;

  @Prop({ required: true, type: MongooseSchema.Types.ObjectId, ref: 'User' })
  user: Types.ObjectId;

  @Prop({ required: true, type: MongooseSchema.Types.ObjectId, ref: 'Event' })
  event: Types.ObjectId;

  @Prop({ default: false })
  submitted: boolean;

  @Prop()
  projectData?: string; // The actually submitted project data

  @Prop()
  freeText?: string;

  @Prop({ type: [[]] })
  questionnaireResponses?: (number | number[] | string | null)[][];

  readonly createdAt: Date;
  readonly updatedAt: Date;
}

export const EventRegistrationSchema = SchemaFactory.createForClass(EventRegistration);
// User/Event combination must be unique
EventRegistrationSchema.index({ user: 1, event: 1 }, { unique: true });
