import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { HydratedDocument } from 'mongoose';
import { PROJECT_SAVE_TYPES, ProjectSaveType } from '@entscheidungsnavi/api-types';

export type ProjectHistoryEntryDocument = HydratedDocument<ProjectHistoryEntry>;

@Schema({ timestamps: false })
export class ProjectHistoryEntry {
  readonly id: string;

  @Prop({ required: true })
  compressedPatch: Buffer;

  // The save type of the version we reach when applying this patch
  @Prop({ type: String, enum: PROJECT_SAVE_TYPES })
  saveType?: ProjectSaveType;

  // The timestamp of the version we reach when applying this patch
  @Prop({ required: true })
  versionTimestamp: Date;
}

export const ProjectHistoryEntrySchema = SchemaFactory.createForClass(ProjectHistoryEntry);
