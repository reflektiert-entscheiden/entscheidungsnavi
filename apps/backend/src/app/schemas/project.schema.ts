import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { PROJECT_SAVE_TYPES, ProjectSaveType } from '@entscheidungsnavi/api-types';
import { HydratedDocument, Schema as MongooseSchema, Types } from 'mongoose';

export type ProjectDocument = HydratedDocument<Project>;

@Schema({ timestamps: true })
export class Project {
  readonly id: string;

  @Prop({ required: true, type: MongooseSchema.Types.ObjectId, ref: 'User' })
  userId: Types.ObjectId;

  @Prop({ required: true })
  name: string;

  @Prop({ required: true })
  data: string;

  @Prop({ type: String, enum: PROJECT_SAVE_TYPES })
  dataSaveType?: ProjectSaveType;

  @Prop({ required: false })
  shareToken?: string;

  @Prop({ type: [{ type: MongooseSchema.Types.ObjectId, ref: 'ProjectHistoryEntry' }], default: [] })
  history: Types.ObjectId[];

  readonly createdAt: Date;
  readonly updatedAt: Date;
}

export const ProjectSchema = SchemaFactory.createForClass(Project);
