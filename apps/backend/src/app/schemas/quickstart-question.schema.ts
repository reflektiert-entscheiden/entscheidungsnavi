import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { HydratedDocument, Schema as MongooseSchema, Types } from 'mongoose';
import { LocalizedString, LocalizedStringSchema } from './common/localized-string.schema';

export type QuickstartQuestionDocument = HydratedDocument<QuickstartQuestion>;

@Schema({ timestamps: true })
export class QuickstartQuestion {
  readonly id: string;

  @Prop({ required: true, type: LocalizedStringSchema })
  name: LocalizedString;

  @Prop({ required: true, type: [{ type: MongooseSchema.Types.ObjectId, ref: 'QuickstartTag' }], default: [] })
  tags: Types.ObjectId[];

  // How many times the question was assigned a weight at all
  @Prop({ default: 0 })
  countAssigned: number;

  readonly createdAt: Date;
  readonly updatedAt: Date;
}

export const QuickstartQuestionSchema = SchemaFactory.createForClass(QuickstartQuestion);
