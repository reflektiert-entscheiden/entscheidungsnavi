import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Schema as MongooseSchema, Types } from 'mongoose';

export type TeamEditTokenDocument = TeamEditToken & Types.Subdocument;

@Schema()
export class TeamEditToken {
  @Prop({ type: MongooseSchema.Types.ObjectId, ref: 'TeamMember' })
  holder: Types.ObjectId;

  @Prop({ type: Date })
  expiresAt: Date;
}

export const TeamEditTokenSchema = SchemaFactory.createForClass(TeamEditToken);
