import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { TeamConfigTargetGroupValues } from '@entscheidungsnavi/api-types';
import { HydratedDocument, Types } from 'mongoose';
import { TeamMember, TeamMemberSchema } from './team-member.schema';
import { TeamEditToken, TeamEditTokenSchema } from './team-edit-token.schema';
import { TeamCommentDocument, TeamCommentSchema } from './team-comment.schema';
import { TeamAccessLog } from './team-access-log.schema';

export type TeamDocument = HydratedDocument<Team>;

@Schema({ timestamps: true })
export class Team {
  @Prop({ required: true, type: String })
  name: string;

  @Prop({ required: true, type: [TeamMemberSchema], default: [] })
  members: Types.DocumentArray<TeamMember>;

  @Prop({ required: true, type: [TeamCommentSchema], default: [] })
  comments: Types.DocumentArray<TeamCommentDocument>;

  @Prop({ required: true, type: String })
  mainProjectData: string;

  @Prop({ required: true, default: '', type: String })
  whiteboard: string;

  @Prop({ default: null, type: TeamEditTokenSchema })
  editToken: TeamEditToken;

  @Prop({ required: true, type: String, default: 'none', enum: TeamConfigTargetGroupValues })
  canTakeEditRightFrom: 'all' | 'editor' | 'none';

  @Prop({ required: true, type: String, default: 'editor', enum: TeamConfigTargetGroupValues })
  editRightExpiresFor: 'all' | 'editor' | 'none';

  @Prop({ required: true, default: [100], type: [Number] })
  importanceShares: number[];

  @Prop({ required: true, default: [''], type: [String] })
  importanceShareNames: string[];

  @Prop({ required: true, default: false, type: Boolean })
  lockedWeightTasks: boolean;

  @Prop({ required: true, default: false, type: Boolean })
  lockedOpinionTasks: boolean;

  @Prop({ required: true, default: () => ({}), type: TeamAccessLog })
  accessLog: TeamAccessLog;

  readonly createdAt: Date;
  readonly updatedAt: Date;
}

export const TeamSchema = SchemaFactory.createForClass(Team);
