import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Schema as MongooseSchema, Types } from 'mongoose';

export type TeamCommentDocument = TeamComment & Types.Subdocument;

@Schema({ timestamps: true })
export class TeamComment {
  @Prop({ required: true, type: MongooseSchema.Types.ObjectId, ref: 'TeamMember' })
  authorMember: Types.ObjectId;

  @Prop({ required: true, type: String })
  objectId: string;

  @Prop({ required: true, type: String })
  content: string;

  readonly createdAt: Date;
  readonly updatedAt: Date;
}

export const TeamCommentSchema = SchemaFactory.createForClass(TeamComment);
