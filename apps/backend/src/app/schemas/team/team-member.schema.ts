import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { TeamRole, TeamRoles } from '@entscheidungsnavi/api-types';
import { Schema as MongooseSchema, Types } from 'mongoose';
import {
  TeamTaskBaseSchema,
  TeamTaskObjectiveScaleSchema,
  TeamTaskObjectiveBrainstormingSchema,
  TeamTaskObjectiveWeightsSchema,
  TeamTaskOpinionSchema,
  TeamTask,
  TeamTaskBase,
} from './tasks/team-task.schema';
import { TeamInviteSchema, TeamInvite } from './team-invite.schema';

export type TeamMemberDocument = TeamMember & Types.Subdocument;

@Schema({ timestamps: true })
export class TeamMember {
  readonly id: string;

  @Prop({ type: Types.ObjectId, ref: 'User' })
  user: Types.ObjectId;

  @Prop({ required: true, default: [], type: [TeamTaskBaseSchema] })
  tasks: Types.DocumentArray<TeamTaskBase & TeamTask>;

  @Prop({ type: [MongooseSchema.Types.ObjectId], ref: 'TeamComment' })
  unreadComments: Types.Array<Types.ObjectId>;

  @Prop({ required: true, default: [], type: [Number] })
  importance: number[];

  @Prop({ type: String })
  name: string;

  @Prop({ required: true, type: String, enum: TeamRoles, default: 'user' })
  role: TeamRole;

  @Prop({ required: false, type: TeamInviteSchema })
  invite: TeamInvite;

  readonly createdAt: Date;
  readonly updatedAt: Date;
}

export const TeamMemberSchema = SchemaFactory.createForClass(TeamMember);

const tasksArray = TeamMemberSchema.path<MongooseSchema.Types.Array>('tasks');
tasksArray.discriminator('objective-scale', TeamTaskObjectiveScaleSchema);
tasksArray.discriminator('objective-brainstorming', TeamTaskObjectiveBrainstormingSchema);
tasksArray.discriminator('objective-weights', TeamTaskObjectiveWeightsSchema);
tasksArray.discriminator('opinion', TeamTaskOpinionSchema);
