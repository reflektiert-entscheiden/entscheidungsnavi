import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Schema as MongooseSchema, Types } from 'mongoose';
import { TeamMember } from './team-member.schema';

@Schema({ _id: false })
class TeamEditTokenLogEntry {
  @Prop({ required: true, type: MongooseSchema.Types.ObjectId, ref: 'TeamMember' })
  member: TeamMember;

  @Prop({ required: true, type: Date })
  from: Date;

  @Prop({ required: false, type: Date })
  to?: Date;
}

export type TeamEditTokenLogEntryDocument = TeamAccessLog & Types.Subdocument;
export const TeamEditTokenLogEntrySchema = SchemaFactory.createForClass(TeamEditTokenLogEntry);

@Schema({ _id: false })
export class TeamAccessLog {
  @Prop({ required: true, type: [TeamEditTokenLogEntrySchema], default: new Types.DocumentArray([]) })
  editToken: Types.DocumentArray<TeamEditTokenLogEntry>;
}

export type TeamAccessLogDocument = TeamAccessLog & Types.Subdocument;

export const TeamAccessLogSchema = SchemaFactory.createForClass(TeamAccessLog);
