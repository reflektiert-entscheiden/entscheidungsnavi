import { TeamTaskTypes, TeamTaskType, AlternativeOpinions } from '@entscheidungsnavi/api-types';
import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';

@Schema({ discriminatorKey: 'type', timestamps: true })
export class TeamTaskBase {
  @Prop({
    type: String,
    required: true,
    enum: TeamTaskTypes,
  })
  type: TeamTaskType;

  @Prop({
    type: Boolean,
    required: true,
    default: false,
  })
  submitted: boolean;

  @Prop({
    type: Boolean,
    required: true,
    default: false,
  })
  applied: boolean;

  readonly createdAt: Date;
  readonly updatedAt: Date;
}

@Schema()
export class TeamTaskObjectiveScale {
  @Prop({
    type: String,
    required: true,
  })
  objectiveUUID: string;

  @Prop({
    type: String,
  })
  data: string;

  type: 'objective-scale';
}

export const TeamTaskObjectiveScaleSchema = SchemaFactory.createForClass(TeamTaskObjectiveScale);

@Schema()
export class TeamTaskObjectiveBrainstorming {
  @Prop({
    type: String,
  })
  data: string;

  type: 'objective-brainstorming';
}

export const TeamTaskObjectiveBrainstormingSchema = SchemaFactory.createForClass(TeamTaskObjectiveBrainstorming);

@Schema()
export class TeamTaskObjectiveWeights {
  type: 'objective-weights';

  @Prop({ required: true, type: {}, default: {} })
  weights: Record<string, number>;
}

export const TeamTaskObjectiveWeightsSchema = SchemaFactory.createForClass(TeamTaskObjectiveWeights);

@Schema()
export class TeamTaskOpinion {
  type: 'opinion';

  @Prop({ required: true, type: {}, default: {} })
  opinions: AlternativeOpinions;
}

export const TeamTaskOpinionSchema = SchemaFactory.createForClass(TeamTaskOpinion);

export const TeamTaskBaseSchema = SchemaFactory.createForClass(TeamTaskBase);

export type TeamTask = TeamTaskObjectiveScale | TeamTaskObjectiveBrainstorming | TeamTaskObjectiveWeights | TeamTaskOpinion;
