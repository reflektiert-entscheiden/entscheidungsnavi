import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';

@Schema({ _id: false })
export class TeamInvite {
  @Prop({ required: true, type: String })
  email: string;

  @Prop({ required: true, type: String })
  inviteToken: string;

  @Prop({ required: true, type: String })
  inviterName: string;
}

export const TeamInviteSchema = SchemaFactory.createForClass(TeamInvite);
