import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { HydratedDocument, Types } from 'mongoose';

export type EmailConfirmationDocument = HydratedDocument<EmailConfirmation>;

/**
 * Email confirmation tokens expire automatically.
 */
@Schema({ timestamps: true })
export class EmailConfirmation {
  @Prop({ required: true, unique: true })
  userId: Types.ObjectId;

  @Prop({ required: true })
  token: string;

  readonly createdAt: Date;
  @Prop({ expires: '15min' })
  readonly updatedAt: Date;
}

export const EmailConfirmationSchema = SchemaFactory.createForClass(EmailConfirmation);
