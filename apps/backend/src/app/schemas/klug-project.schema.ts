import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { HydratedDocument } from 'mongoose';

export type KlugProjectDocument = HydratedDocument<KlugProject>;

@Schema({ timestamps: true })
export class KlugProject {
  readonly id: string;

  @Prop({ required: true, unique: true })
  token: string;

  @Prop({ default: null })
  data: string;

  @Prop({ default: false, type: Boolean })
  isOfficial: boolean;

  @Prop({ default: false, type: Boolean })
  finished: boolean;

  @Prop({ default: null, type: Date, expires: 0 })
  expiresAt: Date;

  @Prop({ default: null, type: Buffer })
  lastPdfExport: Buffer;

  @Prop({ default: false, type: Boolean })
  readonly: boolean;

  readonly createdAt: Date;
  readonly updatedAt: Date;
}

export const KlugProjectSchema = SchemaFactory.createForClass(KlugProject);
