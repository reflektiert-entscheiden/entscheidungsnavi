import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

export type YoutubeVideoCategoryDocument = YoutubeVideoCategory & Document;

@Schema({ timestamps: true })
export class YoutubeVideoCategory {
  readonly id: string;

  @Prop({ required: true, unique: true })
  name: string;

  @Prop({ required: true })
  orderIdx: number;

  readonly createdAt: Date;
  readonly updatedAt: Date;
}

export const YoutubeVideoCategorySchema = SchemaFactory.createForClass(YoutubeVideoCategory);
