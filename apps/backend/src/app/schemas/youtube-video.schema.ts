import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document, Types } from 'mongoose';

export type YoutubeVideoDocument = YoutubeVideo & Document;

@Schema({ timestamps: true })
export class YoutubeVideo {
  readonly id: string;

  @Prop({ required: false, default: '' })
  name: string;

  @Prop({ required: true, unique: true })
  youtubeId: Types.ObjectId;

  @Prop()
  startTime: number;

  @Prop({ required: true, type: [Number] })
  stepNumbers: number[];

  @Prop({ type: [{ type: Types.ObjectId, ref: 'VideoCategory', required: true }], default: [] })
  categoryIds: Types.ObjectId[];

  readonly createdAt: Date;
  readonly updatedAt: Date;
}

export const YoutubeVideoSchema = SchemaFactory.createForClass(YoutubeVideo);
