import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';

@Schema({ _id: false })
export class LocalizedString {
  @Prop({ required: true, minlength: 1 })
  en: string;

  @Prop({ required: true, minlength: 1 })
  de: string;
}

export const LocalizedStringSchema = SchemaFactory.createForClass(LocalizedString);
