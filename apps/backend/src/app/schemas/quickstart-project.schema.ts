import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { HydratedDocument, Schema as MongooseSchema, Types } from 'mongoose';

export type QuickstartProjectDocument = HydratedDocument<QuickstartProject>;

@Schema({ timestamps: true })
export class QuickstartProject {
  readonly id: string;

  @Prop({ required: true })
  name: string;

  @Prop({ required: true, type: Object })
  data: any;

  @Prop({
    required: true,
    type: [{ type: MongooseSchema.Types.ObjectId, ref: 'QuickstartTag' }],
    default: (): Types.ObjectId[] => [],
  })
  tags: Types.ObjectId[];

  @Prop({ required: true, default: true })
  visible: boolean;

  @Prop({
    // Unique if not null: https://stackoverflow.com/a/43804473
    index: {
      unique: true,
      partialFilterExpression: { shareToken: { $type: 'string' } },
    },
    default: null,
  })
  shareToken: string;

  readonly createdAt: Date;
  readonly updatedAt: Date;
}

export const QuickstartProjectSchema = SchemaFactory.createForClass(QuickstartProject);
