import crypto from 'crypto';
import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { HydratedDocument, Types } from 'mongoose';

export type PasswordResetDocument = HydratedDocument<PasswordReset>;

/**
 * PasswortResets expire automatically
 */
@Schema({ timestamps: true })
export class PasswordReset {
  readonly id: string;

  @Prop({ required: true })
  userId: Types.ObjectId;

  @Prop({ default: () => crypto.randomBytes(32).toString('hex'), unique: true })
  token: string;

  // Only the most recent token remains active
  @Prop({ default: true })
  active: boolean;

  @Prop({ expires: '24h' })
  readonly createdAt: Date;
  readonly updatedAt: Date;
}

export const PasswordResetSchema = SchemaFactory.createForClass(PasswordReset);
