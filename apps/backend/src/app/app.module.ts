import { ClassSerializerInterceptor, MiddlewareConsumer, Module, NestModule, ValidationPipe } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import * as Joi from 'joi';
import { InjectConnection, MongooseModule } from '@nestjs/mongoose';
import { APP_FILTER, APP_GUARD, APP_INTERCEPTOR, APP_PIPE } from '@nestjs/core';
import { ThrottlerGuard, ThrottlerModule, seconds } from '@nestjs/throttler';
import mongoose, { Connection } from 'mongoose';
import session from 'express-session';
import passport from 'passport';
import MongoStore from 'connect-mongo';
import { SentryGlobalFilter, SentryModule } from '@sentry/nestjs/setup';
import { ProjectsModule } from './projects/projects.module';
import { AuthModule } from './auth/auth.module';
import { AppController } from './app.controller';
import { EmailModule } from './email/email.module';
import { QuickstartModule } from './quickstart/quickstart.module';
import { StatisticsModule } from './statistics/statistics.module';
import { EventsModule } from './events/events.module';
import { KlugModule } from './klug/klug.module';
import { UsersModule } from './users/users.module';
import { UsersApiModule } from './users-api/users-api.module';
import { TeamsModule } from './team/teams.module';
import { CSRF } from './auth/csrf';
import { YoutubeVideosModule } from './youtube-videos/youtube-videos.module';

export const ENVIRONMENT_TYPES = ['development', 'preview', 'nightly', 'production'] as const;
export type EnvironmentType = (typeof ENVIRONMENT_TYPES)[number];

@Module({
  imports: [
    ConfigModule.forRoot({
      isGlobal: true,
      validationSchema:
        // Disable validation during testing. We might be able to remove this once
        // Nestjs allows us to override modules for testing.
        // https://github.com/nestjs/nest/issues/4905
        process.env.NODE_ENV !== 'test'
          ? Joi.object({
              NODE_ENV: Joi.string().valid('development', 'production').default('development'),
              BASE_URL: Joi.string()
                .required()
                .description('Base URL under which the application is hosted, including scheme (e.g. https://enavi.app)'),
              PORT: Joi.number().default(3000),
              MONGODB_URI: Joi.string().required().description('MongoDB connection URI'),
              SESSION_SECRET: Joi.string().required().description('Secret to encrypt session cookies'),
              SENTRY_DSN: Joi.string().description('DSN for Sentry error logging'),
              ENVIRONMENT_TYPE: Joi.string()
                .valid(...ENVIRONMENT_TYPES)
                .default('development')
                .description('The type of environment we are running'),
              MAIL_HOST: Joi.string(),
              MAIL_USER: Joi.string(),
              MAIL_PASSWORD: Joi.string(),
              MAIL_FROM: Joi.string(),
            })
          : undefined,
    }),
    MongooseModule.forRootAsync({
      imports: [ConfigModule],
      useFactory: async (configService: ConfigService) => ({ uri: configService.get<string>('MONGODB_URI') }),
      inject: [ConfigService],
    }),
    ThrottlerModule.forRoot([
      {
        ttl: seconds(60),
        limit: 100,
      },
    ]),
    AuthModule,
    UsersModule,
    UsersApiModule,
    ProjectsModule,
    EmailModule,
    QuickstartModule,
    StatisticsModule,
    EventsModule,
    KlugModule,
    TeamsModule,
    YoutubeVideosModule,
    SentryModule.forRoot(),
  ],
  providers: [
    {
      // Always validate DTOs that are passed into controller functions
      provide: APP_PIPE,
      useValue: new ValidationPipe({ whitelist: true, transform: true, transformOptions: { exposeUnsetFields: false } }),
    },
    {
      // Apply global rate limiting to all endpoints with default settings (defined above)
      provide: APP_GUARD,
      useValue: ThrottlerGuard,
    },
    {
      // Always apply class-transformers classToPlain on objects
      provide: APP_INTERCEPTOR,
      useValue: ClassSerializerInterceptor,
    },
    {
      provide: APP_FILTER,
      useClass: SentryGlobalFilter,
    },
  ],
  controllers: [AppController],
})
export class AppModule implements NestModule {
  constructor(
    private configService: ConfigService,
    @InjectConnection() private connection: Connection,
  ) {
    // We want required to allow empty strings
    mongoose.Schema.Types.String.checkRequired(v => v != null);
  }

  configure(consumer: MiddlewareConsumer) {
    consumer
      .apply(
        session({
          secret: this.configService.get<string>('SESSION_SECRET'),
          resave: false,
          saveUninitialized: false,
          cookie: {
            // Automatically set the value depending on development/production environment
            secure: 'auto',
            sameSite: 'strict',
            path: '/api',
            // Expire sessions after 7 days
            // (this is only the ttl of the cookie, but connect-mongo automatically expires the session based on that)
            maxAge: 7 * 24 * 60 * 60 * 1000,
          },
          store: MongoStore.create({ client: this.connection.getClient() }),
        }),
        passport.initialize(),
        passport.session(),
      )
      .forRoutes('*')
      .apply(CSRF.csrfSynchronisedProtection)
      .forRoutes('*');
  }
}
