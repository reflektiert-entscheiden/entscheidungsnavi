import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { YoutubeVideo, YoutubeVideoSchema } from '../schemas/youtube-video.schema';
import { YoutubeVideoCategory, YoutubeVideoCategorySchema } from '../schemas/youtube-video-category.schema';
import { YoutubeVideosService } from './youtube-videos.service';
import { YoutubeVideosController } from './youtube-videos.controller';

@Module({
  imports: [
    MongooseModule.forFeature([
      {
        name: YoutubeVideo.name,
        schema: YoutubeVideoSchema,
      },
      {
        name: YoutubeVideoCategory.name,
        schema: YoutubeVideoCategorySchema,
      },
    ]),
  ],
  controllers: [YoutubeVideosController],
  providers: [YoutubeVideosService],
})
export class YoutubeVideosModule {}
