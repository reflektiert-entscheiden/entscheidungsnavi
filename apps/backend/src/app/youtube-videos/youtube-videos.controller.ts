import { Body, Controller, Delete, Get, Param, Patch, Post, UseGuards, UseInterceptors } from '@nestjs/common';
import { Types } from 'mongoose';
import { RolesGuard } from '../auth/roles.guard';
import { UniqueConstraintViolationInterceptor } from '../common/unique-constraint-violation.interceptor';
import { NotFoundInterceptor } from '../common/not-found.interceptor';
import { ParseMongoIdPipe } from '../common/parse-mogo-id.pipe';
import { YoutubeVideosService } from './youtube-videos.service';
import { CreateYoutubeVideoCategoryDto } from './dto/create-youtube-video-category.dto';
import { YoutubeVideoDto } from './dto/youtube-video.dto';
import { UpdateYoutubeVideoCategoriesOrderDto } from './dto/update-youtube-video-categories-order.dto';

@Controller('youtube-videos')
export class YoutubeVideosController {
  constructor(private youtubeVideosService: YoutubeVideosService) {}

  @Get('videos')
  async listVideos() {
    return await this.youtubeVideosService.listVideos();
  }

  @UseGuards(RolesGuard('quickstart-manager'))
  @UseInterceptors(UniqueConstraintViolationInterceptor('A video with this id already exists'))
  @Post('videos')
  async createVideo(@Body() video: YoutubeVideoDto) {
    return await this.youtubeVideosService.createVideo(video);
  }

  @UseGuards(RolesGuard('quickstart-manager'))
  @UseInterceptors(NotFoundInterceptor)
  @UseInterceptors(UniqueConstraintViolationInterceptor('A video with this id already exists'))
  @Patch('videos/:id')
  async updateVideo(@Param('id', ParseMongoIdPipe) id: Types.ObjectId, @Body() video: YoutubeVideoDto) {
    return await this.youtubeVideosService.updateVideo(id, video);
  }

  @UseGuards(RolesGuard('quickstart-manager'))
  @Delete('videos/:id')
  async deleteVideo(@Param('id', ParseMongoIdPipe) id: Types.ObjectId) {
    await this.youtubeVideosService.removeVideo(id);
  }

  @Get('categories')
  async listCategories() {
    return await this.youtubeVideosService.listCategories();
  }

  @UseGuards(RolesGuard('quickstart-manager'))
  @UseInterceptors(UniqueConstraintViolationInterceptor('A category with this name already exists'))
  @Post('categories')
  async createCategory(@Body() category: CreateYoutubeVideoCategoryDto) {
    return await this.youtubeVideosService.createCategory(category);
  }

  @UseGuards(RolesGuard('quickstart-manager'))
  @UseInterceptors(NotFoundInterceptor)
  @UseInterceptors(UniqueConstraintViolationInterceptor('A category with this name already exists'))
  @Patch('categories/:id')
  async updateCategory(@Param('id', ParseMongoIdPipe) id: Types.ObjectId, @Body() category: CreateYoutubeVideoCategoryDto) {
    return await this.youtubeVideosService.updateCategory(id, category);
  }

  @UseGuards(RolesGuard('quickstart-manager'))
  @Post('categories/order')
  async updateCategoryOrder(@Body() updateOrder: UpdateYoutubeVideoCategoriesOrderDto) {
    return await this.youtubeVideosService.updateCategoryOrder(updateOrder);
  }

  @UseGuards(RolesGuard('quickstart-manager'))
  @Delete('categories/:id')
  async deleteCategory(@Param('id', ParseMongoIdPipe) id: Types.ObjectId) {
    await this.youtubeVideosService.removeCategory(id);
  }
}
