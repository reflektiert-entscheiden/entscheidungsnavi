import { Injectable, NotFoundException } from '@nestjs/common';
import { Model, Types } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { plainToClass } from 'class-transformer';
import { YoutubeVideo, YoutubeVideoDocument } from '../schemas/youtube-video.schema';
import { YoutubeVideoCategory, YoutubeVideoCategoryDocument } from '../schemas/youtube-video-category.schema';
import { YoutubeVideoDto } from './dto/youtube-video.dto';
import { YoutubeVideoCategoryDto } from './dto/youtube-video-category.dto';
import { CreateYoutubeVideoCategoryDto } from './dto/create-youtube-video-category.dto';
import { UpdateYoutubeVideoCategoriesOrderDto } from './dto/update-youtube-video-categories-order.dto';

function videoToDto(doc: YoutubeVideoDocument) {
  return plainToClass(YoutubeVideoDto, doc);
}

function categoryToDto(doc: YoutubeVideoCategoryDocument) {
  return plainToClass(YoutubeVideoCategoryDto, doc);
}

@Injectable()
export class YoutubeVideosService {
  constructor(
    @InjectModel(YoutubeVideo.name) private youtubeVideoModel: Model<YoutubeVideoDocument>,
    @InjectModel(YoutubeVideoCategory.name) private videoCategoryModel: Model<YoutubeVideoCategoryDocument>,
  ) {}

  async listCategories() {
    return await this.videoCategoryModel
      .aggregate<YoutubeVideoCategoryDocument & { videoCount: number }>([
        {
          $lookup: {
            from: 'youtubevideos',
            let: { categoryId: '$_id' },
            pipeline: [{ $match: { $expr: { $in: ['$$categoryId', '$categoryIds'] } } }, { $count: 'videoCount' }],
            as: 'videos',
          },
        },
        { $addFields: { videoCount: { $sum: '$videos.videoCount' } } },
        { $project: { videos: 0 } },
        { $sort: { orderIdx: 1 } },
        { $addFields: { id: { $toString: '$_id' } } },
      ])
      .then(docs => docs.map(doc => categoryToDto(doc)));
  }

  async createCategory(category: CreateYoutubeVideoCategoryDto) {
    const lastCategoryInOrder = await this.videoCategoryModel.find().sort({ orderIdx: -1 }).limit(1).exec();
    const newOrderIdx = lastCategoryInOrder.length > 0 ? lastCategoryInOrder[0].orderIdx + 1 : 0;
    const doc = await this.videoCategoryModel.create({
      ...category,
      orderIdx: newOrderIdx,
    });
    const newCategory = categoryToDto(doc);
    newCategory.videoCount = 0;
    return newCategory;
  }

  async updateCategory(id: Types.ObjectId, category: CreateYoutubeVideoCategoryDto) {
    const result: YoutubeVideoCategoryDocument & { videoCount?: number } = await this.videoCategoryModel
      .findByIdAndUpdate(id, category, { new: true })
      .exec();
    result.videoCount = await this.youtubeVideoModel.countDocuments({ categoryId: result.id }).exec();
    return categoryToDto(result);
  }

  /**
   * adjusts the orderIdx of all categories ordered between updateOrder.oldIndex and updateOrder.newIndex
   */
  async updateCategoryOrder(updateOrder: UpdateYoutubeVideoCategoriesOrderDto) {
    const numberOfCategories = await this.videoCategoryModel.countDocuments().exec();
    const categoryId = new Types.ObjectId(updateOrder.categoryId);
    const category = await this.videoCategoryModel.findById(categoryId).exec();

    if (
      category != null &&
      category.orderIdx === updateOrder.oldIndex &&
      updateOrder.newIndex >= 0 &&
      updateOrder.newIndex <= numberOfCategories - 1
    ) {
      // cockpit and backend are synchronized - update rearrange categories
      await this.videoCategoryModel.bulkWrite([
        {
          updateOne: {
            filter: { _id: categoryId },
            update: {
              orderIdx: updateOrder.newIndex,
            },
          },
        },
        {
          updateMany: {
            filter: {
              _id: {
                $ne: categoryId,
              },
              orderIdx: {
                $gte: Math.min(updateOrder.oldIndex, updateOrder.newIndex),
                $lte: Math.max(updateOrder.oldIndex, updateOrder.newIndex),
              },
            },
            update: {
              $inc: {
                orderIdx: updateOrder.oldIndex > updateOrder.newIndex ? 1 : -1,
              },
            },
          },
        },
      ]);
    }
    return await this.listCategories(); // return the list of categories ordered after orderIdx
  }

  async removeCategory(id: Types.ObjectId) {
    await this.youtubeVideoModel.updateMany({ categoryId: id }, { categoryId: null }).exec();

    const deletedCategory = await this.videoCategoryModel.findOneAndDelete({ _id: id }).exec();
    if (deletedCategory == null) {
      throw new NotFoundException();
    } else {
      // decrement all orderIndexes of categories with orderIdx > deletedCategory.orderIdx to avoid gaps
      await this.videoCategoryModel.bulkWrite([
        {
          updateMany: {
            filter: {
              orderIdx: {
                $gt: deletedCategory.orderIdx,
              },
            },
            update: {
              $inc: {
                orderIdx: -1,
              },
            },
          },
        },
      ]);
    }
  }

  async listVideos() {
    const docs = await this.youtubeVideoModel.find().exec();
    return docs.map(doc => videoToDto(doc));
  }

  async createVideo(video: YoutubeVideoDto) {
    video.categoryIds = video.categoryIds.map(categoryId => new Types.ObjectId(categoryId));
    const doc = await this.youtubeVideoModel.create(video);
    return videoToDto(doc);
  }

  async updateVideo(id: Types.ObjectId, video: YoutubeVideoDto) {
    video.categoryIds = video.categoryIds.map(categoryId => new Types.ObjectId(categoryId));
    const result = await this.youtubeVideoModel.findByIdAndUpdate(id, video, { new: true }).exec();
    return videoToDto(result);
  }

  async removeVideo(id: Types.ObjectId) {
    const result = await this.youtubeVideoModel.deleteOne({ _id: id }).exec();
    if (result.deletedCount === 0) {
      throw new NotFoundException();
    }
  }
}
