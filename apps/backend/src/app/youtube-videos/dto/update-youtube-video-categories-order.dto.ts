import { Exclude, Expose } from 'class-transformer';
import { IsNotEmpty, IsNumber, IsString } from 'class-validator';

@Exclude()
export class UpdateYoutubeVideoCategoriesOrderDto {
  @IsNotEmpty()
  @IsString()
  @Expose()
  categoryId: string;

  @IsNotEmpty()
  @IsNumber()
  @Expose()
  oldIndex: number;

  @IsNotEmpty()
  @IsNumber()
  @Expose()
  newIndex: number;
}
