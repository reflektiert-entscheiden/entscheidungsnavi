import { PickType } from '@nestjs/mapped-types';
import { YoutubeVideoCategoryDto } from './youtube-video-category.dto';

export class CreateYoutubeVideoCategoryDto extends PickType(YoutubeVideoCategoryDto, ['name']) {}
