import { Exclude, Expose, Type } from 'class-transformer';
import { IsNotEmpty, IsNumber, IsString } from 'class-validator';
import { VideoCategory as ApiVideoCategory } from '@entscheidungsnavi/api-types';

@Exclude()
export class YoutubeVideoCategoryDto implements ApiVideoCategory {
  @Expose()
  @Type(() => String)
  id: string;

  @Expose()
  @IsNumber()
  orderIdx: number;

  @Expose()
  @IsNumber()
  videoCount: number;

  @IsNotEmpty()
  @IsString()
  @Expose()
  name: string;

  @Expose()
  createdAt: Date;

  @Expose()
  updatedAt: Date;
}
