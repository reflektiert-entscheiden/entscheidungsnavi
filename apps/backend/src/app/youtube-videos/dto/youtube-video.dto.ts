import { Exclude, Expose, Transform, Type } from 'class-transformer';
import { IsMongoId, IsNotEmpty, IsNumber, IsOptional, IsString } from 'class-validator';
import { YoutubeVideo as ApiYoutubeVideo } from '@entscheidungsnavi/api-types';
import { Types } from 'mongoose';

@Exclude()
export class YoutubeVideoDto implements ApiYoutubeVideo {
  @Expose()
  @Type(() => String)
  id: string;

  @IsNotEmpty()
  @IsString()
  @Expose()
  name: string;

  @IsNotEmpty()
  @IsString()
  @Expose()
  youtubeId: string;

  @IsOptional()
  @IsNumber()
  @Expose()
  startTime: number;

  @Expose()
  @IsNumber({}, { each: true })
  stepNumbers: number[];

  @Expose()
  @Type(() => Types.ObjectId)
  @IsMongoId({ each: true })
  @Transform(({ key, obj }) => obj[key]) // https://stackoverflow.com/a/71017898
  categoryIds: Types.ObjectId[];

  @Expose()
  createdAt: Date;

  @Expose()
  updatedAt: Date;
}
