import { ErrorHandler, NgModule } from '@angular/core';
import { BrowserModule, DomSanitizer, EVENT_MANAGER_PLUGINS } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatButtonModule } from '@angular/material/button';
import { MAT_ICON_DEFAULT_OPTIONS, MatIconModule, MatIconRegistry } from '@angular/material/icon';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DecisionData } from '@entscheidungsnavi/decision-data';
import { MatRippleModule } from '@angular/material/core';
import { MAT_TOOLTIP_DEFAULT_OPTIONS, MatTooltipDefaultOptions, MatTooltipModule } from '@angular/material/tooltip';
import { provideHttpClient, withInterceptorsFromDi } from '@angular/common/http';
import * as Sentry from '@sentry/angular';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatMenuModule } from '@angular/material/menu';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatDialogModule } from '@angular/material/dialog';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { QuillModule } from 'ngx-quill';
import { Platform } from '@angular/cdk/platform';
import { ApiClientModule } from '@entscheidungsnavi/api-client';
import { OverlayProgressBarDirective } from '@entscheidungsnavi/widgets/overlay-progress-bar';
import { Router } from '@angular/router';
import { Angulartics2Module } from 'angulartics2';
import { PercentPipe } from '@angular/common';
import { OutsideZoneEventManagerPlugin } from '@entscheidungsnavi/widgets/outside-zone-events/outside-zone-event-manager-plugin';
import { HoverPopOverDirective, RichTextEmptyPipe } from '@entscheidungsnavi/widgets';
import { KlugToolComponent } from './klug-tool.component';
import { NavigationComponent } from './navigation/navigation.component';
import { CodeInputComponent } from './steps/code-input/code-input.component';
import { ObjectivesComponent } from './steps/objectives/objectives.component';
import { DecisionStatementComponent } from './steps/decision-statement/decision-statement.component';
import { HelpComponent } from './help/help.component';
import { ObjectiveListComponent } from './steps/objectives/objective-list/objective-list.component';
import { ObjectiveBoxComponent } from './steps/objectives/objective-list/objective-box/objective-box.component';
import { AlternativesComponent } from './steps/alternatives/alternatives.component';
import { AlternativeListComponent } from './steps/alternatives/alternative-list/alternative-list.component';
import { AlternativeBoxComponent } from './steps/alternatives/alternative-list/alternative-box/alternative-box.component';
import { AssessmentComponent } from './steps/assessment/assessment.component';
import { ComparisonComponent } from './steps/comparison/comparison.component';
import { EvaluationComponent } from './steps/evaluation/evaluation.component';
import { NavlineComponent } from './navigation/navline/navline.component';
import { StepDescriptionComponent } from './shared/step-description/step-description.component';
import { PdfExportComponent } from './shared/pdf-export/pdf-export.component';
import { DecisionStatementBoxComponent } from './steps/decision-statement/decision-statement-box/decision-statement-box.component';
import { EvaluationGraphComponent } from './steps/evaluation/evaluation-graph/evaluation-graph.component';
import { Constants } from './shared/constants';
import { AssessmentGraphComponent } from './steps/assessment/assessment-graph/assessment-graph.component';
import { ComparisonGraphComponent } from './steps/comparison/comparison-graph/comparison-graph.component';
import { KlugRoutingModule } from './klug-tool.routing';
import { GlobalErrorHandler } from './global-error-handler';
import { WeightsGraphComponent } from './steps/evaluation/weights-graph/weights-graph.component';
import { MatrixCellComponent } from './shared/matrix-cell/matrix-cell.component';
import { RequiredFieldRippleDirective } from './shared/required-field-trigger';

@NgModule({
  declarations: [
    KlugToolComponent,
    NavigationComponent,
    HelpComponent,
    CodeInputComponent,
    DecisionStatementComponent,
    DecisionStatementBoxComponent,
    ObjectivesComponent,
    ObjectiveListComponent,
    ObjectiveBoxComponent,
    AlternativesComponent,
    AlternativeListComponent,
    AlternativeBoxComponent,
    AssessmentComponent,
    AssessmentGraphComponent,
    ComparisonComponent,
    ComparisonGraphComponent,
    EvaluationComponent,
    EvaluationGraphComponent,
    NavlineComponent,
    StepDescriptionComponent,
    PdfExportComponent,
    WeightsGraphComponent,
    MatrixCellComponent,
  ],
  bootstrap: [KlugToolComponent],
  imports: [
    Angulartics2Module.forRoot({ pageTracking: { clearQueryParams: true } }),
    BrowserModule,
    ReactiveFormsModule,
    FormsModule,
    MatButtonModule,
    MatIconModule,
    MatFormFieldModule,
    MatInputModule,
    MatRippleModule,
    MatTooltipModule,
    MatProgressSpinnerModule,
    MatMenuModule,
    MatDialogModule,
    MatCheckboxModule,
    MatProgressBarModule,
    QuillModule.forRoot(),
    KlugRoutingModule,
    BrowserAnimationsModule,
    ApiClientModule,
    OverlayProgressBarDirective,
    PercentPipe,
    HoverPopOverDirective,
    RichTextEmptyPipe,
    RequiredFieldRippleDirective,
  ],
  providers: [
    { provide: ErrorHandler, useClass: GlobalErrorHandler },
    {
      provide: Sentry.TraceService,
      deps: [Router],
    },
    { provide: DecisionData, useValue: new DecisionData() },
    {
      provide: MAT_TOOLTIP_DEFAULT_OPTIONS,
      useValue: { disableTooltipInteractivity: true } satisfies Partial<MatTooltipDefaultOptions>,
    },
    {
      provide: Constants,
      useValue: new Constants(),
    },
    {
      provide: MAT_ICON_DEFAULT_OPTIONS,
      useValue: {
        fontSet: 'material-symbols-outlined',
      },
    },
    {
      provide: EVENT_MANAGER_PLUGINS,
      multi: true,
      useClass: OutsideZoneEventManagerPlugin,
    },
    PercentPipe,
    provideHttpClient(withInterceptorsFromDi()),
  ],
})
export class KlugToolModule {
  constructor(
    iconRegistry: MatIconRegistry,
    domSanitizer: DomSanitizer,
    platform: Platform,
    private _: Sentry.TraceService,
  ) {
    iconRegistry.addSvgIconInNamespace('klug', 'objectives', domSanitizer.bypassSecurityTrustResourceUrl(`assets/icons/objectives.svg`));

    iconRegistry.addSvgIconInNamespace(
      'klug',
      'alternatives',
      domSanitizer.bypassSecurityTrustResourceUrl(`assets/icons/alternatives.svg`),
    );

    iconRegistry.addSvgIconInNamespace('klug', 'comparison', domSanitizer.bypassSecurityTrustResourceUrl(`assets/icons/comparison.svg`));

    if (platform.WEBKIT) {
      document.body.classList.add('dt-webkit');
    }
  }
}
