import { Component, ElementRef, ViewChild } from '@angular/core';
import { KlugPdfExportImages } from '@entscheidungsnavi/api-types';
import { createKlugPDFExport } from '@entscheidungsnavi/decision-data/klug';
import { getFontEmbedCSS, toPng } from 'html-to-image';
import { PdfmakeService } from '@entscheidungsnavi/widgets';
import { KlugProjectService } from '../../services/project.service';

@Component({
  selector: 'klug-pdf-export',
  templateUrl: './pdf-export.component.html',
  styleUrls: ['./pdf-export.component.scss'],
})
export class PdfExportComponent {
  @ViewChild('decisionStatement', { read: ElementRef })
  decisionStatement: ElementRef<HTMLElement>;

  @ViewChild('objectiveList', { read: ElementRef })
  objectiveList: ElementRef<HTMLElement>;

  @ViewChild('alternativeList', { read: ElementRef })
  alternativeList: ElementRef<HTMLElement>;

  @ViewChild('assessmentGraph', { read: ElementRef })
  assessmentGraph: ElementRef<HTMLElement>;

  @ViewChild('comparisonGraph', { read: ElementRef })
  comparisonGraph: ElementRef<HTMLElement>;

  @ViewChild('evaluationGraph', { read: ElementRef })
  evaluationGraph: ElementRef<HTMLElement>;

  constructor(
    private projectService: KlugProjectService,
    private pdfmake: PdfmakeService,
  ) {}

  async createPDFBase64() {
    return new Promise<string>((resolve, _reject) => {
      this.createPDFDoc().then(doc => {
        doc.getBase64(result => resolve(result));
      });
    });
  }

  async getPdfImages(): Promise<KlugPdfExportImages> {
    const embeddedFonts = await getFontEmbedCSS(this.assessmentGraph.nativeElement);

    const [
      decisionStatementImage,
      objectiveListImage,
      alternativeListImage,
      assessmentGraphImage,
      comparisonGraphImage,
      evaluationGraphImage,
    ] = await Promise.all([
      toPng(this.decisionStatement.nativeElement, { pixelRatio: 2, fontEmbedCSS: embeddedFonts }),
      toPng(this.objectiveList.nativeElement, { pixelRatio: 2, fontEmbedCSS: embeddedFonts }),
      toPng(this.alternativeList.nativeElement, { pixelRatio: 2, fontEmbedCSS: embeddedFonts }),
      toPng(this.assessmentGraph.nativeElement, { pixelRatio: 2, fontEmbedCSS: embeddedFonts }),
      toPng(this.comparisonGraph.nativeElement, { pixelRatio: 2, fontEmbedCSS: embeddedFonts }),
      toPng(this.evaluationGraph.nativeElement, { pixelRatio: 2, fontEmbedCSS: embeddedFonts }),
    ]);

    return {
      decisionStatementImage,
      objectiveListImage,
      alternativeListImage,
      assessmentGraphImage,
      comparisonGraphImage,
      evaluationGraphImage,
    };
  }

  async createPDFDoc() {
    const document = createKlugPDFExport(this.projectService.getAccessToken(), await this.getPdfImages());
    return await this.pdfmake.createPdf(document);
  }
}
