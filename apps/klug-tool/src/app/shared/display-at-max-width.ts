const MAX_WIDTH_SYMBOL = Symbol();

export function wantsToBeDisplayedAtMaximumWidth(object: any) {
  return !!object[MAX_WIDTH_SYMBOL];
}

/**
 * Attaching this decorator to a component class that is used in routing signals to the component holding the router outlet that
 * this component should be displayed without the default width limit applied. Parts of the component that
 * should still be limited can be marked with the "klug-limit-width" class.
 */
export function DisplayAtMaxWidth(constructor: { new (...args: unknown[]): unknown }) {
  constructor.prototype[MAX_WIDTH_SYMBOL] = true;
}
