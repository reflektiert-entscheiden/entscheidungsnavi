export class Constants {
  public readonly maxObjectiveCount = 10;
  public readonly minObjectiveCount = 3;

  public readonly maxAlternativeCount = 10;
  public readonly minAlternativeCount = 2;

  public readonly diagramColors = [
    '#fb901c',
    '#e8640f',
    '#008cdb',
    '#35617a',
    '#004f88',
    '#3490a5',
    '#4eb8ec',
    '#e0303a',
    '#a72e35',
    '#b55060',
  ];
}
