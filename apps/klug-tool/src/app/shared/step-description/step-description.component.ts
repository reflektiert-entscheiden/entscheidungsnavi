import { Component } from '@angular/core';

@Component({
  selector: 'klug-step-description',
  templateUrl: './step-description.component.html',
  styleUrls: ['./step-description.component.scss'],
})
export class StepDescriptionComponent {}
