import { Component, input } from '@angular/core';
import { Alternative, Objective } from '@entscheidungsnavi/decision-data';

@Component({
  selector: 'klug-matrix-cell',
  templateUrl: './matrix-cell.component.html',
  styleUrls: ['./matrix-cell.component.scss'],
})
export class MatrixCellComponent {
  object = input.required<Alternative | Objective>();
  orientation = input<'horizontal' | 'vertical'>('horizontal');

  get isObjective() {
    return this.object() instanceof Objective;
  }
}
