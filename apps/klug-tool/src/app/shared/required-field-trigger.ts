import { Directive, Input, OnDestroy, OnInit } from '@angular/core';
import { MatRipple } from '@angular/material/core';

@Directive({
  selector: '[klugRequiredFieldRipple]',
  standalone: true,
  hostDirectives: [MatRipple],
})
export class RequiredFieldRippleDirective implements OnInit, OnDestroy {
  public static allRipples: Set<RequiredFieldRippleDirective> = new Set();

  @Input({ alias: 'klugRequiredFieldRipple', required: true })
  isSet: boolean;

  constructor(private ripple: MatRipple) {
    ripple.disabled = true;
  }

  ngOnInit() {
    RequiredFieldRippleDirective.allRipples.add(this);
  }

  ngOnDestroy() {
    RequiredFieldRippleDirective.allRipples.delete(this);
  }

  trigger() {
    if (!this.isSet) {
      this.ripple.launch({
        centered: true,
        animation: {
          enterDuration: 500,
          exitDuration: 500,
        },
      });
    }
  }
}
