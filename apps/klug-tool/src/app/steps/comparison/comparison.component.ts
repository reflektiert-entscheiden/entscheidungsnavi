import { Component } from '@angular/core';
import { StateService } from '../../services/state.service';
import { DisplayAtMaxWidth } from '../../shared/display-at-max-width';
import { AbstractStepComponent } from '../step-container.component';

@Component({
  selector: 'klug-comparison',
  templateUrl: './comparison.component.html',
  styleUrls: ['./comparison.component.scss'],
})
@DisplayAtMaxWidth
export class ComparisonComponent extends AbstractStepComponent {
  constructor(stateService: StateService) {
    super();

    if (!stateService.helpOpen$.value) {
      stateService.toggleHelp();
    }
  }
}
