import { Component } from '@angular/core';
import { Alternative, DecisionData, Objective, Outcome } from '@entscheidungsnavi/decision-data';
import { range } from 'lodash';
import { DisplayAtMaxWidth } from '../../shared/display-at-max-width';
import { AbstractStepComponent } from '../step-container.component';

@Component({
  selector: 'klug-assessment',
  templateUrl: './assessment.component.html',
  styleUrls: ['./assessment.component.scss'],
})
@DisplayAtMaxWidth
export class AssessmentComponent extends AbstractStepComponent {
  protected objectives: Objective[];
  protected alternatives: Alternative[];
  protected outcomes: Outcome[][];

  protected readonly points = range(0, 11);

  constructor(decisionData: DecisionData) {
    super();
    this.objectives = decisionData.objectives;
    this.alternatives = decisionData.alternatives;
    this.outcomes = decisionData.outcomes;
  }
}
