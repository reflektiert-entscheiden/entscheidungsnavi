import { Component } from '@angular/core';
import { DecisionData } from '@entscheidungsnavi/decision-data';
import { Objective, Alternative, Outcome } from '@entscheidungsnavi/decision-data';
import { range } from 'lodash';

@Component({
  selector: 'klug-assessment-graph',
  templateUrl: './assessment-graph.component.html',
  styleUrls: ['./assessment-graph.component.scss'],
})
export class AssessmentGraphComponent {
  protected objectives: Objective[];
  protected alternatives: Alternative[];
  protected outcomes: Outcome[][];

  protected readonly possibleOutcomes = range(0, 11);

  get gridColumns() {
    return `repeat(${this.objectives.length + 1}, 150px)`;
  }

  constructor(decisionData: DecisionData) {
    this.objectives = decisionData.objectives;
    this.alternatives = decisionData.alternatives;
    this.outcomes = decisionData.outcomes;
  }
}
