import { Component } from '@angular/core';
import { AbstractStepComponent } from '../step-container.component';

@Component({
  selector: 'klug-objectives',
  templateUrl: './objectives.component.html',
})
export class ObjectivesComponent extends AbstractStepComponent {}
