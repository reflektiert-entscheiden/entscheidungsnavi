import { Component } from '@angular/core';
import { DecisionData } from '@entscheidungsnavi/decision-data';
import { Objective } from '@entscheidungsnavi/decision-data';
import { PairComparisonService } from '../../../services/pair-comparisons.service';
import { Constants } from '../../../shared/constants';

@Component({
  selector: 'klug-objective-list',
  templateUrl: './objective-list.component.html',
  styleUrls: ['./objective-list.component.scss'],
})
export class ObjectiveListComponent {
  constructor(
    protected decisionData: DecisionData,
    private pairComparisonService: PairComparisonService,
    protected constants: Constants,
  ) {
    if (decisionData.objectives.length < constants.minObjectiveCount) {
      const toAdd = constants.minObjectiveCount - decisionData.objectives.length;
      for (let i = 0; i < toAdd; i++) {
        this.addObjective();
      }
    }
  }

  addObjective() {
    const objective = new Objective();
    objective.numericalData.from = 0;
    objective.numericalData.to = 10;
    this.decisionData.addObjective(objective);
  }

  deleteObjective(objectiveIndex: number) {
    this.decisionData.removeObjective(objectiveIndex);
  }
}
