import { Component, EventEmitter, Input, Output } from '@angular/core';
import { DecisionData } from '@entscheidungsnavi/decision-data';
import { Constants } from '../../../../shared/constants';

@Component({
  selector: 'klug-objective-box',
  templateUrl: './objective-box.component.html',
  styleUrls: ['../../../../shared/box-styles.scss'],
})
export class ObjectiveBoxComponent {
  @Input()
  objectiveIndex: number;

  @Output()
  delete = new EventEmitter();

  get objective() {
    return this.decisionData.objectives[this.objectiveIndex];
  }

  constructor(
    protected decisionData: DecisionData,
    protected constants: Constants,
  ) {}

  deleteObjective() {
    this.delete.emit();
  }
}
