import { Component, OnInit } from '@angular/core';
import { DecisionData } from '@entscheidungsnavi/decision-data';
import { noop } from 'lodash';
import { KlugService } from '@entscheidungsnavi/api-client';
import { KlugPairComparisons, KlugCompareResult } from '@entscheidungsnavi/decision-data/klug';
import { MatDialog } from '@angular/material/dialog';
import { PairComparisonService } from '../../services/pair-comparisons.service';
import { AbstractStepComponent } from '../step-container.component';
import { KlugProjectService } from '../../services/project.service';
import { WeightsGraphComponent } from './weights-graph/weights-graph.component';

@Component({
  selector: 'klug-evaluation',
  templateUrl: './evaluation.component.html',
  styleUrls: ['./evaluation.component.scss'],
})
export class EvaluationComponent extends AbstractStepComponent implements OnInit {
  constructor(
    private decisionData: DecisionData,
    pairComparisonService: PairComparisonService,
    private klugService: KlugService,
    private projectService: KlugProjectService,
    private dialog: MatDialog,
  ) {
    super();

    this.turnComparisonsIntoWeights(pairComparisonService.getComparisonTable());
  }

  ngOnInit() {
    if (!this.projectService.isFinishedProject) {
      this.klugService
        .finishProject(this.projectService.getAccessToken(), {})
        .subscribe({ next: () => (this.projectService.isFinishedProject = true), error: noop });
    }
  }

  openWeights() {
    this.dialog.open(WeightsGraphComponent, {
      height: '600px',
      width: '900px',
    });
  }

  private turnComparisonsIntoWeights(comparisons: KlugPairComparisons) {
    // Fill comparison
    for (let objectiveIndexRow = 0; objectiveIndexRow < this.decisionData.objectives.length; objectiveIndexRow++) {
      for (
        let objectiveIndexColumn = objectiveIndexRow;
        objectiveIndexColumn < this.decisionData.objectives.length;
        objectiveIndexColumn++
      ) {
        if (objectiveIndexColumn === objectiveIndexRow) {
          comparisons[objectiveIndexColumn][objectiveIndexRow] = 'less';
          continue;
        }

        const setComparison = comparisons[objectiveIndexRow][objectiveIndexColumn];
        comparisons[objectiveIndexColumn][objectiveIndexRow] =
          setComparison === 'less' ? 'more' : setComparison === 'more' ? 'less' : setComparison;
      }
    }

    const points: { [key in Exclude<KlugCompareResult, 'missing'>]: number } = {
      more: 0,
      less: 1,
      equal: 0.5,
    };

    for (let objectiveIndexColumn = 0; objectiveIndexColumn < this.decisionData.objectives.length; objectiveIndexColumn++) {
      const sum = comparisons.reduce((acc, row) => {
        return acc + points[row[objectiveIndexColumn] as keyof typeof points];
      }, 0);

      this.decisionData.weights.preliminaryWeights[objectiveIndexColumn].value = sum;
    }

    this.decisionData.weights.normalizePreliminaryWeights();
  }
}
