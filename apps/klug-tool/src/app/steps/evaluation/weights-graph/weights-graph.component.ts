import { ChangeDetectionStrategy, Component, NgZone, OnInit } from '@angular/core';
import { DecisionData } from '@entscheidungsnavi/decision-data';
import { Constants } from '../../../shared/constants';

@Component({
  selector: 'klug-weights-graph',
  templateUrl: './weights-graph.component.html',
  styleUrls: ['./weights-graph.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class WeightsGraphComponent implements OnInit {
  protected hoveredObjectiveIndex = -1;

  protected weightPercentages: number[];
  protected weightAngles: number[];

  get pieGradient() {
    let gradient = 'conic-gradient(';

    for (let objectiveIndex = 0; objectiveIndex < this.weightAngles.length; objectiveIndex++) {
      const start = this.weightAngles[objectiveIndex] - 90;
      const end = objectiveIndex === this.weightAngles.length - 1 ? 360 : this.weightAngles[objectiveIndex + 1] - 90;

      const color =
        this.hoveredObjectiveIndex === objectiveIndex || this.hoveredObjectiveIndex === -1
          ? this.constants.diagramColors[objectiveIndex]
          : `color-mix(in srgb, ${this.constants.diagramColors[objectiveIndex]}, white 70%)`;

      gradient += `${color} ${start + 1}deg ${end}deg,`;
    }

    gradient = gradient.slice(0, -1) + ')';

    return gradient;
  }

  constructor(
    protected decisionData: DecisionData,
    private constants: Constants,
    private zone: NgZone,
  ) {}

  ngOnInit() {
    this.setupData();
  }

  private setupData() {
    const weights = this.decisionData.weights.getWeightValues();
    const sum = weights.reduce((acc, weight) => acc + weight, 0);

    this.weightPercentages = weights.map(weight => (weight / sum) * 100);

    let angle = 0;

    const angles: number[] = [];

    for (const weight of weights) {
      angles.push(90 + angle);
      const end = angle + (weight / sum) * 360;
      angle = end;
    }

    this.weightAngles = angles;
  }

  // Runs outside Zone
  onPieMouseMove(event: MouseEvent) {
    const newHoveredObjectiveIndex = this.getObjectiveIndexFromAngle(event);

    if (this.hoveredObjectiveIndex !== newHoveredObjectiveIndex) {
      this.zone.run(() => {
        this.hoveredObjectiveIndex = newHoveredObjectiveIndex;
      });
    }
  }

  onPieMouseLeave() {
    this.hoveredObjectiveIndex = -1;
  }

  private getObjectiveIndexFromAngle(event: MouseEvent) {
    const rect = (event.target as HTMLElement).getBoundingClientRect();

    const x = event.offsetX / rect.width;
    const y = event.offsetY / rect.height;

    let angle = Math.atan2(y - 0.5, x - 0.5) * (180 / Math.PI) + 90;

    angle = (angle + 360) % 360;

    for (let i = 0; i < this.weightAngles.length - 1; i++) {
      if (angle < this.weightAngles[i + 1] - 90) {
        return i;
      }
    }

    return this.weightAngles.length - 1;
  }
}
