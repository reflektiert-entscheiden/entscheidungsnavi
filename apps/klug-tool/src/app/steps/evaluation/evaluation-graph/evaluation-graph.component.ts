import { PercentPipe } from '@angular/common';
import { Component, Input } from '@angular/core';
import { Alternative, DecisionData, getAlternativeUtilities, Objective } from '@entscheidungsnavi/decision-data';

@Component({
  selector: 'klug-evaluation-graph',
  templateUrl: './evaluation-graph.component.html',
  styleUrls: ['./evaluation-graph.component.scss'],
})
export class EvaluationGraphComponent {
  @Input() showKey = false;

  protected objectives: Objective[];

  protected sortedAlternatives: Alternative[];

  protected sortedUtilityValues: number[][];
  protected sortedDisplayValues: number[][];
  protected sortedSumValues: number[];

  protected hoveredObjectiveIndex = -1;

  protected readonly widthOfScaleCh = 6;

  get gridColumns() {
    return `${this.widthOfScaleCh}ch repeat(${this.sortedAlternatives.length}, 1fr)`;
  }

  get gridRows() {
    if (this.showKey) {
      return 'max-content 1fr min-content';
    } else {
      return '1fr min-content';
    }
  }

  constructor(
    private decisionData: DecisionData,
    protected percentPipe: PercentPipe,
  ) {
    this.objectives = decisionData.objectives;

    const weightedMatrix = decisionData.getWeightedUtilityMatrix();
    const alternativeUtilities = getAlternativeUtilities(weightedMatrix);

    const sortedAlternativeIndices = decisionData.alternatives.map((_, index) => index);
    sortedAlternativeIndices.sort(
      (alternativeIndexA, alternativeIndexB) => alternativeUtilities[alternativeIndexB] - alternativeUtilities[alternativeIndexA],
    );

    this.sortedAlternatives = sortedAlternativeIndices.map(index => this.decisionData.alternatives[index]);

    this.sortedUtilityValues = sortedAlternativeIndices.map(alternativeIndex => weightedMatrix[alternativeIndex]);

    this.sortedDisplayValues = this.sortedUtilityValues.map(alternative => alternative.map(value => Math.round(value * 10 * 10)));
    this.sortedSumValues = this.sortedUtilityValues.map(alternative => alternative.reduce((a, b) => a + b, 0));
  }
}
