import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { KlugService } from '@entscheidungsnavi/api-client';
import { take } from 'rxjs';
import { KlugProjectService, KLUG_TOKEN_KEY } from '../../services/project.service';

@Component({
  selector: 'klug-code-input',
  templateUrl: './code-input.component.html',
  styleUrls: ['./code-input.component.scss'],
})
export class CodeInputComponent implements OnInit {
  protected readonly formGroup = new FormGroup({
    code: new FormControl('', { nonNullable: true, validators: [Validators.required] }),
  });

  constructor(
    private projectService: KlugProjectService,
    private klugService: KlugService,
    activatedRoute: ActivatedRoute,
  ) {
    projectService
      .hasValidAccessToken()
      .pipe(take(1))
      .subscribe(validToken => {
        if (validToken) {
          this.formGroup.disable();
        }
      });

    activatedRoute.queryParams.subscribe(queryParams => {
      if (queryParams['token']) {
        this.formGroup.controls.code.setValue(queryParams['token']);
        this.formGroup.controls.code.markAsTouched();
        this.formGroup.controls.code.markAsDirty();
        this.useToken();
      }
    });
  }

  ngOnInit() {
    const lastToken = localStorage.getItem(KLUG_TOKEN_KEY);

    if (lastToken) {
      this.formGroup.controls.code.setValue(lastToken);
    }
  }

  useTestToken() {
    this.projectService.createUnofficialProject().subscribe();
  }

  useToken() {
    if (!this.formGroup.valid) {
      return;
    }

    const token = this.formGroup.controls.code.value;

    this.projectService.useToken(token).subscribe({
      error: () => {
        this.formGroup.controls.code.setErrors({
          notFound: true,
        });
      },
    });
  }
}
