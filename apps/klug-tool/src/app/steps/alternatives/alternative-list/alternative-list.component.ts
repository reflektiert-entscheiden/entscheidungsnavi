import { Component } from '@angular/core';
import { DecisionData } from '@entscheidungsnavi/decision-data';
import { Constants } from '../../../shared/constants';

@Component({
  selector: 'klug-alternative-list',
  templateUrl: './alternative-list.component.html',
  styleUrls: ['./alternative-list.component.scss'],
})
export class AlternativeListComponent {
  constructor(
    protected decisionData: DecisionData,
    protected constants: Constants,
  ) {
    if (decisionData.alternatives.length < constants.minAlternativeCount) {
      const toAdd = constants.minAlternativeCount - decisionData.alternatives.length;
      for (let i = 0; i < toAdd; i++) {
        this.addAlternative();
      }
    }
  }

  addAlternative() {
    this.decisionData.addAlternative();
  }

  deleteAlternative(alternativeIndex: number) {
    this.decisionData.removeAlternative(alternativeIndex);
  }
}
