import { Component, EventEmitter, Input, Output } from '@angular/core';
import { DecisionData } from '@entscheidungsnavi/decision-data';
import { Constants } from '../../../../shared/constants';

@Component({
  selector: 'klug-alternative-box',
  templateUrl: './alternative-box.component.html',
  styleUrls: ['../../../../shared/box-styles.scss'],
})
export class AlternativeBoxComponent {
  @Input()
  alternativeIndex: number;

  @Output()
  delete = new EventEmitter();

  get alternative() {
    return this.decisionData.alternatives[this.alternativeIndex];
  }

  constructor(
    protected decisionData: DecisionData,
    protected constants: Constants,
  ) {}

  deleteAlternative() {
    this.delete.emit();
  }
}
