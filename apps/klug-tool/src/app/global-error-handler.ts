import { ErrorHandler, Injectable, Injector } from '@angular/core';
import * as Sentry from '@sentry/angular';
import { DecisionData } from '@entscheidungsnavi/decision-data';
import { KlugProjectService } from './services/project.service';

@Injectable()
export class GlobalErrorHandler implements ErrorHandler {
  sentryHandler: ErrorHandler;

  constructor(private injector: Injector) {
    this.sentryHandler = Sentry.createErrorHandler({
      showDialog: false,
      logErrors: false,
    });
  }

  async handleError(error: any) {
    let project: { isOfficial: boolean; accessToken: string; isReadonly: boolean };
    try {
      const projectService = this.injector.get(KlugProjectService);
      project = {
        isOfficial: projectService.isOfficialKlugProject,
        accessToken: projectService.getAccessToken(),
        isReadonly: projectService.isReadonlyProject,
      };
    } catch {}

    let projectVersion: string = null;
    try {
      const decisionData = this.injector.get(DecisionData);
      projectVersion = decisionData.version;
    } catch {}

    Sentry.withScope(scope => {
      scope.setContext('project', {
        version: projectVersion,
        ...project,
      });
      this.sentryHandler.handleError(error);
    });

    // pass the error to the console
    console.error(error);
  }
}
