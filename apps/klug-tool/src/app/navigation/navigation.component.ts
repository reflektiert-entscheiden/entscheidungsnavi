import { Component } from '@angular/core';
import { MAT_TOOLTIP_DEFAULT_OPTIONS, MatTooltipDefaultOptions } from '@angular/material/tooltip';
import { OnDestroyObservable } from '@entscheidungsnavi/tools';
import { map, Observable, takeUntil } from 'rxjs';
import { AccessService } from '../routing/access.service';
import { AutoSaveService } from '../services/auto-save.service';
import { KlugProjectService } from '../services/project.service';
import { StateService } from '../services/state.service';

export const ROUTER_STEPS = ['decision-statement', 'objectives', 'options', 'assessment', 'assessment/comparison', 'evaluation'] as const;
export type RouterStep = (typeof ROUTER_STEPS)[number];

export const NAVIGATION_STEPS: { name: string; icon?: string; svgIcon?: string; link: RouterStep }[] = [
  {
    name: 'Entscheidungsfrage',
    link: 'decision-statement',
  },
  {
    name: 'Ziele',
    svgIcon: 'klug:objectives',
    link: 'objectives',
  },
  {
    name: 'Optionen',
    svgIcon: 'klug:alternatives',
    link: 'options',
  },
  {
    name: 'Vergleichen',
    svgIcon: 'klug:comparison',
    link: 'assessment',
  },
  {
    name: 'Auswertung',
    link: 'evaluation',
  },
];

@Component({
  selector: 'klug-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.scss'],
  providers: [
    {
      provide: MAT_TOOLTIP_DEFAULT_OPTIONS,
      useValue: { disableTooltipInteractivity: false } satisfies Partial<MatTooltipDefaultOptions>,
    },
  ],
})
export class NavigationComponent {
  readonly steps = NAVIGATION_STEPS;

  protected saveIndicatorTooltip$: Observable<string>;

  @OnDestroyObservable()
  onDestroy$: Observable<void>;

  get accessToken() {
    return this.projectService.getAccessToken();
  }

  get isHelpOpen() {
    return this.stateService.helpOpen$;
  }

  constructor(
    protected projectService: KlugProjectService,
    private accessService: AccessService,
    private stateService: StateService,
    protected autoSaveService: AutoSaveService,
  ) {
    this.saveIndicatorTooltip$ = autoSaveService.saveState$.pipe(
      takeUntil(this.onDestroy$),
      map(saveState => {
        if (saveState === 'errored') {
          // eslint-disable-next-line max-len
          return 'Beim Speichern Deiner Änderungen ist ein Problem aufgetreten. Möglicherweise ist Deine Internetverbindung instabil.';
        } else {
          return `Änderungen werden im Hintergrund automatisch im Token „${this.accessToken}“ gespeichert.`;
        }
      }),
    );
  }

  hasAccessToken() {
    return this.projectService.hasValidAccessToken();
  }

  toggleHelp() {
    this.stateService.toggleHelp();
  }

  canAccess(step: RouterStep) {
    return this.accessService.canAccess(step);
  }
}
