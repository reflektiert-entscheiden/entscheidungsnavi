import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Router, RouterStateSnapshot } from '@angular/router';
import { map } from 'rxjs';
import { HasTokenGuard } from './has-token-guard';

@Injectable({
  providedIn: 'root',
})
export class HasNoTokenGuard {
  constructor(
    private hasTokenGuard: HasTokenGuard,
    private router: Router,
  ) {}

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    return this.hasTokenGuard.canActivateChild(route, state).pipe(map(guardResponse => guardResponse !== true));
  }
}
