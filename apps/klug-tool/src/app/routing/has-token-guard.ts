import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Router, RouterStateSnapshot } from '@angular/router';
import { map } from 'rxjs';
import { KlugProjectService } from '../services/project.service';

@Injectable({
  providedIn: 'root',
})
export class HasTokenGuard {
  constructor(
    private projectService: KlugProjectService,
    private router: Router,
  ) {}

  canActivateChild(_route: ActivatedRouteSnapshot, _state: RouterStateSnapshot) {
    return this.projectService.hasValidAccessToken().pipe(
      map(hasToken => {
        if (hasToken) {
          return true;
        } else {
          return this.router.parseUrl('/code');
        }
      }),
    );
  }
}
