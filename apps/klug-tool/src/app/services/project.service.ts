import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { KlugProjectDto, KlugService } from '@entscheidungsnavi/api-client';
import { DecisionData } from '@entscheidungsnavi/decision-data';
import { copyProperties, dataToText, readTextWithMetadata } from '@entscheidungsnavi/decision-data/export';
import { BehaviorSubject, map, tap } from 'rxjs';
import { APP_VERSION } from '../klug-tool.component';

export const KLUG_TOKEN_KEY = 'klug-token';

@Injectable({ providedIn: 'root' })
export class KlugProjectService {
  private accessToken$ = new BehaviorSubject('');

  public isOfficialKlugProject = false;
  public isReadonlyProject = false;
  public isFinishedProject = false;

  constructor(
    private decisionData: DecisionData,
    private klugService: KlugService,
    private router: Router,
  ) {}

  hasValidAccessToken() {
    return this.accessToken$.pipe(map(token => token !== ''));
  }

  getAccessToken() {
    return this.accessToken$.value;
  }

  createUnofficialProject() {
    return this.klugService.generateUnofficialProject().pipe(tap(project => this.useProject(project)));
  }

  useToken(token: string) {
    return this.klugService.getKlugProject(token).pipe(tap(project => this.useProject(project)));
  }

  useProject(project: KlugProjectDto) {
    const data = project.data;
    if (data != null) {
      this.importText(data);
    } else {
      this.importDecisionData(new DecisionData());
      this.decisionData.lastUrl = '/impactmodel';
      this.klugService.updateKlugProject(project.token, { data: dataToText(this.decisionData, APP_VERSION) }).subscribe();
    }

    this.isOfficialKlugProject = project.isOfficial;
    this.isReadonlyProject = project.readonly;
    this.isFinishedProject = project.finished;

    this.setToken(project.token);

    this.router.navigateByUrl('decision-statement');
  }

  importText(text: string) {
    try {
      // load the data in a temporary DecisionData object
      const imp = readTextWithMetadata(text);
      this.importDecisionData(imp.data);
    } catch (e) {
      console.log(e);
      throw new Error('Der übergebene Text ist kein gültiges JSON.');
    }
  }

  importDecisionData(data: DecisionData) {
    this.decisionData.reset();
    copyProperties(data, this.decisionData);
  }

  private setToken(newToken: string) {
    localStorage.setItem(KLUG_TOKEN_KEY, newToken);
    this.accessToken$.next(newToken);
  }
}
