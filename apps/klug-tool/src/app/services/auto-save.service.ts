import { Injectable } from '@angular/core';
import { KlugService } from '@entscheidungsnavi/api-client';
import { DecisionData } from '@entscheidungsnavi/decision-data';
import { dataToText } from '@entscheidungsnavi/decision-data/export';
import { cloneDeep, isEqual } from 'lodash';
import {
  BehaviorSubject,
  concatMap,
  filter,
  fromEvent,
  interval,
  map,
  retry,
  scan,
  Subject,
  switchMap,
  take,
  tap,
  throttle,
  timer,
} from 'rxjs';
import { APP_VERSION } from '../klug-tool.component';
import { KlugProjectService } from './project.service';

type SaveState = 'idle' | 'saving' | 'saved' | 'errored';

@Injectable({ providedIn: 'root' })
export class AutoSaveService {
  public saveState$: BehaviorSubject<SaveState> = new BehaviorSubject('idle');

  constructor(decisionData: DecisionData, klugService: KlugService, projectService: KlugProjectService) {
    projectService
      .hasValidAccessToken()
      .pipe(
        filter(hasAccessToken => hasAccessToken),
        take(1),
        filter(_ => !projectService.isReadonlyProject),
        concatMap(() => {
          const token = projectService.getAccessToken();

          const saveCompleted$ = new Subject<void>();

          return interval(2000).pipe(
            map(_ => decisionData),
            scan(
              ([lastDecisionData], currentDecisionData) => {
                if (isEqual(lastDecisionData, currentDecisionData)) {
                  return [lastDecisionData, false] as const;
                } else {
                  const clone = cloneDeep(currentDecisionData);

                  return [clone, true] as const;
                }
              },
              [cloneDeep(decisionData), false] as const,
            ),
            filter(([, changed]) => changed),
            throttle(() => saveCompleted$, {
              leading: true,
              trailing: true,
            }),
            tap(_ => {
              this.saveState$.next('saving');
            }),
            concatMap(([toSaveDecisionData]) =>
              klugService.updateKlugProject(token, { data: dataToText(toSaveDecisionData, APP_VERSION) }).pipe(
                retry({
                  delay: () => {
                    this.saveState$.next('errored');
                    return timer(2000);
                  },
                }),
              ),
            ),
            tap(() => {
              this.saveState$.next('saved');
              saveCompleted$.next();
            }),
            switchMap(_ => timer(3000)),
            tap(_ => {
              if (this.saveState$.value === 'saved') {
                this.saveState$.next('idle');
              }
            }),
          );
        }),
      )
      .subscribe();

    fromEvent(window, 'beforeunload')
      .pipe(filter(_ => this.saveState$.value === 'errored' || this.saveState$.value === 'saving'))
      .subscribe((event: BeforeUnloadEvent) => {
        event.returnValue =
          'Im Hintergrund werden noch Ihre aktuellen Änderungen gespeichert. Beim Verlassen der Seite gehen diese verloren.';
      });
  }
}
