import { enableProdMode } from '@angular/core';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import * as Sentry from '@sentry/angular';
import { KlugToolModule } from './app/klug-tool.module';
import { ENVIRONMENT } from './environments/environment';

Sentry.init({
  dsn: ENVIRONMENT.sentryDsn,
  tunnel: '/tunnel/polaris',
  release: ENVIRONMENT.version,
  environment: ENVIRONMENT.type,
  ignoreErrors: ['ResizeObserver loop limit exceeded', 'ResizeObserver loop completed with undelivered notifications.'],
  integrations: [Sentry.browserTracingIntegration()],
  tracesSampleRate: 1.0,
});

if (ENVIRONMENT.production) {
  enableProdMode();
}

platformBrowserDynamic()
  .bootstrapModule(KlugToolModule)
  .catch(err => console.error(err));
