import { ApplicationRef, Injectable } from '@angular/core';
import { SwUpdate } from '@angular/service-worker';
import { ReplaySubject, concat, interval } from 'rxjs';
import { first } from 'rxjs/operators';
import * as Sentry from '@sentry/angular';

export type UpdateState = 'no-update' | 'loading-update' | 'update-ready';

@Injectable({ providedIn: 'root' })
export class ServiceWorkerService {
  updateState$ = new ReplaySubject<UpdateState>(1);

  /**
   * Whether or not the service worker is enabled.
   */
  get isEnabled() {
    return this.updates.isEnabled;
  }

  constructor(
    private updates: SwUpdate,
    appRef: ApplicationRef,
  ) {
    if (!updates.isEnabled) {
      return;
    }

    // Service Worker in a state that can't be fixed without a full reload.
    updates.unrecoverable.subscribe(event => {
      console.log('Service Worker is in unrecoverable state -> Reloading');
      Sentry.captureException(new Error('Service worker unrecoverable: ' + event.reason), { level: 'warning' });
      this.reloadPage();
    });

    // Handle the update staet
    updates.versionUpdates.subscribe(event => {
      switch (event.type) {
        case 'NO_NEW_VERSION_DETECTED':
          this.updateState$.next('no-update');
          break;
        case 'VERSION_DETECTED':
          this.updateState$.next('loading-update');
          break;
        case 'VERSION_READY':
          this.updateState$.next('update-ready');
          break;
      }
    });

    // Periodically check for updates for long running navi instances
    const appIsStable$ = appRef.isStable.pipe(first(isStable => isStable === true));
    const everySixHours$ = interval(6 * 60 * 60 * 1000);
    const everySixHoursOnceAppIsStable$ = concat(appIsStable$, everySixHours$);

    everySixHoursOnceAppIsStable$.subscribe(() => updates.checkForUpdate());
  }

  activateUpdate() {
    this.reloadPage();
  }

  private reloadPage() {
    document.location.reload();
  }
}
