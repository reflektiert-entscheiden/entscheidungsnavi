import '@angular/localize/init';
import 'reflect-metadata';
import { TextDecoder, TextEncoder } from 'util';
import { setupZoneTestEnv } from 'jest-preset-angular/setup-env/zone';

setupZoneTestEnv();

Object.assign(global, {
  TextDecoder,
  TextEncoder,
  ResizeObserver: jest.fn().mockImplementation(() => ({
    observe: jest.fn(),
    unobserve: jest.fn(),
    disconnect: jest.fn(),
  })),
});
