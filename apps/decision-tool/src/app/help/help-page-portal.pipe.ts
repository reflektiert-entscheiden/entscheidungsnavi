import { ComponentPortal, Portal, TemplatePortal } from '@angular/cdk/portal';
import { Injector, Pipe, PipeTransform, ViewContainerRef } from '@angular/core';
import { HELP_PAGE_CONTEXT } from '../../modules/shared/help-page-context.token';
import { HelpPage } from './help';

/**
 * Turns a help page into a ComponentPortal to be displayed
 */
@Pipe({
  name: 'helpPagePortal',
})
export class HelpPagePortalPipe implements PipeTransform {
  constructor(
    private injector: Injector,
    private viewContainerRef: ViewContainerRef,
  ) {}

  transform(value?: HelpPage): Portal<any> {
    switch (value?.content.type) {
      case 'component':
        return new ComponentPortal(
          value.content.component,
          this.viewContainerRef,
          Injector.create({
            parent: this.injector,
            providers: [{ provide: HELP_PAGE_CONTEXT, useValue: value.context }],
          }),
        );
      case 'template':
        return new TemplatePortal(value.content.template(), this.viewContainerRef);
      default:
        return null;
    }
  }
}
