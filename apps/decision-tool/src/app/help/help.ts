import { ComponentType } from '@angular/cdk/portal';
import { TemplateRef } from '@angular/core';

interface HelpComponentContent {
  component: ComponentType<any>;
  type: 'component';
}
interface HelpTemplateContent {
  template: () => TemplateRef<any>;
  type: 'template';
}
export interface HelpYoutubeContent {
  stepNumber: number;
  type: 'youtube';
}
export type HelpContent = HelpComponentContent | HelpTemplateContent | HelpYoutubeContent;

/**
 * A HelpPage is one single page of content.
 */
export interface HelpPage {
  name: string;
  content: HelpContent;
  context?: string;
}

class HelpPageBuilder {
  private _name: string;
  private _content: HelpContent;
  private _context?: string;

  name(name: string) {
    this._name = name;
    return this;
  }

  component(component: ComponentType<any>) {
    this._content = { type: 'component', component };
    return this;
  }

  context(context: string) {
    this._context = context;
    return this;
  }

  template(template: () => TemplateRef<any>) {
    this._content = { type: 'template', template };
    return this;
  }

  youtube(stepNumber: number) {
    this._content = { type: 'youtube', stepNumber };
    return this;
  }

  explanation(component: ComponentType<any>) {
    return this.component(component);
  }

  build(): HelpPage {
    return {
      name: this._name,
      content: this._content,
      context: this._context,
    };
  }
}

export function helpPage() {
  return new HelpPageBuilder();
}

/**
 * A help menu consists of some pages, the first of which is a special 'main page'.
 */
export type HelpMenu = HelpPage[];

/**
 * A component may implement this interface if it has an associated help menu.
 *
 * It may define a help menu per project mode or one single menu which is valid
 * for all modes.
 */
export interface HelpMenuProvider {
  helpMenu: HelpMenu;
}

export function hasHelpMenu(element: any): element is HelpMenuProvider {
  return element.helpMenu !== undefined;
}
