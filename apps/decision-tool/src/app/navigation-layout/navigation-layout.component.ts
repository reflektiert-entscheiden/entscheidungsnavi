import { animate, style, transition, trigger } from '@angular/animations';
import { ChangeDetectorRef, Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Event, NavigationEnd, Router } from '@angular/router';
import { DecisionData } from '@entscheidungsnavi/decision-data';
import { MatSidenavContainer } from '@angular/material/sidenav';
import { takeUntilDestroyed } from '@angular/core/rxjs-interop';
import { BehaviorSubject } from 'rxjs';
import { PersistentSetting, PersistentSettingParent } from '@entscheidungsnavi/widgets';
import { implementsDebug } from '../debug/debug-template';
import { DebugService } from '../debug/debug.service';
import { hasHelpMenu } from '../help/help';
import { HelpService } from '../help/help.service';
import { ProjectService } from '../data/project';
import { hasRoutedChildThatWantToBeDisplayedAtMaximumWidth } from '../interfaces/display-at-max-width';
import { hasNavline, NavLine } from '../../modules/shared/navline';
import { SideBySideService } from './side-by-side.service';

@Component({
  templateUrl: './navigation-layout.component.html',
  styleUrls: ['./navigation-layout.component.scss'],
  animations: [
    trigger('sbsEnterExit', [
      transition(':enter', [style({ width: '0' }), animate('250ms ease-in', style({ width: '*' }))]),
      transition(':leave', [animate('250ms ease-out', style({ width: '0' }))]),
    ]),
  ],
})
@PersistentSettingParent('NavigationLayout')
export class NavigationLayoutComponent implements OnInit {
  @PersistentSetting() isSidenavExpanded = true;

  /**
   * True, when the Component inside the Router Outlet requests
   * to be displayed at maximum width.
   */
  isRouterChildMaxWidth$ = new BehaviorSubject<boolean>(false);

  navLine: NavLine | null = null;

  @ViewChild('scrollContainer') scrollContainer?: ElementRef;
  @ViewChild('columnWrapper') columnWrapper: ElementRef<HTMLElement>;

  @ViewChild('sideBySideContent') sbsContentContainer: ElementRef<HTMLElement>;
  sidenavOver = false;
  sidenavEnabled = true;
  disableSidenavAnimation = false;

  @ViewChild(MatSidenavContainer) sidenavContainer: MatSidenavContainer;

  @PersistentSetting() sbsWidth = 350;
  sbsDragging = false;
  sbsOldX: number;
  sbsAnimationEnabled = false;
  sbsActiveTouchId: number;

  get sidenavFolded() {
    return !this.sidenavOver && !this.isSidenavExpanded;
  }

  constructor(
    protected decisionData: DecisionData,
    private helpService: HelpService,
    protected sideBySideService: SideBySideService,
    private debugService: DebugService,
    private projectService: ProjectService,
    activatedRoute: ActivatedRoute,
    private router: Router,
    private cdRef: ChangeDetectorRef,
  ) {
    this.sbsResizeEnd = this.sbsResizeEnd.bind(this);
    this.sbsMouseMove = this.sbsMouseMove.bind(this);

    activatedRoute.data.pipe(takeUntilDestroyed()).subscribe(data => {
      if ('sidenavEnabled' in data) this.sidenavEnabled = data.sidenavEnabled;
    });

    router.events.pipe(takeUntilDestroyed()).subscribe(event => this.navigationHandler(event));
  }

  ngOnInit() {
    // animation should not be activated at initial opening
    setTimeout(() => (this.sbsAnimationEnabled = true));
  }

  toggleSidenav() {
    this.isSidenavExpanded = !this.isSidenavExpanded;
    setTimeout(() => this.sidenavContainer.updateContentMargins());
  }

  changeSidenavOver(sidenavOver: boolean) {
    if (!this.isSidenavExpanded && sidenavOver && !this.sidenavOver) {
      // Disable the animation when we go from 'side' to 'over' mode and the sidenav is not expanded.
      // This prevents the sidenav from flickering.
      this.disableSidenavAnimation = true;
      setTimeout(() => (this.disableSidenavAnimation = false));
    }

    this.sidenavOver = sidenavOver;
  }

  private navigationHandler(event: Event) {
    if (event instanceof NavigationEnd) {
      // scroll to top on navigation
      this.scrollContainer?.nativeElement.scrollTo(0, 0);
      this.isRouterChildMaxWidth$.next(hasRoutedChildThatWantToBeDisplayedAtMaximumWidth(this.router.routerState.snapshot.root));
    }
  }

  routerActivateHandler(component: any) {
    this.navLine = hasNavline(component) ? component.navLine : null;

    let helpMenu = null;
    if (hasHelpMenu(component)) {
      if (Array.isArray(component.helpMenu)) {
        helpMenu = component.helpMenu;
      } else if (this.decisionData.projectMode in component.helpMenu) {
        helpMenu = component.helpMenu[this.decisionData.projectMode];
      }
    }

    this.helpService.setMenu(helpMenu);

    this.debugService.removeFleetingTemplate(this);
    if (implementsDebug(component) && component.hasDebugTemplate) {
      this.debugService.registerFleetingTemplate(this, component.debugTemplate);
    }
  }

  sbsMouseDown(event: MouseEvent) {
    event.preventDefault();

    this.sbsResizeStart(event);
    this.columnWrapper.nativeElement.addEventListener('mouseup', this.sbsResizeEnd);
    this.columnWrapper.nativeElement.addEventListener('mouseleave', this.sbsResizeEnd);
    this.columnWrapper.nativeElement.addEventListener('mousemove', this.sbsMouseMove);
  }

  sbsMouseMove(event: MouseEvent) {
    this.sbsResizeMove(event);
  }

  sbsTouchStart(event: TouchEvent) {
    event.preventDefault();

    const touch = event.changedTouches[0];
    this.sbsActiveTouchId = touch.identifier;
    this.sbsResizeStart(touch);
  }

  sbsTouchMove(event: TouchEvent) {
    const touch = Array.from(event.changedTouches).find(touch => touch.identifier === this.sbsActiveTouchId);
    if (touch != null) {
      this.sbsResizeMove(touch);
    }
  }

  private sbsResizeStart(event: Touch | MouseEvent) {
    this.sbsOldX = event.clientX;
    this.sbsWidth = this.sbsContentContainer.nativeElement.offsetWidth;
    this.sbsDragging = true;
  }

  sbsResizeEnd() {
    this.columnWrapper.nativeElement.removeEventListener('mouseup', this.sbsResizeEnd);
    this.columnWrapper.nativeElement.removeEventListener('mouseleave', this.sbsResizeEnd);
    this.columnWrapper.nativeElement.removeEventListener('mousemove', this.sbsMouseMove);
    this.sbsActiveTouchId = null;
    this.sbsDragging = false;
  }

  private sbsResizeMove(event: Touch | MouseEvent) {
    const newWidth = this.sbsWidth - (event.clientX - this.sbsOldX);

    if (newWidth < 50) {
      this.sideBySideService.close();
    } else if (newWidth < 200) {
      this.sbsWidth = 200;
      this.sbsOldX = event.clientX + newWidth - 200;
    } else {
      this.sbsWidth = newWidth;
      this.sbsOldX = event.clientX;
    }
  }

  isProjectLoaded(): boolean {
    return this.projectService.isProjectLoaded();
  }
}
