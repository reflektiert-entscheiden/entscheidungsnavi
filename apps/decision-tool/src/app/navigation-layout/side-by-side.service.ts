import { Injectable } from '@angular/core';
import { DecisionData } from '@entscheidungsnavi/decision-data';
import { PersistentSetting, PersistentSettingParent } from '@entscheidungsnavi/widgets';
import { AppSettingsService } from '../data/app-settings.service';
import { OnlineProject, ProjectService } from '../data/project';
import { TrackingService } from '../data/tracking.service';

@Injectable({ providedIn: 'root' })
@PersistentSettingParent('SideBySideService')
export class SideBySideService {
  @PersistentSetting()
  private isHelpOpen = false;
  private isHistoryOpen = false;

  get state(): 'help' | 'history' | 'closed' {
    if (this.isHistoryOpen) return 'history';
    else if (this.isHelpOpen) return 'help';
    else return 'closed';
  }

  get isOpen() {
    return this.state !== 'closed';
  }

  constructor(
    appSettings: AppSettingsService,
    projectService: ProjectService,
    decisionData: DecisionData,
    private trackingService: TrackingService,
  ) {
    if (appSettings.helpLaunchMode === 'open') {
      this.isHelpOpen = true;
    } else if (appSettings.helpLaunchMode !== 'restore-last') {
      this.isHelpOpen = false;
    }

    projectService.project$.subscribe(newProject => {
      if (newProject && !(newProject instanceof OnlineProject)) {
        this.isHistoryOpen = false;
      }
    });

    decisionData.projectMode$.subscribe(newProjectMode => {
      if (newProjectMode !== 'educational') {
        this.isHelpOpen = false;
      }
    });
  }

  openHelp() {
    this.isHelpOpen = true;
    this.trackingService.trackEvent('open help', { category: 'side-by-side' });
  }

  openHistory() {
    this.isHistoryOpen = true;
    this.trackingService.trackEvent('open project history', { category: 'side-by-side' });
  }

  close() {
    // Close the top most window
    switch (this.state) {
      case 'history':
        this.isHistoryOpen = false;
        this.trackingService.trackEvent('close project history', { category: 'side-by-side' });
        break;
      case 'help':
        this.isHelpOpen = false;
        this.trackingService.trackEvent('close help', { category: 'side-by-side' });
        break;
    }
  }
}
