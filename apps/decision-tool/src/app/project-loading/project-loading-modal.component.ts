import { Component } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { finalize, map, MonoTypeOperatorFunction, of, switchMap } from 'rxjs';

@Component({
  templateUrl: './project-loading-modal.component.html',
  styleUrls: ['./project-loading-modal.component.scss'],
})
export class ProjectLoadingModalComponent {}

export function projectLoadingModal<T>(dialog: MatDialog): MonoTypeOperatorFunction<T> {
  return observable =>
    of(null).pipe(
      map(() => dialog.open(ProjectLoadingModalComponent, { disableClose: true })),
      switchMap(modal => observable.pipe(finalize(() => modal.close()))),
    );
}
