import { Component, DestroyRef, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { filter } from 'rxjs';
import { takeUntilDestroyed } from '@angular/core/rxjs-interop';
import { AutoSaveService } from '../data/project/auto-save.service';
import { OnlineProject, ProjectService } from '../data/project';

export type UnloadProjectMode = 'change-language' | 'activate-update' | 'close-project';

@Component({
  templateUrl: './unload-project-modal.component.html',
  styleUrls: ['./unload-project-modal.component.scss'],
})
export class UnloadProjectModalComponent implements OnInit {
  get isAutoSaveProject() {
    return this.autoSaveService.saveState$.value !== 'off';
  }

  get isOldProjectVersion() {
    return this.projectService.getProject(OnlineProject)?.isLatestVersion$.value === false;
  }

  constructor(
    protected autoSaveService: AutoSaveService,
    private projectService: ProjectService,
    private dialogRef: MatDialogRef<UnloadProjectModalComponent, boolean>,
    @Inject(MAT_DIALOG_DATA) protected mode: UnloadProjectMode,
    private destroyRef: DestroyRef,
  ) {}

  ngOnInit() {
    if (this.isAutoSaveProject) {
      // This ensures that the dialog can only close _after it is opened_. Otherwise,
      // beforeClosed() on the dialog might be empty.
      this.autoSaveService.saveState$
        .pipe(
          filter(state => state === 'idle'),
          takeUntilDestroyed(this.destroyRef),
        )
        .subscribe(() => this.close(true));
      this.autoSaveService.triggerSave();
    }
  }

  close(accept = false) {
    this.dialogRef.close(accept);
  }
}
