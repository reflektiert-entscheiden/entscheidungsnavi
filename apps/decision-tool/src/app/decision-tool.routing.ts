import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { EducationalDecisionDataValidationGuard, educationalGradualProgressionGuard, projectModeGuard, startPageGuard } from './guards';
import { FinishProjectComponent } from './main/finish-project/finish-project.component';
import { QuickstartComponent } from './main/quickstart';
import { StartComponent } from './main/start';
import { ResetPasswordGuard, ResetPasswordModalComponent } from './main/userarea';
import { NavigationLayoutComponent } from './navigation-layout/navigation-layout.component';
import { ProjectLoadingPageComponent } from './project-loading/project-loading-page.component';
import { LEGACY_ROUTES } from './legacy-routes';

const appRoutes: Routes = [
  {
    path: 'start',
    pathMatch: 'full',
    canActivate: [startPageGuard],
    runGuardsAndResolvers: 'always',
    component: StartComponent,
  },
  {
    path: 'loading',
    component: ProjectLoadingPageComponent,
  },
  {
    path: '',
    canActivate: [projectModeGuard('educational')],
    canActivateChild: [EducationalDecisionDataValidationGuard, educationalGradualProgressionGuard],
    component: NavigationLayoutComponent,
    children: [
      {
        path: 'decisionstatement',
        loadChildren: () => import('../modules/decision-statement/decision-statement.module').then(m => m.DecisionStatementModule),
      },
      {
        path: 'objectives',
        loadChildren: () => import('../modules/objectives/objectives.module').then(m => m.ObjectivesModule),
      },
      {
        path: 'alternatives',
        loadChildren: () => import('../modules/alternatives/alternatives.module').then(m => m.AlternativesModule),
      },
      {
        path: 'impactmodel',
        loadChildren: () => import('../modules/impact-model/impact-model.module').then(m => m.ImpactModelModule),
      },
      { path: 'results', loadChildren: () => import('../modules/results/results.module').then(m => m.ResultsModule) },
      {
        path: 'finishproject',
        component: FinishProjectComponent,
      },
    ],
  },
  {
    path: 'professional',
    canActivate: [projectModeGuard('professional')],
    component: NavigationLayoutComponent,
    data: {
      sidenavEnabled: false,
    },
    loadChildren: () => import('../modules/professional/professional.module').then(m => m.ProfessionalModule),
  },
  {
    path: 'starter',
    canActivate: [projectModeGuard('starter')],
    component: NavigationLayoutComponent,
    data: {
      sidenavEnabled: false,
    },
    loadChildren: () => import('../modules/starter/starter.routing'),
  },
  {
    path: 'quickstart/:name',
    component: QuickstartComponent,
  },
  {
    path: 'reset/:token',
    canActivate: [ResetPasswordGuard],
    component: ResetPasswordModalComponent,
  },
  ...LEGACY_ROUTES,
  {
    path: '**',
    redirectTo: '/start',
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(appRoutes, {
      useHash: false,
      preloadingStrategy: PreloadAllModules,
    }),
  ],
  exports: [RouterModule],
})
export class DecisionToolRoutingModule {}
