import { Component, HostListener, isDevMode, TemplateRef } from '@angular/core';
import { Meta } from '@angular/platform-browser';
import { DebugService } from './debug/debug.service';
import { ProjectService } from './data/project';
import { ChangeLanguageService } from './data/change-language.service';

@Component({
  selector: 'dt-app',
  templateUrl: './decision-tool.component.html',
  styleUrls: ['./decision-tool.component.scss'],
})
export class DecisionToolComponent {
  debugTemplate: () => TemplateRef<any>;

  constructor(
    public debugService: DebugService,
    meta: Meta,
    private projectService: ProjectService,
    private changeLanguageService: ChangeLanguageService,
  ) {
    meta.addTag({
      name: 'description',
      // eslint-disable-next-line max-len
      content: $localize`Entscheidungsprobleme werden häufig nicht gut strukturiert und aufgrund psychologisch bedingter Faktoren durch irrationale Verzerrungen beeinflusst. Trainiere deshalb Deine Entscheidungskompetenz und lasse Dich vom Entscheidungsnavi zu einer guten Entscheidung führen!`,
    });
  }

  @HostListener('window:beforeunload', ['$event'])
  beforeUnload(event: BeforeUnloadEvent) {
    const project = this.projectService.getProject();

    if (project && !project.isProjectSaved() && !this.changeLanguageService.isChangingLanguage && !isDevMode()) {
      event.returnValue = 'Deine eingegebenen Daten gehen beim Verlassen der Seite verloren.';
    }
  }
}
