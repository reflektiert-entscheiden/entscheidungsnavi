import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialog, MatDialogRef } from '@angular/material/dialog';
import { ProjectMode } from '@entscheidungsnavi/decision-data';
import { AuthService, OnlineProjectsService, TeamsService } from '@entscheidungsnavi/api-client';
import { ConfirmModalComponent, ConfirmModalData, createControlDependencyWith } from '@entscheidungsnavi/widgets';
import { catchError, concatMap, filter, finalize, map, Observable, of, OperatorFunction, switchMap } from 'rxjs';
import { OnDestroyObservable } from '@entscheidungsnavi/tools';
import { FormBuilder, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ProjectService } from '../../data/project';
import { OnlineProjectManagementService } from '../../navigation/projects/project-management.service';
import { DecisionDataExportService } from '../../data/decision-data-export.service';

@Component({
  templateUrl: './new-project-modal.component.html',
  styleUrls: ['./new-project-modal.component.scss'],
})
export class NewProjectModalComponent {
  @OnDestroyObservable()
  private onDestroy$: Observable<void>;

  projectForm = this.fb.group({
    name: [''],
    teamProject: this.fb.control(false),
    mode: this.fb.control<ProjectMode>(null, Validators.required),
  });

  get isTeamProjectPossible() {
    return this.projectForm.value.mode !== 'starter' && this.authService.loggedIn;
  }

  constructor(
    private dialog: MatDialog,
    private modal: MatDialogRef<NewProjectModalComponent>,
    private projectService: ProjectService,
    private authService: AuthService,
    private onlineProjectsService: OnlineProjectsService,
    private onlineProjectManagementService: OnlineProjectManagementService,
    private fb: FormBuilder,
    private exportService: DecisionDataExportService,
    private teamsService: TeamsService,
    private snackBar: MatSnackBar,
    @Inject(MAT_DIALOG_DATA) projectMode?: ProjectMode,
  ) {
    if (!this.authService.loggedIn) {
      this.projectForm.controls.teamProject.disable({ emitEvent: false });
    } else {
      createControlDependencyWith(
        this.projectForm.controls.mode,
        this.projectForm.controls.teamProject,
        mode => mode !== 'starter',
        this.onDestroy$,
      );
    }

    if (projectMode) {
      this.projectForm.patchValue({ mode: projectMode });
    }
  }

  close(createdProject: boolean) {
    this.modal.close(createdProject);
  }

  createProject() {
    if (this.projectForm.invalid || this.projectForm.disabled) return;

    const projectName = this.projectForm.value.name || $localize`Unbenanntes Projekt`;
    const projectMode = this.projectForm.value.mode;
    const observable = this.projectService.newProject(projectName, projectMode);

    this.projectForm.disable({ emitEvent: false });

    if (this.isTeamProjectPossible && this.projectForm.value.teamProject) {
      observable
        .pipe(
          filter(Boolean),
          concatMap(_ => {
            const data = this.exportService.dataToText(null);
            return this.teamsService.createTeam(projectName, data);
          }),
          concatMap(teamInfo => this.projectService.loadTeamProject(teamInfo.id, { forceUnload: true })),
          finalize(() => this.projectForm.enable()),
        )
        .subscribe({
          next: () => this.close(true),
          error: () => this.snackBar.open($localize`Beim Erstellen des Teamprojekts ist ein Fehler aufgetreten.`),
        });
    } else {
      observable
        .pipe(
          filter(Boolean),
          this.trySaveOnline(projectName),
          finalize(() => this.projectForm.enable()),
        )
        .subscribe(isSavedOnline => {
          if (!isSavedOnline && projectMode !== 'starter') {
            this.dialog.open<ConfirmModalComponent, ConfirmModalData>(ConfirmModalComponent, {
              data: {
                prompt: $localize`Du hast ein lokales Projekt erstellt.
              Es wird <b>nicht automatisch gespeichert</b>.
              Du musst Deinen Fortschritt selbst regelmäßig exportieren.
              Wenn Du Dich anmeldest, wird Dein Projekt automatisch in Deinem Account gespeichert.`,
                title: $localize`Lokales Projekt`,
                buttonDeny: $localize`Verstanden`,
              },
            });
          }

          this.close(true);
        });
    }
  }

  private trySaveOnline(projectName: string): OperatorFunction<unknown, boolean> {
    return switchMap(() => {
      if (this.authService.loggedIn) {
        return this.onlineProjectsService.getProjectList().pipe(
          switchMap(projectList => {
            const lowerCaseNames = projectList.map(project => project.name.toLowerCase());
            let newProjectName = projectName;
            for (let i = 1; lowerCaseNames.includes(newProjectName.toLowerCase()); i++) {
              newProjectName = projectName + ` (${i})`;
            }
            return this.onlineProjectManagementService.saveAsNewProject(newProjectName);
          }),
          map(() => true),
          catchError(() => of(false)),
        );
      } else {
        return of(false);
      }
    });
  }
}
