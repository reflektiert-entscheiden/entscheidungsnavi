import { Component, TemplateRef, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute } from '@angular/router';
import {
  AuthService,
  OnlineProjectsService,
  ProjectDto,
  QuickstartProjectDto,
  QuickstartService,
  TeamsService,
} from '@entscheidungsnavi/api-client';
import { TeamProjectInfoWithRoleDto } from '@entscheidungsnavi/api-types';
import { OnDestroyObservable } from '@entscheidungsnavi/tools';
import { ConfirmModalComponent, YoutubeModalComponent } from '@entscheidungsnavi/widgets';
import { Observable, first, of, switchMap, takeUntil, zip } from 'rxjs';
import { ENVIRONMENT } from '../../../environments/environment';
import { JsonErrorModalComponent } from '../../../modules/shared/error/json-error-modal/json-error-modal.component';
import { supportedBrowsersRegexp } from '../../../supported-browsers';
import { ProjectService } from '../../data/project';
import { StartupService } from '../../data/startup.service';
import { YoutubeVideosService } from '../../data/youtube-videos.service';
import { ProjectListModalComponent } from '../../navigation';
import { ProjectListModalData } from '../../navigation/projects/project-list-modal/project-list-modal.component';
import { NewProjectModalComponent } from './new-project-modal.component';

@Component({
  templateUrl: './start.component.html',
  styleUrls: ['./start.component.scss'],
})
export class StartComponent {
  @OnDestroyObservable()
  private onDestroy$: Observable<void>;

  unsupportedBrowser: boolean;

  isOffline = false;
  quickstartProjects: QuickstartProjectDto[];
  onlineAndTeamProjects: (ProjectDto | TeamProjectInfoWithRoleDto)[];

  dragCounter = 0;
  showDragArea = false;

  @ViewChild('helpContent') helpContent: TemplateRef<any>;

  get isNightlyVersion() {
    return ENVIRONMENT.nightly;
  }

  constructor(
    private dialog: MatDialog,
    activatedRoute: ActivatedRoute,
    startupService: StartupService,
    private quickstartProjectsService: QuickstartService,
    private onlineProjectsService: OnlineProjectsService,
    private projectService: ProjectService,
    private authService: AuthService,
    private teamsService: TeamsService,
    private videosService: YoutubeVideosService,
  ) {
    this.unsupportedBrowser = !supportedBrowsersRegexp.test(navigator.userAgent);

    activatedRoute.queryParams.pipe(first()).subscribe(params => {
      startupService.autorun(params);
    });

    this.quickstartProjectsService.getProjects().subscribe({
      next: list => (this.quickstartProjects = list),
      error: () => (this.isOffline = true),
    });

    this.authService.loggedIn$
      .pipe(
        switchMap(loggedIn =>
          loggedIn ? zip(this.onlineProjectsService.getProjectList(), this.teamsService.getTeamProjects()) : of(null),
        ),
        takeUntil(this.onDestroy$),
      )
      .subscribe({
        next: projects => {
          if (projects) {
            this.onlineAndTeamProjects = [...projects[0], ...projects[1]];
          } else {
            this.onlineAndTeamProjects = null;
          }
        },
        error: () => (this.isOffline = true),
      });
  }

  newProjectClick() {
    this.dialog.open(NewProjectModalComponent);
  }

  openTemplatesModal(): void {
    if (this.isOffline || this.quickstartProjects == null) {
      return;
    }

    this.dialog.open<ProjectListModalComponent, ProjectListModalData>(ProjectListModalComponent, {
      data: {
        quickstart: true,
        projectList: this.quickstartProjects,
      },
    });
  }

  openProjectList() {
    if (this.isOffline || this.onlineAndTeamProjects == null) return;

    this.dialog.open<ProjectListModalComponent, ProjectListModalData>(ProjectListModalComponent, {
      data: { quickstart: false, projectList: this.onlineAndTeamProjects },
    });
  }

  dragEnter() {
    this.dragCounter++;
    this.showDragArea = true;
  }

  dragLeave() {
    this.dragCounter--;

    if (this.dragCounter === 0) {
      this.showDragArea = false;
    }
  }

  drop(event: DragEvent) {
    this.showDragArea = false;
    this.dragCounter = 0;

    event.preventDefault();

    if (event.dataTransfer.items.length !== 1) {
      this.dialog.open(ConfirmModalComponent, {
        data: {
          title: $localize`Fehler`,
          prompt: $localize`Es kann nur eine Projektdatei auf einmal geladen werden.`,
          buttonDeny: 'Okay',
          size: 'small',
        },
      });
      return;
    }

    const item = event.dataTransfer.items[0];
    if (item.kind !== 'file' || item.getAsFile().type !== 'application/json') {
      this.dialog.open(ConfirmModalComponent, {
        data: {
          title: $localize`Fehler`,
          prompt: $localize`Es können nur Projektdateien geladen werden.`,
          buttonDeny: 'Okay',
          size: 'small',
        },
      });
      return;
    }

    const file = item.getAsFile();
    this.loadProjectFile(file);
  }

  dragOver(event: DragEvent) {
    event.preventDefault();
    event.dataTransfer.dropEffect = 'move';
  }

  private loadProjectFile(file: File) {
    this.projectService.importFile(file).subscribe({
      error: () => {
        this.dialog.open(JsonErrorModalComponent);
      },
    });
  }

  public openVideoModal(): void {
    this.dialog.open(YoutubeModalComponent, {
      data: {
        videos: this.videosService.videos,
        categories: this.videosService.categories,
        categoryIdx: 0,
      },
    });
  }
}
