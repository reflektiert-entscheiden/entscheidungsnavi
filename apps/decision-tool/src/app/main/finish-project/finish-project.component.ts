import { Component, TemplateRef, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { DecisionData, DecisionQuality, NaviStep, NaviStepNames } from '@entscheidungsnavi/decision-data';
import { PdfExportModalComponent } from '../../pdf-export-modal/pdf-export-modal.component';
import { ExcelExportService } from '../../excel-export';
import { ProjectService } from '../../data/project';
import { HelpMenuProvider, helpPage } from '../../help/help';
import { Navigation, NavLine, navLineElement } from '../../../modules/shared/navline';
import { LanguageService } from '../../data/language.service';
import { NavigationStepMetaData } from '../../../modules/shared/navigation/educational-navigation';

@Component({
  selector: 'dt-finish-project',
  templateUrl: './finish-project.component.html',
  styleUrls: ['./finish-project.component.scss'],
})
export class FinishProjectComponent implements HelpMenuProvider, Navigation {
  navLine = new NavLine({
    middle: [
      navLineElement()
        .label($localize`Projekt exportieren`)
        .icon('file_download', 'right')
        .onClick(() => this.jsonExport())
        .build(),
      navLineElement()
        .label($localize`PDF-Report des gesamten Projektes`)
        .onClick(() => this.pdfExport())
        .build(),
      navLineElement()
        .label($localize`Excel-Export der Ergebnismatrix`)
        .onClick(() => this.excelExport())
        .build(),
      navLineElement()
        .label($localize`Feedback`)
        .onClick(() => this.feedback())
        .build(),
    ],
  });

  helpMenu = [
    helpPage()
      .name($localize`So funktioniert's`)
      .template(() => this.helpContent1Educational)
      .build(),
    helpPage()
      .name($localize`Weitere Hinweise`)
      .template(() => this.helpContent2Educational)
      .build(),
  ];
  steps: { [key in NaviStep]: NavigationStepMetaData & NaviStepNames };
  decisionQuality: DecisionQuality;

  @ViewChild('helpContent1Educational') helpContent1Educational: TemplateRef<any>;
  @ViewChild('helpContent2Educational') helpContent2Educational: TemplateRef<any>;

  constructor(
    private excelExportService: ExcelExportService,
    private projectService: ProjectService,
    private dialog: MatDialog,
    private decisionData: DecisionData,
    languageService: LanguageService,
  ) {
    this.steps = languageService.steps;
    this.decisionQuality = this.decisionData.decisionQuality;
  }

  excelExport() {
    this.excelExportService.performExcelExport(false);
  }

  pdfExport() {
    this.dialog.open(PdfExportModalComponent);
  }

  jsonExport() {
    this.projectService.getProject().exportFile();
  }

  feedback() {
    window.open('https://entscheidungsnavi.de/contact', '_blank');
  }
}
