import { Injectable } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRouteSnapshot, Router, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { ResetPasswordModalComponent } from './reset-password-modal.component';

@Injectable({ providedIn: 'root' })
export class ResetPasswordGuard {
  constructor(
    private dialog: MatDialog,
    private router: Router,
  ) {}
  canActivate(
    route: ActivatedRouteSnapshot,
    _state: RouterStateSnapshot,
  ): boolean | UrlTree | Observable<boolean | UrlTree> | Promise<boolean | UrlTree> {
    const token = route.params['token'];

    this.dialog.open(ResetPasswordModalComponent, { data: token });

    this.router.navigateByUrl('/start');
    return false;
  }
}
