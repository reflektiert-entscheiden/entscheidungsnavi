import { AfterViewInit, Component, DestroyRef, ElementRef, EventEmitter, Input, Output, TemplateRef, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import {
  AccountModalComponent,
  EmailConfirmationModalComponent,
  PopOverRef,
  PopOverService,
  SnackbarComponent,
  SnackbarData,
} from '@entscheidungsnavi/widgets';
import { distinctUntilChanged, filter, firstValueFrom, take } from 'rxjs';
import { AuthService } from '@entscheidungsnavi/api-client';
import { DecisionData, ProjectMode } from '@entscheidungsnavi/decision-data';
import { takeUntilDestroyed } from '@angular/core/rxjs-interop';
import { MatSnackBar, MatSnackBarRef } from '@angular/material/snack-bar';
import { OnlineProject, ProjectService } from '../../data/project';
import { ProjectsModalComponent } from '../../navigation';
import { AppSettingsModalComponent } from '../app-settings/app-settings-modal.component';
import { ServiceWorkerService } from '../../../services/service-worker.service';
import { TrackingService } from '../../data/tracking.service';
import { LanguageService } from '../../data/language.service';
import { SideBySideService } from '../../navigation-layout/side-by-side.service';
import { ChangeLanguageService } from '../../data/change-language.service';
import { ModeTransitionService } from '../../../modules/shared/mode-transition/mode-transition.service';
import { AutoSaveService } from '../../data/project/auto-save.service';
import { EventsOverviewModalComponent } from './events';
import { LoginModalComponent } from './login-modal/login-modal.component';

@Component({
  selector: 'dt-userarea',
  templateUrl: 'userarea.component.html',
  styleUrls: ['./userarea.component.scss'],
})
export class UserareaComponent implements AfterViewInit {
  userPopoverRef: PopOverRef;
  updatePopoverRef: PopOverRef;

  @Input({ required: true }) sidenavEnabled: boolean;
  @Input() isStartPage = false;
  isUpdateAvailable = false;

  @Output()
  hamburgerButtonClick = new EventEmitter<void>();

  @ViewChild('updatePopover')
  updatePopoverTemplate: TemplateRef<any>;

  @ViewChild('userButton', { read: ElementRef })
  userButtonRef: ElementRef<HTMLButtonElement>;

  @ViewChild('loginButton', { read: ElementRef })
  loginButtonRef: ElementRef<HTMLButtonElement>;

  @ViewChild('updateButton', { read: ElementRef })
  updateButtonRef: ElementRef<HTMLButtonElement>;

  isMobileLayout = false;

  get language() {
    return this.languageService.isEnglish ? 'en' : 'de';
  }

  constructor(
    protected authService: AuthService,
    protected decisionData: DecisionData,
    private dialog: MatDialog,
    private snackBar: MatSnackBar,
    protected projectService: ProjectService,
    protected sideBySideService: SideBySideService,
    private popOverService: PopOverService,
    private serviceWorkerService: ServiceWorkerService,
    private trackingService: TrackingService,
    private languageService: LanguageService,
    private changeLanguageService: ChangeLanguageService,
    private autoSaveService: AutoSaveService,
    private destroyRef: DestroyRef,
    private modeTransitionService: ModeTransitionService,
  ) {
    this.authService.onLogin$.pipe(takeUntilDestroyed()).subscribe(user => {
      // Make sure CD has run so the button exists
      setTimeout(() => {
        if (this.userButtonRef) this.popOverService.whistle(this.userButtonRef, $localize`Erfolgreich angemeldet`, 'login');
      });

      if (user.emailConfirmed === false) {
        this.dialog.open(EmailConfirmationModalComponent);
      }
    });

    this.authService.onLogout$.pipe(takeUntilDestroyed()).subscribe(() => {
      // Make sure CD has run so the button exists
      setTimeout(() => {
        if (this.loginButtonRef) this.popOverService.whistle(this.loginButtonRef, $localize`Erfolgreich abgemeldet`, 'logout');
      });

      this.userPopoverRef?.close();
    });
  }

  ngAfterViewInit() {
    let openSaveErrorSnackbar: MatSnackBarRef<SnackbarComponent>;
    this.autoSaveService.saveState$.pipe(distinctUntilChanged(), takeUntilDestroyed(this.destroyRef)).subscribe(saveState => {
      if (saveState === 'error') {
        openSaveErrorSnackbar = this.snackBar.openFromComponent<SnackbarComponent, SnackbarData>(SnackbarComponent, {
          data: {
            icon: 'cloud_off',
            message: $localize`Das Speichern ist fehlgeschlagen. Möglicherweise ist Deine Internetverbindung instabil.
            Bis die Verbindung wiederhergestellt ist, arbeitest Du offline.`,
            dismissButton: true,
            cypressId: 'autosave-failed-snackbar',
          },
        });
      } else if (saveState === 'off' || saveState === 'idle') {
        openSaveErrorSnackbar?.dismiss();
        openSaveErrorSnackbar = null;
      }
    });

    this.serviceWorkerService.updateState$
      .pipe(
        filter(state => state === 'update-ready'),
        take(1),
        takeUntilDestroyed(this.destroyRef),
      )
      .subscribe(() => {
        this.isUpdateAvailable = true;
        this.toggleUpdatePopover();
      });
  }

  /**
   * Logout, close project and navigate to /start if an online project is loaded.
   */
  async logout() {
    const project = this.projectService.getProject();

    const needsToCloseProject = project && project instanceof OnlineProject;
    if (needsToCloseProject && !(await firstValueFrom(this.projectService.closeProject()))) {
      // We have an online project and the user does not want to close it
      return;
    }

    this.authService.logout().subscribe();

    this.trackingService.trackEvent('logout', { category: 'user' });
  }

  activateNaviUpdate() {
    this.projectService
      .confirmProjectUnload('activate-update')
      .pipe(filter(Boolean))
      .subscribe(() => this.serviceWorkerService.activateUpdate());
  }

  openUserPopover(template: TemplateRef<any>) {
    this.userPopoverRef = this.popOverService.open(template, this.userButtonRef.nativeElement, {
      position: [{ originX: 'center', originY: 'bottom', overlayX: 'center', overlayY: 'top' }],
      closeCallback: () => (this.userPopoverRef = null),
    });
  }

  openEventModal() {
    this.dialog
      .open(EventsOverviewModalComponent)
      .afterClosed()
      .subscribe(res => {
        if (res === 'open-project') {
          this.userPopoverRef?.close();
        }
      });
  }

  openAccountModal() {
    this.dialog.open(AccountModalComponent);
  }

  toggleUpdatePopover() {
    if (this.updatePopoverRef) {
      this.updatePopoverRef.close();
      this.updatePopoverRef = null;
      return;
    }
    this.updatePopoverRef = this.popOverService.open(this.updatePopoverTemplate, this.updateButtonRef.nativeElement, {
      position: [{ originX: 'center', originY: 'bottom', overlayX: 'center', overlayY: 'top' }],
      hasBackdrop: false,
      closeCallback: () => (this.updatePopoverRef = null),
    });
  }

  openProjectModal() {
    this.dialog.open(ProjectsModalComponent);
  }

  openLoginModal() {
    this.dialog.open(LoginModalComponent);
  }

  openAppSettings() {
    this.dialog.open(AppSettingsModalComponent);
  }

  changeLanguage(newLanguage: 'de' | 'en') {
    if (this.language !== newLanguage) {
      this.changeLanguageService.changeLanguage();
    }
  }

  toggleHelpAssistant() {
    if (this.sideBySideService.isOpen) {
      this.sideBySideService.close();
    } else {
      this.sideBySideService.openHelp();
    }
  }

  switchMode(to: ProjectMode) {
    if (to === 'professional') {
      this.modeTransitionService.transitionIntoProfessional();
    } else if (to === 'educational') {
      this.modeTransitionService.transitionIntoEducational();
    }
  }
}
