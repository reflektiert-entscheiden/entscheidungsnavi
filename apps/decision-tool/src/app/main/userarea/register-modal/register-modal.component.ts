import { Component, OnInit, ViewChild } from '@angular/core';

import { HttpErrorResponse } from '@angular/common/http';
import { UntypedFormControl, UntypedFormGroup, Validators } from '@angular/forms';
import { PasswordFormComponent } from '@entscheidungsnavi/widgets';
import { MatDialogRef } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { AuthService } from '@entscheidungsnavi/api-client';

const timRegexp = /^(?![A-Za-z]{2}\d{6}@rwth-aachen.de).*$/;

@Component({
  templateUrl: 'register-modal.component.html',
  styleUrls: ['register-modal.component.scss'],
})
export class RegisterModalComponent implements OnInit {
  @ViewChild(PasswordFormComponent, { static: true })
  passwordForm: PasswordFormComponent;

  registerForm: UntypedFormGroup;

  constructor(
    private authService: AuthService,
    private dialog: MatDialogRef<RegisterModalComponent, number>,
    private snackBarService: MatSnackBar,
  ) {}

  close() {
    this.dialog.close(0);
  }

  ngOnInit() {
    this.registerForm = new UntypedFormGroup({
      email: new UntypedFormControl('', [Validators.required, Validators.email, Validators.pattern(timRegexp)]),
      username: new UntypedFormControl(''),
      privacy: new UntypedFormControl(false, Validators.requiredTrue),
    });
  }

  onSubmit() {
    if (this.registerForm.errors?.['serverError']) {
      this.registerForm.updateValueAndValidity();
    }
    if (this.registerForm.valid) {
      this.registerForm.disable({ emitEvent: false });

      const email = this.registerForm.get('email').value;
      const username = this.registerForm.get('username').value;

      const password = this.registerForm.get('password').value;

      this.authService.createUser(email, password, username).subscribe({
        next: () => {
          this.dialog.close(1);
          this.snackBarService.open($localize`Du hast Dich erfolgreich registriert!`, undefined, { duration: 4000 });
        },
        error: (error: HttpErrorResponse) => {
          this.registerForm.enable({ emitEvent: false });
          if (error.status === 409) {
            this.registerForm.controls.email.setErrors({ alreadyExists: true });
          } else {
            this.registerForm.setErrors({ serverError: true });
          }
        },
      });
    }
  }
}
