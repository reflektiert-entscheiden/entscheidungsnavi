import { HttpErrorResponse } from '@angular/common/http';
import { Component, Inject, isDevMode, OnInit } from '@angular/core';
import { UntypedFormControl, UntypedFormGroup, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialog, MatDialogRef } from '@angular/material/dialog';
import { AuthService } from '@entscheidungsnavi/api-client';
import { RequestPasswordResetModalComponent } from '@entscheidungsnavi/widgets';
import { SEED_USERS, SeedUser } from '@entscheidungsnavi/tools';
import { TrackingService } from '../../../data/tracking.service';
import { ENVIRONMENT } from '../../../../environments/environment';
import { RegisterModalComponent } from '../register-modal/register-modal.component';

@Component({
  templateUrl: 'login-modal.component.html',
  styleUrls: ['login-modal.component.scss'],
})
export class LoginModalComponent implements OnInit {
  loginForm: UntypedFormGroup;

  protected readonly seedUsers = SEED_USERS;

  get shouldShowSeedUsers() {
    return isDevMode() || ENVIRONMENT.type === 'preview';
  }

  constructor(
    private authService: AuthService,
    private matDialog: MatDialog,
    private dialogRef: MatDialogRef<LoginModalComponent>,
    private trackingService: TrackingService,
    @Inject(MAT_DIALOG_DATA) protected data?: { showSaveHint?: boolean },
  ) {}

  openRegisterModal() {
    this.matDialog
      .open(RegisterModalComponent)
      .afterClosed()
      .subscribe(result => {
        if (result === 1) {
          this.close(true);
        }
      });
  }

  openPasswordResetModal() {
    this.matDialog.open(RequestPasswordResetModalComponent);
  }

  ngOnInit() {
    this.loginForm = new UntypedFormGroup({
      email: new UntypedFormControl('', [Validators.required]),
      password: new UntypedFormControl('', [Validators.required]),
    });
  }

  close(loggedIn = false) {
    this.dialogRef.close(loggedIn);
  }

  loginWithSeedUser(user: SeedUser) {
    this.loginForm.controls.email.setValue(user.email);
    this.loginForm.controls.password.setValue(user.password);
    this.onSubmit();
  }

  onSubmit() {
    this.loginForm.updateValueAndValidity();

    if (this.loginForm.valid) {
      this.loginForm.disable({ emitEvent: false });

      const email = this.loginForm.controls.email.value;
      const password = this.loginForm.controls.password.value;

      this.authService.login(email, password).subscribe({
        next: () => {
          this.close(true);
          this.trackingService.trackEvent('login', { category: 'user' });
        },
        error: (error: HttpErrorResponse) => {
          this.loginForm.enable();
          console.error(error);
          if (error.status === 401) {
            this.loginForm.setErrors({ 'server-error': 'credentials' });
          } else if (error.status === 429) {
            this.loginForm.setErrors({ 'server-error': 'rate-limit' });
          } else {
            this.loginForm.setErrors({ 'server-error': 'something' });
          }
        },
      });
    }
  }
}
