import { HttpErrorResponse } from '@angular/common/http';
import { AfterViewInit, ChangeDetectorRef, Component, ElementRef, Inject, ViewChild } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialog, MatDialogRef } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { EventManagementService, EventRegistrationWithDataDto, EventsService } from '@entscheidungsnavi/api-client';
import { EventProjectRequirements, EventRegistration, UserEvent } from '@entscheidungsnavi/api-types';
import { DecisionData } from '@entscheidungsnavi/decision-data';
import { DECISION_QUALITY_CRITERIA } from '@entscheidungsnavi/decision-data';
import { OnDestroyObservable } from '@entscheidungsnavi/tools';
import { ConfirmModalComponent, ConfirmModalData, FileExport, PopOverService, QuestionnaireComponent } from '@entscheidungsnavi/widgets';
import { loadingIndicator } from '@entscheidungsnavi/widgets/loading-snackbar/loading-snackbar.component';
import {
  catchError,
  concatMap,
  debounceTime,
  delay,
  EMPTY,
  exhaustMap,
  filter,
  finalize,
  interval,
  merge,
  mergeMap,
  Observable,
  of,
  repeat,
  retry,
  startWith,
  Subject,
  Subscription,
  take,
  takeUntil,
  tap,
  timer,
} from 'rxjs';
import { readTextWithMetadata } from '@entscheidungsnavi/decision-data/export';
import sanitize from 'sanitize-filename';
import { DecisionDataExportService } from '../../../../data/decision-data-export.service';
import { ProjectService } from '../../../../data/project';

@Component({
  templateUrl: './event-modal.component.html',
  styleUrls: ['./event-modal.component.scss'],
})
export class EventModalComponent implements AfterViewInit {
  protected event: UserEvent;
  protected submission: EventRegistration;

  protected projectRequirements: [requirementName: keyof EventProjectRequirements, required: boolean, satisfied: boolean][] = [];

  protected questionnaireSatisfied: boolean;
  protected dataUsageAuthorized = false;

  protected freeTextContent: string;
  protected freeTextCharacterCount: number;
  protected freeTextChange$ = new Subject<void>();

  protected startDateReached: boolean;
  protected pastEndDate: boolean;

  @ViewChild(QuestionnaireComponent)
  questionnaireComponent: QuestionnaireComponent;

  @ViewChild('submitButton', { read: ElementRef })
  submitButtonRef: ElementRef<HTMLButtonElement>;

  @ViewChild('saveIndicator')
  saveIndicatorRef: ElementRef<HTMLElement>;

  protected projectLoaded: boolean;

  get submitted() {
    return this.submission.submitted;
  }

  @OnDestroyObservable()
  private onDestroy$: Observable<void>;

  protected saveState: 'idle' | 'saving' | 'saved' | 'errored' = 'idle';
  protected lastSaveSuccessful = true;
  protected isBusy = false; // submitting or leaving

  protected pendingCloseSubscription: Subscription;
  protected secondsSinceClose = 0;

  eventRegistration: any;

  get freeTextSatisfied() {
    return this.freeTextCharacterCount >= this.event.freeTextConfig.minLength;
  }

  get submissionValid() {
    const questionnaireValid = this.questionnaireSatisfied;
    const freeTextValid = !this.event.freeTextConfig.enabled || this.freeTextSatisfied;
    const projectValid = this.projectRequirements.every(requirement => !requirement[1] || requirement[2]);
    const datesValid = this.startDateReached && !this.pastEndDate;
    const dataUsageValid = !this.event.requireDataUsageAuthorization || this.dataUsageAuthorized;

    return this.projectLoaded && questionnaireValid && freeTextValid && projectValid && datesValid && dataUsageValid;
  }

  constructor(
    private dialogRef: MatDialogRef<EventModalComponent>,
    private eventsService: EventsService,
    private eventManagementService: EventManagementService,
    @Inject(MAT_DIALOG_DATA) data: { submission: EventRegistration },
    private snackBar: MatSnackBar,
    protected decisionData: DecisionData,
    private cdRef: ChangeDetectorRef,
    private popOverService: PopOverService,
    private exportService: DecisionDataExportService,
    private dialog: MatDialog,
    protected projectService: ProjectService,
  ) {
    this.submission = data.submission;
    this.event = this.submission.event;

    this.projectLoaded = projectService.isProjectLoaded();

    const projectRequirementChecks = {
      requireDecisionStatement: this.decisionData.validateDecisionStatement()[0],
      requireObjectives: this.decisionData.validateObjectives()[0],
      requireAlternatives: this.decisionData.validateAlternatives()[0],
      requireImpactModel: this.decisionData.validateOutcomes()[0],
      requireObjectiveWeights: this.decisionData.validateWeights()[0],
      requireDecisionQuality: DECISION_QUALITY_CRITERIA.every(criteria => this.decisionData.decisionQuality.criteriaValues[criteria] > -1),
    };

    for (const requirement of Object.keys(this.event.projectRequirements)) {
      const requirementKey = requirement as keyof EventProjectRequirements;
      this.projectRequirements.push([
        requirementKey,
        this.event.projectRequirements[requirementKey],
        projectRequirementChecks[requirementKey],
      ]);
    }

    this.freeTextContent = this.submission.freeText;

    this.startDateReached = this.event.startDate == null || this.event.startDate.getTime() < Date.now();
    // Only schedule the update if it is within 10 days. Otherwise, the delay will execute immediately due to an internal limitation
    // of RxJS. https://github.com/ReactiveX/rxjs/issues/3015
    if (!this.startDateReached && this.event.startDate.getTime() - Date.now() < 10 * 24 * 60 * 60 * 1000) {
      of(null)
        .pipe(delay(this.event.startDate), takeUntil(this.onDestroy$))
        .subscribe(() => {
          this.startDateReached = true;
          this.cdRef.detectChanges();
        });
    }

    this.pastEndDate = this.event.endDate != null && this.event.endDate.getTime() < Date.now();
    // Only schedule the update if it is within 10 days. Otherwise, the delay will execute immediately due to an internal limitation
    // of RxJS. https://github.com/ReactiveX/rxjs/issues/3015
    if (!this.pastEndDate && this.event.endDate != null && this.event.endDate.getTime() - Date.now() < 10 * 24 * 60 * 60 * 1000) {
      of(null)
        .pipe(delay(this.event.endDate), takeUntil(this.onDestroy$))
        .subscribe(() => {
          this.pastEndDate = true;
          this.cdRef.detectChanges();
        });
    }

    this.questionnaireSatisfied = this.event.questionnaire?.length === 0;
  }

  ngAfterViewInit() {
    const changes$ = merge(this.freeTextChange$, this.questionnaireComponent?.questionnaireForm.valueChanges ?? EMPTY).pipe(
      filter(() => !this.submitted),
    );

    changes$.pipe(takeUntil(this.onDestroy$)).subscribe(() => {
      this.saveState = 'saving';
    });

    const manualSaveTrigger$ = new Subject<void>();
    const delayedExhaustedChanges$ = changes$.pipe(exhaustMap(() => timer(5000)));
    const debouncedChanges$ = changes$.pipe(debounceTime(2000));

    merge(delayedExhaustedChanges$, debouncedChanges$, manualSaveTrigger$)
      .pipe(
        take(1),
        repeat(),
        concatMap(() =>
          this.save().pipe(
            retry(2),
            tap({
              next: () => {
                this.saveState = 'saved';
                this.lastSaveSuccessful = true;
              },
              error: () => {
                this.saveState = 'errored';
                if (this.lastSaveSuccessful) {
                  this.lastSaveSuccessful = false;

                  const saveSuccessful$ = new Subject<void>();

                  interval(2000)
                    .pipe(takeUntil(merge(saveSuccessful$, this.onDestroy$)))
                    .subscribe(() => {
                      if (this.lastSaveSuccessful) {
                        saveSuccessful$.next();
                      } else {
                        manualSaveTrigger$.next();
                      }
                    });
                }
              },
            }),
            catchError(() => of(null)),
          ),
        ),
        mergeMap(() => timer(2000)),
        tap(() => {
          if (this.saveState === 'saved') {
            this.saveState = 'idle';
          }
        }),
        takeUntil(this.onDestroy$),
      )
      .subscribe();
  }

  close(res?: string) {
    if (this.isBusy) {
      return;
    }

    if (!this.lastSaveSuccessful || this.saveState !== 'saving') {
      this.dialogRef.close(res);
    }

    if (this.pendingCloseSubscription) {
      if (this.secondsSinceClose > 2) {
        this.dialogRef.close(res);
      }
      return;
    }

    this.pendingCloseSubscription = interval(1000)
      .pipe(
        startWith(-1),
        tap(secondsSinceClose => {
          this.secondsSinceClose = secondsSinceClose;
        }),
        filter(() => this.saveState === 'idle'),
        takeUntil(this.onDestroy$),
      )
      .subscribe(() => this.dialogRef.close(res));
  }

  private save() {
    return this.eventsService
      .updateSubmission(this.submission.id, {
        questionnaireResponses: this.questionnaireComponent?.turnIntoResponse(),
        freeText: this.freeTextContent,
      })
      .pipe(tap(update => Object.assign(this.submission, update)));
  }

  submit() {
    if (!this.submissionValid || this.isBusy) {
      return;
    }

    const projectName = this.projectService.getProjectName();

    this.dialog
      .open<ConfirmModalComponent, ConfirmModalData>(ConfirmModalComponent, {
        data: {
          title: $localize`Projekt abgeben`,
          prompt: $localize`Bist Du sicher, dass Du das Projekt „${projectName}”
          für die Veranstaltung „${this.submission.event.name}” endgültig abgeben möchtest?
          Du kannst den Inhalt der Abgabe anschließend nicht mehr bearbeiten.`,
          buttonConfirm: $localize`Ja, endgültig abgeben`,
          buttonDeny: $localize`Nein, abbrechen`,
        },
      })
      .afterClosed()
      .subscribe(confirmed => {
        if (!confirmed) {
          return;
        }

        const response = this.questionnaireComponent?.turnIntoResponse();
        const projectData = this.exportService.dataToText(projectName);

        this.isBusy = true;
        this.eventsService
          .updateSubmission(this.submission.id, { projectData, questionnaireResponses: response, freeText: this.freeTextContent })
          .pipe(
            tap(update => {
              this.submission.updatedAt = update.updatedAt;
            }),
            loadingIndicator(this.snackBar, $localize`Abgabe wird finalisiert…`),
            concatMap(() => this.eventsService.submitSubmission(this.submission.id)),
            finalize(() => (this.isBusy = false)),
          )
          .subscribe({
            next: () => (this.submission.submitted = true),
            error: error => {
              if (error instanceof HttpErrorResponse && error.status === 409) {
                this.popOverService.whistle(this.submitButtonRef, $localize`Du hast für diese Veranstaltung bereits abgegeben.`);
                return;
              }

              this.popOverService.whistle(
                this.submitButtonRef,
                $localize`Bei der Abgabe ist ein Fehler aufgetreten. Bitte versuche es später erneut.`,
              );
            },
          });
      });
  }

  leaveEvent(leaveButton: ElementRef) {
    this.isBusy = true;
    this.eventsService
      .leaveEvent(this.submission.id)
      .pipe(
        loadingIndicator(this.snackBar, $localize`Veranstaltung wird verlassen…`),
        finalize(() => (this.isBusy = false)),
      )
      .subscribe({
        next: () => {
          this.snackBar.open($localize`Veranstaltung erfolgreich verlassen!`, 'Ok', { duration: 4000 });
          this.eventsService.refreshEvents();
          this.dialogRef.close();
        },
        error: () =>
          this.popOverService.whistle(
            leaveButton,
            $localize`Beim Verlassen ist ein unbekannter Fehler aufgetreten. Bitte versuche es später erneut.`,
          ),
      });
  }

  getEventRegistrationWithSubmission(eventId: string, userId: string) {
    return this.eventManagementService.getOneEventRegistration(eventId, userId);
  }

  downloadProject(downloadProjectButton: ElementRef) {
    this.isBusy = true;
    this.getEventRegistrationWithSubmission(this.submission.event.id, this.submission.id)
      .pipe(
        finalize(() => {
          this.isBusy = false;
        }),
      )
      .subscribe({
        next: (eventRegistration: EventRegistrationWithDataDto) => {
          const { projectName } = readTextWithMetadata(eventRegistration.projectData);
          const fileName = (sanitize(projectName) || 'export') + '.json';
          FileExport.download(fileName, eventRegistration.projectData);
        },
        error: () => {
          this.popOverService.whistle(
            downloadProjectButton,
            $localize`Ein unbekannter Fehler ist beim Herunterladen der Abgabe aufgetreten. Bitte versuche es später erneut.`,
          );
        },
      });
  }

  openProject(eventId: string, registrationId: string, openProjectButton: ElementRef) {
    this.isBusy = true;
    this.projectService
      .loadEventSubmission(eventId, registrationId)
      .pipe(finalize(() => (this.isBusy = false)))
      .subscribe({
        next: () => {
          this.isBusy = false;
          this.close('open-project');
        },
        error: () => {
          this.popOverService.whistle(
            openProjectButton,
            $localize`Ein unbekannter Fehler ist beim Öffnen der Abgabe aufgetreten. Bitte versuche es später erneut.`,
          );
        },
      });
  }
}
