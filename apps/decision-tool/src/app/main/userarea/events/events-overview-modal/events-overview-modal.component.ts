import { HttpErrorResponse } from '@angular/common/http';
import { Component, ViewChild } from '@angular/core';
import { NonNullableFormBuilder } from '@angular/forms';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { EventsService } from '@entscheidungsnavi/api-client';
import { EventRegistration } from '@entscheidungsnavi/api-types';
import { PopOverService } from '@entscheidungsnavi/widgets';
import { catchError, EMPTY } from 'rxjs';
import { EventModalComponent } from '../event-modal/event-modal.component';

@Component({
  templateUrl: './events-overview-modal.component.html',
  styleUrls: ['./events-overview-modal.component.scss'],
})
export class EventsOverviewModalComponent {
  protected hasError = false;
  protected eventSubmissions$ = this.eventsService.events$.pipe(
    catchError(() => {
      this.hasError = true;
      return EMPTY;
    }),
  );

  @ViewChild('joinEventForm')
  joinEventForm: HTMLElement;

  protected joinEventFormGroup = this.fb.group({ code: [''] });

  constructor(
    private dialog: MatDialog,
    private dialogRef: MatDialogRef<EventsOverviewModalComponent>,
    private eventsService: EventsService,
    private popOverService: PopOverService,
    private fb: NonNullableFormBuilder,
  ) {}

  getSubmissionState(submission: EventRegistration) {
    if (submission.submitted) {
      return 'submitted';
    } else if (submission.event.endDate && submission.event.endDate.getTime() < Date.now()) {
      return 'expired';
    } else if (submission.event.startDate && submission.event.startDate.getTime() > Date.now()) {
      return 'notstarted';
    } else {
      return 'inprogress';
    }
  }

  openEventModalForSubmission(submission: EventRegistration) {
    this.dialog
      .open(EventModalComponent, { data: { submission } })
      .afterClosed()
      .subscribe(res => {
        if (res === 'open-project') {
          this.close('open-project');
        }
      });
  }

  joinEvent() {
    if (!this.joinEventFormGroup.value.code || this.joinEventFormGroup.disabled) {
      return;
    }

    const code = this.joinEventFormGroup.value.code;
    this.joinEventFormGroup.disable();

    this.eventsService.joinEvent(code).subscribe({
      next: () => {
        this.joinEventFormGroup.enable();
        this.joinEventFormGroup.reset();

        this.eventsService.refreshEvents();
      },
      error: error => {
        this.joinEventFormGroup.enable();
        this.joinEventFormGroup.controls.code.setErrors([{}]);

        if (error instanceof HttpErrorResponse) {
          if (error.status === 404) {
            this.popOverService.whistle(
              this.joinEventForm,
              $localize`Für den eingegebenen Code konnte keine Veranstaltung gefunden werden.`,
              'warning',
              5000,
              'accent',
            );
            return;
          } else if (error.status === 409) {
            this.popOverService.whistle(
              this.joinEventForm,
              $localize`Du bist bereits Teil dieser Veranstaltung.`,
              'warning',
              5000,
              'accent',
            );
            return;
          }
        }

        this.popOverService.whistle(
          this.joinEventForm,
          $localize`Bei der Anfrage an den Server ist ein unbekannter Fehler aufgetreten.
          Möglicherweise ist Deine Internetverbindung instabil.`,
          'error',
          5000,
          'warn',
        );
      },
    });
  }

  close(res?: string) {
    this.dialogRef.close(res);
  }
}
