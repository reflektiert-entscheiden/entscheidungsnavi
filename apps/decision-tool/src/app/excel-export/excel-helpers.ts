import { richToPlainText } from '@entscheidungsnavi/tools';
import { Cell, CellRichTextValue, RichText, Worksheet } from 'exceljs';
import { isString, range } from 'lodash';
import { Objective } from '@entscheidungsnavi/decision-data';

/*
 * excel helper functions
 */

export function fitColumnsToContent(ws: Worksheet, columns: number[]) {
  for (const cI of columns) {
    const column = ws.getColumn(cI);
    let maxWidth = 0;
    column.eachCell((cell: any, rowIndex: number) => {
      // Ignore Headers
      if (rowIndex > 2) {
        const mergeCount = Math.max(cell._mergeCount, cell.master._mergeCount);

        const value = cell.value;
        let cellLength = 0;

        if (value) {
          if (value.richText) {
            for (const line of value.richText) {
              const length = line.text.length;

              if (length > cellLength) {
                cellLength = length;
              }
            }
          } else if (isString(value)) {
            const length = value.length;

            if (length > cellLength) {
              cellLength = length;
            }
          }
        }

        if (mergeCount > 0) {
          cellLength = Math.round(cellLength / (mergeCount + 1));
        }

        cellLength += 2; // Padding due to column width not being exactly character count

        if (cellLength > maxWidth) {
          maxWidth = cellLength;
        }
      }
    });

    if (maxWidth > column.width) {
      column.width = maxWidth;
    }
  }
}

export function fitObjectiveNameHeader(ws: Worksheet, objectives: Objective[], objectiveColumns: number[]) {
  const firstRow = ws.getRow(1);

  let nextObjectiveStart = 3;

  objectives.forEach((_obj, index) => {
    const firstCell = firstRow.getCell(nextObjectiveStart);
    const value = firstCell.value;
    const columnCount = objectiveColumns[index];

    let desiredWidth = 0;

    if (isRichText(value)) {
      for (const line of value.richText) {
        const length = line.text.length;

        if (length > desiredWidth) {
          desiredWidth = length;
        }
      }
    }

    desiredWidth += 2;

    const currentWidth = () => {
      return range(0, columnCount)
        .map(offset => {
          return ws.getColumn(nextObjectiveStart + offset).width;
        })
        .reduce((previous, current) => {
          return previous + current;
        }, 0);
    };

    while (currentWidth() < desiredWidth) {
      for (let cI = nextObjectiveStart; cI < nextObjectiveStart + columnCount; cI++) {
        ws.getColumn(cI).width++;
      }
    }

    nextObjectiveStart += columnCount;
  });
}

export function applyNumberFormatting(cell: Cell, percent = false) {
  const numberRep = Number.parseFloat(cell.value.toString().replace(',', '.'));
  if (!isNaN(numberRep)) {
    if (percent) {
      if (Number.isInteger(numberRep * 100)) {
        cell.numFmt = '0%';
      } else {
        cell.numFmt = '#0.##%';
      }
    } else if (Number.isInteger(numberRep)) {
      cell.numFmt = '0';
    } else {
      cell.numFmt = '0.#####';
    }
    cell.value = numberRep;
  } else {
    cell.numFmt = '@';
  }
}

export function defaultValues(cell: Cell) {
  return defaultBorder(greyBackground(centerAndWrapText(cell)));
}

export function centerAndWrapText(cell: Cell) {
  cell.alignment = { vertical: 'middle', horizontal: 'center', wrapText: true };

  return cell;
}

export function background(cell: Cell, color: string) {
  cell.fill = {
    type: 'pattern',
    pattern: 'solid',
    fgColor: { argb: '00' + color },
  };

  return cell;
}

export function greyBackground(cell: Cell) {
  background(cell, 'e7e8ea');

  return cell;
}

export function defaultBorder(cell: Cell) {
  cell.border = {
    top: { style: 'thin', color: { argb: '00FFFFFF' } },
    left: { style: 'thin', color: { argb: '00FFFFFF' } },
    bottom: { style: 'thin', color: { argb: '00FFFFFF' } },
    right: { style: 'thin', color: { argb: '00FFFFFF' } },
  };

  return cell;
}

export function isRichText(value: any): value is CellRichTextValue {
  return 'richText' in value;
}

export function multiLine(lines: string[]) {
  const array: RichText[] = [];
  let result: CellRichTextValue = { richText: array };

  result = {
    richText: array,
  };

  lines.forEach((line, index) => {
    array.push({ text: line });

    if (index !== lines.length - 1) {
      array.push({ text: ' \r\n' });
    }
  });

  return result;
}

export function addNote(cell: Cell, comment: string) {
  if (comment == null) {
    cell.note = { texts: [{ text: '' }] };
    return;
  }
  try {
    cell.note = { texts: [{ text: richToPlainText(comment) }] };
  } catch (e) {
    console.error('Could not export Delta as note');
  }
}

/*
 * excel style
 */

export function objectiveBackground(cell: Cell) {
  background(cell, '3b4c5b');
  return cell;
}

export function fieldBackground(cell: Cell) {
  if (cell.fill == null) {
    background(cell, 'e7e8ea'); //f3f3f4
  }
  return cell;
}

export function addBorder(cell: Cell, borderColor: string) {
  if (cell.border == null) {
    cell.border = {
      top: { style: 'thin', color: { argb: borderColor } },
      left: { style: 'thin', color: { argb: borderColor } },
      bottom: { style: 'thin', color: { argb: borderColor } },
      right: { style: 'thin', color: { argb: borderColor } },
    };
  } else {
    // do not override existing borders
    if (cell.border.top == null) cell.border.top = { style: 'thin', color: { argb: borderColor } };
    if (cell.border.left == null) cell.border.left = { style: 'thin', color: { argb: borderColor } };
    if (cell.border.bottom == null) cell.border.bottom = { style: 'thin', color: { argb: borderColor } };
    if (cell.border.right == null) cell.border.right = { style: 'thin', color: { argb: borderColor } };
  }

  return cell;
}

export function defaultMatrixStyle(cell: Cell) {
  return addBorder(fieldBackground(centerAndWrapText(cell)), 'ff262626'); // ff262626
}

export function defaultHeaderStyle(cell: Cell) {
  return addBorder(objectiveBackground(centerAndWrapText(cell)), '00ffffff');
}

export function blueHeaderStyle(cell: Cell, text?: string) {
  if (text) {
    cell.value = {
      richText: [{ font: { color: { argb: '00ffffff' } }, text: text }],
    };
  }
  background(cell, '5d666f');
  addBorder(cell, '00ffffff');
  cell.border.left.style = 'thick';
  centerAndWrapText(cell);
  return cell;
}

export function columnToLetter(column: number) {
  let temp,
    letter = '';
  while (column > 0) {
    temp = (column - 1) % 26;
    letter = String.fromCharCode(temp + 65) + letter;
    column = (column - temp - 1) / 26;
  }
  return letter;
}

// collections the result if it exists (recognizes formulas), otherwise returns value
export function getCellValue(cell: Cell) {
  return cell.result ?? cell.value;
}

export function getHeaderRowNumber(objectives: Objective[]) {
  if (objectives.some(o => o.isIndicator)) {
    // has an extra row for scales
    return 4;
  } else {
    return 3;
  }
}

/* Calculates number of columns for each objective (both import and export). */
export function getObjectiveColumns(objectives: Objective[]): number[] {
  return objectives.map(obj => {
    let length = 2; // influence factor column + value
    if (obj.isIndicator) {
      length = obj.indicatorData.indicators.length + 1; // indicator columns + value
    }
    return length;
  });
}

export function getImpactModelName(isEnglish: boolean) {
  return isEnglish ? 'Consequences Table' : 'Wirkungsmodell';
}

export function getInfluenceFactorsName(isEnglish: boolean) {
  return isEnglish ? 'Influence Factors' : 'Einflussfaktoren';
}

export function getNumericalScale(objective: Objective, isEnglish: boolean) {
  if (isEnglish) {
    return (
      'Numerical scale: from ' + objective.numericalData.from + ' to ' + objective.numericalData.to + ' ' + objective.numericalData.unit
    );
  } else {
    return (
      'Numerische Skala: von ' + objective.numericalData.from + ' bis ' + objective.numericalData.to + ' ' + objective.numericalData.unit
    );
  }
}

export function getIndicatorScale(isEnglish: boolean) {
  return isEnglish ? 'Indicator scale' : 'Indikatorskala';
}
