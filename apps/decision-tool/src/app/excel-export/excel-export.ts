import { Worksheet } from 'exceljs';
import { InfluenceFactorNamePipe, SafeNumberPipe, StateNamePipe } from '@entscheidungsnavi/widgets';
import { DecisionData } from '@entscheidungsnavi/decision-data';
import { range } from 'lodash';
import { ProjectViewData } from '../project-view';
import {
  applyNumberFormatting,
  background,
  centerAndWrapText,
  defaultBorder,
  defaultValues,
  fitColumnsToContent,
  fitObjectiveNameHeader,
  multiLine,
} from './excel-helpers';

export class ExcelExport {
  constructor(
    private decisionData: DecisionData,
    private data: ProjectViewData,
    private safeNumberPipe: SafeNumberPipe,
    private influenceFactorNamePipe: InfluenceFactorNamePipe,
    private stateNamePipe: StateNamePipe,
    private onlyExpanded: boolean,
    private ws: Worksheet,
  ) {}

  /**
   * Generates the required data and adds it to the provided worksheet {@link ws}.
   */
  performExport() {
    // Alternatives
    this.addAlternativeColumn();

    // Total Utility
    this.addTotalUtilityColumn();

    // Objectives
    const [forceSingleLine, objectiveColumns] = this.addObjectives();

    // Make Columns that requested it as wide as their longest line
    fitColumnsToContent(this.ws, forceSingleLine);

    // Due to the first headers being merged cells they have to be handled seperately
    fitObjectiveNameHeader(this.ws, this.decisionData.objectives, objectiveColumns);
  }

  private addAlternativeColumn() {
    this.ws.getColumn('A').width = 50;

    this.data.sortedAlternaticeIndices.forEach((alternativeIndex, arrayIndex) => {
      const cell = this.ws.getCell('A' + (arrayIndex + 3));

      cell.value = multiLine(['Alternative ' + (arrayIndex + 1), this.decisionData.alternatives[alternativeIndex].name]);
      cell.value.richText[0].font = { bold: true };

      centerAndWrapText(cell);
    });
  }

  private addTotalUtilityColumn() {
    this.ws.getColumn('B').width = 15;

    const totalUtilityHeader = this.ws.getCell('B1');

    totalUtilityHeader.border = {
      top: { style: 'thin', color: { argb: '00FFFFFF' } },
      left: { style: 'thin', color: { argb: '00FFFFFF' } },
      right: { style: 'thick', color: { argb: '00FFFFFF' } },
    };

    background(totalUtilityHeader, 'edf2f7');

    const totalUtilityHeader2 = this.ws.getCell('B2');

    totalUtilityHeader2.value = $localize`Gesamtnutzen`;
    totalUtilityHeader2.font = { bold: true };
    totalUtilityHeader2.border = {
      left: { style: 'thin', color: { argb: '00FFFFFF' } },
      bottom: { style: 'thin', color: { argb: '00FFFFFF' } },
      right: { style: 'thick', color: { argb: '00FFFFFF' } },
    };

    background(totalUtilityHeader2, 'edf2f7');
    centerAndWrapText(totalUtilityHeader2);

    // Total Utility Values
    this.data.sortedAlternaticeIndices.forEach((sortedAlternativeIndex, arrayIndex) => {
      const totalUtilityValue = this.safeNumberPipe.transform(this.data.totalAlternativeUtilities[sortedAlternativeIndex], '?', '1.4-4');
      const alternativeRow = this.ws.getRow(3 + arrayIndex);

      const totalUtilityCell = alternativeRow.getCell(2);
      totalUtilityCell.value = totalUtilityValue;
      defaultValues(totalUtilityCell);
      applyNumberFormatting(totalUtilityCell);
      totalUtilityCell.border.right.style = 'thick';
    });
  }

  private addObjectives() {
    const objectiveColumns: number[] = [];
    const forceSingleLine: number[] = [];

    const objectiveHeaderRow = this.ws.getRow(1);
    objectiveHeaderRow.height = 60;
    const objectiveSubHeaderRow = this.ws.getRow(2);
    objectiveSubHeaderRow.height = this.decisionData.objectives.some(
      (objective, objectiveIndex) => objective.isIndicator && (this.data.isObjectiveExpanded[objectiveIndex] || !this.onlyExpanded),
    )
      ? 60
      : 40;

    let currentColumnOffset = 0;

    this.decisionData.objectives.forEach((objective, objectiveIndex) => {
      let columnIndex = 3 + currentColumnOffset;

      const objectiveExpanded = this.data.isObjectiveExpanded[objectiveIndex] || !this.onlyExpanded;

      let requiredColumns =
        objective.isIndicator && objectiveExpanded ? objective.indicatorData.indicators.length + 2 : objectiveExpanded ? 2 : 1;

      const objectiveInfluence =
        this.data.anyInfluenceFactor(objectiveIndex) && (this.data.isAnyIfExpanded(objectiveIndex) || !this.onlyExpanded);

      if (objectiveInfluence) {
        requiredColumns++; // Influence factor

        for (let cI = columnIndex; cI < columnIndex + requiredColumns; cI++) {
          forceSingleLine.push(cI); // Columns with influence factors can't have line breaks or the alignment breaks
        }

        for (let cI = columnIndex + 1; cI <= columnIndex + objective.indicatorData.indicators.length; cI++) {
          this.ws.getColumn(cI).width = 20; // Indicator Columns get Width 20 by default
        }
      }

      // Merge Cells for Objective Header together
      this.ws.mergeCells(1, columnIndex, 1, columnIndex + requiredColumns - 1);
      for (let i = 0; i < requiredColumns; i++) {
        this.ws.getColumn(columnIndex + i).width = 25;
      }

      objectiveColumns.push(requiredColumns);

      const objectiveHeader = objectiveHeaderRow.getCell(columnIndex);
      objectiveHeader.value = {
        richText: [
          { font: { bold: true, color: { argb: '00FFFFFF' } }, text: $localize`Ziel` + ' ' + (objectiveIndex + 1) },
          { text: ' \r\n' },
          { font: { color: { argb: '00FFFFFF' } }, text: this.decisionData.objectives[objectiveIndex].name },
          { text: ' \r\n' },
          {
            font: { color: { argb: '00FFFFFF' } },
            text:
              '(' +
              $localize`Gewicht` +
              ' ' +
              this.safeNumberPipe.transform(this.data.normalizedWeights[objectiveIndex] * 100, '?', '1.0-1') +
              '%)',
          },
        ],
      };

      objectiveHeader.fill = {
        type: 'pattern',
        pattern: 'solid',
        fgColor: { argb: '003B4C5B' },
      };

      defaultBorder(objectiveHeader);
      objectiveHeader.border.left.style = 'thick';
      centerAndWrapText(objectiveHeader);

      // Sub Header
      // Influence Factor
      if (objectiveInfluence) {
        const influenceColumn = columnIndex++;
        this.ws.getColumn(influenceColumn).width = 35;
        const influenceFactorHeader = objectiveSubHeaderRow.getCell(influenceColumn);
        defaultValues(influenceFactorHeader);
        background(influenceFactorHeader, 'd0d4d9');
        influenceFactorHeader.value = $localize`Einflussfaktor`;
        influenceFactorHeader.font = { bold: true };
        influenceFactorHeader.border.left.style = 'thick';
      }

      if (objectiveExpanded) {
        if (objective.isIndicator) {
          objective.indicatorData.indicators.forEach((indicator, index) => {
            const indicatorHeader = objectiveSubHeaderRow.getCell(columnIndex++);
            defaultValues(indicatorHeader);
            background(indicatorHeader, 'd0d4d9');

            indicatorHeader.value = {
              richText: [
                { font: { bold: true }, text: $localize`Indikator` + ' ' + (index + 1) },
                { text: ' \r\n' },
                { text: indicator.name },
                { text: ' \r\n' },
                { text: '(' + $localize`Gewicht` + ' ' + indicator.coefficient + ')' },
              ],
            };
          });
          const indicatorResultHeader = objectiveSubHeaderRow.getCell(columnIndex++);
          this.ws.getColumn(columnIndex - 1).width = 12; // Indicator Result Column Width = 12

          indicatorResultHeader.value = $localize`Ergebnis`;
          indicatorResultHeader.font = { bold: true };

          defaultValues(indicatorResultHeader);
          background(indicatorResultHeader, 'd0d4d9');
        } else {
          const utilityValueHeader = objectiveSubHeaderRow.getCell(columnIndex++);
          this.ws.getColumn(columnIndex - 1).width = 15;

          if (objective.isVerbal) {
            forceSingleLine.push(columnIndex - 1);
          }

          utilityValueHeader.value = objective.isVerbal ? $localize`Verbaler Wert` : $localize`Numerischer Wert`;
          utilityValueHeader.font = { bold: true };
          defaultValues(utilityValueHeader);
          background(utilityValueHeader, 'd0d4d9');
        }
      }

      // Utility Cell Header
      const utilityHeader = objectiveSubHeaderRow.getCell(columnIndex++);
      defaultValues(utilityHeader);
      background(utilityHeader, 'd0d4d9');
      utilityHeader.value = $localize`Nutzen`;
      utilityHeader.font = { bold: true };
      utilityHeader.border.right.style = 'thick';

      // Table Values
      this.addTableValues(3 + currentColumnOffset, objectiveIndex, objectiveExpanded, objectiveInfluence);

      currentColumnOffset += requiredColumns;
    });

    return [forceSingleLine, objectiveColumns];
  }

  private addTableValues(startColumn: number, objectiveIndex: number, objectiveExpanded: boolean, objectiveInfluence: boolean) {
    const objective = this.decisionData.objectives[objectiveIndex];
    this.data.sortedAlternaticeIndices.forEach((alternativeIndex, arrayIndex) => {
      const alternativeRow = this.ws.getRow(3 + arrayIndex);
      let columnIndex = startColumn; // Reset Column Index for Next Row

      const alternativeInfluence =
        (this.data.isAlternativeExpanded[alternativeIndex] || !this.onlyExpanded) &&
        this.decisionData.outcomes[alternativeIndex][objectiveIndex].influenceFactor != null;

      // Influence Cell
      let influenceFactorCell;
      if (objectiveInfluence) {
        influenceFactorCell = alternativeRow.getCell(columnIndex++);
        defaultValues(influenceFactorCell);

        if (alternativeInfluence) {
          const lines: string[] = [];
          lines.push(
            this.influenceFactorNamePipe.transform(this.decisionData.outcomes[alternativeIndex][objectiveIndex].influenceFactor) + ':',
          );

          for (const [index, state] of this.decisionData.outcomes[alternativeIndex][objectiveIndex].influenceFactor.getStates().entries()) {
            lines.push(
              this.stateNamePipe.transform(this.decisionData.outcomes[alternativeIndex][objectiveIndex].influenceFactor, index) +
                ' (' +
                this.safeNumberPipe.transform(state.probability, '?', '1.0-1') +
                '%)',
            );
          }

          influenceFactorCell.value = multiLine(lines);

          for (let i = 1; i < influenceFactorCell.value.richText.length; i++) {
            influenceFactorCell.value.richText[i].font = { color: { argb: '006e6e6e' } };
          }
        }
      }

      if (objectiveExpanded) {
        if (objective.isIndicator) {
          objective.indicatorData.indicators.forEach((indicator, indicatorIndex) => {
            const indicatorCell = alternativeRow.getCell(columnIndex++);
            this.ws.getColumn(columnIndex - 1).width = 20;
            defaultValues(indicatorCell);

            if (alternativeInfluence) {
              const lines: string[] = [];
              lines.push(' ');

              range(this.decisionData.outcomes[alternativeIndex][objectiveIndex].influenceFactor.stateCount).forEach(stateIndex => {
                lines.push(
                  this.safeNumberPipe.transform(
                    this.decisionData.outcomes[alternativeIndex][objectiveIndex].values[stateIndex][indicatorIndex][0],
                    '?',
                  ) +
                    ' ' +
                    indicator.unit,
                );
              });

              indicatorCell.value = multiLine(lines);
              for (let i = 1; i < indicatorCell.value.richText.length; i++) {
                indicatorCell.value.richText[i].font = { color: { argb: '006e6e6e' } };
              }
            } else {
              indicatorCell.value =
                this.safeNumberPipe.transform(this.data.minimalOutcomes[alternativeIndex][objectiveIndex][indicatorIndex], '?') +
                (indicator.unit ? ' ' + indicator.unit : '');

              if (!indicator.unit) {
                applyNumberFormatting(indicatorCell);
              }
            }
          });

          const indicatorResultCell = alternativeRow.getCell(columnIndex++);
          defaultValues(indicatorResultCell);

          if (alternativeInfluence) {
            const lines: string[] = [];
            lines.push(' ');

            range(this.decisionData.outcomes[alternativeIndex][objectiveIndex].influenceFactor.stateCount).forEach(stateIndex => {
              lines.push(
                this.safeNumberPipe.transform(
                  this.data.indicatorOutcomeResults[alternativeIndex][objectiveIndex][stateIndex],
                  '?',
                  '1.0-1',
                ) + objective.indicatorData.aggregatedUnit,
              );
            });

            indicatorResultCell.value = multiLine(lines);
            for (let i = 1; i < indicatorResultCell.value.richText.length; i++) {
              indicatorResultCell.value.richText[i].font = { color: { argb: '006e6e6e' } };
            }
          } else {
            indicatorResultCell.value =
              this.safeNumberPipe.transform(this.data.indicatorMinimalResults[alternativeIndex][objectiveIndex], '?', '1.0-3') +
              objective.indicatorData.aggregatedUnit;
          }
        } else {
          const utilityValueCell = alternativeRow.getCell(columnIndex++);
          defaultValues(utilityValueCell);

          if (objective.isNumerical) {
            if (alternativeInfluence) {
              const lines: string[] = [];
              lines.push(' ');

              range(this.decisionData.outcomes[alternativeIndex][objectiveIndex].influenceFactor.stateCount).forEach(index => {
                lines.push(
                  this.safeNumberPipe.transform(this.decisionData.outcomes[alternativeIndex][objectiveIndex].values[index][0][0], '?') +
                    ' ' +
                    objective.numericalData.unit,
                );
              });

              utilityValueCell.value = multiLine(lines);

              for (let i = 1; i < utilityValueCell.value.richText.length; i++) {
                utilityValueCell.value.richText[i].font = { color: { argb: '006e6e6e' } };
              }
            } else {
              utilityValueCell.value = this.safeNumberPipe.transform(this.data.minimalOutcomes[alternativeIndex][objectiveIndex][0], '?');
              applyNumberFormatting(utilityValueCell);
            }
          } else if (objective.isVerbal) {
            if (alternativeInfluence) {
              const lines: string[] = [];
              lines.push(' ');
              range(this.decisionData.outcomes[alternativeIndex][objectiveIndex].influenceFactor.stateCount).forEach(stateIndex => {
                lines.push(
                  objective.verbalData.options[this.decisionData.outcomes[alternativeIndex][objectiveIndex].values[stateIndex][0][0] - 1],
                );
              });
              utilityValueCell.value = multiLine(lines);

              for (let i = 1; i < utilityValueCell.value.richText.length; i++) {
                utilityValueCell.value.richText[i].font = { color: { argb: '006e6e6e' } };
              }
            } else {
              utilityValueCell.value = objective.verbalData.options[this.data.minimalOutcomes[alternativeIndex][objectiveIndex][0] - 1];
            }
          }
        }
      }

      const utilityCell = alternativeRow.getCell(columnIndex++);

      this.ws.getColumn(columnIndex - 1).width = 12; // Utility Column Width = 12

      defaultValues(utilityCell);
      utilityCell.border.right.style = 'thick';

      if (alternativeInfluence) {
        const lines: string[] = [];
        lines.push(this.safeNumberPipe.transform(this.data.totalOutcomeUtilities[alternativeIndex][objectiveIndex], '?', '1.4-4'));

        range(this.decisionData.outcomes[alternativeIndex][objectiveIndex].influenceFactor.stateCount).forEach(index => {
          lines.push(this.safeNumberPipe.transform(this.data.ifOutcomeUtilities[alternativeIndex][objectiveIndex][index], '?', '1.4-4'));
        });
        utilityCell.value = multiLine(lines);

        for (let i = 1; i < utilityCell.value.richText.length; i++) {
          utilityCell.value.richText[i].font = { color: { argb: '006e6e6e' } };
        }
      } else {
        utilityCell.value = this.safeNumberPipe.transform(this.data.totalOutcomeUtilities[alternativeIndex][objectiveIndex], '?', '1.4-4');
        applyNumberFormatting(utilityCell);
      }
    });
  }
}
