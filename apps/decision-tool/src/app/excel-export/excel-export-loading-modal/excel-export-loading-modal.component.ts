import { Component } from '@angular/core';

@Component({
  templateUrl: './excel-export-loading-modal.component.html',
  styleUrls: ['./excel-export-loading-modal.component.scss'],
})
export class ExcelExportLoadingModalComponent {}
