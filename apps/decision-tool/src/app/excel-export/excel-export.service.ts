import { Injectable } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { InfluenceFactorNamePipe, SafeNumberPipe, StateNamePipe } from '@entscheidungsnavi/widgets';
import sanitize from 'sanitize-filename';
import { DecisionData } from '@entscheidungsnavi/decision-data';
import { truncate } from 'lodash';
import { ProjectViewData } from '../project-view';
import { ProjectService } from '../data/project';
import { ExcelExport } from './excel-export';
import { ExcelExportLoadingModalComponent } from './excel-export-loading-modal/excel-export-loading-modal.component';

@Injectable({
  providedIn: 'root',
})
export class ExcelExportService {
  constructor(
    private decisionData: DecisionData,
    private projectService: ProjectService,
    private safeNumberPipe: SafeNumberPipe,
    private influenceFactorNamePipe: InfluenceFactorNamePipe,
    private stateNamePipe: StateNamePipe,
    private matDialog: MatDialog,
  ) {}

  async performExcelExport(onlyExpanded: boolean, data?: ProjectViewData) {
    const dialogRef = this.matDialog.open(ExcelExportLoadingModalComponent, { disableClose: true });

    data = data ?? new ProjectViewData(this.decisionData);

    const exceljs = (await import('exceljs')).default;
    const wb = new exceljs.Workbook();
    const ws = wb.addWorksheet(this.sanitizeWorksheetName(this.projectService.getProjectName()));
    new ExcelExport(
      this.decisionData,
      data,
      this.safeNumberPipe,
      this.influenceFactorNamePipe,
      this.stateNamePipe,
      onlyExpanded,
      ws,
    ).performExport();

    const excelData = await wb.xlsx.writeBuffer();
    const blob = new Blob([excelData], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' });

    const element = document.createElement('a');
    element.setAttribute('href', URL.createObjectURL(blob));
    element.setAttribute('download', sanitize(this.projectService.getProjectName()) + '.xlsx');
    element.style.display = 'none';
    document.body.appendChild(element);
    element.click();
    document.body.removeChild(element);

    dialogRef.close();
  }

  private sanitizeWorksheetName(name: string) {
    const forbiddenRegex = /[:\\/?*[\]]/g;
    return truncate(name.replace(forbiddenRegex, '_'), { length: 31 });
  }
}
