// -------------- LEGACY: paths to support old routes
import { Routes } from '@angular/router';

export const LEGACY_ROUTES: Routes = [
  {
    path: 'entscheidungsfrage',
    redirectTo: 'decisionstatement',
  },
  {
    path: 'zielformulierung',
    redirectTo: 'objectives',
  },
  {
    path: 'einflussfaktoren',
    redirectTo: 'unsicherheitsfaktoren',
  },
  {
    path: 'alternativen',
    redirectTo: 'alternatives',
  },
  {
    path: 'unsicherheitsfaktoren',
    children: [
      {
        path: ':id',
        redirectTo: '/impactmodel/uncertaintyfactors/:id',
      },
      {
        path: '**',
        redirectTo: '/impactmodel/uncertaintyfactors',
      },
    ],
  },
  {
    path: 'unsicherheitsfaktor/:id',
    redirectTo: 'impactmodel/uncertaintyfactors/:id',
  },
  {
    path: 'prognosen',
    children: [
      {
        path: '**',
        redirectTo: '/impactmodel',
      },
    ],
  },
  {
    path: 'bewertung',
    children: [
      {
        path: 'nutzenfunktion',
        redirectTo: '/results/steps/1',
      },
      {
        path: 'zielgewichtung',
        redirectTo: '/results/steps/2',
      },
    ],
  },
  {
    path: 'ergebnis',
    redirectTo: 'results',
  },
  {
    path: 'unguided',
    redirectTo: 'professional',
  },
];
