import { Injectable } from '@angular/core';
import { DebugService } from '../debug/debug.service';
import { ProjectService } from './project';

@Injectable()
export class GlobalErrorService {
  public allErrors: any[] = [];

  private _errorElement: HTMLElement;

  exportButton: HTMLButtonElement;

  get errorElement() {
    if (this._errorElement) {
      return this._errorElement;
    } else {
      this._errorElement = document.getElementById('dt-navi-error');

      this.exportButton = this._errorElement.querySelector('#dt-navi-error-export-button');
      const continueButton: HTMLElement = this._errorElement.querySelector('#dt-navi-error-continue-button');

      this.exportButton.addEventListener('click', () => {
        try {
          this.projectService.getProject()?.exportFile();
        } catch (e: any) {}
      });

      continueButton.addEventListener('click', () => {
        this.errorElement.style.display = 'none';
      });

      return this._errorElement;
    }
  }

  constructor(
    private projectService: ProjectService,
    private debugService: DebugService,
  ) {}

  handleError(error: any): void {
    window.dt.lastError = error;
    // Hier ggf. Fehler an den Server senden sobald implementiert / Datenschutzprobleme gelöst.
    this.showErrorWarning();
  }

  private showErrorWarning() {
    if (this.debugService.isDebugEnabled) {
      document.querySelector('#dt-debug-error-indicator')?.classList.add('error');
    } else if (this.errorElement) {
      this.errorElement.style.display = 'flex';

      this.exportButton.disabled = !this.projectService.isProjectLoaded();
    }
  }
}
