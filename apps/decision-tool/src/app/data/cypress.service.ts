import { Injectable } from '@angular/core';
import { DecisionData } from '@entscheidungsnavi/decision-data';
import { isCypressRun } from '@entscheidungsnavi/widgets';
import { ProjectService } from './project';

@Injectable({
  providedIn: 'root',
})
export class CypressService {
  constructor(decisionData: DecisionData, projectService: ProjectService) {
    if (isCypressRun()) {
      (window as any)['DecisionData'] = decisionData;
      projectService.project$.subscribe(() => {
        (window as any)['ProjectType'] = projectService.getProjectType();
      });
    }
  }
}
