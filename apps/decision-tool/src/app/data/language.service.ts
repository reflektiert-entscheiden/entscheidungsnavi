import { Inject, Injectable, LOCALE_ID } from '@angular/core';
import { NAVI_STEP_ORDER, NaviStep, NaviStepNames, STEP_NAMES } from '@entscheidungsnavi/decision-data';
import { EDUCATIONAL_STEPS_META_DATA, NavigationStepMetaData } from '../../modules/shared/navigation/educational-navigation';

@Injectable({
  providedIn: 'root',
})
export class LanguageService {
  readonly isEnglish: boolean;
  readonly steps: { [key in NaviStep]: NavigationStepMetaData & NaviStepNames };

  constructor(@Inject(LOCALE_ID) locale: string) {
    this.isEnglish = !locale.startsWith('de');

    this.steps = {} as any;
    for (const step of NAVI_STEP_ORDER) {
      this.steps[step] = { ...STEP_NAMES[this.isEnglish ? 'en' : 'de'][step], ...EDUCATIONAL_STEPS_META_DATA[step] };
    }
  }
}
