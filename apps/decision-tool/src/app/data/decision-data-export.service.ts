import { Injectable } from '@angular/core';
import { copyProperties, dataToText, readTextWithMetadata } from '@entscheidungsnavi/decision-data/export';
import { Subject } from 'rxjs';
import { DecisionData } from '@entscheidungsnavi/decision-data';
import { MatDialog } from '@angular/material/dialog';
import { textSize } from '@entscheidungsnavi/tools';
import { ENVIRONMENT } from '../../environments/environment';
import { NightlyImportModalComponent } from '../nightly/nightly-import-modal/nightly-import-modal.component';

@Injectable({
  providedIn: 'root',
})
export class DecisionDataExportService {
  private onProjectImportSubject = new Subject<void>();
  onProjectImport$ = this.onProjectImportSubject.asObservable();

  private get appVersion() {
    return ENVIRONMENT.version + (ENVIRONMENT.nightly ? '-dev' : '');
  }

  constructor(
    private decisionData: DecisionData,
    private dialog: MatDialog,
  ) {}

  resetDecisionData() {
    this.decisionData.reset(ENVIRONMENT.version);
  }

  importText(text: string): { projectName?: string } {
    try {
      // load the data in a temporary DecisionData object
      const { data, exportVersion, projectName } = readTextWithMetadata(text);
      this.importDecisionData(data, exportVersion);
      return { projectName };
    } catch (e) {
      console.log(e);
      throw new Error('Der übergebene Text ist kein gültiges JSON.');
    }
  }

  private importDecisionData(data: DecisionData, exportVersion?: string) {
    if (exportVersion != null && exportVersion.endsWith('-dev') && !ENVIRONMENT.nightly) {
      this.dialog.open(NightlyImportModalComponent);
    }
    this.resetDecisionData();
    copyProperties(data, this.decisionData);
    this.onProjectImportSubject.next();
  }

  /**
   * Converts the currently loaded decision data object into text.
   * @returns The exported text
   */
  dataToText(projectName: string | null, decisionData?: DecisionData): string {
    return dataToText(decisionData ?? this.decisionData, this.appVersion, projectName);
  }

  /**
   * Converts the change detection relevant properties of the currently loaded decision data object into text.
   * @returns The exported text
   */
  changeDetectionRelevantDataToText() {
    return JSON.stringify(this.decisionData.getPropertiesForChangeDetection());
  }

  getProjectSize() {
    const data = this.dataToText(null);

    const size = textSize(data);
    const megaBytes = size / 1024 / 1024; // Technically Mebibytes but who cares

    return megaBytes;
  }
}
