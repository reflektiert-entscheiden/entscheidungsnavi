import { defer, of, Subject } from 'rxjs';
import { TestBed } from '@angular/core/testing';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { HttpErrorResponse } from '@angular/common/http';
import { TimeTrackingService } from '../time-tracking.service';
import { ProjectConflictModalComponent } from '../../project-conflict-modal/project-conflict-modal.component';
import { AutoSaveService } from './auto-save.service';
import { ProjectService } from './project.service';
import { LocalProject } from './local-project';
import { OnlineProject } from './online-project';

jest.useFakeTimers();

describe('AutoSaveService', () => {
  let autoSaveService: AutoSaveService;
  let decisionDataChanged$: Subject<void>;
  let project$: Subject<LocalProject | OnlineProject>;
  let loadOnlineProject: jest.Mock;
  let dialogOpen: jest.Mock;

  beforeEach(() => {
    decisionDataChanged$ = new Subject();
    project$ = new Subject();
    loadOnlineProject = jest.fn();
    dialogOpen = jest.fn();

    TestBed.configureTestingModule({
      providers: [
        AutoSaveService,
        { provide: TimeTrackingService, useValue: { decisionDataChanged: jest.fn().mockReturnValue(decisionDataChanged$) } },
        { provide: ProjectService, useValue: { project$, loadOnlineProject } },
        { provide: MatDialog, useValue: { open: dialogOpen } },
        { provide: MatSnackBar, useValue: {} },
      ],
    });

    autoSaveService = TestBed.inject(AutoSaveService);
  });

  afterEach(() => {
    decisionDataChanged$.complete();
    project$.complete();
  });

  it('is disabled for non-online projects', () => {
    project$.next(null);
    expect(autoSaveService.saveState$.value).toBe('off');
  });

  describe('with online projects', () => {
    const projectId = 'mock project id';
    let isProjectSaved: jest.Mock;
    let shouldAutoSave$: Subject<boolean>;
    let save$: Subject<void>;
    let saveFactory: jest.Mock;

    beforeEach(() => {
      isProjectSaved = jest.fn();
      shouldAutoSave$ = new Subject<boolean>();
      save$ = new Subject<void>();
      // We use defer and mockImplementation to ensure we can later swap out the save$ subject
      saveFactory = jest.fn().mockImplementation(() => save$);
      const save = jest.fn().mockReturnValue(defer(saveFactory));

      const project = Object.create(OnlineProject.prototype);
      Object.assign(project, { _info: { id: projectId }, isProjectSaved, save, shouldAutoSave$, exportFile: jest.fn() });

      project$.next(project);
    });

    afterEach(() => {
      shouldAutoSave$.complete();
      save$.complete();
    });

    it('handles multiple consecutive changes', () => {
      isProjectSaved.mockReturnValue(false);
      shouldAutoSave$.next(true);

      expect(autoSaveService.saveState$.value).toBe('idle');

      decisionDataChanged$.next();
      expect(autoSaveService.saveState$.value).toBe('saving');
      jest.runAllTimers();
      expect(saveFactory).toHaveBeenCalledTimes(1);

      decisionDataChanged$.next();
      jest.runAllTimers();
      expect(saveFactory).toHaveBeenCalledTimes(1);

      // After our saving finished, the project is still not saved (isProjectSaved() returns false).
      // There might have been changes during saving, for example.
      save$.next();
      expect(autoSaveService.saveState$.value).toBe('saving');
      jest.runAllTimers();
      expect(saveFactory).toHaveBeenCalledTimes(2);

      isProjectSaved.mockReturnValue(true);
      save$.next();

      expect(autoSaveService.saveState$.value).toBe('idle');
    });

    it('handles multiple consecutive firings of shouldAutoSave$', () => {
      isProjectSaved.mockReturnValue(false);

      shouldAutoSave$.next(true);
      decisionDataChanged$.next();

      expect(autoSaveService.saveState$.value).toBe('saving');
      jest.runAllTimers();
      expect(saveFactory).toHaveBeenCalledTimes(1);

      shouldAutoSave$.next(true);
      decisionDataChanged$.next();

      jest.runAllTimers();
      expect(saveFactory).toHaveBeenCalledTimes(1);

      isProjectSaved.mockReturnValue(true);
      save$.next();

      expect(autoSaveService.saveState$.value).toBe('idle');
    });

    it('handles errors while saving', () => {
      isProjectSaved.mockReturnValue(false);

      shouldAutoSave$.next(true);
      decisionDataChanged$.next();
      jest.runAllTimers();

      expect(autoSaveService.saveState$.value).toBe('saving');
      expect(saveFactory).toHaveBeenCalledTimes(1);

      save$.error('error');
      expect(autoSaveService.saveState$.value).toBe('error');

      save$.complete();
      save$ = new Subject<void>();
      jest.runAllTimers();

      expect(saveFactory).toHaveBeenCalledTimes(2);
      save$.next();

      expect(autoSaveService.saveState$.value).toBe('idle');
    });

    it('handles version conflicts', () => {
      // Initiate the first save
      isProjectSaved.mockReturnValue(false);
      shouldAutoSave$.next(true);
      decisionDataChanged$.next();
      jest.runAllTimers();
      expect(saveFactory).toHaveBeenCalledTimes(1);

      // Mock the conflict modal
      const afterClosed$ = new Subject<string>();
      dialogOpen.mockReturnValue({ afterClosed: jest.fn().mockReturnValue(afterClosed$.asObservable()), close: jest.fn() });

      // Mock the conflict save error
      save$.error(new HttpErrorResponse({ status: 409, error: {} }));

      // First scenario: the user simply cancels the modal
      expect(dialogOpen).toHaveBeenCalledWith(ProjectConflictModalComponent, expect.anything());
      afterClosed$.next('cancel');
      expect(autoSaveService.saveState$.value).toBe('error');
      jest.runAllTimers();
      expect(saveFactory).toHaveBeenCalledTimes(2);

      // Second scenario: the user chooses to keep the local project
      expect(dialogOpen).toHaveBeenCalledTimes(2);
      afterClosed$.next('take-local');
      expect(saveFactory).toHaveBeenCalledTimes(3);

      // Third scenario: the user chooses to keep the online project
      loadOnlineProject.mockReturnValue(of(true));
      jest.runAllTimers();
      expect(dialogOpen).toHaveBeenCalledTimes(3);
      isProjectSaved.mockReturnValue(true);
      afterClosed$.next('take-online');
      expect(loadOnlineProject).toHaveBeenCalledWith(projectId, { forceUnload: true });
      expect(autoSaveService.saveState$.value).toBe('idle');
    });
  });
});
