import { Injectable, NgZone } from '@angular/core';
import {
  audit,
  BehaviorSubject,
  catchError,
  debounceTime,
  distinctUntilChanged,
  EMPTY,
  filter,
  finalize,
  fromEvent,
  interval,
  map,
  merge,
  of,
  retry,
  share,
  startWith,
  Subject,
  switchMap,
  tap,
  throwError,
  timer,
} from 'rxjs';
import { HttpErrorResponse } from '@angular/common/http';
import { MatDialog } from '@angular/material/dialog';
import { TimeTrackingService } from '../time-tracking.service';
import {
  ProjectConflictData,
  ProjectConflictModalComponent,
  ProjectConflictResult,
} from '../../project-conflict-modal/project-conflict-modal.component';
import { OnlineProject } from './online-project';
import { AbstractProject } from './abstract-project';
import { ProjectService } from './project.service';
import { TeamProject } from './team-project';

export type SaveState = 'off' | 'idle' | 'saving' | 'error';

@Injectable({ providedIn: 'root' })
export class AutoSaveService {
  readonly saveState$ = new BehaviorSubject<SaveState>('off');

  private _saveError: any;
  get saveError() {
    return this._saveError;
  }

  private triggerSave$ = new Subject<void>();

  constructor(
    private projectService: ProjectService,
    timeTrackingService: TimeTrackingService,
    zone: NgZone,
    private dialog: MatDialog,
  ) {
    zone.runOutsideAngular(() => {
      this.projectService.project$
        .pipe(
          this.filterProject(),
          distinctUntilChanged(),
          switchMap(project => {
            if (project == null) {
              this.updateSaveState('off');
              return EMPTY;
            } else {
              this.updateSaveState('idle');
            }

            // We initiate a save when...
            const initiateSave$ = merge(
              // ...we minimize the window
              fromEvent(window, 'blur'),
              // ...the window is about to be unloaded
              fromEvent(window, 'beforeunload'),
              // ...we were manually triggered
              this.triggerSave$,
              // ...we detect a change in decision data
              timeTrackingService.decisionDataChanged(),
            );

            return initiateSave$.pipe(
              // Suppress change events until we are no longer saving
              audit(() => this.saveState$.pipe(filter(state => state === 'idle'))),
              // Suppress events when the project is actually not modified
              filter(() => !project.isProjectSaved()),
              // Here, the actual update process starts
              tap(() => this.updateSaveState('saving')),
              debounceTime(3_000),
              // Save and retry
              switchMap(() =>
                zone
                  .run(() =>
                    project instanceof OnlineProject ? project.save('patch').pipe(this.handleVersionConflict(project)) : project.save(),
                  )
                  .pipe(
                    retry({
                      delay: error => {
                        this._saveError = error;
                        this.updateSaveState('error');
                        return timer(10_000);
                      },
                    }),
                  ),
              ),
              tap(() => this.updateSaveState('idle')),
            );
          }),
        )
        .subscribe();
    });
  }

  private updateSaveState(newState: SaveState) {
    this.saveState$.next(newState);
  }

  private filterProject() {
    // Only let projects pass that can and should be auto saved. Returns null if the project should not be auto-saved.
    return switchMap((project: AbstractProject | null) => {
      if (project instanceof OnlineProject) {
        return project.shouldAutoSave$.pipe(map(shouldAutoSave => (shouldAutoSave ? project : null)));
      } else if (project instanceof TeamProject) {
        return interval(5000).pipe(
          startWith(0),
          map(_ => project.canSave),
          distinctUntilChanged(),
          map(canSave => (canSave ? project : null)),
        );
      } else {
        return of(null);
      }
    });
  }

  private handleVersionConflict(project: OnlineProject) {
    return catchError(error => {
      // This handler only handles version conflicts (HTTP 409). For other errors, we simply rethrow.
      if (!(error instanceof HttpErrorResponse && error.status === 409)) {
        return throwError(() => error);
      }

      const dialog = this.dialog.open<ProjectConflictModalComponent, ProjectConflictData, ProjectConflictResult>(
        ProjectConflictModalComponent,
        {
          data: {
            localLoadTime: project.info.updatedAt,
            onlineUpdateTime: new Date(error.error.message),
          },
        },
      );

      return dialog.afterClosed().pipe(
        // Make sure the dialog is closed when the observable is cancelled
        finalize(() => dialog.close()),
        switchMap(result => {
          if (result === 'take-local') {
            return project.save('resolve-conflict');
          } else if (result === 'take-online') {
            project.exportFile();

            const load$ = this.projectService.loadOnlineProject(project.info.id, { forceUnload: true }).pipe(share());

            // Make sure we separately subscribe to this observable because:
            // - the project is replaced during this
            // - this will unsubscribe from the running auto-save process observable and thus from this
            load$.pipe(catchError(() => EMPTY)).subscribe();

            return load$;
          } else {
            return throwError(() => error);
          }
        }),
      );
    });
  }

  triggerSave() {
    this.triggerSave$.next();
  }
}
