import { BehaviorSubject, catchError, map, mergeMap, Observable, of, shareReplay, Subject, switchMap, throwError } from 'rxjs';
import { ProjectSaveType, ProjectUpdate, ProjectWithData } from '@entscheidungsnavi/api-types';
import { createPatch, hashCode } from '@entscheidungsnavi/tools';
import { inject } from '@angular/core';
import { OnlineProjectsService } from '@entscheidungsnavi/api-client';
import { AbstractProject } from './abstract-project';
import { checkOnlineProjectSize, saveTimeout } from './save-notification';

export interface OnlineProjectInfo extends Pick<ProjectWithData, 'id' | 'data' | 'name' | 'userId' | 'createdAt' | 'updatedAt'> {
  // Is non-null if we are currently in a loaded history entry of this project
  loadedHistoryEntry?: string;
}

/**
 * A personal project in an online account. It may or may not be a team project (of which we are the owner).
 */
export class OnlineProject extends AbstractProject {
  private onlineProjectsService = inject(OnlineProjectsService);

  get info(): Readonly<OnlineProjectInfo> {
    return this._info;
  }

  get name() {
    return this._info.name;
  }

  // False if the loaded project is an old version from the history of the online project
  readonly isLatestVersion$: BehaviorSubject<boolean>;

  readonly onSaved$ = new Subject<void>();

  readonly shouldAutoSave$: Observable<boolean>;

  private otherProjects$ = this.onlineProjectsService.getProjectList().pipe(shareReplay(1, 60_000));

  constructor(private readonly _info: OnlineProjectInfo) {
    super();

    this.updateLastExportHash();

    this.isLatestVersion$ = new BehaviorSubject(this.info.loadedHistoryEntry == null);
    this.shouldAutoSave$ = this.isLatestVersion$;
  }

  save(mode: 'patch' | ProjectSaveType): Observable<void> {
    // Whether we save a personal project in our own account or a team main project

    this.trackingService.trackEvent('save project', { category: 'project' });

    const data = this.exportService.dataToText(null);
    const dataWithoutEphemeral = this.exportService.changeDetectionRelevantDataToText();

    return of(data).pipe(
      checkOnlineProjectSize(),
      mergeMap(data => {
        const update: ProjectUpdate =
          mode === 'patch'
            ? {
                dataPatch: createPatch(this._info.data, data),
                oldDataHash: hashCode(this._info.data),
              }
            : { data, dataSaveType: mode };

        return this.onlineProjectsService.updateProject(this.info.id, update).pipe(saveTimeout());
      }),

      catchError(error => {
        return throwError(() => error);
      }),
      map(newProject => {
        this.updateLastExportHash(dataWithoutEphemeral);
        this._info.data = data;
        this._info.updatedAt = newProject.updatedAt;
        if (this._info.loadedHistoryEntry != null) {
          this._info.loadedHistoryEntry = null;
          this.isLatestVersion$.next(true);
        }
        this.onSaved$.next();

        return null;
      }),
    );
  }

  rename(newName: string): Observable<void> {
    // We need to make sure we are not setting a name that is already in use
    return this.otherProjects$.pipe(
      switchMap(otherProjects => {
        if (otherProjects.some(project => project.name.toLowerCase() === newName.toLowerCase())) {
          return throwError(() => new Error('name conflict'));
        } else {
          return this.onlineProjectsService.updateProject(this.info.id, { name: newName }).pipe(
            map(updatedProject => {
              this._info.name = updatedProject.name;
              this._info.updatedAt = updatedProject.updatedAt;

              return null;
            }),
          );
        }
      }),
    );
  }
}
