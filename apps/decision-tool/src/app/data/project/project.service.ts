import { EnvironmentInjector, Injectable, runInInjectionContext } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';
import { BehaviorSubject, combineLatest, filter, from, fromEvent, Observable, of, OperatorFunction } from 'rxjs';
import { catchError, exhaustMap, map, pairwise, switchMap, tap } from 'rxjs/operators';
import { DecisionData, ProjectMode } from '@entscheidungsnavi/decision-data';
import { MatDialog } from '@angular/material/dialog';
import {
  AuthService,
  EventManagementService,
  KlugService,
  OnlineProjectsService,
  QuickstartService,
  TeamsService,
} from '@entscheidungsnavi/api-client';
import { ConfirmModalComponent, ConfirmModalData, FileExport, SnackbarComponent, SnackbarData } from '@entscheidungsnavi/widgets';
import { EmbeddableProject } from '@entscheidungsnavi/embedded-decision-tool';
import { ClassConstructor } from 'class-transformer';
import { MatSnackBar } from '@angular/material/snack-bar';
import { resetSessionSettings } from '@entscheidungsnavi/widgets';
import { ProjectInformationComponent } from '../../project-information/project-information.component';
import { TrackingService } from '../tracking.service';
import { DecisionDataExportService } from '../decision-data-export.service';
import { UnloadProjectModalComponent, UnloadProjectMode } from '../../unload-project-modal/unload-project-modal.component';
import { projectLoadingModal } from '../../project-loading/project-loading-modal.component';
import { getStartUrlForProjectMode } from '../../../modules/shared/navigation/navigation';
import { AbstractProject } from './abstract-project';
import { LocalProject } from './local-project';
import { OnlineProject } from './online-project';
import { TeamProject } from './team-project';

@Injectable({ providedIn: 'root' })
export class ProjectService {
  readonly project$ = new BehaviorSubject<LocalProject | OnlineProject | TeamProject | null>(null);

  constructor(
    private decisionData: DecisionData,
    private onlineProjectsService: OnlineProjectsService,
    private quickstartService: QuickstartService,
    private exportService: DecisionDataExportService,
    private router: Router,
    private trackingService: TrackingService,
    private dialog: MatDialog,
    private snackBar: MatSnackBar,
    private klugService: KlugService,
    private teamAPIService: TeamsService,
    private injector: EnvironmentInjector,
    private authService: AuthService,
    private eventManagementService: EventManagementService,
  ) {
    this.authService.onLogout$.subscribe(() => {
      const onlineOrTeamProject = this.getProject();
      if (!(onlineOrTeamProject instanceof TeamProject || onlineOrTeamProject instanceof OnlineProject)) return;

      if (onlineOrTeamProject.isProjectSaved()) {
        this.closeProject().subscribe();

        this.dialog.open<ConfirmModalComponent, ConfirmModalData>(ConfirmModalComponent, {
          data: {
            buttonConfirm: $localize`Verstanden`,
            buttonConfirmColor: 'accent',
            buttonDeny: '',
            title: $localize`Login abgelaufen`,
            prompt: $localize`Dein Login ist abgelaufen. Dein Projekt wurde deshalb geschlossen, ist aber weiterhin
              in deinem Account gespeichert. Melde Dich wieder an, um es erneut zu öffnen.`,
          },
        });
      } else {
        this.project$.next(runInInjectionContext(this.injector, () => new LocalProject('local', onlineOrTeamProject.name)));

        this.dialog.open<ConfirmModalComponent, ConfirmModalData>(ConfirmModalComponent, {
          data: {
            buttonConfirm: $localize`Verstanden`,
            buttonConfirmColor: 'accent',
            buttonDeny: '',
            title: $localize`Login abgelaufen`,
            prompt: $localize`Dein Login ist abgelaufen. Dein Projekt wurde deshalb in ein offline Projekt konvertiert.
              Es wird <b>nicht mehr automatisch gespeichert</b>.<br /><br />
              Um weiter automatisch online zu speichern, melde dich wieder an und speichere das Projekt.`,
          },
        });
      }
    });

    router.events.subscribe(event => {
      if (event instanceof NavigationEnd) {
        if (event.url !== '/loading') this.decisionData.lastUrl = event.url;
      }
    });

    let lastBlurTimestamp: Date;
    fromEvent(window, 'blur').subscribe(() => (lastBlurTimestamp = new Date()));
    fromEvent(window, 'focus')
      .pipe(
        map(() => this.getProject(OnlineProject)),
        filter(Boolean),
        filter(
          project => project.isProjectSaved() && lastBlurTimestamp && new Date().getTime() - lastBlurTimestamp.getTime() > 10 * 60_000,
        ),
        exhaustMap(project => this.loadOnlineProject(project.info.id, { forceUnload: true }).pipe(catchError(() => this.closeProject()))),
      )
      .subscribe({
        next: () => (lastBlurTimestamp = null),
      });

    this.project$.pipe(pairwise()).subscribe(([oldProject, newProject]) => {
      if (oldProject) {
        oldProject.onClose(newProject);
        resetSessionSettings();
      }
    });
  }

  /**
   * Returns the currently loaded project of the specified type, if one is loaded, or null.
   * If no type is specified, it returns any type of project.
   *
   * @param type - The type of project to be returned
   * @returns The project
   */
  getProject(): AbstractProject;
  getProject<T extends AbstractProject>(type: ClassConstructor<T>): T;
  getProject<T extends AbstractProject>(type?: ClassConstructor<AbstractProject>): T | AbstractProject {
    const project = this.project$.value;

    if (type && !(project instanceof type)) {
      return null;
    }

    return project;
  }

  isProjectLoaded() {
    return !!this.getProject();
  }

  getProjectType(): typeof LocalProject.prototype.type | 'online' | 'team' {
    const project = this.getProject();
    if (project instanceof LocalProject) {
      return project.type;
    } else if (project instanceof OnlineProject) {
      return 'online';
    } else if (project instanceof TeamProject) {
      return 'team';
    } else {
      return null;
    }
  }

  getProjectName() {
    return this.getProject()?.name;
  }

  // create a new project
  newProject(name: string, mode: ProjectMode) {
    return of(null).pipe(
      switchMap(() => {
        this.trackingService.trackEvent('new project', { category: 'project' });

        this.exportService.resetDecisionData();
        this.decisionData.projectMode = mode;
        this.project$.next(runInInjectionContext(this.injector, () => new LocalProject('local', name)));

        return from(this.router.navigateByUrl(getStartUrlForProjectMode(mode)));
      }),
      this.loadProject(),
    );
  }

  loadEventSubmission(eventId: string, registrationId: string) {
    return this.eventManagementService.getOneEventRegistration(eventId, registrationId).pipe(
      tap(eventRegistration => {
        this.trackingService.trackEvent('load event submission', { category: 'project' });

        const { projectName } = this.exportService.importText(eventRegistration.projectData);
        this.project$.next(runInInjectionContext(this.injector, () => new LocalProject('local', projectName)));
      }),
      this.loadProject(),
    );
  }

  loadQuickstartProject(handle: string, options: Partial<{ showErrorMessage: boolean }> = {}) {
    return this.quickstartService.getProject(handle).pipe(
      tap(project => {
        this.trackingService.trackEvent('load quickstart project', { name: handle, category: 'project' });

        this.exportService.importText(project.data);
        this.project$.next(runInInjectionContext(this.injector, () => new LocalProject('quickstart', project.name)));

        this.dialog.open(ProjectInformationComponent);
      }),
      tap({
        error: () => {
          if (options.showErrorMessage !== false) {
            this.snackBar.openFromComponent<SnackbarComponent, SnackbarData>(SnackbarComponent, {
              data: {
                message: $localize`Themenprojekt konnte nicht geladen werden. Möglicherweise ist Deine Internetverbindung instabil.`,
                icon: 'error',
                dismissButton: true,
              },
            });
          }
        },
      }),
      this.loadProject(),
    );
  }

  loadOnlineProject(
    id: string,
    options: Partial<{
      historyEntryId: string;
      loadAsLocal: boolean;
      overrideLastUrl: string;
      forceUnload: boolean;
      showErrorMessage: boolean;
    }> = {},
  ) {
    return combineLatest([
      this.onlineProjectsService.getProject(id),
      options.historyEntryId ? this.onlineProjectsService.getDataFromHistory(id, options.historyEntryId) : of(null),
    ]).pipe(
      tap(([project, historyData]) => {
        const event = ['load online project'];
        if (options.historyEntryId) event.push('history entry');
        if (options.loadAsLocal) event.push('as local');
        this.trackingService.trackEvent(event.join(' '), { category: 'project' });

        this.exportService.importText(historyData ?? project.data);

        const newProject = runInInjectionContext(this.injector, () =>
          options.loadAsLocal
            ? new LocalProject('local', project.name)
            : new OnlineProject({ ...project, loadedHistoryEntry: options.historyEntryId }),
        );
        this.project$.next(newProject);
      }),
      tap({
        error: () => {
          if (options.showErrorMessage !== false) {
            this.snackBar.openFromComponent<SnackbarComponent, SnackbarData>(SnackbarComponent, {
              data: {
                message: $localize`Account-Projekt konnte nicht geladen werden. Möglicherweise ist Deine Internetverbindung instabil.`,
                icon: 'error',
                dismissButton: true,
              },
            });
          }
        },
      }),
      this.loadProject(options),
    );
  }

  forceReloadProject() {
    const loadedProject = this.getProject();

    if (loadedProject == null) {
      return of(true);
    }

    if (loadedProject instanceof LocalProject) {
      return of(true);
    } else if (loadedProject instanceof OnlineProject) {
      return this.loadOnlineProject(loadedProject.info.id, { forceUnload: true });
    } else if (loadedProject instanceof TeamProject) {
      return this.loadTeamProject(loadedProject.team.id, { forceUnload: true });
    }

    throw new Error('Trying to reload an unknown project type');
  }

  loadLinkedProject(token: string) {
    return this.onlineProjectsService.getProjectFromShareToken(token).pipe(
      tap(project => {
        this.trackingService.trackEvent('load linked project', { category: 'project' });

        this.exportService.importText(project.data);
        this.project$.next(runInInjectionContext(this.injector, () => new LocalProject('linked', project.name)));
      }),
      this.loadProject(),
    );
  }

  loadKlugProject(klugToken: string) {
    return this.klugService.getKlugProject(klugToken).pipe(
      tap(project => {
        this.trackingService.trackEvent('load klug project', { category: 'project' });

        this.exportService.importText(project.data);
        this.project$.next(runInInjectionContext(this.injector, () => new LocalProject('local', klugToken)));
      }),
      this.loadProject(),
    );
  }

  loadTeamProject(teamId: string, { forceUnload = false, loadAsLocal = false } = {}) {
    return this.teamAPIService.getTeam(teamId).pipe(
      tap(team => {
        this.trackingService.trackEvent('load team project', { category: 'project' });

        this.exportService.importText(team.mainProjectData);

        const newProject = runInInjectionContext(this.injector, () =>
          loadAsLocal ? new LocalProject('local', team.name) : new TeamProject(team),
        );
        this.project$.next(newProject);
      }),
      this.loadProject({ forceUnload }),
    );
  }

  loadEmbeddedProject(project: EmbeddableProject) {
    this.exportService.importText(project.data);
    this.project$.next(runInInjectionContext(this.injector, () => new LocalProject('embedded', project.name)));
    this.router.navigateByUrl(this.decisionData.lastUrl || '/start', { onSameUrlNavigation: 'reload' });
  }

  importFile(file: File): Observable<boolean> {
    return FileExport.readFile(file).pipe(
      tap(fileContent => {
        this.trackingService.trackEvent('import project', { category: 'project' });

        const { projectName } = this.exportService.importText(fileContent);
        this.project$.next(runInInjectionContext(this.injector, () => new LocalProject('local', projectName || file.name)));
      }),
      this.loadProject(),
    );
  }

  closeProject() {
    return of(null).pipe(
      tap(() => {
        this.exportService.resetDecisionData();
        this.project$.next(null);
      }),
      this.loadProject(),
    );
  }

  /**
   * Handles unsaved changes in the currently opened project, if there are any.
   * Returns true if unloading was successful and false if the user aborted.
   */
  confirmProjectUnload(unloadMode: UnloadProjectMode): Observable<boolean> {
    const project = this.getProject();

    if (project == null || project.isProjectSaved()) {
      // The project is already saved, nothing to do
      return of(true);
    }

    return this.dialog
      .open<UnloadProjectModalComponent, UnloadProjectMode, boolean>(UnloadProjectModalComponent, { data: unloadMode })
      .beforeClosed();
  }

  /**
   * An RxJS operator that confirms with the user to close the project on unsaved changes, and then
   * executes the given observable. It
   *
   * Returns true if closing succeeded and false if not.
   */
  private loadProject<T>(options: Partial<{ overrideLastUrl: string; forceUnload: boolean }> = {}): OperatorFunction<T, boolean> {
    return observable =>
      (options.forceUnload ? of(true) : this.confirmProjectUnload('close-project')).pipe(
        switchMap(confirmed => {
          if (!confirmed) {
            return of(false);
          }

          const previousUrl = this.router.url;

          return from(this.router.navigateByUrl('/loading')).pipe(
            switchMap(() =>
              observable.pipe(
                tap({
                  // If loading succeeded, we open the last url. This is done in an async function but DOES NOT BLOCK the observable.
                  next: async () => {
                    const result = await this.router.navigateByUrl(options.overrideLastUrl || this.decisionData.lastUrl || '/start', {
                      onSameUrlNavigation: 'reload',
                      replaceUrl: true,
                    });
                    if (!result) {
                      await this.router.navigateByUrl('/start', { onSameUrlNavigation: 'reload', replaceUrl: true });
                    }
                  },
                  // If loading has failed, we go back to the previous URL (asynchronously)
                  error: () => this.router.navigateByUrl(previousUrl, { replaceUrl: true }),
                }),
                map(() => true),
              ),
            ),
            projectLoadingModal(this.dialog),
          );
        }),
      );
  }
}
