import { Observable, Subject, catchError, concatMap, map, of, tap, throwError } from 'rxjs';
import { inject } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';
import { TeamsService } from '@entscheidungsnavi/api-client';
import { MatSnackBar } from '@angular/material/snack-bar';
import { TEAM_EDIT_RIGHT_DURATION, TeamDto, TeamMemberDto } from '@entscheidungsnavi/api-types';
import { AbstractProject } from './abstract-project';
import { checkOnlineProjectSize } from './save-notification';

/**
 * A personal project in an online account. It may or may not be a team project (of which we are the owner).
 */
export class TeamProject extends AbstractProject {
  private teamsService = inject(TeamsService);
  private snackBar = inject(MatSnackBar);

  readonly onSaved$ = new Subject<void>();

  private memberIdToMember: Map<string, TeamMemberDto>;

  readonly userMemberId: string;

  get name() {
    return this.team.name;
  }

  get canSave() {
    return this.userMemberId === this.validEditToken?.holder;
  }

  get userMember() {
    return this.memberIdToMember.get(this.userMemberId);
  }

  get validEditToken() {
    const editTokenInfo = this.team.editTokenInfo;

    return editTokenInfo.state === 'valid' ? this.team.editToken : null;
  }

  constructor(public readonly team: TeamDto) {
    super();

    const userMember = team.members.find(member => member.user?.id === this.authService.user.id);
    this.userMemberId = userMember.id;

    this.memberIdToMember = new Map(this.team.members.map(member => [member.id, member]));

    this.updateLastExportHash();
  }

  override rename(newName: string): Observable<void> {
    return this.teamsService.updateTeam(this.team.id, { name: newName }).pipe(
      tap(_ => {
        this.team.name = newName;
      }),
    );
  }

  override onClose(newProject: AbstractProject) {
    if (newProject instanceof TeamProject && newProject.team.id === this.team.id) {
      return;
    }

    this.teamsService
      .returnEditToken(this.team.id)
      .pipe(catchError(_ => of()))
      .subscribe();
  }

  getTeamMemberFromMemberId(memberId: string) {
    return this.memberIdToMember.get(memberId);
  }

  addComment(objectId: string, commentText: string) {
    return this.teamsService.addComment(this.team.id, objectId, commentText).pipe(
      tap(comments => {
        this.team.comments = comments;
      }),
    );
  }

  deleteComment(commentId: string) {
    return this.teamsService.deleteComment(this.team.id, commentId).pipe(
      catchError(thrownError => {
        if (thrownError instanceof HttpErrorResponse && thrownError.status === 404 && thrownError?.error?.error === 'comment-not-found') {
          // Comment was already deleted
          this.team.comments = this.team.comments.filter(c => c.id !== commentId);
          return of();
        }

        throw thrownError;
      }),
      tap({
        next: comments => {
          this.team.comments = comments;
        },
      }),
    );
  }

  updateCommentsFor(objectId: string) {
    return this.teamsService.getTeamCommentsForObject(this.team.id, objectId).pipe(
      tap(comments => {
        const commentsWithoutTargetObject = this.team.comments.filter(c => c.objectId !== objectId);
        commentsWithoutTargetObject.push(...comments);
        this.team.comments = commentsWithoutTargetObject;
        this.team.comments.sort((a, b) => a.createdAt.getTime() - b.createdAt.getTime());
      }),
    );
  }

  getUsersUnreadComments() {
    const unreadCommentIds = new Set(this.userMember.unreadComments);

    return this.team.comments.filter(comment => unreadCommentIds.has(comment.id));
  }

  returnEditToken() {
    return this.teamsService.returnEditToken(this.team.id).pipe(
      tap({
        next: () => {
          this.team.editToken = null;
        },
      }),
    );
  }

  save(): Observable<void> {
    this.trackingService.trackEvent('save team project', { category: 'project' });

    const data = this.exportService.dataToText(null);
    const dataWithoutEphemeral = this.exportService.changeDetectionRelevantDataToText();

    return of(data).pipe(
      checkOnlineProjectSize(),
      concatMap(data => this.teamsService.updateTeam(this.team.id, { mainProjectData: data })),
      catchError(error => {
        if (error instanceof HttpErrorResponse && error.status === 403) {
          this.snackBar.open($localize`Du hast nicht länger das Recht das Hauptprojekt zu bearbeiten.`, 'Ok');

          this.team.editToken = null;

          return of();
        }

        return throwError(() => error);
      }),
      map(_ => {
        this.updateLastExportHash(dataWithoutEphemeral);
        this.team.mainProjectData = data;
        this.team.editToken.expiresAt = new Date(Date.now() + TEAM_EDIT_RIGHT_DURATION);
        this.onSaved$.next();

        return null;
      }),
    );
  }
}
