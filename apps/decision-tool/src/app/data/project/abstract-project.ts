import { hashCode } from '@entscheidungsnavi/tools';
import { inject } from '@angular/core';
import { FileExport } from '@entscheidungsnavi/widgets';
import { AuthService } from '@entscheidungsnavi/api-client';
import sanitize from 'sanitize-filename';
import { Observable } from 'rxjs';
import { DecisionDataExportService } from '../decision-data-export.service';
import { TrackingService } from '../tracking.service';

export abstract class AbstractProject {
  protected exportService = inject(DecisionDataExportService);
  protected trackingService = inject(TrackingService);
  protected authService = inject(AuthService);

  private lastExportHash: number;

  abstract get name(): string;
  abstract rename(newName: string): Observable<void>;

  protected updateLastExportHash(exportText?: string) {
    exportText = exportText ?? this.exportService.changeDetectionRelevantDataToText();
    this.lastExportHash = hashCode(exportText);
  }

  isProjectSaved(): boolean {
    const data = this.exportService.changeDetectionRelevantDataToText();
    return hashCode(data) === this.lastExportHash;
  }

  exportFile() {
    this.trackingService.trackEvent('export project', { category: 'project' });

    const name = (sanitize(this.name) || 'export') + '.json';
    const data = this.exportService.dataToText(this.name);
    FileExport.download(name, data);
  }

  onClose(_newProject: AbstractProject) {
    // noop
  }
}
