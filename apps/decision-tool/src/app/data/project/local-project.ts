import { defer, Observable, of } from 'rxjs';
import { AbstractProject } from './abstract-project';

export class LocalProject extends AbstractProject {
  private _projectName: string;

  get name() {
    return this._projectName;
  }

  constructor(
    public readonly type: 'local' | 'quickstart' | 'linked' | 'embedded',
    projectName: string,
    isInitiallySaved = true,
  ) {
    super();

    this._projectName = projectName;

    if (isInitiallySaved) {
      this.updateLastExportHash();
    }
  }

  override exportFile() {
    super.exportFile();
    // Exporting to JSON counts as saving for local projects
    this.updateLastExportHash();
  }

  override rename(newName: string): Observable<void> {
    return defer(() => {
      this._projectName = newName;
      return of(null);
    });
  }
}
