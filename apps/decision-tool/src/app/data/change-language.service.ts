import { Injectable } from '@angular/core';
import { Location } from '@angular/common';
import { firstValueFrom } from 'rxjs';
import { LanguageService } from './language.service';
import { ProjectService } from './project';

@Injectable({ providedIn: 'root' })
export class ChangeLanguageService {
  private _isChangingLanguage = false;
  get isChangingLanguage() {
    return this._isChangingLanguage;
  }

  constructor(
    private languageService: LanguageService,
    private location: Location,
    private projectService: ProjectService,
  ) {}

  async changeLanguage() {
    if (!(await firstValueFrom(this.projectService.confirmProjectUnload('change-language')))) {
      return;
    }

    this._isChangingLanguage = true;
    let url: string;
    if (this.languageService.isEnglish) {
      url = '../de';
    } else {
      url = '../en';
    }
    document.location.href = this.location.prepareExternalUrl(url + this.location.path());
  }
}
