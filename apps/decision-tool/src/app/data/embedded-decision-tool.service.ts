import { Inject, Injectable, OnDestroy } from '@angular/core';
import { plainToInstance } from 'class-transformer';
import { BehaviorSubject, filter, pairwise, shareReplay, Subscription } from 'rxjs';
import {
  AbstractEmbedderToDecisionToolMessageBodyDto,
  DecisionToolToEmbedderMessageDto,
  EmbedderToDecisionToolMessageDto,
  LoadProjectMessageBodyDto,
  ProjectDirtyMessageBodyDto,
  ProjectRequestMessageBodyDto,
  ProjectResponseMessageBodyDto,
} from '@entscheidungsnavi/embedded-decision-tool';
import { COCKPIT_ORIGIN } from '../decision-tool.module';
import { TimeTrackingService } from './time-tracking.service';
import { ProjectService } from './project';
import { DecisionDataExportService } from './decision-data-export.service';

const STATE = {
  NO_PROJECT_LOADED: 'open-but-no-project-loaded',
  PROJECT_LOADED: 'open-and-project-loaded',
  CLOSED: 'closed',
} as const;

type State = (typeof STATE)[keyof typeof STATE];

/**
 * This service assists `EmbeddedDecisionToolPortalOutletComponent` in
 * - _loading_ a project into DT,
 * - _fetching_ a project from DT,
 * - and _notifying_ it with project updates from DT
 */
@Injectable({
  providedIn: 'root',
})
export class EmbeddedDecisionToolService implements OnDestroy {
  private state$ = new BehaviorSubject<State>(STATE.CLOSED);

  private isProjectLoaded$ = this.state$.pipe(
    filter(state => state === STATE.PROJECT_LOADED),
    shareReplay(1),
  );

  private isProjectUnloaded$ = this.state$.pipe(
    pairwise(),
    filter(([previousState, _]) => previousState === STATE.PROJECT_LOADED),
  );

  private embedderOrigin: string;
  private projectUpdatedSubscription: Subscription | null = null;

  messageListener: (message: MessageEvent<unknown>) => void;

  constructor(
    @Inject(COCKPIT_ORIGIN) private cockpitOrigin: string,
    private timeTrackingService: TimeTrackingService,
    private projectService: ProjectService,
    private decisionDataExportService: DecisionDataExportService,
  ) {}

  openFromCockpit() {
    if (this.state$.value !== STATE.CLOSED) throw new Error('Portal is already open.');

    this.embedderOrigin = this.cockpitOrigin;

    this.messageListener = unsafeMessage => this.receiveUnsafeMessage(unsafeMessage);
    window.addEventListener('message', this.messageListener);

    this.state$.next(STATE.NO_PROJECT_LOADED);

    return this.isProjectLoaded();
  }

  markProjectAsDirty() {
    if (this.state$.value === STATE.CLOSED) return;

    this.sendMessage(
      plainToInstance(ProjectDirtyMessageBodyDto, {
        messageType: 'dt-embedded-project-dirty',
      } satisfies ProjectDirtyMessageBodyDto),
    );
  }

  isProjectLoaded() {
    return this.isProjectLoaded$;
  }

  private receiveUnsafeMessage(unsafeMessage: MessageEvent<unknown>) {
    if (unsafeMessage.origin !== this.embedderOrigin) return;
    if (unsafeMessage.source !== window.parent) return;

    const embedderToDecisionToolMessage = plainToInstance(EmbedderToDecisionToolMessageDto, unsafeMessage.data);
    this.receiveMessage(embedderToDecisionToolMessage.body);
  }

  private receiveMessage(messageBody: AbstractEmbedderToDecisionToolMessageBodyDto) {
    if (messageBody instanceof LoadProjectMessageBodyDto && this.state$.value === STATE.NO_PROJECT_LOADED) {
      this.projectService.loadEmbeddedProject(messageBody.project);
      this.projectUpdatedSubscription = this.timeTrackingService.decisionDataChanged().subscribe(() => this.markProjectAsDirty());
      this.state$.next(STATE.PROJECT_LOADED);
    } else if (messageBody instanceof ProjectRequestMessageBodyDto && this.state$.value === STATE.PROJECT_LOADED) {
      this.sendMessage(
        plainToInstance(ProjectResponseMessageBodyDto, {
          messageType: 'dt-embedded-project-response',
          project: {
            name: this.projectService.getProjectName(),
            data: this.decisionDataExportService.dataToText(null),
          },
        } satisfies ProjectResponseMessageBodyDto),
      );
    } else {
      throw new Error(`Cannot receive message of type '${messageBody.messageType}' in state '${this.state$.value}'.`);
    }
  }

  private sendMessage(message: ProjectResponseMessageBodyDto | ProjectDirtyMessageBodyDto) {
    const send = () => window.parent.postMessage({ body: message } satisfies DecisionToolToEmbedderMessageDto, this.embedderOrigin);

    if (message instanceof ProjectResponseMessageBodyDto && this.state$.value === STATE.PROJECT_LOADED) {
      send();
    } else if (message instanceof ProjectDirtyMessageBodyDto && this.state$.value === STATE.PROJECT_LOADED) {
      send();
    } else {
      throw new Error(`Cannot send message of type '${message.messageType}' in state '${this.state$.value}'.`);
    }
  }

  close() {
    window.removeEventListener('message', this.messageListener);
    this.projectUpdatedSubscription?.unsubscribe();
    this.state$.next(STATE.CLOSED);
  }

  ngOnDestroy() {
    this.close();
  }
}
