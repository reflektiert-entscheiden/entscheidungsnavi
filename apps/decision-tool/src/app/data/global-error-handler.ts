import { ErrorHandler, Injectable, Injector } from '@angular/core';
import * as Sentry from '@sentry/angular';
import { AuthService, UserDto } from '@entscheidungsnavi/api-client';
import { DecisionData } from '@entscheidungsnavi/decision-data';
import { pick } from 'lodash';
import { GlobalErrorService } from './global-error.service';
import { ProjectService } from './project';

@Injectable()
export class GlobalErrorHandler implements ErrorHandler {
  sentryHandler: ErrorHandler;

  constructor(private injector: Injector) {
    this.sentryHandler = Sentry.createErrorHandler({
      showDialog: false,
      logErrors: false,
    });
  }

  async handleError(error: any) {
    let projectVersion: string = null;
    try {
      const projectService = this.injector.get(ProjectService);
      if (projectService.isProjectLoaded()) {
        const decisionData = this.injector.get(DecisionData);
        projectVersion = decisionData.version;
      }
    } catch {}

    let user: UserDto = null;
    try {
      user = this.injector.get(AuthService).user;
    } catch {}

    Sentry.withScope(scope => {
      if (projectVersion) {
        scope.setContext('project', {
          version: projectVersion,
        });
      }
      if (user) {
        scope.setUser(pick(user, ['id', 'email', 'roles']));
      }
      this.sentryHandler.handleError(error);
    });

    // inject manually, since this provider is loaded before services
    const globalErrorService = this.injector.get(GlobalErrorService);
    globalErrorService.handleError(error);
    // pass the error to the console
    console.error(error);
  }
}
