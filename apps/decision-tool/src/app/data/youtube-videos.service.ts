import { Injectable } from '@angular/core';
import { forkJoin, Subject } from 'rxjs';
import { YoutubeVideoCategoryDto, YoutubeVideoDto, YoutubeVideosService as APIYoutubeVideosService } from '@entscheidungsnavi/api-client';
import { YoutubeModalComponent, YoutubeModalData } from '@entscheidungsnavi/widgets';
import { MatDialog } from '@angular/material/dialog';

@Injectable({
  providedIn: 'root',
})
export class YoutubeVideosService {
  videos: YoutubeVideoDto[] = [];
  categories: YoutubeVideoCategoryDto[] = [];

  dataFetched$ = new Subject<void>();

  constructor(
    private dialog: MatDialog,
    protected youtubeVideosService: APIYoutubeVideosService,
  ) {
    forkJoin({
      videos: this.youtubeVideosService.getYoutubeVideosList(),
      categories: this.youtubeVideosService.getVideoCategoriesList(),
    }).subscribe({
      next: result => {
        this.videos = result.videos ?? [];
        this.categories = result.categories ?? [];
        this.dataFetched$.next();
      },
      error: (err: any) => console.error(err),
    });
  }

  openYoutubeVideoModal(videoId: string, startTime?: number) {
    this.dialog.open<YoutubeModalComponent, YoutubeModalData>(YoutubeModalComponent, {
      data: {
        videoId: videoId,
        startTime: startTime,
        videos: this.videos,
        categories: this.categories,
      },
    });
  }
}
