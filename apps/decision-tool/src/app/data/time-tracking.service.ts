import { Injectable, NgZone } from '@angular/core';
import { DecisionData, NAVI_STEP_ORDER, TimeRecording } from '@entscheidungsnavi/decision-data';
import { ResolveEnd, Router } from '@angular/router';
import { filter, Subject } from 'rxjs';
import { cloneDeep, debounce, isEqual } from 'lodash';
import { AuthService } from '@entscheidungsnavi/api-client';
import { AppSettingsService } from './app-settings.service';
import { ProjectService } from './project';

@Injectable({
  providedIn: 'root',
})
export class TimeTrackingService {
  // if no changes to DD are performed within this limit, the activeTime will stop being measured
  private readonly inactivityTimeLimit = 60; // in seconds
  // minimal time before next DD quality check (1s)
  private readonly checkDDCooldown = 1000; // in ms

  private lastDDChangeTime: number; // time of last changes to DD
  private unfocusStartTime: number; // time at which the window was unfocused
  private lastData: ReturnType<typeof DecisionData.prototype.getPropertiesForChangeDetection>;

  private ddChanged$ = new Subject<void>();

  timeRecording: TimeRecording;

  comparisonTimeOut: ReturnType<typeof setTimeout>;

  private userIsPrivileged = false;

  get timeTrackingEnabled() {
    // When the user is not privileged or has the setting enabled
    return !this.userIsPrivileged || this.appSettings.measureTimeSpent;
  }

  constructor(
    private appSettings: AppSettingsService,
    projectService: ProjectService,
    private authService: AuthService,
    private decisionData: DecisionData,
    private router: Router,
    private zone: NgZone,
  ) {
    this.checkForDDChanges = debounce(this.checkForDDChanges, this.checkDDCooldown, { leading: true, trailing: true, maxWait: 5000 });

    this.authService.userHasRole('privileged-user').subscribe(isPrivileged => (this.userIsPrivileged = isPrivileged));

    this.router.events.pipe(filter(event => event instanceof ResolveEnd)).subscribe(() => {
      this.calculateTime();
      this.updateLastDDChangeDate();
    });

    projectService.project$.subscribe(() => this.reset());
  }

  onFocus() {
    if (this.unfocusStartTime) {
      const idleTimePassed = performance.now() - this.unfocusStartTime;
      this.lastDDChangeTime = this.lastDDChangeTime + idleTimePassed;
    }
  }

  onBlur() {
    this.unfocusStartTime = performance.now();
  }

  private calculateTime() {
    if (!this.timeTrackingEnabled) {
      return;
    }

    const currentTime = performance.now();
    const currentSubStep = this.getCurrentSubStep();
    if (currentSubStep == null) {
      return;
    }
    const currentSubStepIdx = currentSubStep.subStepIndex
      ? currentSubStep.subStepIndex - 1
      : this.timeRecording.timers[currentSubStep.step].length - 1;
    const timeDiff = (currentTime - this.lastDDChangeTime) / 1000;
    if (timeDiff > 0) {
      this.timeRecording.timers[currentSubStep.step][currentSubStepIdx].activeTime += Math.min(timeDiff, this.inactivityTimeLimit);
      this.timeRecording.timers[currentSubStep.step][currentSubStepIdx].totalTime += timeDiff;
    }
  }

  checkForDDChanges() {
    // This function runs outside the Angular Zone because it is called by the Time Tracking Component
    const tempData = this.decisionData.getPropertiesForChangeDetection();

    if (!isEqual(tempData, this.lastData)) {
      this.calculateTime();
      this.updateLastDDChangeDate();
      this.lastData = cloneDeep(tempData);
      this.zone.run(() => this.ddChanged$.next());
    }
  }

  decisionDataChanged() {
    return this.ddChanged$.asObservable();
  }

  private updateLastDDChangeDate() {
    this.lastDDChangeTime = performance.now();
  }

  reset() {
    this.lastDDChangeTime = performance.now();
    this.unfocusStartTime = null;
    this.lastData = cloneDeep(this.decisionData.getPropertiesForChangeDetection());

    this.comparisonTimeOut = null;

    this.timeRecording = this.decisionData.timeRecording;
  }

  private getCurrentSubStep() {
    const urlPaths = this.router.parseUrl(this.router.url).root.children.primary?.segments.map(s => {
      return s.path;
    });

    if (!urlPaths) {
      return null;
    }

    // TODO: add professional tracking after merging #671
    for (const naviStep of NAVI_STEP_ORDER) {
      const stepIndex = urlPaths.indexOf(naviStep.toLowerCase());
      if (stepIndex >= 0) {
        if (urlPaths.indexOf('uncertaintyfactors') >= 0) {
          // Influence Factors
          // /impactmodel/uncertaintyfactors
          return { step: naviStep, subStepIndex: 1 };
        }
        if (urlPaths[stepIndex + 2] != null) {
          // Step 1,2,3,5 + Substep
          // e.g. /alternatives/steps/2 or /results/steps/1
          return { step: naviStep, subStepIndex: parseInt(urlPaths[stepIndex + 2]) };
        }
        // Step 4 or finish project
        // /impactmodel or /finishproject
        return { step: naviStep };
      }
    }

    return null;
  }
}
