import { coerceStringArray } from '@angular/cdk/coercion';
import { HttpErrorResponse } from '@angular/common/http';
import { Injectable, isDevMode } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { AuthService, QuickstartService, TeamsService } from '@entscheidungsnavi/api-client';
import { DecisionData, PROJECT_MODES, ProjectMode } from '@entscheidungsnavi/decision-data';
import { ConfirmModalComponent, ConfirmModalData, YoutubeModalComponent, YoutubeModalData } from '@entscheidungsnavi/widgets';
import { loadingIndicator } from '@entscheidungsnavi/widgets/loading-snackbar/loading-snackbar.component';
import { isMongoId } from 'class-validator';
import { EMPTY, catchError, concatMap, filter, first, firstValueFrom, fromEvent, of, switchMap } from 'rxjs';
import { ENVIRONMENT } from '../../environments/environment';
import { NewProjectModalComponent } from '../main/start';
import { LoginModalComponent } from '../main/userarea';
import { ProjectListModalComponent } from '../navigation';
import { ProjectListModalData } from '../navigation/projects/project-list-modal/project-list-modal.component';
import { NightlyWarningModalComponent } from '../nightly/nightly-warning-modal/nightly-warning-modal.component';
import { AppSettingsService } from './app-settings.service';
import { DecisionDataExportService } from './decision-data-export.service';
import { EmbeddedDecisionToolService } from './embedded-decision-tool.service';
import { LocalProject, OnlineProject, ProjectService } from './project';
import { TeamProject } from './project/team-project';
import { YoutubeVideosService } from './youtube-videos.service';

type QuickactionParam =
  | { quickaction: 'openQuickstartProject'; query?: string; tags?: string | string[] }
  | { quickaction: 'newProject'; projectMode: ProjectMode }
  | { quickaction: 'openVideoModal'; youtubeVideoId?: string; videoSection?: 'steps' | 'topics'; stepIdx?: string; topicId?: string };

type StartParams =
  | Partial<{
      quickstart: string;
      embedded: unknown;
      linkedproject: string;
      eventId: string;
      registrationId: string;
      klugToken: string;
      joinTeam: string;
    }>
  | QuickactionParam;

const lastProjectInfoKey = 'dt-last-project-info';
const devModeTempStorageKey = 'dev-mode-project-store';

@Injectable({
  providedIn: 'root',
})
export class StartupService {
  private alreadyRan = false;

  constructor(
    private projectService: ProjectService,
    private exportService: DecisionDataExportService,
    private embeddedDecisionToolService: EmbeddedDecisionToolService,
    private dialog: MatDialog,
    private appSettings: AppSettingsService,
    private quickstartService: QuickstartService,
    private snackbarService: MatSnackBar,
    private authService: AuthService,
    private teamAPIService: TeamsService,
    private router: Router,
    private decisionData: DecisionData,
    private videosService: YoutubeVideosService,
  ) {
    this.projectService.project$.pipe(filter(() => this.alreadyRan)).subscribe(project => {
      if (project instanceof OnlineProject) {
        sessionStorage.setItem(lastProjectInfoKey, JSON.stringify({ onlineProjectId: project.info.id }));
      } else if (project instanceof TeamProject) {
        sessionStorage.setItem(lastProjectInfoKey, JSON.stringify({ teamId: project.team.id }));
      } else sessionStorage.removeItem(lastProjectInfoKey);
    });

    if (isDevMode()) {
      fromEvent(window, 'beforeunload').subscribe(() => {
        if (this.projectService.isProjectLoaded()) {
          sessionStorage.setItem(devModeTempStorageKey, this.exportService.dataToText(this.projectService.getProjectName()));
        } else {
          sessionStorage.removeItem(devModeTempStorageKey);
        }
      });
    }
  }

  async autorun(params: StartParams) {
    if (this.alreadyRan) {
      return;
    }
    this.alreadyRan = true;

    this.showNightlyWarning();

    if ('embedded' in params) {
      return await firstValueFrom(this.embeddedDecisionToolService.openFromCockpit());
    }

    if ('quickaction' in params) {
      const action = params.quickaction;
      if (action === 'newProject') {
        const projectMode = params.projectMode && PROJECT_MODES.includes(params.projectMode) ? params.projectMode : null;

        if (projectMode === 'starter') {
          this.projectService.newProject($localize`Unbenanntes Projekt`, 'starter').subscribe();
        } else {
          this.dialog.open(NewProjectModalComponent, { data: projectMode });
        }
      } else if (action === 'openQuickstartProject') {
        this.quickstartService
          .getProjects()
          .pipe(loadingIndicator(this.snackbarService, $localize`Themenprojekte werden geladen`))
          .subscribe({
            next: list =>
              this.dialog.open<ProjectListModalComponent, ProjectListModalData>(ProjectListModalComponent, {
                data: {
                  quickstart: true,
                  projectList: list,
                  query: params.query,
                  tagFilter: coerceStringArray(params.tags),
                },
              }),
            error: () => {
              this.dialog.open(ConfirmModalComponent, {
                data: {
                  title: $localize`Fehler`,
                  prompt: $localize`Themenprojekte sind aktuell nicht verfügbar. Versuche es später erneut.`,
                  buttonDeny: 'Okay',
                },
              });
            },
          });
      } else if (action === 'openVideoModal') {
        let youtubeVideoId: string | null, selectedTabIndex: number | null, stepNumber: number | null, categoryId: string | null;
        if (params.youtubeVideoId != null) {
          // prioritize videoId over videoSection
          youtubeVideoId = params.youtubeVideoId;
        } else {
          if (params.videoSection === 'steps') {
            selectedTabIndex = 0;
            if (params.stepIdx) {
              stepNumber = Number(params.stepIdx);
            }
          } else if (params.videoSection === 'topics') {
            selectedTabIndex = 1;
            if (params.topicId) {
              categoryId = params.topicId;
            }
          }
        }

        if (this.videosService.videos.length === 0 && this.videosService.categories.length === 0) {
          // wait for data to be fetched
          this.videosService.dataFetched$.pipe(first()).subscribe(() => {
            this.dialog.open<YoutubeModalComponent, YoutubeModalData>(YoutubeModalComponent, {
              data: {
                videos: this.videosService.videos,
                categories: this.videosService.categories,
                videoId: youtubeVideoId,
                selectedTabIndex,
                stepNumber,
                categoryId,
              },
            });
          });
        } else {
          this.dialog.open<YoutubeModalComponent, YoutubeModalData>(YoutubeModalComponent, {
            data: {
              videos: this.videosService.videos,
              categories: this.videosService.categories,
              videoId: youtubeVideoId,
              selectedTabIndex,
              stepNumber,
              categoryId,
            },
          });
        }
      }
      return;
    }

    if (params.quickstart) {
      this.projectService.loadQuickstartProject(params.quickstart, { showErrorMessage: false }).subscribe({
        error: () => {
          this.dialog.open(ConfirmModalComponent, {
            data: {
              title: $localize`Fehler`,
              prompt: $localize`Das verlinkte Themenprojekt existiert nicht mehr.`,
              buttonDeny: 'Okay',
            },
          });
        },
      });

      return;
    }

    if (params.linkedproject) {
      this.projectService.loadLinkedProject(params.linkedproject).subscribe({
        error: () => {
          this.dialog.open(ConfirmModalComponent, {
            data: {
              title: $localize`Fehler`,
              prompt: $localize`Das verlinkte Projekt existiert nicht mehr oder der Link wurde deaktiviert.`,
              buttonDeny: 'Okay',
            },
          });
        },
      });

      return;
    }

    if (isMongoId(params.eventId) && isMongoId(params.registrationId)) {
      this.projectService.loadEventSubmission(params.eventId, params.registrationId).subscribe({
        error: (error: HttpErrorResponse) => {
          this.dialog.open(ConfirmModalComponent, {
            data: {
              title: $localize`Fehler beim Laden der Abgabe zur Veranstaltung`,
              prompt:
                error.status === 401 || error.status === 403
                  ? $localize`Du besitzt nicht die nötige Berechtigung, um Abgaben aus dieser Veranstaltung zu laden.
                  Stelle sicher, dass Du mit dem richtigen Account eingeloggt bist.`
                  : error.status === 404
                    ? $localize`Die angegebene Abgabe zur Veranstaltung wurde nicht gefunden. Möglicherweise ist der Link fehlerhaft.`
                    : $localize`Ein unbekannter Fehler ist beim Laden der Abgabe aufgetreten.`,
              buttonDeny: 'Okay',
            },
          });
        },
      });
    }

    if (params.klugToken) {
      this.projectService.loadKlugProject(params.klugToken).subscribe({
        error: (error: Error) => {
          const isMissing = error instanceof HttpErrorResponse && error.status === 404;

          this.dialog.open<ConfirmModalComponent, ConfirmModalData>(ConfirmModalComponent, {
            data: {
              title: $localize`Fehler beim Laden des KLUGentscheiden Projekts`,
              prompt: isMissing
                ? $localize`Unter dem Token „${params.klugToken}“ konnte kein Projekt gefunden werden.`
                : $localize`Bei der Abfrage des Projekts ist ein Fehler aufgetreten. Versuche es eventuell später erneut.`,
              buttonDeny: 'Okay',
            },
          });
        },
      });
      return;
    }

    if (params.joinTeam) {
      await this.joinTeam(params.joinTeam);
      return;
    }

    let lastProjectInfo: unknown;
    try {
      const lastProjectText = sessionStorage.getItem(lastProjectInfoKey);
      if (lastProjectText) {
        lastProjectInfo = JSON.parse(lastProjectText);
      }
    } catch {}

    if (lastProjectInfo && typeof lastProjectInfo === 'object') {
      const lastProjectInfoObject = lastProjectInfo;
      this.authService.loggedIn$
        .pipe(
          first(),
          filter(Boolean),
          switchMap(() => {
            if ('onlineProjectId' in lastProjectInfoObject && typeof lastProjectInfoObject.onlineProjectId === 'string') {
              return this.projectService.loadOnlineProject(lastProjectInfoObject.onlineProjectId, { showErrorMessage: false });
            } else if ('teamId' in lastProjectInfoObject && typeof lastProjectInfoObject.teamId === 'string') {
              return this.projectService.loadTeamProject(lastProjectInfoObject.teamId);
            } else {
              return EMPTY;
            }
          }),
        )
        .subscribe({
          error: () => this.snackbarService.open($localize`Das zuletzt geöffnete Projekt konnte nicht automatisch geladen werden.`, 'Ok'),
        });
      return;
    }

    if (isDevMode()) {
      const project = sessionStorage.getItem(devModeTempStorageKey);
      if (project) {
        try {
          const { projectName } = this.exportService.importText(project);
          this.projectService.project$.next(new LocalProject('local', projectName));
          this.router.navigateByUrl(this.decisionData.lastUrl || '/start', { onSameUrlNavigation: 'reload' });
          return;
        } catch (e) {
          console.error(e);
        }
      }
    }
  }

  private async joinTeam(token: string) {
    if (!(await firstValueFrom(this.authService.loggedIn$))) {
      await firstValueFrom(this.dialog.open(LoginModalComponent).afterClosed());
    }

    if (await firstValueFrom(this.authService.loggedIn$)) {
      const info = await firstValueFrom(
        this.teamAPIService.getInviteInfo(token).pipe(
          catchError(error => {
            if (error instanceof HttpErrorResponse && error.status == 404) {
              this.snackbarService.open(
                $localize`Die Einladung wurde bereits verwendet oder das Team existiert nicht mehr.`,
                $localize`Ok`,
              );
            } else {
              this.snackbarService.open(
                $localize`Beim Abrufen der Team-Informationen ist ein unbekannter Fehler aufgetreten.`,
                $localize`Ok`,
              );
            }

            return of(null);
          }),
        ),
      );

      if (!info) {
        return;
      }

      const confirmed = await firstValueFrom(
        this.dialog
          .open<ConfirmModalComponent, ConfirmModalData>(ConfirmModalComponent, {
            data: {
              title: $localize`Team beitreten`,
              prompt: $localize`${info.inviterName} hat Dich in das Team „${info.teamName}“ eingeladen.
              Möchtest Du dem Team beitreten?`,
              buttonConfirm: $localize`Ja`,
              buttonDeny: $localize`Nein`,
              buttonConfirmColor: 'accent',
            },
          })
          .afterClosed(),
      );

      if (!confirmed) {
        return;
      }

      this.teamAPIService
        .join(token)
        .pipe(concatMap(project => this.projectService.loadTeamProject(project.id)))
        .subscribe({
          error: error => {
            if (error instanceof HttpErrorResponse && error.status === 409) {
              this.snackbarService.open($localize`Du bist bereits Teil dieses Teams.`, $localize`Ok`);
              return;
            }

            this.snackbarService.open($localize`Beim Teambeitritt ist ein unbekannter Fehler aufgetreten.`, $localize`Ok`);
          },
        });
    }
  }

  private showNightlyWarning() {
    if (this.appSettings.showNightlyWarning && ENVIRONMENT.nightly) {
      this.dialog.open(NightlyWarningModalComponent);
    }
  }
}
