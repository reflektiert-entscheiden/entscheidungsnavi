import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { DecisionData } from '@entscheidungsnavi/decision-data';
import { calculateProjectStatusValues } from '@entscheidungsnavi/widgets';
import { catchError, EMPTY, Subject, switchMap, tap, timer } from 'rxjs';
import { takeUntilDestroyed } from '@angular/core/rxjs-interop';
import { ProjectViewData } from '../project-view';
import { ExcelExportService } from '../excel-export';
import { DecisionDataExportService } from '../data/decision-data-export.service';
import { OnlineProject, ProjectService } from '../data/project';

@Component({
  templateUrl: './project-information.component.html',
  styleUrls: ['./project-information.component.scss'],
})
export class ProjectInformationComponent implements OnInit {
  data: ProjectViewData;
  projectLocation: 'online' | 'local';
  selectedTabIndex: number;
  projectStatusText: string[] = ['', '', '', '', ''];
  dates: Date[];

  projectSize: number;

  get projectName() {
    return this.projectService.getProjectName();
  }

  newProjectName$ = new Subject<string>();
  renameState: 'saving' | 'conflict' | 'error' | 'saved' = null;

  constructor(
    protected decisionData: DecisionData,
    private excelExportService: ExcelExportService,
    public dialogRef: MatDialogRef<ProjectInformationComponent>,
    private exportService: DecisionDataExportService,
    private projectService: ProjectService,
  ) {
    this.newProjectName$
      .pipe(
        switchMap(newName => {
          this.renameState = 'saving';
          return timer(1_500).pipe(
            switchMap(() => this.projectService.getProject().rename(newName)),
            catchError(error => {
              if (error instanceof Error && error.message === 'name conflict') {
                this.renameState = 'conflict';
              } else {
                this.renameState = 'error';
              }
              return EMPTY;
            }),
            tap(() => (this.renameState = 'saved')),
          );
        }),
        takeUntilDestroyed(),
      )
      .subscribe();
  }

  ngOnInit() {
    this.data = new ProjectViewData(this.decisionData);
    const project = this.projectService.getProject();

    this.projectSize = this.exportService.getProjectSize();

    // online/linked/quickstart projects belong to Entscheidungsnavi Cloud / online
    // whereas projects loaded from own device and newly created projects belong to local storage
    if (project instanceof OnlineProject) {
      this.dates = [project.info.createdAt, project.info.updatedAt];
      this.projectLocation = 'online';
    } else {
      this.projectLocation = 'local';
    }

    this.projectStatusText = calculateProjectStatusValues(this.decisionData);
  }

  exportAsExcel(onlyExpanded: boolean) {
    this.excelExportService.performExcelExport(onlyExpanded, this.data);
  }
}
