import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import {
  Alternative,
  CompositeUserDefinedInfluenceFactor,
  Outcome,
  SimpleUserDefinedInfluenceFactor,
} from '@entscheidungsnavi/decision-data';
import { InfluenceFactorNamePipe } from '@entscheidungsnavi/widgets';
import { DecisionData } from '@entscheidungsnavi/decision-data';
import { isEqual } from 'lodash';
import sanitize from 'sanitize-filename';
import { getImpactModelName, getInfluenceFactorsName } from '../excel-export/excel-helpers';
import { LanguageService } from '../data/language.service';
import { ProjectService } from '../data/project';
import { ModifiedInfluenceFactors, performImpactModelImport } from './sheets/excel-impact-model-import';
import { performExportReadme } from './sheets/excel-readme-sheet';
import { performImpactModelExport } from './sheets/excel-impact-model-export';
import { performInfluenceFactorExport } from './sheets/excel-influence-factors-export';
import { performInfluenceFactorImport } from './sheets/excel-influence-factors-import';

export type ChangePair = {
  targetName: string;
  changeType: ChangeType;
};

export enum ChangeType {
  AlternativeAdded,
  AlternativeChanged,
  AlternativeUnchanged,
  AlternativeNotFound,
  InfluenceFactorChanged,
}

export type ErrorPair = {
  targetName?: string;
  coordinates?: string;
  errorType: ErrorType;
};

export enum ErrorType {
  SheetMissing,
  ObjectiveNameInvalid,
  ObjectiveScaleInvalid,
  ObjectiveIndicatorRowsInvalid,
  AlternativeDuplicate,
  AlternativeUnknownInfluenceFactor,
  AlternativeOrderFalse,
  AlternativeValueOutsideOfBounds,
  InfluenceFactorUnknownInfluenceFactor,
  InfluenceFactorUnknownState,
  InfluenceFactorInvalidValue,
  InfluenceFactorPrecisionsDontSumUp,
}

@Injectable()
export class ExcelImpactModelExportService {
  importAlternatives: Alternative[];
  importOutcomes: Outcome[][];
  importInfluenceFactorReferences: ModifiedInfluenceFactors[];
  importInfluenceFactors: SimpleUserDefinedInfluenceFactor[];

  loadingImportSuccessful: boolean;

  constructor(
    private decisionData: DecisionData,
    private languageService: LanguageService,
    private projectService: ProjectService,
    private snackBar: MatSnackBar,
  ) {}

  /* Top level function that creates the Excel file and its sheets and calls on specific functions for exporting the data. */
  async performExcelExport(protectSheet: boolean) {
    const decisionData = this.decisionData;
    const isEnglish = this.languageService.isEnglish;

    const exceljs = (await import('exceljs')).default;
    const wb = new exceljs.Workbook();
    const ws1 = wb.addWorksheet(getImpactModelName(isEnglish));
    performImpactModelExport(decisionData, ws1, protectSheet, isEnglish);
    const ws2 = wb.addWorksheet(getInfluenceFactorsName(isEnglish));
    performInfluenceFactorExport(decisionData, ws2, protectSheet);
    const ws3 = wb.addWorksheet(`README`);
    performExportReadme(ws3);

    const excelData = await wb.xlsx.writeBuffer();
    const blob = new Blob([excelData], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' });

    // download export
    const element = document.createElement('a');
    element.setAttribute('href', URL.createObjectURL(blob));
    element.setAttribute('download', sanitize(this.projectService.getProjectName()) + '-' + getImpactModelName(isEnglish) + '.xlsx');
    element.style.display = 'none';
    document.body.appendChild(element);
    element.click();
    document.body.removeChild(element);
  }

  /* Top level function that reads from the Excel file and checks which Alternatives and InfluenceFactors were changed. */
  async performExcelImport(file: File) {
    this.resetImportData();

    const exceljs = (await import('exceljs')).default;
    const wb = new exceljs.Workbook();
    const isEnglish = this.languageService.isEnglish;

    return new Promise<{ messagePairs: ChangePair[] | ErrorPair[] }>((res, rej) => {
      const reader = new FileReader();
      reader.onload = async () => {
        const buffer = reader.result;
        const workbook = await wb.xlsx.load(buffer as ArrayBuffer);
        /* Check if all sheets are available. */
        const impactModelSheet = workbook.getWorksheet(getImpactModelName(true)) ?? workbook.getWorksheet(getImpactModelName(false));
        if (impactModelSheet == null) {
          this.loadingImportSuccessful = false;
          return res({ messagePairs: [{ targetName: getImpactModelName(false), errorType: ErrorType.SheetMissing }] });
        }
        const influenceFactorsSheet =
          workbook.getWorksheet(getInfluenceFactorsName(true)) ?? workbook.getWorksheet(getInfluenceFactorsName(false));
        if (influenceFactorsSheet == null) {
          this.loadingImportSuccessful = false;
          return res({
            messagePairs: [{ targetName: getInfluenceFactorsName(isEnglish), errorType: ErrorType.SheetMissing }],
          });
        }
        /* Perform local import (does not update DecisionData). */
        const impactModelImportReturn = performImpactModelImport(this.decisionData, impactModelSheet);
        const influenceFactorsImportReturn = performInfluenceFactorImport(this.decisionData, influenceFactorsSheet);
        if (
          impactModelImportReturn.alternatives != null &&
          impactModelImportReturn.outcomes != null &&
          impactModelImportReturn.influenceFactors != null &&
          influenceFactorsImportReturn.influenceFactors != null
        ) {
          // NO ERRORS
          /* Save data from Excel locally. */
          this.importAlternatives = impactModelImportReturn.alternatives;
          this.importOutcomes = impactModelImportReturn.outcomes;
          this.importInfluenceFactorReferences = impactModelImportReturn.influenceFactors;
          this.importInfluenceFactors = influenceFactorsImportReturn.influenceFactors;
          /* Compute change log. */
          const changeLog = [];
          const alternativeNames = this.decisionData.alternatives.map(a => a.name);
          for (let importAlternativeIdx = 0; importAlternativeIdx < impactModelImportReturn.alternatives.length; importAlternativeIdx++) {
            const alternativeName = impactModelImportReturn.alternatives[importAlternativeIdx].name;
            const alternativeIdx = alternativeNames.indexOf(alternativeName);
            if (alternativeIdx >= 0) {
              const alternativeChanged = !this.decisionData.objectives.every((_, objectiveIdx) => {
                return (
                  this.areInfluenceFactorsEqual(alternativeIdx, objectiveIdx) &&
                  isEqual(
                    this.decisionData.outcomes[alternativeIdx][objectiveIdx].values,
                    impactModelImportReturn.outcomes[alternativeIdx][objectiveIdx].values,
                  )
                );
              });
              if (alternativeChanged) {
                /* DecisionData alternative found in Excel file and changed. */
                changeLog.push({ targetName: alternativeName, changeType: ChangeType.AlternativeChanged });
              } else {
                /* DecisionData alternative found in Excel file but not changed. */
                changeLog.push({ targetName: alternativeName, changeType: ChangeType.AlternativeUnchanged });
              }
            } else {
              changeLog.push({ targetName: alternativeName, changeType: ChangeType.AlternativeAdded });
            }
          }
          const importAlternativeNames = impactModelImportReturn.alternatives.map(a => a.name);
          for (const alternativeName of alternativeNames) {
            if (importAlternativeNames.indexOf(alternativeName) === -1) {
              /* DecisionData alternative not found in Excel file. */
              changeLog.push({ targetName: alternativeName, changeType: ChangeType.AlternativeNotFound });
            }
          }
          this.loadingImportSuccessful = true;
          changeLog.push(...this.checkInfluenceFactorsForChanges(this.decisionData, influenceFactorsImportReturn.influenceFactors));
          res({ messagePairs: this.removeDuplicatesFromChangeLog(changeLog) });
        } else {
          // ERRORS
          this.loadingImportSuccessful = false;
          const errorLog = impactModelImportReturn.errorLog.concat(influenceFactorsImportReturn.errorLog);
          res({ messagePairs: this.removeDuplicatesFromErrorLog(errorLog) });
        }
      };
      reader.onerror = e => rej(e);
      reader.readAsArrayBuffer(file);
    });
  }

  private removeDuplicatesFromChangeLog(pairs: ChangePair[]) {
    return pairs.filter((v, i, a) => a.findIndex(v2 => v2.targetName === v.targetName && v2.changeType === v.changeType) === i);
  }

  private removeDuplicatesFromErrorLog(pairs: ErrorPair[]) {
    return pairs.filter((v, i, a) => a.findIndex(v2 => v2.targetName === v.targetName && v2.errorType === v.errorType) === i);
  }

  /* Updates DecisionData after user confirmation. */
  executeImport() {
    if (this.loadingImportSuccessful) {
      // update InfluenceFactor references (otherwise rest of Navi doesn't recognize IF clones)
      this.importInfluenceFactorReferences.forEach(modifiedUF => {
        this.importOutcomes[modifiedUF.alternativeIdx][modifiedUF.objectiveIdx].influenceFactor = modifiedUF.influenceFactor;
      });
      this.addNewAlternatives();
      this.updateOutcomes();
      this.checkInfluenceFactorsForChanges(this.decisionData, this.importInfluenceFactors, true);
      this.snackBar.open($localize`Die Excel-Datei wurde erfolgreich importiert.`, 'Ok', { duration: 4000 });
    }
  }

  private addNewAlternatives() {
    if (this.importAlternatives.length > this.decisionData.alternatives.length) {
      // if new alternatives were added they are always at the end of the stack
      for (let alternativeIdx = this.decisionData.alternatives.length; alternativeIdx < this.importAlternatives.length; alternativeIdx++) {
        this.decisionData.addAlternative({ alternative: this.importAlternatives[alternativeIdx] });
      }
    }
  }

  private updateOutcomes() {
    for (let alternativeIdx = 0; alternativeIdx < this.importOutcomes.length; alternativeIdx++) {
      for (let objectiveIdx = 0; objectiveIdx < this.importOutcomes[alternativeIdx].length; objectiveIdx++) {
        const importOutcome = this.importOutcomes[alternativeIdx][objectiveIdx];
        this.decisionData.outcomes[alternativeIdx][objectiveIdx].values = importOutcome.values;
        this.decisionData.outcomes[alternativeIdx][objectiveIdx].influenceFactor = importOutcome.influenceFactor;
      }
    }
  }

  private resetImportData() {
    this.importAlternatives = [];
    this.importOutcomes = [];
    this.importInfluenceFactorReferences = [];
    this.importInfluenceFactors = [];
    this.loadingImportSuccessful = null;
  }

  /* Goes through InfluenceFactors and either checks for changes or updates values. */
  private checkInfluenceFactorsForChanges(
    decisionData: DecisionData,
    importInfluenceFactors: SimpleUserDefinedInfluenceFactor[],
    updateValue = false,
  ) {
    const changeLog = [];
    let importedInfluenceFactorIdx = 0;
    for (const influenceFactor of decisionData.influenceFactors) {
      if (influenceFactor instanceof CompositeUserDefinedInfluenceFactor) continue; // ignore composite influence factors

      const excelUf = importInfluenceFactors[importedInfluenceFactorIdx];
      let influenceFactorChanged = false;
      if (!(influenceFactor instanceof CompositeUserDefinedInfluenceFactor) && influenceFactor.precision !== excelUf.precision) {
        influenceFactorChanged = true;
        if (updateValue) {
          influenceFactor.precision = excelUf.precision;
        }
      }
      const states = influenceFactor.getStates();
      for (let stateIdx = 0; stateIdx < states.length; stateIdx++) {
        const excelUfState = excelUf.states[stateIdx];
        if (states[stateIdx].probability !== excelUfState.probability) {
          influenceFactorChanged = true;
          if (updateValue) {
            states[stateIdx].probability = excelUfState.probability;
          }
        }
      }
      if (influenceFactorChanged) {
        changeLog.push({
          targetName: InfluenceFactorNamePipe.prototype.transform(influenceFactor),
          changeType: ChangeType.InfluenceFactorChanged,
        });
      }
      importedInfluenceFactorIdx++;
    }
    return changeLog;
  }

  private areInfluenceFactorsEqual(alternativeIdx: number, objectiveIdx: number) {
    const modifiedInfluenceFactor = this.importInfluenceFactorReferences.find(
      uf => uf.alternativeIdx === alternativeIdx && uf.objectiveIdx === objectiveIdx,
    );
    if (modifiedInfluenceFactor == null) {
      return true; // influence factor wasn't changed
    }
    return (
      InfluenceFactorNamePipe.prototype.transform(this.decisionData.outcomes[alternativeIdx][objectiveIdx].influenceFactor) ===
      InfluenceFactorNamePipe.prototype.transform(modifiedInfluenceFactor.influenceFactor)
    );
  }
}
