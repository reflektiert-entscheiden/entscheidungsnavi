import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialog, MatDialogRef } from '@angular/material/dialog';
import { ConfirmModalComponent } from '@entscheidungsnavi/widgets';
import { DecisionData } from '@entscheidungsnavi/decision-data';
import { ChangePair, ChangeType, ErrorPair, ErrorType, ExcelImpactModelExportService } from '../excel-impact-model-export.service';
import { TrackingService } from '../../data/tracking.service';

@Component({
  templateUrl: './export-impact-model-modal.component.html',
  styleUrls: ['./export-impact-model-modal.component.scss'],
  providers: [ExcelImpactModelExportService],
})
export class ExportImpactModelModalComponent implements OnInit {
  includesDuplicateAlternatives = false; // in DecisionData

  showDragArea = false;
  dragCounter = 0;
  file: File;

  tabIdx = 0;
  unProtectSheet = false;

  readonly = false;

  changeMessages: Record<ChangeType, { title: string; icon: string; messages: string[] }> = {
    [ChangeType.AlternativeAdded]: { title: $localize`Folgende Alternativen werden neu erstellt`, icon: 'add', messages: [] },
    [ChangeType.AlternativeChanged]: { title: $localize`Folgende Alternativen enthalten Änderungen`, icon: 'edit', messages: [] },
    [ChangeType.AlternativeUnchanged]: {
      title: $localize`Folgende Alternativen enthalten keine Änderungen`,
      icon: 'edit_off',
      messages: [],
    },
    [ChangeType.AlternativeNotFound]: {
      title: $localize`Folgende Alternativen wurden beim Import nicht gefunden`,
      icon: 'search_off',
      messages: [],
    },
    [ChangeType.InfluenceFactorChanged]: { title: $localize`Folgende Einflussfaktoren enthalten Änderungen`, icon: 'edit', messages: [] },
  };
  errorMessages: Record<ErrorType, { title: string; messages: string[] }> = {
    [ErrorType.SheetMissing]: { title: $localize`Die folgenden Blätter fehlen in Deiner Excel-Datei`, messages: [] },
    [ErrorType.ObjectiveNameInvalid]: { title: $localize`Die Namen der folgenden Ziele stimmen nicht überein`, messages: [] },
    [ErrorType.ObjectiveScaleInvalid]: { title: $localize`Bei den Skalen der folgenden Ziele liegt ein Fehler vor`, messages: [] },
    [ErrorType.ObjectiveIndicatorRowsInvalid]: {
      title: $localize`Bei den Indikatoren der folgenden Ziele liegt ein Fehler vor`,
      messages: [],
    },
    [ErrorType.AlternativeDuplicate]: { title: $localize`Die folgenden Alternativen kommen mehrfach vor`, messages: [] },
    [ErrorType.AlternativeUnknownInfluenceFactor]: {
      title: $localize`Bei den folgenden Alternativen liegt ein unbekannter Einflussfaktor vor`,
      messages: [],
    },
    [ErrorType.AlternativeOrderFalse]: {
      title: $localize`Bei den folgenden Alternativen liegt eine unzulässige Reihenfolge der Werte`,
      messages: [],
    },
    [ErrorType.AlternativeValueOutsideOfBounds]: {
      title: $localize`Bei den folgenden Alternativen liegt einer der Werte außerhalb der entsprechenden Skala`,
      messages: [],
    },
    [ErrorType.InfluenceFactorUnknownInfluenceFactor]: {
      title: $localize`Die folgenden Einflussfaktoren wurden nicht erkannt`,
      messages: [],
    },
    [ErrorType.InfluenceFactorUnknownState]: {
      title: $localize`Bei den folgenden Einflussfaktoren liegen unbekannte Zustände vor`,
      messages: [],
    },
    [ErrorType.InfluenceFactorInvalidValue]: {
      title: $localize`Bei den folgenden Einflussfaktoren liegen unzulässige Werte vor`,
      messages: [],
    },
    [ErrorType.InfluenceFactorPrecisionsDontSumUp]: {
      title: $localize`Bei den folgenden Einflussfaktoren ist die Summe der Wahrscheinlichkeiten nicht gleich 100`,
      messages: [],
    },
  };

  constructor(
    public excelForecastExportService: ExcelImpactModelExportService,
    private decisionData: DecisionData,
    private dialog: MatDialog,
    public dialogRef: MatDialogRef<ExportImpactModelModalComponent>,
    private trackingService: TrackingService,
    @Inject(MAT_DIALOG_DATA) data: { readonly: boolean } = { readonly: false },
  ) {
    this.readonly = data.readonly;
  }

  ngOnInit() {
    const alternativeNames = this.decisionData.alternatives.map(a => a.name);
    this.includesDuplicateAlternatives = new Set(alternativeNames).size !== alternativeNames.length;
  }

  export() {
    this.excelForecastExportService.performExcelExport(!this.unProtectSheet);
    this.trackingService.trackEvent('excel export', { category: 'impact model' });
  }

  import() {
    if (this.excelForecastExportService.loadingImportSuccessful) {
      this.trackingService.trackEvent('excel import', { category: 'impact model' });
      this.excelForecastExportService.executeImport();
      this.dialogRef.close();
    }
  }

  async uploadFromInput(input: HTMLInputElement) {
    if (input.files.length !== 1) {
      return;
    }
    const file = input.files[0];
    if (file.name.split('.').pop() !== 'xlsx') {
      return;
    }
    this.file = file;

    input.value = '';

    this.attemptImport();
  }

  getEnumValues(enumObj: any) {
    return Object.keys(enumObj).filter(key => !isNaN(Number(enumObj[key])));
  }

  dragEnter() {
    this.dragCounter++;
    this.showDragArea = true;
  }

  dragLeave() {
    this.dragCounter--;

    if (this.dragCounter === 0) {
      this.showDragArea = false;
    }
  }

  dragOver(event: DragEvent) {
    event.preventDefault();
    event.dataTransfer.dropEffect = 'move';
  }

  drop(event: DragEvent) {
    this.showDragArea = false;
    this.dragCounter = 0;

    event.preventDefault();

    if (event.dataTransfer.items.length !== 1) {
      this.dialog.open(ConfirmModalComponent, {
        data: {
          title: $localize`Fehler`,
          prompt: $localize`Es kann nur eine Excel-Datei auf einmal geladen werden.`,
          buttonDeny: 'Okay',
          size: 'small',
        },
      });
      return;
    }

    const item = event.dataTransfer.items[0];
    if (item.kind !== 'file' || item.getAsFile().type !== 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet') {
      this.dialog.open(ConfirmModalComponent, {
        data: {
          title: $localize`Fehler`,
          prompt: $localize`Es können nur Excel-Dateien geladen werden.`,
          buttonDeny: 'Okay',
          size: 'small',
        },
      });
      return;
    }

    this.file = item.getAsFile();
    this.attemptImport();
  }

  async attemptImport() {
    for (const changeKey in this.changeMessages) {
      (this.changeMessages as any)[changeKey].messages = [];
    }
    for (const errorKey in this.errorMessages) {
      (this.errorMessages as any)[errorKey].messages = [];
    }
    const importResult = await this.excelForecastExportService.performExcelImport(this.file);
    const messagePairs = importResult.messagePairs;
    if (this.excelForecastExportService.loadingImportSuccessful) {
      for (const messagePair of messagePairs as ChangePair[]) {
        this.changeMessages[messagePair.changeType].messages.push(messagePair.targetName);
      }
    } else {
      for (const messagePair of messagePairs as ErrorPair[]) {
        this.errorMessages[messagePair.errorType].messages.push(
          messagePair.targetName + (messagePair.coordinates ? ' (' + messagePair.coordinates + ')' : ''),
        );
      }
    }
  }
}
