import { Alternative, calculateIndicatorValue, DecisionData, Objective, ObjectiveInput, Outcome } from '@entscheidungsnavi/decision-data';
import { InfluenceFactorNamePipe, StateNamePipe } from '@entscheidungsnavi/widgets';
import { Worksheet } from 'exceljs';
import { range } from 'lodash';
import {
  addNote,
  background,
  blueHeaderStyle,
  centerAndWrapText,
  defaultHeaderStyle,
  defaultMatrixStyle,
  fitObjectiveNameHeader,
  getHeaderRowNumber,
  getIndicatorScale,
  getNumericalScale,
  getObjectiveColumns,
  multiLine,
} from '../../excel-export/excel-helpers';

/* Generates Impact Model data adds it to the provided worksheet. */
export function performImpactModelExport(decisionData: DecisionData, ws: Worksheet, protectSheet: boolean, isEnglish: boolean) {
  const headerRowNumber = getHeaderRowNumber(decisionData.objectives);
  const alternativeRows = getAlternativeRowsExport(decisionData.outcomes);
  const objectiveColumns = getObjectiveColumns(decisionData.objectives);
  ws.properties.defaultRowHeight = 20;
  ws.properties.defaultColWidth = 20;
  ws.getRows(1, headerRowNumber).forEach(row => (row.height = 60));

  createOuterBorder(
    ws,
    headerRowNumber + 1,
    2,
    headerRowNumber + alternativeRows.reduce((a, b) => a + b, 0),
    objectiveColumns.reduce((a, b) => a + b, 0) + 1,
    'medium',
    'ff262626',
  );

  // Alternatives
  addAlternativeColumn(ws, headerRowNumber, decisionData.alternatives, alternativeRows);

  // Objectives
  addObjectiveHeaders(ws, headerRowNumber, decisionData.objectives, objectiveColumns, isEnglish);

  addForecastAndAlternativeStyle(ws, headerRowNumber, alternativeRows, objectiveColumns);
  addForecasts(
    ws,
    headerRowNumber,
    decisionData.alternatives,
    decisionData.objectives,
    decisionData.outcomes,
    alternativeRows,
    objectiveColumns,
  );

  ws.getCell(1, 1).value = { text: $localize`Bitte beachte die Hinweise unter README!`, hyperlink: "#'README'!A1" };
  ws.mergeCells(1, 1, headerRowNumber, 1);
  centerAndWrapText(ws.getCell(1, 1));

  fitObjectiveNameHeader(ws, decisionData.objectives, objectiveColumns);

  if (protectSheet) {
    protect(ws, headerRowNumber, alternativeRows, '');
  }
}

/* Calculates the number of rows for each alternative (for export). */
function getAlternativeRowsExport(outcomes: Outcome[][]): number[] {
  return outcomes.map(aRow =>
    aRow.reduce((prev, outcome) => {
      return Math.max(prev, outcome.influenceFactor?.stateCount + 1 || 1);
    }, 1),
  );
}

function createOuterBorder(
  ws: Worksheet,
  startRow: number,
  startCol: number,
  endRow: number,
  endCol: number,
  borderThickness: string,
  borderColor: string,
  directions: { top: boolean; right: boolean; bottom: boolean; left: boolean } = { top: true, right: true, bottom: true, left: true },
) {
  const borderStyle = {
    style: borderThickness,
    color: { argb: borderColor },
  };
  for (let i = startRow; i <= endRow; i++) {
    const leftBorderCell = ws.getCell(i, startCol);
    const rightBorderCell = ws.getCell(i, endCol);
    if (directions.left) {
      leftBorderCell.border = {
        ...leftBorderCell.border,
        left: borderStyle as any,
      };
    }
    if (directions.right) {
      rightBorderCell.border = {
        ...rightBorderCell.border,
        right: borderStyle as any,
      };
    }
  }

  for (let i = startCol; i <= endCol; i++) {
    const topBorderCell = ws.getCell(startRow, i);
    const bottomBorderCell = ws.getCell(endRow, i);
    if (directions.top) {
      topBorderCell.border = {
        ...topBorderCell.border,
        top: borderStyle as any,
      };
    }
    if (directions.bottom) {
      bottomBorderCell.border = {
        ...bottomBorderCell.border,
        bottom: borderStyle as any,
      };
    }
  }
}

function addAlternativeColumn(ws: Worksheet, headerRowNumber: number, alternatives: Alternative[], alternativeRows: number[]) {
  const col = ws.getColumn('A');
  col.width = 50;

  let rowNumber = headerRowNumber + 1;
  alternativeRows.forEach((length, index) => {
    const alternative = alternatives[index];
    const cell = ws.getCell(rowNumber, 1);

    cell.value = alternative.name;
    ws.mergeCells(rowNumber, 1, rowNumber + length - 1, 1);
    addNote(cell, alternative.comment);

    centerAndWrapText(cell);

    createOuterBorder(ws, rowNumber, 1, rowNumber + length - 1, 1, 'medium', 'ff262626');

    rowNumber += length;
  });
}

function addObjectiveHeaders(
  ws: Worksheet,
  headerRowNumber: number,
  objectives: Objective[],
  objectiveColumns: number[],
  isEnglish: boolean,
) {
  let objFirstCol = 2;
  objectiveColumns.forEach((colCount, objectiveIdx) => {
    createOuterBorder(ws, 1, objFirstCol, headerRowNumber, objFirstCol + colCount - 1, 'medium', 'ffffffff', {
      top: false,
      right: true,
      bottom: false,
      left: true,
    });
    const objective = objectives[objectiveIdx];
    if (colCount > 1) {
      ws.mergeCells(1, objFirstCol, 1, objFirstCol + colCount - 1);
      ws.mergeCells(2, objFirstCol, 2, objFirstCol + colCount - 1);
    } else {
      ws.getColumn(objFirstCol).width = 50;
    }

    const nameCell = ws.getCell(1, objFirstCol);
    blueHeaderStyle(nameCell);
    nameCell.value = {
      richText: [
        {
          font: { bold: true, color: { argb: '00FFFFFF' } },
          text: isEnglish ? `Objective ${objectiveIdx + 1}: ` : `Ziel ${objectiveIdx + 1}: `,
        },
        { font: { color: { argb: '00FFFFFF' } }, text: objective.name },
      ],
    };
    addNote(nameCell, objective.comment);

    const scaleCell = ws.getCell(2, objFirstCol);
    if (objective.isNumerical) {
      blueHeaderStyle(scaleCell, getNumericalScale(objective, isEnglish));
    } else if (objective.isVerbal) {
      let scaleString = $localize`Verbale Skala: `;
      objective.verbalData.options.forEach((option, optionIdx) => {
        if (optionIdx !== 0) scaleString += ', ';
        scaleString += `${optionIdx + 1} = "${option}"`;
      });
      blueHeaderStyle(scaleCell, scaleString);
    } else if (objective.isIndicator) {
      blueHeaderStyle(scaleCell, getIndicatorScale(isEnglish));
    }

    // grey influence factor header
    const cell = ws.getCell(3, objFirstCol);
    cell.value = { richText: [{ font: { bold: true, color: { argb: '00FFFFFF' } }, text: $localize`Einflussfaktor` }] };
    defaultHeaderStyle(cell);
    if (headerRowNumber === 4) {
      defaultHeaderStyle(ws.getCell(4, objFirstCol));
    }
    // grey indicator scale header
    const startCol = objFirstCol + 1;
    if (objective.isIndicator) {
      objective.indicatorData.indicators.forEach((indicator, indicatorIdx) => {
        const colNumber = startCol + indicatorIdx;
        const nameCell = centerAndWrapText(defaultHeaderStyle(ws.getCell(3, colNumber)));
        const scaleCell = centerAndWrapText(defaultHeaderStyle(ws.getCell(4, colNumber)));

        nameCell.value = multiLine([
          isEnglish ? `Indicator ${indicatorIdx + 1}` : `Indikator ${indicatorIdx + 1}`,
          indicator.name,
          indicator.isVerbalized ? $localize`qualitativ (gesperrt)` : $localize`numerisch`,
        ]);
        nameCell.value.richText[0].font = { bold: true, color: { argb: '00FFFFFF' } };
        nameCell.value.richText[2].font = { color: { argb: '00FFFFFF' } };
        nameCell.value.richText[4].font = { color: { argb: '00FFFFFF' }, italic: true };
        nameCell.alignment.vertical = 'top';
        addNote(nameCell, indicator.comment);
        scaleCell.value = isEnglish
          ? `from ${indicator.min} to ${indicator.max} (${indicator.unit})`
          : `von ${indicator.min} bis ${indicator.max} (${indicator.unit})`;
        scaleCell.font = { color: { argb: '00FFFFFF' } };
      });
    } else {
      // apply grey style on empty header cells in value column
      if (headerRowNumber >= 3) {
        defaultHeaderStyle(ws.getCell(3, startCol)); // colCount for influence factor offset
      }
      if (headerRowNumber == 4) {
        defaultHeaderStyle(ws.getCell(4, startCol));
      }
    }

    // left-most black border (header)
    if (objectiveIdx === 0) {
      for (let row = 1; row <= headerRowNumber; row++) {
        ws.getCell(row, 2).border.left = { style: 'medium', color: { argb: 'ff262626' } };
      }
    }
    // right-most black border (header)
    if (objectiveIdx === objectives.length - 1) {
      for (let row = 1; row <= headerRowNumber; row++) {
        ws.getCell(row, objFirstCol + 1).border.right = { style: 'medium', color: { argb: 'ff262626' } };
      }
    }

    objFirstCol += colCount;
  });
}

function addForecastAndAlternativeStyle(ws: Worksheet, headerRowNumber: number, alternativeRows: number[], objectiveColumns: number[]) {
  const rows = alternativeRows.reduce((p, c) => p + c, 0);
  const cols = objectiveColumns.reduce((p, c) => p + c, 0);
  for (let i = 1; i <= rows; i++) {
    for (let j = 1; j <= cols + 1; j++) {
      defaultMatrixStyle(ws.getCell(i + headerRowNumber, j));
    }
  }
}

function addForecasts(
  ws: Worksheet,
  headerRowNumber: number,
  alternatives: Alternative[],
  objectives: Objective[],
  outcomes: Outcome[][],
  alternativeRows: number[],
  objectiveColumns: number[],
) {
  let startCol = 2;
  objectives.forEach((objective, objectiveIdx) => {
    let startRow = headerRowNumber + 1;
    const isIndicator = objectives[objectiveIdx].isIndicator;
    alternatives.forEach((alternative, alternativeIdx) => {
      const maxNumberOfStates = maxNumberOfStatesInRow(alternativeIdx, objectives, outcomes);
      const outcome = outcomes[alternativeIdx][objectiveIdx];
      if (outcome.influenceFactor) {
        const influenceNameCell = ws.getCell(startRow, startCol);
        influenceNameCell.value = {
          richText: [{ font: { bold: true }, text: InfluenceFactorNamePipe.prototype.transform(outcome.influenceFactor) }],
        };
        range(outcome.influenceFactor.stateCount).forEach(stateIdx => {
          const rowNum = startRow + stateIdx + 1;
          ws.getCell(rowNum, startCol).value = StateNamePipe.prototype.transform(outcome.influenceFactor, stateIdx);
          setForecastValue(ws, rowNum, startCol + 1, objective, outcome.values[stateIdx]);
        });
        createOuterBorder(
          ws,
          startRow,
          startCol,
          startRow + maxNumberOfStates,
          startCol + 1 + (isIndicator ? objectives[objectiveIdx].indicatorData.indicators.length - 1 : 0),
          'medium',
          'ff262626',
        );
      } else {
        setForecastValue(ws, startRow, startCol + 1, objective, outcome.values[0]);
        let numberOfCellsToTheRight = 1;
        if (isIndicator) {
          numberOfCellsToTheRight += objectives[objectiveIdx].indicatorData.indicators.length - 1;
        }
        for (let currentColOffset = 0; currentColOffset <= numberOfCellsToTheRight; currentColOffset++) {
          // merge each forecast sub-column (occurs for indicator scales and columns including influence factors)
          ws.mergeCells(startRow, startCol + currentColOffset, startRow + maxNumberOfStates, startCol + currentColOffset);
        }
        createOuterBorder(
          ws,
          startRow,
          startCol,
          startRow + maxNumberOfStates,
          startCol + 1 + (isIndicator ? objectives[objectiveIdx].indicatorData.indicators.length - 1 : 0),
          'medium',
          'ff262626',
        );
      }
      startRow += alternativeRows[alternativeIdx];
    });
    startCol += objectiveColumns[objectiveIdx];
  });
}

function setForecastValue(ws: Worksheet, rowNum: number, startCol: number, objective: Objective, values: ObjectiveInput) {
  if (objective.isNumerical) {
    ws.getCell(rowNum, startCol).value = values[0][0];
    background(ws.getCell(rowNum, startCol), 'ffffff');
  } else if (objective.isVerbal) {
    ws.getCell(rowNum, startCol).value = values[0][0];
    background(ws.getCell(rowNum, startCol), 'ffffff');
  } else {
    objective.indicatorData.indicators.forEach((indicator, indicatorIdx) => {
      if (indicator.isVerbalized) {
        // verbal indicator
        ws.getCell(rowNum, startCol + indicatorIdx).value = calculateIndicatorValue(values[indicatorIdx], indicator);
        ws.getCell(rowNum, startCol + indicatorIdx).protection = { locked: true };
      } else {
        // normal indicator
        ws.getCell(rowNum, startCol + indicatorIdx).value = values[indicatorIdx][0];
      }
      background(ws.getCell(rowNum, startCol + indicatorIdx), 'ffffff');
    });
  }
}

/* Calculates the maximum number of states among the influence factors of an alternative. */
function maxNumberOfStatesInRow(alternativeIdx: number, objectives: Objective[], outcomes: Outcome[][]) {
  let maxNumberOfStates = 0;
  for (let objectiveIdx = 0; objectiveIdx < objectives.length; objectiveIdx++) {
    maxNumberOfStates = Math.max(maxNumberOfStates, outcomes[alternativeIdx][objectiveIdx].influenceFactor?.stateCount ?? 0);
  }
  return maxNumberOfStates;
}

function protect(ws: Worksheet, headerRowNumber: number, alternativeRows: number[], password = '', unprotectedRows = 1000) {
  ws.getCell(1, 1).protection = { locked: true };

  // add empty cells, so we can remove protection from these rows
  const unprotecedStart = headerRowNumber + 1;
  const totalRows = headerRowNumber + alternativeRows.reduce((p, c) => p + c, 0);
  for (let i = totalRows + 1; i < unprotecedStart + unprotectedRows; i++) {
    ws.getCell(i, 1).value = '';
  }

  ws.getRows(unprotecedStart, unprotectedRows).forEach(row => {
    row.eachCell(cell => {
      if (cell.protection == null) cell.protection = { locked: false }; // unlock values that are not already locked (verbal indicators)
    });
  });
  ws.protect(password, { deleteRows: true, insertRows: true, formatRows: true, formatColumns: true, formatCells: true });
}
