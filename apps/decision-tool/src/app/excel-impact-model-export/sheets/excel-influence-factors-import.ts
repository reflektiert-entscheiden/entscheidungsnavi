import { InfluenceFactorNamePipe, StateNamePipe } from '@entscheidungsnavi/widgets';
import { CompositeUserDefinedInfluenceFactor, SimpleUserDefinedInfluenceFactor } from '@entscheidungsnavi/decision-data';
import { DecisionData } from '@entscheidungsnavi/decision-data';
import { cloneDeep, range } from 'lodash';
import { Worksheet } from 'exceljs';
import { getCellValue } from '../../excel-export/excel-helpers';
import { ErrorPair, ErrorType } from '../excel-impact-model-export.service';

type ImportInfluenceFactorsReturnType = {
  influenceFactors: SimpleUserDefinedInfluenceFactor[];
  errorLog: ErrorPair[];
};

export function performInfluenceFactorImport(decisionData: DecisionData, ws: Worksheet): ImportInfluenceFactorsReturnType {
  let currentRowImport = 2;
  const errorLog: ErrorPair[] = [];
  const influenceFactors = cloneDeep(decisionData.getUserDefinedInfluenceFactors());

  // collect data from table and check validity
  for (const [influenceFactorIdx, influenceFactor] of influenceFactors.entries()) {
    if (influenceFactor instanceof CompositeUserDefinedInfluenceFactor) continue; // ignore composite influence factors

    if (ws.getCell(currentRowImport, 1).text !== InfluenceFactorNamePipe.prototype.transform(influenceFactor)) {
      errorLog.push({
        targetName: ws.getCell(currentRowImport, 1).text,
        coordinates: 'A' + currentRowImport,
        errorType: ErrorType.InfluenceFactorUnknownInfluenceFactor,
      });
    } else {
      const precision = parseInt(getCellValue(ws.getCell(currentRowImport, 4)).toString());
      if (isNaN(precision)) {
        errorLog.push({
          targetName: InfluenceFactorNamePipe.prototype.transform(influenceFactor),
          coordinates: 'D' + currentRowImport,
          errorType: ErrorType.InfluenceFactorInvalidValue,
        });
      } else {
        influenceFactors[influenceFactorIdx].precision = precision;
        let probabilitySum = 0,
          errorInStates = false;
        range(influenceFactor.stateCount).forEach(stateIdx => {
          if (ws.getCell(currentRowImport + stateIdx + 1, 2).text !== StateNamePipe.prototype.transform(influenceFactor, stateIdx)) {
            errorLog.push({
              targetName: InfluenceFactorNamePipe.prototype.transform(influenceFactor),
              coordinates: 'B' + (currentRowImport + stateIdx + 1),
              errorType: ErrorType.InfluenceFactorUnknownState,
            });
            errorInStates = true;
          } else {
            const probability = parseInt(getCellValue(ws.getCell(currentRowImport + stateIdx + 1, 3)).toString());
            if (isNaN(probability)) {
              errorLog.push({
                targetName: InfluenceFactorNamePipe.prototype.transform(influenceFactor),
                coordinates: 'C' + (currentRowImport + stateIdx + 1),
                errorType: ErrorType.InfluenceFactorInvalidValue,
              });
              errorInStates = true;
            } else {
              influenceFactors[influenceFactorIdx].states[stateIdx].probability = probability;
              probabilitySum += probability;
            }
          }
        });
        if (!errorInStates && probabilitySum !== 100) {
          errorLog.push({
            targetName: InfluenceFactorNamePipe.prototype.transform(influenceFactor),
            errorType: ErrorType.InfluenceFactorPrecisionsDontSumUp,
          });
        }
      }
    }
    currentRowImport += influenceFactor.stateCount + 1;
  }

  if (errorLog.length > 0) {
    return { influenceFactors: null, errorLog };
  } else {
    return { influenceFactors, errorLog: [] };
  }
}
