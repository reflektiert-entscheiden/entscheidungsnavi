import {
  Alternative,
  InfluenceFactor,
  Objective,
  ObjectiveInput,
  Outcome,
  PREDEFINED_INFLUENCE_FACTORS,
  PredefinedInfluenceFactor,
} from '@entscheidungsnavi/decision-data';
import { DecisionData } from '@entscheidungsnavi/decision-data';
import { InfluenceFactorNamePipe, StateNamePipe } from '@entscheidungsnavi/widgets';
import { Worksheet } from 'exceljs';
import { cloneDeep } from 'lodash';
import { ErrorPair, ErrorType } from '../excel-impact-model-export.service';
import {
  columnToLetter,
  getCellValue,
  getHeaderRowNumber,
  getIndicatorScale,
  getNumericalScale,
  getObjectiveColumns,
} from '../../excel-export/excel-helpers';

export type ModifiedInfluenceFactors = {
  influenceFactor: InfluenceFactor;
  alternativeIdx: number;
  objectiveIdx: number;
};

type ImportImpactModelReturnType = {
  alternatives: Alternative[];
  outcomes: Outcome[][];
  influenceFactors: ModifiedInfluenceFactors[];
  errorLog: ErrorPair[];
};
export function performImpactModelImport(decisionData: DecisionData, ws: Worksheet): ImportImpactModelReturnType {
  const headerRowNumber = getHeaderRowNumber(decisionData.objectives);
  const alternativeRows = getAlternativeRowsImport(ws, headerRowNumber);
  const objectiveColumns = getObjectiveColumns(decisionData.objectives);

  const predefinedInfluenceFactors = Object.entries(PREDEFINED_INFLUENCE_FACTORS).map(entry => entry[1]);

  const influenceFactors = (decisionData.influenceFactors as InfluenceFactor[]).concat(predefinedInfluenceFactors);

  const objectiveStructureErrors = checkObjectiveStructure(ws, decisionData.objectives, objectiveColumns);
  if (objectiveStructureErrors.length > 0) {
    // break further checks
    return {
      alternatives: null,
      outcomes: null,
      influenceFactors: null,
      errorLog: objectiveStructureErrors,
    };
  }

  // Clone alternatives and outcomes (except InfluenceFactors), so that data isn't modified without user confirmation
  const alternativesFromImport: Alternative[] = cloneDeep(decisionData.alternatives);
  const outcomesFromImport: Outcome[][] = cloneDeep(decisionData.outcomes);

  // Copy InfluenceFactor reference (otherwise cloned influence factors aren't recognized by the rest of Navi)
  outcomesFromImport.forEach((ocForAlt, alternativeIdx) =>
    ocForAlt.forEach((outcome, objectiveIdx) => {
      outcome.influenceFactor = decisionData.outcomes[alternativeIdx][objectiveIdx].influenceFactor;
    }),
  );

  let rowIdx = headerRowNumber + 1;
  const alternativeNames: string[] = [];
  // check for duplicate alternatives
  const duplicateAlternativeNameErrors = [];
  for (let alternativeRowIdx = 0; alternativeRowIdx < alternativeRows.length; alternativeRowIdx++) {
    const alternativeName = ws.getCell(rowIdx, 1).text;
    if (alternativeNames.includes(alternativeName)) {
      duplicateAlternativeNameErrors.push({
        targetName: alternativeName,
        errorType: ErrorType.AlternativeDuplicate,
      });
    }
    alternativeNames.push(alternativeName);
    rowIdx += alternativeRows[alternativeRowIdx];
  }
  if (duplicateAlternativeNameErrors.length > 0) {
    // break further checks
    return {
      alternatives: null,
      outcomes: null,
      influenceFactors: null,
      errorLog: duplicateAlternativeNameErrors,
    };
  }

  // save Influence Factors for each Outcome separately (attach to Outcome only after import is committed)
  const modifiedInfluenceFactors: ModifiedInfluenceFactors[] = [];
  const errorLog: ErrorPair[] = [];

  rowIdx = headerRowNumber + 1;
  for (let alternativeRowIdx = 0; alternativeRowIdx < alternativeRows.length; alternativeRowIdx++) {
    const alternativeName = ws.getCell(rowIdx, 1).text;
    // use alternative name as identifier (no duplicates by definition)
    let alternativeIdx = decisionData.alternatives.map(alternative => alternative.name).indexOf(alternativeName);
    if (alternativeIdx === -1) {
      // new alternative
      alternativesFromImport.push(new Alternative(0, alternativeName));
      outcomesFromImport.push(decisionData.objectives.map(objective => new Outcome(null, objective)));
      alternativeIdx = alternativesFromImport.length - 1;
    }
    let columnIdx = 1;
    for (let objectiveIdx = 0; objectiveIdx < decisionData.objectives.length; objectiveIdx++) {
      const objective = decisionData.objectives[objectiveIdx];
      const influenceFactor = getInfluenceFactorFromImport(ws, influenceFactors, rowIdx, columnIdx + 1);
      if (influenceFactor === null) {
        // there is an influence factor, but it's not properly given (name mismatch)
        errorLog.push({
          targetName: ws.getCell(rowIdx, 1).text,
          coordinates: columnToLetter(columnIdx + 1) + '' + rowIdx,
          errorType: ErrorType.AlternativeUnknownInfluenceFactor,
        });
        columnIdx += objectiveColumns[objectiveIdx];
        continue;
      } else if (influenceFactor !== undefined) {
        if (
          outcomesFromImport[alternativeIdx][objectiveIdx].influenceFactor == null ||
          InfluenceFactorNamePipe.prototype.transform(outcomesFromImport[alternativeIdx][objectiveIdx].influenceFactor) !==
            InfluenceFactorNamePipe.prototype.transform(influenceFactor)
        ) {
          // save InfluenceFactor reference, don't attach to Outcome yet
          modifiedInfluenceFactors.push({ influenceFactor, alternativeIdx, objectiveIdx });
        }
      }
      if (objective.isNumerical || objective.isVerbal) {
        // tries to import numerical/verbal value, collects errors that occur
        errorLog.push(
          ...handleNonIndicatorValue(ws, objective, outcomesFromImport, alternativeIdx, objectiveIdx, influenceFactor, rowIdx, columnIdx),
        );
      } else {
        // tries to import indicator value, collects errors that occur
        errorLog.push(
          ...handleIndicatorValue(ws, objective, outcomesFromImport, alternativeIdx, objectiveIdx, influenceFactor, rowIdx, columnIdx),
        );
      }
      columnIdx += objectiveColumns[objectiveIdx];
    }
    rowIdx += alternativeRows[alternativeRowIdx];
  }

  if (errorLog.length > 0) {
    // return error stack
    return {
      alternatives: null,
      outcomes: null,
      influenceFactors: null,
      errorLog: [...new Set(errorLog)],
    };
  }

  // no errors, return modified clone data
  return {
    alternatives: alternativesFromImport,
    outcomes: outcomesFromImport,
    influenceFactors: modifiedInfluenceFactors,
    errorLog: [],
  };
}

function handleIndicatorValue(
  ws: Worksheet,
  objective: Objective,
  outcomesFromImport: Outcome[][],
  alternativeIdx: number,
  objectiveIdx: number,
  influenceFactor: InfluenceFactor,
  rowIdx: number,
  columnIdx: number,
) {
  const currentErrors = [];
  if (influenceFactor !== undefined) {
    for (let stateIdx = 0; stateIdx < influenceFactor.stateCount; stateIdx++) {
      const currentObjectiveInput: ObjectiveInput = [];
      for (let indicatorIdx = 0; indicatorIdx < objective.indicatorData.indicators.length; indicatorIdx++) {
        const currentValue = getCellValue(ws.getCell(rowIdx + stateIdx + 1, columnIdx + 2 + indicatorIdx));
        if (
          typeof currentValue === 'number' &&
          isValueBetween(
            currentValue,
            objective.indicatorData.indicators[indicatorIdx].min,
            objective.indicatorData.indicators[indicatorIdx].max,
          )
        ) {
          if (objective.indicatorData.indicators[indicatorIdx].isVerbalized) {
            // ignore changes in values when indicator is qualitativ (verbal)
            currentObjectiveInput.push(cloneDeep(outcomesFromImport[alternativeIdx][objectiveIdx].values[stateIdx][indicatorIdx]));
          } else {
            currentObjectiveInput.push([currentValue]);
          }
        } else {
          currentErrors.push({
            targetName: ws.getCell(rowIdx, 1).text,
            coordinates: columnToLetter(columnIdx + 2 + indicatorIdx) + '' + (rowIdx + stateIdx + 1),
            errorType: ErrorType.AlternativeValueOutsideOfBounds,
          });
        }
      }
      outcomesFromImport[alternativeIdx][objectiveIdx].values[stateIdx] = currentObjectiveInput;
    }
    if (influenceFactor instanceof PredefinedInfluenceFactor) {
      for (let indicatorIdx = 0; indicatorIdx < objective.indicatorData.indicators.length; indicatorIdx++) {
        if (
          !isArrayInTheCorrectOrder(
            outcomesFromImport[alternativeIdx][objectiveIdx].values.map(v => v[indicatorIdx][0]),
            objective.indicatorData.indicators[indicatorIdx].max - objective.indicatorData.indicators[indicatorIdx].min,
          )
        ) {
          currentErrors.push({
            targetName: ws.getCell(rowIdx, 1).text,
            coordinates:
              columnToLetter(columnIdx + 2) +
              '' +
              (rowIdx + 1) +
              ' - ' +
              columnToLetter(columnIdx + 2) +
              '' +
              (rowIdx + influenceFactor.states.length),
            errorType: ErrorType.AlternativeOrderFalse,
          });
        }
      }
    }
  } else {
    const currentObjectiveInput: ObjectiveInput = [];
    for (let indicatorIdx = 0; indicatorIdx < objective.indicatorData.indicators.length; indicatorIdx++) {
      const currentValue = getCellValue(ws.getCell(rowIdx, columnIdx + 2 + indicatorIdx));
      if (
        typeof currentValue === 'number' &&
        isValueBetween(
          currentValue,
          objective.indicatorData.indicators[indicatorIdx].min,
          objective.indicatorData.indicators[indicatorIdx].max,
        )
      ) {
        if (objective.indicatorData.indicators[indicatorIdx].isVerbalized) {
          // ignore changes in values when indicator is qualitativ (verbal)
          currentObjectiveInput.push(cloneDeep(outcomesFromImport[alternativeIdx][objectiveIdx].values[0][indicatorIdx]));
        } else {
          currentObjectiveInput.push([currentValue]);
        }
      } else {
        currentErrors.push({
          targetName: ws.getCell(rowIdx, 1).text,
          coordinates: columnToLetter(columnIdx + 2 + indicatorIdx) + '' + rowIdx,
          errorType: ErrorType.AlternativeValueOutsideOfBounds,
        });
      }
    }
    outcomesFromImport[alternativeIdx][objectiveIdx].values[0] = currentObjectiveInput;
  }
  return currentErrors;
}

function handleNonIndicatorValue(
  ws: Worksheet,
  objective: Objective,
  outcomesFromImport: Outcome[][],
  alternativeIdx: number,
  objectiveIdx: number,
  influenceFactor: InfluenceFactor,
  rowIdx: number,
  columnIdx: number,
) {
  const currentErrors = [];
  let from, to;
  if (objective.isNumerical) {
    from = objective.numericalData.from;
    to = objective.numericalData.to;
  } else {
    from = 1;
    to = objective.verbalData.options.length;
  }
  if (influenceFactor != null) {
    for (let stateIdx = 0; stateIdx < influenceFactor.stateCount; stateIdx++) {
      const currentValue = getCellValue(ws.getCell(rowIdx + stateIdx + 1, columnIdx + 2));
      if (typeof currentValue === 'number' && isValueBetween(currentValue, to, from)) {
        outcomesFromImport[alternativeIdx][objectiveIdx].values[stateIdx] = [[currentValue]];
        if (
          influenceFactor instanceof PredefinedInfluenceFactor &&
          !isArrayInTheCorrectOrder(outcomesFromImport[alternativeIdx][objectiveIdx].values.flat(2), to - from)
        ) {
          currentErrors.push({
            targetName: ws.getCell(rowIdx, 1).text,
            coordinates:
              columnToLetter(columnIdx + 2) +
              '' +
              (rowIdx + stateIdx + 1) +
              ' - ' +
              columnToLetter(columnIdx + 2) +
              '' +
              (rowIdx + stateIdx + influenceFactor.states.length),
            errorType: ErrorType.AlternativeOrderFalse,
          });
        }
      } else {
        currentErrors.push({
          targetName: ws.getCell(rowIdx, 1).text,
          coordinates: columnToLetter(columnIdx + 2) + '' + (rowIdx + stateIdx + 1),
          errorType: ErrorType.AlternativeValueOutsideOfBounds,
        });
      }
    }
  } else {
    const currentValue = getCellValue(ws.getCell(rowIdx, columnIdx + 2));
    if (typeof currentValue === 'number' && isValueBetween(currentValue, to, from)) {
      outcomesFromImport[alternativeIdx][objectiveIdx].values[0] = [[currentValue]];
    } else {
      currentErrors.push({
        targetName: ws.getCell(rowIdx, 1).text,
        coordinates: columnToLetter(columnIdx + 2) + '' + rowIdx,
        errorType: ErrorType.AlternativeValueOutsideOfBounds,
      });
    }
  }
  return currentErrors;
}

/* Calculates the number of rows for each alternative (for import). Stops reading after {emptyRowsLimit} empty rows. */
function getAlternativeRowsImport(ws: Worksheet, headerRowNumber: number, alternativesLimit = 1000, emptyRowsLimit = 10): number[] {
  const alternativeRows = [];
  let row = headerRowNumber + 1;
  let currentMasterCellRows = 0;
  let emptyRows = 0;
  while (row < alternativesLimit) {
    if (emptyRows >= emptyRowsLimit) {
      alternativeRows.push(currentMasterCellRows);
      break;
    }
    const currentCell = ws.getCell(row, 1);
    if (currentCell.master.address === currentCell.address) {
      if (currentCell.value == null || currentCell.value === '') {
        emptyRows++;
        currentMasterCellRows++;
      } else {
        if (row > headerRowNumber + 1) {
          // save alternative row number
          alternativeRows.push(currentMasterCellRows);
        }
        emptyRows = 0;
        currentMasterCellRows = 1;
      }
    } else {
      currentMasterCellRows++;
      emptyRows++;
    }
    row++;
  }
  return alternativeRows;
}

/* Checks if there's an influence factor in the given cell. If there is, it checks whether it is valid. Returns IF reference. */
function getInfluenceFactorFromImport(ws: Worksheet, influenceFactors: InfluenceFactor[], rowIdx: number, colIdx: number) {
  const influenceFactorName = ws.getCell(rowIdx, colIdx).text;
  if (influenceFactorName === '') {
    return undefined; // no influence factor
  } else {
    const ufIdx = influenceFactors.map(uf => InfluenceFactorNamePipe.prototype.transform(uf)).indexOf(influenceFactorName);
    if (ufIdx === -1) {
      return null; // wrong influence factor (return error)
    } else {
      for (let stateIdx = 0; stateIdx < influenceFactors[ufIdx].stateCount; stateIdx++) {
        const stateName = ws.getCell(rowIdx + stateIdx + 1, colIdx).text;
        if (StateNamePipe.prototype.transform(influenceFactors[ufIdx], stateIdx) !== stateName) {
          return null; // wrong influence factor (return error)
        }
      }
      return influenceFactors[ufIdx];
    }
  }
}

/* Checks if the header structure has been changed (objective name, scale, indicators). */
function checkObjectiveStructure(ws: Worksheet, objectives: Objective[], objectiveColumns: number[]) {
  const errors = [];
  let columnIdx = 2;
  for (const [objectiveIdx, colCount] of objectiveColumns.entries()) {
    const objective = objectives[objectiveIdx];
    if (
      ws.getCell(1, columnIdx).text !== 'Ziel ' + (objectiveIdx + 1) + ': ' + objective.name &&
      ws.getCell(1, columnIdx).text !== 'Objective ' + (objectiveIdx + 1) + ': ' + objective.name
    ) {
      errors.push({
        targetName: objective.name,
        coordinates: columnToLetter(columnIdx) + '' + 1,
        errorType: ErrorType.ObjectiveNameInvalid,
      });
    }
    if (
      objective.isNumerical &&
      ws.getCell(2, columnIdx).text.includes(getNumericalScale(objective, true)) &&
      ws.getCell(2, columnIdx).text.includes(getNumericalScale(objective, false))
    ) {
      errors.push({
        targetName: objective.name,
        coordinates: columnToLetter(columnIdx) + '' + 2,
        errorType: ErrorType.ObjectiveScaleInvalid,
      });
    } else if (objective.isVerbal) {
      for (const [optionIdx, option] of objective.verbalData.options.entries()) {
        if (!ws.getCell(2, columnIdx).text.includes(optionIdx + 1 + ' = "' + option + '"')) {
          errors.push({
            targetName: objective.name,
            coordinates: columnToLetter(columnIdx) + '' + 2,
            errorType: ErrorType.ObjectiveScaleInvalid,
          });
          break;
        }
      }
    } else if (objective.isIndicator) {
      if (ws.getCell(2, columnIdx).text !== getIndicatorScale(true) && ws.getCell(2, columnIdx).text !== getIndicatorScale(false)) {
        errors.push({
          targetName: objective.name,
          coordinates: columnToLetter(columnIdx) + '' + 2,
          errorType: ErrorType.ObjectiveScaleInvalid,
        });
        for (const [indicatorIdx, indicator] of objective.indicatorData.indicators.entries()) {
          if (
            ws.getCell(3, columnIdx + indicatorIdx + 1).text !==
              'von ' + indicator.min + ' bis ' + indicator.max + ' (' + objective.numericalData.unit + ')' &&
            ws.getCell(3, columnIdx + indicatorIdx + 1).text !==
              'from ' + indicator.min + ' to ' + indicator.max + ' (' + objective.numericalData.unit + ')'
          ) {
            errors.push({
              targetName: objective.name,
              coordinates: columnToLetter(columnIdx + indicatorIdx + 1) + '' + 4,
              errorType: ErrorType.ObjectiveIndicatorRowsInvalid,
            });
            break;
          }
          if (
            ws.getCell(4, columnIdx + indicatorIdx + 1).text !== 'Indikator ' + (indicatorIdx + 1) + ' ' + indicator.name &&
            ws.getCell(4, columnIdx + indicatorIdx + 1).text !== 'Indicator ' + (indicatorIdx + 1) + ' ' + indicator.name
          ) {
            errors.push({
              targetName: objective.name,
              coordinates: columnToLetter(columnIdx + indicatorIdx + 1) + '' + 3,
              errorType: ErrorType.ObjectiveIndicatorRowsInvalid,
            });
            break;
          }
        }
      }
    }
    columnIdx += colCount;
  }
  return errors;
}

function isArrayInTheCorrectOrder(values: number[], limitsDiff: number) {
  return values.every((value, i) => {
    return i === 0 || value === values[i - 1] || Math.sign(value - values[i - 1]) === Math.sign(limitsDiff);
  });
}

function isValueBetween(value: any, from: number, to: number) {
  return value == null || (value <= to && value >= from && from <= to) || (value <= from && value >= to && from >= to);
}
