import { Component } from '@angular/core';
import { OnlineProjectsService } from '@entscheidungsnavi/api-client';
import { MatSnackBar } from '@angular/material/snack-bar';
import { catchError, EMPTY, filter, finalize, map, startWith, switchMap, tap } from 'rxjs';
import { Router } from '@angular/router';
import { noop } from 'lodash';
import { ProjectHistoryEntry } from '@entscheidungsnavi/api-types';
import { OnlineProject, ProjectService } from '../data/project';

@Component({
  selector: 'dt-project-history',
  templateUrl: './project-history.component.html',
  styleUrls: ['./project-history.component.scss'],
})
export class ProjectHistoryComponent {
  // The maximum number of ms between two group entries
  private readonly groupTimespanCutoff = 10 * 60_1000; // 10 minutes

  isRestoringBackup = false;
  hasError = false;

  projectData$ = this.projectService.project$.pipe(
    filter((project): project is OnlineProject => project instanceof OnlineProject),
    switchMap(project =>
      project.onSaved$.pipe(
        startWith(null),
        map(() => project),
      ),
    ),
    switchMap(project =>
      this.onlineProjectsService.getHistory(project.info.id).pipe(
        catchError(() => {
          this.hasError = true;
          return EMPTY;
        }),
        map(history => ({ project, groupedVersions: this.groupHistoryEntries(history.list.reverse()) })),
        tap(data => this.resetExpandedGroups(data.project, data.groupedVersions)),
      ),
    ),
  );

  expandedGroupIds = new Set<string>();

  constructor(
    private projectService: ProjectService,
    private onlineProjectsService: OnlineProjectsService,
    private snackBar: MatSnackBar,
    private router: Router,
  ) {}

  onClick(projectId: string, historyEntryId?: string) {
    this.projectService.loadOnlineProject(projectId, { historyEntryId, overrideLastUrl: this.router.url }).subscribe({
      error: noop,
    });
  }

  /**
   * Groups project versions. Two adjacent versions are grouped iff:
   * - They are both 'patch' versions, and
   * - Their timestamps are closer than the cutoff.
   *
   * @param entries - The project versions to group, sorted by timestamp descending (i.e., newest first).
   * @returns The grouped project versions.
   */
  private groupHistoryEntries(entries: ProjectHistoryEntry[]): ProjectHistoryEntry[][] {
    if (entries.length === 0) return [];

    const groups: ProjectHistoryEntry[][] = [[entries[0]]];

    for (let i = 1; i < entries.length; i++) {
      const currentEntry = entries[i];
      const currentGroup = groups.at(-1);
      const previousEntry = currentGroup.at(-1);

      if (
        currentEntry.saveType == null &&
        previousEntry.saveType == null &&
        previousEntry.versionTimestamp.getTime() - currentEntry.versionTimestamp.getTime() < this.groupTimespanCutoff
      ) {
        currentGroup.push(currentEntry);
      } else {
        groups.push([currentEntry]);
      }
    }

    return groups;
  }

  private resetExpandedGroups(project: OnlineProject, groups: ProjectHistoryEntry[][]) {
    this.expandedGroupIds.clear();

    for (const group of groups) {
      for (const entry of group) {
        if (entry.id === project.info.loadedHistoryEntry) {
          this.expandedGroupIds.add(group.at(-1).id);
        }
      }
    }
  }

  toggleGroup(groupId: string) {
    if (this.expandedGroupIds.has(groupId)) {
      this.expandedGroupIds.delete(groupId);
    } else {
      this.expandedGroupIds.add(groupId);
    }
  }

  restoreBackup() {
    this.isRestoringBackup = true;
    this.projectService
      .getProject(OnlineProject)
      .save('restore-backup')
      .pipe(finalize(() => (this.isRestoringBackup = false)))
      .subscribe({
        error: () => this.snackBar.open($localize`Fehler beim Wiederherstellen der Sicherung`, 'Ok'),
      });
  }
}
