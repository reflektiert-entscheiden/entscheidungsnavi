import { Component, Input, OnInit } from '@angular/core';
import { DecisionData } from '@entscheidungsnavi/decision-data';
import { ExcelExportService } from '../excel-export';
import { ProjectViewData } from './project-view-data';

@Component({
  selector: 'dt-project-view',
  templateUrl: 'project-view.component.html',
  styleUrls: ['project-view.component.scss'],
})
export class ProjectViewComponent implements OnInit {
  data: ProjectViewData;

  @Input() addShadow: boolean;

  constructor(
    protected decisionData: DecisionData,
    private excelExportService: ExcelExportService,
  ) {}

  ngOnInit() {
    this.data = new ProjectViewData(this.decisionData);
  }

  exportAsExcel(onlyExpanded: boolean) {
    this.excelExportService.performExcelExport(onlyExpanded, this.data);
  }
}
