import { isFinite, range } from 'lodash';
import { validateValue } from '@entscheidungsnavi/decision-data/validations';
import {
  applyWeightsToUtilityMatrix,
  calculateIndicatorValue,
  DecisionData,
  getAlternativeUtilities,
  getUtilityMatrixMeta,
  InfluenceFactorState,
  ObjectiveInput,
  ObjectiveType,
  Outcome,
  PREDEFINED_INFLUENCE_FACTORS,
  PredefinedInfluenceFactorId,
} from '@entscheidungsnavi/decision-data';
import { assertUnreachable } from '@entscheidungsnavi/tools/assert-unreachable';
import { normalizeContinuous } from '@entscheidungsnavi/tools';

export class ProjectViewData {
  /**
   * The objective weights, normalized to sum to 1.
   */
  normalizedWeights: number[];
  /**
   * All outcomes cloned and with their values filtered by validity. Invalid outcome
   * values are set to null. Validity is checked using decision_data validators.
   */
  filteredOutcomes: Outcome[][];
  /**
   * Minimal and maximal outcomes regarding the influence factors - [objectiveIdx][alternativeIdx][indicatorIdx].
   */
  minimalOutcomes: number[][][];
  maximalOutcomes: number[][][];
  /**
   * Utilities for every individual outcome.
   */
  totalOutcomeUtilities: number[][];
  /**
   * Total utilities for every alternative.
   */
  totalAlternativeUtilities: number[];
  /**
   * Utilities for each outcome for each influence factor state.
   *
   * The third index is the influence factor state. If there is no influence factor,
   * the third index only has one entry.
   */
  ifOutcomeUtilities: number[][][];
  /**
   * The results for each outcome for each influence factor state. (Only contains indicator objectives)
   *
   * The third index is the influence factor state. If there is no influence factor,
   * the third array only has one entry.
   */
  indicatorOutcomeResults: number[][][];
  /**
   * Minimal and maximal results for every outcome regarding the influence factor. (Only contains indicator objectives)
   */
  indicatorMinimalResults: number[][];
  indicatorMaximalResults: number[][];
  /**
   * The indices of the alternatives sorted by their total utility descending.
   * A utility of null/NaN is sorted to the bottom.
   */
  sortedAlternaticeIndices: number[];
  /**
   * Whether or not an alternative has at least one outcome with an influence factor.
   * The index corresponds to the indices in the sorted alternative list.
   */
  isAlternativeExpandable: boolean[];
  /**
   * Whether or not every alternative/every objective is expanded.
   */
  isAlternativeExpanded: boolean[];
  isObjectiveExpanded: boolean[];
  /**
   * The states of each influence factor, i.e., for each entry in decisionData.influenceFactors
   */
  influenceFactorStates: Map<PredefinedInfluenceFactorId | number, InfluenceFactorState[]>;

  constructor(private decisionData: DecisionData) {
    this.influenceFactorStates = new Map();
    this.decisionData.influenceFactors.forEach(influenceFactor =>
      this.influenceFactorStates.set(influenceFactor.id, influenceFactor.getStates()),
    );
    Object.values(PREDEFINED_INFLUENCE_FACTORS).forEach(influenceFactor =>
      this.influenceFactorStates.set(influenceFactor.id, influenceFactor.getStates()),
    );

    const weights = this.decisionData.weights.getWeightValues();
    normalizeContinuous(weights);
    this.normalizedWeights = weights;

    // Set all invalid outcomes to null
    this.filteredOutcomes = this.decisionData.outcomes.map(ocForAlternative => {
      return ocForAlternative.map((outcome, objectiveIdx) => {
        return new Outcome(
          outcome.values.map(value => {
            if (validateValue(value, this.decisionData.objectives[objectiveIdx])[0]) {
              return value;
            } else {
              return value.map(innerValues => innerValues.map(() => null));
            }
          }),
          undefined,
          outcome.influenceFactor,
        );
      });
    });

    this.minimalOutcomes = this.getMinMaxOutcomes('min');
    this.maximalOutcomes = this.getMinMaxOutcomes('max');

    this.totalOutcomeUtilities = getUtilityMatrixMeta(this.filteredOutcomes, this.decisionData.objectives);

    const tempWeightedUtilityMatrix = this.totalOutcomeUtilities.slice();
    applyWeightsToUtilityMatrix(tempWeightedUtilityMatrix, this.decisionData.weights.getWeightValues());
    this.totalAlternativeUtilities = getAlternativeUtilities(tempWeightedUtilityMatrix);

    this.indicatorMinimalResults = this.indicatorOutcomes('min');
    this.indicatorMaximalResults = this.indicatorOutcomes('max');

    // For results, we take c = 0. For utilities we take the set c.
    this.indicatorOutcomeResults = this.getIndicatorIfOutcomes();
    this.ifOutcomeUtilities = this.getUtilitesPerIfState();

    // Alternatives are sorted by utility. Not-processed/incomplete alternatives are sorted to the bottom.
    this.sortedAlternaticeIndices = this.totalAlternativeUtilities
      .map((utility, index) => ({ utility, index }))
      .sort((a, b) => {
        if (isFinite(a.utility)) {
          if (isFinite(b.utility)) {
            return b.utility - a.utility;
          } else {
            return -1;
          }
        } else if (isFinite(b.utility)) {
          return 1;
        } else {
          return 0;
        }
      })
      .map(({ index }) => index);

    this.isAlternativeExpandable = this.sortedAlternaticeIndices.map(index =>
      this.decisionData.outcomes[index].some(outcome => outcome.influenceFactor != null),
    );
    this.isAlternativeExpanded = this.decisionData.alternatives.map(() => false);
    this.isObjectiveExpanded = this.decisionData.objectives.map(() => false);
  }

  /**
   * Return true if for this objective there exists an outcome with an influence factor whose alternative is
   * expanded.
   *
   * @param objectiveIdx - the index of the objective
   */
  isAnyIfExpanded(objectiveIdx: number) {
    return this.isAlternativeExpanded.some(
      (entry, alternativeIdx) => entry && this.decisionData.outcomes[alternativeIdx][objectiveIdx].influenceFactor != null,
    );
  }

  /**
   * Return true if for this objective there exists an outcome with an influence factor.
   *
   * @param objectiveIndex - the index of the objective
   */
  anyInfluenceFactor(objectiveIndex: number) {
    return this.decisionData.alternatives.some(
      (alternative, alternativeIdx) => this.decisionData.outcomes[alternativeIdx][objectiveIndex].influenceFactor != null,
    );
  }

  /**
   * Compute the minimal outcomes concerning an influence factor. If there is no influence factor,
   * this exactly corresponds to the outcomes array.
   *
   * @param minMax - Whether to calculate the min (worst) or max (best) outcomes
   */
  private getMinMaxOutcomes(minMax: 'min' | 'max'): number[][][] {
    // [objectiveIdx][alternativeIdx][indicatorIdx]
    const higherIsBetterSelector = minMax === 'max' ? Math.max : Math.min;
    const lowerIsBetterSelector = minMax === 'max' ? Math.min : Math.max;

    // Replace all null-values with undefined. Math.min/max considers null as 0 which we want to prevent.
    const preprocessValues = (stateValues: ObjectiveInput[]) =>
      stateValues.map(indicatorValues => indicatorValues.map(values => values.map(v => (v != null ? v : undefined))));

    return this.filteredOutcomes.map(outcForAlternative => {
      return outcForAlternative.map((outcome, objectiveIdx) => {
        const obj = this.decisionData.objectives[objectiveIdx];
        const preprocessedValues = preprocessValues(outcome.values);

        switch (obj.objectiveType) {
          case ObjectiveType.Numerical: {
            return obj.numericalData.from < obj.numericalData.to
              ? [higherIsBetterSelector(...preprocessedValues.map(value => value[0][0]))]
              : [lowerIsBetterSelector(...preprocessedValues.map(value => value[0][0]))];
          }
          case ObjectiveType.Verbal: {
            return [higherIsBetterSelector(...preprocessedValues.map(value => value[0][0]))];
          }
          case ObjectiveType.Indicator: {
            return obj.indicatorData.indicators.map((indicator, indicatorIdx) => {
              const higherIsBetter = Math.sign(indicator.max - indicator.min) * Math.sign(indicator.coefficient);
              const indValues = preprocessedValues.map(value => calculateIndicatorValue(value[indicatorIdx], indicator));
              return higherIsBetter ? higherIsBetterSelector(...indValues) : lowerIsBetterSelector(...indValues);
            });
          }
          default:
            assertUnreachable(obj.objectiveType);
        }
      });
    });
  }

  /**
   * Calculate minimal & maximal results for an indicator objective from all influence factors
   */
  private indicatorOutcomes(get: 'min' | 'max'): number[][] {
    const getMin = get === 'min';

    return this.filteredOutcomes.map(ocsForAlternative => {
      return ocsForAlternative.map((outcome, objectiveIdx) => {
        const objective = this.decisionData.objectives[objectiveIdx];
        if (objective.isIndicator) {
          const aggregationFunction = objective.indicatorData.aggregationFunction;
          const aggregatedValues = outcome.values.map(value => aggregationFunction(value));
          return getMin ? Math.min(...aggregatedValues) : Math.max(...aggregatedValues);
        }
        return 0;
      });
    });
  }

  /**
   * Calculate results for every out come and for every state of each set influence factor. (For Indicator Objectives)
   * The first index is for the alternative, the second for the objective like for outcomes. The third index
   * is for the state of the influence factor set for this outcome. If there is no influence factor set, the
   * third array has only one entry.
   */
  private getIndicatorIfOutcomes() {
    return this.filteredOutcomes.map(ocsForAlternative => {
      return ocsForAlternative.map((outcome, objectiveIdx) => {
        const objective = this.decisionData.objectives[objectiveIdx];
        if (objective.isIndicator) {
          const aggregationFunction = objective.indicatorData.aggregationFunction;

          if (outcome.influenceFactor != null) {
            return range(outcome.influenceFactor.stateCount).map(stateIdx => {
              return aggregationFunction(outcome.values[stateIdx]);
            });
          } else {
            return [aggregationFunction(outcome.values[0])];
          }
        }
        return [0];
      });
    });
  }

  /**
   * Calculate utility values for every outcome and for every state of each set influence factor.
   * The first index is for the alternative, the second for the objective like for outcomes. The third index
   * is for the state of the influence factor set for this outcome. If there is no influence factor set, the
   * third array has only one entry.
   *
   * @param c - sets a fixed c used for all utility functions. If unset, the c of the objective is used.
   */
  private getUtilitesPerIfState(): number[][][] {
    const utilityFunctions = this.decisionData.objectives.map(objective => objective.getUtilityFunction());

    // We use the filteredOutcomes for the values
    return this.filteredOutcomes.map(ocsForAlternative => {
      return ocsForAlternative.map((outcome, objectiveIdx) => {
        const uf = utilityFunctions[objectiveIdx];
        if (outcome.influenceFactor != null) {
          return range(outcome.influenceFactor.stateCount).map(stateIdx => {
            return uf(outcome.values[stateIdx]);
          });
        } else {
          return [uf(outcome.values[0])];
        }
      });
    });
  }
}
