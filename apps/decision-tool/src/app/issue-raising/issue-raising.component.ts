import { Component, ElementRef, OnInit, QueryList, ViewChild, ViewChildren } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { MatRipple } from '@angular/material/core';
import { AspectBoxComponent, ConfirmModalComponent, ConfirmModalData, PopOverService } from '@entscheidungsnavi/widgets';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import {
  Alternative,
  DecisionData,
  InfluenceFactor,
  ISSUE_CATEGORIES,
  IssueCategory,
  Objective,
  ObjectiveElement,
  SimpleUserDefinedInfluenceFactor,
} from '@entscheidungsnavi/decision-data';
import { EducationalNavigationService } from '../../modules/shared/navigation/educational-navigation.service';
import { LanguageService } from '../data/language.service';
import { urlToEducationalSubStep } from '../../modules/shared/navigation/educational-navigation';
import { IssueRaisingEventsService } from './issue-raising-events.service';

type FooterElements =
  | 'delete'
  | 'objective'
  | 'alternative'
  | 'influenceFactor'
  | 'decisionStatement'
  | 'decisionStatement_preNotes'
  | 'decisionStatement_afterNotes'
  | 'objective_brainstorming';
@Component({
  templateUrl: './issue-raising.component.html',
  styleUrls: ['./issue-raising.component.scss'],
})
export class IssueRaisingComponent implements OnInit {
  objectiveExpanded: boolean[];
  issueCategories = ISSUE_CATEGORIES;
  @ViewChild('addingNew') addingNewElement: ElementRef;
  @ViewChildren(AspectBoxComponent) elements: QueryList<AspectBoxComponent>;

  headerElements: {
    category: IssueCategory;
    labelNotdragging: string;
    labelDragging: string;
  }[] = [
    {
      category: 'notAssigned',
      labelNotdragging: $localize`Nicht zugeordnet`,
      labelDragging: $localize`aus aktuellem Pool entfernen`,
    },
    {
      category: 'decisionStatement',
      labelNotdragging: $localize`Entscheidungsfrage`,
      labelDragging: $localize`zum Entscheidungsfrage-Pool hinzufügen`,
    },
    {
      category: 'objectives',
      labelNotdragging: $localize`Ziele`,
      labelDragging: $localize`zum Ziele-Pool hinzufügen`,
    },
    {
      category: 'alternatives',
      labelNotdragging: $localize`Alternativen`,
      labelDragging: $localize`zum Alternativen-Pool hinzufügen`,
    },
    {
      category: 'influenceFactors',
      labelNotdragging: $localize`Einflussfaktoren`,
      labelDragging: $localize`zum Einflussfaktoren-Pool hinzufügen`,
    },
  ];
  footerElements: {
    category: IssueCategory;
    id: FooterElements;
    labelNotdragging: string;
    labelDragging: string;
    tooltip: string;
  }[] = [
    {
      category: 'decisionStatement',
      id: 'decisionStatement',
      labelNotdragging: $localize`Entscheidungsfrage`,
      labelDragging: $localize`als Entscheidungsfrage übernehmen`,
      tooltip: $localize`Erfolgreich als Entscheidungsfrage übernommen`,
    },
    {
      category: 'decisionStatement',
      id: 'decisionStatement_preNotes',
      labelNotdragging: $localize`Annahme`,
      labelDragging: $localize`als Annahme übernehmen`,
      tooltip: $localize`Erfolgreich als Annahme übernommen`,
    },
    {
      category: 'decisionStatement',
      id: 'decisionStatement_afterNotes',
      labelNotdragging: $localize`spätere Entscheidung`,
      labelDragging: $localize`als spätere Entscheidung übernehmen`,
      tooltip: $localize`Erfolgreich als spätere Entscheidung übernommen`,
    },
    {
      category: 'objectives',
      id: 'objective',
      labelNotdragging: $localize`Fundamentalziel`,
      labelDragging: $localize`als Fundamentalziel übernehmen`,
      tooltip: 'Erfolgreich als Fundamentalziel übernommen',
    },
    {
      category: 'objectives',
      id: 'objective_brainstorming',
      labelNotdragging: $localize`Brainstorming-Aspekt`,
      labelDragging: $localize`als Brainstorming-Aspekt übernehmen`,
      tooltip: $localize`Erfolgreich als Brainstorming-Aspekt übernommen`,
    },
    {
      category: 'alternatives',
      id: 'alternative',
      labelNotdragging: $localize`Alternative`,
      labelDragging: $localize`als Alternative übernehmen`,
      tooltip: $localize`Erfolgreich als Alternative übernommen`,
    },
    {
      category: 'influenceFactors',
      id: 'influenceFactor',
      labelNotdragging: $localize`Einflussfaktor`,
      labelDragging: $localize`als Einflussfaktor übernehmen`,
      tooltip: $localize`Erfolgreich als Einflussfaktor übernommen`,
    },
  ];

  activeCategory: IssueCategory = 'notAssigned';
  issues: Record<IssueCategory, string[]>;
  newIssue: string;
  isDragging = false;
  isAddingNew = false;
  oldIndexCurDragged = 0;
  indexCurDragged = 0;
  dragOverCategory: IssueCategory = null;
  overFooterElement: FooterElements = null;
  enterTarget: EventTarget = null;

  constructor(
    private router: Router,
    public dialogRef: MatDialogRef<IssueRaisingComponent>,
    private dialog: MatDialog,
    private snackBar: MatSnackBar,
    public educationalNavigationService: EducationalNavigationService,
    protected decisionData: DecisionData,
    private popOverService: PopOverService,
    private languageService: LanguageService,
    private issueRaisingEventsService: IssueRaisingEventsService,
  ) {}

  addNewElementField(): void {
    this.isAddingNew = true;
  }
  addElement(isEnter: boolean): void {
    this.isAddingNew = false;
    if (!this.newIssue) {
      return;
    }
    this.decisionData.issues[this.activeCategory].push(this.newIssue);
    this.newIssue = '';
    if (isEnter) {
      this.addNewElementField();
    }
  }

  onNameChange(event: string, i: number): void {
    //event is '' if the content was deleted
    if (event === '') {
      this.decisionData.issues[this.activeCategory].splice(i, 1);
    }
  }
  isDraggingBox(i: number): boolean {
    return this.isDragging && this.indexCurDragged === i;
  }

  //used for all areas where objects can be dropped
  dragEnter(event: DragEvent) {
    this.enterTarget = event.target;
  }

  //funktions for drag and drop issue raising box
  dragStartIssueRaisingBox(event: DragEvent, index: number): void {
    event.dataTransfer.effectAllowed = 'move';
    //set drag image to fix it is not visible in safari
    const aspectBox = event.target as HTMLElement;
    const aspectBoxInnerContainer = aspectBox.children[0].children[0] as HTMLElement;
    // to update drag image in chrome and firefox
    aspectBoxInnerContainer.style.opacity = '0.99';
    event.dataTransfer.setDragImage(aspectBoxInnerContainer, event.offsetX, event.offsetY);
    event.dataTransfer.setData('text/plain', this.elements.get(index).name || ' ');
    this.indexCurDragged = index;
    this.oldIndexCurDragged = index;
    this.isDragging = true;
  }
  dragOverIssueRaisingBox(event: DragEvent, index: number): void {
    event.preventDefault();
    if (this.indexCurDragged !== index) {
      this.issues[this.activeCategory].splice(index, 0, ...this.issues[this.activeCategory].splice(this.indexCurDragged, 1));
      this.indexCurDragged = index;
    }
    //sometimes the drag leave event is not fired, so we need to reset the index
    this.overFooterElement = null;
    this.dragOverCategory = null;
  }
  dragLeaveIssueRaisingBox(event: DragEvent): void {
    event.preventDefault();
    if (this.enterTarget === event.target) {
      this.decisionData.issues[this.activeCategory].splice(
        this.oldIndexCurDragged,
        0,
        ...this.issues[this.activeCategory].splice(this.indexCurDragged, 1),
      );
      this.indexCurDragged = this.oldIndexCurDragged;
    }
  }
  dropIssueRaisingBox(): void {
    this.isDragging = false;
  }
  dragEndIssueRaisingBox(): void {
    this.isDragging = false;
  }

  //functions for drag and drop taps
  dragOverTap(event: DragEvent, category: IssueCategory) {
    event.preventDefault();
    this.dragOverCategory = category;
  }
  dragLeaveTap(event: DragEvent) {
    if (this.enterTarget === event.target) {
      this.dragOverCategory = null;
    }
  }
  dropTap(event: DragEvent, category: IssueCategory, ripple: MatRipple) {
    //add to new list
    this.decisionData.issues[category].push(event.dataTransfer.getData('text/plain').toString());
    //delete from old list
    this.decisionData.issues[this.activeCategory].splice(this.indexCurDragged, 1);
    //reset
    ripple.launch({ centered: true, color: 'rgba(248, 147, 31, 0.5)' });
    this.isDragging = false;
    this.dragOverCategory = null;
  }

  //funktions for drag and drop footer areas
  dragOverFooter(event: DragEvent, area: FooterElements) {
    event.preventDefault();
    this.overFooterElement = area;
  }
  dragLeaveFooter(event: DragEvent) {
    if (this.enterTarget === event.target) {
      this.overFooterElement = null;
    }
  }
  dropFooter(event: DragEvent, area: FooterElements, ripple: MatRipple, tooltip: string) {
    const issue = event.dataTransfer.getData('text/plain').toString();
    if (issue === ' ' && area !== 'delete') {
      this.snackBar.open($localize`Bitte trag etwas in das Element ein`, 'OK', { duration: 5000 });
      this.overFooterElement = null;
      this.isDragging = false;
      return;
    }
    switch (area) {
      case 'delete': {
        this.decisionData.issues[this.activeCategory].splice(this.indexCurDragged, 1);
        break;
      }
      case 'objective': {
        const objective = new Objective(issue);
        objective.aspects.value.createdInSubStep = 1;
        this.decisionData.addObjective(objective);
        this.issueRaisingEventsService.objectiveAdded$.next(this.decisionData.objectives.length - 1);
        this.decisionData.issues[this.activeCategory].splice(this.indexCurDragged, 1);
        this.resetViewToImpactModel();
        break;
      }
      case 'objective_brainstorming': {
        const brainstorming = new ObjectiveElement(issue);
        brainstorming.createdInSubStep = 1;
        this.decisionData.objectiveAspects.listOfAspects.push(brainstorming);
        this.decisionData.objectiveAspectAdded$.next();
        this.decisionData.issues[this.activeCategory].splice(this.indexCurDragged, 1);
        break;
      }
      case 'alternative': {
        const alternative = new Alternative(1, issue);
        this.decisionData.addAlternative({ alternative });
        this.decisionData.issues[this.activeCategory].splice(this.indexCurDragged, 1);
        this.resetViewToImpactModel();
        break;
      }
      case 'decisionStatement': {
        const data = issue;
        this.dialog
          .open<ConfirmModalComponent, ConfirmModalData>(ConfirmModalComponent, {
            data: {
              title: $localize`Entscheidungsfrage überschreiben`,
              prompt: $localize`Bist Du sicher, dass Du die Entscheidungsfrage überschreiben willst?`,
              buttonConfirm: $localize`Ja, überschreiben`,
              buttonConfirmColor: 'accent',
              buttonDeny: $localize`Nein, abbrechen`,
            },
          })
          .afterClosed()
          .subscribe(result => {
            if (result) {
              this.decisionData.decisionStatement.statement = data;

              this.decisionData.issues[this.activeCategory].splice(this.indexCurDragged, 1);
              // this.decisionData.removeIssue(this.indexCurDragged, this.activeCategory);
              const source = document.getElementById('dt-footer-element-' + area);
              this.popOverService.whistle(source, tooltip);
            }
          });
        break;
      }
      case 'decisionStatement_preNotes': {
        this.decisionData.decisionStatement.preNotesFinal.push(issue);
        this.decisionData.issues[this.activeCategory].splice(this.indexCurDragged, 1);
        break;
      }
      case 'decisionStatement_afterNotes': {
        this.decisionData.decisionStatement.afterNotesFinal.push(issue);
        this.decisionData.issues[this.activeCategory].splice(this.indexCurDragged, 1);
        break;
      }
      case 'influenceFactor': {
        const influencefactor: InfluenceFactor = new SimpleUserDefinedInfluenceFactor(issue, [
          { name: $localize`Zustand 1`, probability: 50 },
          { name: $localize`Zustand 2`, probability: 50 },
        ]);
        this.decisionData.addInfluenceFactor(influencefactor);
        this.decisionData.issues[this.activeCategory].splice(this.indexCurDragged, 1);
        break;
      }
    }
    this.overFooterElement = null;
    if (area !== 'delete' && area !== 'decisionStatement') {
      const source = document.getElementById('dt-footer-element-' + area);
      this.popOverService.whistle(source, tooltip);
    }
    ripple.launch({ centered: true, color: area === 'delete' ? 'rgba(248, 0, 0, 0.5)' : 'rgba(248, 147, 31, 0.5)' });
    this.isDragging = false;
  }

  resetViewToImpactModel() {
    if (
      this.decisionData.projectMode === 'educational' &&
      (urlToEducationalSubStep(this.router.url).step === 'results' || urlToEducationalSubStep(this.router.url).step === 'finishProject')
    ) {
      this.snackBar.open($localize`Die Ansicht wird auf das Wirkungsmodell zurückgesetzt, da nun Daten fehlen.`, 'Ok');
      this.educationalNavigationService.navigateToStep('impactModel');
    } else if (this.decisionData.projectMode === 'professional' && this.router.url.includes('evaluate-and-decide')) {
      this.snackBar.open($localize`Die Ansicht wird auf das Wirkungsmodell zurückgesetzt, da nun Daten fehlen.`, 'Ok');
      this.router.navigateByUrl('/professional/structure-and-estimate', { onSameUrlNavigation: 'reload' });
    }
  }

  ngOnInit(): void {
    this.issues = this.decisionData.issues;
    this.addNewElementField();
  }
}
