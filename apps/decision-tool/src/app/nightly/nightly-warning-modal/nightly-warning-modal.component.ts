import { Component } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { AppSettingsService } from '../../data/app-settings.service';

@Component({
  templateUrl: './nightly-warning-modal.component.html',
  styleUrls: ['./nightly-warning-modal.component.scss'],
})
export class NightlyWarningModalComponent {
  get dns() {
    return !this.appSettings.showNightlyWarning;
  }
  set dns(value: boolean) {
    this.appSettings.showNightlyWarning = !value;
  }

  constructor(
    public dialogRef: MatDialogRef<NightlyWarningModalComponent>,
    private appSettings: AppSettingsService,
  ) {}

  openNormalVersion() {
    window.open('https://enavi.app', '_blank', 'noopener');
  }
}
