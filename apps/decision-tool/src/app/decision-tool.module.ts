/** angular imports */
import { DatePipe, DecimalPipe, registerLocaleData } from '@angular/common';
import { provideHttpClient, withInterceptorsFromDi } from '@angular/common/http';
import localeDe from '@angular/common/locales/de';
import { APP_INITIALIZER, ErrorHandler, InjectionToken, NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { MAT_SNACK_BAR_DEFAULT_OPTIONS } from '@angular/material/snack-bar';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { Angulartics2Module } from 'angulartics2';
import {
  AutoFocusDirective,
  ChangeDetectionInfoComponent,
  ElementRefDirective,
  HoverPopOverDirective,
  InfluenceFactorNamePipe,
  NoteBtnComponent,
  NoteBtnPresetPipe,
  RichTextEditorComponent,
  RichTextEmptyPipe,
  SafeNumberPipe,
  StateNamePipe,
  TagInputComponent,
  TagListComponent,
  TimeAgoPipe,
  WidthTriggerDirective,
} from '@entscheidungsnavi/widgets';
import { OverlayContainer } from '@angular/cdk/overlay';
import { ServiceWorkerModule } from '@angular/service-worker';
import { MAT_FORM_FIELD_DEFAULT_OPTIONS } from '@angular/material/form-field';
import { MAT_TOOLTIP_DEFAULT_OPTIONS, MatTooltipDefaultOptions } from '@angular/material/tooltip';
import { DecisionData } from '@entscheidungsnavi/decision-data';
import * as Sentry from '@sentry/angular';
import { Router } from '@angular/router';
import { ClipboardModule } from '@angular/cdk/clipboard';
import { ApiClientModule } from '@entscheidungsnavi/api-client';
import { Platform } from '@angular/cdk/platform';
import { ENVIRONMENT } from '../environments/environment';
import { SharedModule } from '../modules/shared/shared.module';
import { ServiceWorkerService } from '../services/service-worker.service';
import { ExplanationService } from '../modules/shared/decision-quality';
import { GlobalErrorHandler } from './data/global-error-handler';
import { GlobalErrorService } from './data/global-error.service';
import { TrackingService } from './data/tracking.service';
import { DecisionToolComponent } from './decision-tool.component';
import { DecisionToolRoutingModule } from './decision-tool.routing';
import { QuickstartComponent } from './main/quickstart';
import { NewProjectModalComponent, StartComponent } from './main/start';
import { LoginModalComponent, RegisterModalComponent, ResetPasswordModalComponent, UserareaComponent } from './main/userarea';
import { NavigationComponent, ProjectListModalComponent, ProjectsModalComponent, SaveAsModalComponent } from './navigation';
import { DebugComponent } from './debug/debug.component';
import { TimeTrackingService } from './data/time-tracking.service';
import { DecisionDataExportService } from './data/decision-data-export.service';
import { ProjectViewComponent } from './project-view';
import { PdfExportModalComponent } from './pdf-export-modal/pdf-export-modal.component';
import { PdfExportService } from './pdf-export-modal/pdf-export/pdf-export.service';
import { DynamicFullscreenOverlayContainer } from './dynamic-fullscreen-overlay/dynamic-fullscreen-overlay-container';
import { ExcelExportLoadingModalComponent } from './excel-export';
import { GradualProgressionWarningModalComponent } from './guards';
import { HelpComponent } from './help/help.component';
import { NavigationLayoutComponent } from './navigation-layout/navigation-layout.component';
import { HelpPagePortalPipe } from './help/help-page-portal.pipe';
import { FilteredSubStepsPipe } from './navigation/filtered-sub-steps.pipe';
import { AppSettingsModalComponent } from './main/app-settings/app-settings-modal.component';
import { ProjectInformationComponent } from './project-information/project-information.component';
import { NightlyWarningModalComponent } from './nightly/nightly-warning-modal/nightly-warning-modal.component';
import { NightlyImportModalComponent } from './nightly/nightly-import-modal/nightly-import-modal.component';
import { TransferNotificationComponent, TransferService } from './transfer';
import { ProjectLinkModalComponent } from './navigation/projects/project-link-modal/project-link-modal.component';
import { CypressService } from './data/cypress.service';
import { TimeTrackingComponent } from './time-tracking/time-tracking.component';
import { AboutUsModalComponent } from './about-us-modal/about-us-modal.component';
import { ProjectAreaComponent } from './project-area/project-area.component';
import { RenameProjectModalComponent } from './navigation/projects/rename-project-modal/rename-project-modal.component';
import { FinishProjectModule } from './main/finish-project/finish-project.module';
import { ExportImpactModelModalComponent } from './excel-impact-model-export/export-modal/export-impact-model-modal.component';
import { EventModalComponent, EventsOverviewModalComponent } from './main/userarea/events';
import { IssueRaisingComponent } from './issue-raising/issue-raising.component';
import { ProjectConflictModalComponent } from './project-conflict-modal/project-conflict-modal.component';
import { UnloadProjectModalComponent } from './unload-project-modal/unload-project-modal.component';
import { RecentlyEditedProjectsComponent } from './navigation/projects/recently-edited-projects/recently-edited-projects.component';
import { ProjectHistoryComponent } from './project-history/project-history.component';
import { ProjectLoadingPageComponent } from './project-loading/project-loading-page.component';
import { ProjectLoadingModalComponent } from './project-loading/project-loading-modal.component';
import { YoutubeVideosService } from './data/youtube-videos.service';

// set the default locale
registerLocaleData(localeDe, 'de');

function setGlobals() {
  return () => (window.dt = { lastError: null as unknown });
}

export const COCKPIT_ORIGIN = new InjectionToken('Origin of the cockpit.');

@NgModule({
  declarations: [
    AboutUsModalComponent,
    LoginModalComponent,
    AppSettingsModalComponent,
    DebugComponent,
    DecisionToolComponent,
    ExcelExportLoadingModalComponent,
    ExportImpactModelModalComponent,
    FilteredSubStepsPipe,
    GradualProgressionWarningModalComponent,
    HelpComponent,
    HelpPagePortalPipe,
    NavigationComponent,
    NavigationLayoutComponent,
    NewProjectModalComponent,
    NightlyImportModalComponent,
    NightlyWarningModalComponent,
    PdfExportModalComponent,
    ProjectAreaComponent,
    ProjectInformationComponent,
    ProjectLinkModalComponent,
    ProjectListModalComponent,
    ProjectViewComponent,
    ProjectsModalComponent,
    QuickstartComponent,
    RegisterModalComponent,
    RenameProjectModalComponent,
    ResetPasswordModalComponent,
    SaveAsModalComponent,
    StartComponent,
    TransferNotificationComponent,
    TimeTrackingComponent,
    UserareaComponent,
    EventsOverviewModalComponent,
    EventModalComponent,
    IssueRaisingComponent,
    ProjectConflictModalComponent,
    ProjectHistoryComponent,
    UnloadProjectModalComponent,
    RecentlyEditedProjectsComponent,
    ProjectLoadingPageComponent,
    ProjectLoadingModalComponent,
  ],
  bootstrap: [DecisionToolComponent],
  imports: [
    Angulartics2Module.forRoot({ pageTracking: { clearQueryParams: true } }),
    BrowserAnimationsModule,
    BrowserModule,
    DecisionToolRoutingModule,
    FinishProjectModule,
    ReactiveFormsModule,
    ServiceWorkerModule.register('ngsw-worker.js', {
      enabled: ENVIRONMENT.type === 'nightly' || ENVIRONMENT.type === 'production',
      // Register the ServiceWorker as soon as the app is stable
      // or after 30 seconds (whichever comes first).
      registrationStrategy: 'registerWhenStable:30000',
    }),
    ApiClientModule,
    SharedModule,
    RichTextEditorComponent,
    HoverPopOverDirective,
    NoteBtnPresetPipe,
    NoteBtnComponent,
    WidthTriggerDirective,
    TimeAgoPipe,
    ChangeDetectionInfoComponent,
    ClipboardModule,
    TagListComponent,
    TagInputComponent,
    AutoFocusDirective,
    ElementRefDirective,
  ],
  providers: [
    { provide: APP_INITIALIZER, useFactory: () => setGlobals(), multi: true },
    { provide: ErrorHandler, useClass: GlobalErrorHandler },
    {
      provide: Sentry.TraceService,
      deps: [Router],
    },
    { provide: COCKPIT_ORIGIN, useValue: ENVIRONMENT.cockpitOrigin },
    { provide: OverlayContainer, useClass: DynamicFullscreenOverlayContainer },
    { provide: DecisionData, useValue: new DecisionData(ENVIRONMENT.version) },
    DatePipe,
    DecimalPipe,
    DecisionDataExportService,
    ExplanationService,
    GlobalErrorService,
    InfluenceFactorNamePipe,
    PdfExportService,
    RichTextEmptyPipe,
    SafeNumberPipe,
    StateNamePipe,
    TrackingService,
    TransferService,
    {
      provide: MAT_SNACK_BAR_DEFAULT_OPTIONS,
      useValue: {
        horizontalPosition: 'right',
        verticalPosition: 'top',
        panelClass: 'dt-snackbar-container',
      },
    },
    {
      provide: MAT_FORM_FIELD_DEFAULT_OPTIONS,
      useValue: { appearance: 'fill', hideRequiredMarker: true },
    },
    {
      provide: MAT_TOOLTIP_DEFAULT_OPTIONS,
      useValue: { disableTooltipInteractivity: true } satisfies Partial<MatTooltipDefaultOptions>,
    },
    provideHttpClient(withInterceptorsFromDi()),
  ],
})
export class DecisionToolModule {
  constructor(
    private _1: ServiceWorkerService,
    private _2: TransferService,
    private _3: CypressService,
    private _4: TimeTrackingService,
    private _5: Sentry.TraceService,
    private _6: YoutubeVideosService,
    platform: Platform,
  ) {
    if (platform.IOS) {
      const styles = `
      .statement-input {  font-size: max(1em, 16px);  }
      input { font-size: max(1em, 16px) !important; } `;
      const styleSheet = document.createElement('style');
      styleSheet.innerText = styles;
      document.head.appendChild(styleSheet);
    }
  }
}
