import { Component, DoCheck, HostListener, NgZone } from '@angular/core';

import { TimeTrackingService } from '../data/time-tracking.service';

@Component({
  selector: 'dt-time-tracking',
  template: '',
})
export class TimeTrackingComponent implements DoCheck {
  constructor(
    private timeTrackingService: TimeTrackingService,
    private zone: NgZone,
  ) {}

  @HostListener('window:focus') focus() {
    this.timeTrackingService.onFocus();
  }

  @HostListener('window:blur') blur() {
    this.timeTrackingService.onBlur();
  }

  ngDoCheck() {
    this.zone.runOutsideAngular(() => {
      this.timeTrackingService.checkForDDChanges();
    });
  }
}
