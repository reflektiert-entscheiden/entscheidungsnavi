import { CanActivateFn, Router } from '@angular/router';
import { inject } from '@angular/core';
import { DecisionData, ProjectMode } from '@entscheidungsnavi/decision-data';
import { ProjectService } from '../data/project';
import { getStartUrlForProjectMode, urlToProjectMode } from '../../modules/shared/navigation/navigation';

export function projectModeGuard(targetMode: ProjectMode): CanActivateFn {
  return () => {
    const router = inject(Router);

    const projectService = inject(ProjectService);
    if (!projectService.isProjectLoaded()) {
      return router.parseUrl('/start');
    }

    const decisionData = inject(DecisionData);
    if (decisionData.projectMode !== targetMode) {
      // We are trying to switch to a different targetMode. If we came from a url matching the current targetMode,
      // we simply block the navigation. Otherwise, we redirect to the start url of the current targetMode.
      const currentMode = urlToProjectMode(router.url);
      if (currentMode === decisionData.projectMode) {
        return false;
      } else {
        return router.parseUrl(getStartUrlForProjectMode(decisionData.projectMode));
      }
    }

    return true;
  };
}
