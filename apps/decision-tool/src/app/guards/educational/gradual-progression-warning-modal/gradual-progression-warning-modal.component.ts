import { Component } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { AppSettingsService } from '../../../data/app-settings.service';

export type ModalResult = 'abort' | 'confirm' | 'navigate-to-latest';

@Component({
  selector: 'dt-gradual-progression-warning-modal',
  templateUrl: './gradual-progression-warning-modal.component.html',
  styleUrls: ['./gradual-progression-warning-modal.component.scss'],
})
export class GradualProgressionWarningModalComponent {
  doNotShowAgain = !this.appSettings.skipSubstepWarning;

  constructor(
    private dialog: MatDialogRef<GradualProgressionWarningModalComponent>,
    private appSettings: AppSettingsService,
  ) {}

  close(result: ModalResult = 'abort') {
    if (result === 'confirm') {
      this.appSettings.skipSubstepWarning = !this.doNotShowAgain;
    }
    this.dialog.close(result);
  }
}
