import { Injectable } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRouteSnapshot, CanActivateChild, Router, RouterStateSnapshot } from '@angular/router';
import { DecisionData, NAVI_STEP_ORDER } from '@entscheidungsnavi/decision-data';
import { ErrorModalComponent } from '../../../modules/shared/error';
import { LanguageService } from '../../data/language.service';
import { urlToEducationalSubStep } from '../../../modules/shared/navigation/educational-navigation';

/**
 * This guard ensures that all previous steps are completed and valid.
 *
 * e.g. if we enter the alternatives, it makes sure that decision-statement and objectives are completed and valid.
 */
@Injectable({ providedIn: 'root' })
export class EducationalDecisionDataValidationGuard implements CanActivateChild {
  constructor(
    private decisionData: DecisionData,
    private dialog: MatDialog,
    private router: Router,
    private languageService: LanguageService,
  ) {}

  canActivateChild(_childRoute: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    const targetStep = urlToEducationalSubStep(state.url);

    if (targetStep == null) {
      return true;
    }

    // eslint-disable-next-line prefer-const
    let [firstErrorStep, errorResult] = this.decisionData.getAllErrors(targetStep.step);

    // Navigation to decision statement should be safe.
    if (targetStep.step === 'decisionStatement') {
      errorResult = [true, []];
    }

    // Navigation to some prior (or the same) step should also be safe.
    if (['objectives', 'alternatives', 'impactModel'].includes(targetStep.step)) {
      if (NAVI_STEP_ORDER.indexOf(firstErrorStep) >= NAVI_STEP_ORDER.indexOf(targetStep.step)) {
        errorResult = [true, []];
      }
    }

    if (targetStep.step === 'results' && targetStep.subStepIndex <= 1 && firstErrorStep === 'results') {
      errorResult = [true, []];
    }

    if (!errorResult[0]) {
      ErrorModalComponent.open(
        this.dialog,
        $localize`Du musst zunächst den Schritt ${this.languageService.steps[firstErrorStep].name}
        abschließen, um hierhin navigieren zu können.`,
        errorResult[1],
      );

      const currentStep = urlToEducationalSubStep(this.router.url);
      if (firstErrorStep !== currentStep?.step) {
        if (firstErrorStep === 'results') {
          /**
           * The results step is the only step where errors in substeps prevent access to the main page.
           * If resultSubstepProgress gets out of sync somehow, we would loop here forever. This is a safety net.
           */
          return this.router.parseUrl(this.languageService.steps[firstErrorStep].routerLink + '/steps/1');
        }

        return this.router.parseUrl(this.languageService.steps[firstErrorStep].routerLink);
      } else {
        return false;
      }
    }

    return true;
  }
}
