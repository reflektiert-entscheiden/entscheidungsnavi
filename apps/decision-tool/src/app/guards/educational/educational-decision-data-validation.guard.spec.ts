import { Alternative, DecisionData, Objective } from '@entscheidungsnavi/decision-data';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { LanguageService } from '../../data/language.service';
import { EducationalDecisionDataValidationGuard } from './educational-decision-data-validation.guard';

describe('EducationalDecisionDataValidationGuard', () => {
  it('handles out of sync resultSubstepProgress', () => {
    const decisionData = new DecisionData();
    decisionData.decisionStatement.statement = 'Test Decision Statement';
    decisionData.addObjective(new Objective('Test Objective'));
    decisionData.addAlternative({ alternative: new Alternative(0, 'Test Alternative 1') });
    decisionData.addAlternative({ alternative: new Alternative(0, 'Test Alternative 2') });

    decisionData.outcomes[0][0].values = [[[1]]];
    decisionData.outcomes[1][0].values = [[[1]]];

    decisionData.weights.preliminaryWeights[0].value = 100;

    decisionData.resultSubstepProgress = 2;

    // Make sure we created a valid decisionData
    expect(decisionData.getFirstStepWithErrors()).toBe(null);

    // Fake an error in the result step
    jest.spyOn(decisionData, 'getAllErrors').mockReturnValue(['results', [false, [{ code: 146 }]]]);

    const fakeDialog = {
      open: jest.fn(),
    } as unknown as MatDialog;

    const fakeRouter = {
      parseUrl: jest.fn().mockReturnValue('parsed'),
      url: '/decisionstatement',
    } as unknown as Router;

    const fakeLanguageService = new LanguageService('de-DE');

    const guard = new EducationalDecisionDataValidationGuard(decisionData, fakeDialog, fakeRouter, fakeLanguageService);

    const result = guard.canActivateChild(null, { url: '/results' } as any);
    expect(result).toBe('parsed');
    expect(fakeRouter.parseUrl).toHaveBeenCalledWith(fakeLanguageService.steps['results'].routerLink + '/steps/1');
  });
});
