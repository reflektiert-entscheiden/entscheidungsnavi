import { inject } from '@angular/core';
import { CanActivateFn, Router } from '@angular/router';
import { DecisionData } from '@entscheidungsnavi/decision-data';
import { ProjectService } from '../data/project';
import { getStartUrlForProjectMode } from '../../modules/shared/navigation/navigation';

export const startPageGuard: CanActivateFn = () => {
  if (inject(ProjectService).project$.value == null) return true;

  const router = inject(Router);
  const projectMode = inject(DecisionData).projectMode;

  return router.parseUrl(getStartUrlForProjectMode(projectMode));
};
