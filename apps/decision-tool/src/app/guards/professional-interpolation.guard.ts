import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot } from '@angular/router';
import { InterpolationService } from '../../services/interpolation.service';
import { EVALUATE_AND_DECIDE_TOOLS, STRUCTURE_AND_ESTIMATE_TOOLS } from '../../modules/professional/main/pf-main.component';

/**
 * Completes decision data for analysis to make navigation to this route frictionless.
 *
 * It basically goes through all decision data properties like a ValidateDecisionDataGuard but instead of signalling
 * the errors it corrects/patches them.
 *
 * This is the reason why both, this guard and the ValidateDecisionDataGuard, should be held in sync.
 */
@Injectable({
  providedIn: 'root',
})
export class ProfessionalInterpolationGuard {
  constructor(private interpolationService: InterpolationService) {}

  canActivate() {
    this.interpolationService.interpolateForSecondProfessionalTab({ suppressObjectiveWeightsSnackbar: true });
    return true;
  }

  canActivateChild(route: ActivatedRouteSnapshot) {
    if ('tool' in route.data) {
      return this.guardTab(route.data.tool);
    }

    return true;
  }

  private guardTab(tool: string): boolean {
    if (STRUCTURE_AND_ESTIMATE_TOOLS.includes(tool as any)) {
      this.interpolationService.interpolateForFirstProfessionalTab();
    } else if (EVALUATE_AND_DECIDE_TOOLS.includes(tool as any)) {
      this.interpolationService.interpolateForSecondProfessionalTab();
    } else {
      return false;
    }

    return true;
  }
}
