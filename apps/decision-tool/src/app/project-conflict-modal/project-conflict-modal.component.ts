import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';

export interface ProjectConflictData {
  localLoadTime: Date;
  onlineUpdateTime: Date;
}

export type ProjectConflictResult = 'take-online' | 'take-local' | 'cancel';

@Component({
  templateUrl: './project-conflict-modal.component.html',
  styleUrls: ['./project-conflict-modal.component.scss'],
})
export class ProjectConflictModalComponent {
  resolutionMode: 'take-online' | 'take-local';

  constructor(
    private dialogRef: MatDialogRef<ProjectConflictModalComponent>,
    @Inject(MAT_DIALOG_DATA) protected data: ProjectConflictData,
  ) {}

  close(result: ProjectConflictResult = 'cancel') {
    this.dialogRef.close(result);
  }
}
