import { cmToP } from '@entscheidungsnavi/tools';
import { mapValues, omit, pick } from 'lodash';
import { Content, ContentImage, ContentOrderedList, ContentText, ContentUnorderedList } from 'pdfmake/interfaces';
import Delta from 'quill-delta/dist/Delta';
import Op from 'quill-delta/dist/Op';
import { PARAGRAPH_MARGIN } from './pdf-export-config';

type DeltaListType = 'bullet' | 'ordered' | 'none';

// A single line of text in Quill Delta which may consist of multiple Ops
interface DeltaLine {
  ops: Op[];
  listType: DeltaListType;
}

// A group of lines with the same list type
interface DeltaGroup {
  items: DeltaLine[];
  listType: DeltaListType;
}

// A subset of Content from pdfmake which we use to denote text. It only allows string texts,
// instead of more complex types like pdfmake.
type TextContent = ContentText & { text: string };

/**
 * Converts the rich text output from Quill.js to the format used by pdfMake.
 *
 * @param richText - Input which may be a JSON object from Quill or plain text
 * @param trimTrailing - Remove trailing whitespace according to String.prototype.trim() if true
 */
export function parseRichText(richText: string, trimTrailing = true): Content {
  try {
    const delta = new Delta(JSON.parse(richText));
    const content = processDelta(delta, trimTrailing);
    return { stack: content };
  } catch {
    // The text is not in JSON format, so we assume it is plaintext
    return { text: richText };
  }
}

/**
 * Takes Quill Delta as input and processes it into Content definitions for pdfMake.
 *
 * @param delta - Quill Delta input
 */
function processDelta(delta: Delta, trimTrailing: boolean): Content[] {
  // Lists in Quill Delta are defined on a line by line basis. If a line has a list attribute
  // it is part of a list. Each line with a list attribute is a list item. Otherwise it is not.
  // A line may consist of multiple Ops, so we have to segment these ops by line break.

  const lines = segmentOpsByLineBreak(delta.ops);

  // Split lines when we find an image, so that every image has its own line.
  // pdfmake does not allow images to be inline.
  for (let i = 0; i < lines.length; i++) {
    let firstImage: number;

    while (
      lines[i].ops.length > 1 &&
      (firstImage = lines[i].ops.findIndex(op => typeof op.insert === 'object' && 'image' in op.insert)) > -1
    ) {
      lines.splice(i + 1, 0, { listType: lines[i].listType, ops: lines[i].ops.splice(firstImage, 1) });
      lines.splice(i + 2, 0, { listType: lines[i].listType, ops: lines[i].ops.splice(firstImage) });
    }
  }

  const result = groupLinesByListType(lines).map(group => {
    // An array of lines which all consist of multiple Ops
    const content = group.items.map(line => line.ops).map(ops => ops.map(deltaOpToContent));
    if (group.listType === 'none') {
      // Empty lines are ignored by pdfmake and not printed in the document. We assume they denote a new paragraph
      // and add a corresponding margin.
      // Furthermore, we put images into separate lines.
      return content.map(items => {
        if (items.length === 0) return { text: '', marginBottom: PARAGRAPH_MARGIN };
        else if (items.length === 1 && items[0].image) return items;
        else return { text: items };
      });
    } else {
      // If the lines belong to a list, each line is a list item

      const list: ContentUnorderedList | ContentOrderedList = group.listType === 'bullet' ? { ul: content } : { ol: content };
      return list;
    }
  });

  // Trim trailing whitespace, if enabled. This also removes the bottom margin of the last block, if it is a text block.
  if (trimTrailing) {
    trimTrailingWhitespace(result);
  }

  return result;
}

/**
 * Trims any trailing whitespace (as defined by {@link String.prototype.trim}) in the given content in-place.
 *
 * @param content - The content to trim
 * @returns true iff non-empty content was found
 */
function trimTrailingWhitespace(content: Content): boolean {
  if (typeof content === 'string') {
    // If we get a string, we simply check if it is non-empty
    return content.trimEnd().length > 0;
  } else if (Array.isArray(content)) {
    // Iterate the array from end to start, trim all strings,
    // terminate once we find non-empty content.
    for (let i = content.length - 1; i >= 0; i--) {
      const item = content[i];
      if (typeof item === 'string') {
        content[i] = item.trimEnd();
      }

      if (trimTrailingWhitespace(content[i])) {
        return true;
      }
    }
  } else if (typeof content === 'object' && 'text' in content) {
    if (typeof content.text === 'string') {
      content.text = content.text.trimEnd();
      // Remove bottom margin from all elements from end to start, until (but including) the first non-empty one
      content.marginBottom = 0;
      return content.text.length > 0;
    } else {
      return trimTrailingWhitespace(content.text);
    }
  } else {
    // We have a node that is not simply text. In the context of this parser,
    // this always means non-empty (e.g., a list or an image).
    return true;
  }
}

/**
 * Takes an array of lines as input and groups subsequent lines of the same list type into a list group.
 *
 * @param lines - the array of lines
 */
function groupLinesByListType(lines: DeltaLine[]): DeltaGroup[] {
  lines = lines.slice(); // shallow clone to not modify the original
  const groups: DeltaGroup[] = [];

  while (lines.length > 0) {
    const listType = lines[0].listType;
    const endIndex = lines.findIndex(line => line.listType !== listType);

    let lineGroup: DeltaLine[] = [];
    if (endIndex === -1) {
      lineGroup = lines;
      lines = [];
    } else {
      lineGroup.push(...lines.splice(0, endIndex));
    }

    groups.push({ items: lineGroup, listType });
  }

  return groups;
}

/**
 * We split the operations into lines that are separated by a line break (\\n). This array
 * of lines is returned where each line has a list type associated with it.
 *
 * @param ops - the array of Ops
 */
function segmentOpsByLineBreak(ops: Op[]): DeltaLine[] {
  ops = [...ops]; // shallow clone to not modify the original
  const result: DeltaLine[] = [];

  while (ops.length > 0) {
    const nextLb = scanUntilNextLineBreak(ops);

    if (nextLb.index === -1) {
      result.push({ ops, listType: nextLb.listType });
      ops = [];
    } else {
      // We have a line break at which we split the content into the current line and the following content.
      const lbOp = ops[nextLb.index];
      const lbAttributes = lbOp.attributes || {};
      const lbText = lbOp.insert as string;
      const lbIndex = lbText.indexOf('\n');

      // Add the text until the lb to the current line
      const currentLineOps = ops.splice(0, nextLb.index);
      currentLineOps.push({
        attributes: lbAttributes,
        insert: lbText.substring(0, lbIndex),
      });

      // Add the text after the lb to the next operation
      ops[0] = {
        attributes: omit(lbAttributes, 'list'),
        insert: lbText.substring(lbIndex + 1),
      };

      // Save the line in the results
      result.push({ ops: currentLineOps, listType: nextLb.listType });
    }
  }

  // Filter empty operations from lines
  result.forEach(line => (line.ops = line.ops.filter(op => typeof op.insert !== 'string' || op.insert.length > 0)));

  return result;
}

/**
 * Scans the operations until the next line break is found. Returns whether a list attribute was used up until
 * the line break and returns the index of the operation containing the line break or -1 if no line break is found.
 *
 * @param ops - the operations to search
 */
function scanUntilNextLineBreak(ops: Op[]): { index: number; listType: DeltaListType } {
  ops = [...ops]; // shallow clone to not modify the original
  let listType: DeltaListType = 'none';

  for (let i = 0; i < ops.length; i++) {
    const opListType = getListType(ops[i]);
    if (opListType !== 'none') {
      listType = opListType;
    }

    const ins = ops[i].insert;
    if (typeof ins === 'string' && ins.includes('\n')) {
      return { index: i, listType };
    }
  }

  return { index: -1, listType };
}

/**
 * Determine the type of the list attribute in the given operation.
 *
 * @param op - the delta operation to determine the type of the list attribute
 */
function getListType(op: Op): DeltaListType {
  if (
    'attributes' in op &&
    'list' in op.attributes &&
    (op.attributes.list === 'bullet' || op.attributes.list === 'ordered') &&
    'insert' in op &&
    op.insert === '\n'
  ) {
    return op.attributes.list;
  }
  return 'none';
}

/**
 * Takes a Delta Operation and transforms it into a content definition for pdfmake.
 * The return type is a subset of Content.
 *
 * @param op - the delta operation to transform
 */
function deltaOpToContent(op: Op): TextContent | ContentImage {
  if (typeof op.insert === 'string') {
    const data: TextContent = { text: op.insert };

    if ('attributes' in op) {
      if ('bold' in op.attributes && op.attributes.bold === true) {
        data.bold = true;
      }
      if ('italic' in op.attributes && op.attributes.italic === true) {
        data.italics = true;
      }
      if ('link' in op.attributes && typeof op.attributes.link === 'string') {
        data.link = op.attributes.link;
      }
      if ('underline' in op.attributes && op.attributes.underline === true) {
        data.decoration = 'underline';
      }
    }

    return data;
  } else if (typeof op.insert === 'object' && 'image' in op.insert && typeof op.insert.image === 'string') {
    if (!canImageBeUsedInPdf(op.insert.image)) {
      return { text: '' };
    }

    // We divide width and height by 2 as a heuristic.
    const { width, height } = mapValues(
      pick(op.attributes, 'width', 'height'),
      // Quill's width/height is likely in px (although there is no documentation afaik).
      // The value of one px differs depending on the DPI of the used device. We use a guesstimate to convert
      // to pt for PDF.
      (value: string) => cmToP(parseInt(value) * 0.026458),
    );
    // We define a max width for images which roughly corresponds to the usable page size
    return { image: op.insert.image, fit: [Math.min(450, width ?? Infinity), Math.min(700, height ?? Infinity)] };
  } else {
    return { text: '' };
  }
}

function canImageBeUsedInPdf(imageURI: string) {
  let parsedImageURI: URL;
  try {
    parsedImageURI = new URL(imageURI);
  } catch (e) {}

  if (parsedImageURI == null) {
    console.error('Invalid image URI:', imageURI);
    return false;
  }

  if (parsedImageURI.protocol !== 'data:') {
    console.error('URI is not a data url:', imageURI);
    return false;
  } else {
    const mediaType = parsedImageURI.pathname.split(';')[0];

    if (!mediaType.startsWith('image/')) {
      console.error('Data URL is not an image:', mediaType);
      return false;
    }

    const imageFormat = mediaType.split('/')[1];

    if (!imageFormat) {
      console.error('Data URL has no image format', imageFormat);
      return false;
    }

    if (!['png', 'jpeg'].includes(imageFormat)) {
      console.error('Unsupported image format:', imageFormat);
      return false;
    }

    return true;
  }
}
