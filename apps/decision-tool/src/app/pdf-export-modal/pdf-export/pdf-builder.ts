import { cmToP } from '@entscheidungsnavi/tools';
import { defaults } from 'lodash';
import { Content, Node, Style, TDocumentDefinitions } from 'pdfmake/interfaces';
import { ENVIRONMENT } from '../../../environments/environment';
import { PARAGRAPH_MARGIN, SECTION_MARGIN } from './pdf-export-config';

/**
 * This class handles header numbering.
 */
class HeaderNumbering {
  private count = 0;
  private child?: HeaderNumbering;

  /**
   * Returns a list of numbers which represents the number for the next header on the
   * specified level.
   *
   * @example
   * ```ts
   *   getAndIncrement(1) \\ [1]
   *   getAndIncrement(1) \\ [2]
   *   getAndIncrement(3) \\ [2, 1, 1]
   * ```
   * @param level - the depth of the current header
   */
  getAndIncrement(level: number): number[] {
    if (level > 1) {
      if (this.child == null) {
        this.child = new HeaderNumbering();
      }
      return [this.count, ...this.child.getAndIncrement(level - 1)];
    } else if (level === 1) {
      this.count++;
      this.child = undefined;
      return [this.count];
    } else {
      return [];
    }
  }
}

export class PdfBuilder {
  private content: Content[] = [];
  private headerData = new HeaderNumbering();

  constructor(
    private projectName: string,
    private date: Date,
    private author?: string,
  ) {
    this.content.push(
      { text: 'Entscheidungsnavi-REPORT', fontSize: 26, bold: true, margin: [0, 50, 0, 0], alignment: 'center' },
      {
        stack: [
          { text: [{ text: $localize`Projektname`, bold: true }, ': ', projectName] },
          { text: [{ text: $localize`Datum`, bold: true }, ': ', date.toLocaleDateString()] },
          { text: author != null ? [{ text: $localize`Autor`, bold: true }, ': ', author] : [] },
        ],
        alignment: 'center',
        margin: [0, 40, 0, 40],
        lineHeight: 1.5,
      },
      {
        toc: { title: { text: $localize`Inhaltsverzeichnis`, style: 'header', margin: [0, 0, 0, PARAGRAPH_MARGIN] } },
        pageBreak: 'after',
      },
    );
  }

  /**
   * Returns the finished document.
   */
  buildDocument(): TDocumentDefinitions {
    // Default style for text
    const defaultStyle: Style = {
      fontSize: 11,
      lineHeight: 1.08,
      alignment: 'justify',
      color: 'black',
    };

    // The document template
    return {
      info: {
        title: `${this.projectName} — Entscheidungsnavi-REPORT`,
        author: this.author,
        creator: `Entscheidungsnavi v${ENVIRONMENT.version}`,
        creationDate: this.date,
        modDate: new Date(),
      },
      pageSize: 'A4',
      pageOrientation: 'portrait',
      pageMargins: [cmToP(2.5), cmToP(2.0), cmToP(2.0), cmToP(2.25)],
      content: this.content,
      footer: currentPage => ({
        text: `${currentPage}`,
        alignment: 'center',
      }),
      pageBreakBefore(currentNode: Node, _followingNodesOnPage?: any, _nodesOnNextPage?: any, _previousNodesOnPage?: any) {
        // If the heading is in the lower part of the page, break to the next page
        return (
          (currentNode.headlineLevel === 1 && currentNode.startPosition.verticalRatio > 0.7) ||
          (currentNode.headlineLevel === 2 && currentNode.startPosition.verticalRatio > 0.8)
        );
      },
      defaultStyle,
      styles: {
        default: defaultStyle,
        header: {
          fontSize: 16,
          alignment: 'left',
          color: '#1f4e79',
        },
        subheader: {
          fontSize: 13,
          alignment: 'left',
          color: '#2e74b5',
        },
      },
    };
  }

  addContentBlock(...content: Content[]) {
    // Add a default bottom margin if not otherwise specified
    this.content.push(addDefaultMargin(content, SECTION_MARGIN));
    return this;
  }

  addHeader(text: string, level?: number) {
    // Parse the number for the given level
    let levelText = '';
    if (level != null && level > 0) {
      levelText = this.headerData.getAndIncrement(level).join('.') + '. ';
    }

    this.content.push({
      text: `${levelText}${text}`,
      style: 'header',
      tocItem: level <= 2,
      headlineLevel: level,
      margin: [0, level === 1 ? 20 : 10, 0, PARAGRAPH_MARGIN],
    });
    return this;
  }

  addSubheader(text: string) {
    this.content.push({
      text,
      style: 'subheader',
      margin: [0, 5, 0, PARAGRAPH_MARGIN],
    });
    return this;
  }

  addList<T>(
    list: T[],
    style: 'ordered' | 'unordered',
    generator: (item: T, index: number) => { header: Content; content: Content[] },
    placeholder?: Content,
  ) {
    if (list.length > 0) {
      const listItems: Content[] = list.map((item, index) => {
        const gen = generator(item, index);
        return {
          style: 'default',
          stack: [
            { text: gen.header, style: 'subheader', headlineLevel: 2, margin: [0, PARAGRAPH_MARGIN, 0, PARAGRAPH_MARGIN] },
            // We add a default margin between the content elements
            ...addDefaultMargin(gen.content, PARAGRAPH_MARGIN),
          ],
        };
      });
      this.content.push(style === 'ordered' ? { ol: listItems, style: 'subheader' } : { ul: listItems, style: 'subheader' });
    } else if (placeholder != null) {
      this.addContentBlock(placeholder);
    }
    return this;
  }
}

function addDefaultMargin(content: Content[], margin: number): Content[] {
  return content.map(entry => defaults(entry, { marginBottom: margin }));
}
