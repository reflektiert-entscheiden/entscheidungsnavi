import { Content } from 'pdfmake/interfaces';
import { isRichTextEmpty } from '@entscheidungsnavi/tools';
import { parseRichText } from './quill-delta-parser';

/**
 * Add a rich text snippet if it is not empty.
 *
 * @param richText - the rich text to add if not empty
 * @param placeholder - the text to add if the rich text is empty
 * @param margin - margin of the text
 */
export function addNonEmptyRichText(richText: string, placeholder?: string, margin?: [number, number, number, number]): Content[] {
  if (isRichTextEmpty(richText)) {
    if (placeholder != null) {
      return [{ text: placeholder, margin }];
    } else {
      return [];
    }
  } else {
    return [{ stack: [parseRichText(richText)], margin }];
  }
}
