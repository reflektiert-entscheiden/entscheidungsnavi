import { Inject, Injectable, LOCALE_ID } from '@angular/core';
import {
  formatNumberSafe,
  getCrossImpactName,
  getInfluenceFactorName,
  getInfluenceFactorStateName,
  PdfmakeService,
} from '@entscheidungsnavi/widgets';
import {
  Alternative,
  calculateIndicatorValue,
  CompositeUserDefinedInfluenceFactor,
  UserDefinedInfluenceFactor,
  DECISION_QUALITY_CRITERIA,
  DecisionData,
  DecisionQuality,
  Objective,
  ObjectiveInput,
  ObjectiveType,
  Outcome,
  UtilityFunction,
} from '@entscheidungsnavi/decision-data';
import { isRichTextEmpty, normalizeDiscrete } from '@entscheidungsnavi/tools';
import { inRange, range, zip } from 'lodash';
import { Content, ContentStack } from 'pdfmake/interfaces';
import { assertUnreachable } from '@entscheidungsnavi/tools/assert-unreachable';
import { formatNumber, formatPercent } from '@angular/common';
import { verbalizeDQCriteria } from '../../../modules/shared/decision-quality';
import { PdfBuilder } from './pdf-builder';
import { addNonEmptyRichText } from './pdf-export-tools';
import { parseRichText } from './quill-delta-parser';
import { PARAGRAPH_MARGIN } from './pdf-export-config';

export enum PdfMode {
  REDUCED = 0,
  COMPLETE,
}

@Injectable({
  providedIn: 'root',
})
export class PdfExportService {
  constructor(
    private decisionData: DecisionData,
    private pdfMake: PdfmakeService,
    @Inject(LOCALE_ID) private locale: string,
  ) {}

  /**
   * Generates a PDF report document from the current project.
   *
   * @param pdfMode - the mode in which to generate the report
   * @param projectTitle - the project title
   * @param author - omitted when undefined
   * @param date - defaults to current date
   * @param robustnessCheck - the results of the robustness check containing the minimum and maximum utility,
   *                        the total utility and the ranking score.
   * @param robustnessCheckSteps - number of steps performed in the robustness check
   */
  async generatePdfReport(
    pdfMode: PdfMode,
    projectTitle: string,
    author?: string,
    date = new Date(),
    robustnessCheck?: Array<{ minUtility: number; maxUtility: number; score: number }>,
    robustnessCheckSteps?: number,
  ) {
    const decisionData = this.decisionData;
    const validObjectiveWeighting = this.decisionData.validateTradeoffObjective()[0] && this.decisionData.validateWeights()[0];

    const mode = decisionData.projectMode;
    const isStarterMode = mode === 'starter';

    const listInfluenceFactors = !isStarterMode; // Whether to list used Influence Factors
    const listUtilityFunctions = !isStarterMode; // Whether to list utility function information together with objective scales
    const detailedObjectiveWeights = !isStarterMode; // Whether to list the reference objective and tradeoff comments
    const showRobustnessCheck = !isStarterMode; // Whether to add columns related to the robustness check to the results table

    const pdfBuilder = new PdfBuilder(projectTitle, date, author)
      .addHeader($localize`Vorbemerkungen`)
      .addContentBlock(...addNonEmptyRichText(decisionData.projectNotes, $localize`Keine Vorbemerkungen vorhanden.`))
      // Decision Statement
      .addHeader($localize`Entscheidungsfrage`, 1)
      .addContentBlock(...addNonEmptyRichText(decisionData.stepExplanations['decisionStatement']))
      .addSubheader($localize`Ausformulierte Entscheidungsfrage`)
      .addContentBlock({
        text: decisionData.decisionStatement.statement ?? '',
      })
      .addSubheader($localize`Annahmen und Vorentscheidungen`)
      .addContentBlock(
        decisionData.decisionStatement.preNotesFinal.length > 0
          ? {
              ul: decisionData.decisionStatement.preNotesFinal.slice(),
            }
          : { text: $localize`Keine Annahmen oder Vorentscheidungen definiert.`, italics: true },
      )
      .addSubheader($localize`Folgeentscheidungen`)
      .addContentBlock(
        decisionData.decisionStatement.afterNotesFinal.length > 0
          ? {
              ul: decisionData.decisionStatement.afterNotesFinal.slice(),
            }
          : { text: $localize`Keine Folgeentscheidungen definiert.`, italics: true },
      )
      // Objectives
      .addHeader($localize`Ziele`, 1)
      .addContentBlock(...addNonEmptyRichText(decisionData.stepExplanations['objectives']))
      .addList(decisionData.objectives, 'unordered', (i, n) => this.generateObjectiveInformation(i, n))
      // Alternatives
      .addHeader($localize`Alternativen`, 1)
      .addContentBlock(...addNonEmptyRichText(decisionData.stepExplanations['alternatives']))
      .addList(decisionData.alternatives, 'unordered', (i, n) => this.generateAlternativeInformation(i, n));

    if (pdfMode === PdfMode.COMPLETE) {
      pdfBuilder
        .addHeader($localize`Wirkungsmodell`, 1)
        .addContentBlock(...addNonEmptyRichText(decisionData.stepExplanations['impactModel']))
        .addHeader($localize`Zielskalen`, 2)
        .addList(decisionData.objectives, 'unordered', (i, n) => this.generateObjectiveScaleInformation(i, n));
    }

    if (listInfluenceFactors) {
      pdfBuilder
        .addHeader($localize`Einflussfaktoren`, pdfMode === PdfMode.COMPLETE ? 2 : 1)
        .addList(decisionData.influenceFactors, 'unordered', (i, n) => this.generateInfluenceFactorInformation(i, n), {
          text: $localize`Es wurden keine Einflussfaktoren verwendet.`,
        });
    }

    if (pdfMode === PdfMode.COMPLETE) {
      pdfBuilder.addHeader($localize`Ergebnisabschätzungen`, 2).addList(decisionData.objectives, 'unordered', (i, n) =>
        this.generateOutcomeInformation(
          i,
          n,
          decisionData.alternatives,
          decisionData.outcomes.map(ocsForAlt => ocsForAlt[n]),
        ),
      );

      pdfBuilder.addHeader($localize`Evaluation`, 1);

      if (listUtilityFunctions) {
        pdfBuilder
          .addHeader($localize`Nutzenfunktionen`, 2)
          .addList(decisionData.objectives, 'unordered', (i, n) => this.generateUtilityFunctionInformation(i, n));
      }

      pdfBuilder.addHeader($localize`Zielgewichtung`, 2);

      if (detailedObjectiveWeights) {
        pdfBuilder.addContentBlock({
          text: validObjectiveWeighting
            ? $localize`Gewähltes Referenzziel\: ${decisionData.objectives[decisionData.weights.tradeoffObjectiveIdx].name}`
            : $localize`Kein Referenzziel ausgewählt. Die Zielgewichtung wurde nicht vollständig durchgeführt.`,
        });
      }

      if (validObjectiveWeighting) {
        pdfBuilder.addContentBlock({
          stack: [
            { text: $localize`Die Zielgewichte sind\:` },
            {
              marginLeft: 15,
              ul: zip(decisionData.objectives, normalizeDiscrete(decisionData.weights.getWeightValues(), 100, 1)).map(
                ([objective, weight]) => ({
                  text: `${objective.name}: ` + formatPercent(weight / 100, this.locale),
                }),
              ),
            },
          ],
        });
      }

      if (detailedObjectiveWeights) {
        pdfBuilder.addList(
          zip(decisionData.objectives, decisionData.weights.explanations).filter(
            (obj, index) => index !== decisionData.weights.tradeoffObjectiveIdx && !isRichTextEmpty(obj[1]),
          ),
          'unordered',
          i => this.generateTradeoffInformation(decisionData.objectives[decisionData.weights.tradeoffObjectiveIdx], i[0], i[1]),
          { text: $localize`Keine Erläuterungen einzelner Trade-offs vorhanden.` },
        );
      }

      pdfBuilder.addHeader($localize`Ergebnis`, 2).addContentBlock(...addNonEmptyRichText(decisionData.stepExplanations['results']));

      if (validObjectiveWeighting) {
        pdfBuilder.addContentBlock(
          {
            text: showRobustnessCheck
              ? $localize`Im Robustheitstest wurde in ${formatNumber(
                  robustnessCheckSteps,
                  this.locale,
                  // eslint-disable-next-line max-len
                )} Simulationsläufen überprüft, wie häufig eine Alternative die beste war (Rangposition 1), die zweitbeste (Rangposition 2), etc. Der Rangfolge-Score berechnet auf dieser Basis die durchschnittliche Rangposition.`
              : '',
          },
          this.generateResultsTable(robustnessCheck, showRobustnessCheck),
        );
      } else {
        pdfBuilder.addContentBlock({
          text: showRobustnessCheck
            ? $localize`Der Robustheitstest wurde aufgrund fehlender Zielgewichte nicht durchgeführt.`
            : $localize`Aufgrund fehlender Zielgewichte konnte das Resultat nicht berechnet werden.`,
        });
      }

      pdfBuilder.addHeader($localize`Decision Quality`, 2).addContentBlock(this.generateDecisionQualityTable(decisionData.decisionQuality));
    }

    return await this.pdfMake.createPdf(pdfBuilder.buildDocument());
  }

  private generateDecisionQualityTable(dq: DecisionQuality): Content {
    return {
      layout: 'lightHorizontalLines',
      table: {
        headerRows: 1,
        widths: ['*', 'auto'],
        body: [
          [
            { text: $localize`Kriterium`, bold: true },
            { text: $localize`Bewertung (0 bis 10)`, bold: true },
          ],
          ...DECISION_QUALITY_CRITERIA.map(criterion => [
            verbalizeDQCriteria(criterion),
            dq.criteriaValues[criterion] !== -1
              ? formatNumber(dq.criteriaValues[criterion], this.locale, '1.1-1')
              : $localize`Nicht bewertet`,
          ]),
        ],
      },
    };
  }

  /**
   * Generate the pdf-Content explaining this objective.
   *
   * @param objective - the given objective
   * @param index - the index of this objective in decision-data
   */
  private generateObjectiveInformation(objective: Objective, index: number): { header: string; content: Content[] } {
    const content = [...addNonEmptyRichText(objective.comment)];
    const aspects = objective.getAspectsAsArray();
    if (aspects.length > 0) {
      content.push({
        text: [{ text: $localize`Teilaspekte\:`, italics: true }, ' ', aspects.join(', ')],
      });
    }
    return {
      header: $localize`Ziel ${index + 1}\: ${objective.name}`,
      content,
    };
  }

  /**
   * Generate the pdf-Content explaining this alternative.
   *
   * @param alternative - the given alternative
   * @param index - the index of that alternative in decision-data
   */
  private generateAlternativeInformation(alternative: Alternative, index: number): { header: string; content: Content[] } {
    return {
      header: $localize`Alternative ${index + 1}\: ${alternative.name}`,
      content: [...addNonEmptyRichText(alternative.comment)],
    };
  }

  /**
   * Generate the pdf-Content explaining this influence factor.
   *
   * @param influenceFactor - the given influence factor
   * @param index - the index of the influence factor in decision-data
   */
  private generateInfluenceFactorInformation(
    influenceFactor: UserDefinedInfluenceFactor,
    index: number,
  ): { header: string; content: Content[] } {
    if (influenceFactor instanceof CompositeUserDefinedInfluenceFactor) {
      const states = influenceFactor.getStates();
      return {
        header: $localize`Einflussfaktor ${index + 1}\: ${influenceFactor.name}`,
        content: [
          {
            text:
              $localize`Kombiniert aus den Einflussfaktoren` +
              ` ${influenceFactor.baseFactors[0].id + 1} (${influenceFactor.baseFactors[0].name}) ` +
              $localize`und` +
              ` ${influenceFactor.baseFactors[1].id + 1} (${influenceFactor.baseFactors[1].name}).`,
          },
          {
            layout: 'lightHorizontalLines',
            table: {
              headerRows: 1,
              widths: ['*', 'auto', 'auto'],
              body: [
                [
                  { text: $localize`Zustand`, bold: true },
                  { text: $localize`Cross-Impact`, bold: true },
                  { text: $localize`Gewichtung/W.-keit`, bold: true },
                ],
                ...states.map((state, index) => {
                  const [firstIndex, secondIndex] = influenceFactor.getBaseStateIndices(index);

                  return [
                    getInfluenceFactorStateName(influenceFactor, index),
                    getCrossImpactName(influenceFactor.crossImpacts[firstIndex][secondIndex]),
                    formatPercent(state.probability / 100, this.locale, '1.0-2'),
                  ];
                }),
              ],
            },
          },
          ...addNonEmptyRichText(influenceFactor.comment),
        ],
      };
    } else {
      return {
        header: $localize`Einflussfaktor ${index + 1}\: ${influenceFactor.name}`,
        content: [
          {
            layout: 'lightHorizontalLines',
            table: {
              headerRows: 1,
              widths: ['auto', '*', 'auto'],
              body: [
                [
                  { text: $localize`Zustand`, bold: true },
                  { text: $localize`Kurze Beschreibung`, bold: true },
                  { text: $localize`Gewichtung/W.-keit`, bold: true },
                ],
                ...influenceFactor.states.map(state => [
                  state.name,
                  state.comment || '—',
                  formatPercent(state.probability / 100, this.locale),
                ]),
              ],
            },
          },
          { text: $localize`Präzisionsintervall` + ': ± ' + formatPercent(influenceFactor.precision / 100, this.locale) },
          ...addNonEmptyRichText(influenceFactor.comment),
        ],
      };
    }
  }

  /**
   * Generate pdf-Content that describes the outcomes for one objective.
   *
   * @param objective - the objective in question
   * @param index - index of the objective in decision-data
   * @param alternatives - alternatives to consider
   * @param outcomes - the array of outcomes for the alternatives concerning that objective
   */
  private generateOutcomeInformation(
    objective: Objective,
    index: number,
    alternatives: Alternative[],
    outcomes: Outcome[],
  ): { header: Content; content: Content[] } {
    const format = (value: ObjectiveInput): Content => {
      switch (objective.objectiveType) {
        case ObjectiveType.Numerical:
          return `${formatNumberSafe(value[0][0], this.locale, '?')} ${objective.numericalData.unit}`;
        case ObjectiveType.Verbal:
          return inRange(value[0][0] - 1, 0, objective.verbalData.options.length)
            ? `${objective.verbalData.options[value[0][0] - 1]}`
            : '?';
        case ObjectiveType.Indicator:
          return {
            text: objective.indicatorData.indicators.flatMap((indicator, indIndex) => [
              { text: indicator.name, italics: true },
              ': ',
              formatNumberSafe(calculateIndicatorValue(value[indIndex], indicator), this.locale, '?'),
              ' ',
              indicator.unit,
              indIndex !== objective.indicatorData.indicators.length - 1 ? ', ' : '',
            ]),
          };
      }
    };

    return {
      header: [$localize`Ergebnisabschätzungen aller Alternativen im Ziel ${index + 1}`, ': ', { text: objective.name, italics: true }],
      content: alternatives.map((alternative, altIndex) => {
        const oc = outcomes[altIndex];
        const contentStack: ContentStack = {
          stack: [
            {
              text: [
                $localize`Alternative`,
                ' ',
                { text: alternative.name, italics: true },
                ': ',
                oc.influenceFactor == null ? format(oc.values[0]) : '',
              ],
              bold: true,
            },
            ...(oc.influenceFactor != null
              ? [
                  {
                    text: [
                      $localize`In Abhängigkeit des Einflussfaktors`,
                      ' ',
                      { text: getInfluenceFactorName(oc.influenceFactor), italics: true },
                      ':',
                    ],
                  },
                  {
                    ul: range(oc.influenceFactor.stateCount).map(stateIndex => ({
                      text: [
                        { text: getInfluenceFactorStateName(oc.influenceFactor, stateIndex), italics: true },
                        ': ',
                        format(oc.values[stateIndex]),
                      ],
                    })),
                    marginLeft: 15,
                  },
                ]
              : []),
            ...addNonEmptyRichText(oc.comment, null, [15, PARAGRAPH_MARGIN, 0, 0]),
          ],
          marginLeft: 15,
          marginBottom: PARAGRAPH_MARGIN,
        };

        return contentStack;
      }),
    };
  }

  /**
   * Generates scale information for the objective.
   *
   * @param objective - the objective in question
   * @param index - the index of the objective in decision data
   * @returns a list entry
   */
  private generateObjectiveScaleInformation(objective: Objective, index: number): { header: string; content: Content[] } {
    const content: Content[] = [];

    // Scale
    switch (objective.objectiveType) {
      case ObjectiveType.Numerical:
        content.push({
          text: [
            $localize`Verwendete Messskala\: Numerische Skala von ${formatNumber(objective.numericalData.from, this.locale)}`,
            ' ',
            $localize`bis ${formatNumber(objective.numericalData.to, this.locale)}`,
            ' ',
            objective.numericalData.unit,
          ],
        });
        if (!isRichTextEmpty(objective.numericalData.commentFrom)) {
          content.push(
            { text: $localize`Erläuterung zum unteren Ende der Skala`, italics: true },
            ...addNonEmptyRichText(objective.numericalData.commentFrom, null, [15, 0, 0, PARAGRAPH_MARGIN]),
          );
        }
        if (!isRichTextEmpty(objective.numericalData.commentTo)) {
          content.push(
            { text: $localize`Erläuterung zum oberen Ende der Skala`, italics: true },
            ...addNonEmptyRichText(objective.numericalData.commentTo, null, [15, 0, 0, PARAGRAPH_MARGIN]),
          );
        }
        break;
      case ObjectiveType.Verbal:
        content.push({
          stack: [
            {
              text: $localize`Verwendete Messskala\: Verbale Skala mit ${objective.verbalData.options.length} Ausprägungen`,
              marginBottom: 0,
            },
            {
              ol: zip(objective.verbalData.options, objective.verbalData.comments)
                .reverse()
                .map(([option, comment]) => ({
                  stack: [
                    { text: option, italics: true },
                    ...addNonEmptyRichText(comment, null, [0, PARAGRAPH_MARGIN, 0, PARAGRAPH_MARGIN]),
                  ],
                })),
              margin: [15, 0, 0, PARAGRAPH_MARGIN],
            },
          ],
        });
        break;
      case ObjectiveType.Indicator:
        content.push(
          {
            text: $localize`Verwendete Messskala\: Konstruierte Skala mit ${objective.indicatorData.indicators.length} Indikatoren`,
            marginBottom: 0,
          },
          {
            ul: objective.indicatorData.indicators.map(indicator => ({
              stack: [
                {
                  text: [
                    { text: indicator.name, italics: true },
                    ' ',
                    $localize`von ${formatNumber(indicator.min, this.locale)} bis ${formatNumber(indicator.max, this.locale)} ${
                      indicator.unit
                    }`,
                    ' ',
                    $localize`(Indikatorgewicht ${formatNumber(indicator.coefficient, this.locale)})`,
                  ],
                },
                ...addNonEmptyRichText(indicator.comment),
              ],
            })),
            marginLeft: 15,
          },
        );

        if (objective.indicatorData.useCustomAggregation) {
          content.push(
            { text: $localize`Verwendete Formel zur Aggregation\:`, marginBottom: 0 },
            {
              ul: [
                {
                  text: objective.indicatorData.customAggregationFormula,
                },
              ],
              marginLeft: 15,
            },
          );
        }

        content.push({
          text: [
            $localize`Resultierendes Intervall\:`,
            ' ',
            $localize`${formatNumber(objective.indicatorData.worstValue, this.locale)} bis ${formatNumber(
              objective.indicatorData.bestValue,
              this.locale,
            )}`,
            ' ',
            objective.indicatorData.aggregatedUnit,
          ],
        });

        if (objective.indicatorData.stages?.length > 0) {
          content.push(
            { text: $localize`Beschriebene Ausprägungen\:`, marginBottom: 0 },
            {
              ul: objective.indicatorData.stages.map(stage => ({
                stack: [
                  { text: $localize`Ausprägung ${formatNumber(stage.value, this.locale)}`, italics: true },
                  ...addNonEmptyRichText(stage.description),
                ],
              })),
              marginLeft: 15,
            },
          );
        }
        break;
      default:
        assertUnreachable(objective.objectiveType);
    }

    return {
      header: $localize`Ziel ${index + 1}\: ${objective.name}`,
      content,
    };
  }

  /**
   * Generate utility function information for an objective.
   *
   * @param objective - the objective in question
   * @param index - index of the objective in decision-data
   */
  private generateUtilityFunctionInformation(objective: Objective, index: number): { header: string; content: Content[] } {
    const content: Content[] = [];

    // Utility function
    let ufComment: string;
    const ufContent: Content = {
      stack: [],
      marginLeft: 15,
    };
    let nf: UtilityFunction;

    switch (objective.objectiveType) {
      case ObjectiveType.Numerical:
        nf = objective.numericalData.utilityfunction;
        break;
      case ObjectiveType.Verbal:
        ufComment = objective.verbalData.utilityFunctionExplanation;
        ufContent.stack.push({
          stack: [
            { text: $localize`Nutzenwerte der Ausprägungen\:` },
            {
              ul: zip(objective.verbalData.options, objective.verbalData.utilities).map(([name, utility]) => ({
                text: $localize`${name}\: ${formatNumber(utility, this.locale)}`,
              })),
              margin: [15, 0, 0, 0],
            },
            { text: $localize`Präzisionsintervall\: ± ${formatNumber(objective.verbalData.precision, this.locale)}` },
          ],
        });
        break;
      case ObjectiveType.Indicator:
        nf = objective.indicatorData.utilityfunction;
        break;
    }
    if (nf != null) {
      ufComment = nf.explanation;
      ufContent.stack.push({
        text:
          $localize`Exponentielle Nutzenfunktion mit c = ${formatNumber(nf.c, this.locale)}` +
          '\n' +
          `Präzisionsintervall ± ${formatNumber(nf.precision, this.locale)}`,
      });
    }
    content.push(ufContent);
    content.push(...addNonEmptyRichText(ufComment));

    return {
      header: $localize`Ziel ${index + 1}\: ${objective.name}`,
      content,
    };
  }

  /**
   * Generate pdf-Content for the one single conducted tradeoff.
   *
   * @param tradeoffObjective - the first objective in the tradeoff (for the header)
   * @param currentObjective - the second objective in the tradeoff (for the header)
   * @param explanation - the actual content to render
   */
  private generateTradeoffInformation(
    tradeoffObjective: Objective,
    currentObjective: Objective,
    explanation: string,
  ): { header: string; content: Content[] } {
    return {
      header: $localize`Trade-off zwischen ${tradeoffObjective.name} und ${currentObjective.name}`,
      content: [parseRichText(explanation)],
    };
  }

  /**
   * Generate the pdf-Table that shows the result of the robustness check
   *
   * @param robustnessCheckData - the data to include
   * @param showRobustnessCheckColumn - whether to show the data in a column
   */
  private generateResultsTable(
    robustnessCheckData: Array<{ minUtility: number; maxUtility: number; score: number }>,
    showRobustnessCheckColumn: boolean,
  ): Content {
    const format = (value: number) => formatNumberSafe(value, this.locale, '?', '1.3-3');

    const decisionData = this.decisionData;
    const expectedUtilities = decisionData.getAlternativeUtilities();
    const sortedIndices = robustnessCheckData
      .map((value, index) => ({ index, score: value.score }))
      .sort((a, b) => a.score - b.score)
      .map(val => val.index);

    if (showRobustnessCheckColumn) {
      return {
        layout: 'lightHorizontalLines',
        alignment: 'left',
        table: {
          headerRows: 2,
          widths: ['*', 'auto', 'auto', 'auto'],
          body: [
            [
              { text: $localize`Alternative`, rowSpan: 2, colSpan: 2, bold: true },
              { text: $localize`Gesamtnutzen`, rowSpan: 2, colSpan: 2, bold: true },
              { text: $localize`Robustheitstest`, colSpan: 2, bold: true },
              {},
            ],
            [{}, {}, { text: $localize`Bandbreite`, bold: true }, { text: $localize`ø Rang`, bold: true }],
            ...sortedIndices.map(index => [
              { text: decisionData.alternatives[index].name },
              { text: `${format(expectedUtilities[index])}` },
              {
                text: `${format(robustnessCheckData[index].minUtility)} - ${format(robustnessCheckData[index].maxUtility)}`,
                noWrap: true,
              },
              { text: `${format(robustnessCheckData[index].score)}` },
            ]),
          ],
        },
      };
    } else {
      return {
        layout: 'lightHorizontalLines',
        alignment: 'left',
        table: {
          headerRows: 1,
          widths: ['*', 'auto'],
          body: [
            [
              { text: $localize`Alternative`, bold: true },
              { text: $localize`Gesamtnutzen`, bold: true },
            ],
            ...sortedIndices.map(index => [
              { text: decisionData.alternatives[index].name },
              { text: `${format(expectedUtilities[index])}` },
            ]),
          ],
        },
      };
    }
  }
}
