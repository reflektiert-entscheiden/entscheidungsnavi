import { Component, computed, DestroyRef, EventEmitter, inject, input, Output } from '@angular/core';
import { ProjectDto } from '@entscheidungsnavi/api-client';
import { noop, sortBy } from 'lodash';
import { TeamProjectInfoDto } from '@entscheidungsnavi/api-types';
import { typeGuardFor } from '@entscheidungsnavi/tools/type-guard-for';
import { first } from 'rxjs';
import { ProjectService } from '../../../data/project';

@Component({
  selector: 'dt-recently-edited-projects',
  templateUrl: './recently-edited-projects.component.html',
  styleUrls: ['./recently-edited-projects.component.scss'],
})
export class RecentlyEditedProjectsComponent {
  // In each list we will show only the most recent 5 projects.
  static readonly MAX_RECENTLY_EDITED = 5;

  projects = input.required<(ProjectDto | TeamProjectInfoDto)[]>();
  sortedProjects = computed(() => sortBy(this.projects(), project => project.updatedAt.getTime()).reverse());

  recentlyEditedSingleProjects = computed(() =>
    this.sortedProjects().filter(typeGuardFor(ProjectDto)).slice(0, RecentlyEditedProjectsComponent.MAX_RECENTLY_EDITED),
  );

  recentlyEditedTeamProjects = computed(() =>
    this.sortedProjects().filter(typeGuardFor(TeamProjectInfoDto)).slice(0, RecentlyEditedProjectsComponent.MAX_RECENTLY_EDITED),
  );

  @Output() projectLoaded = new EventEmitter<void>();

  projectService = inject(ProjectService);
  destroyRef = inject(DestroyRef);

  loadHistoryProject(project: ProjectDto | TeamProjectInfoDto) {
    const loadProject$ =
      project instanceof ProjectDto ? this.projectService.loadOnlineProject(project.id) : this.projectService.loadTeamProject(project.id);

    loadProject$.pipe(first()).subscribe({
      next: () => {
        this.projectLoaded.emit();
      },
      error: noop,
    });
  }
}
