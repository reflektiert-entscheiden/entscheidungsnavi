import { Component, Inject } from '@angular/core';
import { takeUntilDestroyed } from '@angular/core/rxjs-interop';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Sort } from '@angular/material/sort';
import { noop, throttle, without } from 'lodash';
import { MAT_DIALOG_DATA, MatDialog, MatDialogRef } from '@angular/material/dialog';
import { filter, switchMap } from 'rxjs/operators';
import {
  ConfirmModalComponent,
  ConfirmModalData,
  PersistentSetting,
  PersistentSettingParent,
  SnackbarComponent,
  SnackbarData,
  Tag,
} from '@entscheidungsnavi/widgets';
import { DatePipe } from '@angular/common';
import { AuthService, ProjectDto, QuickstartProjectDto, QuickstartService, QuickstartTagDto } from '@entscheidungsnavi/api-client';
import { OverlayProgressBarDirective } from '@entscheidungsnavi/widgets/overlay-progress-bar';
import { TeamProjectInfoWithRoleDto } from '@entscheidungsnavi/api-types';
import { plainToClass } from 'class-transformer';
import { ProjectLinkModalComponent } from '../project-link-modal/project-link-modal.component';
import { LocalProject, OnlineProject, ProjectService } from '../../../data/project';
import { RenameProjectModalComponent } from '../rename-project-modal/rename-project-modal.component';
import { OnlineProjectManagementService } from '../project-management.service';

type ProjectListEntry = QuickstartProjectDto | ProjectDto | TeamProjectInfoWithRoleDto;

export type ProjectListModalData = { quickstart: boolean; projectList: ProjectListEntry[]; query?: string; tagFilter?: string[] };

@Component({
  templateUrl: './project-list-modal.component.html',
  styleUrls: ['./project-list-modal.component.scss'],
  hostDirectives: [OverlayProgressBarDirective],
})
@PersistentSettingParent('ProjectListModal')
export class ProjectListModalComponent {
  projects: ProjectListEntry[];
  quickstart: boolean;

  @PersistentSetting()
  private filterTextTemplates: string;
  @PersistentSetting()
  private filterTextUserProjects: string;

  get filterText() {
    return this.quickstart ? this.filterTextTemplates : this.filterTextUserProjects;
  }

  set filterText(value: string) {
    if (this.quickstart) {
      this.filterTextTemplates = value;
    } else {
      this.filterTextUserProjects = value;
    }
  }

  @PersistentSetting()
  filterTagIds: string[] = [];

  filteredProjects: ProjectListEntry[] = [];
  updateProjectsAndTags: () => void;

  activeSorting: Sort;

  tags: Tag[] = [];

  tagsById: { [id: string]: QuickstartTagDto };

  openOnlineProjectId: string;

  constructor(
    private authService: AuthService,
    private snackbar: MatSnackBar,
    @Inject(MAT_DIALOG_DATA) data: ProjectListModalData,
    private dialogRef: MatDialogRef<ProjectListModalComponent>,
    private projectService: ProjectService,
    private projectManagementService: OnlineProjectManagementService,
    private dialog: MatDialog,
    private date: DatePipe,
    quickstartService: QuickstartService,
    private progressDirective: OverlayProgressBarDirective,
  ) {
    this.quickstart = data.quickstart;
    this.projects = data.projectList;

    this.filterTextTemplates = data.query ?? '';
    this.filterTextUserProjects = '';

    if (!this.quickstart) {
      this.authService.onLogout$.pipe(takeUntilDestroyed()).subscribe(() => {
        this.dialogRef.close();
      });
    }

    this.activeSorting = this.quickstart ? { active: 'name', direction: 'asc' } : { active: 'date', direction: 'desc' };

    this.projectService.project$
      .pipe(takeUntilDestroyed())
      .subscribe(project => (this.openOnlineProjectId = project instanceof OnlineProject ? project.info.id : null));

    this.updateProjectsAndTags = throttle(() => {
      let list = this.filterText
        ? this.projects.filter(project => project.name.toLocaleLowerCase().includes(this.filterText.toLocaleLowerCase()))
        : this.projects;
      list = list.filter(project => this.filterTagIds.every(tag => project instanceof QuickstartProjectDto && project.tags.includes(tag)));

      this.filterTagIds = [...this.filterTagIds];

      this.filteredProjects = this.sort(list);

      this.tags.forEach(
        tag => (tag.isProductive = this.filteredProjects.some(p => p instanceof QuickstartProjectDto && p.tags.includes(tag.id))),
      );
    }, 50);

    if (data.quickstart) {
      quickstartService.getTags().subscribe({
        next: tags => {
          this.tags = tags;
          this.tagsById = tags.reduce(
            (prev, curr) => {
              prev[curr.id] = curr;
              return prev;
            },
            {} as { [key: string]: Tag },
          );

          this.filterTagIds = data.tagFilter ?? [];

          this.updateProjectsAndTags();
        },
        error: error => console.error(error),
      });
    }

    this.updateProjectsAndTags();
  }

  isTeamProject(project: unknown): project is TeamProjectInfoWithRoleDto {
    return project instanceof TeamProjectInfoWithRoleDto;
  }

  closeModal(loadedProject: boolean) {
    this.dialogRef.close(loadedProject);
  }

  sort(projectList: ProjectListEntry[]) {
    return projectList.sort((a, b) => {
      let result = 0;
      if (this.activeSorting.active === 'name') {
        result = a.name.localeCompare(b.name);
      } else if (this.activeSorting.active === 'date') {
        result = a.updatedAt.getTime() - b.updatedAt.getTime();
      }

      return this.activeSorting.direction === 'asc' ? result : -result;
    });
  }

  changeSorting(sort: Sort) {
    this.activeSorting = sort;
    this.updateProjectsAndTags();
  }

  submitSearch(event: Event) {
    event.preventDefault();
    if (this.filteredProjects.length === 1) {
      this.loadProject(this.filteredProjects[0]);
    }
  }

  loadProject(project: ProjectListEntry, createCopy = false) {
    const loadObservable =
      project instanceof QuickstartProjectDto
        ? this.projectService.loadQuickstartProject(project.id)
        : project instanceof TeamProjectInfoWithRoleDto
          ? this.projectService.loadTeamProject(project.id, { loadAsLocal: createCopy })
          : this.projectService.loadOnlineProject(project.id, { loadAsLocal: createCopy });

    loadObservable.subscribe({
      next: success => {
        if (!success) return;

        this.closeModal(true);

        if (createCopy) {
          const project = this.projectService.getProject(LocalProject);
          project.rename(project.name + ' ' + $localize`(Kopie)`).subscribe();
        }
      },
      error: noop,
    });
  }

  renameProject(project: ProjectDto) {
    this.dialog.open(RenameProjectModalComponent, { data: { project, projectList: this.projects } });
  }

  deleteProject(project: ProjectDto) {
    this.dialog
      .open<ConfirmModalComponent, ConfirmModalData>(ConfirmModalComponent, {
        data: {
          title: $localize`Projekt Löschen`,
          prompt: $localize`Bist Du sicher, dass Du das Projekt „${project.name}“ vom ${this.date.transform(
            project.updatedAt,
          )} löschen möchten?`,
          template: 'delete',
        },
      })
      .afterClosed()
      .pipe(
        filter(Boolean),
        switchMap(() => this.projectManagementService.delete(project.id)),
      )
      .subscribe({
        next: () => {
          // Remove the project from our list
          this.projects.splice(
            this.projects.findIndex(pr => pr.id === project.id),
            1,
          );
          this.updateProjectsAndTags();
        },
        error: () => {
          this.snackbar.openFromComponent<SnackbarComponent, SnackbarData>(SnackbarComponent, {
            data: {
              message: $localize`Projekt konnte nicht gelöscht werden. Möglicherweise ist Deine Internetverbindung instabil.`,
              icon: 'error',
            },
            duration: 8000,
          });
        },
      });
  }

  createTeam(project: ProjectDto) {
    return this.projectManagementService
      .createTeamFromOnlineProject(project.id, $localize`${project.name} (Team)`)
      .pipe(this.progressDirective.reactivePipe())
      .subscribe({
        next: team => {
          const teamWithRole: TeamProjectInfoWithRoleDto = plainToClass(TeamProjectInfoWithRoleDto, { ...team, userMemberRole: 'owner' });
          this.projects.push(teamWithRole);
          this.updateProjectsAndTags();

          this.loadProject(teamWithRole);
        },
        error: () => {
          this.snackbar.open($localize`Team konnte nicht erstellt werden. Möglicherweise ist Deine Internetverbindung instabil.`, 'Ok', {
            duration: 5000,
          });
        },
      });
  }

  leaveTeam(project: TeamProjectInfoWithRoleDto) {
    this.projectManagementService
      .leaveTeam(project.id)
      .pipe(this.progressDirective.reactivePipe())
      .subscribe({
        next: () => {
          this.projects.splice(
            this.projects.findIndex(pr => pr.id === project.id),
            1,
          );
          this.updateProjectsAndTags();
        },
        error: () => {
          this.snackbar.open(
            $localize`Das Team konnte nicht verlassen werden. Als letztes echtes Teammitglied kannst Du das Team nur löschen.`,
            'Ok',
            { duration: 5000 },
          );
        },
      });
  }

  getQuickstartProjectLink(project: QuickstartProjectDto): string {
    return project.shareUrl;
  }

  createProjectLink(project: ProjectDto) {
    this.dialog.open(ProjectLinkModalComponent, { data: { project } });
  }

  onTagClick(tag: QuickstartTagDto) {
    if (!this.filterTagIds.includes(tag.id)) {
      this.filterTagIds.push(tag.id);
    } else {
      this.filterTagIds = without(this.filterTagIds, tag.id);
    }

    this.updateProjectsAndTags();
  }
}
