import { EnvironmentInjector, Injectable, runInInjectionContext } from '@angular/core';
import { OnlineProjectsService, TeamsService } from '@entscheidungsnavi/api-client';
import { concatMap, defer, map, mergeMap, Observable, of, tap } from 'rxjs';
import { DecisionDataExportService } from '../../data/decision-data-export.service';
import { LocalProject, OnlineProject, ProjectService } from '../../data/project';
import { checkOnlineProjectSize, saveTimeout } from '../../data/project/save-notification';
import { TrackingService } from '../../data/tracking.service';
import { TeamProject } from '../../data/project/team-project';

/**
 * A helper service to manages the users online projects.
 * It handles the case where the currently opened project is modified gracefully.
 */
@Injectable({ providedIn: 'root' })
export class OnlineProjectManagementService {
  constructor(
    private projectService: ProjectService,
    private exportService: DecisionDataExportService,
    private teamsService: TeamsService,
    private onlineProjectsService: OnlineProjectsService,
    private trackingService: TrackingService,
    private injector: EnvironmentInjector,
  ) {}

  /**
   * Returns the currently opened online project with this ID, or null, if this project is not opened.
   *
   * @param projectId - The project id
   * @returns The project or null
   */
  private getCurrentProject(projectId: string) {
    const activeProject = this.projectService.getProject(OnlineProject);

    if (activeProject && activeProject.info.id === projectId) {
      return activeProject;
    } else {
      return null;
    }
  }

  rename(projectId: string, newName: string): Observable<void> {
    this.trackingService.trackEvent('rename project', { category: 'project' });

    const activeProject = this.getCurrentProject(projectId);

    if (activeProject) {
      return activeProject.rename(newName);
    } else {
      return this.onlineProjectsService.renameProject(projectId, newName).pipe(map(() => null));
    }
  }

  delete(projectId: string) {
    this.trackingService.trackEvent('delete project', { category: 'project' });

    return this.onlineProjectsService.deleteProject(projectId).pipe(
      tap(() => {
        const activeProject = this.getCurrentProject(projectId);

        if (activeProject) {
          const newProject = runInInjectionContext(this.injector, () => new LocalProject('local', activeProject.name));
          this.projectService.project$.next(newProject);
        }
      }),
    );
  }

  saveAsNewProject(newName: string) {
    const data = this.exportService.dataToText(null);

    this.trackingService.trackEvent('save as new project', { category: 'project' });

    const saveObservable = this.onlineProjectsService.addProject(newName, data);

    return of(data).pipe(
      checkOnlineProjectSize(),
      mergeMap(() => saveObservable.pipe(saveTimeout())),
      map(project => {
        const newProject = runInInjectionContext(this.injector, () => new OnlineProject({ ...project, data }));
        this.projectService.project$.next(newProject);
        return newProject;
      }),
    );
  }

  saveAsExistingProject(projectId: string) {
    const data = this.exportService.dataToText(null);

    const activeProject = this.projectService.getProject(OnlineProject);

    if (projectId && activeProject?.info.id === projectId) {
      return activeProject.save('save-as').pipe(map(() => activeProject));
    } else {
      this.trackingService.trackEvent('save as existing project', { category: 'project' });

      const saveObservable = this.onlineProjectsService.updateProject(projectId, { data, dataSaveType: 'save-as' });

      return of(data).pipe(
        checkOnlineProjectSize(),
        mergeMap(() => saveObservable.pipe(saveTimeout())),
        map(project => {
          const newProject = runInInjectionContext(this.injector, () => new OnlineProject({ ...project, data }));
          this.projectService.project$.next(newProject);
          return newProject;
        }),
      );
    }
  }

  createTeamFromOnlineProject(projectId: string, name: string) {
    this.trackingService.trackEvent('create team from project', { category: 'project' });

    return defer(() => {
      const activeProject = this.projectService.getProject(OnlineProject);

      if (activeProject?.info.id === projectId) {
        return of(this.exportService.dataToText(null));
      } else {
        return this.onlineProjectsService.getProject(projectId).pipe(map(project => project.data));
      }
    }).pipe(concatMap(data => this.teamsService.createTeam(name, data)));
  }

  leaveTeam(id: string) {
    this.trackingService.trackEvent('leave team', { category: 'project' });

    return this.teamsService.leave(id).pipe(
      tap(() => {
        const activeProject = this.projectService.getProject(TeamProject);

        if (activeProject?.team.id === id) {
          const newProject = runInInjectionContext(this.injector, () => new LocalProject('local', activeProject.team.name));
          this.projectService.project$.next(newProject);
        }
      }),
    );
  }
}
