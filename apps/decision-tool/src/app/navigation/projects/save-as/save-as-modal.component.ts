import { Component, DestroyRef, ElementRef, Inject, OnInit, ViewChild } from '@angular/core';
import { NonNullableFormBuilder, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialog, MatDialogRef } from '@angular/material/dialog';
import { ConfirmModalComponent, ConfirmModalData, PopOverService, SnackbarComponent, SnackbarData } from '@entscheidungsnavi/widgets';
import { OnlineProjectsService, ProjectDto } from '@entscheidungsnavi/api-client';
import { firstValueFrom, of } from 'rxjs';
import { Sort } from '@angular/material/sort';
import { MatSnackBar } from '@angular/material/snack-bar';
import { takeUntilDestroyed } from '@angular/core/rxjs-interop';
import { saveErrorToMessage } from '../../../data/project/save-notification';
import { OnlineProjectManagementService } from '../project-management.service';
import { ProjectService } from '../../../data/project';

@Component({
  templateUrl: './save-as-modal.component.html',
  styleUrls: ['./save-as-modal.component.scss'],
})
export class SaveAsModalComponent implements OnInit {
  @ViewChild('saveButton', { read: ElementRef }) private saveButton: ElementRef<HTMLElement>;

  readonly newProjectForm = this.fb.group({
    name: [this.projectService.getProjectName(), Validators.required],
  });

  private projectList: ProjectDto[];
  projectNameFilter = '';
  activeProjectSort: Sort = { active: 'updatedAt', direction: 'desc' };
  visibleProjects: ProjectDto[];

  get existingProject() {
    return this.projectList.find(project => project.name.toLowerCase() === this.newProjectForm.value.name.toLowerCase());
  }

  get selectedProjectIndex() {
    return this.visibleProjects?.indexOf(this.existingProject);
  }

  state: 'loading' | 'load-error' | 'idle' | 'saving' = 'loading';

  constructor(
    private dialogRef: MatDialogRef<SaveAsModalComponent>,
    private snackBar: MatSnackBar,
    private dialog: MatDialog,
    private managementService: OnlineProjectManagementService,
    onlineProjectsService: OnlineProjectsService,
    private popoverService: PopOverService,
    private fb: NonNullableFormBuilder,
    @Inject(MAT_DIALOG_DATA) projectList: ProjectDto[],
    private projectService: ProjectService,
    private destroyRef: DestroyRef,
  ) {
    (projectList ? of(projectList) : onlineProjectsService.getProjectList()).subscribe({
      next: list => {
        this.projectList = list;
        this.updateVisibleProjects();
        this.state = 'idle';
      },
      error: () => (this.state = 'load-error'),
    });
  }

  ngOnInit() {
    this.newProjectForm.markAllAsTouched();
  }

  updateVisibleProjects() {
    this.visibleProjects = this.projectList
      .filter(project => project.name.toLowerCase().includes(this.projectNameFilter.toLowerCase()))
      .sort((a, b) => {
        const compareValue =
          this.activeProjectSort.active === 'name' ? a.name.localeCompare(b.name) : a.updatedAt.getTime() - b.updatedAt.getTime();
        return compareValue * (this.activeProjectSort.direction === 'asc' ? 1 : -1);
      });
  }

  closeModal() {
    this.dialogRef.close();
  }

  async saveAs() {
    if (this.state !== 'idle' || this.newProjectForm.invalid) {
      return;
    }

    const newName = this.newProjectForm.value.name;

    const existingProject = this.existingProject;
    if (
      existingProject &&
      !(await firstValueFrom(
        this.dialog
          .open<ConfirmModalComponent, ConfirmModalData>(ConfirmModalComponent, {
            data: {
              prompt: $localize`Bist Du sicher, dass Du das Projekt „${existingProject.name}“ überschreiben möchtest?`,
              title: $localize`Projekt überschreiben`,
              buttonConfirm: $localize`Ja, überschreiben`,
            },
          })
          .afterClosed(),
      ))
    ) {
      return;
    }

    this.state = 'saving';
    (existingProject ? this.managementService.saveAsExistingProject(existingProject.id) : this.managementService.saveAsNewProject(newName))
      .pipe(takeUntilDestroyed(this.destroyRef))
      .subscribe({
        next: project => {
          this.dialogRef.close(project.info);
          this.snackBar.openFromComponent<SnackbarComponent, SnackbarData>(SnackbarComponent, {
            data: { message: $localize`Erfolgreich gespeichert`, icon: 'done' },
            duration: 2500,
          });
        },
        error: error => {
          this.state = 'idle';
          this.popoverService.whistle(this.saveButton, saveErrorToMessage(error), 'error', 5000);
        },
      });
  }
}
