import { Component, Inject } from '@angular/core';
import { NonNullableFormBuilder, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { ProjectDto } from '@entscheidungsnavi/api-client';
import { notOneOfValidator } from '@entscheidungsnavi/widgets';
import { OnlineProjectManagementService } from '../project-management.service';

@Component({
  templateUrl: './rename-project-modal.component.html',
  styleUrls: ['./rename-project-modal.component.scss'],
})
export class RenameProjectModalComponent {
  formGroup = this.fb.group({
    name: [
      this.data.project.name,
      [
        Validators.required,
        notOneOfValidator(
          this.data.projectList.filter(project => project.id !== this.data.project.id).map(project => project.name),
          false,
        ),
      ],
    ],
  });

  constructor(
    private dialogRef: MatDialogRef<RenameProjectModalComponent>,
    @Inject(MAT_DIALOG_DATA) private data: { project: ProjectDto; projectList: ProjectDto[] },
    private projectManagementService: OnlineProjectManagementService,
    private fb: NonNullableFormBuilder,
  ) {}

  save() {
    this.formGroup.updateValueAndValidity();
    if (this.formGroup.invalid) {
      return;
    }

    const newName = this.formGroup.get('name').value;
    this.formGroup.disable({ emitEvent: false });

    this.projectManagementService.rename(this.data.project.id, newName).subscribe({
      next: () => {
        this.data.project.name = newName;
        this.dialogRef.close();
      },
      error: () => {
        this.formGroup.enable({ emitEvent: false });
        this.formGroup.setErrors({ 'server-error': true });
      },
    });
  }

  close() {
    if (this.formGroup.disabled) {
      return;
    }

    this.dialogRef.close();
  }
}
