import { Component, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ConfirmModalComponent, SnackbarComponent, SnackbarData } from '@entscheidungsnavi/widgets';
import {
  AuthService,
  OnlineProjectsService,
  ProjectDto,
  QuickstartProjectDto,
  QuickstartService,
  TeamsService,
} from '@entscheidungsnavi/api-client';
import { TeamProjectInfoWithRoleDto } from '@entscheidungsnavi/api-types';
import { OnDestroyObservable } from '@entscheidungsnavi/tools';
import { Observable, firstValueFrom, forkJoin, takeUntil } from 'rxjs';
import { ProjectListModalComponent, SaveAsModalComponent } from '..';
import { ProjectService } from '../../data/project';
import { ExcelExportService } from '../../excel-export';
import { NewProjectModalComponent } from '../../main/start';
import { PdfExportModalComponent } from '../../pdf-export-modal/pdf-export-modal.component';
import { JsonErrorModalComponent } from '../../../modules/shared/error/json-error-modal/json-error-modal.component';
import { LoginModalComponent } from '../../main/userarea';
import { ProjectListModalData } from './project-list-modal/project-list-modal.component';

@Component({
  templateUrl: 'projects-modal.component.html',
  styleUrls: ['./projects-modal.component.scss'],
})
export class ProjectsModalComponent implements OnInit {
  @OnDestroyObservable()
  private onDestroy$: Observable<void>;

  isOffline = false;
  isLoggedIn = false;

  quickstartProjects: QuickstartProjectDto[];
  onlineProjects: ProjectDto[];
  teamProjects: TeamProjectInfoWithRoleDto[];

  onlineAndTeamProjects: (ProjectDto | TeamProjectInfoWithRoleDto)[];

  dragCounter = 0;
  showDragArea = false;

  constructor(
    private authService: AuthService,
    private quickstartService: QuickstartService,
    private onlineProjectsService: OnlineProjectsService,
    private dialog: MatDialog,
    private dialogRef: MatDialogRef<ProjectsModalComponent>,
    private projectService: ProjectService,
    private excelExport: ExcelExportService,
    private snackBar: MatSnackBar,
    private teamsService: TeamsService,
  ) {
    this.authService.loggedIn$.pipe(takeUntil(this.onDestroy$)).subscribe(loggedIn => (this.isLoggedIn = loggedIn));
  }

  ngOnInit() {
    // Load Projects
    this.quickstartService.getProjects().subscribe({
      next: list => (this.quickstartProjects = list),
      error: () => (this.isOffline = true),
    });

    if (this.isLoggedIn) {
      forkJoin([this.onlineProjectsService.getProjectList(), this.teamsService.getTeamProjects()]).subscribe({
        next: ([onlineProjects, teamProjects]) => {
          this.onlineProjects = onlineProjects;
          this.teamProjects = teamProjects;
          this.onlineAndTeamProjects = [...onlineProjects, ...teamProjects];
        },
        error: () => (this.isOffline = true),
      });
    }
  }

  newProject() {
    this.dialog
      .open(NewProjectModalComponent)
      .beforeClosed()
      .subscribe(createdProject => {
        if (createdProject) {
          this.closeModal();
        }
      });
  }

  loadUserProject() {
    if (this.isOffline || this.onlineProjects == null) {
      return;
    }

    if (!this.isLoggedIn) {
      this.dialog.open(LoginModalComponent);
    } else {
      this.dialog
        .open<ProjectListModalComponent, ProjectListModalData>(ProjectListModalComponent, {
          data: { quickstart: false, projectList: this.onlineAndTeamProjects },
        })
        .beforeClosed()
        .subscribe((loadedProject: boolean) => {
          if (loadedProject) {
            this.closeModal();
          }
        });
    }
  }

  loadQuickstartProject() {
    if (this.isOffline || this.quickstartProjects == null) {
      return;
    }

    this.dialog
      .open<ProjectListModalComponent, ProjectListModalData>(ProjectListModalComponent, {
        data: { quickstart: true, projectList: this.quickstartProjects },
      })
      .beforeClosed()
      .subscribe((loadedProject: boolean) => {
        if (loadedProject) {
          this.closeModal();
        }
      });
  }

  projectInputChange(input: HTMLInputElement) {
    if (input.files.length !== 1) {
      return;
    }
    const file = input.files[0];
    this.loadProjectFile(file);

    input.value = '';
  }

  private loadProjectFile(file: File) {
    this.projectService.importFile(file).subscribe({
      next: () => {
        this.closeModal();
      },
      error: () => {
        this.dialog.open(JsonErrorModalComponent);
      },
    });
  }

  closeModal() {
    this.dialogRef.close();
  }

  isProjectLoaded() {
    return this.projectService.isProjectLoaded();
  }

  save() {
    if (this.isOffline) {
      this.snackBar.openFromComponent<SnackbarComponent, SnackbarData>(SnackbarComponent, {
        duration: 5000,
        data: {
          message: $localize`Du kannst kein Projekt im Account speichern, da Du offline bist.`,
          icon: 'error',
        },
      });
      return;
    }

    if (!this.isProjectLoaded() || !this.isLoggedIn) {
      return;
    }

    this.dialog
      .open(SaveAsModalComponent, { data: this.onlineProjects })
      .afterClosed()
      .subscribe(project => {
        if (project) {
          this.onlineProjects.splice(
            this.onlineProjects.findIndex(p => p.id === project.id),
            1,
          );
          this.onlineProjects.push(project);
        }
      });
  }

  exportProject(as: 'json' | 'excel' | 'pdf') {
    switch (as) {
      case 'json':
        this.projectService.getProject().exportFile();
        break;
      case 'excel':
        this.excelExport.performExcelExport(false);
        break;
      case 'pdf':
        this.dialog.open(PdfExportModalComponent);
        break;
    }
  }

  dragEnter() {
    this.dragCounter++;
    this.showDragArea = true;
  }

  dragLeave() {
    this.dragCounter--;

    if (this.dragCounter === 0) {
      this.showDragArea = false;
    }
  }

  drop(event: DragEvent) {
    this.showDragArea = false;
    this.dragCounter = 0;

    event.preventDefault();

    if (event.dataTransfer.items.length !== 1) {
      this.dialog.open(ConfirmModalComponent, {
        data: {
          title: $localize`Fehler`,
          prompt: $localize`Es kann nur eine Projektdatei auf einmal geladen werden.`,
          buttonDeny: 'Okay',
          size: 'small',
        },
      });
      return;
    }

    const item = event.dataTransfer.items[0];
    if (item.kind !== 'file' || item.getAsFile().type !== 'application/json') {
      this.dialog.open(ConfirmModalComponent, {
        data: {
          title: $localize`Fehler`,
          prompt: $localize`Es können nur Projektdateien geladen werden.`,
          buttonDeny: 'Okay',
          size: 'small',
        },
      });
      return;
    }

    const file = item.getAsFile();
    this.loadProjectFile(file);
  }

  dragOver(event: DragEvent) {
    event.preventDefault();
    event.dataTransfer.dropEffect = 'move';
  }

  async closeProject() {
    if (!this.isProjectLoaded()) {
      return;
    }

    if (await firstValueFrom(this.projectService.closeProject())) {
      this.closeModal();
    }
  }

  feedback() {
    window.open('https://entscheidungsnavi.de/contact', '_blank', 'noopener');
  }
}
