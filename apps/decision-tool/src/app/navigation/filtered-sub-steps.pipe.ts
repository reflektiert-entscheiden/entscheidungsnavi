import { Pipe, PipeTransform } from '@angular/core';
import { NaviStep, NaviStepNames, ProjectMode } from '@entscheidungsnavi/decision-data';

import { NavigationStepMetaData } from '../../modules/shared/navigation/educational-navigation';

@Pipe({ name: 'filteredSubSteps' })
export class FilteredSubStepsPipe implements PipeTransform {
  transform(step: NavigationStepMetaData & NaviStepNames & { id: NaviStep }, projectMode: ProjectMode): { name: string; index: number }[] {
    if (projectMode === 'educational') {
      return step.subSteps.map((name, index) => ({ name, index }));
    } else if (step.id === 'results') {
      return step.subSteps.map((name, index) => ({ name, index })).slice(1);
    } else {
      return [];
    }
  }
}
