import { ChangeDetectorRef, Component, ElementRef, EventEmitter, Input, Output, ViewChild } from '@angular/core';
import { DecisionData, NAVI_STEP_ORDER, NaviStep, NaviSubStep, TeamUUIDs } from '@entscheidungsnavi/decision-data';
import { Observable, takeUntil } from 'rxjs';
import { OnDestroyObservable } from '@entscheidungsnavi/tools';
import { subStepToNumber } from '@entscheidungsnavi/decision-data/progress';
import { EducationalNavigationService } from '../../modules/shared/navigation/educational-navigation.service';
import { ProjectService } from '../data/project';
import { LanguageService } from '../data/language.service';
import { DecisionDataState } from '../../modules/shared/decision-data-state';

@Component({
  selector: 'dt-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.scss'],
})
export class NavigationComponent {
  steps = NAVI_STEP_ORDER.map(stepId => ({
    id: stepId,
    ...this.languageService.steps[stepId],
  }));

  @Input() isUnfolded = true;

  @Input() isSidenavOver: boolean;

  @Output() unfoldButtonClick = new EventEmitter<void>();

  @ViewChild('decisionStatementContainer') decisionStatementContainer: ElementRef<HTMLElement>;

  @OnDestroyObservable()
  onDestroy$: Observable<void>;

  // eslint-disable-next-line @typescript-eslint/naming-convention
  TeamUUIDs = TeamUUIDs;

  constructor(
    protected decisionData: DecisionData,
    protected languageService: LanguageService,
    private educationalNavigationService: EducationalNavigationService,
    private cdRef: ChangeDetectorRef,
    protected projectService: ProjectService,
    protected decisionDataState: DecisionDataState,
  ) {
    this.educationalNavigationService.currentProgressUpdate$.pipe(takeUntil(this.onDestroy$)).subscribe(() => {
      this.cdRef.detectChanges();
    });
  }

  isSubStepDone(step: NaviSubStep) {
    return subStepToNumber(step) < subStepToNumber(this.educationalNavigationService.currentProgress);
  }

  isSubStepCurrent(step: NaviSubStep) {
    return subStepToNumber(step) === subStepToNumber(this.educationalNavigationService.currentProgress);
  }

  toStep(step: NaviStep) {
    this.educationalNavigationService.navigateToStep(step);
  }

  isProjectLoaded(): boolean {
    return this.projectService.isProjectLoaded();
  }

  toggleUnfold() {
    this.unfoldButtonClick.emit();
  }
}
