import { Injectable, isDevMode, ApplicationRef, NgZone, TemplateRef } from '@angular/core';
import { filter, fromEvent } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class DebugService {
  isDebugEnabled = isDevMode();
  isDevMode = isDevMode();

  fleetingComponentDebugTemplates: { componentInstance: any; template: TemplateRef<any> }[];

  constructor(
    private appRef: ApplicationRef,
    zone: NgZone,
  ) {
    window.enableDTDebug = () => {
      zone.run(() => {
        this.enableDebug();
        this.appRef.tick();
      });
    };

    zone.runOutsideAngular(() => {
      fromEvent(window, 'keydown')
        .pipe(filter((event: KeyboardEvent) => event.key === 'a' && event.ctrlKey && event.altKey))
        .subscribe(() => zone.run(() => this.enableDebug()));
    });

    this.fleetingComponentDebugTemplates = [];
  }

  removeFleetingTemplate(componentInstance: any) {
    if (!this.isDebugEnabled) {
      return;
    }

    const index = this.fleetingComponentDebugTemplates.findIndex(o => o.componentInstance === componentInstance);

    if (index >= 0) {
      this.fleetingComponentDebugTemplates.splice(index, 1);
    }
  }

  registerFleetingTemplate(componentInstance: any, template: TemplateRef<any>) {
    if (!this.isDebugEnabled) {
      return;
    }

    setTimeout(() => {
      this.fleetingComponentDebugTemplates.push({ componentInstance, template });
    });
  }

  enableDebug() {
    this.isDebugEnabled = true;
  }

  disableDebug() {
    this.isDebugEnabled = false;
  }
}
