import { Component, OnInit } from '@angular/core';
import { DecisionData, NAVI_STEP_ORDER } from '@entscheidungsnavi/decision-data';
import { UntypedFormControl, UntypedFormGroup } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { MatRadioChange } from '@angular/material/radio';
import { map } from 'rxjs';
import { EducationalNavigationService } from '../../modules/shared/navigation/educational-navigation.service';
import { TimeTrackingService } from '../data/time-tracking.service';
import { ENVIRONMENT } from '../../environments/environment';
import { ServiceWorkerService, UpdateState } from '../../services/service-worker.service';
import { ProjectService } from '../data/project';
import { NightlyWarningModalComponent } from '../nightly/nightly-warning-modal/nightly-warning-modal.component';
import { ModeTransitionService } from '../../modules/shared/mode-transition/mode-transition.service';
import { EnvironmentType } from '../../environments/environment-types';
import { AutoSaveService } from '../data/project/auto-save.service';
import { DebugService } from './debug.service';

/**
 * Component Debug Templates:
 * - If the Component is the target of a route just implement the Debug Interface and set hasDebugTemplate to true.
 *   | Example: RobustheitstestComponent
 * - If the Component is not the target of a route call DebugService.registerFleetingTemplate in ngOnInit
 *   and DebugService.removeFleetingTemplate in ngOnDestroy with the debug template and the component instance.
 *   | Example:  ObjectiveAspectHierarchyComponent
 */
@Component({
  selector: 'dt-debug',
  templateUrl: './debug.component.html',
  styleUrls: ['./debug.component.scss'],
})
export class DebugComponent implements OnInit {
  open = false;

  modeRadioGroup: UntypedFormGroup;

  get environmentType() {
    return ENVIRONMENT.type;
  }
  set environmentType(value: EnvironmentType) {
    ENVIRONMENT.type = value;
  }

  get timeTrackingActive() {
    return this.timeTrackingService.timeTrackingEnabled;
  }

  lastDecisionDataChangeEventTime$ = this.timeTrackingService.decisionDataChanged().pipe(map(() => new Date()));

  get fleetingDebugTemplates() {
    return this.debugService.fleetingComponentDebugTemplates;
  }

  constructor(
    private modeTransitionService: ModeTransitionService,
    protected debugService: DebugService,
    private decisionData: DecisionData,
    private timeTrackingService: TimeTrackingService,
    protected currentProgressService: EducationalNavigationService,
    protected projectService: ProjectService,
    private dialog: MatDialog,
    protected serviceWorkerService: ServiceWorkerService,
    protected autoSaveService: AutoSaveService,
  ) {}

  ngOnInit() {
    {
      const modeControl = new UntypedFormControl(this.decisionData.projectMode);
      this.modeRadioGroup = new UntypedFormGroup({
        currentMode: modeControl,
      });

      this.decisionData.projectMode$.subscribe(mode => {
        modeControl.setValue(mode);
      });
    }
  }

  valueChange(change: MatRadioChange) {
    // Reset the default change to the target mode since we may not navigate there.
    this.modeRadioGroup.get('currentMode').reset(this.decisionData.projectMode);
    // TODO: Is there a better solution?
    change.source._inputElement.nativeElement.checked = false;

    const nextMode = change.value;

    if (nextMode === 'starter') {
      // Unsupported transition!
      console.warn('DebugComponent performed an unsupported transition.');
      this.decisionData.projectMode = 'starter';
    } else {
      let transition;

      if (nextMode === 'educational') {
        transition = this.modeTransitionService.transitionIntoEducational();
      } else {
        transition = this.modeTransitionService.transitionIntoProfessional();
      }

      transition.then(transitioned => {
        if (transitioned) this.modeRadioGroup.get('currentMode').setValue(this.decisionData.projectMode);
      });
    }
  }

  crash() {
    (null as any).crash();
  }

  dummyUpdateState(state: UpdateState) {
    this.serviceWorkerService.updateState$.next(state);
  }

  manuallyLogTimeRecordings() {
    const timers = this.decisionData.timeRecording.timers;
    for (const naviStep of NAVI_STEP_ORDER) {
      for (const naviSubStepIdx in timers[naviStep]) {
        console.log(
          naviStep +
            naviSubStepIdx +
            ': (A)' +
            timers[naviStep][naviSubStepIdx].activeTime.toFixed(2) +
            ' (I)' +
            timers[naviStep][naviSubStepIdx].totalTime.toFixed(2),
        );
      }
    }
    console.log('-----');
  }

  showNightlyWarning() {
    this.dialog.open(NightlyWarningModalComponent);
  }
}
