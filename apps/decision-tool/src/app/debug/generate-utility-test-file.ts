import { QuickstartService } from '@entscheidungsnavi/api-client';
import { readText, ObjectiveElement, ObjectiveAspects, DecisionStatement } from '@entscheidungsnavi/decision-data';
import { Tree } from '@entscheidungsnavi/tools';
import { concatMap, from, mergeMap, map, filter, tap, reduce } from 'rxjs';
import { DecisionDataExportService } from '../data/decision-data-export.service';

export function generateUtilityTestsFile(quickstartService: QuickstartService, decisionDataExportService: DecisionDataExportService) {
  quickstartService
    .getProjects()
    .pipe(
      concatMap(projects => from(projects)),
      mergeMap(project => quickstartService.getProject(project.id)),
      map(project => ({
        decisionData: readText(project.data),
        projectString: project.data,
        projectName: project.name,
      })),
      filter(({ decisionData }) => [null, 'results'].includes(decisionData.getFirstStepWithErrors())),
      // Fix weights
      tap(({ decisionData }) => {
        for (let objectiveIndex = 0; objectiveIndex < decisionData.objectives.length; objectiveIndex++) {
          const weight = decisionData.weights.getWeight(objectiveIndex);

          if (weight.value == null) {
            decisionData.weights.adjustPreliminaryWeightValue(objectiveIndex, 50);
          }
        }

        decisionData.weights.normalizePreliminaryWeights();
      }),
      map(({ decisionData, projectName }) => {
        if (decisionData.getFirstStepWithErrors() != null) {
          throw new Error('Fixing weights did not work');
        }

        const utilityValues = decisionData.getAlternativeUtilities();

        if (utilityValues.some(value => isNaN(value) || value == null)) {
          throw new Error('Project with invalid utility values');
        }

        return { decisionData, utilityValues, projectName };
      }),
      // Remove Stuff
      tap(({ decisionData }) => {
        let nameCounter = 1;
        deepIterate(decisionData, (key, value, path) => {
          if (typeof value === 'string' && value.startsWith(`{"ops":[`)) {
            setPropertyByPath(decisionData, path, '');
          }

          if (typeof value === 'string' && key === 'uuid') {
            setPropertyByPath(decisionData, path, '');
          }

          if (typeof value === 'string' && key === 'projectNotes') {
            setPropertyByPath(decisionData, path, '');
          }

          if (typeof value === 'string' && key === 'name') {
            setPropertyByPath(decisionData, path, '' + nameCounter++);
          }
        });

        for (const alternative of decisionData.alternatives) {
          const childCount = alternative.children.length;
          for (let i = 0; i < childCount; i++) {
            alternative.removeChild(0);
          }
        }

        for (const objective of decisionData.objectives) {
          for (let optionIndex = 0; optionIndex < objective.verbalData.optionCount(); optionIndex++) {
            objective.verbalData.options[optionIndex] = '' + (optionIndex + 1);
          }

          objective.aspects = new Tree(new ObjectiveElement());
        }

        decisionData.objectiveAspects = new ObjectiveAspects(decisionData);

        decisionData.decisionStatement = new DecisionStatement(decisionData.decisionStatement.statement);
      }),
      tap(({ decisionData, utilityValues }) => {
        // Make sure they stay consistent on reload
        const text = decisionDataExportService.dataToText('', decisionData);

        const decisionData2 = readText(text);
        const utilityValues2 = decisionData2.getAlternativeUtilities();

        if (utilityValues.length !== utilityValues2.length) {
          throw new Error('Utility values length mismatch');
        }

        for (let i = 0; i < utilityValues.length; i++) {
          if (utilityValues[i] !== utilityValues2[i]) {
            throw new Error('Utility values mismatch');
          }
        }
      }),
      map(({ decisionData, utilityValues, projectName }) => {
        const jsonProjectString = decisionDataExportService.dataToText(decisionData.decisionStatement.statement, decisionData);

        const simpleObject = JSON.parse(jsonProjectString);

        delete simpleObject.timeRecording;

        return { jsonProjectString: JSON.stringify(simpleObject), utilityValues, projectName };
      }),
      reduce(
        (acc, { jsonProjectString, utilityValues, projectName }) => {
          acc.push({ jsonProjectString, utilityValues, projectName });
          return acc;
        },
        [] as { jsonProjectString: string; utilityValues: number[]; projectName: string }[],
      ),
    )
    .subscribe(result => {
      console.log(JSON.stringify(result));
    });
}

type Callback = (key: string, value: any, path: string[]) => void;
function deepIterate(obj: any, callback: Callback, path: string[] = [], visited: Set<any> = new Set()): void {
  if (obj === null || obj === undefined) {
    callback('', obj, path);
    return;
  }

  if (typeof obj !== 'object') {
    callback('', obj, path);
    return;
  }

  if (visited.has(obj)) {
    return;
  }

  visited.add(obj);

  if (Array.isArray(obj)) {
    obj.forEach((item, index) => {
      const currentPath = [...path, `${index}`];
      deepIterate(item, callback, currentPath, visited);
    });
  } else {
    for (const key in obj) {
      if (Object.prototype.hasOwnProperty.call(obj, key)) {
        const value = obj[key];
        const currentPath = [...path, key];

        if (value && typeof value === 'object') {
          deepIterate(value, callback, currentPath, visited);
        } else {
          callback(key, value, currentPath);
        }
      }
    }
  }

  visited.delete(obj);
}

type AnyObject = { [key: string]: any };
// eslint-disable-next-line @typescript-eslint/naming-convention
function setPropertyByPath<T extends AnyObject, V>(obj: T, path: string[], value: V): void {
  if (!Array.isArray(path) || path.length === 0) {
    throw new Error('Path must be a non-empty array of strings.');
  }

  let current: AnyObject = obj;

  for (let i = 0; i < path.length - 1; i++) {
    const key = path[i];

    if (!(key in current) || typeof current[key] !== 'object' || current[key] === null) {
      current[key] = {};
    }

    current = current[key];
  }

  const lastKey = path[path.length - 1];
  current[lastKey] = value;
}
