import { Component } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { DecisionData } from '@entscheidungsnavi/decision-data';
import { AuthService } from '@entscheidungsnavi/api-client';
import { auditTime, distinctUntilChanged } from 'rxjs';
import { ProjectInformationComponent } from '../project-information/project-information.component';
import { SaveAsModalComponent } from '../navigation';
import { OnlineProject, ProjectService } from '../data/project';
import { IssueRaisingComponent } from '../issue-raising/issue-raising.component';
import { AutoSaveService } from '../data/project/auto-save.service';
import { saveErrorToMessage } from '../data/project/save-notification';
import { SideBySideService } from '../navigation-layout/side-by-side.service';
import { TeamProject } from '../data/project/team-project';
import { LoginModalComponent } from '../main/userarea';
import { TeamDashboardService } from '../../modules/shared/team/team-dashboard/team-dashboard.service';

@Component({
  selector: 'dt-project-area',
  templateUrl: './project-area.component.html',
  styleUrls: ['./project-area.component.scss'],
})
export class ProjectAreaComponent {
  get isTeamProject() {
    return !!this.teamProject;
  }

  get onlineProject() {
    return this.projectService.getProject(OnlineProject);
  }

  get teamProject() {
    return this.projectService.getProject(TeamProject);
  }

  get team() {
    return this.teamProject.team;
  }

  get teamName() {
    return this.team.name;
  }

  get saveErrorMessage() {
    return saveErrorToMessage(this.autoSaveService.saveError);
  }

  // Delay the save state emission to make sure we do not change saving -> idle -> saving instantly
  saveState$ = this.autoSaveService.saveState$.pipe(auditTime(100), distinctUntilChanged());

  constructor(
    protected decisionData: DecisionData,
    private dialog: MatDialog,
    protected projectService: ProjectService,
    protected authService: AuthService,
    protected autoSaveService: AutoSaveService,
    protected sideBySideService: SideBySideService,
    private teamDashboardService: TeamDashboardService,
  ) {}

  openTeamDashboard() {
    this.teamDashboardService.openTeamDashboard();
  }

  isProjectLoaded(): boolean {
    return this.projectService.isProjectLoaded();
  }

  openProjectNotes() {
    this.dialog.open(ProjectInformationComponent);
  }

  saveNonOnlineProject() {
    if (this.authService.loggedIn) {
      this.dialog.open(SaveAsModalComponent);
    } else {
      this.dialog
        .open(LoginModalComponent, { data: { showSaveHint: true } })
        .afterClosed()
        .subscribe(loggedIn => {
          if (loggedIn) this.dialog.open(SaveAsModalComponent);
        });
    }
  }

  openIssueRaising() {
    this.dialog.open(IssueRaisingComponent);
  }
}
