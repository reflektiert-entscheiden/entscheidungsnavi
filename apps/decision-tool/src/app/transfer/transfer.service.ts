import { Overlay } from '@angular/cdk/overlay';
import { ComponentPortal } from '@angular/cdk/portal';
import { Injectable } from '@angular/core';
import { Alternative, Objective } from '@entscheidungsnavi/decision-data';
import { loadObjective } from '@entscheidungsnavi/decision-data/export';
import { Subject } from 'rxjs';
import { ProjectService } from '../data/project';
import { TransferNotificationComponent } from '.';

type MessageAlternative = { type: 'alternative'; data: string };
type MessageObjective = { type: 'objective'; data: string };
type Message = MessageAlternative | MessageObjective;

@Injectable({
  providedIn: 'root',
})
export class TransferService {
  // eslint-disable-next-line @typescript-eslint/naming-convention
  private static readonly CHANNEL_NAME = 'dt';

  private broadcastChannel: BroadcastChannel;

  private onReceivedSubject = new Subject<Objective | Alternative>();
  public onReceived$ = this.onReceivedSubject.asObservable();

  constructor(
    private projectService: ProjectService,
    private overlay: Overlay,
  ) {
    if ('BroadcastChannel' in self) {
      this.broadcastChannel = new BroadcastChannel(TransferService.CHANNEL_NAME);
      this.addListeners();
    }
  }

  private addListeners() {
    this.broadcastChannel.addEventListener('message', ({ data }) => {
      if (!this.projectService.isProjectLoaded()) {
        return;
      }

      if (data.type) {
        const msg = data as Message;

        if (msg.type === 'alternative') {
          const object = JSON.parse(msg.data);

          const alternative = Alternative.clone(object);

          this.onReceivedSubject.next(alternative);
        } else if (msg.type === 'objective') {
          const object = JSON.parse(msg.data);

          const objective = loadObjective(object);

          this.onReceivedSubject.next(objective);
        }
      } else {
        console.error(`Received broken message over BroadcastChannel: ${data}`);
      }
    });
  }

  public openNotification(received: Objective | Alternative) {
    const overlayRef = this.overlay.create({
      positionStrategy: this.overlay.position().global().right('0').centerVertically(),
      hasBackdrop: true,
    });

    const portal = new ComponentPortal(TransferNotificationComponent);
    const notification = overlayRef.attach(portal).instance;

    overlayRef.backdropClick().subscribe(() => {
      notification.close();
    });

    notification.received = received;
    notification.overlayRef = overlayRef;
  }

  private broadcastMessage(msg: Message) {
    if (this.broadcastChannel) {
      this.broadcastChannel.postMessage(msg);
    }
  }

  async broadcastAlternative(alternative: Alternative) {
    const json = JSON.stringify(alternative);
    this.broadcastMessage({
      type: 'alternative',
      data: json,
    });
  }

  async broadcastObjective(objective: Objective) {
    const json = JSON.stringify(objective);
    this.broadcastMessage({
      type: 'objective',
      data: json,
    });
  }
}
