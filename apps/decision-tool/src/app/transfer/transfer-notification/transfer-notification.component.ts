import { animate, style, transition, trigger, AnimationEvent } from '@angular/animations';
import { OverlayRef } from '@angular/cdk/overlay';
import { Component, HostBinding, HostListener, Input } from '@angular/core';
import { DecisionData } from '@entscheidungsnavi/decision-data';
import { Alternative, Objective } from '@entscheidungsnavi/decision-data';

@Component({
  templateUrl: './transfer-notification.component.html',
  styleUrls: ['./transfer-notification.component.scss'],
  animations: [
    trigger('animation', [
      transition(':enter', [style({ transform: 'translateX(100%)' }), animate('250ms ease', style({ transform: 'none' }))]),
      transition(':leave', [animate('150ms linear', style({ opacity: 0 }))]),
    ]),
  ],
})
export class TransferNotificationComponent {
  @Input()
  received: Objective | Alternative;

  @Input()
  overlayRef: OverlayRef;

  @HostBinding('@animation') public animation = true;

  get receivedType() {
    return this.received instanceof Objective ? 'objective' : 'alternative';
  }

  constructor(private decisionData: DecisionData) {}

  add() {
    if (this.received instanceof Alternative) {
      this.decisionData.addAlternative({ alternative: this.received });
    } else {
      this.decisionData.addObjective(this.received);
    }

    this.overlayRef.detach();
  }

  close() {
    this.overlayRef.detach();
  }

  @HostListener('@animation.done', ['$event'])
  animationDone(event: AnimationEvent) {
    if (event.toState === 'void') {
      this.overlayRef.dispose();
    }
  }
}
