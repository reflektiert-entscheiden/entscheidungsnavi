import { Component, Injector, Input, TemplateRef, ViewChild } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Alternative } from '@entscheidungsnavi/decision-data';
import { helpPage } from '../../../../app/help/help';
import { HelpBackgroundComponent } from '../help-background/help-background.component';
import { AbstractAlternativeHintComponent } from '../alternative-hint.component';
import { AlternativeListComponent } from '../../alternative-list/alternative-list.component';
import { DisplayAtMaxWidth } from '../../../../app/interfaces/display-at-max-width';
import { Help1Component } from './help1/help1.component';
import { Help2Component } from './help2/help2.component';

@Component({
  selector: 'dt-alternative-hint-3',
  templateUrl: './hint3.component.html',
  styleUrls: ['./hint3.component.scss'],
})
@DisplayAtMaxWidth
export class AlternativeHint3Component extends AbstractAlternativeHintComponent {
  @Input()
  showTitle = true;

  public newAlternative = '';

  get helpMenu() {
    return [
      helpPage()
        .name($localize`So funktioniert's`)
        .component(Help1Component)
        .build(),
      helpPage()
        .name($localize`Weitere Hinweise`)
        .component(Help2Component)
        .build(),
      helpPage()
        .name($localize`Hintergrundwissen zum Schritt 3`)
        .component(HelpBackgroundComponent)
        .build(),
      helpPage()
        .name($localize`Videos`)
        .youtube(2)
        .build(),
    ];
  }

  @ViewChild('notify', { static: true }) private notifyRef: TemplateRef<any>;
  @ViewChild(AlternativeListComponent) alternativeList: AlternativeListComponent;

  constructor(
    injector: Injector,
    private snackBar: MatSnackBar,
  ) {
    super(injector);
    this.pageKey = 3;
  }

  addAlternative() {
    if (this.newAlternative) {
      this.decisionData.addAlternative({ alternative: new Alternative(this.pageKey, this.newAlternative) });
      this.newAlternative = '';
      this.alternativeList.updateAlternatives();

      this.snackBar.openFromTemplate(this.notifyRef, { duration: 3000 });
    }
  }
}
