import { Directive, Injector, OnInit } from '@angular/core';
import { ALTERNATIVES_STEPS, DecisionData, NaviSubStep } from '@entscheidungsnavi/decision-data';
import { EducationalNavigationService } from '../../shared/navigation/educational-navigation.service';
import { HelpMenu, HelpMenuProvider } from '../../../app/help/help';
import { Navigation, NavLine, navLineElement } from '../../shared/navline';
import { DecisionDataState } from '../../shared/decision-data-state';

@Directive()
// eslint-disable-next-line @angular-eslint/directive-class-suffix
export abstract class AbstractAlternativeHintComponent implements OnInit, Navigation, HelpMenuProvider {
  navLine = new NavLine({
    left: [
      navLineElement()
        .back(() => (this.pageKey === 1 ? '/objectives' : `/alternatives/steps/${this.pageKey - 1}`))
        .build(),
    ],
    right: [
      navLineElement()
        .continue(() => (this.pageKey === ALTERNATIVES_STEPS.length - 1 ? '/alternatives' : `/alternatives/steps/${this.pageKey + 1}`))
        .build(),
    ],
  });

  abstract helpMenu: HelpMenu;

  pageKey: number; // 1-based
  decisionData: DecisionData;

  protected currentProgressService: EducationalNavigationService;

  protected decisionDataState: DecisionDataState;

  get naviSubStep(): NaviSubStep {
    return { step: 'alternatives', subStepIndex: this.pageKey - 1 };
  }

  protected constructor(injector: Injector) {
    this.currentProgressService = injector.get(EducationalNavigationService);
    this.decisionData = injector.get(DecisionData);
    this.decisionDataState = injector.get(DecisionDataState);
  }

  ngOnInit() {
    if (this.pageKey > ALTERNATIVES_STEPS.indexOf(this.decisionData.hintAlternatives.subStepProgression) + 1) {
      this.decisionData.hintAlternatives.subStepProgression = ALTERNATIVES_STEPS[this.pageKey - 1];
    }

    this.currentProgressService.updateProgress();

    if (this.decisionData.alternatives.length === 0) {
      while (this.decisionData.alternatives.length < 2) {
        this.decisionData.addAlternative();
      }
    }
  }
}
