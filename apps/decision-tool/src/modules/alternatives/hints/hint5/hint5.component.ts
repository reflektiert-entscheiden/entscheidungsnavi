import { ChangeDetectorRef, Component, Injector, Input, OnInit, ViewChild } from '@angular/core';
import { Alternative, HintAlternatives, Screw, ScrewConfiguration } from '@entscheidungsnavi/decision-data';
import { MatDialog } from '@angular/material/dialog';
import { ScrewListComponent } from '@entscheidungsnavi/widgets';
import { helpPage } from '../../../../app/help/help';
import { HelpBackgroundComponent } from '../help-background/help-background.component';
import { AbstractAlternativeHintComponent } from '../alternative-hint.component';
import { AlternativeListComponent } from '../../alternative-list/alternative-list.component';
import { DisplayAtMaxWidth } from '../../../../app/interfaces/display-at-max-width';
import { ScrewModalComponent, ScrewModalData, ScrewModalResultData } from '../screw-modal/screw-modal.component';
import { Help1Component } from './help1/help1.component';
import { Help2Component } from './help2/help2.component';

@Component({
  selector: ' dt-alternative-hint-5',
  templateUrl: './hint5.component.html',
  styleUrls: ['./hint5.component.scss'],
})
@DisplayAtMaxWidth
export class AlternativeHint5Component extends AbstractAlternativeHintComponent implements OnInit {
  get helpMenu() {
    return [
      helpPage()
        .name($localize`So funktioniert's`)
        .component(Help1Component)
        .build(),
      helpPage()
        .name($localize`Weitere Hinweise`)
        .component(Help2Component)
        .build(),
      helpPage()
        .name($localize`Hintergrundwissen zum Schritt 3`)
        .component(HelpBackgroundComponent)
        .build(),
      helpPage()
        .name($localize`Videos`)
        .youtube(2)
        .build(),
    ];
  }
  @ViewChild(AlternativeListComponent) alternativeList: AlternativeListComponent;
  @ViewChild(ScrewListComponent) screwList: ScrewListComponent;

  @Input()
  showTitle = true;

  screws: Screw[];

  screwConfigurationForNewAlternative: ScrewConfiguration;

  screwEditModalOpened = false;

  constructor(
    private cdr: ChangeDetectorRef,
    injector: Injector,
    public matDialog: MatDialog,
  ) {
    super(injector);
    this.pageKey = 5;
  }

  override ngOnInit() {
    super.ngOnInit();

    this.screws = this.decisionData.hintAlternatives.screws;
    while (this.screws.length < 2) {
      this.decisionData.hintAlternatives.appendScrew('', ['', '']);
    }

    this.screwConfigurationForNewAlternative = new Array(this.decisionData.hintAlternatives.screws.length).fill(null);
  }

  appendScrew() {
    this.screwEditModalOpened = true;
    this.matDialog
      .open<ScrewModalComponent, ScrewModalData, ScrewModalResultData>(ScrewModalComponent, { data: { allowScrewDeletion: false } })
      .afterClosed()
      .subscribe(result => {
        this.screwEditModalOpened = false;

        if (result == null || result.shouldScrewBeDeleted === true) {
          return;
        }

        this.decisionData.hintAlternatives.appendScrew();
        const screwIndex = this.decisionData.hintAlternatives.screws.length - 1;
        HintAlternatives.adjustScrewConfigurationOnAppendedScrew(this.screwConfigurationForNewAlternative);

        this.decisionData.hintAlternatives.updateScrew(screwIndex, result.editedScrew, result.executedOperationsOnScrew);
        HintAlternatives.adjustScrewConfigurationOnUpdatedScrew(
          this.screwConfigurationForNewAlternative,
          screwIndex,
          result.executedOperationsOnScrew,
          this.decisionData.hintAlternatives.screws[screwIndex].states.length,
        );
      });
  }

  moveScrew(screwIndex: number, destinationScrewIndex: number) {
    this.decisionData.hintAlternatives.moveScrew(screwIndex, destinationScrewIndex);
    HintAlternatives.adjustScrewConfigurationOnScrewMove(this.screwConfigurationForNewAlternative, screwIndex, destinationScrewIndex);

    this.cdr.detectChanges();
  }

  editOrDeleteScrew(screwIndex: number) {
    this.screwEditModalOpened = true;

    const screwModalRef = this.matDialog.open<ScrewModalComponent, ScrewModalData, ScrewModalResultData>(ScrewModalComponent, {
      data: {
        screwIndex,
        allowScrewDeletion: this.screws.length > 2,
      },
    });

    screwModalRef.afterClosed().subscribe(screwEditResult => {
      this.screwEditModalOpened = false;

      if (screwEditResult == null) return;

      if (screwEditResult.shouldScrewBeDeleted === true) {
        this.decisionData.hintAlternatives.deleteScrew(screwIndex);
        HintAlternatives.adjustScrewConfigurationOnScrewDeletion(this.screwConfigurationForNewAlternative, screwIndex);
      } else {
        this.decisionData.hintAlternatives.updateScrew(screwIndex, screwEditResult.editedScrew, screwEditResult.executedOperationsOnScrew);
        HintAlternatives.adjustScrewConfigurationOnUpdatedScrew(
          this.screwConfigurationForNewAlternative,
          screwIndex,
          screwEditResult.executedOperationsOnScrew,
          this.decisionData.hintAlternatives.screws[screwIndex].states.length,
        );
      }
    });
  }

  addAlternative() {
    this.decisionData.addAlternative({
      alternative: new Alternative(this.pageKey, '', '', [], this.screwConfigurationForNewAlternative),
    });
    this.alternativeList.updateAlternatives();
  }
}
