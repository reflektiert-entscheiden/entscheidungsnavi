import { Component, ElementRef, Injector, Input, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Alternative, Objective } from '@entscheidungsnavi/decision-data';
import { helpPage } from '../../../../app/help/help';
import { HelpBackgroundComponent } from '../help-background/help-background.component';
import { AbstractAlternativeHintComponent } from '../alternative-hint.component';
import { AlternativeListComponent } from '../../alternative-list/alternative-list.component';
import { Help1Component } from './help1/help1.component';
import { Help2Component } from './help2/help2.component';

@Component({
  selector: 'dt-alternative-hint-2',
  templateUrl: './hint2.component.html',
  styleUrls: ['./hint2.component.scss'],
})
export class AlternativeHint2Component extends AbstractAlternativeHintComponent implements OnInit {
  @Input()
  showTitle = true;

  @ViewChild('notify', { static: true }) private notifyRef: TemplateRef<any>;
  @ViewChild(AlternativeListComponent) alternativeList: AlternativeListComponent;

  @ViewChild('alternativeNameInput')
  private alternativeInput: ElementRef;

  comment = '';
  name = '';
  selectedObjective: Objective;
  selectedAlternative: Alternative;

  get helpMenu() {
    return [
      helpPage()
        .name($localize`So funktioniert's`)
        .component(Help1Component)
        .build(),
      helpPage()
        .name($localize`Weitere Hinweise`)
        .component(Help2Component)
        .build(),
      helpPage()
        .name($localize`Hintergrundwissen zum Schritt 3`)
        .component(HelpBackgroundComponent)
        .build(),
      helpPage()
        .name($localize`Videos`)
        .youtube(2)
        .build(),
    ];
  }

  constructor(
    injector: Injector,
    private snackBar: MatSnackBar,
  ) {
    super(injector);
    this.pageKey = 2;
  }

  selectAlternative(alternative: Alternative) {
    this.selectedAlternative = alternative;
  }

  selectObjective(objective: Objective) {
    this.selectedObjective = objective;
    setTimeout(() => this.alternativeInput.nativeElement.focus(), 0);
    // eslint-disable-next-line max-len
    this.comment = $localize`Entstanden aus der Alternative "${this.selectedAlternative.name}" durch Verbesserung im Ziel "${this.selectedObjective.name}".`;
  }

  addAlternativeInHeader() {
    if (!this.decisionDataState.isProjectReadonly && this.name && this.selectedObjective && this.selectedAlternative) {
      this.addAlternative(new Alternative(2, this.name, this.comment));
      this.name = '';
      this.comment = '';
      this.selectedObjective = undefined;
      this.selectedAlternative = undefined;
    }
  }

  addAlternative(alternative: Alternative) {
    this.decisionData.addAlternative({ alternative });
    this.alternativeList.updateAlternatives();

    this.snackBar.openFromTemplate(this.notifyRef, { duration: 3000 });
  }
}
