import { DecisionData, Screw, ScrewStateOp } from '@entscheidungsnavi/decision-data';
import { Component, Inject } from '@angular/core';
import { ConfirmModalComponent } from '@entscheidungsnavi/widgets';
import { MAT_DIALOG_DATA, MatDialog, MatDialogRef } from '@angular/material/dialog';
import { CdkDragDrop, moveItemInArray } from '@angular/cdk/drag-drop';
import { filter } from 'rxjs';
import { assertUnreachable } from '@entscheidungsnavi/tools/assert-unreachable';

export type ScrewModalData = {
  screwIndex?: number;
  allowScrewDeletion: boolean;
};

export type ScrewModalResultData =
  | {
      shouldScrewBeDeleted: false;
      editedScrew: Screw;
      executedOperationsOnScrew: ScrewStateOp[];
    }
  | { shouldScrewBeDeleted: true }
  | null;

@Component({
  templateUrl: './screw-modal.component.html',
  styleUrls: ['./screw-modal.component.scss'],
})
export class ScrewModalComponent {
  readonly minNumberOfStates = 2;
  readonly maxNumberOfStates = 12;

  private executedOperationsOnScrew: ScrewStateOp[] = [];

  readonly screwIndex: number;
  readonly screwDeletionAllowed: boolean;

  editedScrew: Screw;

  get numberOfNonEmptyStates() {
    return this.editedScrew.states.filter(Boolean).length;
  }

  get isEditedScrewValid() {
    return (
      this.editedScrew.name.length > 0 &&
      this.numberOfNonEmptyStates >= this.minNumberOfStates &&
      this.editedScrew.states.length <= this.maxNumberOfStates
    );
  }

  get hasChanges() {
    const originalScrew = this.decisionData.hintAlternatives.screws[this.screwIndex];
    return !this.editedScrew.isEqual(originalScrew);
  }

  constructor(
    protected decisionData: DecisionData,
    @Inject(MAT_DIALOG_DATA) data: ScrewModalData,
    public dialogRef: MatDialogRef<ScrewModalComponent, ScrewModalResultData>,
    private dialog: MatDialog,
  ) {
    this.screwDeletionAllowed = data.allowScrewDeletion;

    if (data.screwIndex != null) {
      this.screwIndex = data.screwIndex;
      const currentScrew = this.decisionData.hintAlternatives.screws[this.screwIndex];
      this.editedScrew = currentScrew.clone();
    } else {
      this.editedScrew = new Screw();
      this.addState(0, false);
      this.addState(0, false);
    }
  }

  addState(stateIndex: number, focusField = true) {
    this.editedScrew.states.splice(stateIndex, 0, '');
    this.executedOperationsOnScrew.push({
      opCode: 'addition',
      stateIndex,
    });
    if (focusField) this.focusField(stateIndex);
  }

  stateDrop(event: CdkDragDrop<string[]>) {
    this.moveStates(event.previousIndex, event.currentIndex);
  }

  moveStates(fromStateIndex: number, toStateIndex: number) {
    moveItemInArray(this.editedScrew.states, fromStateIndex, toStateIndex);
    this.executedOperationsOnScrew.push({
      opCode: 'move',
      fromStateIndex,
      toStateIndex,
    });
  }

  deleteState(stateIndex: number) {
    this.editedScrew.states.splice(stateIndex, 1);
    this.executedOperationsOnScrew.push({
      opCode: 'deletion',
      stateIndex,
    });
  }

  deleteScrew() {
    this.closeScrewModal('delete');
  }

  saveScrew() {
    if (this.isEditedScrewValid) {
      this.cleanEmptyFields();
      this.closeScrewModal('apply');
    }
  }

  cleanEmptyFields() {
    for (let stateIndex = 0; stateIndex < this.editedScrew.states.length; stateIndex++) {
      if (!this.editedScrew.states[stateIndex]) {
        this.deleteState(stateIndex);
        stateIndex--;
      }
    }
  }

  tryCloseScrewModal() {
    const originalScrew = this.decisionData.hintAlternatives.screws[this.screwIndex];
    if (this.editedScrew.isEqual(originalScrew)) {
      this.closeScrewModal('discard');
    } else {
      this.dialog
        .open(ConfirmModalComponent, {
          data: {
            title: $localize`Ungespeicherte Änderungen`,
            prompt: $localize`Bist Du sicher, dass Du die ungespeicherten Änderungen verwerfen willst?`,
            buttonConfirm: $localize`Verwerfen`,
            buttonDeny: $localize`Abbrechen`,
          },
        })
        .afterClosed()
        .pipe(filter(Boolean))
        .subscribe(() => this.closeScrewModal('discard'));
    }
  }

  closeScrewModal(action: 'apply' | 'discard' | 'delete') {
    switch (action) {
      case 'discard':
        this.dialogRef.close(null);
        break;
      case 'delete':
        this.dialogRef.close({
          shouldScrewBeDeleted: true,
        });
        break;
      case 'apply':
        this.dialogRef.close({
          shouldScrewBeDeleted: false,
          editedScrew: this.editedScrew,
          executedOperationsOnScrew: this.executedOperationsOnScrew,
        });
        break;
      default:
        assertUnreachable(action);
    }
  }

  focusField(stateIndex: number) {
    setTimeout(() => {
      const inputElements = document.querySelectorAll('.screw-state');
      (inputElements[stateIndex] as HTMLElement)?.focus();
    }, 50);
  }
}
