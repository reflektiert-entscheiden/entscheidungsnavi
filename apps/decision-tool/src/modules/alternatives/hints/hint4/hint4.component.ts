import { Component, Injector } from '@angular/core';
import { helpPage } from '../../../../app/help/help';
import { HelpBackgroundComponent } from '../help-background/help-background.component';
import { AbstractAlternativeHintComponent } from '../alternative-hint.component';
import { Help1Component } from './help1/help1.component';

@Component({
  templateUrl: './hint4.component.html',
})
export class AlternativeHint4Component extends AbstractAlternativeHintComponent {
  get helpMenu() {
    return [
      helpPage()
        .name($localize`So funktioniert's`)
        .component(Help1Component)
        .build(),
      helpPage()
        .name($localize`Hintergrundwissen zum Schritt 3`)
        .component(HelpBackgroundComponent)
        .build(),
      helpPage()
        .name($localize`Videos`)
        .youtube(2)
        .build(),
    ];
  }

  constructor(injector: Injector) {
    super(injector);
    this.pageKey = 4;
  }
}
