import { moveItemInArray } from '@angular/cdk/drag-drop';
import { Component, EventEmitter, Input, Output, QueryList, ViewChildren } from '@angular/core';
import { NotePage } from '@entscheidungsnavi/decision-data';
import { AspectBoxComponent, WidgetsModule } from '@entscheidungsnavi/widgets';

// Data type used to identify drag and drop data. The random number is used to make it unique to this navi instance.
const DRAG_DATA_TYPE = 'application/decision-tool-objective-group' + Math.random();

@Component({
  selector: 'dt-objective-group',
  styleUrls: ['./objective-group.component.scss'],
  templateUrl: './objective-group.component.html',
  standalone: true,
  imports: [WidgetsModule],
})
export class ObjectiveGroupComponent {
  static dragDropData: {
    noteGroupIndex: number;
    noteIndex: number;
  };

  @Input() notePage: NotePage;
  @Input() noteGroupIdx: number;

  @Input() readonly = false;
  @Input() showAddButton = true;

  @Output() dirty = new EventEmitter(); // Emit an event when something besides inputs changes

  @ViewChildren(AspectBoxComponent) items: QueryList<AspectBoxComponent>;

  get noteGroup() {
    return this.notePage.noteGroups[this.noteGroupIdx];
  }

  removeNote(index: number) {
    this.noteGroup.removeNote(index);
    this.dirty.emit();
  }

  addItem() {
    this.notePage.addNote('', this.noteGroupIdx);
    setTimeout(() => this.items.last.edit(), 0);
    this.dirty.emit();
  }

  dragOverGroup(event: DragEvent) {
    if (!event.dataTransfer.types.includes(DRAG_DATA_TYPE)) {
      return;
    }

    const source = ObjectiveGroupComponent.dragDropData;
    if (this.noteGroupIdx !== source.noteGroupIndex) {
      const toIndex = this.noteGroup.notes.length;
      this.notePage.swapNoteGroup(source.noteGroupIndex, source.noteIndex, this.noteGroupIdx, toIndex);
      source.noteGroupIndex = this.noteGroupIdx;
      source.noteIndex = toIndex;
      this.dirty.emit();
    }

    event.dataTransfer.dropEffect = 'move';
    event.preventDefault();
  }

  dragOver(event: DragEvent, index: number) {
    if (!event.dataTransfer.types.includes(DRAG_DATA_TYPE)) {
      return;
    }

    const source = ObjectiveGroupComponent.dragDropData;
    if (this.noteGroupIdx === source.noteGroupIndex) {
      moveItemInArray(this.noteGroup.notes, source.noteIndex, index);
    } else {
      this.notePage.swapNoteGroup(source.noteGroupIndex, source.noteIndex, this.noteGroupIdx, index);
    }

    source.noteGroupIndex = this.noteGroupIdx;
    source.noteIndex = index;
    event.dataTransfer.dropEffect = 'move';
    event.preventDefault();
    this.dirty.emit();
  }

  dragStart(event: DragEvent, index: number) {
    event.dataTransfer.setData('text', 'dummy');
    event.dataTransfer.setData(DRAG_DATA_TYPE, 'dummy');
    event.dataTransfer.effectAllowed = 'move';
    ObjectiveGroupComponent.dragDropData = {
      noteGroupIndex: this.noteGroupIdx,
      noteIndex: index,
    };
  }

  drop(event: DragEvent) {
    event.preventDefault();
  }
}
