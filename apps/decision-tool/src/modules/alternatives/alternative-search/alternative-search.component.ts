import { Component } from '@angular/core';

import { WidgetsModule } from '@entscheidungsnavi/widgets';
import { DecisionData, NotePage } from '@entscheidungsnavi/decision-data';
import { DecisionDataState } from '../../shared/decision-data-state';
import { ObjectiveGroupComponent } from './objective-group/objective-group.component';

@Component({
  selector: 'dt-alternative-search',
  templateUrl: './alternative-search.component.html',
  styleUrls: ['./alternative-search.component.scss'],
  standalone: true,
  imports: [ObjectiveGroupComponent, WidgetsModule],
})
export class AlternativeSearchComponent {
  aspects: Array<ReadonlyArray<string>>;

  get ideas(): NotePage {
    return this.decisionData.hintAlternatives.ideas;
  }

  constructor(
    protected decisionData: DecisionData,
    protected decisionDataState: DecisionDataState,
  ) {
    this.aspects = this.decisionData.objectives.map(o => o.getAspectsAsArray());

    if (!this.decisionData.hintAlternatives.ideas) {
      this.decisionData.hintAlternatives.initIdeas();
    } else {
      while (this.decisionData.hintAlternatives.ideas.noteGroups.length < this.decisionData.objectives.length) {
        this.decisionData.hintAlternatives.ideas.addNoteGroup();
      }
    }
  }
}
