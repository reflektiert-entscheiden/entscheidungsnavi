import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  AutoFocusDirective,
  ConfirmPopoverDirective,
  ElementRefDirective,
  HoverPopOverDirective,
  NoteBtnComponent,
  NoteBtnPresetPipe,
  OverflowDirective,
  ScrewListComponent,
} from '@entscheidungsnavi/widgets';
import { SelectLineComponent } from '@entscheidungsnavi/widgets/select';
import { SharedModule } from '../shared/shared.module';
import { NoteHoverComponent } from '../shared/note-hover/note-hover.component';
import { AlternativesComponent } from './main/alternatives.component';
import { AlternativesRoutingModule } from './alternatives-routing.module';
import {
  AlternativeHint1Component,
  AlternativeHint2Component,
  AlternativeHint3Component,
  AlternativeHint4Component,
  AlternativeHint5Component,
  AlternativeHint6Component,
  AlternativeHint7Component,
  HelpBackgroundComponent,
  ScrewModalComponent,
} from './hints';
import { AlternativeListComponent } from './alternative-list/alternative-list.component';
import { Help1Component as Hint1Help1Component } from './hints/hint1/help1/help1.component';
import { Help2Component as Hint1Help2Component } from './hints/hint1/help2/help2.component';
import { Help1Component as Hint2Help1Component } from './hints/hint2/help1/help1.component';
import { Help1Component as Hint3Help1Component } from './hints/hint3/help1/help1.component';
import { Help1Component as Hint4Help1Component } from './hints/hint4/help1/help1.component';
import { Help1Component as Hint5Help1Component } from './hints/hint5/help1/help1.component';
import { Help2Component as Hint5Help2Component } from './hints/hint5/help2/help2.component';
import { Help1Component as Hint6Help1Component } from './hints/hint6/help1/help1.component';
import { Help2Component as Hint6Help2Component } from './hints/hint6/help2/help2.component';
import { Help1Component as Hint7Help1Component } from './hints/hint7/help1/help1.component';
import { AlternativeBoxComponent } from './alternative-box/alternative-box.component';
import { AlternativeSearchComponent } from './alternative-search/alternative-search.component';

@NgModule({
  declarations: [
    AlternativesComponent,
    AlternativeHint1Component,
    AlternativeHint2Component,
    AlternativeHint3Component,
    AlternativeHint4Component,
    AlternativeHint5Component,
    AlternativeHint6Component,
    AlternativeHint7Component,
    AlternativeListComponent,
    ScrewModalComponent,
    HelpBackgroundComponent,
    Hint1Help1Component,
    Hint1Help2Component,
    Hint2Help1Component,
    Hint3Help1Component,
    Hint4Help1Component,
    Hint5Help1Component,
    Hint5Help2Component,
    Hint6Help1Component,
    Hint6Help2Component,
    Hint7Help1Component,
    AlternativeBoxComponent,
  ],
  imports: [
    CommonModule,
    AlternativesRoutingModule,
    ScrewListComponent,
    SharedModule,
    OverflowDirective,
    NoteBtnComponent,
    NoteBtnPresetPipe,
    HoverPopOverDirective,
    NoteHoverComponent,
    ConfirmPopoverDirective,
    SelectLineComponent,
    AutoFocusDirective,
    AlternativeSearchComponent,
    ElementRefDirective,
  ],
  exports: [
    AlternativeListComponent,
    AlternativesComponent,
    AlternativeHint2Component,
    AlternativeHint3Component,
    AlternativeHint5Component,
  ],
})
export class AlternativesModule {}
