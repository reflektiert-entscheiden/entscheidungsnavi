import { Component, Input, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { Alternative, ALTERNATIVES_STEPS, DecisionData } from '@entscheidungsnavi/decision-data';
import { OnDestroyObservable } from '@entscheidungsnavi/tools';
import { Observable, takeUntil } from 'rxjs';
import { EducationalNavigationService } from '../../shared/navigation/educational-navigation.service';
import { HelpMenuProvider, helpPage } from '../../../app/help/help';
import { TransferService } from '../../../app/transfer';
import { AppSettingsService } from '../../../app/data/app-settings.service';
import { ExplanationService } from '../../shared/decision-quality';
import { HelpBackgroundComponent } from '../hints/help-background/help-background.component';
import { Navigation, NavLine, navLineElement } from '../../shared/navline';
import { DecisionDataState } from '../../shared/decision-data-state';

@Component({
  selector: 'dt-alternatives',
  templateUrl: './alternatives.component.html',
  styleUrls: ['./alternatives.component.scss'],
})
export class AlternativesComponent implements OnInit, Navigation, HelpMenuProvider {
  navLine = new NavLine({
    left: [navLineElement().back('/alternatives/steps/7').build()],
    middle: [this.explanationService.generateAssessmentButton('CREATIVE_DOABLE_ALTERNATIVES')],
    right: [navLineElement().continue('/impactmodel').build()],
  });

  @Input()
  showTitle = true;

  helpMenu = [
    helpPage()
      .name($localize`Ergebnisseite`)
      .template(() => this.helpTemplate)
      .build(),
    helpPage()
      .name($localize`Hintergrundwissen zum Schritt 3`)
      .component(HelpBackgroundComponent)
      .build(),
    helpPage()
      .name($localize`Videos`)
      .youtube(2)
      .build(),
  ];

  @ViewChild('helpTemplate') helpTemplate: TemplateRef<any>;

  @OnDestroyObservable()
  private onDestroy$: Observable<void>;

  constructor(
    protected decisionData: DecisionData,
    private currentProgressService: EducationalNavigationService,
    private explanationService: ExplanationService,
    protected appSettings: AppSettingsService,
    protected decisionDataState: DecisionDataState,
    transferService: TransferService,
  ) {
    transferService.onReceived$.pipe(takeUntil(this.onDestroy$)).subscribe(receivedObject => {
      if (receivedObject instanceof Alternative) {
        transferService.openNotification(receivedObject);
      }
    });
  }

  move(from: number, to: number) {
    this.decisionData.moveAlternative(from, to);
  }

  delete(position: number) {
    this.decisionData.removeAlternative(position);
  }

  ngOnInit() {
    this.decisionData.hintAlternatives.subStepProgression = ALTERNATIVES_STEPS[ALTERNATIVES_STEPS.length - 1];
    this.currentProgressService.updateProgress();

    if (this.decisionData.alternatives.length === 0) {
      // Make sure at least two alternatives are present
      while (this.decisionData.alternatives.length < 2) {
        this.decisionData.addAlternative();
      }
    }
  }
}
