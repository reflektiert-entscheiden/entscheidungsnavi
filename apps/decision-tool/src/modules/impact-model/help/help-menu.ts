import { NaviStep, NaviStepNames } from '@entscheidungsnavi/decision-data';
import { helpPage } from '../../../app/help/help';
import { NavigationStepMetaData } from '../../shared/navigation/educational-navigation';
import { Help1Component } from './help1/help1.component';
import { Help2Component } from './help2/help2.component';
import { HelpBackgroundComponent } from './help-background/help-background.component';

type ImpactModelHelpContext = 'uncertaintyfactors';

export function getHelpMenu(steps: { [key in NaviStep]: NavigationStepMetaData & NaviStepNames }, context?: ImpactModelHelpContext) {
  return [
    helpPage()
      .name($localize`So funktioniert's`)
      .component(Help1Component)
      .context(context)
      .build(),
    helpPage()
      .name($localize`Weitere Hinweise`)
      .component(Help2Component)
      .build(),
    helpPage()
      .name($localize`Hintergrundwissen zum Schritt 4`)
      .component(HelpBackgroundComponent)
      .build(),
    helpPage()
      .name($localize`Videos`)
      .youtube(3)
      .build(),
  ];
}
