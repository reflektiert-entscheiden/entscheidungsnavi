import { Component } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ExplanationService } from '../../shared/decision-quality';
import { DisplayAtMaxWidth } from '../../../app/interfaces/display-at-max-width';
import { HelpMenuProvider } from '../../../app/help/help';
import { getHelpMenu } from '../help';
import { Navigation, NavLine, navLineElement } from '../../shared/navline';
import { LanguageService } from '../../../app/data/language.service';
import { DecisionDataState } from '../../shared/decision-data-state';
import { GlobalVariablesModalComponent } from '../global-variables-modal/global-variables-modal.component';

@Component({
  selector: 'dt-impactmodel',
  templateUrl: './impact-model.component.html',
  styleUrls: ['./impact-model.component.scss'],
})
@DisplayAtMaxWidth
export class ImpactModelComponent implements Navigation, HelpMenuProvider {
  readonly navLine = new NavLine({
    left: [navLineElement().back('/alternatives').build()],
    middle: [
      navLineElement()
        .label($localize`Einflussfaktoren`)
        .link('/impactmodel/uncertaintyfactors')
        .build(),
      navLineElement()
        .label($localize`Globale Variablen`)
        .onClick(() => this.dialog.open(GlobalVariablesModalComponent))
        .cypressId('global-variables')
        .build(),
      this.explanationService.generateAssessmentButton('CLEAR_VALUES'),
    ],
    right: [navLineElement().continue('/results/steps/1').build()],
  });

  readonly helpMenu = getHelpMenu(this.languageService.steps);

  constructor(
    private dialog: MatDialog,
    private explanationService: ExplanationService,
    private languageService: LanguageService,
    protected decisionDataState: DecisionDataState,
  ) {}
}
