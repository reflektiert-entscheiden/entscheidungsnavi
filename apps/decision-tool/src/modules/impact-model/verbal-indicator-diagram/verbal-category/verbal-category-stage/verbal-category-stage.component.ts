import { Component, EventEmitter, Input, Output, TemplateRef, ViewChild } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { MAX_STAGES_PER_VERBAL_INDICATOR_CATEGORY, ObjectiveInput, VerbalIndicatorCategory } from '@entscheidungsnavi/decision-data';

@Component({
  selector: 'dt-verbal-category-stage',
  templateUrl: './verbal-category-stage.component.html',
  styleUrls: ['./verbal-category-stage.component.scss'],
})
export class VerbalCategoryStageComponent {
  @Input() categoryIdx: number;
  @Input() editable: boolean;
  @Input() hasMultipleCategories: boolean;
  @Input() indicatorIdx: number;
  @Input() outcomeValues: ObjectiveInput[];
  @Input() showEmptyField = false;
  @Input() stageIdx: number;
  @Input() stateIdx: number;
  @Input() verbalIndicatorCategory: VerbalIndicatorCategory;

  selectedStageIdx: number;

  @ViewChild('expandStageModal') expandStageModal: TemplateRef<HTMLElement>;

  maxNumberOfStages = MAX_STAGES_PER_VERBAL_INDICATOR_CATEGORY;

  @Output() createStage = new EventEmitter<{ categoryIdx: number; stageIdx: number }>();
  @Output() removeStage = new EventEmitter<{ categoryIdx: number; stageIdx: number }>();
  @Output() setVerbalIndicatorValue = new EventEmitter<{ categoryIdx: number; newValue: number }>();

  private dialogRef: MatDialogRef<unknown>;

  constructor(private dialog: MatDialog) {}

  openExpandStageModal(stageIdx: number) {
    this.selectedStageIdx = stageIdx;
    this.dialogRef = this.dialog.open(this.expandStageModal);
  }
}
