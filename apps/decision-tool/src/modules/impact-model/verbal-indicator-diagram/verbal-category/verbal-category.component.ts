import { Component, EventEmitter, Input, Output } from '@angular/core';
import {
  MAX_VERBAL_INDICATOR_CATEGORIES,
  MAX_STAGES_PER_VERBAL_INDICATOR_CATEGORY,
  ObjectiveInput,
  VerbalIndicatorCategory,
  VerbalIndicatorCategoryStage,
} from '@entscheidungsnavi/decision-data';
import { CdkDragDrop } from '@angular/cdk/drag-drop';

@Component({
  selector: 'dt-verbal-category',
  templateUrl: './verbal-category.component.html',
  styleUrls: ['./verbal-category.component.scss'],
})
export class VerbalCategoryComponent {
  @Input() categoryIdx: number;
  @Input() editable: boolean;
  @Input() hasMultipleCategories: boolean;
  @Input() indicatorIdx: number;
  @Input() outcomeValues: ObjectiveInput[];
  @Input() showEmptyField = false;
  @Input() stateIdx: number;
  @Input() verbalIndicatorCategory: VerbalIndicatorCategory;

  weightMax = MAX_VERBAL_INDICATOR_CATEGORIES;
  maxNumberOfStages = MAX_STAGES_PER_VERBAL_INDICATOR_CATEGORY;

  @Output() changeWeight = new EventEmitter<{ categoryIdx: number; newWeight: number }>();
  @Output() removeCategory = new EventEmitter<number>();
  @Output() createStage = new EventEmitter<{ categoryIdx: number; stageIdx: number }>();
  @Output() removeStage = new EventEmitter<{ categoryIdx: number; stageIdx: number }>();
  @Output() setVerbalIndicatorValue = new EventEmitter<{ categoryIdx: number; newValue: number }>();
  @Output() swapStages = new EventEmitter<CdkDragDrop<VerbalIndicatorCategoryStage[]>>();
}
