import { Component, ElementRef, HostListener, Input, OnDestroy, OnInit, TemplateRef, ViewChild } from '@angular/core';
import {
  Alternative,
  calculateIndicatorValue,
  DecisionData,
  Indicator,
  InfluenceFactor,
  ObjectiveInput,
  Screw,
  ScrewConfiguration,
  VerbalIndicatorCategory,
  VerbalIndicatorCategoryStage,
} from '@entscheidungsnavi/decision-data';
import { PopOverRef, PopOverService, SimpleHierarchyNode } from '@entscheidungsnavi/widgets';
import { NumberRoundingPipe } from '@entscheidungsnavi/widgets/pipes/number-rounding.pipe';
import { LocationSequence, Interval, Tree } from '@entscheidungsnavi/tools';
import { CdkDragDrop, moveItemInArray } from '@angular/cdk/drag-drop';
import { clamp } from 'lodash';

@Component({
  selector: 'dt-verbal-indicator-diagram',
  templateUrl: './verbal-indicator-diagram.component.html',
  styleUrls: ['./verbal-indicator-diagram.component.scss'],
})
export class VerbalIndicatorDiagramComponent implements OnDestroy, OnInit {
  /* Mutual properties. */
  @Input() diagramMode: 'definition' | 'forecast' = 'definition';
  @Input() indicatorIdx: number;
  @Input() indicator: Indicator;

  /* Scale definition properties. */
  @Input() objectiveTree: Tree<SimpleHierarchyNode>;
  // outcome.values for each outcome and the respective objective
  // changed when swapping/deleting categories or adding/deleting stages
  @Input() outcomeValuesInColumn: ObjectiveInput[][]; // [alternativeIdx][influenceFactorStateIdx][indicatorIdx][categoryIdx]
  @Input() showEmptyField = false;

  alternatives: Alternative[];

  isDraggingPopOver = false;
  popOverRef: PopOverRef;
  mouseDownOffsetX: number;
  mouseDownOffsetY: number;
  screws: Screw[];
  screwConfigurationForNewAlternative: ScrewConfiguration;
  suggestionsMode: 'hierarchy' | 'screw' = 'hierarchy';

  /* Forecast properties. */
  @Input() stateIdx = 0;
  @Input() outcomeValues: ObjectiveInput[];
  @Input() influenceFactor: InfluenceFactor;

  forecastResult: number;
  scaleValues: number[] = [];

  /* Design properties. */
  @ViewChild('sourcesTemplate') sourcesTemplate: TemplateRef<any>;
  @ViewChild('sourcesContent') sourcesContent: ElementRef<HTMLElement>;
  @ViewChild('sourcesButton', { read: ElementRef }) sourcesButton: ElementRef<HTMLElement>;
  @ViewChild('verticalScrollContainer') verticalScrollContainer: ElementRef<HTMLElement>;

  private readonly defaultNumberOfStages = 5; // preferred number of scale values in case no good divisor is found
  private readonly maxNumberOfStages = 7; // maximum number of scale sections (left panel)

  numberOfStages: number; // number of stages after calculateScaleValues()

  isNaN: (x: number) => boolean = Number.isNaN;

  get numberOfColumns() {
    return this.indicator.isVerbalized ? this.indicator.verbalIndicatorCategories.length : 1;
  }

  get editable() {
    return this.diagramMode === 'definition';
  }

  get hasMultipleCategories() {
    return this.indicator.verbalIndicatorCategories.length > 1;
  }

  get dottedLinesWidth() {
    // NUMBER_OF_COLUMNS * (COLUMN_WIDTH + SPACE_BETWEEN_COLUMNS) + SPACE_BETWEEN_SCALE_AND_CATEGORIES;
    return this.numberOfColumns * (Math.min(400, window.innerWidth * 0.8) + 20) + 20;
  }

  constructor(
    private decisionData: DecisionData,
    private popOverService: PopOverService,
  ) {}

  @HostListener('click', ['$event.target'])
  onClick(targetElement: HTMLElement) {
    // ignore clicks on the sourcesButton
    if (this.sourcesButton == null || this.sourcesButton.nativeElement.contains(targetElement)) return;
    this.popOverRef?.close(); // manually track for backdrop clicks
    this.popOverRef = null;
  }

  @HostListener('window:mouseup')
  @HostListener('window:touchend')
  onMouseUp() {
    this.isDraggingPopOver = false;
  }

  @HostListener('window:mousemove', ['$event'])
  @HostListener('window:touchmove', ['$event'])
  onMouseMove(event: MouseEvent | TouchEvent) {
    const clientX = (event as MouseEvent).clientX ?? (event as TouchEvent).touches[0].clientX;
    const clientY = (event as MouseEvent).clientY ?? (event as TouchEvent).touches[0].clientY;
    if (!this.isDraggingPopOver) return;
    const newX = clamp(clientX - this.mouseDownOffsetX, 0, window.innerWidth - this.sourcesContent.nativeElement.offsetWidth);
    const newY = clamp(clientY - this.mouseDownOffsetY, 0, window.innerHeight - this.sourcesContent.nativeElement.offsetHeight);
    this.popOverService.setPosition(newX, newY, this.popOverRef.overlayRef);
  }

  ngOnInit() {
    this.alternatives = this.decisionData.alternatives;
    this.screws = this.decisionData.hintAlternatives.screws;
    this.screwConfigurationForNewAlternative = new Array(this.screws.length).fill(null);
    this.calculateScaleValues(this.indicator.min, this.indicator.max);

    if (this.editable) {
      if (!this.indicator.isVerbalized) {
        this.createCategory();
      }
    } else {
      this.forecastResult = calculateIndicatorValue(this.outcomeValues[this.stateIdx][this.indicatorIdx], this.indicator);
    }
  }

  ngOnDestroy() {
    this.popOverRef?.close(); // otherwise pop stays when modal is closed
  }

  /* --- ON CLICK FUNCTIONS --- */
  // definition mode
  changeWeight(categoryIdx: number, newValue: number) {
    if (this.editable) {
      this.indicator.verbalIndicatorCategories[categoryIdx].weight = newValue;
    }
  }

  // definition mode
  createStage(categoryIdx: number, plusIdx: number, stageName = '') {
    this.indicator.verbalIndicatorCategories[categoryIdx].stages.splice(plusIdx + 1, 0, { name: stageName, description: '' });
    this.outcomeValuesInColumn.forEach((_, alternativeIdx) => {
      this.outcomeValuesInColumn[alternativeIdx].forEach((_, stateIdx) => {
        const categoryValue = this.outcomeValuesInColumn[alternativeIdx][stateIdx][this.indicatorIdx][categoryIdx];
        if (categoryValue != null && categoryValue > plusIdx) {
          this.outcomeValuesInColumn[alternativeIdx][stateIdx][this.indicatorIdx][categoryIdx]++;
        }
      });
    });
  }

  // definition mode
  removeStage(categoryIdx: number, stageIdx: number) {
    if (this.indicator.verbalIndicatorCategories[categoryIdx].stages.length <= 2) return;
    this.indicator.verbalIndicatorCategories[categoryIdx].stages.splice(stageIdx, 1);
    this.outcomeValuesInColumn.forEach((_, alternativeIdx) => {
      this.outcomeValuesInColumn[alternativeIdx].forEach((_, stateIdx) => {
        const categoryValue = this.outcomeValuesInColumn[alternativeIdx][stateIdx][this.indicatorIdx][categoryIdx];
        if (categoryValue != null && categoryValue > stageIdx) {
          this.outcomeValuesInColumn[alternativeIdx][stateIdx][this.indicatorIdx][categoryIdx]--;
        } else if (categoryValue === stageIdx) {
          this.outcomeValuesInColumn[alternativeIdx][stateIdx][this.indicatorIdx][categoryIdx] = undefined;
        }
      });
    });
  }

  // definition mode
  createCategory(category: VerbalIndicatorCategory = null) {
    if (category == null) {
      category = {
        name: '',
        weight: 1,
        stages: [
          { name: '', description: '' },
          { name: '', description: '' },
          { name: '', description: '' },
        ],
      };
    }
    this.indicator.verbalIndicatorCategories.push(category);
    this.outcomeValuesInColumn.forEach((_, alternativeIdx) => {
      this.outcomeValuesInColumn[alternativeIdx].forEach((verbalIndicatorValuesPerState, stateIdx) => {
        if (this.indicator.verbalIndicatorCategories.length === 1) {
          // initializing verbal categories, delete older numerical value
          this.outcomeValuesInColumn[alternativeIdx][stateIdx][this.indicatorIdx] = [];
        }
        this.outcomeValuesInColumn[alternativeIdx][stateIdx][this.indicatorIdx].push(undefined);
      });
    });
    setTimeout(() => {
      const scrollContainer = this.verticalScrollContainer.nativeElement;
      if (scrollContainer.clientWidth < scrollContainer.scrollWidth) {
        // container is overflowing-x
        scrollContainer.scrollTo({ left: scrollContainer.scrollWidth - scrollContainer.clientWidth, behavior: 'smooth' });
      }
    }, 50);
  }

  // definition mode
  removeCategory(categoryIdx: number) {
    this.indicator.verbalIndicatorCategories.splice(categoryIdx, 1);
    this.outcomeValuesInColumn.forEach((_, alternativeIdx) => {
      this.outcomeValuesInColumn[alternativeIdx].forEach((_, stateIdx) => {
        this.outcomeValuesInColumn[alternativeIdx][stateIdx][this.indicatorIdx].splice(categoryIdx, 1);
      });
    });
  }

  // definition mode
  addCategoryFromHierarchy(element: SimpleHierarchyNode, location: LocationSequence, htmlElement: HTMLElement) {
    if (location.getLevel() === 0) {
      return;
    }

    this.createCategory({
      name: element.name,
      weight: 1,
      stages: [
        { name: '', description: '' },
        { name: '', description: '' },
        { name: '', description: '' },
      ],
    });
    this.popOverService.whistle(htmlElement, $localize`Als Kategorie hinzugefügt!`, 'add');
  }

  // definition mode
  addCategoryFromScrews([categoryName, htmlElement]: [string, HTMLElement]) {
    this.createCategory({
      name: categoryName,
      weight: 1,
      stages: [
        { name: '', description: '' },
        { name: '', description: '' },
        { name: '', description: '' },
      ],
    });
    this.popOverService.whistle(htmlElement, $localize`Als Kategorie hinzugefügt!`, 'add');
  }

  // forecast mode
  setVerbalIndicatorValue(categoryIdx: number, newValue: number) {
    if (!this.editable) {
      this.outcomeValues[this.stateIdx][this.indicatorIdx][categoryIdx] = newValue;
      this.forecastResult = calculateIndicatorValue(this.outcomeValues[this.stateIdx][this.indicatorIdx], this.indicator);
    }
  }

  /* --- POPOVER --- */
  openHierarchyScrew() {
    if (this.popOverRef != null) return;
    this.popOverRef = this.popOverService.open(this.sourcesTemplate, this.sourcesButton, {
      hasBackdrop: false, // backdrop prevents drag & drop
      position: [{ originX: 'center', originY: 'top', overlayX: 'center', overlayY: 'bottom' }],
    });
    // position the draggable header of the popover under the cursor
    // also prevents the popover from clipping out of the viewport when switching between suggestionModes
    this.popOverService.setPosition(
      this.sourcesButton.nativeElement.getBoundingClientRect().left,
      this.sourcesButton.nativeElement.getBoundingClientRect().top,
      this.popOverRef.overlayRef,
    );
  }

  popOverDrag(event: MouseEvent, isTitle = false) {
    event.stopImmediatePropagation();
    // if the title is dragged on, we need to adjust offsetX/offsetY (must always be relative to .hierarchy-screw-header)
    this.mouseDownOffsetX = event.offsetX + (isTitle ? (event.target as HTMLElement).offsetLeft : 0);
    this.mouseDownOffsetY = event.offsetY + (isTitle ? (event.target as HTMLElement).offsetTop : 0);
    this.isDraggingPopOver = true;
  }

  popOverDragTouch(event: TouchEvent) {
    const rect = (event.target as HTMLElement).getBoundingClientRect();
    this.mouseDownOffsetX = event.touches[0].clientX - window.scrollX - rect.left;
    this.mouseDownOffsetY = event.touches[0].clientY - window.scrollY - rect.top;
    this.isDraggingPopOver = true;
  }

  /* --- CDK ON DROP FUNCTIONS --- */
  // definition mode
  swapCategories(event: CdkDragDrop<VerbalIndicatorCategory[]>) {
    moveItemInArray(this.indicator.verbalIndicatorCategories, event.previousIndex, event.currentIndex);
    this.outcomeValuesInColumn.forEach((_, alternativeIdx) => {
      this.outcomeValuesInColumn[alternativeIdx].forEach((_, stateIdx) => {
        moveItemInArray(this.outcomeValuesInColumn[alternativeIdx][stateIdx][this.indicatorIdx], event.previousIndex, event.currentIndex);
      });
    });
  }

  // definition mode
  swapStages(event: CdkDragDrop<VerbalIndicatorCategoryStage[]>, categoryIdx: number) {
    // TODO: DISABLED by #446
    return;
    moveItemInArray(this.indicator.verbalIndicatorCategories[categoryIdx].stages, event.previousIndex, event.currentIndex);

    const affectedInterval = new Interval(event.previousIndex, event.currentIndex);
    const positionShift = event.previousIndex < event.currentIndex ? -1 : 1;

    this.outcomeValuesInColumn.flat(2).forEach(valuesPerCategory => {
      if (valuesPerCategory[categoryIdx] === event.previousIndex) {
        valuesPerCategory[categoryIdx] = event.currentIndex;
      } else if (valuesPerCategory[categoryIdx] != null && affectedInterval.includes(valuesPerCategory[categoryIdx])) {
        valuesPerCategory[categoryIdx] += positionShift;
      }
    });
  }

  protected forecastPercentage() {
    return (this.forecastResult - this.indicator.min) / (this.indicator.max - this.indicator.min);
  }

  // determines a good number of scale stages, so that displayed numbers are "good-looking"
  // tries with 7 -> 6 -> .. -> 3 divisions, if none produce "good-looking" numbers it takes 5 stages
  private calculateScaleValues(min: number, max: number) {
    let scaleDiff = Math.round((max - min) * 10 ** 5) / 10 ** 5;
    while (scaleDiff % 1 != 0) {
      scaleDiff *= 10;
    }
    // if scaleDiff % n == 1 then the scale can be divided in n + 1 parts (including min and max)
    for (let divisor = this.maxNumberOfStages; divisor >= 3; divisor--) {
      if (scaleDiff % (divisor - 1) === 0) {
        this.numberOfStages = divisor;
        break;
      }
    }
    // no good divisor was found, use default
    if (this.numberOfStages == null) this.numberOfStages = this.defaultNumberOfStages;
    for (let i = 0; i < this.numberOfStages; i++) {
      const stage = min + (i * (max - min)) / (this.numberOfStages - 1);
      this.scaleValues.push(NumberRoundingPipe.prototype.transform(stage, 5));
    }
  }
}
