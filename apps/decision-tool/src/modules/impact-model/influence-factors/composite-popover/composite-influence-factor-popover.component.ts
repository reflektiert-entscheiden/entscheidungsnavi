import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import {
  DecisionData,
  generateListOfUnderlyingUserDefinedFactors,
  SimpleUserDefinedInfluenceFactor,
} from '@entscheidungsnavi/decision-data';
import { CompositeUserDefinedInfluenceFactor, CrossImpact, UserDefinedInfluenceFactor } from '@entscheidungsnavi/decision-data';
import { range } from 'lodash';

@Component({
  selector: 'dt-composite-influence-factor-popover',
  templateUrl: './composite-influence-factor-popover.component.html',
  styleUrls: ['./composite-influence-factor-popover.component.scss'],
})
export class CompositeInfluenceFactorPopoverComponent implements OnInit {
  influenceFactorsWithBaseIFs: Map<UserDefinedInfluenceFactor, { underlyingFactors: SimpleUserDefinedInfluenceFactor[] }> = new Map();
  @Input() userDefinedIFs: Map<SimpleUserDefinedInfluenceFactor, { blackList: Set<SimpleUserDefinedInfluenceFactor> }>;
  // emits the id of the newly created custom influence factor
  @Output() create = new EventEmitter<number>();

  firstBaseFactor: UserDefinedInfluenceFactor;
  secondBaseFactor: UserDefinedInfluenceFactor;

  availableInfluenceFactor: UserDefinedInfluenceFactor[];

  get isValid() {
    return this.canCombineInScenario(this.firstBaseFactor, this.secondBaseFactor);
  }

  constructor(protected decisionData: DecisionData) {}

  ngOnInit() {
    for (const influenceFactor of this.decisionData.influenceFactors) {
      const underlyingFactors = generateListOfUnderlyingUserDefinedFactors(influenceFactor);
      this.influenceFactorsWithBaseIFs.set(influenceFactor, { underlyingFactors });
    }

    // compute the set of influence factors that can be combined (not every combination between its elements is valid)
    this.availableInfluenceFactor = Array.from(this.influenceFactorsWithBaseIFs.keys()).filter(customIF1 =>
      Array.from(this.influenceFactorsWithBaseIFs.keys()).some(customIF2 => this.canCombineInScenario(customIF1, customIF2)),
    );

    // make sure the default selected combination is a valid one
    this.firstBaseFactor = this.availableInfluenceFactor[0];
    this.secondBaseFactor = this.availableInfluenceFactor.find(availableIF => this.canCombineInScenario(this.firstBaseFactor, availableIF));
  }

  save() {
    const crossImpacts: CrossImpact[][] = range(this.firstBaseFactor.stateCount).map(() =>
      range(this.secondBaseFactor.stateCount).map(() => 3),
    );
    const iF = this.decisionData.addInfluenceFactor(
      new CompositeUserDefinedInfluenceFactor(-1, '', [this.firstBaseFactor, this.secondBaseFactor], crossImpacts, ''),
    );
    this.create.emit(iF.id);
  }

  // Checks whether the two user-defined influence factors can be combined into a scenario.
  // This is done by checking if the underlying user-defined influence factors of `customIF1`
  // are not blacklisted by some underlying influence factor of `customIF2` and vice versa.
  protected canCombineInScenario(customIF1: UserDefinedInfluenceFactor, customIF2: UserDefinedInfluenceFactor) {
    const underlyingFactors1 = this.influenceFactorsWithBaseIFs.get(customIF1).underlyingFactors;
    const underlyingFactors2 = this.influenceFactorsWithBaseIFs.get(customIF2).underlyingFactors;
    return (
      customIF1 !== customIF2 &&
      underlyingFactors1.every(baseIF =>
        [...this.userDefinedIFs.get(baseIF).blackList].every(blackListedIF => !underlyingFactors2.includes(blackListedIF)),
      ) &&
      underlyingFactors2.every(baseIF =>
        [...this.userDefinedIFs.get(baseIF).blackList].every(blackListedIF => !underlyingFactors1.includes(blackListedIF)),
      )
    );
  }
}
