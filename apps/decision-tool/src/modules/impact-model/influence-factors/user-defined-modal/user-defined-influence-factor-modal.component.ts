import { Component, ElementRef, Inject, OnInit, QueryList, TemplateRef, ViewChild, ViewChildren } from '@angular/core';
import { ConfirmModalComponent, PopOverRef, PopOverService } from '@entscheidungsnavi/widgets';
import {
  ActionEntry,
  checkProbabilities,
  CompositeUserDefinedInfluenceFactor,
  DecisionData,
  generateListOfUnderlyingUserDefinedFactors,
  normalizeProbabilities,
  SimpleUserDefinedInfluenceFactor,
  UserDefinedState,
} from '@entscheidungsnavi/decision-data';
import { ArrayCopy, normalizeDiscrete } from '@entscheidungsnavi/tools';
import { clamp } from 'lodash';
import { CdkDragDrop } from '@angular/cdk/drag-drop';
import { MAT_DIALOG_DATA, MatDialog, MatDialogRef } from '@angular/material/dialog';
import { lastValueFrom } from 'rxjs';
import { animate, state, style, transition, trigger } from '@angular/animations';
import { NgModel } from '@angular/forms';
import { PersistentSetting, PersistentSettingParent } from '@entscheidungsnavi/widgets';
import { InfluenceFactorHelperService } from '../influence-factors-helper.service';

export interface UserDefinedInfluenceFactorModalData {
  influenceFactor?: SimpleUserDefinedInfluenceFactor; // A new influence factor is created when this is null
  usageCount?: number;
  probabilities?: number[]; // Optionally, the probabilities in the influence factor can be overwritten
  onSaveCallback?: (influenceFactorId: number) => void;
}

export type UserDefinedInfluenceFactorModalResult = { showConnections?: boolean } & (
  | { action: 'discard' }
  | { action: 'save'; influenceFactorId: number }
);

@Component({
  templateUrl: './user-defined-influence-factor-modal.component.html',
  styleUrls: ['./user-defined-influence-factor-modal.component.scss'],
  animations: [
    trigger('expandCollapseIndicator', [
      state('false', style({ transform: 'rotate(0)' })),
      state('true', style({ transform: 'rotate(-180deg)' })),
      transition('true <=> false', [animate('300ms ease-in-out')]),
    ]),
  ],
})
@PersistentSettingParent('UserDefinedInfluenceFactorModalComponent')
export class UserDefinedInfluenceFactorModalComponent implements OnInit {
  @ViewChild('premadeOpener', { read: ElementRef }) premadeOpener: ElementRef;
  @ViewChild('premadePopoverTemplate', { read: TemplateRef }) premadePopoverTemplate: TemplateRef<any>;

  @ViewChild('confirmShowConnections') confirmShowConnectionsModal: TemplateRef<any>;

  @ViewChildren(NgModel) allInputs: QueryList<NgModel>;

  @ViewChildren('stateInput') stateInputs: QueryList<ElementRef<HTMLInputElement>>;

  @ViewChild('confirmModal', { static: true }) confirmModal: TemplateRef<any>;

  readonly pMax = SimpleUserDefinedInfluenceFactor.P_MAX;
  readonly pDefault = SimpleUserDefinedInfluenceFactor.P_DEFAULT;

  premadePopover: PopOverRef;
  premadeMean = 0;
  premadeStandardDeviation = 0.4;
  premadePopoverProbabilities: number[];

  // a list of actions covering every 'add', 'delete' and 'move' action in chronological order
  actionList: ActionEntry[] = [];

  @PersistentSetting()
  commentExpanded = false;

  copy: {
    name: string;
    states: ArrayCopy<UserDefinedState>;
    precision: number;
    comment: string;
  };

  /**
   * Checks whether everything is valid IGNORING normalization
   */
  get isValid() {
    return (
      this.copy.name &&
      this.copy.states.length > 1 &&
      this.copy.states.elements.every(state => state.name && state.probability >= 0 && state.probability <= 100) &&
      this.copy.precision != null
    );
  }
  get areProbabilitiesValid() {
    return checkProbabilities(this.copy.states.elements.map(state => state.probability));
  }

  get isNew() {
    return this.data.influenceFactor == null;
  }

  get hasChanges() {
    if (this.isNew) {
      return true;
    } else {
      const uf = this.data.influenceFactor;
      return (
        this.copy.name !== uf.name ||
        this.copy.precision !== uf.precision ||
        this.copy.states.hasChanges(
          uf.states,
          (orig, cur) => orig.name === cur.name && orig.probability === cur.probability && orig.comment === cur.comment,
        ) ||
        this.copy.comment !== uf.comment
      );
    }
  }

  get hasStructuralChanges() {
    return this.actionList.length > 0;
  }

  // returns scenarios of depth >= 2 that contain the influence factor
  get scenariosToBeReset() {
    return (
      !this.isNew &&
      this.decisionData
        .getCompositeUserDefinedInfluenceFactors()
        .filter(
          compositeIF =>
            (compositeIF.baseFactors[0] instanceof CompositeUserDefinedInfluenceFactor &&
              generateListOfUnderlyingUserDefinedFactors(compositeIF.baseFactors[0]).includes(this.data.influenceFactor)) ||
            (compositeIF.baseFactors[1] instanceof CompositeUserDefinedInfluenceFactor &&
              generateListOfUnderlyingUserDefinedFactors(compositeIF.baseFactors[1]).includes(this.data.influenceFactor)),
        )
    );
  }

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: UserDefinedInfluenceFactorModalData,
    public dialogRef: MatDialogRef<UserDefinedInfluenceFactorModalComponent, UserDefinedInfluenceFactorModalResult>,
    private dialog: MatDialog,
    private popOverService: PopOverService,
    private influenceFactorHelper: InfluenceFactorHelperService,
    private decisionData: DecisionData,
  ) {
    this.dialogRef.disableClose = true;

    if (data.influenceFactor && data.usageCount == null) {
      data.usageCount = this.decisionData.outcomes.flat().filter(outcome => outcome.influenceFactor === data.influenceFactor).length;
    }
  }

  openPremadeOptions() {
    if (this.copy.states.length === 0) {
      return;
    }

    this.premadePopover = this.popOverService.open(this.premadePopoverTemplate, this.premadeOpener, {
      position: [{ originX: 'center', originY: 'bottom', overlayX: 'center', overlayY: 'top' }],
      closeCallback: () => {
        this.premadePopover = undefined;
        this.premadePopoverProbabilities = undefined;
      },
    });
    this.calculatePopoverProbabilities();
  }

  ngOnInit() {
    this.load();
  }

  stateDrop(event: CdkDragDrop<UserDefinedState[]>) {
    this.copy.states.move(event.previousIndex, event.currentIndex);
    this.actionList.push({ action: 'move', fromIndex: event.previousIndex, toIndex: event.currentIndex });
  }

  async tryClose() {
    if (this.hasChanges) {
      const result = await lastValueFrom(
        this.dialog
          .open(ConfirmModalComponent, {
            data: {
              title: $localize`Ungespeicherte Änderungen`,
              prompt: $localize`Bist Du sicher, dass Du die ungespeicherten Änderungen am Einflussfaktor verwerfen willst?`,
              buttonConfirm: $localize`Ja, Änderungen verwerfen`,
              buttonDeny: $localize`Nein, abbrechen`,
            },
          })
          .afterClosed(),
      );

      if (result) {
        this.discard();
      }

      return result;
    } else {
      this.discard();
      return true;
    }
  }

  onSubmitClick() {
    if (this.isValid) {
      // TODO: Uncomment or delete in the future
      this.save();
      // if (this.scenariosToBeReset.length > 0 && this.hasStructuralChanges) {
      //   this.dialog
      //     .open(this.confirmModal)
      //     .afterClosed()
      //     .subscribe((confirmed: boolean) => {
      //       if (confirmed) {
      //         this.save();
      //       }
      //     });
      // } else {
      //   this.save();
      // }
    } else {
      while (this.copy.states.length < 2) {
        this.addState();
      }

      this.allInputs.forEach(input => input.control.markAsTouched());
    }
  }

  discard() {
    this.dialogRef.close({ action: 'discard' });
  }

  private load() {
    if (this.isNew) {
      this.copy = {
        name: '',
        states: ArrayCopy.createEmpty(),
        precision: this.pDefault,
        comment: '',
      };
      // Add two states
      this.copy.states.add(0, { name: '', probability: 0 });
      this.copy.states.add(0, { name: '', probability: 0 });
    } else {
      const uf = this.data.influenceFactor;
      this.copy = {
        name: uf.name,
        states: ArrayCopy.createArrayCopy(uf.states, (state, index) => ({
          name: state.name,
          probability: this.data.probabilities?.[index] ?? state.probability,
          comment: state.comment,
        })),
        precision: uf.precision,
        comment: uf.comment,
      };
    }
  }

  private save(showConnections = false) {
    // Normalize if necessary
    if (!this.areProbabilitiesValid) {
      this.normalizeProbabilities();
      this.influenceFactorHelper.showInfluenceFactorsNormalizedNotification([this.copy.name]);
    }

    if (this.isNew) {
      const newUf = new SimpleUserDefinedInfluenceFactor(
        this.copy.name,
        this.copy.states.elements,
        null,
        this.copy.precision,
        this.copy.comment,
      );
      this.decisionData.addInfluenceFactor(newUf);
      this.data.onSaveCallback?.(newUf.id);
      this.dialogRef.close({ action: 'save', influenceFactorId: newUf.id, showConnections });
    } else {
      const uf = this.data.influenceFactor;
      uf.name = this.copy.name;
      this.copy.states.mergeBack(
        uf.states,
        (position, element) => {
          // add
          this.decisionData.addState(uf, element, position);
        },
        (position, element) => {
          // set
          Object.assign(uf.states[position], element);
        },
        position => {
          // remove
          this.decisionData.removeState(uf, position);
        },
        (fromPosition, toPosition) => {
          this.decisionData.moveState(uf, fromPosition, toPosition);
        },
      );
      uf.precision = this.copy.precision;
      uf.comment = this.copy.comment;
      this.decisionData.influenceFactorStateCountChanged(uf, this.actionList);
      this.data.onSaveCallback?.(uf.id);
      this.dialogRef.close({ action: 'save', influenceFactorId: uf.id, showConnections });
    }
  }

  calculatePopoverProbabilities() {
    const numberOfStates = this.copy.states.length;

    if (this.premadeStandardDeviation === 2 && 100 % numberOfStates === 0) {
      // Uniform distribution
      return new Array(numberOfStates).fill(100 / numberOfStates);
    }

    const lowerLimit = -1;
    const upperLimit = 1;
    const len = numberOfStates;

    const lowerIntervalLimit: number[] = [];
    const upperIntervalLimit: number[] = [];

    for (let idx = 0; idx < len; idx++) {
      lowerIntervalLimit.push(cdf(lowerLimit + (idx * (upperLimit - lowerLimit)) / len, this.premadeMean, this.premadeStandardDeviation));
    }

    for (let idx = 0; idx < len; idx++) {
      upperIntervalLimit.push(
        cdf(lowerLimit + ((idx + 1) * (upperLimit - lowerLimit)) / len, this.premadeMean, this.premadeStandardDeviation),
      );
    }

    const cdfDiff = upperIntervalLimit.map((val, idx) => +val - +lowerIntervalLimit[idx]);
    const cdfDiffSum = cdfDiff.reduce((sum, val) => sum + val, 0);

    const probabilities = cdfDiff.map(val => (100 * val) / cdfDiffSum); // normalize and multiply with 100 for bar slider
    this.premadePopoverProbabilities = normalizeDiscrete(probabilities, 100, 1);
  }

  /**
   * calculate new probabilities with new mean value or standard derivation
   */
  savePopoverProbabilities() {
    this.copy.states.elements.forEach(val => (val.probability = this.premadePopoverProbabilities.shift()));
    this.premadePopover.close();
  }

  enter(index: number) {
    if (index !== this.copy.states.length - 1) {
      this.stateInputs.get(index + 1).nativeElement.focus();
    } else {
      this.addState();
      setTimeout(() => {
        this.stateInputs.last.nativeElement.focus();
      });
    }
  }

  /**
   * Add a state at the given position.
   */
  addState(position: number = this.copy.states.length) {
    this.copy.states.add(position, { name: '', probability: 0 });
    this.actionList.push({ action: 'add', index: position });
  }

  /**
   * Remove the state at the given position.
   */
  removeState(position: number) {
    this.copy.states.remove(position);
    this.actionList.push({ action: 'delete', index: position });
  }

  normalizeProbabilities() {
    normalizeProbabilities(this.copy.states.elements);
  }

  relativeAbweichung(probability: number, praezision: number) {
    return probability > 0 ? praezision * Math.min(1 - probability / 100, probability / 100) : 0;
  }

  adjustPrecision(change: number) {
    this.changePrecision(this.copy.precision + change);
  }

  changePrecision(newP: number) {
    this.copy.precision = newP == null ? null : clamp(newP.round(2), 0, this.pMax);
  }

  updateProbability(state: UserDefinedState, change: number) {
    state.probability = clamp((state.probability ?? 0) + change, 0, 100);
  }

  async showConnections() {
    if (this.isNew || !this.isValid) {
      return;
    }

    if (!this.hasChanges) {
      this.dialogRef.close({ action: 'discard', showConnections: true });
    } else {
      const modalResult = await lastValueFrom(this.dialog.open(this.confirmShowConnectionsModal).afterClosed());

      if (modalResult === 'save') {
        this.save(true);
      } else if (modalResult === 'dismiss') {
        this.dialogRef.close({ action: 'discard', showConnections: true });
      }
    }
  }
}

/**
 * taken from {@link https://stackoverflow.com/questions/14846767/std-normal-cdf-normal-cdf-or-error-function/14873282}
 */
function erf(x: number): number {
  // save the sign of x
  const sign = x >= 0 ? 1 : -1;
  x = Math.abs(x);

  // constants
  const a1 = 0.254829592;
  const a2 = -0.284496736;
  const a3 = 1.421413741;
  const a4 = -1.453152027;
  const a5 = 1.061405429;
  const p = 0.3275911;

  const t = 1.0 / (1.0 + p * x);
  const y = 1.0 - ((((a5 * t + a4) * t + a3) * t + a2) * t + a1) * t * Math.exp(-x * x);
  return sign * y; // erf(-x) = -erf(x);
}

function cdf(x: number, mu: number, sigma: number): number {
  return 0.5 * (1 + erf((x - mu) / Math.sqrt(2 * sigma * sigma)));
}
