import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { InfluenceFactor } from '@entscheidungsnavi/decision-data';
import { DecisionData } from '@entscheidungsnavi/decision-data';
import { SnackbarComponent, SnackbarData } from '@entscheidungsnavi/widgets';

@Injectable({
  providedIn: 'root',
})
export class InfluenceFactorHelperService {
  constructor(
    private router: Router,
    private snackBar: MatSnackBar,
    private decisionData: DecisionData,
  ) {}

  showInfluenceFactorConnections(influenceFactor: InfluenceFactor) {
    const params = { highlightInfluenceFactor: influenceFactor.id };
    this.router.navigate([this.decisionData.isProfessional() ? '/professional/structure-and-estimate' : '/impactmodel'], {
      queryParams: params,
    });
  }

  /**
   * Shows a SnackBar notifying the user that influence factors have been automatically normalized.
   *
   * @param influenceFactorNames - The names of the normalized influence factors
   */
  showInfluenceFactorsNormalizedNotification(influenceFactorNames: string[]) {
    if (influenceFactorNames.length === 0) {
      return;
    }

    const message =
      influenceFactorNames.length > 1
        ? $localize`Die Wahrscheinlichkeiten der Einflussfaktoren
        „${influenceFactorNames.slice(0, -1).join(', ')}“ und
        „${influenceFactorNames[influenceFactorNames.length - 1]}“ wurden automatisch normiert.`
        : $localize`Die Wahrscheinlichkeiten des Einflussfaktors „${influenceFactorNames[0]}“ wurden automatisch normiert.`;

    this.snackBar.openFromComponent<SnackbarComponent, SnackbarData>(SnackbarComponent, {
      data: { message, dismissButton: true },
      duration: undefined,
    });
  }
}
