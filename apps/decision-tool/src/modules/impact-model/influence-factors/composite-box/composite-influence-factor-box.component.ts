import { Component, EventEmitter, inject, Input, OnInit, Output } from '@angular/core';
import { CompositeUserDefinedInfluenceFactor, InfluenceFactorState } from '@entscheidungsnavi/decision-data';
import { InfluenceFactorHelperService } from '../influence-factors-helper.service';

@Component({
  selector: 'dt-composite-influence-factor-box',
  templateUrl: './composite-influence-factor-box.component.html',
  styleUrls: ['./composite-influence-factor-box.component.scss'],
})
export class CompositeInfluenceFactorBoxComponent implements OnInit {
  // TODO: Uncomment or delete in the future
  // readonly crossImpacts: CrossImpact[] = [1, 2, 3, 4, 5];

  @Input() influenceFactor: CompositeUserDefinedInfluenceFactor;
  @Input() usageCount: number;
  @Input() usedInCombinedIF: boolean;

  @Input() readonly = false;

  @Output() deleteClick = new EventEmitter<void>();

  firstBaseFactorStates: InfluenceFactorState[];
  secondBaseFactorStates: InfluenceFactorState[];
  probabilities: number[][];

  private influenceFactorHelper = inject(InfluenceFactorHelperService);

  ngOnInit() {
    this.recomputeProbabilities();
  }

  recomputeProbabilities() {
    this.firstBaseFactorStates = this.influenceFactor.baseFactors[0].getStates();
    this.secondBaseFactorStates = this.influenceFactor.baseFactors[1].getStates();
    this.probabilities = this.influenceFactor.computeProbabilityMatrix();
  }

  showConnections() {
    this.influenceFactorHelper.showInfluenceFactorConnections(this.influenceFactor);
  }
}
