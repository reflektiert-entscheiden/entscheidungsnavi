import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { checkProbabilities, SimpleUserDefinedInfluenceFactor } from '@entscheidungsnavi/decision-data';
import { clamp, debounce } from 'lodash';
import {
  UserDefinedInfluenceFactorModalComponent,
  UserDefinedInfluenceFactorModalData,
  UserDefinedInfluenceFactorModalResult,
} from '../user-defined-modal/user-defined-influence-factor-modal.component';
import { InfluenceFactorHelperService } from '../influence-factors-helper.service';

@Component({
  selector: 'dt-user-defined-influence-factor-box',
  templateUrl: './user-defined-influence-factor-box.component.html',
  styleUrls: ['./user-defined-influence-factor-box.component.scss'],
})
export class UserDefinedInfluenceFactorBoxComponent implements OnInit {
  @Input() influenceFactor: SimpleUserDefinedInfluenceFactor;
  @Input() usageCount: number;
  @Input() usedInCombinedIF: boolean;
  @Input() readonly = false;

  @Output() deleteClick = new EventEmitter<void>();
  @Output() isNormalizedChange = new EventEmitter<boolean>();
  @Output() probabilitiesChange = new EventEmitter<void>();

  readonly pMax = SimpleUserDefinedInfluenceFactor.P_MAX;

  // We show unnormalized probabilities, but save normalized ones in the background
  shownProbabilities: number[];
  // Wether the shownProbabilities are normalized. The saved probabilites are always normalized.
  isNormalized: boolean;

  private writeBackProbabilities = debounce(() => {
    // Write the probs back to the influence factor and normalize
    this.shownProbabilities.forEach((prob, index) => {
      this.influenceFactor.states[index].probability = clamp(prob, 0, 100);
    });
    this.influenceFactor.normalizeProbabilities();
    this.probabilitiesChange.emit();
  }, 500);

  get isValid() {
    return this.shownProbabilities.every(p => p != null && p >= 0 && p <= 100);
  }

  constructor(
    private dialog: MatDialog,
    private influenceFactorHelper: InfluenceFactorHelperService,
  ) {}

  ngOnInit() {
    this.initProbabilities();
  }

  changePrecision(newPrecision: number) {
    this.influenceFactor.precision = clamp(newPrecision, 0, SimpleUserDefinedInfluenceFactor.P_MAX);
  }

  adjustPrecision(change: number) {
    this.changePrecision(this.influenceFactor.precision + change);
  }

  changeProbability(index: number, value: number) {
    this.shownProbabilities[index] = value;
    this.checkNormalized();
    this.writeBackProbabilities();
  }

  adjustProbability(index: number, change: number) {
    this.changeProbability(index, this.shownProbabilities[index] + change);
  }

  private checkNormalized() {
    const isNormalized = checkProbabilities(this.shownProbabilities);
    if (isNormalized !== this.isNormalized) {
      this.isNormalized = isNormalized;
      this.isNormalizedChange.next(isNormalized);
    }
  }

  normalize() {
    this.writeBackProbabilities.flush();
    this.initProbabilities();
  }

  initProbabilities() {
    this.shownProbabilities = this.influenceFactor.states.map(state => state.probability ?? 0);
    this.checkNormalized();
  }

  edit() {
    if (this.readonly) {
      return;
    }

    this.dialog
      .open<UserDefinedInfluenceFactorModalComponent, UserDefinedInfluenceFactorModalData, UserDefinedInfluenceFactorModalResult>(
        UserDefinedInfluenceFactorModalComponent,
        {
          data: {
            influenceFactor: this.influenceFactor,
            usageCount: this.usageCount,
            probabilities: this.shownProbabilities,
            onSaveCallback: () => {
              this.initProbabilities();
              this.probabilitiesChange.emit();
            },
          },
        },
      )
      .afterClosed()
      .subscribe((result: UserDefinedInfluenceFactorModalResult) => {
        if (result.showConnections) this.showConnections();
      });
  }

  showConnections() {
    this.influenceFactorHelper.showInfluenceFactorConnections(this.influenceFactor);
  }

  relativeDeviation(probability: number) {
    return probability > 0 ? this.influenceFactor.precision * Math.min(1 - probability / 100, probability / 100) : 0;
  }
}
