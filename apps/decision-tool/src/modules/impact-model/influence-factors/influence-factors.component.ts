import { Component, ElementRef, Input, OnDestroy, OnInit, QueryList, ViewChildren } from '@angular/core';
import {
  CompositeUserDefinedInfluenceFactor,
  UserDefinedInfluenceFactor,
  generateListOfUnderlyingUserDefinedFactors,
  isCustomInfluenceFactor,
  SimpleUserDefinedInfluenceFactor,
} from '@entscheidungsnavi/decision-data';
import { DecisionData } from '@entscheidungsnavi/decision-data';
import { MatDialog } from '@angular/material/dialog';
import { Observable } from 'rxjs';
import { OnDestroyObservable } from '@entscheidungsnavi/tools';
import { takeUntil } from 'rxjs/operators';
import { PopOverService } from '@entscheidungsnavi/widgets';
import { HelpMenuProvider } from '../../../app/help/help';
import { getHelpMenu } from '../help';
import { DisplayAtMaxWidth } from '../../../app/interfaces/display-at-max-width';
import { Navigation, NavLine, navLineElement } from '../../shared/navline';
import { LanguageService } from '../../../app/data/language.service';
import { DecisionDataState } from '../../shared/decision-data-state';
import {
  UserDefinedInfluenceFactorModalComponent,
  UserDefinedInfluenceFactorModalData,
} from './user-defined-modal/user-defined-influence-factor-modal.component';
import { InfluenceFactorHelperService } from './influence-factors-helper.service';
import { CompositeInfluenceFactorBoxComponent } from './composite-box/composite-influence-factor-box.component';
import { CompositeInfluenceFactorPopoverComponent } from './composite-popover/composite-influence-factor-popover.component';

@Component({
  selector: 'dt-influence-factors',
  templateUrl: './influence-factors.component.html',
  styleUrls: ['./influence-factors.component.scss'],
})
@DisplayAtMaxWidth
export class InfluenceFactorsComponent implements OnInit, Navigation, HelpMenuProvider, OnDestroy {
  @OnDestroyObservable()
  private onDestroy$: Observable<void>;

  @Input()
  showTitle = true;

  readonly navLine = new NavLine({
    middle: [
      navLineElement()
        .label($localize`Einflussfaktor hinzufügen`)
        .onClick(() => this.addUserDefinedIF())
        .build(),
      navLineElement()
        .label($localize`Kombinierten Einflussfaktor hinzufügen`)
        .disabled(() => !this.canAddCombinedIF)
        .tooltip(() =>
          this.canAddCombinedIF ? undefined : $localize`Es müssen mindestens zwei ungenutzte Einflussfaktoren oder Szenarien vorliegen`,
        )
        .onClick(elementRef => this.addCombinedIF(elementRef))
        .build(),
    ],
  }).addWhen(() => this.decisionData.projectMode !== 'professional', {
    left: [navLineElement().back('/impactmodel').build()],
  });
  readonly helpMenu = getHelpMenu(this.languageService.steps, 'uncertaintyfactors');

  influenceFactors: Map<UserDefinedInfluenceFactor, { usage: number; isNormalized: boolean; isUsedInCombinedIF: boolean }>;
  // Every simple user-defined influence factor can only be combined with another simple user-defined influence factor.
  // After combining, both influence factors blacklist each other.
  // Blacklisting also applies on higher level (combinations) - e.g. if A & B is combined with C, then A, B and C all blacklist each other
  userDefinedIFs: Map<SimpleUserDefinedInfluenceFactor, { blackList: Set<SimpleUserDefinedInfluenceFactor> }>;
  canAddCombinedIF: boolean;

  // The influence factors split into the displayed columns
  userDefinedIFColumnCount = 3;
  userDefinedIFColumns: SimpleUserDefinedInfluenceFactor[][];
  combinedIFs: CompositeUserDefinedInfluenceFactor[];

  @ViewChildren(CompositeInfluenceFactorBoxComponent) combinedIFBoxes: QueryList<CompositeInfluenceFactorBoxComponent>;

  constructor(
    protected decisionData: DecisionData,
    private languageService: LanguageService,
    private dialog: MatDialog,
    private influenceFactorHelper: InfluenceFactorHelperService,
    private popoverService: PopOverService,
    protected decisionDataState: DecisionDataState,
  ) {}

  ngOnInit() {
    this.combinedIFs = this.decisionData.influenceFactors.filter(
      (iF): iF is CompositeUserDefinedInfluenceFactor => iF instanceof CompositeUserDefinedInfluenceFactor,
    );

    this.influenceFactors = new Map();
    this.userDefinedIFs = new Map();
    this.decisionData.influenceFactors.forEach(influenceFactor => {
      this.influenceFactors.set(influenceFactor, { usage: 0, isNormalized: true, isUsedInCombinedIF: false });
      if (influenceFactor instanceof SimpleUserDefinedInfluenceFactor) {
        this.userDefinedIFs.set(influenceFactor, { blackList: new Set() });
      }
    });
    this.decisionData.outcomes.forEach(ocForAlt =>
      ocForAlt
        .map(outcome => outcome.influenceFactor)
        .filter(isCustomInfluenceFactor)
        .forEach(influenceFactor => this.influenceFactors.get(influenceFactor).usage++),
    );

    this.decisionData.influenceFactorAdded$.pipe(takeUntil(this.onDestroy$)).subscribe(influenceFactorId => {
      this.influenceFactors.set(this.decisionData.influenceFactors[influenceFactorId], {
        usage: 0,
        isNormalized: true,
        isUsedInCombinedIF: false,
      });
      this.recomputeColumns();
    });
    this.recomputeUsageAndBlacklists();
    this.recomputeColumns();
  }

  ngOnDestroy() {
    const normalizedInfluenceFactors = Array.from(this.influenceFactors.entries())
      .filter(([_, info]) => !info.isNormalized)
      .map(([influenceFactor]) => influenceFactor);

    if (normalizedInfluenceFactors.length > 0) {
      this.influenceFactorHelper.showInfluenceFactorsNormalizedNotification(
        normalizedInfluenceFactors.map(influenceFactor => influenceFactor.name),
      );
    }
  }

  addUserDefinedIF() {
    this.dialog.open<UserDefinedInfluenceFactorModalComponent, UserDefinedInfluenceFactorModalData>(
      UserDefinedInfluenceFactorModalComponent,
      {
        data: {
          onSaveCallback: influenceFactorId => {
            this.influenceFactors.set(this.decisionData.influenceFactors[influenceFactorId], {
              usage: 0,
              isNormalized: true,
              isUsedInCombinedIF: false,
            });
            this.userDefinedIFs.set(this.decisionData.influenceFactors[influenceFactorId] as SimpleUserDefinedInfluenceFactor, {
              blackList: new Set(),
            });
            this.recomputeUsageAndBlacklists();
            this.recomputeColumns();
          },
        },
      },
    );
  }

  addCombinedIF(elementRef: ElementRef<HTMLElement>) {
    const ref = this.popoverService.open(CompositeInfluenceFactorPopoverComponent, elementRef, {
      position: [{ originX: 'center', originY: 'top', overlayX: 'center', overlayY: 'bottom' }],
    });

    // pass computed blacklists and influence factors to the popover
    ref.componentInstance.userDefinedIFs = this.userDefinedIFs;

    ref.componentInstance.create.pipe(takeUntil(ref.closed$)).subscribe(result => {
      const combinedIF = this.decisionData.influenceFactors[result] as CompositeUserDefinedInfluenceFactor;
      this.influenceFactors.set(combinedIF, { isNormalized: true, usage: 0, isUsedInCombinedIF: false });
      this.combinedIFs.push(combinedIF);
      this.recomputeUsageAndBlacklists();
      ref.close();
    });
  }

  deleteInfluenceFactor(influenceFactor: UserDefinedInfluenceFactor) {
    if (influenceFactor instanceof CompositeUserDefinedInfluenceFactor) {
      this.combinedIFs.splice(this.combinedIFs.indexOf(influenceFactor), 1);
    } else {
      this.userDefinedIFs.delete(influenceFactor);
    }
    this.influenceFactors.delete(influenceFactor);
    this.decisionData.removeInfluenceFactor(influenceFactor.id);
    this.recomputeUsageAndBlacklists();
    this.recomputeColumns();
  }

  onWidthTriggerChange(state: { [key: string]: boolean }) {
    const columnCount = state['one-column'] ? 1 : state['two-column'] ? 2 : 3;
    if (columnCount !== this.userDefinedIFColumnCount) {
      this.userDefinedIFColumnCount = columnCount;
      this.recomputeColumns();
    }
  }

  onProbabilitiesChanged(influenceFactor: UserDefinedInfluenceFactor) {
    const dependentInfluenceFactors = this.decisionData.influenceFactors.filter(
      (iF): iF is CompositeUserDefinedInfluenceFactor =>
        iF instanceof CompositeUserDefinedInfluenceFactor && iF.baseFactors.includes(influenceFactor),
    );

    for (const dependentInfluenceFactor of dependentInfluenceFactors) {
      this.onProbabilitiesChanged(dependentInfluenceFactor);
      this.combinedIFBoxes.find(combinedIFBox => combinedIFBox.influenceFactor === dependentInfluenceFactor).recomputeProbabilities();
    }
  }

  private recomputeColumns() {
    // Heuristically split the influence factors into columns
    const columnStateCounts = new Array(this.userDefinedIFColumnCount).fill(0);
    this.userDefinedIFColumns = columnStateCounts.map(_ => []);

    for (const influenceFactor of this.decisionData.getUserDefinedInfluenceFactors()) {
      const smallestColumnHeight = Math.min(...columnStateCounts);
      const smallestColumnIndex = columnStateCounts.indexOf(smallestColumnHeight);

      this.userDefinedIFColumns[smallestColumnIndex].push(influenceFactor);
      columnStateCounts[smallestColumnIndex] += influenceFactor.states.length;
    }
  }

  recomputeUsageAndBlacklists() {
    // update isUsedInCombinedIF
    this.influenceFactors.forEach(value => (value.isUsedInCombinedIF = false));
    this.combinedIFs.forEach(iF => iF.baseFactors.forEach(baseFactor => (this.influenceFactors.get(baseFactor).isUsedInCombinedIF = true)));

    // update blacklists

    // Start by adding each influence factor to its own blacklist
    for (const userDefinedIF of this.userDefinedIFs.keys()) {
      this.userDefinedIFs.get(userDefinedIF).blackList = new Set([userDefinedIF]);
    }

    // For each combination add the involved influence factors to each other's blacklists
    // A & B => A and B should have each other in their blacklists
    for (const combinedIF of this.combinedIFs) {
      const underlyingUserDefinedEFs = generateListOfUnderlyingUserDefinedFactors(combinedIF);
      for (const underlyingUserDefinedEF1 of underlyingUserDefinedEFs) {
        underlyingUserDefinedEFs.forEach(underlyingUserDefinedEF2 =>
          this.userDefinedIFs.get(underlyingUserDefinedEF1).blackList.add(underlyingUserDefinedEF2),
        );
      }
    }

    // For each combination add the involved influence factors to each other's blacklists
    // this repeats a lot of the elements in the blacklist but helps catch implications
    // e.g. A & B and B & C => A and C should have each other in their blacklists
    for (const userDefinedIF of this.userDefinedIFs.keys()) {
      for (const innerUserDefinedIF of this.userDefinedIFs.get(userDefinedIF).blackList) {
        if (userDefinedIF !== innerUserDefinedIF) {
          this.userDefinedIFs
            .get(userDefinedIF)
            .blackList.forEach(blackListedIF => this.userDefinedIFs.get(innerUserDefinedIF).blackList.add(blackListedIF));
        }
      }
    }

    this.userDefinedIFs = new Map(this.userDefinedIFs);

    // all possible combinations are created iff all blacklists are filled
    this.canAddCombinedIF = Array.from(this.userDefinedIFs.values()).some(value => value.blackList.size < this.userDefinedIFs.size);
  }
}
