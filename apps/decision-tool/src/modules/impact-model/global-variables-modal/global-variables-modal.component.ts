import { Component } from '@angular/core';

import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { DecisionData, Objective, parseFormula } from '@entscheidungsnavi/decision-data';
import { ModalModule } from '@entscheidungsnavi/widgets/modal/modal.module';
import { MatButtonModule } from '@angular/material/button';
import { FormArray, FormControl, FormGroup, NonNullableFormBuilder, ReactiveFormsModule, ValidatorFn, Validators } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatIconModule } from '@angular/material/icon';
import { MatTooltipModule } from '@angular/material/tooltip';
import {
  ConfirmModalComponent,
  ConfirmModalData,
  HoverPopOverDirective,
  matchParentError,
  patternValidator,
} from '@entscheidungsnavi/widgets';
import { firstValueFrom, Observable } from 'rxjs';
import { OnDestroyObservable } from '@entscheidungsnavi/tools';
import { SymbolNode } from 'mathjs';

@Component({
  templateUrl: './global-variables-modal.component.html',
  styleUrls: ['./global-variables-modal.component.scss'],
  standalone: true,
  imports: [
    ModalModule,
    MatButtonModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    MatIconModule,
    MatTooltipModule,
    HoverPopOverDirective,
  ],
})
export class GlobalVariablesModalComponent {
  @OnDestroyObservable()
  private onDestroy$: Observable<void>;

  readonly formGroup = this.fb.group({
    variables: this.fb.array(
      Array.from(this.decisionData.globalVariables.map.entries()).map(([key, value]) => this.getFormForEntry(key, value)),
      this.duplicateNameValidator(),
    ),
  });

  protected usedVariables: Map<string, Objective[]> = new Map();

  constructor(
    private dialogRef: MatDialogRef<GlobalVariablesModalComponent>,
    private dialog: MatDialog,
    private decisionData: DecisionData,
    private fb: NonNullableFormBuilder,
  ) {
    // Check which variables are currently used in custom aggregation formula
    for (const objective of this.decisionData.objectives) {
      if (objective.isIndicator && objective.indicatorData.useCustomAggregation) {
        const parsedFormula = parseFormula(objective.indicatorData.customAggregationFormula);

        const usedSymbols = new Set(
          parsedFormula
            .filter(() => true)
            .filter((node): node is SymbolNode => node.type === 'SymbolNode')
            .map(node => node.name),
        );

        for (const node of usedSymbols) {
          this.usedVariables.set(node, (this.usedVariables.get(node) || []).concat(objective));
        }
      }
    }

    const usedVariablesSet = new Set(this.usedVariables.keys());

    // Disable form elements for variables that are used in custom aggregation formulas
    for (const formElement of this.formGroup.controls.variables.controls) {
      const name = formElement.value.name;
      if (usedVariablesSet.has(name)) {
        formElement.controls.name.disable();
      }
    }
  }

  save() {
    if (this.formGroup.invalid) return;

    this.decisionData.globalVariables.map.clear();
    this.formGroup.getRawValue().variables.forEach(({ name, value }) => this.decisionData.globalVariables.map.set(name, value));
    this.decisionData.globalVariables.variablesChanged$.next();

    this.dialogRef.close(true);
  }

  async discard() {
    if (
      this.formGroup.dirty &&
      !(await firstValueFrom(
        this.dialog
          .open<ConfirmModalComponent, ConfirmModalData, boolean>(ConfirmModalComponent, {
            data: {
              title: $localize`Änderungen verwerfen`,
              prompt: $localize`Möchtest Du ungespeicherte Änderungen verwerfen?`,
              template: 'discard',
            },
          })
          .afterClosed(),
      ))
    )
      return;

    this.dialogRef.close();
  }

  addVariable() {
    this.formGroup.controls.variables.push(this.getFormForEntry('', 0));
  }

  private getFormForEntry(name: string, value: number) {
    return this.fb.group(
      {
        name: [
          name,
          [
            Validators.required,
            patternValidator(/^[A-Z][a-zA-Z0-9_]*$/, { pattern: true }),
            patternValidator(/^(?!Ind\d+$)/, { reservedName: true }),
            matchParentError('duplicates', this.onDestroy$),
          ],
        ],
        value: [value, Validators.required],
      },
      {
        validators: matchParentError<FormGroup<{ name: FormControl<string> }>>('duplicates', this.onDestroy$, {
          errorFilter: (control, errorValue) => errorValue.includes(control.value.name),
        }),
      },
    );
  }

  private duplicateNameValidator(): ValidatorFn {
    return (control: FormArray<FormGroup<{ name: FormControl<string> }>>) => {
      const names = control.getRawValue().map(value => value.name);
      const duplicateNames = names.filter(name => names.filter(n => n && n === name).length > 1);

      return duplicateNames.length > 0 ? { duplicates: duplicateNames } : null;
    };
  }

  deleteVariable(position: number) {
    this.formGroup.controls.variables.removeAt(position);
    this.formGroup.controls.variables.markAsDirty();
  }
}
