import { Pipe, PipeTransform } from '@angular/core';
import { getIndicatorCoefficientName, getIndicatorValueName } from '@entscheidungsnavi/decision-data';

// TODO: improve unclear description
/**
 * Given the index of an indicator, this pipe returns the name of the variable for the indicator value in the indicator aggregation formula.
 *
 * @example
 * ```
 * index | indicatorVariable:'indicator'
 * ```
 */

@Pipe({ name: 'indicatorVariable' })
export class IndicatorVariableNamePipe implements PipeTransform {
  /**
   * @param index - the index of the indicator in the objective
   * @param type - whether to return the indicator value variable of the indicator coefficient variable
   */
  transform(index: number, type: 'indicator' | 'coefficient'): string {
    return type === 'indicator' ? getIndicatorValueName(index) : getIndicatorCoefficientName(index);
  }
}
