import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Objective } from '@entscheidungsnavi/decision-data';

/**
 * Displays outcome input field/drop-down for Numerical and Verbal Objectives.
 *
 * @example
 * ```html
 *  <dt-numerical-or-verbal-value
 *    [objective]="objective"
 *    [(outcomeValues)]="outcome.values[0][0][0]"
 *  ></dt-numerical-or-verbal-value>
 * ```
 */

@Component({
  selector: 'dt-numerical-or-verbal-value',
  templateUrl: './numerical-or-verbal-value.component.html',
  styleUrls: ['./numerical-or-verbal-value.component.scss'],
})
export class NumericalOrVerbalValueComponent implements OnInit {
  @Input() objective: Objective;

  /**
   * This can be bound from decisionData -\> outcomes[i] -\> values.
   * It can be used with a two-way binding.
   */
  @Input() grayedOutText = false;
  @Input() outcomeValue: number;
  @Output() outcomeValueChange = new EventEmitter<number>();

  min: number;
  max: number;

  ngOnInit() {
    // Initialize formControls. We assume the objective does not change while the component is displayed.
    this.min = this.objective.isNumerical ? Math.min(this.objective.numericalData.from, this.objective.numericalData.to) : 1;
    this.max = this.objective.isNumerical
      ? Math.max(this.objective.numericalData.from, this.objective.numericalData.to)
      : this.objective.verbalData.optionCount();
  }

  onValueChange(newValue: number) {
    this.outcomeValue = newValue;
    this.outcomeValueChange.emit(newValue);
  }
}
