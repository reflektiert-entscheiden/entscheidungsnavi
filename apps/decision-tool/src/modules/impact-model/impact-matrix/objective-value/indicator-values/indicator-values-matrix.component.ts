import { Component, HostBinding, Input, OnChanges, SimpleChanges } from '@angular/core';
import {
  Alternative,
  CompositeUserDefinedInfluenceFactor,
  InfluenceFactor,
  InfluenceFactorState,
  Objective,
  ObjectiveInput,
  PredefinedInfluenceFactor,
} from '@entscheidungsnavi/decision-data';
import { MatDialog } from '@angular/material/dialog';

@Component({
  selector: 'dt-indicator-values-matrix',
  templateUrl: './indicator-values-matrix.component.html',
  styleUrls: ['./indicator-values-matrix.component.scss'],
})
export class IndicatorValuesMatrixComponent implements OnChanges {
  @Input() alternative: Alternative;
  @Input() objective: Objective;
  @Input() outcomeValues: ObjectiveInput[];
  @Input() influenceFactor: InfluenceFactor;

  @HostBinding('style.--indicator-count')
  get visibleAlternativeCount() {
    return this.objective.indicatorData.indicators.length;
  }

  @HostBinding('style.--state-count')
  get objectiveCount() {
    return this.influenceFactor ? this.influenceFactorStates.length : 1;
  }

  transposed = true;

  influenceFactorStates: InfluenceFactorState[];

  constructor(public dialog: MatDialog) {}

  ngOnChanges(changes: SimpleChanges): void {
    if ('influenceFactor' in changes) {
      this.influenceFactorStates = this.influenceFactor?.getStates();
    }
  }

  protected isInfluenceFactorCombined(influenceFactor: InfluenceFactor): influenceFactor is CompositeUserDefinedInfluenceFactor {
    return influenceFactor instanceof CompositeUserDefinedInfluenceFactor;
  }

  isInfluenceFactorPredefined(influenceFactor: InfluenceFactor): influenceFactor is PredefinedInfluenceFactor {
    return influenceFactor instanceof PredefinedInfluenceFactor;
  }
}
