import { ChangeDetectorRef, Component, HostBinding, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import {
  Alternative,
  Indicator,
  InfluenceFactor,
  InfluenceFactorState,
  Objective,
  ObjectiveInput,
  calculateIndicatorValue,
} from '@entscheidungsnavi/decision-data';
import { Interval } from '@entscheidungsnavi/tools';
import { MatDialog } from '@angular/material/dialog';
import { VerbalIndicatorForecastModalComponent, VerbalIndicatorForecastModalData } from '../../../forecast-of-outcomes-modal';
@Component({
  selector: 'dt-indicator-matrix',
  templateUrl: './indicator-matrix.component.html',
  styleUrls: ['./indicator-matrix.component.scss'],
})
export class IndicatorMatrixComponent implements OnChanges, OnInit {
  @Input() alternative: Alternative;
  @Input() objective: Objective;
  @Input() indicator: Indicator;
  @Input() influenceFactor: InfluenceFactor;

  @Input() outcomeValues: ObjectiveInput[]; //[stateIdx][indicatorIdx][categoryIdx]
  @Input() indicatorIdx: number;

  @HostBinding('style.--category-count')
  get categoryCount() {
    return this.numberOfCategories;
  }

  @HostBinding('class.transposed')
  @Input()
  transposed: boolean;

  categoryIdx = 0;

  minimized: boolean[] = [false];
  efDefined: boolean[] = [false];
  valueWithoutEF: number[] = [null];
  numberOfCategories: number;

  influenceFactorStates: InfluenceFactorState[];

  sliderTextAbbreviation = $localize`EF`;

  constructor(
    private dialog: MatDialog,
    private cdRef: ChangeDetectorRef,
  ) {}

  ngOnInit() {
    this.initializeValueWithoutEF();
  }

  initializeValueWithoutEF() {
    this.influenceFactorStates = this.influenceFactor?.getStates();
    this.numberOfCategories = Math.max(this.indicator.verbalIndicatorCategories.length, 1);
    //array of length numberOfCategories
    this.minimized = Array(this.numberOfCategories).fill(false);
    this.efDefined = Array(this.numberOfCategories).fill(false);
    this.valueWithoutEF = Array(this.numberOfCategories).fill(null);
    for (let i = 0; i < this.numberOfCategories; i++) {
      const array = this.outcomeValues.map(state => state[this.indicatorIdx][i]);
      //all values are the same
      if (array.every((val, i, arr) => val === arr[0])) {
        this.valueWithoutEF[i] = array[0];
        this.efDefined[i] = false;
      } else {
        this.efDefined[i] = true;
      }
      this.minimized[i] = false;
    }
  }

  ngOnChanges(changes: SimpleChanges) {
    if ('influenceFactor' in changes) {
      this.initializeValueWithoutEF();
      this.cdRef.detectChanges();
    }
  }

  handleToggleChange(val: boolean, categoryIdx: number) {
    if (!val) {
      const array = this.outcomeValues.map(state => state[this.indicatorIdx][categoryIdx]);
      //all values are the same
      if (array.every((val, i, arr) => val === arr[0])) {
        this.valueWithoutEF[categoryIdx] = array[0];
      }
    } else {
      if (this.valueWithoutEF[categoryIdx] != undefined) {
        this.outcomeValues.forEach(state => {
          state[this.indicatorIdx][categoryIdx] = this.valueWithoutEF[categoryIdx];
        });
      }
    }
    this.cdRef.detectChanges();
  }

  protected openVerbalIndicatorForecastModal(indicatorIdx: number, stateIdx = 0) {
    this.dialog
      .open<VerbalIndicatorForecastModalComponent, VerbalIndicatorForecastModalData>(VerbalIndicatorForecastModalComponent, {
        data: {
          indicatorIdx,
          alternative: this.alternative,
          objective: this.objective,
          influenceFactor: this.influenceFactor,
          outcomeValues: this.outcomeValues,
          stateIdx,
        },
      })
      .afterClosed()
      .subscribe((data: { values: ObjectiveInput[] }) => {
        if (data != null) {
          this.outcomeValues = data.values;
          this.numberOfCategories = Math.max(this.indicator.verbalIndicatorCategories.length, 1);
          for (let i = 0; i < this.numberOfCategories; i++) {
            const array = this.outcomeValues.map(state => state[this.indicatorIdx][i]);
            //all values are the same
            if (array.every((val, i, arr) => val === arr[0])) {
              this.valueWithoutEF[i] = array[0];
              this.efDefined[i] = false;
            } else {
              this.efDefined[i] = true;
            }
          }
        }
      });
  }

  protected handleValueChange(newValue: number, stateIdx?: number, categoryIdx?: number) {
    if (stateIdx == undefined) {
      if (this.influenceFactor) {
        this.outcomeValues.forEach(state => {
          state[this.indicatorIdx][categoryIdx || 0] = newValue;
        });
        this.valueWithoutEF[categoryIdx] = newValue;
      } else {
        this.outcomeValues[0][this.indicatorIdx][categoryIdx] = newValue;
        this.valueWithoutEF[categoryIdx] = newValue;
      }
    } else {
      this.outcomeValues[stateIdx][this.indicatorIdx][categoryIdx || 0] = newValue;
      this.valueWithoutEF[categoryIdx] = null;
    }
    this.cdRef.detectChanges();
  }

  getColor(stateIdx?: number, categoryIdx?: number) {
    if (
      this.influenceFactor &&
      ((!this.efDefined[categoryIdx] && this.valueWithoutEF[categoryIdx] == undefined) ||
        (this.efDefined[categoryIdx] && this.outcomeValues[stateIdx][this.indicatorIdx][categoryIdx || 0] == undefined))
    ) {
      return `#f5f5f5`;
    }

    if (!this.influenceFactor && this.outcomeValues[0][this.indicatorIdx][categoryIdx || 0] == null) {
      return `#f5f5f5`;
    }

    const interval = new Interval(this.indicator.min, this.indicator.max);
    const value = stateIdx == undefined ? this.calculateValueWithoutEF(categoryIdx) : this.calculateValue(stateIdx, categoryIdx);

    if (value < Math.min(interval.start, interval.end) || value > Math.max(interval.start, interval.end)) {
      return `#f5f5f5`;
    }

    const percentage = interval.normalizePoint(value);
    const alpha = Math.abs(percentage - 0.5) / 2.5;
    const color = percentage < 0.5 ? '255,0,0' : '0,255,0';
    return `linear-gradient(rgba(${color},${alpha}), rgba(${color},${alpha})),white`;
  }

  calculateValue(stateIdx: number, categoryIdx?: number) {
    if (categoryIdx == undefined) {
      return calculateIndicatorValue(this.outcomeValues[stateIdx][this.indicatorIdx], this.indicator);
    } else {
      const values = this.outcomeValues[stateIdx][this.indicatorIdx];
      for (let j = 0; j < this.indicator.verbalIndicatorCategories.length; j++) {
        if (j != categoryIdx) {
          if (values[j] == undefined) {
            values[j] = (this.indicator.verbalIndicatorCategories[j].stages.length - 1) / 2;
          }
        }
      }
      return calculateIndicatorValue(values, this.indicator);
    }
  }

  calculateValueWithoutEF(categoryIdx?: number) {
    let avgValue = 0;
    if (categoryIdx == undefined) {
      for (let i = 0; i < this.outcomeValues.length; i++) {
        avgValue += calculateIndicatorValue(this.outcomeValues[i][this.indicatorIdx], this.indicator);
      }
      avgValue /= this.outcomeValues.length;
      return avgValue;
    } else {
      for (let i = 0; i < this.outcomeValues.length; i++) {
        const vals = [...this.outcomeValues[i][this.indicatorIdx]];
        for (let j = 0; j < this.indicator.verbalIndicatorCategories.length; j++) {
          if (j != categoryIdx) {
            if (vals[j] == undefined) {
              //set with average to not distort the displayed color
              vals[j] = (this.indicator.verbalIndicatorCategories[j].stages.length - 1) / 2;
            }
          }
        }
        avgValue += calculateIndicatorValue(vals, this.indicator);
      }
      avgValue /= this.outcomeValues.length;
      return avgValue;
    }
  }
}
