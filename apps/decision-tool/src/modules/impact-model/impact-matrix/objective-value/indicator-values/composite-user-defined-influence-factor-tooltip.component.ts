import { Component, Input } from '@angular/core';
import { CompositeUserDefinedInfluenceFactor, InfluenceFactor } from '@entscheidungsnavi/decision-data';

@Component({
  selector: 'dt-composite-user-defined-influence-factor-tooltip',
  template: ` @if (isInfluenceFactorCombined(influenceFactor)) {
    <div class="scenario-tooltip">
      @for (stateWithProbability of influenceFactor.getCompositeStateInfos(stateIdx); let idx = $index; track idx) {
        <span class="scenario-tooltip-if-name" [title]="stateWithProbability.influenceFactorName"
          >{{ stateWithProbability.influenceFactorName }}:</span
        >
        <div class="scenario-info">
          <span class="scenario-tooltip-state" [title]="stateWithProbability.stateName">{{ stateWithProbability.stateName }}</span>
          <span class="scenario-tooltip-probability">({{ stateWithProbability.probability / 100 | percent }})</span>
        </div>
      }
    </div>
  }`,
  styles: [
    `
      @use 'variables' as dt;

      .scenario-tooltip {
        display: grid;
        max-width: 400px;
        grid-template-columns: auto auto;
        background-color: white;
        border-radius: 4px;
        border: 1px solid dt.$blue;
        padding: 10px 15px;
        gap: 5px 10px;

        .scenario-tooltip-if-name {
          font-weight: 500;
          overflow: hidden;
          text-overflow: ellipsis;
          white-space: nowrap;
        }

        .scenario-info {
          display: flex;
          overflow: hidden;
          gap: 10px;

          .scenario-tooltip-state {
            overflow: hidden;
            text-overflow: ellipsis;
            white-space: nowrap;
          }

          .scenario-tooltip-probability {
            min-width: 52px;
            font-style: italic;
          }
        }
      }
    `,
  ],
})
export class CompositeUserDefinedInfluenceFactorTooltipComponent {
  @Input() influenceFactor: InfluenceFactor;
  @Input() stateIdx: number;

  isInfluenceFactorCombined(influenceFactor: InfluenceFactor): influenceFactor is CompositeUserDefinedInfluenceFactor {
    return influenceFactor instanceof CompositeUserDefinedInfluenceFactor;
  }
}
