import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  EventEmitter,
  HostBinding,
  Input,
  OnChanges,
  Output,
  TemplateRef,
  ViewChild,
} from '@angular/core';
import { DomSanitizer, SafeStyle } from '@angular/platform-browser';
import { Alternative, DecisionData, Objective, Outcome, PredefinedInfluenceFactor } from '@entscheidungsnavi/decision-data';
import { validateObjectiveScales, validateValue } from '@entscheidungsnavi/decision-data/validations';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { NoteBtnComponent, PopOverService } from '@entscheidungsnavi/widgets';
import { takeUntil } from 'rxjs';
import { MatSelect } from '@angular/material/select';
import { takeUntilDestroyed } from '@angular/core/rxjs-interop';
import { ForecastOfOutcomesModalComponent, ForecastOfOutcomesModalData } from './forecast-of-outcomes-modal';

export type Colorization =
  | { mode: 'off' }
  | { mode: 'absolute' }
  | { mode: 'relative'; referenceOutcomePercentage: number }
  | { mode: 'pro-contra'; objectiveRange: { min: number; max: number } };

@Component({
  selector: 'dt-impact-matrix-field',
  templateUrl: './matrix-field.component.html',
  styleUrls: ['matrix-field.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ImpactMatrixFieldComponent implements OnChanges {
  @Input({ required: true }) outcome: Outcome;
  @Input({ required: true }) objective: Objective;
  @Input({ required: true }) alternative: Alternative;

  @Input({ required: true }) colorization: Colorization;

  @Input() readonly = false;

  @Output() matrixFieldChange = new EventEmitter<void>();
  @Output() isValueValidChange = new EventEmitter<boolean>();
  @Output() influenceFactorModified = new EventEmitter<number>(); // Emits the ID of the modified influence factor

  @HostBinding('attr.data-cy')
  validationStatus = 'invalid';

  // Data specific to numerical scales
  numericalScaleMin: number;
  numericalScaleMax: number;

  // True iff the current values are valid and the outcome is processed
  isValueValid: boolean;
  hasTornadoDiagram: boolean;

  // Smallest/biggest value in outcome.values (indicator values are rounded to 2 decimal places)
  min: number | string;
  max: number | string;

  backgroundImage: SafeStyle;

  @ViewChild(NoteBtnComponent) noteBtn: NoteBtnComponent;
  @ViewChild(MatSelect) verbalInput: MatSelect;
  @ViewChild('readonlyDisplay') readonlyDisplay: ElementRef<HTMLElement>;
  @ViewChild('numericalInputPopover') numericalInputPopover: TemplateRef<unknown>;

  get verbalOutcomeValue() {
    return this.outcome.values[0][0][0] !== undefined ? this.objective.verbalData.options[this.outcome.values[0][0][0] - 1] : null;
  }

  constructor(
    protected decisionData: DecisionData,
    private domSanitizer: DomSanitizer,
    private cdRef: ChangeDetectorRef,
    private matDialog: MatDialog,
    protected snackBar: MatSnackBar,
    protected popOverService: PopOverService,
  ) {
    this.isValueValidChange.pipe(takeUntilDestroyed()).subscribe(isValid => (this.validationStatus = isValid ? 'valid' : 'invalid'));
  }

  ngOnChanges() {
    this.update();
  }

  onChange() {
    this.update();
    this.matrixFieldChange.emit();
  }

  openForecastModal(selectedTabIndex = 0) {
    const dialogRef = this.matDialog.open<ForecastOfOutcomesModalComponent, ForecastOfOutcomesModalData>(ForecastOfOutcomesModalComponent, {
      data: {
        selectedTabIndex,
        alternative: this.alternative,
        objective: this.objective,
        outcome: this.outcome,
      },
    });
    dialogRef.componentInstance.influenceFactorModified.pipe(takeUntil(dialogRef.afterClosed())).subscribe(influenceFactorId => {
      this.influenceFactorModified.emit(influenceFactorId);
    });
    dialogRef.afterClosed().subscribe(() => {
      this.onChange();
    });
  }

  update() {
    this.outcome.checkProcessed();

    const isValueValid =
      this.outcome.processed &&
      validateObjectiveScales([this.objective])[0] &&
      this.outcome.values.every(value => validateValue(value, this.objective)[0]);

    if (isValueValid !== this.isValueValid) {
      this.isValueValid = isValueValid;
      this.isValueValidChange.emit(isValueValid);
    }

    this.hasTornadoDiagram =
      this.isValueValid && this.objective.isIndicator && this.outcome.influenceFactor instanceof PredefinedInfluenceFactor;
    this.hasTornadoDiagram = this.isValueValid && this.objective.isIndicator && this.outcome.includesNonTrivialUncertainty(this.objective);

    this.numericalScaleMin = Math.min(this.objective.numericalData.from, this.objective.numericalData.to);
    this.numericalScaleMax = Math.max(this.objective.numericalData.from, this.objective.numericalData.to);

    if (this.objective.isNumerical || this.objective.isVerbal) {
      const min = Math.min(...this.outcome.values.map(value => value[0][0]));
      const max = Math.max(...this.outcome.values.map(value => value[0][0]));

      if (this.objective.isVerbal) {
        this.min = this.objective.verbalData.options[min - 1];
        this.max = this.objective.verbalData.options[max - 1];
      } else {
        this.min = min;
        this.max = max;
      }
    } else {
      this.min = this.indicatorOutcome('min');
      this.max = this.indicatorOutcome('max');
    }

    this.calcBackgroundImage();
    this.cdRef.markForCheck();
  }

  private indicatorOutcome(get: 'min' | 'max'): number {
    const getMin = get === 'min';
    // We calculate the outcome values for all influence factor states
    const aggregationFunction = this.objective.indicatorData.aggregationFunction;
    const aggregatedValues = this.outcome.values.map(value => aggregationFunction(value));
    // Return the numerically lowest/highest outcome value
    return getMin ? Math.min(...aggregatedValues) : Math.max(...aggregatedValues);
  }

  private calcBackgroundImage() {
    if (
      !this.isValueValid ||
      this.colorization.mode === 'off' ||
      (this.colorization.mode === 'relative' && this.colorization.referenceOutcomePercentage == null)
    ) {
      this.backgroundImage = undefined;
      return;
    }

    if (this.colorization.mode === 'relative') {
      const myExpectedPercentage = this.outcome.getExpectedValuePercentage(this.outcome.getValuePercentages(this.objective));

      // Adjust such that percentage mirrors the absolute deviation from the reference.
      // e.g. 0 iff we are 0 and ref is 1, and 1 iff we are 1 and ref is 0.
      const adjustedPercentage = (myExpectedPercentage - this.colorization.referenceOutcomePercentage + 1) / 2;

      const staticColor = this.getColorForPercentage(adjustedPercentage);
      this.backgroundImage = this.domSanitizer.bypassSecurityTrustStyle(`linear-gradient(to right, ${staticColor}, ${staticColor})`);
    } else {
      let valuePercentages = this.outcome.getValuePercentages(this.objective);
      let expectedValuePercentage = this.outcome.getExpectedValuePercentage(valuePercentages);

      if (this.colorization.mode === 'pro-contra') {
        // Adjust the values to the range of values defined in the outcomes
        const range = this.colorization.objectiveRange;
        valuePercentages = valuePercentages.map(p => (p - range.min) / (range.max - range.min));
        expectedValuePercentage = (expectedValuePercentage - range.min) / (range.max - range.min);
      }

      const percentages = [Math.min(...valuePercentages), expectedValuePercentage, Math.max(...valuePercentages)];

      const colors = percentages.map(p => this.getColorForPercentage(p));

      let middlePercentage = (Math.abs(percentages[1] - percentages[0]) / Math.abs(percentages[0] - percentages[2])) * 100;

      if (Number.isNaN(middlePercentage)) {
        middlePercentage = 50;
      }

      // Scale
      middlePercentage = 8 + 0.84 * middlePercentage;

      this.backgroundImage = this.domSanitizer.bypassSecurityTrustStyle(
        `linear-gradient(to right, ${colors[0]}, ${colors[1]} ${100 - middlePercentage}%, ${colors[2]})`,
      );
    }
  }

  private getColorForPercentage(percentage: number) {
    if (percentage < 0.5) {
      const alpha = (0.5 - percentage) / 2.5;
      return `rgba(255,0,0,${alpha})`;
    } else {
      const alpha = (percentage - 0.5) / 2.5;
      return `rgba(0,255,0,${alpha})`;
    }
  }

  onClick() {
    if (this.readonly) {
      return;
    }

    if (this.objective.isVerbal && !this.outcome.influenceFactor) {
      this.verbalInput.open();
    } else if (this.objective.isNumerical && !this.outcome.influenceFactor) {
      this.popOverService.open(this.numericalInputPopover, this.readonlyDisplay, {
        position: [{ originX: 'center', originY: 'center', overlayX: 'center', overlayY: 'center' }],
      });
    } else {
      this.openForecastModal();
    }
  }

  openNote() {
    this.noteBtn.open();
    // Otherwise, the note btn does not work with our OnPush strategy
    this.cdRef.markForCheck();
  }

  isFiniteNumber(value: number | string): boolean {
    return Number.isFinite(value);
  }
}
