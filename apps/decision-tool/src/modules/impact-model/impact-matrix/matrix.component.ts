import {
  AfterViewInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  HostBinding,
  Input,
  NgZone,
  OnInit,
  QueryList,
  ViewChild,
  ViewChildren,
} from '@angular/core';
import {
  Alternative,
  calculateDominanceRelations,
  DecisionData,
  DominanceRelation,
  InfluenceFactor,
  Objective,
  Outcome,
  PREDEFINED_INFLUENCE_FACTORS,
} from '@entscheidungsnavi/decision-data';
import { takeUntilDestroyed } from '@angular/core/rxjs-interop';
import { validateValue } from '@entscheidungsnavi/decision-data/validations';
import { MatDialog } from '@angular/material/dialog';
import { FullscreenRef, FullscreenService, NoteBtnComponent, PlatformDetectService, PopOverService } from '@entscheidungsnavi/widgets';
import { lastValueFrom, map, pairwise, startWith } from 'rxjs';
import { delay, filter } from 'rxjs/operators';
import { ActivatedRoute, Router } from '@angular/router';
import { MatSnackBar } from '@angular/material/snack-bar';
import { CdkDragSortEvent } from '@angular/cdk/drag-drop';
import { moveInArray } from '@entscheidungsnavi/tools';
import { PersistentSetting, PersistentSettingParent } from '@entscheidungsnavi/widgets';
import { ObjectiveScaleModalComponent } from '../objective-scale';
import { ExplanationService } from '../../shared/decision-quality';
import { ExportImpactModelModalComponent } from '../../../app/excel-impact-model-export/export-modal/export-impact-model-modal.component';
import { DecisionDataState } from '../../shared/decision-data-state';
import { AppSettingsService } from '../../../app/data/app-settings.service';
import { Colorization, ImpactMatrixFieldComponent } from './matrix-field.component';
import { CopyScaleModalComponent } from './copy-scale-modal/copy-scale-modal.component';
import { NameFieldComponent } from './name-field/name-field.component';

type DragData = { objective: number; alternative: number };

@Component({
  selector: 'dt-impact-matrix',
  templateUrl: 'matrix.component.html',
  styleUrls: ['matrix.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
@PersistentSettingParent('ImpactMatrixComponent')
export class ImpactMatrixComponent implements OnInit, AfterViewInit {
  @Input()
  @HostBinding('class.extendable')
  extendable = false;

  @Input()
  settingsContainerPadding: string;

  @PersistentSetting()
  dominanceMode: 'off' | 'highlight' | 'filter' = 'highlight';

  @PersistentSetting()
  colorMode: 'off' | 'absolute' | 'relative' | 'pro-contra' = 'absolute';

  @HostBinding('class.compact')
  @PersistentSetting()
  smallBoxHeight = true;

  @HostBinding('class.transposed')
  @PersistentSetting()
  transposed = false;

  isInSortingMode = false;

  highlightInfluenceFactorId = -1; // -1 means no highlighting

  // Used for dynamic coloring of the matrix
  colorizationPerObjective: Colorization[];
  colorizationReferenceAlternativeIdx: number;

  @ViewChildren(ImpactMatrixFieldComponent) matrixFields: QueryList<ImpactMatrixFieldComponent>;

  // Contains dominated alternatives as keys and the corresponding set of DominanceRelations as value
  dominatedAlternatives = new Map<Alternative, DominanceRelation[]>();

  // We highlight the dominance relations for a specific dominated alternative
  highlightedDominatedAlternative: Alternative;
  // We look at the highlightedDominatedAlternative: For each alternative that dominates
  // it denotes in what type of dominance is present (with or without indicator)
  highlightedDominances: Alternative[] = [];

  dragData: DragData;
  fieldDragCounter = new Map<ImpactMatrixFieldComponent, number>(); // Used to determine when we remove outline class
  dragConfirmation: { from: DragData; to: DragData; toElement: ImpactMatrixFieldComponent }; // used for confirmation modal

  influenceFactorOptions: InfluenceFactor[];

  isEverythingValid = false;

  fullscreenRef: FullscreenRef | null = null;

  @ViewChild('stepToExplainMenu', { static: true }) stepToExplainMenu: ElementRef;

  highlightObjectives = false;
  highlightAlternatives = false;
  highlightCells = false;

  dragAndDropOrientation: 'horizontal' | 'vertical' = 'horizontal';
  dragging = false;

  objectiveOrderOverride: number[] = null;
  alternativeOrderOverride: number[] = null;

  readonly transposedTooltip = $localize`Ziele in der Kopfzeile anzeigen`;
  readonly notTransposedTooltip = $localize`Alternativen in der Kopfzeile anzeigen`;
  readonly compactScalingTooltip = $localize`Ändere Skalierung zu normal`;
  readonly normalScalingTooltip = $localize`Ändere Skalierung zu kompakt`;
  readonly sortingOnTooltip = $localize`Sortiermodus ausschalten`;
  readonly sortingOffTooltip = $localize`Sortiermodus aktivieren`;

  @ViewChild('matrixContainer', { static: true })
  matrixContainerRef: ElementRef<HTMLElement>;

  @ViewChildren('objectiveNameField') objectiveNameInputRefs: QueryList<NameFieldComponent>;
  @ViewChildren('alternativeNameField') alternativeNameInputRefs: QueryList<NameFieldComponent>;

  // True iff there exists at least one indicator objective
  get hasIndicatorObjective() {
    return this.decisionData.objectives.some(obj => obj.isIndicator);
  }

  get highlightDominance() {
    return this.isEverythingValid && this.dominanceMode === 'highlight';
  }

  @HostBinding('style.--alternative-count')
  get visibleAlternativeCount() {
    return this.dominanceMode === 'filter' && this.isEverythingValid
      ? this.decisionData.alternatives.length - this.dominatedAlternatives.size
      : this.decisionData.alternatives.length;
  }

  @HostBinding('style.--objective-count')
  get objectiveCount() {
    return this.decisionData.objectives.length;
  }

  get readonly() {
    return this.decisionDataState.isProjectReadonly;
  }

  get objectiveOrientation() {
    return this.transposed ? 'vertical' : 'horizontal';
  }

  get alternativeOrientation() {
    return this.transposed ? 'horizontal' : 'vertical';
  }

  constructor(
    protected decisionData: DecisionData,
    public cdRef: ChangeDetectorRef,
    private dialog: MatDialog,
    private explanationService: ExplanationService,
    private myElementRef: ElementRef<HTMLElement>,
    private fullscreenService: FullscreenService,
    private zone: NgZone,
    private route: ActivatedRoute,
    private router: Router,
    protected popOverService: PopOverService,
    protected snackBar: MatSnackBar,
    protected platformDetectService: PlatformDetectService,
    protected appSettings: AppSettingsService,
    private decisionDataState: DecisionDataState,
  ) {
    this.influenceFactorOptions = [...this.decisionData.influenceFactors, ...Object.values(PREDEFINED_INFLUENCE_FACTORS)];

    this.route.queryParams.pipe(takeUntilDestroyed()).subscribe(params => {
      this.highlightInfluenceFactorId = 'highlightInfluenceFactor' in params ? +params.highlightInfluenceFactor : -1;
      this.cdRef.markForCheck();
    });

    this.decisionData.alternativeAdded$.pipe(takeUntilDestroyed()).subscribe(() => {
      this.onChange();
    });
    this.decisionData.objectiveAdded$.pipe(takeUntilDestroyed()).subscribe(() => {
      this.onChange();
    });
    this.decisionData.globalVariables.variablesChanged$.pipe(takeUntilDestroyed()).subscribe(() => this.onMeasuringScaleChange());
  }

  ngOnInit() {
    this.resetColorization();
    this.onChange();
  }

  onMeasuringScaleChange() {
    this.matrixFields.forEach(field => field.update());
    this.onChange();
  }

  changeHighlightedInfluenceFactor(newInfluenceFactorId: number) {
    const queryParams = newInfluenceFactorId >= 0 ? { highlightInfluenceFactor: newInfluenceFactorId } : {};
    this.router.navigate([], { relativeTo: this.route, queryParams, replaceUrl: true });
  }

  explainImpactModel() {
    this.explanationService.openExplanationModalFor(
      'CLEAR_VALUES',
      this.decisionData.decisionQuality.criteriaValues['CLEAR_VALUES'],
      true,
      false,
    );
  }

  toggleFullScreen() {
    if (this.fullscreenRef) {
      this.fullscreenRef.close();
    } else {
      this.fullscreenRef = this.fullscreenService.fullscreenElement(this.myElementRef);
      this.cdRef.detectChanges();

      this.fullscreenRef.onClose.subscribe(() => {
        this.fullscreenRef = null;
        this.cdRef.detectChanges();
      });
    }
  }

  onChange() {
    this.isEverythingValid = this.decisionData.validateOutcomes()[0];
    this.influenceFactorOptions = [...this.decisionData.influenceFactors, ...Object.values(PREDEFINED_INFLUENCE_FACTORS)];

    if (this.dominanceMode !== 'off' && this.isEverythingValid) {
      // On data change we need to rerun the dominance check
      this.performDominanceCheck();
    }

    if (
      this.highlightDominance &&
      this.highlightedDominatedAlternative != null &&
      this.dominatedAlternatives.has(this.highlightedDominatedAlternative)
    ) {
      this.calculateDominanceDetails();
    } else {
      this.hideDominanceDetails();
    }

    this.resetColorization();

    this.cdRef.markForCheck();
  }

  private performDominanceCheck() {
    this.dominatedAlternatives.clear();
    calculateDominanceRelations(this.decisionData.objectives, this.decisionData.alternatives, this.decisionData.outcomes).forEach(
      relation => {
        if (this.dominatedAlternatives.has(relation.dominated)) {
          this.dominatedAlternatives.get(relation.dominated).push(relation);
        } else {
          this.dominatedAlternatives.set(relation.dominated, [relation]);
        }
      },
    );
  }

  addAlternative() {
    this.decisionData.addAlternative({
      alternative: new Alternative(0, $localize`Alternative` + ' ' + (this.decisionData.alternatives.length + 1)),
    });
  }

  copyAlternativeWithOutcomes(alternative: Alternative, alternativeIdx: number) {
    const copiedAlternative = alternative.clone();
    this.decisionData.addAlternative({ alternative: copiedAlternative, position: alternativeIdx + 1 });

    this.decisionData.outcomes[alternativeIdx].forEach((outcome: Outcome, objectiveIdx: number) => {
      // set the copied outcome to the same values as the original alternative
      this.decisionData.outcomes[alternativeIdx + 1][objectiveIdx] = outcome.clone();
    });

    this.onChange();
    this.cdRef.detectChanges();
    this.alternativeNameInputRefs.toArray()[alternativeIdx + 1].focus(true);
  }

  explainAlternativesSteps() {
    this.explanationService.openExplanationModalFor(
      'CREATIVE_DOABLE_ALTERNATIVES',
      this.decisionData.decisionQuality.criteriaValues['CREATIVE_DOABLE_ALTERNATIVES'],
      true,
      false,
    );
  }

  showDominanceDetails(alternativeIndex: number) {
    this.changeHighlightedInfluenceFactor(-1);
    this.highlightedDominatedAlternative = this.decisionData.alternatives[alternativeIndex];
    this.calculateDominanceDetails();
  }

  hideDominanceDetails() {
    this.highlightedDominatedAlternative = null;
    this.highlightedDominances = [];
  }

  addObjective() {
    this.decisionData.addObjective(new Objective($localize`Ziel` + ' ' + (this.decisionData.objectives.length + 1)));
  }

  explainObjectivesStep() {
    this.explanationService.openExplanationModalFor(
      'MEANINGFUL_RELIABLE_INFORMATION',
      this.decisionData.decisionQuality.criteriaValues['MEANINGFUL_RELIABLE_INFORMATION'],
      true,
      false,
    );
  }

  dragField(event: DragEvent, objective: number, alternative: number) {
    if (this.readonly) {
      return;
    }
    this.dragData = { objective, alternative };
    event.dataTransfer.effectAllowed = 'copy';

    // Set something for Firefox and Safari
    event.dataTransfer.setData('text', 'dummy');
  }

  dragEnterField(event: DragEvent, field: ImpactMatrixFieldComponent, objective: number, alternative: number) {
    if (this.dragData && this.dragData.objective === objective && this.dragData.alternative !== alternative) {
      event.dataTransfer.dropEffect = 'copy';
      event.preventDefault();

      this.fieldDragCounter.set(field, (this.fieldDragCounter.get(field) ?? 0) + 1);
    }
  }

  dragOverField(event: DragEvent, objective: number, alternative: number) {
    if (this.dragData && this.dragData.objective === objective && this.dragData.alternative !== alternative) {
      const htmlElement = event.currentTarget as HTMLElement;
      htmlElement.classList.add('impact-drag-over');

      event.dataTransfer.dropEffect = 'copy';
      event.preventDefault();
    }
  }

  dropOnField(element: ImpactMatrixFieldComponent, event: DragEvent, objective: number, alternative: number) {
    const htmlElement = event.currentTarget as HTMLElement;
    htmlElement.classList.remove('impact-drag-over');

    if (this.dragData && this.dragData.objective === objective && this.dragData.alternative !== alternative) {
      const targetOutcome = this.decisionData.outcomes[alternative][objective];
      const doesOutcomeRequireConfirmation =
        targetOutcome.processed && targetOutcome.values.every(value => validateValue(value, this.decisionData.objectives[objective])[0]);

      if (!this.appSettings.overwriteOutcomeConfirmation || !doesOutcomeRequireConfirmation) {
        this.confirmDragDrop(this.dragData, { objective, alternative }, element);
      } else {
        this.dragConfirmation = {
          from: this.dragData,
          to: { objective, alternative },
          toElement: element,
        };
        this.cdRef.detectChanges(); // Fixes ExpressionChanged Error related to ng-untouched
      }
    }
  }

  dragLeaveField(event: DragEvent, field: ImpactMatrixFieldComponent, objective: number, alternative: number) {
    if (this.dragData && this.dragData.objective === objective && this.dragData.alternative !== alternative) {
      this.fieldDragCounter.set(field, (this.fieldDragCounter.get(field) ?? 0) - 1);

      if (this.fieldDragCounter.get(field) === 0) {
        const htmlElement = event.currentTarget as HTMLElement;
        htmlElement.classList.remove('impact-drag-over');
      }
    }
  }

  clearDragData() {
    this.dragData = undefined;
    this.fieldDragCounter.clear();
  }

  confirmDragDrop(from: DragData, to: DragData, toElement: ImpactMatrixFieldComponent) {
    this.dragConfirmation = undefined;
    const originOutcome = this.decisionData.outcomes[from.alternative][from.objective];
    const targetOutcome = this.decisionData.outcomes[to.alternative][to.objective];

    const targetComment = targetOutcome.comment;

    targetOutcome.copyBack(originOutcome);
    targetOutcome.comment = targetComment;

    toElement.update();
    this.onChange();
  }

  private calculateDominanceDetails() {
    this.highlightedDominances = [];
    this.dominatedAlternatives.get(this.highlightedDominatedAlternative).forEach(dominanceRelation => {
      this.highlightedDominances.push(dominanceRelation.dominating);
    });
  }

  isDominating(alternative: Alternative) {
    return this.highlightedDominances.includes(alternative);
  }

  isDominated(alternative: Alternative) {
    return (
      this.highlightDominance &&
      ((this.dominatedAlternatives.has(alternative) && this.highlightedDominatedAlternative == null) ||
        this.highlightedDominatedAlternative === alternative)
    );
  }

  isAlternativeFadedOut(alternativeIndex: number) {
    const alternative = this.decisionData.alternatives[alternativeIndex];

    // We are currently highlighting a dominance relationship, but this alternative is not part of it (neither dominating nor dominated)
    const isIrrelevantForHighlightedDominance =
      this.highlightedDominatedAlternative != null &&
      this.highlightedDominatedAlternative !== alternative &&
      !this.isDominating(alternative);

    return isIrrelevantForHighlightedDominance;
  }

  isOutcomeFadedOut(alternativeIndex: number, objectiveIndex: number) {
    // We are dragging a different objective
    const draggingDifferentObjective = this.dragData && this.dragData.objective !== objectiveIndex;

    // We are highlighting an influence factor that is not used in this outcome
    const irrelevantForHighlightedInfluenceFactor =
      this.highlightInfluenceFactorId !== -1 &&
      this.decisionData.outcomes[alternativeIndex][objectiveIndex].influenceFactor?.id !=
        this.influenceFactorOptions[this.highlightInfluenceFactorId].id;

    return this.isAlternativeFadedOut(alternativeIndex) || draggingDifferentObjective || irrelevantForHighlightedInfluenceFactor;
  }

  isOutcomeHighlighted(alternativeIndex: number, objectiveIndex: number) {
    return (
      this.highlightInfluenceFactorId !== -1 &&
      this.decisionData.outcomes[alternativeIndex][objectiveIndex].influenceFactor?.id ==
        this.influenceFactorOptions[this.highlightInfluenceFactorId].id
    );
  }

  // Lazy Loading Cells
  ngAfterViewInit() {
    const focusLastIfExtended = (queryList: QueryList<{ focus: (setCursor: boolean) => void }>) => {
      queryList.changes
        .pipe(
          map(() => queryList.length),
          startWith(queryList.length),
          pairwise(),
          filter(([lengthBefore, lengthAfter]) => lengthBefore < lengthAfter),
          delay(0), // Required to skip to the next cycle when the element is properly rendered
        )
        .subscribe(() => {
          queryList.last.focus(true);
        });
    };

    focusLastIfExtended(this.objectiveNameInputRefs);
    focusLastIfExtended(this.alternativeNameInputRefs);
  }

  onInfluenceFactorModified(influenceFactorId: number) {
    this.matrixFields.forEach(field => {
      if (field.outcome.influenceFactor?.id === influenceFactorId) field.update();
    });
  }

  excelExport() {
    this.dialog
      .open(ExportImpactModelModalComponent, { data: { readonly: this.readonly } })
      .afterClosed()
      .subscribe(() => {
        this.matrixFields.forEach(field => field.update());
      });
  }

  dominatedAlternativeClicked(alternativeIdx: number) {
    if (this.highlightedDominatedAlternative) {
      this.hideDominanceDetails();
    } else {
      this.showDominanceDetails(alternativeIdx);
    }
  }

  async deleteAlternative(alternativeBox: HTMLElement, alternativeIdx: number) {
    if (this.decisionData.alternatives.length <= 2) return;

    if (
      await this.popOverService.confirm(alternativeBox, $localize`Alternative wirklich löschen?`, {
        position: [{ originX: 'center', originY: 'center', overlayX: 'center', overlayY: 'center' }],
      })
    ) {
      this.decisionData.removeAlternative(alternativeIdx);
      this.onChange();
    }
  }

  async openScaleEditor(objectiveIdx: number) {
    if (await lastValueFrom(this.dialog.open(ObjectiveScaleModalComponent, { data: { objectiveIdx } }).afterClosed())) {
      this.onMeasuringScaleChange();
    }
  }

  openCopyScale(objectiveIdx: number) {
    this.dialog
      .open(CopyScaleModalComponent, {
        data: {
          objectiveIdx: objectiveIdx,
        },
      })
      .afterClosed()
      .subscribe((copied: boolean) => {
        if (copied) {
          this.onMeasuringScaleChange();
        }
      });
  }

  async deleteObjective(objectiveBox: HTMLElement, objectiveIdx: number) {
    if (this.decisionData.objectives.length <= 1) return;

    if (
      await this.popOverService.confirm(objectiveBox, $localize`Ziel wirklich löschen?`, {
        position: [{ originX: 'center', originY: 'center', overlayX: 'center', overlayY: 'center' }],
      })
    ) {
      this.decisionData.removeObjective(objectiveIdx);
      this.onChange();
    }
  }

  resetColorization() {
    const colorMode = this.colorMode;
    if (colorMode === 'relative') {
      this.colorizationPerObjective = this.decisionData.objectives.map(
        () => ({ mode: 'relative', referenceOutcomePercentage: null }) satisfies Colorization,
      );
    } else if (colorMode === 'pro-contra') {
      this.colorizationPerObjective = this.decisionData.objectives.map((objective, objectiveIndex) => {
        const objectiveOutcomeValues = this.decisionData.outcomes
          .map(outcomes => outcomes[objectiveIndex].getValuePercentages(objective))
          .flat();

        return {
          mode: 'pro-contra',
          objectiveRange: { min: Math.min(...objectiveOutcomeValues), max: Math.max(...objectiveOutcomeValues) },
        };
      });
    } else {
      this.colorizationPerObjective = this.decisionData.objectives.map(() => ({ mode: colorMode }));
    }
  }

  setHoveredAlternative(alternativeIdx: number) {
    if (this.colorMode === 'relative') {
      this.colorizationReferenceAlternativeIdx = alternativeIdx;

      this.colorizationPerObjective = this.decisionData.objectives.map((_, objectiveIdx) => {
        const outcome = this.decisionData.outcomes[alternativeIdx][objectiveIdx];
        return {
          mode: 'relative',
          referenceOutcomePercentage: outcome.getExpectedValuePercentage(
            outcome.getValuePercentages(this.decisionData.objectives[objectiveIdx]),
          ),
        };
      });
    }
  }

  clearHoveredAlternative() {
    this.colorizationReferenceAlternativeIdx = null;
    this.resetColorization();
  }

  openNote(noteBtn: NoteBtnComponent) {
    noteBtn.open();
    // Otherwise, the note btn does not work with our OnPush strategy
    this.cdRef.markForCheck();
  }

  getGridAreaForCell(alternativeIndex: number, objectiveIndex: number) {
    let row = this.alternativeOrderOverride?.indexOf(alternativeIndex) ?? alternativeIndex;
    let column = this.objectiveOrderOverride?.indexOf(objectiveIndex) ?? objectiveIndex;

    if (this.transposed) {
      [row, column] = [column, row];
    }

    return `${row + 2} / ${column + 2}`;
  }

  reorderElementsOnDrag(event: CdkDragSortEvent) {
    if (
      (this.dragAndDropOrientation === 'horizontal' && !this.transposed) ||
      (this.dragAndDropOrientation === 'vertical' && this.transposed)
    ) {
      // Dragging Objective
      const previousObjectiveIndex = event.previousIndex;
      const currentObjectiveIndex = event.currentIndex;

      // Rearrange objectives
      if (!this.objectiveOrderOverride) {
        this.objectiveOrderOverride = this.decisionData.objectives.map((_, objectiveIndex) => objectiveIndex);
      }
      moveInArray(this.objectiveOrderOverride, previousObjectiveIndex, currentObjectiveIndex);
    } else {
      // Dragging Alternative
      const previousAlternativeIndex = event.previousIndex;
      const currentAlternativeIndex = event.currentIndex;

      // Rearrange alternatives
      if (!this.alternativeOrderOverride) {
        this.alternativeOrderOverride = this.decisionData.alternatives.map((_, alternativeIndex) => alternativeIndex);
      }
      moveInArray(this.alternativeOrderOverride, previousAlternativeIndex, currentAlternativeIndex);
    }

    this.cdRef.detectChanges();
  }

  reorderElementsOnDrop(event: CdkDragSortEvent) {
    if (
      (this.dragAndDropOrientation === 'horizontal' && !this.transposed) ||
      (this.dragAndDropOrientation === 'vertical' && this.transposed)
    ) {
      // Dragging Objective
      const previousObjectiveIndex = event.previousIndex;
      const currentObjectiveIndex = event.currentIndex;

      // Rearrange objectives
      this.decisionData.moveObjective(previousObjectiveIndex, currentObjectiveIndex);
    } else {
      // Dragging Alternative

      const previousAlternativeIndex = event.previousIndex;
      const currentAlternativeIndex = event.currentIndex;

      // Rearrange alternatives
      this.decisionData.moveAlternative(previousAlternativeIndex, currentAlternativeIndex);
    }

    this.alternativeOrderOverride = null;
    this.objectiveOrderOverride = null;
  }

  changeCdkOrientation(newOrientation: 'horizontal' | 'vertical') {
    if (!this.dragging) this.dragAndDropOrientation = newOrientation;
  }
}
