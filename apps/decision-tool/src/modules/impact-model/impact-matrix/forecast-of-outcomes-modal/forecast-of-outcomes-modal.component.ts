import { Component, EventEmitter, Inject, Input, OnInit, Output } from '@angular/core';
import { ConfirmModalComponent, ConfirmModalData } from '@entscheidungsnavi/widgets';
import {
  Alternative,
  calculateIndicatorValue,
  UserDefinedInfluenceFactor,
  DecisionData,
  InfluenceFactor,
  InfluenceFactorState,
  Objective,
  Outcome,
  PREDEFINED_INFLUENCE_FACTORS,
  PredefinedInfluenceFactor,
  SimpleUserDefinedInfluenceFactor,
} from '@entscheidungsnavi/decision-data';
import { isEqual } from 'lodash';
import { validateValue } from '@entscheidungsnavi/decision-data/validations';
import { lastValueFrom } from 'rxjs';
import { MAT_DIALOG_DATA, MatDialog, MatDialogRef } from '@angular/material/dialog';
import {
  InfluenceFactorHelperService,
  UserDefinedInfluenceFactorModalComponent,
  UserDefinedInfluenceFactorModalData,
  UserDefinedInfluenceFactorModalResult,
} from '../../influence-factors';
import { DecisionDataState } from '../../../shared/decision-data-state';

export interface ForecastOfOutcomesModalData {
  selectedTabIndex: number;
  alternative: Alternative;
  objective: Objective;
  outcome: Outcome;
}

@Component({
  templateUrl: './forecast-of-outcomes-modal.component.html',
  styleUrls: ['./forecast-of-outcomes-modal.component.scss'],
})
export class ForecastOfOutcomesModalComponent implements OnInit {
  @Input() objective: Objective;
  @Input() alternative: Alternative;
  @Input() outcome: Outcome;

  @Output() modalSave = new EventEmitter<void>();
  @Output() influenceFactorModified = new EventEmitter<number>(); // Emits the ID of the modified influence factor

  copy: Outcome;
  selectedTabIndex = 0;
  readonly predefinedInfluenceFactors = Object.values(PREDEFINED_INFLUENCE_FACTORS);

  readonly wrongOrderMessage = $localize`Unzulässige Reihenfolge der angegebenen Werte.`;
  readonly noTornadoAvailableMessage = $localize`Die Darstellung eines Tornadodiagramms setzt voraus,
  dass eine Indikatorskala vorhanden ist.`;

  get hasChanges() {
    return !(
      isEqual(this.outcome.values, this.copy.values) &&
      this.outcome.influenceFactor === this.copy.influenceFactor &&
      this.outcome.comment === this.copy.comment &&
      this.outcome.processed === this.copy.processed
    );
  }

  get isValid() {
    return this.copy.values.every(value => validateValue(value, this.objective)[0]) && this.isCorrectSystemSidedIF();
  }

  get isTornadoAvailable() {
    return this.objective.isIndicator && this.isCorrectSystemSidedIF() && this.copy.includesNonTrivialUncertainty(this.objective);
  }

  get viewType() {
    return this.objective.isIndicator ? 'indicatorView' : this.copy.influenceFactor == null ? 'noIndicatorWithoutIF' : 'noIndicatorWithIF';
  }

  get readonly() {
    return this.decisionDataState.isProjectReadonly;
  }

  influenceFactorStates: InfluenceFactorState[];

  constructor(
    protected decisionData: DecisionData,
    @Inject(MAT_DIALOG_DATA) data: ForecastOfOutcomesModalData,
    private dialog: MatDialog,
    public dialogRef: MatDialogRef<ForecastOfOutcomesModalComponent>,
    private influenceFactorHelper: InfluenceFactorHelperService,
    private decisionDataState: DecisionDataState,
  ) {
    this.selectedTabIndex = data.selectedTabIndex;
    this.alternative = data.alternative;
    this.objective = data.objective;
    this.outcome = data.outcome;
  }

  ngOnInit() {
    this.copy = this.outcome.clone();
    if (!this.isValid) {
      this.selectedTabIndex = 0;
    } else if (this.selectedTabIndex === 2 && !this.isTornadoAvailable) {
      this.selectedTabIndex = 1;
    }

    this.influenceFactorStates = this.copy.influenceFactor?.getStates();
  }

  close() {
    this.dialogRef.close(true);
  }

  async tryClose() {
    if (!this.hasChanges || (await this.confirmDiscard())) {
      this.close();
      return true;
    }

    return false;
  }

  private async confirmDiscard() {
    const result = await lastValueFrom(
      this.dialog
        .open<ConfirmModalComponent, ConfirmModalData, boolean>(ConfirmModalComponent, {
          data: {
            title: $localize`Ungespeicherte Änderungen`,
            prompt: $localize`Bist Du sicher, dass Du die ungespeicherten Änderungen an der Wirkungsprognose verwerfen willst?`,
            template: 'discard',
          },
        })
        .afterClosed(),
    );
    return result;
  }

  apply() {
    this.save();
    this.close();
  }

  save() {
    this.outcome.copyBack(this.copy);
  }

  changeUncertaintyFactor(newUf: UserDefinedInfluenceFactor) {
    this.copy.setInfluenceFactor(newUf);
    this.influenceFactorStates = this.copy.influenceFactor?.getStates();
  }

  /* Checks if row of values follows the order the corresponding (indicator/total) scale. */
  isCorrectSystemSidedIF() {
    if (!this.isInfluenceFactorPredefined(this.copy.influenceFactor) || this.copy.influenceFactor == null) {
      return true;
    }
    if (this.objective.isIndicator) {
      for (let i = 0; i < this.copy.values[0].length; i++) {
        const direction = this.objective.indicatorData.indicators[i].max - this.objective.indicatorData.indicators[i].min;
        const indicator = this.objective.indicatorData.indicators[i];
        if (
          (Math.sign(
            calculateIndicatorValue(this.copy.values[2][i], indicator) - calculateIndicatorValue(this.copy.values[1][i], indicator),
          ) !== Math.sign(direction) &&
            calculateIndicatorValue(this.copy.values[2][i], indicator) - calculateIndicatorValue(this.copy.values[1][i], indicator) !==
              0) ||
          (Math.sign(
            calculateIndicatorValue(this.copy.values[1][i], indicator) - calculateIndicatorValue(this.copy.values[0][i], indicator),
          ) !== Math.sign(direction) &&
            calculateIndicatorValue(this.copy.values[1][i], indicator) - calculateIndicatorValue(this.copy.values[0][i], indicator) !== 0)
        ) {
          return false;
        }
      }
    } else {
      let direction = 1; // always positive for verbal scale
      if (this.objective.isNumerical) {
        direction = this.objective.numericalData.to - this.objective.numericalData.from;
      }
      const copyValues = this.copy.values;
      if (
        (Math.sign(copyValues[2][0][0] - copyValues[1][0][0]) !== Math.sign(direction) &&
          copyValues[2][0][0] - copyValues[1][0][0] !== 0) ||
        (Math.sign(copyValues[1][0][0] - copyValues[0][0][0]) !== Math.sign(direction) && copyValues[1][0][0] - copyValues[0][0][0] !== 0)
      ) {
        return false;
      }
    }
    return true;
  }

  /**
   * The precision in decision-data is relative to the probability. This is calculated here.
   */
  relativeDeviation(probability: number, praezision: number) {
    return probability > 0 ? praezision * Math.min(1 - probability / 100, probability / 100) : 0;
  }

  isInfluenceFactorPredefined(influenceFactor: InfluenceFactor): influenceFactor is PredefinedInfluenceFactor {
    return influenceFactor instanceof PredefinedInfluenceFactor;
  }

  isIFUserDefined(influenceFactor: InfluenceFactor): influenceFactor is SimpleUserDefinedInfluenceFactor {
    return influenceFactor instanceof SimpleUserDefinedInfluenceFactor;
  }

  editInfluenceFactor() {
    if (this.copy.influenceFactor != null) {
      const influenceFactor = this.copy.influenceFactor as SimpleUserDefinedInfluenceFactor;
      this.dialog
        .open<UserDefinedInfluenceFactorModalComponent, UserDefinedInfluenceFactorModalData, UserDefinedInfluenceFactorModalResult>(
          UserDefinedInfluenceFactorModalComponent,
          {
            data: {
              influenceFactor: influenceFactor,
              onSaveCallback: influenceFactorId => {
                this.changeUncertaintyFactor(this.decisionData.influenceFactors[influenceFactorId]);
                this.influenceFactorModified.emit(influenceFactorId);
              },
            },
          },
        )
        .afterClosed()
        .subscribe(async (result: UserDefinedInfluenceFactorModalResult) => {
          if (result.showConnections && (await this.tryClose())) {
            this.influenceFactorHelper.showInfluenceFactorConnections(influenceFactor);
          }
        });
    }
  }

  addInfluenceFactor() {
    this.dialog.open<UserDefinedInfluenceFactorModalComponent, UserDefinedInfluenceFactorModalData>(
      UserDefinedInfluenceFactorModalComponent,
      {
        data: {
          onSaveCallback: influenceFactorId => this.changeUncertaintyFactor(this.decisionData.influenceFactors[influenceFactorId]),
        },
      },
    );
  }
}
