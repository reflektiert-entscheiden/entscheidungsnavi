import {
  Alternative,
  Indicator,
  Objective,
  ObjectiveType,
  Outcome,
  SimpleUserDefinedInfluenceFactor,
  UserDefinedState,
} from '@entscheidungsnavi/decision-data';
import { Interval } from '@entscheidungsnavi/tools';
import { ProbabilisticRiskProfile } from './probabilistic-risk-profile';

describe('ProbabilisticRiskProfile', () => {
  it('works for numerical objectives without influence factor', () => {
    const objective = new Objective();
    objective.objectiveType = ObjectiveType.Numerical;
    objective.numericalData.from = 0;
    objective.numericalData.to = 10;

    const profile = new ProbabilisticRiskProfile(objective, new Alternative(), new Outcome([[[5]]]));
    const points = profile.getDataSeries(new Interval(0, 10)).dataPoints;

    expect(points).toEqual([
      { x: 0, y: 1, outcomeValue: 0 },
      { x: 0.5, y: 0, outcomeValue: 5 },
      { x: 1, y: 0, outcomeValue: 10 },
    ]);
  });

  it('allows contracting the visible interval', () => {
    const objective = new Objective();
    objective.objectiveType = ObjectiveType.Numerical;
    objective.numericalData.from = 0;
    objective.numericalData.to = 10;

    const profile = new ProbabilisticRiskProfile(objective, new Alternative(), new Outcome([[[5]]]));
    const points = profile.getDataSeries(new Interval(5, 10)).dataPoints;

    expect(points).toEqual([
      { x: 0, y: 1, outcomeValue: 5 },
      { x: 0, y: 0, outcomeValue: 5 },
      { x: 1, y: 0, outcomeValue: 10 },
    ]);
  });

  it('works for numerical objectives with influence factor', () => {
    const objective = new Objective();
    objective.objectiveType = ObjectiveType.Numerical;
    objective.numericalData.from = 0;
    objective.numericalData.to = 10;
    const iF = new SimpleUserDefinedInfluenceFactor('', [new UserDefinedState('', 30), new UserDefinedState('', 70)]);
    const outcome = new Outcome([[[5]], [[10]]], undefined, iF);

    const profile = new ProbabilisticRiskProfile(objective, new Alternative(), outcome);
    const points = profile.getDataSeries(new Interval(0, 10)).dataPoints;

    expect(points).toEqual([
      { x: 0, y: 1, outcomeValue: 0 },
      { x: 0.5, y: 0.7, outcomeValue: 5 },
      { x: 1, y: 0, outcomeValue: 10 },
    ]);
  });

  it('works for inverted numerical objectives with influence factor', () => {
    const objective = new Objective();
    objective.objectiveType = ObjectiveType.Numerical;
    objective.numericalData.from = 10;
    objective.numericalData.to = 0;
    const iF = new SimpleUserDefinedInfluenceFactor('', [new UserDefinedState('', 30), new UserDefinedState('', 70)]);
    const outcome = new Outcome([[[5]], [[10]]], undefined, iF);

    const profile = new ProbabilisticRiskProfile(objective, new Alternative(), outcome);
    const points = profile.getDataSeries(new Interval(10, 0)).dataPoints;

    expect(points).toEqual([
      { x: 0, y: 1, outcomeValue: 10 },
      { x: 0, y: 0.3, outcomeValue: 10 },
      { x: 0.5, y: 0, outcomeValue: 5 },
      { x: 1, y: 0, outcomeValue: 0 },
    ]);
  });

  it('works for verbal objectives', () => {
    const objective = new Objective();
    objective.objectiveType = ObjectiveType.Verbal;
    objective.verbalData.addOption(0, '');
    objective.verbalData.addOption(0, '');
    objective.verbalData.addOption(0, '');
    const iF = new SimpleUserDefinedInfluenceFactor('', [new UserDefinedState('', 30), new UserDefinedState('', 70)]);
    const outcome = new Outcome([[[1]], [[2]]], undefined, iF);

    const profile = new ProbabilisticRiskProfile(objective, new Alternative(), outcome);
    const points = profile.getDataSeries(new Interval(1, 3)).dataPoints;

    expect(points).toEqual([
      { x: 0, y: 1, outcomeValue: 1 },
      { x: 0, y: 0.7, outcomeValue: 1 },
      { x: 0.5, y: 0, outcomeValue: 2 },
      { x: 1, y: 0, outcomeValue: 3 },
    ]);
  });

  it('works for indicator objectives', () => {
    const objective = new Objective();
    objective.objectiveType = ObjectiveType.Indicator;
    objective.indicatorData.indicators = [new Indicator('', 0, 10)];
    objective.indicatorData.defaultAggregationWorst = 0;
    objective.indicatorData.defaultAggregationBest = 10;
    const iF = new SimpleUserDefinedInfluenceFactor('', [new UserDefinedState('', 30), new UserDefinedState('', 70)]);
    const outcome = new Outcome([[[5]], [[10]]], undefined, iF);

    const profile = new ProbabilisticRiskProfile(objective, new Alternative(), outcome);
    const points = profile.getDataSeries(new Interval(0, 10)).dataPoints;

    expect(points).toEqual([
      { x: 0, y: 1, outcomeValue: 0 },
      { x: 0.5, y: 0.7, outcomeValue: 5 },
      { x: 1, y: 0, outcomeValue: 10 },
    ]);
  });
});
