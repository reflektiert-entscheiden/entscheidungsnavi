import {
  Alternative,
  generatePredefinedScenarios,
  Objective,
  ObjectiveType,
  Outcome,
  PredefinedInfluenceFactor,
  SimpleUserDefinedInfluenceFactor,
} from '@entscheidungsnavi/decision-data';
import { assertUnreachable } from '@entscheidungsnavi/tools/assert-unreachable';
import { RiskProfileChartSeries } from '@entscheidungsnavi/widgets';
import { Interval } from '@entscheidungsnavi/tools';

export interface RiskProfileSeries extends RiskProfileChartSeries {
  dataPoints: Array<{ x: number; y: number; outcomeValue: number }>;
}

/**
 * This class computes a probabilistic risk profile for an outcome.
 *
 * The risk profile is computed over the prior probabilities of the attached influence factors. Hence,
 * it is probabilistic.
 */
export class ProbabilisticRiskProfile {
  private _riskProfile: Array<{ outcomeValue: number; cumulativeProbability: number }>;

  constructor(
    public objective: Objective,
    public alternative: Alternative,
    public outcome: Outcome,
  ) {
    this.computeRiskProfile(outcome);
  }

  private computeRiskProfile(outcome: Outcome) {
    const newRiskProfile: Array<{ outcomeValue: number; probability: number }> = [];

    switch (this.objective.objectiveType) {
      case ObjectiveType.Numerical:
      case ObjectiveType.Verbal:
        for (let i = 0; i < outcome.values.length; i++) {
          newRiskProfile.push({
            outcomeValue: outcome.values[i][0][0],
            probability: (outcome.influenceFactor?.getStates()[i].probability ?? 100) / 100,
          });
        }
        break;
      case ObjectiveType.Indicator: {
        const aggregationFunction = this.objective.indicatorData.aggregationFunction;

        if (outcome.influenceFactor instanceof PredefinedInfluenceFactor) {
          const cartesian = generatePredefinedScenarios(
            outcome.values,
            outcome.influenceFactor.states.map(state => state.probability / 100),
          );
          cartesian.forEach(combination => {
            newRiskProfile.push({
              outcomeValue: aggregationFunction(combination.value),
              probability: combination.probability,
            });
          });
        } else if (outcome.influenceFactor instanceof SimpleUserDefinedInfluenceFactor) {
          for (let i = 0; i < outcome.values.length; i++) {
            newRiskProfile.push({
              outcomeValue: aggregationFunction(outcome.values[i]),
              probability: outcome.influenceFactor.states[i].probability / 100,
            });
          }
        } else {
          newRiskProfile.push({
            outcomeValue: aggregationFunction(outcome.values[0]),
            probability: 1,
          });
        }
        break;
      }
      default:
        assertUnreachable(this.objective.objectiveType);
    }

    const valueRange = this.objective.getRangeInterval();

    // Order by X value.
    const ascOrDesc = valueRange.start > valueRange.end ? 1 : -1;
    newRiskProfile.sort((a, b) => {
      if (a.outcomeValue < b.outcomeValue) {
        return ascOrDesc;
      }
      if (a.outcomeValue > b.outcomeValue) {
        return -ascOrDesc;
      }
      if (a.outcomeValue === b.outcomeValue) {
        return a.probability > b.probability ? ascOrDesc : -ascOrDesc;
      }
    });

    // Merge points with same x. Take the y of the latest equal point.
    // E.g. (1, 5), (1, 3), (1, 2) -> (1, 2)
    for (let i = 0; i < newRiskProfile.length - 1; i++) {
      if (Math.abs(newRiskProfile[i].outcomeValue - newRiskProfile[i + 1].outcomeValue) < 1e-15) {
        newRiskProfile[i + 1].probability += newRiskProfile[i].probability;
        newRiskProfile.splice(i--, 1);
      }
    }

    // Calculate the cumulative probability for every point
    this._riskProfile = new Array(newRiskProfile.length);
    let accumulator = 0;

    for (let i = newRiskProfile.length - 1; i >= 0; i--) {
      const entry = newRiskProfile[i];
      this._riskProfile[i] = {
        outcomeValue: entry.outcomeValue,
        cumulativeProbability: accumulator,
      };
      accumulator += entry.probability;
    }
  }

  getOutcomeValueInterval() {
    const allValues = this._riskProfile.map(profile => profile.outcomeValue);
    const objectiveRange = this.objective.getRangeInterval();

    return objectiveRange.start < objectiveRange.end
      ? new Interval(Math.min(...allValues), Math.max(...allValues))
      : new Interval(Math.max(...allValues), Math.min(...allValues));
  }

  getDataSeries(visibleOutcomeValueInterval: Interval): RiskProfileSeries {
    const dataPoints: RiskProfileSeries['dataPoints'] = [];

    dataPoints.push({ x: 0, y: 1, outcomeValue: visibleOutcomeValueInterval.start });

    for (const point of this._riskProfile) {
      dataPoints.push({
        x: visibleOutcomeValueInterval.normalizePoint(point.outcomeValue),
        y: point.cumulativeProbability,
        outcomeValue: point.outcomeValue,
      });
    }

    // If the profile does not end at the right-most x-value, make sure it does
    if (dataPoints[dataPoints.length - 1].x !== 1) {
      dataPoints.push({ x: 1, y: 0, outcomeValue: visibleOutcomeValueInterval.end });
    }

    return {
      seriesLabel: this.alternative.name,
      dataPoints,
    };
  }
}
