import { Component, Input, OnInit } from '@angular/core';
import { Alternative, DecisionData, Objective, Outcome } from '@entscheidungsnavi/decision-data';
import { at, range, round } from 'lodash';
import { Interval } from '@entscheidungsnavi/tools';
import { ProbabilisticRiskProfile, RiskProfileSeries } from './probabilistic-risk-profile';

@Component({
  selector: 'dt-risk-profile',
  templateUrl: './risk-profile.component.html',
  styleUrls: ['./risk-profile.component.scss'],
})
export class RiskProfileComponent implements OnInit {
  @Input()
  alternative: Alternative;

  @Input()
  objective: Objective;

  @Input()
  outcome: Outcome;

  // All risk profiles that can be selected. Includes this.outcome.
  riskProfiles: Array<ProbabilisticRiskProfile>;
  // The indices of risk profiles selected in the riskProfiles array EXCEPT the first. That is always selected implicitly.
  selectedRiskProfileIndices: number[] = [];

  get selectedRiskProfiles() {
    return at(this.riskProfiles, 0, this.selectedRiskProfileIndices);
  }

  visibleDataSeries: RiskProfileSeries[];
  horizontalLabels: string[];

  // Whether or not we use the whole aggregated indicator scale or just the relevant range
  useIndicatorScale = true;

  /* Number of significant digits when rounding numbers. */
  readonly significantDigits = 5;

  constructor(protected decisionData: DecisionData) {}

  ngOnInit() {
    this.createRiskProfiles();
    this.computeDataSeries();
  }

  private createRiskProfiles() {
    this.riskProfiles = [new ProbabilisticRiskProfile(this.objective, this.alternative, this.outcome)];

    const objectiveIndex = this.decisionData.objectives.indexOf(this.objective);
    // Select all outcomes of the current objective that are different from the current outcome and have defined values.
    this.decisionData.outcomes.forEach((outcomes, alternativeIndex) => {
      const outcome = outcomes[objectiveIndex];

      if (outcome.values.flat().every(value => value != null) && this.decisionData.alternatives[alternativeIndex] !== this.alternative) {
        this.riskProfiles.push(new ProbabilisticRiskProfile(this.objective, this.decisionData.alternatives[alternativeIndex], outcome));
      }
    });
  }

  computeDataSeries() {
    const selectedRiskProfiles = this.selectedRiskProfiles;

    const objectiveRange = this.objective.getRangeInterval();
    let visibleOutcomeRange: Interval;

    if (this.objective.isVerbal || this.useIndicatorScale) {
      visibleOutcomeRange = objectiveRange;
    } else {
      const allRanges = selectedRiskProfiles.map(profile => profile.getOutcomeValueInterval());

      if (objectiveRange.start < objectiveRange.end) {
        visibleOutcomeRange = new Interval(
          Math.min(...allRanges.map(range => range.start)),
          Math.max(...allRanges.map(range => range.end)),
        );
      } else {
        visibleOutcomeRange = new Interval(
          Math.max(...allRanges.map(range => range.start)),
          Math.min(...allRanges.map(range => range.end)),
        );
      }

      if (visibleOutcomeRange.start === visibleOutcomeRange.end) {
        visibleOutcomeRange = new Interval(visibleOutcomeRange.start, objectiveRange.end);
      }
    }

    if (this.objective.isVerbal) {
      this.horizontalLabels = this.objective.verbalData.options;
    } else {
      // Numerical and indicator have 9 equidistant labels
      this.horizontalLabels = range(9).map(index =>
        round(visibleOutcomeRange.start + (index * visibleOutcomeRange.length) / 8, this.significantDigits).toString(),
      );
    }

    this.visibleDataSeries = selectedRiskProfiles.map(riskProfile => riskProfile.getDataSeries(visibleOutcomeRange));
  }

  createTooltip(seriesIndex: number, valueIndex: number) {
    const riskProfileValue = this.visibleDataSeries[seriesIndex].dataPoints[valueIndex];

    const xValue = this.objective.isVerbal
      ? this.objective.verbalData.options[riskProfileValue.outcomeValue]
      : riskProfileValue.outcomeValue;

    return `(x: ${xValue} ${this.objective.unit()}; y: ${round(riskProfileValue.y * 100, 2)}%)`;
  }
}
