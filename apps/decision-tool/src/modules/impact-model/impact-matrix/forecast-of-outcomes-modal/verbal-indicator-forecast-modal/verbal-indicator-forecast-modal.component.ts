import { Component, Inject, ViewChild } from '@angular/core';
import { Alternative, InfluenceFactor, Objective, ObjectiveInput } from '@entscheidungsnavi/decision-data';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { VerbalIndicatorDiagramComponent } from '../../../verbal-indicator-diagram/verbal-indicator-diagram.component';

export type VerbalIndicatorForecastModalData = {
  indicatorIdx: number;
  alternative: Alternative;
  objective: Objective;
  influenceFactor: InfluenceFactor;
  outcomeValues: ObjectiveInput[];
  stateIdx: number;
};

@Component({
  selector: 'dt-verbal-indicator-values-modal',
  templateUrl: './verbal-indicator-forecast-modal.component.html',
})
export class VerbalIndicatorForecastModalComponent {
  verbalIndicatorForecastModalData: VerbalIndicatorForecastModalData;

  @ViewChild('diagram') diagram: VerbalIndicatorDiagramComponent;

  constructor(
    @Inject(MAT_DIALOG_DATA)
    data: VerbalIndicatorForecastModalData,
    public dialogRef: MatDialogRef<VerbalIndicatorForecastModalComponent>,
  ) {
    this.verbalIndicatorForecastModalData = {
      ...data,
    };
  }

  apply() {
    this.dialogRef.close({
      values: this.verbalIndicatorForecastModalData.outcomeValues,
    });
  }
}
