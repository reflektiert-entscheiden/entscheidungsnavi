import { Component, inject, Input, OnInit, ViewChild } from '@angular/core';
import {
  AggregationFunction,
  calculateIndicatorValue,
  getIndicatorAggregationFunction,
  Objective,
  Outcome,
  PredefinedInfluenceFactor,
} from '@entscheidungsnavi/decision-data';
import { round } from 'lodash';
import {
  orderAndMergeTornadoPoints,
  ScaleAxis,
  TornadoData,
  TornadoPoint,
} from '@entscheidungsnavi/widgets/tornado-diagram/tornado-helper';
import { getInfluenceFactorStateName, PersistentSettingParent } from '@entscheidungsnavi/widgets';
import { TornadoDiagramComponent } from '@entscheidungsnavi/widgets/tornado-diagram/tornado-diagram.component';
import { DecimalPipe } from '@angular/common';
import { PersistentSetting } from '@entscheidungsnavi/widgets';

@Component({
  selector: 'dt-outcome-tornado-diagram',
  templateUrl: './outcome-tornado-diagram.component.html',
  styleUrls: ['./outcome-tornado-diagram.component.scss'],
})
@PersistentSettingParent('OutcomeTornadoDiagramComponent')
export class OutcomeTornadoDiagramComponent implements OnInit {
  @Input()
  objective: Objective;

  @Input()
  outcome: Outcome;

  @ViewChild(TornadoDiagramComponent) tornadoDiagram: TornadoDiagramComponent;

  /* Scale min/max when "Worst-Best-Szenarien" is selected. */
  worstBestScaleMin: number;
  worstBestScaleMax: number;

  /* Scale min/max when "fixed" is selected. */
  fixedScaleMin: number;
  fixedScaleMax: number;

  /* Total scale (indicator min and max). */
  indicatorScaleMin: number;
  indicatorScaleMax: number;

  /* Expected value in the probabilistic variant (center dashed line). */
  totalExpectedValue: number;

  indicatorFunction: AggregationFunction;

  isOutcomePredefined: boolean;

  /* Select box values. */
  isProbabilisticVariant = false;
  scaleVariant: 'total' | 'worst-best' | 'fixed';
  @PersistentSetting()
  defaultOrder = true;

  tornadoData: TornadoData = {
    bars: [],
    totalExpectedValue: { value: 0 },
  };
  scaleAxis: ScaleAxis = {
    label: $localize`Ergebnis`,
    unit: '',
    from: 0,
    to: 0,
  };

  readonly significantDigits = 5;

  private readonly decimalPipe = inject(DecimalPipe);

  ngOnInit() {
    this.isOutcomePredefined = this.outcome.influenceFactor instanceof PredefinedInfluenceFactor;
    this.scaleVariant = this.isOutcomePredefined ? 'worst-best' : 'fixed';
    this.scaleAxis.unit = this.objective.unit();
    this.updateIndicatorInfo();
    // draw diagram
    if (this.allOutcomeValuesDefined()) {
      this.createTornadoDiagram();
    }
  }

  // indicator scale min/max and indicator function
  updateIndicatorInfo() {
    if (!this.objective.indicatorData.useCustomAggregation) {
      // additive min/max
      this.indicatorFunction = getIndicatorAggregationFunction(this.objective.indicatorData.indicators, {
        worst: this.objective.indicatorData.defaultAggregationWorst,
        best: this.objective.indicatorData.defaultAggregationBest,
      });
      this.indicatorScaleMin = this.objective.indicatorData.defaultAggregationWorst;
      this.indicatorScaleMax = this.objective.indicatorData.defaultAggregationBest;
    } else {
      // custom min/max
      this.indicatorFunction = getIndicatorAggregationFunction(this.objective.indicatorData.indicators, {
        formula: this.objective.indicatorData.customAggregationFormula,
        globalVariables: this.objective.indicatorData.globalVariables.map,
      });
      this.indicatorScaleMin = this.indicatorFunction(
        this.objective.indicatorData.indicators.map(ind => [ind.min]),
        true,
      );
      this.indicatorScaleMax = this.indicatorFunction(
        this.objective.indicatorData.indicators.map(ind => [ind.max]),
        true,
      );
    }
  }

  createTornadoDiagram() {
    this.calculateTornadoPoints();
    this.calculateScaleLimits();
    this.updateScaleLimits();
  }

  /* Convert outcome.values into tornado points. */
  private calculateTornadoPoints() {
    const states = this.outcome.influenceFactor.getStates();
    this.tornadoData.bars = [];

    // generate a template vector including either medians (simple variant / user defined IF) or expected values (probabilistic variant)
    const valuesTemplate =
      !this.isProbabilisticVariant && this.isOutcomePredefined
        ? this.outcome.generateMedianVector(this.objective)
        : this.outcome.generateExpectedValuesVector(this.objective);

    let totalExpectedValue = 0;
    for (let indicatorIdx = 0; indicatorIdx < this.objective.indicatorData.indicators.length; indicatorIdx++) {
      this.tornadoData.bars.push({
        label: this.objective.indicatorData.indicators[indicatorIdx].name,
        unit: this.objective.indicatorData.indicators[indicatorIdx].unit,
        points: [],
        children: [],
        areChildrenExpanded: false,
      });
      const points: TornadoPoint[] = [];
      for (let stateIdx = 0; stateIdx < states.length; stateIdx++) {
        // initialise a combination including all template values
        const tornadoPointValues = valuesTemplate.map(v => [v]); // encapsulate in extra array for the verbal dimension
        // replace the template value of the current indicatorIdx with its actual value
        tornadoPointValues[indicatorIdx][0] = calculateIndicatorValue(
          this.outcome.values[stateIdx][indicatorIdx],
          this.objective.indicatorData.indicators[indicatorIdx],
        );
        const tooltipRows = [
          {
            stateName: getInfluenceFactorStateName(this.outcome.influenceFactor, stateIdx, true),
            stateValue: this.getOutcomeValue(stateIdx, indicatorIdx),
            probability: states[stateIdx].probability,
          },
        ];
        points.push({
          tornadoPointLabel:
            round(
              calculateIndicatorValue(this.outcome.values[stateIdx][indicatorIdx], this.objective.indicatorData.indicators[indicatorIdx]),
              this.significantDigits,
            ) + '',
          tornadoPointValue: this.indicatorFunction(tornadoPointValues, true),
          tooltipCombinations: [{ probability: states[stateIdx].probability / 100, tooltipRows }],
        });
        totalExpectedValue += points[stateIdx].tornadoPointValue;
      }

      this.tornadoData.bars[indicatorIdx].points = orderAndMergeTornadoPoints(points, this.outcome.influenceFactor);
    }
    this.tornadoData.totalExpectedValue.value =
      !this.isProbabilisticVariant && this.isOutcomePredefined
        ? this.indicatorFunction(
            valuesTemplate.map(v => [v]),
            true,
          ) // simple variant takes the median of either of the bars (all equal)
        : totalExpectedValue / (this.objective.indicatorData.indicators.length * this.outcome.influenceFactor.stateCount);
  }

  private calculateScaleLimits() {
    const allTornadoPointValues = this.tornadoData.bars.flatMap(bar => bar.points.map(v => v.tornadoPointValue));
    const isIndicatorScaleInverted = this.indicatorScaleMin > this.indicatorScaleMax;
    this.fixedScaleMin = !isIndicatorScaleInverted ? Math.min(...allTornadoPointValues) : Math.max(...allTornadoPointValues);
    this.fixedScaleMax = isIndicatorScaleInverted ? Math.min(...allTornadoPointValues) : Math.max(...allTornadoPointValues);

    if (this.isOutcomePredefined) {
      const scaleLimit1 = this.indicatorFunction(this.outcome.values[0]);
      const scaleLimit2 = this.indicatorFunction(this.outcome.values[2]);
      this.worstBestScaleMin = !isIndicatorScaleInverted ? Math.min(scaleLimit1, scaleLimit2) : Math.max(scaleLimit1, scaleLimit2);
      this.worstBestScaleMax = isIndicatorScaleInverted ? Math.min(scaleLimit1, scaleLimit2) : Math.max(scaleLimit1, scaleLimit2);
    }
  }

  updateScaleLimits() {
    switch (this.scaleVariant) {
      case 'total':
        this.scaleAxis.from = this.indicatorScaleMin;
        this.scaleAxis.to = this.indicatorScaleMax;
        break;
      case 'worst-best':
        this.scaleAxis.from = this.worstBestScaleMin;
        this.scaleAxis.to = this.worstBestScaleMax;
        break;
      case 'fixed':
        this.scaleAxis.from = this.fixedScaleMin;
        this.scaleAxis.to = this.fixedScaleMax;
        break;
    }
    this.tornadoData = { ...this.tornadoData };
  }

  private allOutcomeValuesDefined() {
    return this.outcome.values.flat(2).every(value => value != null);
  }

  private getOutcomeValue(stateIdx: number, indicatorIdx?: number) {
    const value = this.decimalPipe.transform(this.outcome.values[stateIdx][indicatorIdx][0], `1.0-${this.significantDigits}`);
    const unit = this.objective.indicatorData.indicators[indicatorIdx].unit;
    return `${value}${unit !== '' ? ' ' + unit : ''}`;
  }
}
