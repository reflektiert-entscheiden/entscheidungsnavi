import { cloneDeep } from 'lodash';
import { Component, Inject, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { DecisionData, Outcome } from '@entscheidungsnavi/decision-data';
import { MAT_DIALOG_DATA, MatDialog, MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'dt-copy-scale-modal',
  templateUrl: './copy-scale-modal.component.html',
  styleUrls: ['./copy-scale-modal.component.scss'],
})
export class CopyScaleModalComponent implements OnInit {
  checkBoxes: boolean[];
  objectiveIdx: number;

  @ViewChild('confirmModal', { static: true })
  confirmModal: TemplateRef<any>;

  get noObjectivesChecked(): boolean {
    return this.checkBoxes.every((value, index) => value == false || index == this.objectiveIdx);
  }

  get allObjectivesChecked(): boolean {
    return this.checkBoxes.every((value, index) => value == true || index == this.objectiveIdx);
  }

  constructor(
    protected decisionData: DecisionData,
    private dialog: MatDialog,
    @Inject(MAT_DIALOG_DATA) data: any,
    public dialogRef: MatDialogRef<CopyScaleModalComponent>,
  ) {
    this.objectiveIdx = data.objectiveIdx;
  }

  ngOnInit() {
    this.checkBoxes = new Array(this.decisionData.objectives.length).fill(false);
  }

  /* Either checkAll or checkNone */
  checkAll() {
    const checkValue = !this.allObjectivesChecked;
    for (let i = 0; i < this.checkBoxes.length; i++) {
      if (i == this.objectiveIdx) {
        continue;
      }
      this.checkBoxes[i] = checkValue;
    }
  }

  confirmChanges() {
    if (this.noObjectivesChecked) {
      return;
    }
    if (this.shouldConfirm()) {
      // confirm copy
      this.dialog
        .open(this.confirmModal)
        .afterClosed()
        .subscribe((confirmed: boolean) => {
          if (confirmed) {
            this.copy();
            this.dialogRef.close(true);
          }
        });
    } else {
      // coppy
      this.copy();
      this.dialogRef.close(true);
    }
  }

  /* Copies scale to the selected objectives */
  copy() {
    for (let i = 0; i < this.checkBoxes.length; i++) {
      if (this.checkBoxes[i]) {
        // copy scale
        const numericalDataClone = cloneDeep(this.decisionData.objectives[this.objectiveIdx].numericalData);
        const verbalDataClone = cloneDeep(this.decisionData.objectives[this.objectiveIdx].verbalData);
        const indicatorDataClone = cloneDeep(this.decisionData.objectives[this.objectiveIdx].indicatorData);
        this.decisionData.objectives[i].numericalData = numericalDataClone;
        this.decisionData.objectives[i].verbalData = verbalDataClone;
        this.decisionData.objectives[i].indicatorData = indicatorDataClone;
        this.decisionData.changeObjectiveType(i, this.decisionData.objectives[this.objectiveIdx].objectiveType);
        // reset data in column
        for (let j = 0; j < this.decisionData.outcomes.length; j++) {
          this.decisionData.outcomes[j][i] = new Outcome(
            null,
            this.decisionData.objectives[this.objectiveIdx],
            undefined,
            undefined,
            this.decisionData.outcomes[j][i].comment,
          );
        }
        // uncheck box
        this.checkBoxes[i] = false;
      }
    }
  }

  /* Checks if copy should be confirmed */
  shouldConfirm() {
    for (let i = 0; i < this.checkBoxes.length; i++) {
      if (this.checkBoxes[i] && this.objectiveHasValues(i)) {
        return true;
      }
    }
    return false;
  }

  /* Checks if matrix column is empty */
  objectiveHasValues(idx: number) {
    for (let j = 0; j < this.decisionData.outcomes.length; j++) {
      if (this.decisionData.outcomes[j][idx].isEmpty == false || this.decisionData.outcomes[j][idx].processed == true) {
        return true;
      }
    }
    return false;
  }
}
