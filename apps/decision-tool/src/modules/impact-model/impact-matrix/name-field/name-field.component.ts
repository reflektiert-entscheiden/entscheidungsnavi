import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  EventEmitter,
  HostBinding,
  Input,
  Output,
  ViewChild,
} from '@angular/core';

@Component({
  selector: 'dt-impact-matrix-name-field',
  templateUrl: './name-field.component.html',
  styleUrls: ['./name-field.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class NameFieldComponent {
  @ViewChild('nameField') nameField: ElementRef<HTMLElement>;

  protected _name = '';

  @Input()
  set name(value: string) {
    if (value !== this._name && !this.focused) {
      this._name = value;
    }
  }

  @Output()
  nameChange = new EventEmitter<string>();

  // must be without any line breaks.
  @Input()
  defaultName: string;

  @Input()
  editable = true;

  @Input()
  isTwoLiner = false;

  // TODO: Does not have real 'disabled'-like functionality yet. For now it only makes the text faded out.
  // NOTE: This is only supported in light-mode.
  @HostBinding('class.disabled')
  @Input()
  disabled = false;

  @HostBinding('class.light')
  @Input()
  light = true;

  focused = false;

  constructor(private cdr: ChangeDetectorRef) {}

  focus(setCursor = false) {
    if (!this.editable) return;

    this.nameField.nativeElement.focus();
    if (setCursor) {
      const range = document.createRange();
      range.selectNodeContents(this.nameField.nativeElement);
      range.collapse(false);

      const sel = window.getSelection();
      sel.removeAllRanges();
      sel.addRange(range);
    }
    this.focused = true;

    // Remember we are an OnPush-Component and focus may not come with a change detection cycle.
    this.cdr.detectChanges();
  }

  blur() {
    this.focused = false;
    this.nameField.nativeElement.scroll(0, 0);
    this._name = this.nameField.nativeElement.textContent;

    if (!this._name) this.nameChange.emit(this.defaultName);
  }

  onInput() {
    this.nameChange.emit(this.nameField.nativeElement.textContent);
  }

  blurIfEnter(event: KeyboardEvent) {
    if (event.key === 'Enter') {
      event.preventDefault();

      if (window.getSelection) {
        if (window.getSelection().empty) {
          // Chrome
          window.getSelection().empty();
        } else if (window.getSelection().removeAllRanges) {
          // Firefox
          window.getSelection().removeAllRanges();
        }
      }

      this.blur();
    }
  }
}
