import { Component, ElementRef, Inject, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialog, MatDialogRef } from '@angular/material/dialog';
import {
  IndicatorObjectiveData,
  IndicatorObjectiveStage,
  isEqualIndicatorObjectiveStage,
  VerbalIndicatorCategory,
} from '@entscheidungsnavi/decision-data';
import { cloneDeep, round, zip } from 'lodash';

@Component({
  templateUrl: './indicator-description-modal.component.html',
  styleUrls: ['./indicator-description-modal.component.scss'],
})
export class IndicatorDescriptionModalComponent implements OnInit {
  @ViewChild('stageNumberInputModal') stageNumberInputModal: TemplateRef<any>;
  @ViewChild('initParamsOpener', { read: ElementRef }) initParamsOpener: ElementRef;
  @ViewChild('mainContainer') mainContainer: ElementRef;

  // data
  indicatorData: IndicatorObjectiveData;
  scaleMin: number;
  scaleMax: number;

  // stages
  numberOfStages: number;
  decimalPositions: number;
  copyStages: IndicatorObjectiveStage[]; // saves temporary stage data

  // custom stage number modal
  selectedCustomNumberIdx: number;
  possibleCustomNumberOfStages: number[];
  customNumberOfStagesDialogRef: MatDialogRef<any>;

  // UI
  addingStageMode = false;
  openedStage = -1;
  showStages = true;
  displayVariant: 'simple' | 'extended' = 'simple';

  // constants
  readonly maxDecimalPoints = 14;
  readonly minNumberOfPrettyNumbers = 100;

  get hasChanges() {
    if (this.copyStages.length !== this.indicatorData.stages.length) {
      return true;
    }

    return zip(this.copyStages, this.indicatorData.stages).some(
      ([newStage, oldStage]) => !isEqualIndicatorObjectiveStage(newStage, oldStage),
    );
  }

  get hasVerbalIndicators() {
    return this.indicatorData.indicators.some(indicator => indicator.isVerbalized);
  }

  get listOfVerbalCategories() {
    return this.indicatorData.indicators
      .map(indicator => {
        return indicator.verbalIndicatorCategories.map((category: VerbalIndicatorCategory & { isSolo: boolean; indicatorName: string }) => {
          category.indicatorName = indicator.name;
          category.isSolo = indicator.verbalIndicatorCategories.length === 1;
          return category;
        });
      })
      .flat(1);
  }

  get validStages() {
    let currentMinValue = Math.min(this.scaleMin, this.scaleMax);
    if (this.copyStages[0].value <= this.copyStages[this.copyStages.length - 1].value) {
      if (this.scaleMin > this.scaleMax) {
        return false;
      }
      for (let i = 0; i < this.copyStages.length; i++) {
        if (this.copyStages[i].value < currentMinValue || this.copyStages[i].value > Math.max(this.scaleMin, this.scaleMax)) {
          return false;
        }
        currentMinValue = this.copyStages[i].value;
      }
    } else {
      if (this.scaleMin < this.scaleMax) {
        return false;
      }
      for (let i = this.copyStages.length - 1; i >= 0; i--) {
        if (this.copyStages[i].value < currentMinValue || this.copyStages[i].value > Math.max(this.scaleMin, this.scaleMax)) {
          return false;
        }
        currentMinValue = this.copyStages[i].value;
      }
    }
    return true;
  }

  get hasDuplicates() {
    const stageValues = this.copyStages.map(s => s.value);
    return new Set(stageValues).size !== stageValues.length;
  }

  constructor(
    private dialog: MatDialog,
    public dialogRef: MatDialogRef<IndicatorDescriptionModalComponent>,
    @Inject(MAT_DIALOG_DATA)
    data: {
      indicatorData: IndicatorObjectiveData;
    },
  ) {
    this.copyStages = cloneDeep(data.indicatorData.stages);
    this.numberOfStages = this.copyStages.length;
    this.indicatorData = cloneDeep(data.indicatorData);
    this.scaleMin = data.indicatorData.defaultAggregationWorst;
    this.scaleMax = data.indicatorData.defaultAggregationBest;

    if (this.copyStages.length === 0) {
      this.copyStages = [
        { value: this.scaleMin, description: '' },
        { value: this.scaleMin + (this.scaleMax - this.scaleMin) / 2, description: '' },
        { value: this.scaleMax, description: '' },
      ];

      this.numberOfStages = this.copyStages.length;
    }
  }

  ngOnInit() {
    // stages precision
    this.decimalPositions = this.findScalePrecision(this.scaleMin, this.scaleMax, this.minNumberOfPrettyNumbers);
  }

  findScalePrecision(lowerBound: number, upperBound: number, numberOfPrettyNumbers: number) {
    let precision = 1;
    for (let i = 0; i < this.maxDecimalPoints; i++) {
      if (Math.min(lowerBound, upperBound) + precision * numberOfPrettyNumbers <= Math.max(lowerBound, upperBound)) {
        return i;
      } else {
        precision /= 10;
      }
    }
    return 0;
  }

  addStage(stageIdx: number) {
    if (this.numberOfStages >= this.indicatorData.maxStages) {
      // already at max number of stages (7)
      return;
    }
    let avgValue;
    if (stageIdx === 0) {
      avgValue = this.scaleMin;
    } else if (stageIdx === this.numberOfStages) {
      avgValue = this.scaleMax;
    } else {
      avgValue = (this.copyStages[stageIdx - 1].value + this.copyStages[stageIdx].value) / 2;
    }
    avgValue = round(avgValue, this.decimalPositions);
    // insert new row
    this.copyStages.splice(stageIdx, 0, { value: avgValue, description: '' });
    this.numberOfStages++;
    this.focusField(stageIdx);
  }

  deleteStage(stageIdx: number) {
    if (this.numberOfStages <= this.indicatorData.minStages || stageIdx === 0 || stageIdx === this.copyStages.length - 1) {
      return;
    }
    this.copyStages.splice(stageIdx, 1);
    this.numberOfStages--;
  }

  focusField(idx: number) {
    setTimeout(() => {
      const inputElements = document.querySelectorAll('.stage-value');
      (inputElements[idx] as HTMLElement)?.focus();
    }, 50);
  }

  saveData() {
    if (this.validStages && !this.hasDuplicates) {
      this.dialogRef.close(this.copyStages);
    }
  }

  openStage(expandRowIdx: number) {
    if (!this.validStages || this.hasDuplicates) {
      return;
    }

    this.openedStage = expandRowIdx;
    this.showStages = false;
  }

  closeStage() {
    this.openedStage = -1;
    this.showStages = true;
  }

  toggleAddingStageMode() {
    if (!this.addingStageMode && this.numberOfStages >= this.indicatorData.maxStages) {
      return;
    }
    this.addingStageMode = !this.addingStageMode;
  }

  checkVerbalStages() {
    const stageNumbers = this.indicatorData.indicators
      .map(ind =>
        ind.verbalIndicatorCategories.map(category => {
          return category.stages.length;
        }),
      )
      .flat(1);
    const bestNumberOfStages = this.indicatorData.calculateBestNumberOfVerbalStages();
    if (bestNumberOfStages === 2 && !stageNumbers.every(x => x === stageNumbers[0])) {
      // most fitting number of verbal stages is 2, ask user if they prefer a greater number of stages
      this.possibleCustomNumberOfStages = [...new Set(this.listOfVerbalCategories.map(category => category.stages.length))].sort();
      this.customNumberOfStagesDialogRef = this.dialog.open(this.stageNumberInputModal);
    } else {
      this.copyStages = this.indicatorData.generateSuggestedVerbalStages(this.decimalPositions, bestNumberOfStages);
      this.numberOfStages = this.copyStages.length;
      this.addingStageMode = false;
    }
  }

  generateSuggestedVerbalStages() {
    if (
      this.selectedCustomNumberIdx == null ||
      this.possibleCustomNumberOfStages[this.selectedCustomNumberIdx] < 2 ||
      this.possibleCustomNumberOfStages[this.selectedCustomNumberIdx] > 7
    )
      return;
    this.copyStages = this.indicatorData.generateSuggestedVerbalStages(
      this.decimalPositions,
      this.possibleCustomNumberOfStages[this.selectedCustomNumberIdx],
    );
    this.numberOfStages = this.copyStages.length;
    this.addingStageMode = false;
    this.closeCustomNumberOfStagesModal();
  }

  closeCustomNumberOfStagesModal() {
    this.customNumberOfStagesDialogRef.close();
    this.selectedCustomNumberIdx = null;
  }
}
