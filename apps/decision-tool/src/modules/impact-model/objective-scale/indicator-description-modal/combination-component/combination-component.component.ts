import { AfterViewInit, ChangeDetectorRef, Component, ElementRef, Inject, Input, LOCALE_ID, OnInit, ViewChild } from '@angular/core';
import { formatNumber } from '@angular/common';

import {
  getIndicatorAggregationFunction,
  getIndicatorValueName,
  Indicator,
  IndicatorObjectiveStage,
} from '@entscheidungsnavi/decision-data';
import { appendRichText } from '@entscheidungsnavi/tools';
import { cloneDeep, without } from 'lodash';
import { GlobalVariables } from '@entscheidungsnavi/decision-data/classes/global-variables';
import { MatTooltip } from '@angular/material/tooltip';
import { MatIcon } from '@angular/material/icon';
import { FormsModule } from '@angular/forms';
import { WidthTriggerDirective } from '@entscheidungsnavi/widgets/directives/width-trigger.directive';
import { SafeNumberPipe } from '@entscheidungsnavi/widgets/pipes/safe-number.pipe';
import { MatButton } from '@angular/material/button';

@Component({
  selector: 'dt-combination-component',
  templateUrl: './combination-component.component.html',
  styleUrls: ['./combination-component.component.scss'],
  standalone: true,
  imports: [MatButton, WidthTriggerDirective, MatTooltip, MatIcon, FormsModule, SafeNumberPipe],
})
export class CombinationComponent implements AfterViewInit, OnInit {
  @Input() globalVariables: GlobalVariables;
  @Input() aggregatedUnit: string;
  @Input() customAggregationFormula: string;
  @Input() expandOptions = false;
  @Input() indicators: Indicator[];
  @Input() isAdditive: boolean;
  @Input() openedStage: number;
  @Input() scaleMin: number;
  @Input() scaleMax: number;
  @Input() stages: IndicatorObjectiveStage[];

  @ViewChild('table') table: ElementRef;

  // precisions
  scalePrecision = 0.05;
  indicatorPrecisionIndexes: number[];
  maxIndicatorPrecisionIndexes: number[];

  // input & output
  solutionCount = 0;
  indicatorTestValuesInput: number[];
  indicatorTestValuesOutput: { values: number[]; saved: boolean }[];

  // error messages
  showNoSolution = false;
  showNoNewSolution = false;

  // other
  areIndicatorsPresent: boolean[] = [];
  numberOfUsedIndicators: number;

  // constants
  readonly roundingErrorPrecision = 0.0000000001;
  readonly maxDecimalPoints = 14; // number of numbers after decimal point
  readonly maxIterations = 10000;
  readonly minIndPrecision = 0.000001;
  readonly maxIndPrecision = 1000000; // 1M
  readonly minNumberOfStages = 10;
  readonly minScalePrecision = 0;
  readonly maxDeviationCorrections = 5;
  readonly maxScalePrecision = 0.1;

  get numberFormat() {
    const value = this.scalePrecision * Math.abs(this.scaleMax - this.scaleMin);
    return this.fixRoundingError(value);
  }

  constructor(
    private cdr: ChangeDetectorRef,
    @Inject(LOCALE_ID) private locale: string,
  ) {}

  ngAfterViewInit() {
    this.cdr.detectChanges();
  }

  ngOnInit() {
    this.areIndicatorsPresent = new Array(this.indicators.length).fill(true);
    if (!this.isAdditive) {
      // mark unused indicators
      for (let i = 0; i < this.indicators.length; i++) {
        if (!this.customAggregationFormula.includes(getIndicatorValueName(i))) {
          this.areIndicatorsPresent[i] = false;
        }
      }
    }
    this.numberOfUsedIndicators = this.areIndicatorsPresent.filter(Boolean).length;

    this.indicatorPrecisionIndexes = this.indicators.map(ind => {
      const startingValue = this.findMinPrecisionForRange(ind.min, ind.max);
      const startingIdx = this.precisionAbsToIndex(startingValue);
      return startingIdx;
    });
    this.maxIndicatorPrecisionIndexes = this.indicators.map(ind => {
      const maxValue = Math.min(this.maxIndPrecision, Math.max(Math.abs(ind.min), Math.abs(ind.max)));
      const maxProperValue = this.findNextProperValue(maxValue);
      const maxIdx = this.precisionAbsToIndex(maxProperValue);
      return maxIdx;
    });
    this.indicatorTestValuesOutput = [];
    this.indicatorTestValuesInput = [];
    for (let i = 0; i < this.indicators.length; i++) {
      this.indicatorTestValuesInput[i] = null;
    }

    for (let i = 0; i < 5; i++) {
      this.calculateForInput();
    }

    this.closeAllErrors();
  }

  roundNumber(n: number) {
    return Math.round(n * 100) / 100;
  }

  // fixes undesired rounding errors such as 0.1 + 0.2 = 0.30000000000000004
  // 0.30000000000000004 comes as input 0.3 comes as output in this case
  // in some cases such an error leaves correct solutions outside of the desired range
  fixRoundingError(n: number) {
    return Math.round(n * (1 / this.roundingErrorPrecision)) / (1 / this.roundingErrorPrecision);
  }

  calculateForInput() {
    this.closeAllErrors();
    const emptyIndexes = [];
    for (let i = 0; i < this.indicators.length; i++) {
      if (this.indicatorTestValuesInput[i] == null) {
        emptyIndexes.push(i);
      }
    }

    if (emptyIndexes.length > 0 && !this.isInputIncorrect()) {
      if (this.isAdditive) {
        this.calculateAdditive(emptyIndexes);
      } else {
        this.calculateCustom(emptyIndexes);
      }
    }
  }

  calculateAdditive(emptyIndexes: number[]) {
    if (this.openedStage < 0) {
      return;
    }
    const emptyIndexesOriginal = cloneDeep(emptyIndexes);
    const coefficientVector = this.indicators.map(ind => ind.coefficient);
    const coefficientSum = coefficientVector.reduce((partialSum, coef) => partialSum + coef, 0);
    const coefficientPercVector = coefficientVector.map(coef => coef / coefficientSum);
    const lowerBounds = this.indicators.map((ind, idx) => {
      if (this.indicatorTestValuesInput[idx] != null) {
        return (this.indicatorTestValuesInput[idx] - this.indicators[idx].min) / (this.indicators[idx].max - this.indicators[idx].min);
      } else {
        return 0;
      }
    });
    const upperBounds = this.indicators.map((ind, idx) => {
      if (this.indicatorTestValuesInput[idx] != null) {
        return (this.indicatorTestValuesInput[idx] - this.indicators[idx].min) / (this.indicators[idx].max - this.indicators[idx].min);
      } else {
        return 1;
      }
    });
    const utilityValueMin = coefficientPercVector.reduce((partialSum, coefPerc, idx) => partialSum + coefPerc * lowerBounds[idx], 0);
    const utilityValueMax = coefficientPercVector.reduce((partialSum, coefPerc, idx) => partialSum + coefPerc * upperBounds[idx], 0);
    const desiredValueRaw = this.stages[this.openedStage].value;
    const desiredValuePerc = (desiredValueRaw - this.scaleMin) / (this.scaleMax - this.scaleMin);
    if (
      !(
        Math.min(utilityValueMin, utilityValueMax) <= desiredValuePerc + this.scalePrecision &&
        Math.max(utilityValueMin, utilityValueMax) >= desiredValuePerc - this.scalePrecision
      )
    ) {
      this.displayNoSolution();
      return;
    }
    for (let i = 0; i < this.maxIterations; i++) {
      emptyIndexes = cloneDeep(emptyIndexesOriginal);
      const lowerBoundVectors = [];
      const upperBoundVectors = [];
      const result = [];
      for (let j = 0; j < this.indicators.length; j++) {
        if (!emptyIndexes.includes(j)) {
          result[j] = (this.indicatorTestValuesInput[j] - this.indicators[j].min) / (this.indicators[j].max - this.indicators[j].min);
        } else {
          result[j] = null;
        }
      }
      const emptyIndexesCopy = cloneDeep(emptyIndexes);
      if (emptyIndexes.length > 0) {
        // still unsolvable
        for (const emptyIdx of emptyIndexesCopy) {
          lowerBoundVectors[emptyIdx] = [];
          upperBoundVectors[emptyIdx] = [];
          for (let j = 0; j < this.indicators.length; j++) {
            // build upper/lower bound for indicator idx
            // set all values beside the one at position [idx]
            if (emptyIndexes.includes(j) && j !== emptyIdx) {
              // empty indicators (which are not the current indicator)
              lowerBoundVectors[emptyIdx][j] = upperBounds[j];
              upperBoundVectors[emptyIdx][j] = lowerBounds[j];
            } else {
              // non-empty indicator (already assigned)
              lowerBoundVectors[emptyIdx][j] = result[j];
              upperBoundVectors[emptyIdx][j] = result[j];
            }
          }
          // adjust according to constructed scale precision
          const lowerDesiredValue = desiredValuePerc - this.scalePrecision;
          const upperDesiredValue = desiredValuePerc + this.scalePrecision;
          // set values at position [idx]
          // ignores itself in the sum
          const lowerBoundSumOfProducts = lowerBoundVectors[emptyIdx].reduce(
            (partialSum, lbValue, idx) => partialSum + (lbValue ?? 0) * coefficientPercVector[idx],
            0,
          );
          lowerBoundVectors[emptyIdx][emptyIdx] = (lowerDesiredValue - lowerBoundSumOfProducts) / coefficientPercVector[emptyIdx];
          const upperBoundSumOfProducts = upperBoundVectors[emptyIdx].reduce(
            (partialSum, ubValue, idx) => partialSum + (ubValue ?? 0) * coefficientPercVector[idx],
            0,
          );
          upperBoundVectors[emptyIdx][emptyIdx] = (upperDesiredValue - upperBoundSumOfProducts) / coefficientPercVector[emptyIdx];

          const currentLBCutOff = Math.max(lowerBoundVectors[emptyIdx][emptyIdx], lowerBounds[emptyIdx]);
          const currentUBCutOff = Math.min(upperBoundVectors[emptyIdx][emptyIdx], upperBounds[emptyIdx]);

          const randomNumber = this.getRoundedRandom(currentLBCutOff, currentUBCutOff, emptyIdx);
          if (randomNumber == null) {
            // no valid value for the last indicator with the current precision
            continue;
          }
          result[emptyIdx] = randomNumber;
          emptyIndexes = without(emptyIndexes, emptyIdx);
          if (emptyIndexes.length < 1) {
            // we can solve for the last index
            break;
          }
        }
      }
      const resultValues = result.map((val, idx) => {
        return this.percentageToValue(val, this.indicators[idx].min, this.indicators[idx].max);
      });
      const fct = getIndicatorAggregationFunction(this.indicators, { worst: this.scaleMin, best: this.scaleMax });
      const fctRes = fct(
        resultValues.map(x => [x]),
        true,
      ); // encapsulate indicator values (all indicators act like numerical in this module)
      const fctResPerc = (fctRes - this.scaleMin) / (this.scaleMax - this.scaleMin);
      const resultDeviation = Math.abs(desiredValuePerc - fctResPerc);
      if (this.fixRoundingError(resultDeviation) <= this.scalePrecision && this.isNewSolution(resultValues)) {
        const scaleOutputPrecision = this.findScalePrecision(this.scaleMin, this.scaleMax, 10000);
        const fctResRounded = Math.round(fctRes * (1 / scaleOutputPrecision)) / (1 / scaleOutputPrecision);
        resultValues.push(fctResRounded);
        this.indicatorTestValuesOutput.unshift({ values: resultValues, saved: false }); // add new row
        this.solutionCount++;
        return; // solution found
      }
      // solution already exists or invalid solution, continue searching
    }
    this.displayNoNewSolution();
  }

  calculateCustom(emptyIndexes: number[]) {
    if (this.openedStage < 0) {
      return;
    }
    const desiredValueRaw = this.stages[this.openedStage].value;
    const desiredValuePerc = (desiredValueRaw - this.scaleMin) / (this.scaleMax - this.scaleMin);
    let attemptCounter = 0;
    let solutionFound = false;
    while (attemptCounter < this.maxDeviationCorrections) {
      for (let i = 0; i < this.maxIterations; i++) {
        const overOrUnder = this.indicators.map(() => Math.random() < 0.5); // random booleans
        // random values
        const randomPercentages = this.indicators.map(() => Math.random());
        const result = [];
        for (let j = 0; j < this.indicators.length; j++) {
          if (!emptyIndexes.includes(j)) {
            result[j] = this.indicatorTestValuesInput[j];
          } else {
            result[j] = null;
          }
        }
        for (const emptyIdx of emptyIndexes) {
          if (overOrUnder[emptyIdx]) {
            // over
            result[emptyIdx] = desiredValuePerc + (1 - desiredValuePerc) * randomPercentages[emptyIdx];
          } else {
            // under
            result[emptyIdx] = desiredValuePerc - desiredValuePerc * randomPercentages[emptyIdx];
          }
        }
        let impossible = false;
        const resultValues = result.map((val, idx) => {
          if (!emptyIndexes.includes(idx)) {
            return val;
          }
          const min = this.getIndicatorMin(idx);
          const max = this.getIndicatorMax(idx);
          let res = this.percentageToValue(val, min, max);

          res =
            Math.round(res * (1 / this.precisionIndexToAbs(this.indicatorPrecisionIndexes[idx]))) /
            (1 / this.precisionIndexToAbs(this.indicatorPrecisionIndexes[idx]));

          // check if in range
          if (res < min) {
            // not in range (under)
            res += this.precisionIndexToAbs(this.indicatorPrecisionIndexes[idx]);
          } else if (res > max) {
            // not in range (over)
            res -= this.precisionIndexToAbs(this.indicatorPrecisionIndexes[idx]);
          }

          if (res < min || res > max) {
            // there exists no rounded value with this precision
            impossible = true;
            return this.percentageToValue(val, min, max);
          }
          // fix rounding error
          return this.fixRoundingError(res);
        });
        if (impossible) {
          this.displayNoSolution();
          return;
        }
        const fct = getIndicatorAggregationFunction(this.indicators, {
          formula: this.customAggregationFormula,
          globalVariables: this.globalVariables.map,
        });
        const fctRes = fct(
          resultValues.map(x => [x]),
          true,
        ); // encapsulate indicator values (all indicators act like numerical in this module)
        const fctResPerc = (fctRes - this.scaleMin) / (this.scaleMax - this.scaleMin);
        const resultDeviation = Math.abs(desiredValuePerc - fctResPerc);
        if (this.fixRoundingError(resultDeviation) <= this.scalePrecision) {
          solutionFound = true;
          if (this.isNewSolution(resultValues)) {
            const scaleOutputPrecision = this.findScalePrecision(this.scaleMin, this.scaleMax, 10000);
            const fctResRounded = Math.round(fctRes * (1 / scaleOutputPrecision)) / (1 / scaleOutputPrecision);
            resultValues.push(fctResRounded);
            this.indicatorTestValuesOutput.unshift({ values: resultValues, saved: false }); // add new row
            this.solutionCount++;
            return;
          }
        }
      }
      // no solution for 10 000 iterations with this maxDeviation (change maxDeviation & keep trying)
      // or solution already exists (keep trying)
      attemptCounter++;
    }
    if (!solutionFound) {
      // no solution found
      this.displayNoSolution();
    } else {
      // no UNIQUE solution found
      this.displayNoNewSolution();
    }
  }

  getRoundedRandom(bottom: number, top: number, idx: number) {
    bottom = this.fixRoundingError(bottom);
    top = this.fixRoundingError(top);
    const min = this.getIndicatorMin(idx);
    const max = this.getIndicatorMax(idx);
    const randomNumber = Math.random();
    const percentage = this.percentageToValue(randomNumber, bottom, top);
    let value = this.percentageToValue(percentage, this.indicators[idx].min, this.indicators[idx].max);
    value =
      Math.round(value * (1 / this.precisionIndexToAbs(this.indicatorPrecisionIndexes[idx]))) /
      (1 / this.precisionIndexToAbs(this.indicatorPrecisionIndexes[idx]));
    let roundedPercentage = this.valueToPercentage(value, this.indicators[idx].min, this.indicators[idx].max);

    // check if in range
    if (roundedPercentage < bottom) {
      // not in range (under)
      roundedPercentage = this.valueToPercentage(value + this.precisionIndexToAbs(this.indicatorPrecisionIndexes[idx]), min, max);
    } else if (roundedPercentage > top) {
      // note in range (over)
      roundedPercentage = this.valueToPercentage(value - this.precisionIndexToAbs(this.indicatorPrecisionIndexes[idx]), min, max);
    }

    if (roundedPercentage < bottom || roundedPercentage > top) {
      // there exists no rounded value with this precision
      return null;
    }

    return roundedPercentage;
  }

  findScalePrecision(lowerBound: number, upperBound: number, numberOfPrettyNumbers: number) {
    let precision = 1;
    for (let i = 0; i < this.maxDecimalPoints; i++) {
      if (Math.min(lowerBound, upperBound) + precision * numberOfPrettyNumbers <= Math.max(lowerBound, upperBound)) {
        return precision;
      } else {
        precision /= 10;
      }
    }
    return 1;
  }

  findMinPrecisionForRange(lowerBound: number, upperBound: number) {
    if (lowerBound > upperBound) {
      // makke sure lB <= uB
      const temp = upperBound;
      upperBound = lowerBound;
      lowerBound = temp;
    }
    let precision = this.maxIndPrecision;
    let idx = 0;
    while (precision >= this.minIndPrecision) {
      if (Math.floor(upperBound - lowerBound) / precision >= this.minNumberOfStages) {
        return precision;
      } else {
        if (idx % 2 == 0) {
          precision /= 2;
        } else {
          precision /= 5;
        }
      }
      idx++;
    }
    return 1;
  }

  isNewSolution(result: number[]) {
    for (let i = 0; i < this.indicatorTestValuesOutput.length; i++) {
      let isDifferent = false;
      for (let j = 0; j < this.indicators.length; j++) {
        if (!this.areIndicatorsPresent[j]) {
          // indicator plays no role
          continue;
        }
        if (this.indicatorTestValuesOutput[i].values[j] !== result[j]) {
          isDifferent = true;
          break;
        }
      }
      if (!isDifferent) {
        return false;
      }
    }
    return true;
  }

  getIndicatorMin(idx: number) {
    return Math.min(this.indicators[idx].min, this.indicators[idx].max);
  }

  getIndicatorMax(idx: number) {
    return Math.max(this.indicators[idx].min, this.indicators[idx].max);
  }

  valueToPercentage(value: number, min: number, max: number) {
    return (value - min) / (max - min);
  }

  percentageToValue(percentage: number, min: number, max: number) {
    return min + percentage * (max - min);
  }

  isInputFull() {
    return this.indicatorTestValuesInput.filter(x => x != null).length === this.indicatorTestValuesInput.length;
  }

  isInputIncorrect() {
    return (
      this.indicatorTestValuesInput.filter(
        (val, idx) => val != null && (val < this.getIndicatorMin(idx) || val > this.getIndicatorMax(idx)),
      ).length > 0
    );
  }

  closeAllErrors() {
    this.showNoSolution = false;
    this.showNoNewSolution = false;
  }

  getLastDecimal(n: number) {
    const asString = n.toString();
    return Number.parseInt(asString.charAt(asString.length - 1));
  }

  getFirstDigit(n: number) {
    const asString = n.toString();
    return Number.parseInt(asString.charAt(0));
  }

  increasePrecision(idx: number) {
    if (this.indicatorPrecisionIndexes[idx] > 0) {
      this.indicatorPrecisionIndexes[idx]--;
    }
  }

  decreasePrecision(idx: number) {
    if (this.indicatorPrecisionIndexes[idx] < this.maxIndicatorPrecisionIndexes[idx]) {
      this.indicatorPrecisionIndexes[idx]++;
    }
  }

  increaseScalePrecision() {
    if (this.scalePrecision <= this.minScalePrecision) {
      return;
    }
    if (this.scalePrecision === 0.01) {
      this.scalePrecision = 0.005;
    } else if (this.scalePrecision === 0.005) {
      this.scalePrecision = 0.001;
    } else if (this.scalePrecision === 0.001) {
      this.scalePrecision = 0;
    } else {
      this.scalePrecision -= 0.01;
    }
    // fix rounding error
    this.scalePrecision = this.fixRoundingError(this.scalePrecision);
  }

  decreaseScalePrecision() {
    if (this.scalePrecision >= this.maxScalePrecision) {
      return;
    }
    if (this.scalePrecision === 0) {
      this.scalePrecision = 0.001;
    } else if (this.scalePrecision === 0.001) {
      this.scalePrecision = 0.005;
    } else if (this.scalePrecision === 0.005) {
      this.scalePrecision = 0.01;
    } else {
      this.scalePrecision += 0.01;
    }
    // fix rounding error
    this.scalePrecision = this.fixRoundingError(this.scalePrecision);
  }

  precisionIndexToAbs(idx: number) {
    let precision = this.minIndPrecision;
    precision *= Math.pow(5, Math.ceil(idx / 2)) * Math.pow(2, Math.floor(idx / 2));
    precision = this.fixRoundingError(precision);
    return Math.min(precision, this.maxIndPrecision);
  }

  precisionAbsToIndex(precision: number) {
    let tempPrecision = precision;
    let idx = 0;
    // either first or last digit is 1 or 5 (otherwise 0)
    const digitCondition = Math.max(this.getLastDecimal(precision), this.getFirstDigit(precision));
    let divider = digitCondition === 5 ? 5 : 2;
    while (tempPrecision > this.minIndPrecision) {
      tempPrecision = this.fixRoundingError(tempPrecision / divider);
      if (idx % 2 === 0) {
        divider = 5;
      } else {
        divider = 2;
      }
      idx++;
    }
    return idx;
  }

  findNextProperValue(value: number) {
    let properValue = this.maxIndPrecision;
    let idx = 0;
    while (properValue > Math.max(this.minIndPrecision, value)) {
      if (idx % 2 === 0) {
        properValue /= 2;
      } else {
        properValue /= 5;
      }
      idx++;
    }
    return properValue;
  }

  saveCombination(outputRowIdx: number) {
    if (this.indicatorTestValuesOutput[outputRowIdx].saved) {
      // saved already
      return;
    }
    this.indicatorTestValuesOutput[outputRowIdx].saved = true;
    const combination = this.indicatorTestValuesOutput[outputRowIdx].values.slice(0, this.indicators.length);
    const combinationEdited = [];
    for (let i = 0; i < combination.length; i++) {
      if (!this.areIndicatorsPresent[i]) {
        // indicator plays no role
        continue;
      }
      combinationEdited.push(this.indicators[i].name + ': ' + formatNumber(combination[i], this.locale, '1.0-' + this.maxDecimalPoints));
    }
    const description = this.stages[this.openedStage].description;
    const input = '[' + combinationEdited.join('; ') + ']';
    this.stages[this.openedStage].description = appendRichText(description, input);
  }

  displayNoSolution() {
    if (!this.showNoSolution) {
      this.showNoSolution = true;
      setTimeout(() => {
        this.showNoSolution = false;
      }, 5000);
    }
  }

  displayNoNewSolution() {
    if (!this.showNoNewSolution) {
      this.showNoNewSolution = true;
      setTimeout(() => {
        this.showNoNewSolution = false;
      }, 5000);
    }
  }

  getColor(row: number, col: number) {
    if (col === this.indicators.length) {
      const desiredValue = this.stages[this.openedStage]?.value;
      const actualValue = this.indicatorTestValuesOutput[row].values[col];
      const difference = Math.abs(actualValue - desiredValue);
      const differencePerc = Math.min(difference / Math.abs(this.scaleMin - this.scaleMax), 1);
      const colorVal = Math.round(Math.floor(differencePerc * 1000) * 1.5);
      return 'rgb(' + colorVal + ', ' + colorVal + ', ' + colorVal + ')';
    }
    return;
  }
}
