import { ComponentFixture, TestBed } from '@angular/core/testing';
import { GlobalVariables, Indicator, IndicatorObjectiveStage } from '@entscheidungsnavi/decision-data';
import { CombinationComponent } from './combination-component.component';

describe('CombinationComponent', () => {
  let component: CombinationComponent;
  let fixture: ComponentFixture<CombinationComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [CombinationComponent],
    });

    fixture = TestBed.createComponent(CombinationComponent);
    component = fixture.componentInstance;
  });

  describe('should not crash for verbal indicator', () => {
    let descriptionStages: IndicatorObjectiveStage[];
    let indicator: Indicator;

    beforeEach(() => {
      descriptionStages = [
        {
          value: 1,
          description: '',
        },
        {
          value: 3,
          description: '',
        },
        {
          value: 5,
          description: '',
        },
      ];

      indicator = new Indicator('indicator', 1, 5);

      indicator.verbalIndicatorCategories = [
        {
          name: 'category',
          weight: 1,
          stages: [
            {
              name: 'Stage 1',
              description: 'Description',
            },
            {
              name: 'Stage 2',
              description: 'Description',
            },
            {
              name: 'Stage 3',
              description: 'Description',
            },
          ],
        },
      ];

      fixture.componentRef.setInput('indicators', [indicator]);
      fixture.componentRef.setInput('stages', descriptionStages);
      fixture.componentRef.setInput('openedStage', 1);
      fixture.componentRef.setInput('scaleMin', 1);
      fixture.componentRef.setInput('scaleMax', 5);
    });

    it('with default aggregation', () => {
      fixture.componentRef.setInput('isAdditive', true);

      expect(() => fixture.detectChanges()).not.toThrow();

      expect(component.indicatorTestValuesOutput.length).toBeGreaterThan(0);
      expect(component.indicatorTestValuesOutput.length).toBe(component.solutionCount);

      component.indicatorTestValuesOutput.forEach(solution => {
        expect(solution.values.length).toBe(2);
        expect(solution.values[0]).toBe(solution.values[1]);
      });
    });

    it('with custom aggregation', () => {
      fixture.componentRef.setInput('isAdditive', false);
      fixture.componentRef.setInput('customAggregationFormula', 'Ind1');
      fixture.componentRef.setInput('globalVariables', new GlobalVariables());

      expect(() => fixture.detectChanges()).not.toThrow();

      expect(component.indicatorTestValuesOutput.length).toBeGreaterThan(0);
      expect(component.indicatorTestValuesOutput.length).toBe(component.solutionCount);

      component.indicatorTestValuesOutput.forEach(solution => {
        expect(solution.values.length).toBe(2);
        expect(solution.values[0]).toBe(solution.values[1]);
      });
    });
  });
});
