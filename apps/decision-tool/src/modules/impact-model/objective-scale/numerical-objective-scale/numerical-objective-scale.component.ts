import { ChangeDetectionStrategy, Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormControl, FormGroup, NonNullableFormBuilder, Validators } from '@angular/forms';
import { NumericalObjectiveData } from '@entscheidungsnavi/decision-data';
import { OnDestroyObservable } from '@entscheidungsnavi/tools';
import { cloneDeep, isEqual } from 'lodash';
import { Observable, distinctUntilChanged, map, startWith, takeUntil } from 'rxjs';

@Component({
  selector: 'dt-numerical-objective-scale',
  templateUrl: './numerical-objective-scale.component.html',
  styleUrls: ['./numerical-objective-scale.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class NumericalObjectiveScaleComponent implements OnInit {
  @Input() numericalData: NumericalObjectiveData;
  @Output() isValid = new EventEmitter<boolean>();
  @Output() isDirty = new EventEmitter<boolean>();

  form: ReturnType<typeof this.getFormGroup>;

  private initialValue: typeof this.form.value;

  @OnDestroyObservable()
  private onDestroy$: Observable<void>;

  constructor(private fb: NonNullableFormBuilder) {}

  ngOnInit() {
    this.form = this.getFormGroup();
    this.initialValue = cloneDeep(this.form.value);

    this.form.statusChanges.pipe(takeUntil(this.onDestroy$)).subscribe(status => this.isValid.emit(status === 'VALID'));
    this.isValid.emit(this.form.valid);

    this.form.valueChanges
      .pipe(
        startWith(null),
        map(() => this.checkHasChanges()),
        distinctUntilChanged(),
        takeUntil(this.onDestroy$),
      )
      .subscribe(isDirty => this.isDirty.emit(isDirty));
  }

  save() {
    this.numericalData.from = this.form.value.from.value;
    this.numericalData.commentFrom = this.form.value.from.comment;
    this.numericalData.to = this.form.value.to.value;
    this.numericalData.commentTo = this.form.value.to.comment;
    this.numericalData.unit = this.form.value.unit;
  }

  private checkHasChanges() {
    return !isEqual(this.initialValue, this.form.value);
  }

  private getFormGroup() {
    return this.fb.group(
      {
        from: this.fb.group({
          value: [this.numericalData.from, Validators.required],
          comment: [this.numericalData.commentFrom],
        }),
        to: this.fb.group({
          value: [this.numericalData.to, Validators.required],
          comment: [this.numericalData.commentTo],
        }),
        unit: [this.numericalData.unit, Validators.maxLength(10)],
      },
      { validators: this.validateInterval },
    );
  }

  private validateInterval(
    group: FormGroup<{ from: FormGroup<{ value: FormControl<number> }>; to: FormGroup<{ value: FormControl<number> }> }>,
  ) {
    const from = group.value.from.value,
      to = group.value.to.value;
    if (from === to) {
      return { collapsedInterval: true };
    }
    return null;
  }
}
