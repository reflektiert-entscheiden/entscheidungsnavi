import { Component, Inject, OnInit, ViewChild } from '@angular/core';
import { ConfirmModalComponent, ConfirmModalData } from '@entscheidungsnavi/widgets';
import { DecisionData, ObjectiveType } from '@entscheidungsnavi/decision-data';
import { MAT_DIALOG_DATA, MatDialog, MatDialogRef } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { firstValueFrom } from 'rxjs';
import { showManualTradeoffNotification } from '../../shared/manual-tradeoff-notification';
import { DecisionDataState } from '../../shared/decision-data-state';
import { IndicatorObjectiveScaleComponent, NumericalObjectiveScaleComponent, VerbalObjectiveScaleComponent } from '.';

@Component({
  templateUrl: './objective-scale-modal.component.html',
  styleUrls: ['./objective-scale-modal.component.scss'],
})
export class ObjectiveScaleModalComponent implements OnInit {
  get objective() {
    return this.decisionData.objectives[this.data.objectiveIdx];
  }

  @ViewChild(NumericalObjectiveScaleComponent) numericalScale: NumericalObjectiveScaleComponent;
  @ViewChild(VerbalObjectiveScaleComponent) verbalScale: VerbalObjectiveScaleComponent;
  @ViewChild(IndicatorObjectiveScaleComponent) indicatorScale: IndicatorObjectiveScaleComponent;

  objectiveType: ObjectiveType;
  comment: string;
  scaleComment: string;

  // eslint-disable-next-line @typescript-eslint/naming-convention
  ObjectiveType = ObjectiveType;

  numericalIsValid: boolean;
  verbalIsValid: boolean;
  indicatorIsValid: boolean;

  get isValid() {
    switch (this.objectiveType) {
      case ObjectiveType.Numerical:
        return this.numericalIsValid;
      case ObjectiveType.Verbal:
        return this.verbalIsValid;
      case ObjectiveType.Indicator:
        return this.indicatorIsValid;
    }
  }

  numericalIsDirty: boolean;
  verbalIsDirty: boolean;
  indicatorIsDirty: boolean;

  get scaleIsDirty() {
    switch (this.objectiveType) {
      case ObjectiveType.Numerical:
        return this.numericalIsDirty;
      case ObjectiveType.Verbal:
        return this.verbalIsDirty;
      case ObjectiveType.Indicator:
        return this.indicatorIsDirty;
    }
  }

  get hasObjectiveTypeChanged() {
    return this.objectiveType !== this.objective.objectiveType;
  }

  get isDirty() {
    return this.hasObjectiveTypeChanged || this.comment !== this.objective.comment || this.scaleIsDirty;
  }

  get readonly() {
    return this.decisionDataState.isProjectReadonly;
  }

  allOutcomesEmpty: boolean;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: { objectiveIdx: number },
    private dialogRef: MatDialogRef<ObjectiveScaleModalComponent>,
    private decisionData: DecisionData,
    private dialog: MatDialog,
    private snackBar: MatSnackBar,
    private decisionDataState: DecisionDataState,
  ) {}

  ngOnInit() {
    this.allOutcomesEmpty = this.decisionData.outcomes.every(ocForAlternative => ocForAlternative[this.data.objectiveIdx].isEmpty);
    this.load();
  }

  close(save: boolean) {
    if (save) {
      this.save();
    }
    this.dialogRef.close(save);
  }

  async closeClick() {
    if (
      !this.isDirty ||
      (await firstValueFrom(
        this.dialog
          .open(ConfirmModalComponent, {
            data: {
              title: $localize`Änderungen verwerfen`,
              prompt: $localize`Bist Du sicher, dass Du die ungespeicherten Änderungen an der Zielskala verwerfen willst?`,
              template: 'discard',
            } as ConfirmModalData,
          })
          .afterClosed(),
      ))
    ) {
      this.close(false);
    }
  }

  private load() {
    this.objectiveType = this.objective.objectiveType;
    this.comment = this.objective.comment;
    this.scaleComment = this.objective.scaleComment;
  }

  private save() {
    const isObjectiveTypeChanged = this.objective.objectiveType !== this.objectiveType;
    if (isObjectiveTypeChanged && this.decisionData.changeObjectiveType(this.data.objectiveIdx, this.objectiveType)) {
      showManualTradeoffNotification(this.snackBar);
    }
    this.objective.comment = this.comment;
    this.objective.scaleComment = this.scaleComment;
    switch (this.objectiveType) {
      case ObjectiveType.Numerical:
        this.numericalScale.save();
        break;
      case ObjectiveType.Verbal:
        this.verbalScale.save();
        break;
      case ObjectiveType.Indicator:
        this.indicatorScale.save(isObjectiveTypeChanged);
        break;
    }
  }
}
