import { CdkDragDrop } from '@angular/cdk/drag-drop';
import {
  ChangeDetectionStrategy,
  Component,
  DestroyRef,
  ElementRef,
  EventEmitter,
  Input,
  OnInit,
  Output,
  QueryList,
  TemplateRef,
  ViewChild,
  ViewChildren,
} from '@angular/core';
import { NonNullableFormBuilder, Validators } from '@angular/forms';
import { DecisionData, VerbalObjectiveData } from '@entscheidungsnavi/decision-data';
import { PopOverRef, PopOverService } from '@entscheidungsnavi/widgets';
import { cloneDeep, isEqual, zip } from 'lodash';
import { distinctUntilChanged, map, startWith } from 'rxjs';
import { takeUntilDestroyed } from '@angular/core/rxjs-interop';
import { mergeBackArrayCopy } from '@entscheidungsnavi/tools';

@Component({
  selector: 'dt-verbal-objective-scale',
  templateUrl: './verbal-objective-scale.component.html',
  styleUrls: ['./verbal-objective-scale.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class VerbalObjectiveScaleComponent implements OnInit {
  @Input({ required: true }) objectiveIndex: number;
  @Input({ required: true }) verbalData: VerbalObjectiveData;
  @Output() isValid = new EventEmitter<boolean>();
  @Output() isDirty = new EventEmitter<boolean>();

  initParamsForm: ReturnType<typeof this.getInitParamsForm>;
  statesForm: ReturnType<typeof this.getStatesForm>;

  private initialValue: typeof this.statesForm.value;

  @ViewChild('initParamsPopover') initParamsPopover: TemplateRef<any>;
  @ViewChild('initParamsOpener', { read: ElementRef }) initParamsOpener: ElementRef;
  openPopover: PopOverRef;

  @ViewChildren('outcomeInput')
  outcomeInputs: QueryList<ElementRef<HTMLInputElement>>;

  constructor(
    private decisionData: DecisionData,
    private fb: NonNullableFormBuilder,
    private popover: PopOverService,
    private destroyRef: DestroyRef,
  ) {}

  ngOnInit() {
    this.initParamsForm = this.getInitParamsForm();
    this.statesForm = this.getStatesForm();
    this.initialValue = cloneDeep(this.statesForm.value);

    // We do not care about the validity of the init params
    this.statesForm.statusChanges.pipe(takeUntilDestroyed(this.destroyRef)).subscribe(status => this.isValid.emit(status === 'VALID'));
    this.isValid.emit(this.statesForm.valid);

    this.statesForm.valueChanges
      .pipe(
        startWith(null),
        map(() => this.checkHasChanges()),
        distinctUntilChanged(),
        takeUntilDestroyed(this.destroyRef),
      )
      .subscribe(hasChanges => this.isDirty.emit(hasChanges));

    if (this.statesForm.value.states.length === 0) {
      this.applyInitParams();
    }
  }

  private checkHasChanges() {
    return !isEqual(this.initialValue, this.statesForm.value);
  }

  save() {
    this.verbalData.initParams.from = this.initParamsForm.value.from;
    this.verbalData.initParams.to = this.initParamsForm.value.to;
    this.verbalData.initParams.stepNumber = this.initParamsForm.value.stepNumber;

    mergeBackArrayCopy(
      this.statesForm.value.states.map(({ originalPosition, comment, name }) => [originalPosition, { name, comment }]),
      this.verbalData.optionCount(),
      (position, element) => {
        this.decisionData.addObjectiveOption(this.objectiveIndex, position, element.name);
        this.verbalData.comments[position] = element.comment;
      },
      (position, element) => {
        this.verbalData.options[position] = element.name;
        this.verbalData.comments[position] = element.comment;
      },
      position => {
        this.decisionData.removeObjectiveOption(this.objectiveIndex, position);
      },
      (fromPosition, toPosition) => {
        this.decisionData.moveObjectiveOption(this.objectiveIndex, fromPosition, toPosition);
      },
    );
  }

  enter(index: number) {
    if (index !== this.statesForm.controls.states.length - 1) {
      this.outcomeInputs.get(index + 1).nativeElement.focus();
    } else if (this.statesForm.controls.states.length < 7) {
      this.addState();
      setTimeout(() => {
        this.outcomeInputs.last.nativeElement.focus();
      });
    }
  }

  openInitParams() {
    this.openPopover = this.popover.open(this.initParamsPopover, this.initParamsOpener, {
      position: [{ originX: 'center', originY: 'top', overlayX: 'center', overlayY: 'bottom' }],
    });
  }

  applyInitParams() {
    if (!this.initParamsForm.valid) {
      return;
    }

    const from: string = this.initParamsForm.value.from,
      to: string = this.initParamsForm.value.to;
    const optionNameTemplates = [
      $localize`sehr ${from}`,
      from,
      $localize`eher ${from}`,
      $localize`mittel`,
      $localize`eher ${to}`,
      to,
      $localize`sehr ${to}`,
    ];

    let optionIndices: number[] = [];
    switch (this.initParamsForm.value.stepNumber) {
      case 2:
        optionIndices = [1, 5];
        break;
      case 3:
        optionIndices = [1, 3, 5];
        break;
      case 4:
        optionIndices = [1, 2, 4, 5];
        break;
      case 5:
        optionIndices = [1, 2, 3, 4, 5];
        break;
      case 6:
        optionIndices = [0, 1, 2, 4, 5, 6];
        break;
      case 7:
        optionIndices = [0, 1, 2, 3, 4, 5, 6];
        break;
    }
    const newOptionNames = optionIndices.map(index => optionNameTemplates[index]);

    const statesFormArray = this.statesForm.controls.states;
    statesFormArray.clear();
    newOptionNames.forEach(option => statesFormArray.push(this.getStateForm(option, '')));
  }

  addState() {
    this.statesForm.controls.states.push(this.getStateForm('', ''));
  }

  deleteState(index: number) {
    this.statesForm.controls.states.removeAt(index);
  }

  private getInitParamsForm() {
    return this.fb.group({
      from: [this.verbalData.initParams.from ?? $localize`gering`, Validators.required],
      to: [this.verbalData.initParams.to ?? $localize`hoch`, Validators.required],
      stepNumber: [this.verbalData.initParams.stepNumber, [Validators.required, Validators.min(2), Validators.max(7)]],
    });
  }

  private getStatesForm() {
    const statesFormArray = this.fb.array(
      zip(this.verbalData.options, this.verbalData.comments).map(([name, comment], position) => this.getStateForm(name, comment, position)),
    );
    return this.fb.group({ states: statesFormArray });
  }

  private getStateForm(name: string, comment: string, originalPosition = -1) {
    return this.fb.group({
      name: [name, Validators.required],
      comment: [comment],
      originalPosition: [originalPosition],
    });
  }

  drop(event: CdkDragDrop<unknown>) {
    const value = this.statesForm.controls.states.at(event.previousIndex);
    this.statesForm.controls.states.removeAt(event.previousIndex, { emitEvent: false });
    this.statesForm.controls.states.insert(event.currentIndex, value);
  }

  getColorForRank(index: number) {
    return `rgb(${200 * (1 - index / this.statesForm.controls.states.length)}, ${
      200 * (index / this.statesForm.controls.states.length)
    }, 0)`;
  }
}
