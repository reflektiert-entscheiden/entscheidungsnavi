import { CdkDragDrop, moveItemInArray } from '@angular/cdk/drag-drop';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  EventEmitter,
  Input,
  OnInit,
  Output,
  QueryList,
  ViewChild,
  ViewChildren,
} from '@angular/core';
import {
  FormControl,
  FormGroup,
  UntypedFormArray,
  UntypedFormBuilder,
  UntypedFormControl,
  UntypedFormGroup,
  ValidatorFn,
  Validators,
} from '@angular/forms';
import {
  DecisionData,
  getIndicatorAggregationFunction,
  getIndicatorCoefficientName,
  getIndicatorValueName,
  Indicator,
  IndicatorObjectiveData,
  IndicatorObjectiveStage,
  isEqualIndicatorObjectiveStage,
  Objective,
  ObjectiveInput,
  VerbalIndicatorCategory,
} from '@entscheidungsnavi/decision-data';
import { MatDialog } from '@angular/material/dialog';
import { ArrayCopy, LocationSequence, OnDestroyObservable, Tree } from '@entscheidungsnavi/tools';
import { matchParentError, PopOverService, SimpleHierarchyNode } from '@entscheidungsnavi/widgets';
import { cloneDeep, isEqual, omit, range, zip } from 'lodash';
import { merge, Observable } from 'rxjs';
import { parse } from 'mathjs';
import { IndicatorDescriptionModalComponent } from '../indicator-description-modal/indicator-description-modal.component';
import {
  VerbalIndicatorDefinitionModalComponent,
  VerbalIndicatorDefinitionModalData,
} from './verbal-indicator-definition-modal/verbal-indicator-definition-modal.component';
import { IndicatorAggregationExplanationModalComponent } from './indicator-aggregation-explanation-modal.component';

@Component({
  selector: 'dt-indicator-objective-scale',
  templateUrl: './indicator-objective-scale.component.html',
  styleUrls: ['./indicator-objective-scale.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class IndicatorObjectiveScaleComponent implements OnInit {
  @OnDestroyObservable()
  private onDestroy$: Observable<void>;

  @Input() objectiveIdx: number;
  @Input() indicatorData: IndicatorObjectiveData;
  @Output() isValid = new EventEmitter<boolean>();
  @Output() isDirty = new EventEmitter<boolean>();

  oldFormValue: any;
  formChanged = false;

  @ViewChild('hierarchyContainer', { static: true })
  hierarchyContainer: ElementRef<HTMLElement>;

  @ViewChildren('nameInput')
  nameInputs: QueryList<ElementRef<HTMLInputElement>>;

  // This is used to write the indicators back later. Simply changing the indicators array is not enough, we
  // also need to update the outcomes.
  indicators: ArrayCopy<Indicator>;
  objectives: Objective[];

  form: UntypedFormGroup;
  indicatorsForm: UntypedFormArray;
  verbalIndicatorListCollapsed: boolean[] = [];

  aggregationFormulaMin: number;
  aggregationFormulaMax: number;

  stages: IndicatorObjectiveStage[];
  stagesChanged = false;

  // keeps track of worst/best after each convertStages()
  lastWorst: number;
  lastBest: number;

  objectiveTree: Tree<SimpleHierarchyNode>;

  // outcome.verbalIndicatorValues for each outcome
  // used to update outcome values when the order of existing verbal categories and their stages changes
  outcomeValues: ObjectiveInput[][]; // [alternativeIdx][stageIdx][indicatorIdx][categoryIdx]

  customAggregationFormulaLatex = '';

  readonly roundingErrorPrecision = 0.0000000001;

  get isFormulaComplete() {
    if (!this.form.get('useCustomAggregation').value) {
      // additive formula is always complete
      return true;
    }
    for (let i = 0; i < this.indicators.length; i++) {
      if (!this.form.get('customAggregationFormula').value.includes(getIndicatorValueName(i))) {
        // missing indicator
        return false;
      }
    }
    // custom formula, complete
    return true;
  }

  get isScaleChanged() {
    return !this.isRangeUnchanged && !this.hasAggregationTypeChanged && (!this.areStagesNew || (this.areStagesNew && this.stagesChanged));
  }

  get areStagesNew() {
    return this.stages.every(stage => {
      return stage.description === '';
    });
  }

  get isFormulaChanged() {
    return this.hasAggregationTypeChanged && (!this.areStagesNew || (this.areStagesNew && this.stagesChanged));
  }

  get isRangeUnchanged() {
    // either additive formula with different worst/best
    // or custom formula with different worst/best
    return (
      ((!this.form.get('useCustomAggregation').value || !this.form.get('automaticCustomAggregationLimits')) &&
        this.indicatorData.defaultAggregationWorst === this.form.get('range.worst').value &&
        this.indicatorData.defaultAggregationBest === this.form.get('range.best').value) ||
      (this.form.get('useCustomAggregation').value &&
        this.calculateAggregationFunctionBounds('min', this.indicatorData.customAggregationFormula) ===
          this.calculateAggregationFunctionBounds('min', this.form.get('customAggregationFormula').value) &&
        this.calculateAggregationFunctionBounds('max', this.indicatorData.customAggregationFormula) ===
          this.calculateAggregationFunctionBounds('max', this.form.get('customAggregationFormula').value))
    );
  }

  get hasAggregationTypeChanged() {
    return this.indicatorData.useCustomAggregation !== this.form.get('useCustomAggregation').value;
  }

  get defaultAggregationTex() {
    const addBracketOnNegative = (input: number) => (input < 0 ? `(${input})` : `${input}`);

    return (
      'f(' +
      range(this.indicatorsForm.length)
        .map(index => getIndicatorValueName(index))
        .join(',') +
      ') = ' +
      this.form.get('range.worst').value +
      ' + (' +
      this.form.get('range.best').value +
      ' - ' +
      this.form.get('range.worst').value +
      ') * \\cfrac{' +
      this.indicatorsForm.controls
        .map((indicatorControl, index) => {
          const min = addBracketOnNegative(indicatorControl.get('min').value);
          const max = addBracketOnNegative(indicatorControl.get('max').value);
          return `${getIndicatorCoefficientName(index)} * \\cfrac{${getIndicatorValueName(index)} - ${min}}{${max} - ${min}}`;
        })
        .join(' + ') +
      '}{' +
      range(this.indicatorsForm.length)
        .map(index => getIndicatorCoefficientName(index))
        .join(' + ') +
      '}'
    );
  }

  constructor(
    private fb: UntypedFormBuilder,
    private decisionData: DecisionData,
    private dialog: MatDialog,
    private cdRef: ChangeDetectorRef,
    private popOverService: PopOverService,
  ) {}

  ngOnInit() {
    this.indicators = ArrayCopy.createArrayCopy(this.indicatorData.indicators, indicator => indicator.clone());
    this.objectives = this.decisionData.objectives;
    this.verbalIndicatorListCollapsed = new Array(this.indicators.length).fill(false);
    this.indicatorsForm = this.fb.array(this.indicatorData.indicators.map(this.getIndicatorForm.bind(this)));
    this.stages = cloneDeep(this.indicatorData.stages);

    if (this.indicatorData.useCustomAggregation && this.indicatorData.automaticCustomAggregationLimits) {
      this.lastWorst = this.calculateAggregationFunctionBounds('min', this.indicatorData.customAggregationFormula);
      this.lastBest = this.calculateAggregationFunctionBounds('max', this.indicatorData.customAggregationFormula);
    } else {
      this.lastWorst = this.indicatorData.defaultAggregationWorst;
      this.lastBest = this.indicatorData.defaultAggregationBest;
    }

    this.outcomeValues = this.decisionData.outcomes.map(outcomesRow => {
      return cloneDeep(outcomesRow[this.objectiveIdx].values);
    });

    this.form = this.fb.group({
      indicators: this.indicatorsForm,
      aggregatedUnit: [this.indicatorData.aggregatedUnit, Validators.maxLength(10)],
      useCustomAggregation: [this.indicatorData.useCustomAggregation],
      automaticCustomAggregationLimits: [this.indicatorData.automaticCustomAggregationLimits],
      customAggregationFormula: [
        this.indicatorData.customAggregationFormula,
        { validators: [Validators.required, this.validateCustomFormula.bind(this)] },
      ],
      range: this.fb.group(
        {
          worst: [this.indicatorData.defaultAggregationWorst, Validators.required],
          best: [this.indicatorData.defaultAggregationBest, Validators.required],
        },
        { validators: this.validateRange.bind(this) as ValidatorFn },
      ),
    });

    this.oldFormValue = this.form.value;

    // Changes in the indicator scales might make the aggregation formula invalid
    merge(this.indicatorsForm.valueChanges, this.form.get('automaticCustomAggregationLimits').valueChanges).subscribe(() => {
      this.form.get('customAggregationFormula').markAsTouched();
      this.form.get('customAggregationFormula').updateValueAndValidity();

      this.form.get('range').markAsTouched();
      this.form.get('range').updateValueAndValidity();
    });

    this.form.get('useCustomAggregation').valueChanges.subscribe(() => this.onCustomAggregationChange());
    this.onCustomAggregationChange();

    this.form.statusChanges.subscribe(status => this.isValid.emit(status === 'VALID'));
    this.isValid.emit(this.form.valid);

    this.form.valueChanges.subscribe(value => {
      this.formChanged = !isEqual(
        value.useCustomAggregation ? this.oldFormValue : omit(this.oldFormValue, ['customAggregationFormula']),
        value,
      );

      this.reportDirty();
    });

    const objective = this.decisionData.objectives[this.objectiveIdx];
    this.objectiveTree = new Tree<SimpleHierarchyNode>(
      {
        name: objective.name,
        comment: objective.comment,
        style: {
          'background-color': objective.aspects.value.backgroundColor ?? '#5d666f',
          color: objective.aspects.value.textColor ?? '#ffffff',
        },
      },
      objective.aspects.children?.map(child =>
        child.map(node => ({
          name: node.name,
          comment: node.comment,
          style: {
            'background-color': node.backgroundColor,
            color: node.textColor,
          },
        })),
      ),
    );

    this.form.get('customAggregationFormula').valueChanges.subscribe(() => {
      this.updateCustomFormulaLatex();
    });

    this.updateCustomFormulaLatex();
  }

  save(isObjectiveTypeChanged = false) {
    const hasAggregationTypeChanged = this.hasAggregationTypeChanged;

    this.indicatorData.aggregatedUnit = this.form.get('aggregatedUnit').value;
    this.indicatorData.useCustomAggregation = this.form.get('useCustomAggregation').value;
    this.indicatorData.automaticCustomAggregationLimits = this.form.get('automaticCustomAggregationLimits').value;
    this.indicatorData.customAggregationFormula = this.form.get('customAggregationFormula').value;
    this.indicatorData.defaultAggregationWorst = this.form.get('range.worst').value;
    this.indicatorData.defaultAggregationBest = this.form.get('range.best').value;

    let newWorst = this.indicatorData.defaultAggregationWorst;
    let newBest = this.indicatorData.defaultAggregationBest;
    if (this.indicatorData.useCustomAggregation && this.indicatorData.automaticCustomAggregationLimits) {
      newWorst = this.calculateAggregationFunctionBounds('min', this.form.get('customAggregationFormula').value);
      newBest = this.calculateAggregationFunctionBounds('max', this.form.get('customAggregationFormula').value);
    }

    this.convertStages(hasAggregationTypeChanged, newWorst, newBest);

    this.indicatorData.stages = cloneDeep(this.stages);

    // Write values back to the ArrayCopy...
    this.indicatorsForm.controls.forEach((indicatorGroup, index) => {
      const indicator = this.indicators.get(index);
      indicator.name = indicatorGroup.get('name').value;
      indicator.comment = indicatorGroup.get('comment').value;
      indicator.min = indicatorGroup.get('min').value;
      indicator.max = indicatorGroup.get('max').value;
      indicator.unit = indicatorGroup.get('unit').value;
      indicator.coefficient = indicatorGroup.get('weight').value;
      indicator.verbalIndicatorCategories = indicatorGroup.get('verbalIndicatorCategories').value;
    });

    // ...and then merge that back into the objective
    this.indicators.mergeBack(
      this.indicatorData.indicators,
      (position: number, element: Indicator) => {
        // add
        this.decisionData.addObjectiveIndicator(this.objectiveIdx, position);
        Object.assign(this.indicatorData.indicators[position], element);
      },
      (position: number, element: Indicator) => {
        // set
        Object.assign(this.indicatorData.indicators[position], element);
      },
      (position: number) => {
        // remove
        this.decisionData.removeObjectiveIndicator(this.objectiveIdx, position);
      },
      (fromPosition: number, toPosition: number) => {
        // move
        this.decisionData.moveObjectiveIndicator(this.objectiveIdx, fromPosition, toPosition);
      },
    );

    if (!isObjectiveTypeChanged) {
      // if objective type is not changed, set to this.outcomeValues
      this.decisionData.outcomes.forEach((_, alternativeIdx) => {
        this.decisionData.outcomes[alternativeIdx][this.objectiveIdx].values = cloneDeep(this.outcomeValues[alternativeIdx]);
      });
    }
  }

  private validateCustomFormula(control: UntypedFormControl) {
    this.aggregationFormulaMin = this.calculateAggregationFunctionBounds('min', control.value);
    this.aggregationFormulaMax = this.calculateAggregationFunctionBounds('max', control.value);

    if (isNaN(this.aggregationFormulaMin) || isNaN(this.aggregationFormulaMax)) {
      return { invalidFormula: true };
    }

    const useAutomaticLimits =
      this.form?.get('automaticCustomAggregationLimits').value ?? this.indicatorData.automaticCustomAggregationLimits;

    if (useAutomaticLimits && this.aggregationFormulaMin === this.aggregationFormulaMax) {
      return { collapsedInterval: true };
    } else {
      return null;
    }
  }

  private validateRange(group: UntypedFormGroup) {
    if (this.form?.get('automaticCustomAggregationLimits').value ?? this.indicatorData.automaticCustomAggregationLimits) {
      return null;
    }

    const worst = group.get('worst').value,
      best = group.get('best').value;
    if (worst === best) {
      return { collapsedInterval: true };
    }
    return null;
  }

  reportDirty() {
    this.isDirty.emit(this.formChanged || this.stagesChanged);
  }

  private onCustomAggregationChange() {
    // Update validators
    if (this.form.get('useCustomAggregation').value) {
      this.form.get('customAggregationFormula').enable();
    } else {
      this.form.get('customAggregationFormula').disable();
    }
  }

  private getIndicatorForm(indicator: Indicator) {
    return this.fb.group(
      {
        name: [indicator.name, Validators.required],
        comment: [indicator.comment],
        min: [indicator.min, Validators.required],
        max: [indicator.max, [Validators.required, matchParentError('minMaxCollapsed', this.onDestroy$)]],
        unit: [indicator.unit, Validators.maxLength(10)],
        weight: [indicator.coefficient, [Validators.required, Validators.min(0)]],
        verbalIndicatorCategories: [indicator.verbalIndicatorCategories],
      },
      {
        validators: (group: FormGroup<{ min: FormControl<number>; max: FormControl<number> }>) =>
          group.value.min != null && group.value.max != null && group.value.min === group.value.max ? { minMaxCollapsed: true } : null,
      },
    );
  }

  enter(index: number) {
    if (index !== this.indicators.length - 1) {
      this.nameInputs.get(index + 1).nativeElement.focus();
    } else {
      this.addIndicator();
      setTimeout(() => {
        this.nameInputs.last.nativeElement.focus();
      });
    }
  }

  addIndicator(name?: string) {
    const indicator = new Indicator(name);

    this.indicators.add(this.indicators.length, indicator);
    this.indicatorsForm.push(this.getIndicatorForm(indicator));
    this.outcomeValues.forEach((_, alternativeIdx) => {
      this.outcomeValues[alternativeIdx].forEach((_, stateIdx) => {
        this.outcomeValues[alternativeIdx][stateIdx].push([undefined]); // add values for new indicator
      });
    });
  }

  addIndicatorFromHierarchy(element: SimpleHierarchyNode, location: LocationSequence, htmlElement: HTMLElement) {
    if (location.getLevel() === 0) {
      return;
    }

    const emptyIndicator = this.indicatorsForm.controls.find(indicatorControl => indicatorControl.get('name').value === '');

    if (emptyIndicator) {
      emptyIndicator.get('name').setValue(element.name);
    } else {
      this.addIndicator(element.name);
      this.cdRef.detectChanges();
      this.hierarchyContainer.nativeElement.scrollIntoView();
    }

    this.popOverService.whistle(htmlElement, $localize`Hinzugefügt!`, 'add');
  }

  convertStages(hasAggregationTypeChanged: boolean, newWorst: number, newBest: number) {
    if (hasAggregationTypeChanged && !this.stagesChanged) {
      // generate new stages
      this.stages = [];
      this.stages.push({ value: newWorst, description: '' });
      this.stages.push({ value: Math.min(newWorst, newBest) + Math.abs(newBest - newWorst) / 2, description: '' });
      this.stages.push({ value: newBest, description: '' });
    } else {
      // convert old stages
      for (let i = 0; i < this.stages.length; i++) {
        const valuePerc = (this.stages[i].value - this.lastWorst) / (this.lastBest - this.lastWorst);
        const value = newWorst + valuePerc * (newBest - newWorst);
        const description = this.stages[i].description;
        this.stages[i] = {
          value: value,
          description: description,
        };
      }
    }
    this.stages.map(s => {
      s.value = Math.round(s.value * (1 / this.roundingErrorPrecision)) / (1 / this.roundingErrorPrecision);
      return s;
    });
    this.lastWorst = newWorst;
    this.lastBest = newBest;
  }

  openVerbalIndicatorModal(indicatorIdx: number) {
    const verbalIndicatorModalRef = this.dialog.open<VerbalIndicatorDefinitionModalComponent, VerbalIndicatorDefinitionModalData>(
      VerbalIndicatorDefinitionModalComponent,
      {
        data: {
          indicatorIdx: indicatorIdx,
          indicatorCount: this.indicatorsForm.length,
          indicator: new Indicator(
            this.indicatorsForm.controls[indicatorIdx].get('name').value,
            this.indicatorsForm.controls[indicatorIdx].get('min').value,
            this.indicatorsForm.controls[indicatorIdx].get('max').value,
            this.indicatorsForm.controls[indicatorIdx].get('unit').value,
            this.indicatorsForm.controls[indicatorIdx].get('weight').value,
            this.indicatorsForm.controls[indicatorIdx].get('comment').value,
            this.indicatorsForm.controls[indicatorIdx].get('verbalIndicatorCategories').value,
          ),
          objectiveIdx: this.objectiveIdx,
          objective: this.objectives[this.objectiveIdx],
          objectiveTree: this.objectiveTree,
          outcomeValues: this.outcomeValues,
        },
      },
    );
    verbalIndicatorModalRef
      .afterClosed()
      .subscribe((data: { outcomeValues: ObjectiveInput[][]; verbalIndicatorCategories: VerbalIndicatorCategory[] }) => {
        if (data?.verbalIndicatorCategories != null) {
          if (
            this.indicatorsForm.controls[indicatorIdx].get('verbalIndicatorCategories').value.length > 0 &&
            data.verbalIndicatorCategories.length === 0
          ) {
            // QUALITATIVE SCALE DELETED
            this.outcomeValues.forEach((_, alternativeIdx) => {
              this.outcomeValues[alternativeIdx].forEach(valuesPerState => {
                valuesPerState[indicatorIdx] = [undefined];
              });
            });
          } else if (data.outcomeValues != null) {
            // QUALITATIVE SCALE CHANGED
            this.outcomeValues = data.outcomeValues;
          }

          this.indicatorsForm.controls[indicatorIdx].get('verbalIndicatorCategories').setValue(data.verbalIndicatorCategories);
        }
      });
  }

  openDescription() {
    if (!this.form.valid) {
      return;
    }
    const indicators: Indicator[] = [];
    this.indicatorsForm.controls.forEach((indicatorGroup, index) => {
      const indicator = this.indicators.get(index);
      indicator.name = indicatorGroup.get('name').value;
      indicator.comment = indicatorGroup.get('comment').value;
      indicator.min = indicatorGroup.get('min').value;
      indicator.max = indicatorGroup.get('max').value;
      indicator.unit = indicatorGroup.get('unit').value;
      indicator.coefficient = indicatorGroup.get('weight').value;
      indicator.verbalIndicatorCategories = indicatorGroup.get('verbalIndicatorCategories').value;
      indicators.push(indicator);
    });
    let newWorst = this.form.get('range.worst').value;
    let newBest = this.form.get('range.best').value;
    if (this.form.get('useCustomAggregation').value && this.form.get('automaticCustomAggregationLimits').value) {
      newWorst = this.calculateAggregationFunctionBounds('min', this.form.get('customAggregationFormula').value);
      newBest = this.calculateAggregationFunctionBounds('max', this.form.get('customAggregationFormula').value);
    }
    this.convertStages(this.hasAggregationTypeChanged, newWorst, newBest);

    const indicatorData = new IndicatorObjectiveData(
      indicators,
      null,
      this.form.get('useCustomAggregation').value,
      this.form.get('customAggregationFormula').value,
      newWorst,
      newBest,
      this.form.get('aggregatedUnit').value,
      this.stages,
      this.form.get('automaticCustomAggregationLimits').value,
      this.decisionData.globalVariables,
    );

    this.dialog
      .open(IndicatorDescriptionModalComponent, { data: { indicatorData: indicatorData } })
      .afterClosed()
      .subscribe((stages: IndicatorObjectiveStage[]) => {
        if (stages) {
          this.stages = stages;
        }

        this.stagesChanged =
          this.stages.length !== this.indicatorData.stages.length ||
          zip(this.stages, this.indicatorData.stages).some(([newStage, oldStage]) => !isEqualIndicatorObjectiveStage(newStage, oldStage));

        this.reportDirty();
      });
  }

  deleteIndicator(index: number) {
    this.outcomeValues.forEach((values, alternativeIdx) =>
      values.forEach((_, stateIdx) => {
        this.outcomeValues[alternativeIdx][stateIdx].splice(index, 1);
      }),
    );
    this.indicators.remove(index);
    this.indicatorsForm.removeAt(index);
  }

  indicatorDrop(event: CdkDragDrop<unknown>) {
    this.outcomeValues.forEach((_, alternativeIdx) => {
      this.outcomeValues[alternativeIdx].forEach((_, stateIdx) => {
        moveItemInArray(this.outcomeValues[alternativeIdx][stateIdx], event.previousIndex, event.currentIndex);
      });
    });
    moveItemInArray(this.indicatorsForm.controls, event.previousIndex, event.currentIndex);
    moveItemInArray(this.indicatorsForm.value, event.previousIndex, event.currentIndex);
    this.indicatorsForm.setValue(this.indicatorsForm.value);
    this.indicators.move(event.previousIndex, event.currentIndex);
  }

  toggleVerbalIndicatorList(event: MouseEvent, indicatorIdx: number) {
    event.stopPropagation();
    this.verbalIndicatorListCollapsed[indicatorIdx] = !this.verbalIndicatorListCollapsed[indicatorIdx];
  }

  private updateCustomFormulaLatex() {
    const allCoefficientNames = range(this.indicatorsForm.length).map(getIndicatorCoefficientName);
    const allIndicatorNames = range(this.indicatorsForm.length).map(getIndicatorValueName);
    const allGlobalVariableNames = Array.from(this.decisionData.globalVariables.map.keys());

    try {
      const node = parse(this.form.get('customAggregationFormula').value);

      this.customAggregationFormulaLatex = node.toTex({
        implicit: 'hide',
        parenthesis: 'auto',
        handler: (node: { isSymbolNode: boolean; name: string }, _options: any) => {
          if (node.isSymbolNode) {
            if (allIndicatorNames.includes(node.name)) {
              const indicatorIdx = allIndicatorNames.indexOf(node.name);

              if (indicatorIdx < this.indicatorsForm.length) {
                const indicatorCount = this.indicatorsForm.length;

                const colorPalette = ['red', 'green', 'blue', 'gray', 'brown', 'magenta', 'orange', 'purple', 'teal', 'navy'];

                return '\\textcolor{' + colorPalette[indicatorIdx % indicatorCount] + '}{' + node.name + '}';
              }
            }

            if (allCoefficientNames.includes(node.name) || allGlobalVariableNames.includes(node.name)) {
              // eslint-disable-next-line max-len
              return `\\bold{${node.name}}`;
            }
          }
        },
      });
    } catch (error) {}
  }

  private calculateAggregationFunctionBounds(mode: 'min' | 'max', formula: string) {
    try {
      const tempIndicators = this.indicatorsForm.controls.map(
        control => new Indicator('', control.get('min').value, control.get('max').value, '', control.get('weight').value),
      );
      const af = getIndicatorAggregationFunction(tempIndicators, { formula, globalVariables: this.decisionData.globalVariables.map });
      return af(tempIndicators.map(ind => (mode === 'min' ? [ind.min] : [ind.max])));
    } catch {
      return NaN;
    }
  }

  openAggregationExplanation() {
    this.dialog.open(IndicatorAggregationExplanationModalComponent, {
      data: this.indicatorsForm.controls.map(control => control.get('name').value),
    });
  }
}
