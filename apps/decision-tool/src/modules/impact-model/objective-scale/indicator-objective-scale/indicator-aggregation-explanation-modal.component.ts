import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { DecisionData, getIndicatorCoefficientName, getIndicatorValueName } from '@entscheidungsnavi/decision-data';

@Component({
  template: `
    <dt-modal-content (closeClick)="dialogRef.close()">
      <ng-template #title i18n>Die benutzerdefinierte Formel kann folgende Elemente umfassen:</ng-template>
      <ng-template #content>
        <table class="indicator-aggregation-example">
          <tr>
            <td i18n>Indikatorvariablen:</td>
            <td>
              @for (indicatorName of indicatorNames; track indicatorName; let i = $index; let last = $last) {
                <div>
                  <ng-container i18n>„{{ getIndicatorValueName(i) }}“ steht für den Wert im Indikator „{{ indicatorName }}“</ng-container
                  >{{ last ? '.' : ',' }}
                </div>
              }
            </td>
          </tr>
          <tr>
            <td i18n>Indikatorkoeffizienten:</td>
            <td i18n
              >Die definierten Koeffizienten {{ coefficientNames }} können mit Deinen eingetragenen numerischen Werten auch in der Formel
              verwendet werden.</td
            >
          </tr>
          <tr>
            <td i18n>Globale Variablen:</td>
            <td>
              @if (decisionData.globalVariables.map.size === 0) {
                <div i18n>Es sind keine globalen Variablen definiert.</div>
              }
              @for (variable of decisionData.globalVariables.map | keyvalue; track variable; let last = $last) {
                <div>
                  <ng-container i18n>„{{ variable.key }}“ mit dem Wert {{ variable.value }}</ng-container
                  >{{ last ? '.' : ',' }}
                </div>
              }
            </td>
          </tr>
          <tr>
            <td></td>
            <td i18n
              >Bei Verwendung von globalen Variablen in einer benutzerdefinierten Formel wird empfohlen, die Bandbreite der Indikatorskala
              nicht automatisch berechnen zu lassen, sondern explizit anzugeben.</td
            >
          </tr>
          <tr>
            <td i18n>Zahlen:</td>
            <td i18n
              >Es können beliebige reelle Zahlen mit Dezimalstellen verwendet werden. Der Punkt wird als Dezimaltrennzeichen verwendet.</td
            >
          </tr>
          <tr>
            <td i18n>Operationszeichen:</td>
            <td i18n
              >Erlaubt sind die Grundrechenarten „+“, „-“, „*“, „/“, Klammerung mittels „(“ und „)“ sowie die folgenden Symbole: exp, log,
              „^“, sqrt, cos, sin, tan.</td
            >
          </tr>
          <tr>
            <td i18n>Weitere Operationen:</td>
            <td>abs, cbrt, ceil, cube, floor, nthRoot, square, round, max, min, sum, mean, median.</td>
          </tr>
          <tr>
            <td i18n>Trigonometrische Operationen:</td>
            <td
              >acos, acosh, acot, acoth, acsc, acsch, asec, asech, asin, asinh, atan, atan2, atanh, cosh, cot, coth, csc, csch, sec, sech,
              sinh, tanh.</td
            >
          </tr>
          <tr>
            <td colspan="2">
              <ng-container i18n>Beispiel 1: (Ind1 - 5.5) * Ind2</ng-container><br />
              <ng-container i18n>Beispiel 2: ((Ind1 / 100) * (Ind2 + 2))/(Ind2 + a + b)</ng-container>
            </td>
          </tr>
        </table>
      </ng-template>
    </dt-modal-content>
  `,
  styles: [
    //language=SCSS
    `
      // Custom formula explanation modal
      table.indicator-aggregation-example {
        td {
          vertical-align: top;

          &:not(:last-child) {
            padding-right: 0.5rem;
          }
        }

        tr:not(:last-child) td {
          padding-bottom: 1rem;
        }
      }
    `,
  ],
})
export class IndicatorAggregationExplanationModalComponent {
  coefficientNames = this.indicatorNames.map((_, i) => getIndicatorCoefficientName(i)).join(', ');

  constructor(
    @Inject(MAT_DIALOG_DATA) protected indicatorNames: string[],
    protected decisionData: DecisionData,
    protected dialogRef: MatDialogRef<IndicatorAggregationExplanationModalComponent>,
  ) {}

  getIndicatorValueName(indicatorIdx: number) {
    return getIndicatorValueName(indicatorIdx);
  }
}
