import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { KatexModule } from '@entscheidungsnavi/widgets/katex';
import { DragDropModule } from '@angular/cdk/drag-drop';
import {
  AutoFocusDirective,
  ConfirmPopoverDirective,
  CrossImpactNamePipe,
  FocusPopoverDirective,
  HoverPopOverDirective,
  IndicatorValueCalculationPipe,
  IsIntegerPipe,
  NoteBtnComponent,
  NoteBtnPresetPipe,
  OverflowDirective,
  PlainTextPasteDirective,
  RichTextEditorComponent,
  RiskProfileChartComponent,
  ScrewListComponent,
  SimpleHierarchyComponent,
  ToggleButtonComponent,
  WidthTriggerDirective,
} from '@entscheidungsnavi/widgets';
import { DiagramLegendComponent } from '@entscheidungsnavi/widgets/diagram-legend/diagram-legend.component';
import { SelectLineComponent } from '@entscheidungsnavi/widgets/select';
import { SlideToggleComponent } from '@entscheidungsnavi/widgets/slide-toggle/slide-toggle.component';
import { TornadoDiagramComponent } from '@entscheidungsnavi/widgets/tornado-diagram/tornado-diagram.component';
import { SharedModule } from '../shared/shared.module';
import { NoteHoverComponent } from '../shared/note-hover/note-hover.component';
import { CopyScaleModalComponent, ImpactMatrixComponent, ImpactMatrixFieldComponent } from './impact-matrix';
import { ImpactModelRoutingModule } from './impact-model-routing.module';
import { IndicatorVariableNamePipe } from './indicator-variable-name.pipe';
import { ImpactModelComponent } from './main';
import {
  CompositeInfluenceFactorBoxComponent,
  CompositeInfluenceFactorPopoverComponent,
  InfluenceFactorsComponent,
  UserDefinedInfluenceFactorBoxComponent,
  UserDefinedInfluenceFactorModalComponent,
} from './influence-factors';
import {
  ImpactModelHint1Component,
  ImpactModelHint2Component,
  ImpactModelHint3Component,
  ImpactModelHint4Component,
  ImpactModelHint5Component,
  ImpactModelHint6Component,
} from './help';
import {
  IndicatorDescriptionModalComponent,
  IndicatorObjectiveScaleComponent,
  NumericalObjectiveScaleComponent,
  ObjectiveScaleModalComponent,
  VerbalObjectiveScaleComponent,
} from './objective-scale';
import { CombinationComponent } from './objective-scale/indicator-description-modal/combination-component/combination-component.component';
import { Help1Component } from './help/help1/help1.component';
import { Help2Component } from './help/help2/help2.component';
import { HelpBackgroundComponent } from './help/help-background/help-background.component';
import {
  ForecastOfOutcomesModalComponent,
  RiskProfileComponent,
  OutcomeTornadoDiagramComponent,
  VerbalIndicatorForecastModalComponent,
} from './impact-matrix/forecast-of-outcomes-modal';
import { NameFieldComponent } from './impact-matrix/name-field/name-field.component';
import {
  CompositeUserDefinedInfluenceFactorTooltipComponent,
  IndicatorValuesMatrixComponent,
  IndicatorMatrixComponent,
  NumericalInputComponent,
  NumericalOrVerbalValueComponent,
} from './impact-matrix/objective-value';
import {
  IndicatorAggregationExplanationModalComponent,
  VerbalIndicatorDefinitionModalComponent,
} from './objective-scale/indicator-objective-scale';
import { VerbalCategoryComponent, VerbalCategoryStageComponent, VerbalIndicatorDiagramComponent } from './verbal-indicator-diagram';

@NgModule({
  declarations: [
    CopyScaleModalComponent,
    ForecastOfOutcomesModalComponent,
    ImpactMatrixComponent,
    ImpactMatrixFieldComponent,
    ImpactModelComponent,
    ImpactModelHint1Component,
    ImpactModelHint2Component,
    ImpactModelHint3Component,
    ImpactModelHint4Component,
    ImpactModelHint5Component,
    ImpactModelHint6Component,
    IndicatorDescriptionModalComponent,
    IndicatorObjectiveScaleComponent,
    IndicatorVariableNamePipe,
    NameFieldComponent,
    NumericalObjectiveScaleComponent,
    ObjectiveScaleModalComponent,
    NumericalOrVerbalValueComponent,
    VerbalObjectiveScaleComponent,
    Help1Component,
    Help2Component,
    HelpBackgroundComponent,
    OutcomeTornadoDiagramComponent,
    RiskProfileComponent,
    UserDefinedInfluenceFactorBoxComponent,
    UserDefinedInfluenceFactorModalComponent,
    InfluenceFactorsComponent,
    VerbalIndicatorDefinitionModalComponent,
    VerbalIndicatorForecastModalComponent,
    VerbalIndicatorDiagramComponent,
    CompositeUserDefinedInfluenceFactorTooltipComponent,
    IndicatorMatrixComponent,
    IndicatorValuesMatrixComponent,
    NumericalInputComponent,
    VerbalCategoryComponent,
    VerbalCategoryStageComponent,
    IndicatorMatrixComponent,
    CompositeInfluenceFactorBoxComponent,
    CompositeInfluenceFactorPopoverComponent,
    IndicatorAggregationExplanationModalComponent,
  ],
  imports: [
    AutoFocusDirective,
    CommonModule,
    DragDropModule,
    DiagramLegendComponent,
    ImpactModelRoutingModule,
    IndicatorValueCalculationPipe,
    KatexModule,
    SharedModule,
    OverflowDirective,
    NoteBtnComponent,
    NoteBtnPresetPipe,
    HoverPopOverDirective,
    NoteHoverComponent,
    ScrewListComponent,
    IsIntegerPipe,
    WidthTriggerDirective,
    ConfirmPopoverDirective,
    RiskProfileChartComponent,
    RichTextEditorComponent,
    ToggleButtonComponent,
    CrossImpactNamePipe,
    PlainTextPasteDirective,
    SelectLineComponent,
    AutoFocusDirective,
    SimpleHierarchyComponent,
    SlideToggleComponent,
    FocusPopoverDirective,
    CombinationComponent,
    TornadoDiagramComponent,
  ],
  exports: [ImpactMatrixComponent, InfluenceFactorsComponent],
})
export class ImpactModelModule {}
