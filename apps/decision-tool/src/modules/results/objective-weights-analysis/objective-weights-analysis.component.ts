import { Component, HostBinding, inject, OnInit, NgZone } from '@angular/core';
import { Alternative, DecisionData, Objective } from '@entscheidungsnavi/decision-data';
import { OnDestroyObservable } from '@entscheidungsnavi/tools';
import { range, sum } from 'lodash';
import { Observable, takeUntil, throttleTime } from 'rxjs';
import { PersistentSetting, PersistentSettingParent } from '@entscheidungsnavi/widgets';
import { ObjectiveWeightsAnalysisService, OWADataPoint, OWAResult } from '../../../worker/objective-weights-analysis.service';
import { DisplayAtMaxWidth } from '../../../app/interfaces/display-at-max-width';
import { Navigation, NavLine, navLineElement } from '../../shared/navline';
import { HelpMenuProvider } from '../../../app/help/help';
import { getHelpMenu } from '../help';
import { LanguageService } from '../../../app/data/language.service';

@Component({
  templateUrl: './objective-weights-analysis.component.html',
  styleUrls: ['./objective-weights-analysis.component.scss'],
  providers: [ObjectiveWeightsAnalysisService],
})
@DisplayAtMaxWidth
@PersistentSettingParent('ObjectiveWeightsAnalysisComponent')
export class ObjectiveWeightsAnalysisComponent implements Navigation, HelpMenuProvider, OnInit {
  @OnDestroyObservable()
  private onDestroy$: Observable<void>;

  currentResult: OWAResult;
  elapsedTime = 0;
  isPaused = false;
  private lastUpdateTime: number;

  alternatives: Alternative[];
  objectives: Objective[];

  @HostBinding('class.compact')
  @PersistentSetting()
  smallRowHeight = true;

  endlessMode = false;
  displayMode: 'simple' | 'detailed' = 'simple'; // only center value (bar diagram) vs center value + quantiles + extrema (box plot)
  quantilesOption: 'p.10' | 'p.25' = 'p.10'; // p.10 stands for p.10 and p.90, p.25 stands for p.25 and p.75
  selectedAlternativeIndices = range(this.decisionData.alternatives.length);
  sortingOption: 'utilityValues' | 'gutFeeling' = 'utilityValues';
  sortedAlternativeIndices = range(this.decisionData.alternatives.length);

  hoveredAlternativeIdx = -1;

  private languageService = inject(LanguageService);

  readonly compactScalingTooltip = $localize`Ändere Skalierung zu normal`;
  readonly normalScalingTooltip = $localize`Ändere Skalierung zu kompakt`;

  readonly navLine = new NavLine({ left: [navLineElement().back('/results').build()] });
  readonly helpMenu = getHelpMenu(this.languageService.steps, 'objective-weights-analysis');

  get state(): 'done' | 'calculating' | 'paused' {
    if (!this.isPaused) return 'calculating';
    else if (this.objectiveWeightService.isRandomComputationDone) return 'done';
    else return 'paused';
  }

  get alternativeIndices() {
    return this.sortingOption === 'utilityValues' ? this.sortedAlternativeIndices : range(this.alternatives.length);
  }

  get alternativesCounterSum() {
    return sum(this.currentResult.bestAlternativeCounters);
  }

  @HostBinding('style.--selected-alternatives-count')
  get selectedAlternativesCount() {
    return this.selectedAlternativeIndices.length;
  }

  @HostBinding('style.--objective-count')
  get objectivesCount() {
    return this.objectives.length;
  }

  get meanPath() {
    const points: [number, number][] = [];

    const innerBoxWidth = 0.9;
    const quantilWidth = 0.7;

    const quantilWidthToBox = innerBoxWidth * quantilWidth;

    const totalBoxPercentage = 100 / this.objectives.length;

    for (let objectiveIndex = 0; objectiveIndex < this.objectives.length; objectiveIndex++) {
      const dataPoint = this.currentResult.diagramData[this.hoveredAlternativeIdx][objectiveIndex];
      const startXBox = totalBoxPercentage * objectiveIndex;
      const endXBox = totalBoxPercentage * (objectiveIndex + 1);
      const width = endXBox - startXBox;
      const dif = (1 - quantilWidthToBox) * width;

      points.push([startXBox + dif / 2, 100 - dataPoint.avg * 100]);
      points.push([endXBox - dif / 2, 100 - dataPoint.avg * 100]);
    }

    return `M ${points[0][0]} ${points[0][1]} ${points
      .slice(1)
      .map(p => `L ${p[0]} ${p[1]}`)
      .join(' ')}`;
  }

  constructor(
    protected decisionData: DecisionData,
    protected objectiveWeightService: ObjectiveWeightsAnalysisService,
    private zone: NgZone,
  ) {
    this.alternatives = this.decisionData.alternatives;
    this.objectives = this.decisionData.objectives;
  }

  ngOnInit() {
    this.objectiveWeightService.onUpdate$
      .pipe(throttleTime(1_000, undefined, { leading: true, trailing: true }), takeUntil(this.onDestroy$))
      .subscribe(() => {
        this.zone.run(() => {
          this.updateResults();
        });
      });
    this.start();
  }

  updateResults() {
    if (this.objectiveWeightService.isRandomComputationDone && !this.endlessMode) this.isPaused = true;
    this.currentResult = this.objectiveWeightService.getCurrentResult();

    // calculate sorting indices based on bestAlternativeCounters
    const alternativeCounters = this.currentResult.bestAlternativeCounters;
    const indices = range(this.alternatives.length);
    indices.sort((a, b) => alternativeCounters[b] - alternativeCounters[a]);
    this.sortedAlternativeIndices = indices;

    const currentUpdateTime = performance.now();
    this.elapsedTime += currentUpdateTime - this.lastUpdateTime;
    this.lastUpdateTime = currentUpdateTime;
  }

  // begins or unpauses calculations
  start() {
    this.isPaused = false;
    this.lastUpdateTime = performance.now();
    this.objectiveWeightService.startWorker(this.endlessMode);
  }

  // pauses all calculations, only available after the initial algorithm (that calculates the fixed combinations) has terminated
  pause() {
    if (!this.objectiveWeightService.isFixedComputationDone()) return;
    this.isPaused = true;
    this.objectiveWeightService.pauseWorker();
  }

  // returns the lower or upper quantile based on quantilesOption (p.10/p.90 vs p.25/p.75)
  getQuantile(dataPoint: OWADataPoint, quantile: 'low' | 'high') {
    return this.quantilesOption === 'p.10'
      ? quantile === 'low'
        ? dataPoint.p10
        : dataPoint.p90
      : quantile === 'low'
        ? dataPoint.p25
        : dataPoint.p75;
  }

  isAlternativeEverBest(alternativeIdx: number) {
    return !this.currentResult.diagramData[alternativeIdx].some(cellValue => cellValue.min == null || cellValue.max == null);
  }

  // determines whether the tooltip should be displayed under or on top
  isTopTooltip(alternativeIdx: number, diagramContainer: HTMLElement, diagramGrid: HTMLElement) {
    // if there's enough space for the tooltip - always show under
    if (diagramContainer.offsetHeight - diagramGrid.offsetHeight > 160) return false;

    // determines how many alternatives come after this alternative in the current ordering
    const alternativesAfterThis =
      this.sortingOption === 'utilityValues'
        ? this.sortedAlternativeIndices.length - 1 - this.sortedAlternativeIndices.indexOf(alternativeIdx)
        : this.alternatives.length - 1 - alternativeIdx;

    // show top tooltip for last alternative and second to last alternative when smallRowHeight
    return alternativesAfterThis === 0 || (alternativesAfterThis === 1 && this.smallRowHeight);
  }
}
