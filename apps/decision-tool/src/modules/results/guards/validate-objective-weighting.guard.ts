import { Injectable } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { DecisionData } from '@entscheidungsnavi/decision-data';
import { ErrorModalComponent } from '../../shared/error';

@Injectable({
  providedIn: 'root',
})
export class ValidateObjectiveWeightingGuard {
  constructor(
    private decisionData: DecisionData,
    private dialog: MatDialog,
  ) {}

  canActivate(_route: ActivatedRouteSnapshot, _state: RouterStateSnapshot): boolean {
    const result = this.decisionData.validateWeights();
    if (!result[0]) {
      ErrorModalComponent.open(
        this.dialog,
        $localize`Du musst zuerst die Zielgewichtung durchführen, um hierhin navigieren zu können.`,
        result[1],
      );
      return false;
    }
    return true;
  }
}
