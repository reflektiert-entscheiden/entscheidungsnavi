import { Injectable } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { DecisionData } from '@entscheidungsnavi/decision-data';
import { ErrorModalComponent } from '../../shared/error';

@Injectable({
  providedIn: 'root',
})
export class ValidateUtilityFunctionsGuard {
  constructor(
    private decisionData: DecisionData,
    private dialog: MatDialog,
  ) {}

  canActivate(_route: ActivatedRouteSnapshot, _state: RouterStateSnapshot): boolean {
    return this.validateAndWarn();
  }

  validateAndWarn() {
    const result = this.decisionData.validateUtilityFunctions();
    if (!result[0]) {
      ErrorModalComponent.open(
        this.dialog,
        $localize`Du musst zuerst die Nutzenfunktionen vervollständigen, um hierhin navigieren zu können.`,
        result[1],
      );
      return false;
    }
    return true;
  }
}
