import { CanActivateFn, Router } from '@angular/router';
import { inject } from '@angular/core';
import { DecisionData } from '@entscheidungsnavi/decision-data';

export function atLeastTwoObjectivesGuard(fallbackUrl: string): CanActivateFn {
  return () => {
    const dd = inject(DecisionData);

    if (dd.objectives.length <= 1) {
      return inject(Router).parseUrl(fallbackUrl);
    }

    return true;
  };
}
