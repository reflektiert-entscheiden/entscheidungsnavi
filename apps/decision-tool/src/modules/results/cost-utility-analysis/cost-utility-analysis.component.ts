import { AfterViewInit, Component, computed, ElementRef, HostBinding, HostListener, Input, OnInit, signal, ViewChild } from '@angular/core';
import {
  DecisionData,
  getUtilityToDisplayFunction,
  NumericalObjectiveData,
  Objective,
  ObjectiveInput,
  UtilityFunction,
} from '@entscheidungsnavi/decision-data';
import { range, sum } from 'lodash';
import { Router } from '@angular/router';
import { Interval, normalizeContinuous } from '@entscheidungsnavi/tools';
import { Navigation, NavLine, navLineElement } from '../../shared/navline';
import { HelpMenuProvider } from '../../../app/help/help';
import { getHelpMenu } from '../help';
import { LanguageService } from '../../../app/data/language.service';

type AlternativePoint = {
  name: string;
  x: number;
  y: number;
  totalUtility: number;
  isLabelTopAligned: boolean;
  isLabelLeftAligned: boolean;
};

@Component({
  selector: 'dt-cost-utility-analysis',
  templateUrl: './cost-utility-analysis.component.html',
  styleUrls: ['./cost-utility-analysis.component.scss'],
})
export class CostUtilityAnalysisComponent implements AfterViewInit, HelpMenuProvider, Navigation, OnInit {
  /* dt-select-line */
  selectedAlternatives: number[] = range(this.decisionData.alternatives.length); // all alternatives selected by default
  costObjectiveIdx = signal<number>(null);
  costObjective = computed<Objective>(() => this.decisionData.objectives[this.costObjectiveIdx()]);
  showPointLabel = true; // point labels toggle

  /**
   * We distinguish between the terms totalUtility and adjustedUtility
   *
   * totalUtility refers to the sum of utility * normalizedWeight for each alternative
   * adjustedUtility is the sum of utility * normalizedAdjustedWeight for each alternative
   * where normalizedAdjustedWeight of objective X = normalizedWeightOfX / (1 - normalizedWeightOfCostObjectiveWeight)
   *
   * therefore to convert we can convert from total to adjusted at any time using the formula:
   * totalUtility = adjustedUtility * (1 - normalizedWeights[costObjectiveIdx]);
   *
   * On the diagram:
   * X = outcomes[alternativeIdx][costObjectiveIdx].value (aggregate if indicator)
   * Y = adjustedUtility - utility(X) * normalizedAdjustedWeights[costObjectiveIdx] (in %)
   *
   * The best alternative is top-left-most alternative, the worst is the bottom-right-most one
   * and their can be compared with each other by following the indifference curves separating them.
   */

  /* Fixed computations */
  normalizedWeights: number[]; // weights normalized between 0 and 1
  utilityMatrix: number[][]; // unweighted utility matrix
  totalUtilitiesPerAlternative: number[]; // sum of utilities multiplied with normalizedWeights

  /* Computations updated with costObjectiveIdx (signal) */
  // [from,to]/[worst,best] of cost objective
  costObjectiveInterval = computed<Interval>(() => this.decisionData.objectives[this.costObjectiveIdx()].getRangeInterval());
  // utility function for cost objective
  costObjectiveUtilityFunction = computed<(x: ObjectiveInput) => number>(() => {
    let objective: Objective;
    if (this.costObjective().isIndicator) {
      // we convert the indicator objective to a numerical objective, while keeping the utility function
      // that way we can pass single-value arguments to the UF instead of indicator values (fits better with the x-axis traverse)
      const numericalData = new NumericalObjectiveData(
        this.costObjectiveInterval().start,
        this.costObjectiveInterval().end,
        '',
        new UtilityFunction(
          this.costObjective().indicatorData.utilityfunction.c,
          this.costObjective().indicatorData.utilityfunction.precision,
        ),
      );
      objective = new Objective('', numericalData);
    } else if (this.costObjective().isNumerical) {
      objective = this.costObjective();
    }

    return objective.getUtilityFunction();
  });
  // (utility [0,1] -> shown value [from,to]/[0,100]/[worst,best])
  inverseDisplayFct = computed<(utility: number) => number>(() => getUtilityToDisplayFunction(this.costObjective()));
  // normalizedAdjustedWeightOfX = normalizedWeightOfX / sum(normalizedWeightsExceptCostObjectiveWeight)
  normalizedAdjustedWeights = computed<number[]>(() =>
    this.normalizedWeights.map(w => w / (1 - this.normalizedWeights[this.costObjectiveIdx()])),
  );
  // sum of utilities multiplied with normalizedAdjustedWeights
  totalAdjustedUtilitiesPerAlternative = computed<number[]>(() =>
    this.utilityMatrix.map(row =>
      row.reduce(
        (adjustedUtilityAcc, utility, objectiveIdx) => adjustedUtilityAcc + utility * this.normalizedAdjustedWeights()[objectiveIdx],
        0,
      ),
    ),
  );

  /* Diagram data */
  alternativePointSeries = computed<AlternativePoint[]>(() => {
    const alternativePoints = this.decisionData.alternatives.map((alternative, alternativeIdx) => {
      const x = this.inverseDisplayFct()(
        this.decisionData.outcomes[alternativeIdx][this.costObjectiveIdx()].getUtility(this.costObjective()),
      );
      const y =
        (this.totalAdjustedUtilitiesPerAlternative()[alternativeIdx] -
          this.utilityMatrix[alternativeIdx][this.costObjectiveIdx()] * this.normalizedAdjustedWeights()[this.costObjectiveIdx()]) *
        100;
      return {
        name: alternative.name,
        x,
        y,
        totalUtility: this.totalUtilitiesPerAlternative[alternativeIdx],
        isLabelLeftAligned: this.isLabelLeftAligned(x),
        isLabelTopAligned: false,
      };
    });
    alternativePoints.forEach((point, pointIdx) => {
      alternativePoints[pointIdx].isLabelTopAligned = this.isLabelTopAligned(
        point,
        pointIdx < alternativePoints.length - 1 ? alternativePoints[pointIdx + 1] : null,
      );
    });
    return alternativePoints;
  });

  // default indifference curves (blue curves)
  defaultCurves = computed<{ path: string; totalUtility: number }[]>(() =>
    range(this.numberOfIndifferenceCurves).map(curveStep => {
      const totalUtility = (curveStep + 0.5) / (this.numberOfIndifferenceCurves - 1); // 10, 30, 50, 70, 90
      const adjustedUtility = totalUtility / (1 - this.normalizedWeights[this.costObjectiveIdx()]);
      return { path: this.computeCurve(adjustedUtility), totalUtility };
    }),
  );
  // on-hover indifference curves (red curves)
  onHoverCurvePaths = computed<string[]>(() =>
    this.totalAdjustedUtilitiesPerAlternative().map(adjustedUtility => this.computeCurve(adjustedUtility)),
  );

  /* UI */
  forceLabelHidden = false;
  hoveredPointIdx = -1; // last hovered point
  hoveredCurveIdx = -1; // last hovered indifference curve

  // determined so that numbers are split "nicely"
  numberOfXTicks = computed<number>(() => {
    const possibleNumberOfTicks = [10, 9, 8, 7, 6, 5, 4, 3];
    const intervalWidth = this.costObjectiveInterval().end - this.costObjectiveInterval().start;
    const suggestedNumberOfTicks = possibleNumberOfTicks.find(numberOfXTicks => !(intervalWidth % (numberOfXTicks - 1)));
    return suggestedNumberOfTicks ?? 6;
  });

  @HostBinding('style.--col-count')
  get numberOfColumns() {
    return this.numberOfXTicks() - 1;
  }

  showHoveredCurve = false; // is a point currently hovered (not entirely the same as hoveredPointIdx)
  tooltipPos: [number, number] = [0, 0]; // coordinates of the curve pop-over

  readonly numberOfPointsInCurve = 40;
  readonly numberOfIndifferenceCurves = 6;

  readonly hiddenTooltip = $localize`Punktbeschriftungen anzeigen`;
  readonly shownTooltip = $localize`Punktbeschriftungen verbergen`;
  readonly verbalObjectivesDisabledTooltip = $localize`Verbale Ziele können nicht als Kostenziel ausgewählt werden.`;

  readonly labelRightMargin = 210; // in px
  readonly labelTopMargin = 4; // in %

  readonly navLine = new NavLine({ left: [navLineElement().back('/results').build()] });

  @ViewChild('chart')
  chartElement: ElementRef<HTMLElement>;
  chartElementWidth = signal<number>(-1);

  @Input() showTitle = true;

  helpMenu = getHelpMenu(this.languageService.steps, 'cost-utility-analysis');

  constructor(
    protected decisionData: DecisionData,
    private languageService: LanguageService,
    private router: Router,
  ) {}

  @HostListener('window:resize', ['$event'])
  onResize() {
    this.chartElementWidth.set(this.chartElement.nativeElement.offsetWidth);
  }

  ngAfterViewInit() {
    this.chartElementWidth.set(this.chartElement.nativeElement.offsetWidth);
  }

  ngOnInit() {
    /* Values that remain the same even when changing the const objective */
    this.utilityMatrix = this.decisionData.getUtilityMatrix();
    this.totalUtilitiesPerAlternative = this.decisionData.getWeightedUtilityMatrix().map(row => sum(row));
    this.normalizedWeights = this.decisionData.weights.getWeightValues();
    normalizeContinuous(this.normalizedWeights);
    this.costObjectiveIdx.set(this.decisionData.objectives.findIndex(obj => !obj.isVerbal));
    if (this.costObjectiveIdx() === -1) {
      this.router.navigate(['/results']);
    }
  }

  xTickFromIndex(tickIdx: number) {
    return (
      this.costObjectiveInterval().end +
      (tickIdx / (this.numberOfXTicks() - 1)) * (this.costObjectiveInterval().start - this.costObjectiveInterval().end)
    );
  }

  // helper function for converting absolute X values to percentages
  protected xValueToPercentage(x: number) {
    // reverse start and end
    return (x - this.costObjectiveInterval().end) / (this.costObjectiveInterval().start - this.costObjectiveInterval().end);
  }

  // computes a single indifference curve based on a predefined number of points (numberOfPointsInCurve)
  private computeCurve(adjustedUtility: number) {
    const points: [number, number][] = [];
    for (let pointIdx = 0; pointIdx <= this.numberOfPointsInCurve; pointIdx++) {
      const x =
        this.costObjectiveInterval().start +
        ((this.costObjectiveInterval().end - this.costObjectiveInterval().start) * (this.numberOfPointsInCurve - pointIdx)) /
          this.numberOfPointsInCurve;
      const y =
        (adjustedUtility - this.costObjectiveUtilityFunction()([[x]]) * this.normalizedAdjustedWeights()[this.costObjectiveIdx()]) * 100;
      points.push([(pointIdx / this.numberOfPointsInCurve) * 100, 100 - y]);
    }
    return `M ${points[0][0]} ${points[0][1]} ${points
      .slice(1)
      .map(p => `L ${p[0]} ${p[1]}`)
      .join(' ')}`;
  }

  private isLabelLeftAligned(pointX: number) {
    const remainingWidth = (1 - this.xValueToPercentage(pointX)) * this.chartElementWidth(); // in px
    return remainingWidth < this.labelRightMargin;
  }

  private isLabelTopAligned(point: AlternativePoint, nextPoint: AlternativePoint) {
    if (point.y < this.labelTopMargin) {
      // overlaps with end of diagram
      return true;
    } else {
      if (nextPoint != null && point.y - nextPoint.y < this.labelTopMargin) {
        // there's a next point, and they overlap on y
        return this.arePointsOverlappingOnX(point, nextPoint);
      } else {
        return false;
      }
    }
  }

  // checks if two points are horizontally overlapping
  private arePointsOverlappingOnX(point1: AlternativePoint, point2: AlternativePoint) {
    const overlappingPercentage = this.labelRightMargin / this.chartElementWidth();
    if (point1.isLabelLeftAligned === point2.isLabelLeftAligned) {
      // both left aligned or right aligned
      return Math.abs(this.xValueToPercentage(point1.x) - this.xValueToPercentage(point2.x)) < overlappingPercentage;
    } else {
      if (point1.isLabelLeftAligned && !point2.isLabelLeftAligned) {
        return this.xValueToPercentage(point2.x) - this.xValueToPercentage(point1.x) < 2 * overlappingPercentage;
      } else {
        return this.xValueToPercentage(point1.x) - this.xValueToPercentage(point2.x) < 2 * overlappingPercentage;
      }
    }
  }
}
