import { ChangeDetectionStrategy, Component, Inject, Input, OnInit } from '@angular/core';
import { DecisionData, getUtilityFunction, Objective, ObjectiveType } from '@entscheidungsnavi/decision-data';
import { isRichTextEmpty } from '@entscheidungsnavi/tools';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { clamp, identity } from 'lodash';
import { assertUnreachable } from '@entscheidungsnavi/tools/assert-unreachable';
import { Descriptor, Value } from './index';

@Component({
  templateUrl: './tradeoff-comparison.component.html',
  styleUrls: ['./tradeoff-comparison.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TradeoffComparisonComponent implements OnInit {
  static readonly MAX_PERCENT_HEIGHT = 50;

  @Input()
  objectiveIndex1: number;

  @Input()
  objectiveIndex2: number;

  objective1: Objective;
  objective2: Objective;

  descriptors1: Descriptor[];
  descriptors2: Descriptor[];

  values1: Value[];
  values2: Value[];

  ticks1: number[];
  ticks2: number[];

  valuesLeft: [a: number, b: number] = [1, 0];
  valuesRight: [a: number, b: number] = [1, 0];

  scaling: [number, number] = [1, 1];
  showModesDropDown1: boolean;
  showModesDropDown2: boolean;

  indicatorDescriptionModes: ['custom' | 'suggested', 'custom' | 'suggested'];

  // Just an Alias for objectiveIndex2 to make clear it's the objective which weight gets adjusted based on the tradeoff.
  get targetObjectiveIndex() {
    return this.objectiveIndex2;
  }

  get necessaryPadding() {
    let highestTop = 0;
    let lowestBottom = 0;

    for (const descriptor of [...this.descriptors1, ...this.descriptors2]) {
      if (descriptor.height && descriptor.position + descriptor.height / 100 > highestTop) {
        highestTop = descriptor.position + descriptor.height / 100;
      }

      if (descriptor.height && descriptor.position - descriptor.height / 100 < lowestBottom) {
        lowestBottom = descriptor.position - descriptor.height / 100;
      }
    }

    const padding = Math.max(highestTop - 1, -lowestBottom);

    if (padding === 0) {
      return undefined;
    }

    const half = padding / 2;

    return (half / (2 * half + 1)) * 100;
  }

  get modalSize() {
    if (this.objective1.isIndicator || this.objective2.isIndicator) {
      return 'giant';
    } else {
      return 'normal';
    }
  }

  get weights() {
    return this.decisionData.weights.tradeoffWeights;
  }

  get isTradeoffValid() {
    return this.valuesLeft[0] < this.valuesLeft[1] === this.valuesRight[0] > this.valuesRight[1];
  }

  // eslint-disable-next-line @typescript-eslint/naming-convention
  protected readonly ObjectiveType = ObjectiveType;

  constructor(
    private decisionData: DecisionData,
    private dialogRef: MatDialogRef<TradeoffComparisonComponent>,
    @Inject(MAT_DIALOG_DATA)
    data: { objectiveIndex1: number; objectiveIndex2: number; initialValues: [[number, number], [number, number]] },
  ) {
    this.objectiveIndex1 = data.objectiveIndex1;
    this.objectiveIndex2 = data.objectiveIndex2;

    this.valuesLeft = data.initialValues[0];
    this.valuesRight = data.initialValues[1];
  }

  ngOnInit() {
    this.objective1 = this.decisionData.objectives[this.objectiveIndex1];
    this.objective2 = this.decisionData.objectives[this.objectiveIndex2];

    this.indicatorDescriptionModes = [
      this.areAllStagesEmpty(this.objective1) ? 'suggested' : 'custom',
      this.areAllStagesEmpty(this.objective2) ? 'suggested' : 'custom',
    ];

    this.values1 = this.makeValues(this.objective1);
    this.values2 = this.makeValues(this.objective2);

    this.updateDescriptions();

    this.showModesDropDown1 =
      this.objective1.objectiveType === ObjectiveType.Indicator &&
      this.objective1.includesVerbalIndicators &&
      !this.objective1.indicatorData.useCustomAggregation;
    this.showModesDropDown2 =
      this.objective2.objectiveType === ObjectiveType.Indicator &&
      this.objective2.includesVerbalIndicators &&
      !this.objective2.indicatorData.useCustomAggregation;
  }

  updateDescriptions() {
    this.descriptors1 = this.makeDescriptors(0);
    this.descriptors2 = this.makeDescriptors(1);

    this.ticks1 = this.makeTicks(this.objective1, this.values1, this.descriptors1);
    this.ticks2 = this.makeTicks(this.objective2, this.values2, this.descriptors2);
  }

  close() {
    this.dialogRef.close();
  }

  applyTradeoff() {
    const ufLeft = this.getBasicUtilityFunction(this.objective1);
    const ufRight = this.getBasicUtilityFunction(this.objective2);

    this.decisionData.weights.manualTradeoffs[this.targetObjectiveIndex] = [
      [ufRight(this.valuesRight[0]), ufLeft(this.valuesLeft[0])],
      [ufRight(this.valuesRight[1]), ufLeft(this.valuesLeft[1])],
    ];
    const [leftPoint, rightPoint] = this.decisionData.weights.manualTradeoffs[this.targetObjectiveIndex];

    const newSlope = -(rightPoint[1] - leftPoint[1]) / (rightPoint[0] - leftPoint[0]);

    // The gewichtung is relative to the gewichtung of the reference objective
    this.weights[this.targetObjectiveIndex].value = this.adjustWeight(
      newSlope * this.weights[this.decisionData.weights.tradeoffObjectiveIdx].value,
    );
    this.weights[this.targetObjectiveIndex].precision = 0;

    this.close();
  }

  private getBasicUtilityFunction(objective: Objective) {
    switch (objective.objectiveType) {
      case ObjectiveType.Numerical:
        return getUtilityFunction(objective.numericalData.utilityfunction.c);
      case ObjectiveType.Indicator:
        return getUtilityFunction(objective.indicatorData.utilityfunction.c);
      case ObjectiveType.Verbal:
        return identity;
      default:
        assertUnreachable(objective.objectiveType);
    }
  }

  getScaleValue(objective: Objective) {
    return (position: number) => {
      if (objective.objectiveType === ObjectiveType.Verbal) {
        return Math.round(position * 100) + '%';
      } else if (objective.objectiveType === ObjectiveType.Numerical) {
        const numericalRange = objective.numericalData.to - objective.numericalData.from;

        return (
          (objective.numericalData.from + numericalRange * position).round(2) +
          (objective.numericalData.unit ? ' ' + objective.numericalData.unit : '')
        );
      } else if (objective.objectiveType === ObjectiveType.Indicator) {
        const indicatorRange = objective.indicatorData.bestValue - objective.indicatorData.worstValue;

        return (
          (objective.indicatorData.worstValue + indicatorRange * position).round(2) +
          (objective.indicatorData.aggregatedUnit ? ' ' + objective.indicatorData.aggregatedUnit : '')
        );
      }
    };
  }

  areAllStagesEmpty(objective: Objective) {
    return objective.indicatorData.stages.every(stage => stage.description === '');
  }

  private adjustWeight(val: number): number {
    return clamp(val, 1, 1000);
  }

  private makeTicks(objective: Objective, values: Value[], _descriptors: Descriptor[]) {
    const ticks: number[] = [];
    switch (objective.objectiveType) {
      case ObjectiveType.Numerical:
      case ObjectiveType.Indicator:
        values
          .filter(value => value.position !== 0 && value.position !== 1)
          .forEach(value => {
            ticks.push(value.position);
          });
        break;
      case ObjectiveType.Verbal:
        values
          .filter(value => value.position !== 0 && value.position !== 1)
          .forEach(value => {
            ticks.push(value.position);
          });
        break;
    }

    return ticks;
  }

  private makeDescriptors(columnIdx: number) {
    const objective = columnIdx === 0 ? this.objective1 : this.objective2;
    const descriptors: Descriptor[] = [];
    if (objective.objectiveType === ObjectiveType.Numerical) {
      if (!isRichTextEmpty(objective.numericalData.commentTo)) {
        descriptors.push({ position: 1, description: objective.numericalData.commentTo, rich: true, height: 25 });
      }
      if (!isRichTextEmpty(objective.numericalData.commentFrom)) {
        descriptors.push({ position: 0, description: objective.numericalData.commentFrom, rich: true, height: 25 });
      }
    } else if (objective.objectiveType === ObjectiveType.Verbal) {
      const utilityFunction = objective.getUtilityFunction();

      const positions = objective.verbalData.options.map((_, index) => {
        return utilityFunction([[index + 1]]);
      });

      const layout = this.layoutDescriptors(
        positions,
        objective.verbalData.options.map((_o, i) => {
          if (!isRichTextEmpty(objective.verbalData.comments[i])) {
            return TradeoffComparisonComponent.MAX_PERCENT_HEIGHT;
          } else {
            return 0;
          }
        }),
      );

      objective.verbalData.options.forEach((option, index) => {
        if (!isRichTextEmpty(objective.verbalData.comments[index])) {
          descriptors.push({
            ...layout[index],
            rich: true,
            description: objective.verbalData.comments[index],
          });
        } else {
          descriptors.push({
            ...layout[index],
            rich: false,
            description: option,
          });
        }
      });
    } else if (objective.objectiveType === ObjectiveType.Indicator) {
      const stages =
        this.indicatorDescriptionModes[columnIdx] === 'suggested' && !objective.indicatorData.useCustomAggregation
          ? objective.indicatorData.generateSuggestedVerbalStages(0.01)
          : objective.indicatorData.stages;
      const descriptions = stages
        .filter(stage => !isRichTextEmpty(stage.description))
        .map(stage => {
          const value = stage.value;
          const description = stage.description;

          const range = objective.indicatorData.bestValue - objective.indicatorData.worstValue;

          const position = (value - objective.indicatorData.worstValue) / range;

          return { position, description };
        })
        .sort((a, b) => a.position - b.position);

      const layout = this.layoutDescriptors(
        descriptions.map(d => d.position),
        descriptions.map(_ => TradeoffComparisonComponent.MAX_PERCENT_HEIGHT),
      );

      descriptors.push(...layout.map((p, i) => ({ ...p, rich: true, description: descriptions[i].description })));
    }

    return descriptors;
  }

  private layoutDescriptors(positions: number[], initialHeights: number[]) {
    const heights = initialHeights.slice();
    let overlap = false;
    do {
      overlap = false;
      for (let i = 0; i < positions.length; i++) {
        const position = positions[i];
        let top = position + heights[i] / 100 / 2;

        for (let a = i + 1; a < positions.length; a++) {
          const otherPosition = positions[a];
          const otherBottom = otherPosition - heights[a] / 100 / 2;

          if (otherBottom < top) {
            overlap = true;
            if (heights[i] >= 1) {
              heights[i]--;
              top = position + heights[i] / 100 / 2;
            }
            if (heights[a] >= 1 && heights[a] >= heights[i]) {
              heights[a]--;
            }
          }
        }
      }
    } while (overlap);

    return positions.map((p, i) => ({ position: p, height: heights[i] }));
  }

  private makeValues(objective: Objective) {
    const values: Value[] = [];
    if (objective.objectiveType === ObjectiveType.Numerical) {
      const numericalRange = objective.numericalData.to - objective.numericalData.from;
      for (let i = 0; i <= 10; i += 2) {
        const outcome = objective.numericalData.from + (numericalRange * i) / 10;
        values.push({ position: (1 / 10) * i, description: outcome.round(2) + ' ' + objective.numericalData.unit });
      }
    } else if (objective.objectiveType === ObjectiveType.Verbal) {
      for (let i = 0; i <= 100; i += 10) {
        values.push({
          position: i / 100,
          description: i + '%',
        });
      }
    } else if (objective.objectiveType === ObjectiveType.Indicator) {
      const indicatorRange = objective.indicatorData.bestValue - objective.indicatorData.worstValue;
      for (let i = 0; i <= 10; i += 1) {
        const outcome = objective.indicatorData.worstValue + (indicatorRange * i) / 10;
        values.push({ position: (1 / 10) * i, description: outcome.round(2) + ' ' + objective.indicatorData.aggregatedUnit });
      }
    }

    return values;
  }
}
