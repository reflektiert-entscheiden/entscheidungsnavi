import { ChangeDetectionStrategy, Component, HostBinding, Input } from '@angular/core';

export type Value = { position: number; description: string };

@Component({
  selector: 'dt-scale-values',
  templateUrl: './scale-values.component.html',
  styleUrls: ['./scale-values.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ScaleValuesComponent {
  @Input()
  points: Value[];

  @Input()
  align: 'left' | 'right' = 'left';

  @HostBinding('style.align-items')
  get alignItems() {
    return this.align === 'left' ? 'flex-start' : 'flex-end';
  }
}
