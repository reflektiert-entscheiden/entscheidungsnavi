import { ChangeDetectionStrategy, ChangeDetectorRef, Component, EventEmitter, Input, NgZone, Output } from '@angular/core';
import { OnDestroyObservable } from '@entscheidungsnavi/tools';
import { clamp } from 'lodash';
import { fromEvent, merge, Observable } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'dt-double-slider',
  templateUrl: './double-slider.component.html',
  styleUrls: ['./double-slider.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DoubleSliderComponent {
  @Input()
  valueA = 1; // range [0, 1]

  @Input()
  valueB = 0; // range [0, 1]

  @Input()
  ticks: number[] = [];

  @Input()
  positionToValue: (position: number) => string;

  @Output()
  valueAChange = new EventEmitter<number>();

  @Output()
  valueBChange = new EventEmitter<number>();

  focusedMarker: 'a' | 'b' = 'a';

  dragData: { marker: 'a' | 'b' | null; heightReference: number; startY: number; startValue: number };

  @OnDestroyObservable()
  private $onDestroy: Observable<void>;

  constructor(
    private cdRef: ChangeDetectorRef,
    private zone: NgZone,
  ) {}

  getPositionMarkerTip(position: number) {
    return this.positionToValue(position);
  }

  dragMarker(marker: 'a' | 'b', event: MouseEvent, heightReference: number) {
    this.focusedMarker = marker;

    this.dragData = { marker, heightReference, startY: event.clientY, startValue: marker === 'a' ? this.valueA : this.valueB };

    this.zone.runOutsideAngular(() => {
      const stopDrag = merge(this.$onDestroy, fromEvent(window, 'mouseleave'), fromEvent(window, 'mouseup'));

      fromEvent(window, 'mousemove')
        .pipe(takeUntil(stopDrag))
        .subscribe({
          next: event => this.mouseMove(event as MouseEvent),
          complete: () => {
            this.dragData = null;
            this.zone.run(() => {
              this.cdRef.detectChanges();
            });
          },
        });
    });
  }

  mouseMove(event: MouseEvent) {
    if (!this.dragData) {
      return;
    }

    const mouseY = event.clientY;
    const yDif = this.dragData.startY - mouseY;
    const relativeYDif = yDif / this.dragData.heightReference;

    const newValue = clamp(this.dragData.startValue + relativeYDif, 0, 1);

    this.zone.run(() => {
      if (this.dragData.marker === 'a') {
        this.valueA = newValue;
        this.valueAChange.emit(this.valueA);
      } else {
        this.valueB = newValue;
        this.valueBChange.emit(this.valueB);
      }

      this.cdRef.detectChanges();
    });
  }
}
