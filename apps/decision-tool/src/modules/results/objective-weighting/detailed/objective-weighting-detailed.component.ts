import { Component } from '@angular/core';
import { AreaSeries, Axis, CircleProperties, LineSeries, Point, TextCircleSeries } from '@entscheidungsnavi/widgets/charts';
import {
  DecisionData,
  getInverseUtilityFunctionClamped,
  getUtilityToDisplayFunction,
  Objective,
  ObjectiveWeight,
  TradeoffPointPair,
} from '@entscheidungsnavi/decision-data';
import { clamp, inRange, range, round, uniq } from 'lodash';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute } from '@angular/router';
import { Interval } from '@entscheidungsnavi/tools';
import { TradeoffComparisonComponent } from './index';

enum ReferencePointSource {
  Alternative, // Points generated from Alternatives
  Edge, // Points at the top-left and bottom-right
  Manual, // Is currently being used for tradeoffs from explanation-III
}

class ReferencePoint {
  source: ReferencePointSource;
  alternativeIdx?: number; // null, if the source is not alternative
  point: [number, number]; // these x and y coordinates are normalized [0,1]
}

@Component({
  templateUrl: 'objective-weighting-detailed.component.html',
  styleUrls: ['objective-weighting-detailed.component.scss'],
})
export class ObjectiveWeightingDetailedComponent {
  objectiveIdx: number;

  get objectives(): Objective[] {
    return this.decisionData.objectives;
  }

  get weighting(): ObjectiveWeight[] {
    return this.decisionData.weights.tradeoffWeights;
  }

  get precision(): number | null {
    return this.weighting[this.objectiveIdx].precision;
  }

  get objective(): Objective {
    return this.objectives[this.objectiveIdx];
  }

  get objectiveRefIdx() {
    return this.decisionData.weights.tradeoffObjectiveIdx;
  }

  get objectiveRef(): Objective {
    return this.objectives[this.objectiveRefIdx];
  }

  /**
   * Returns whether the currently set tradeoff (If it exists otherwise =\> false) matches the objective weight
   */
  get isTradeoffActive() {
    return this.decisionData.weights.isManualTradeoffActive(this.objectiveIdx);
  }

  // Chart options
  lineChartSeries: LineSeries[] = [
    { name: '', series: [], color: '#3b4c5b', circleColor: '#ff1e23' }, // top line (+precision)
    { name: '', series: [], color: '#3b4c5b', circleColor: '#ff1e23' }, // bottom line (-precision)
  ];
  areaChartSeries: AreaSeries[] = [
    { opacity: 0.4, color: '#ff807d', series: [] }, // red area top
    { opacity: 0.4, color: '#ff807d', series: [] }, // red area bottom
    { opacity: 0.2, color: '#6bb9ff', series: [] }, // blue area
  ];
  private circleActiveColor = '#ff1e23'; // red
  textCircleSeries: TextCircleSeries[] = [
    { series: [], color: 'rgba(0, 0, 0, 0.25)' }, // Alternatives/Potential reference points (grey by default)
    { series: [], color: this.circleActiveColor }, // Comparison points
  ];
  xAxis = new Axis({
    displayAxis: true,
    gridLines: true,
    showLabel: true,
  });
  yAxis = new Axis({
    displayAxis: true,
    gridLines: true,
    showLabel: true,
  });

  /* Bezugspunkt */
  // The points that correspond to the alternatives sorted ascending by y-value
  referencePoints: ReferencePoint[];
  // The index of the selected point in the above array
  activeReferencePointIndex: number;

  /* Vergleichspunkt */
  // Boundaries for the comparison point
  comparisonPointXMin: number;
  comparisonPointXMax: number;
  // The point currently selected by the slider
  comparisonPointX: number;
  comparisonPointXOriginal: number;
  // Constant
  comparisonPointSteps = 1000;

  // explanation I
  // the [string, string] tuples always contain the formatted string value
  // of the ref-objective in the first and the value of the other obj
  // in the second entry.
  // AlternativeName is non null whenever the selected reference point is an alternative.
  explanation: {
    alternativeName?: string;
    referencePoint: [string, string];
    atMost: [string, string];
    atLeast?: [string, string];
  };

  // explanation II
  // We distinguish between a linear (numerical or indicator) objective and everything else
  explanation2: {
    targetIncreaseLinear?: { of: string };
    targetIncreaseNonLinear?: { from: string; to: string };
    // In the reference objective, we might need to consider precision -> we save [top, bot] values
    referenceDecreaseLinear?: { of: [string, string] };
    referenceDecreaseNonLinear?: { from: [string, string]; to: [string, string] };
  };

  // The original normalized values from decisionData
  get manualTradeoffNormalized(): TradeoffPointPair {
    return this.decisionData.weights.manualTradeoffs[this.objectiveIdx];
  }

  // Number of significant digits for all values, set by slider
  sigDigits = 2;

  public readonly maxPrecision = 125;
  public readonly machinePrecision = 1e-15;

  // To be used in the template
  // eslint-disable-next-line @typescript-eslint/naming-convention
  Math = Math;
  // eslint-disable-next-line @typescript-eslint/naming-convention
  ReferencePointSource = ReferencePointSource;

  constructor(
    private decisionData: DecisionData,
    private dialog: MatDialog,
    route: ActivatedRoute,
  ) {
    // get the ZielID from the URL/ActivatedRoute
    route.params.subscribe(params => {
      this.objectiveIdx = +params['objectiveId'];
      this.activeReferencePointIndex = this.weighting[this.objectiveIdx].activeReferencePointIndex;
      this.comparisonPointX = this.weighting[this.objectiveIdx].comparisonPointX;
      this.comparisonPointXOriginal = undefined;
      this.onChanges();
    });
  }

  updateSigDigits(newValue: number) {
    this.sigDigits = clamp(newValue, 0, 10);
    this.onChanges();
  }

  onReferencePointChange() {
    this.weighting[this.objectiveIdx].activeReferencePointIndex = this.activeReferencePointIndex;
    if (
      inRange(this.activeReferencePointIndex, this.referencePoints.length) &&
      Math.abs(this.getActiveReferencePoint()[0] - this.comparisonPointX) <= this.machinePrecision
    ) {
      // Reset the comparison point when a change of the reference point
      // - moves both points to the same place
      // - moves the comparison point out of bounds
      this.comparisonPointX = undefined;
    }
    this.onChanges();
  }

  // used to determine whether the comparison point was changed manually or not
  onChanges(changeComparisonPointX?: boolean) {
    this.weighting[this.objectiveRefIdx].value = this.weighting[this.objectiveRefIdx].value ?? 100;
    this.weighting[this.objectiveIdx].value = clamp(this.weighting[this.objectiveIdx].value ?? 100, 1, 1000);
    this.weighting[this.objectiveIdx].precision = clamp(this.weighting[this.objectiveIdx].precision ?? 0, 0, this.maxPrecision);

    this.drawChart(null, changeComparisonPointX);
  }

  adjustWeighting(x: number) {
    this.weighting[this.objectiveIdx].value = clamp(Math.round(this.weighting[this.objectiveIdx].value + x), 1, 1000);
    this.onChanges();
  }

  updatePrecision(x: number) {
    this.weighting[this.objectiveIdx].precision = clamp(x, 0, this.maxPrecision);
    this.onChanges();
  }

  adjustPrecision(x: number) {
    this.updatePrecision(this.weighting[this.objectiveIdx].precision + x);
    this.onChanges();
  }

  /**
   * Draws the chart.
   * @param selectReferencePoint - if not null, then this reference point is selected among all
   *                             reference points.
   * @param changeComparisonPointX - next comparison point to be used
   */
  private drawChart(selectReferencePoint: [number, number] = null, changeComparisonPointX = false) {
    this.formatAxes();

    this.setAndAddPointsToGraph(selectReferencePoint, changeComparisonPointX);
    this.addAreasToGraph();
    this.addExplanationsToGraph();

    // Trigger the change detection for the graph to redraw
    this.lineChartSeries = [...this.lineChartSeries];
  }

  private formatAxes() {
    this.formatAxis(this.xAxis, this.objective);
    this.formatAxis(this.yAxis, this.objectiveRef);
  }

  private setAndAddPointsToGraph(selectReferencePoint: [number, number], changeComparisonPointX: boolean) {
    this.setNonActiveReferencePoints();
    this.setActiveReferencePoint(selectReferencePoint);
    this.addReferencePointsToGraph();
    this.addDataPointsToGraph(changeComparisonPointX);
  }

  private setNonActiveReferencePoints() {
    this.setAlternativeReferencePoints();
    this.setEdgeReferencePoints();
    this.setManualTradeoffReferencePoint();
    this.sortNonActiveReferencePoints();
  }

  private setAlternativeReferencePoints() {
    this.referencePoints = this.decisionData.alternatives.map((_, index) => {
      // Calculate values of the alternative in the two objectives
      const xUtility = this.decisionData.outcomes[index][this.objectiveIdx].getUtility(this.objective);
      const yUtility = this.decisionData.outcomes[index][this.objectiveRefIdx].getUtility(this.objectiveRef);

      return {
        source: ReferencePointSource.Alternative,
        alternativeIdx: index,
        point: [xUtility, yUtility],
      } as ReferencePoint;
    });
  }

  private setEdgeReferencePoints() {
    // Add the edge points, if not already present
    const edgePoints: Array<[number, number]> = [
      [0, 1],
      [1, 0],
    ];

    edgePoints.forEach(edgePoint => {
      if (this.referencePoints.find(point => point.point[0] === edgePoint[0] && point.point[1] === edgePoint[1]) == null) {
        this.referencePoints.push({
          source: ReferencePointSource.Edge,
          point: edgePoint,
        });
      }
    });
  }

  private setManualTradeoffReferencePoint() {
    // Add the manual tradeoff point that has been manually added through explanation III
    if (this.isTradeoffActive) {
      this.referencePoints.push(
        ...this.manualTradeoffNormalized.map(point => ({
          source: ReferencePointSource.Manual,
          point,
        })),
      );
    }
  }

  private sortNonActiveReferencePoints() {
    // Sort mainly by ascending x-coordinate and secondly descending by y-coordinate (top-left to bottom-right)
    const topLeftToBottomRightSorter = (rpA: ReferencePoint, rpB: ReferencePoint) =>
      rpA.point[0] === rpB.point[0] ? rpB.point[1] - rpA.point[1] : rpA.point[0] - rpB.point[0];
    this.referencePoints.sort(topLeftToBottomRightSorter);
  }

  private setActiveReferencePoint(selectReferencePoint: [number, number] | null) {
    // We try to select the given reference point (-1 if non-existent -> clamped in next line)
    if (selectReferencePoint != null) {
      this.activeReferencePointIndex = this.referencePoints.findIndex(
        rp => rp.point[0] === selectReferencePoint[0] && rp.point[1] === selectReferencePoint[1],
      );
    } else if (this.activeReferencePointIndex == null) {
      this.activeReferencePointIndex = Math.round((this.referencePoints.length - 1) / 2);
    }
    this.activeReferencePointIndex = clamp(this.activeReferencePointIndex, 0, this.referencePoints.length - 1);
  }

  private getActiveReferencePoint() {
    return this.referencePoints[this.activeReferencePointIndex].point;
  }

  private getComparisonPoints(): {
    comparisonPoint1: [number, number];
    comparisonPoint2: [number, number];
    comparisonPointAtMost: [number, number];
    comparisonPointAtLeast: [number, number];
  } {
    const referencePoint = this.getActiveReferencePoint();
    const { utilToDisplayFct, utilToDisplayFctRef } = this.getUtilityDisplayFunctions();

    /*
      Calculate and add the comparison points (two if precision > 0, else one)
     */
    let tradeoffFunction = this.getTradeoffFunction(referencePoint, +this.precision);
    const comparisonPoint1: [number, number] = [
      utilToDisplayFct(this.comparisonPointX),
      utilToDisplayFctRef(tradeoffFunction(this.comparisonPointX)),
    ];
    let comparisonPoint2: [number, number] = null;

    this.textCircleSeries[1].series = [
      {
        name: comparisonPoint1[0],
        value: [comparisonPoint1[1], { id: 0 }],
      },
    ];

    if (this.precision > 0) {
      tradeoffFunction = this.getTradeoffFunction(referencePoint, -this.precision);
      comparisonPoint2 = [utilToDisplayFct(this.comparisonPointX), utilToDisplayFctRef(tradeoffFunction(this.comparisonPointX))];
      this.textCircleSeries[1].series.push({
        name: comparisonPoint2[0],
        value: [comparisonPoint2[1], { id: 1 }],
      });
    }

    // Calculate which of the two values is the higher one (in terms of utility, not value)
    let comparisonPointAtMost: [number, number];
    let comparisonPointAtLeast: [number, number];
    if (this.precision > 0) {
      if (this.comparisonPointX > referencePoint[0]) {
        comparisonPointAtMost = comparisonPoint2;
        comparisonPointAtLeast = comparisonPoint1;
      } else {
        comparisonPointAtMost = comparisonPoint1;
        comparisonPointAtLeast = comparisonPoint2;
      }
    } else {
      comparisonPointAtMost = comparisonPoint1;
      comparisonPointAtLeast = comparisonPoint1;
    }
    return {
      comparisonPoint1,
      comparisonPoint2,
      comparisonPointAtMost,
      comparisonPointAtLeast,
    };
  }

  private addReferencePointsToGraph() {
    const { utilToDisplayFct, utilToDisplayFctRef } = this.getUtilityDisplayFunctions();

    this.textCircleSeries[0].series = this.referencePoints.map((rp, index) => {
      return {
        name: utilToDisplayFct(rp.point[0]),
        value: [
          utilToDisplayFctRef(rp.point[1]),
          {
            tag: rp.source,
            text: rp.source === ReferencePointSource.Alternative ? this.decisionData.alternatives[rp.alternativeIdx].name : null,
            showText: rp.source === ReferencePointSource.Alternative && index === this.activeReferencePointIndex,
            color: index === this.activeReferencePointIndex ? this.circleActiveColor : undefined,
            textColor: 'gray',
          },
        ],
      } as Point<[number, CircleProperties]>;
    });
  }

  private addDataPointsToGraph(changeComparisonPointX: boolean) {
    const xs = this.calculateXs(changeComparisonPointX);

    const activeReferencePoint = this.getActiveReferencePoint();

    /*
      Calculate data points for the line
     */
    if (this.precision > 0) {
      this.lineChartSeries[0].series = this.calculateYs(xs, activeReferencePoint, this.precision);
      this.lineChartSeries[0].name = this.objectiveRef.name + ' + ' + $localize`Ungenauigkeit`;
      this.lineChartSeries[1].series = this.calculateYs(xs, activeReferencePoint, -this.precision);
      this.lineChartSeries[1].name = this.objectiveRef.name + ' - ' + $localize`Ungenauigkeit`;
    } else {
      this.lineChartSeries[0].series = this.calculateYs(xs, activeReferencePoint, 0);
      this.lineChartSeries[0].name = this.objectiveRef.name;
      this.lineChartSeries[1].series = [];
    }
  }

  private addAreasToGraph() {
    const { comparisonPoint1, comparisonPoint2, comparisonPointAtLeast } = this.getComparisonPoints();
    const referencePoint = this.getActiveReferencePoint();
    const precision = this.weighting[this.objectiveIdx].precision;
    const { utilToDisplayFct, utilToDisplayFctRef } = this.getUtilityDisplayFunctions();

    // The reference value in the display scale [von, bis]/[0,1] instead of normalized [0,1]
    const referencePointDisplay = [utilToDisplayFct(referencePoint[0]), utilToDisplayFctRef(referencePoint[1])];

    // Add the red areas between the two comparison points and the reference point
    this.areaChartSeries[0].series = [
      { name: referencePointDisplay[0], value: [referencePointDisplay[1], comparisonPoint1[1]] },
      { name: comparisonPoint1[0], value: [referencePointDisplay[1], comparisonPoint1[1]] },
    ];
    if (precision > 0) {
      this.areaChartSeries[1].series = [
        { name: referencePointDisplay[0], value: [referencePointDisplay[1], comparisonPoint2[1]] },
        { name: comparisonPoint1[0], value: [referencePointDisplay[1], comparisonPoint2[1]] },
      ];
    } else {
      this.areaChartSeries[1].series = [];
    }

    // Add the blue area between the x-values
    let topMostYValue: number;
    if (this.comparisonPointX > referencePoint[0]) {
      topMostYValue = referencePointDisplay[1];
    } else {
      topMostYValue = comparisonPointAtLeast[1];
    }
    this.areaChartSeries[2].series = [
      { name: referencePointDisplay[0], value: [utilToDisplayFctRef(0), topMostYValue] },
      { name: comparisonPoint1[0], value: [utilToDisplayFctRef(0), topMostYValue] },
    ];
  }

  private getUtilityDisplayFunctions() {
    const utilToDisplayFct = getUtilityToDisplayFunction(this.objective);
    const utilToDisplayFctRef = getUtilityToDisplayFunction(this.objectiveRef);
    return { utilToDisplayFct, utilToDisplayFctRef };
  }

  private addExplanationsToGraph() {
    const { comparisonPointAtMost, comparisonPointAtLeast } = this.getComparisonPoints();

    const referencePointObject = this.referencePoints[this.activeReferencePointIndex];
    const referencePoint = referencePointObject.point;
    const precision = this.precision;

    const { utilToDisplayFct, utilToDisplayFctRef } = this.getUtilityDisplayFunctions();
    const referencePointDisplay = [utilToDisplayFct(referencePoint[0]), utilToDisplayFctRef(referencePoint[1])];

    /*
      Calculate the remaining explanation data
     */
    this.explanation = {
      alternativeName:
        referencePointObject.source === ReferencePointSource.Alternative
          ? this.decisionData.alternatives[referencePointObject.alternativeIdx].name
          : null,
      referencePoint: [this.xAxis.format(referencePointDisplay[0]), this.yAxis.format(referencePointDisplay[1])],
      atLeast: precision > 0 ? [this.xAxis.format(comparisonPointAtLeast[0]), this.yAxis.format(comparisonPointAtLeast[1])] : null,
      atMost: [this.xAxis.format(comparisonPointAtMost[0]), this.yAxis.format(comparisonPointAtMost[1])],
    };

    this.explanation2 = {};

    if (this.objective.hasLinearUtilityFunction) {
      this.explanation2.targetIncreaseLinear = { of: this.xAxis.format(Math.abs(referencePointDisplay[0] - comparisonPointAtMost[0])) };
    } else {
      let targetIncreaseNonLinear = Interval.from(referencePointDisplay[0]).to(comparisonPointAtMost[0]).ordered();

      if (this.objective.isScaleReverse) targetIncreaseNonLinear = targetIncreaseNonLinear.reverse();

      this.explanation2.targetIncreaseNonLinear = {
        from: this.xAxis.format(targetIncreaseNonLinear.start),
        to: this.xAxis.format(targetIncreaseNonLinear.end),
      };
    }

    if (this.objectiveRef.hasLinearUtilityFunction) {
      this.explanation2.referenceDecreaseLinear = {
        of: [
          this.yAxis.format(Math.abs(referencePointDisplay[1] - comparisonPointAtMost[1])),
          // at least might be null, in that case both are equal
          this.yAxis.format(Math.abs(referencePointDisplay[1] - comparisonPointAtLeast[1])),
        ],
      };
    } else {
      let referenceObjectiveLevelChangeWithRespectToMost = Interval.from(referencePointDisplay[1]).to(comparisonPointAtMost[1]).ordered();
      let referenceObjectiveLevelChangeWithRespectToLeast = Interval.from(referencePointDisplay[1]).to(comparisonPointAtLeast[1]).ordered();

      if (this.objectiveRef.isScaleReverse) {
        referenceObjectiveLevelChangeWithRespectToMost = referenceObjectiveLevelChangeWithRespectToMost.reverse();
        referenceObjectiveLevelChangeWithRespectToLeast = referenceObjectiveLevelChangeWithRespectToLeast.reverse();
      }

      this.explanation2.referenceDecreaseNonLinear = {
        from: [
          this.yAxis.format(referenceObjectiveLevelChangeWithRespectToMost.end),
          this.yAxis.format(referenceObjectiveLevelChangeWithRespectToLeast.end),
        ],
        to: [
          this.yAxis.format(referenceObjectiveLevelChangeWithRespectToMost.start),
          this.yAxis.format(referenceObjectiveLevelChangeWithRespectToLeast.start),
        ],
      };
    }
  }

  /**
   * Set the properties of the axis to match the given objective.
   *
   * @param axis - axis in question
   * @param objective - objective to which axis should be adjusted to
   */
  private formatAxis(axis: Axis, objective: Objective) {
    const unit = this.getObjectiveUnit(objective);
    axis.labelText = objective.name;

    // The format functions are not only used for the ticks, but also for other points
    if (objective.isVerbal) {
      axis.min = 0;
      axis.max = 100;

      // Add all discrete states of the verbal objective
      axis.ticks = objective.verbalData.utilities;
      axis.format = (val: number) => {
        const index = objective.verbalData.utilities.indexOf(val);
        val = round(val, this.sigDigits);
        return index === -1 ? `${val}${unit}` : `${objective.verbalData.options[index]} (${val}${unit})`;
      };
    } else {
      // Numerical or indicator
      [axis.min, axis.max] = objective.isNumerical
        ? [objective.numericalData.from, objective.numericalData.to]
        : [objective.indicatorData.worstValue, objective.indicatorData.bestValue];
      axis.format = (val: number) => `${round(val, this.sigDigits)}${unit}`;
      // We keep the ticks at the edges
      axis.ticks = [axis.min, axis.max];
      // Between those edges, we try to have at most 5 more ticks
      const tickDistance = Math.ceil(Math.abs(axis.max - axis.min) / 5);
      const sign = Math.sign(axis.max - axis.min);
      for (let tick = axis.min + sign * tickDistance; sign * tick < sign * axis.max; tick += sign * tickDistance) {
        axis.ticks.push(tick);
      }
    }
  }

  private calculateXs(changeComparisonPointX: boolean): number[] {
    /*
      Calculate the x-values that should be explicitly calculated
     */
    // Take x-values every 1%, [0,1]
    let xs = range(0, 1.01, 0.01);
    // For verbal objectives add points for the discrete states
    if (this.objective.verbalData) {
      // Add x-values for the discrete states of the verbal objective (if not already present)
      xs.push(...this.objective.verbalData.utilities.map(x => x / 100));
    }

    /*
      Calculate the valid x-interval in which the tradeoff-function fits
     */
    // this point is given as utility value in [0,1] for x and y
    const activeReferencePoint = this.getActiveReferencePoint();
    const precision = this.precision;

    // Calculate the valid x-range for the top line
    let invTradeoffFct = this.getInverseTradeoffFunction(activeReferencePoint, precision);
    [this.comparisonPointXMin, this.comparisonPointXMax] = [clamp(invTradeoffFct(1), 0, 1), clamp(invTradeoffFct(0), 0, 1)];
    // The point is chosen to be on the far side of the reference point, if it is undefined or out of bounds
    if (this.comparisonPointX == null) {
      // initialize suitable comparison point
      this.comparisonPointX =
        Math.abs(this.comparisonPointXMin - activeReferencePoint[0]) > Math.abs(this.comparisonPointXMax - activeReferencePoint[0])
          ? this.comparisonPointXMin
          : this.comparisonPointXMax;
    } else if (this.comparisonPointX < this.comparisonPointXMin - this.machinePrecision) {
      this.comparisonPointX = this.comparisonPointXMin;
    } else if (this.comparisonPointX > this.comparisonPointXMax + this.machinePrecision) {
      this.comparisonPointX = this.comparisonPointXMax;
    } else {
      if (!changeComparisonPointX && this.comparisonPointXOriginal != null) {
        this.comparisonPointX = this.comparisonPointXOriginal;
      }
      this.comparisonPointX = clamp(this.comparisonPointX, this.comparisonPointXMin, this.comparisonPointXMax);
    }

    // if changed manually or not initialized
    if (changeComparisonPointX || this.comparisonPointXOriginal == null) {
      this.comparisonPointXOriginal = this.comparisonPointX;
      this.weighting[this.objectiveIdx].comparisonPointX = this.comparisonPointX;
    }

    // Add the comparisonPoint to the xs
    xs.push(this.comparisonPointX);

    // Calculate the valid x-range for the top line and add it to the xs
    xs.push(this.comparisonPointXMin, this.comparisonPointXMax);

    // Calculate the valid x-range for the bottom line and add it to the xs
    invTradeoffFct = this.getInverseTradeoffFunction(activeReferencePoint, -precision);
    xs.push(clamp(invTradeoffFct(1), 0, 1), clamp(invTradeoffFct(0), 0, 1));

    // Round to 15 decimal places (machine accuracy usually 10^-16) and remove duplicate xs
    xs = uniq(xs.map(x => round(x, 15)));

    return xs;
  }

  private calculateYs(xs: number[], referencePoint: [number, number], precision: number): Array<Point<number>> {
    const { utilToDisplayFct, utilToDisplayFctRef } = this.getUtilityDisplayFunctions();

    // Line function according to the niveau and weights
    const lineFct = this.getTradeoffFunction(referencePoint, precision);
    // lineFct = precision > 0 ? this.getTradeoffFunction(referencePoint, 0) : this.getInverseTradeoffFunction(referencePoint, 0);

    return (
      xs
        .map(x => ({
          name: x,
          value: lineFct(x),
        }))
        // Constrain to the visible space
        .filter(p => p.value > -0.01 && p.value < 1.01)
        .map(p => ({
          name: utilToDisplayFct(p.name),
          value: utilToDisplayFctRef(p.value),
        }))
    );
  }

  /**
   * Return the string representing the unit for the given objective (with a leading whitespace, it non-empty).
   */
  getObjectiveUnit(objective: Objective) {
    if (objective.isNumerical) {
      return objective.numericalData.unit != null && objective.numericalData.unit.length > 0 ? ` ${objective.numericalData.unit}` : '';
    } else if (objective.isIndicator) {
      return ` ${objective.indicatorData.aggregatedUnit}`;
    } else {
      return ' %';
    }
  }

  /**
   * Calculates the slope of the tradeoff function depending on a precision parameter.
   *
   * @remarks
   * The slope is computed depending on the weights of the two objectives in the tradeoff.
   * {@link precision} acts as a small disturbance to express uncertainty in the function definition.
   * Usually its is used to render to functions, one with +precision and
   * one with -precision to display two functions - one as an upper bound for the y-values and one for the lower bound.
   *
   * @param precision - small disturbance
   */
  private getSlope(precision = 0) {
    const objectiveWeighting = clamp(precision + this.weighting[this.objectiveIdx].value, 1, 1000);
    return -objectiveWeighting / this.weighting[this.objectiveRefIdx].value;
  }

  /**
   * Get the shown tradeoff function corresponding to the current weights.
   *
   * @remarks
   * The {@link referencePoint} must be given in normalized form (coordinates must be in [0,1]).
   *
   * @param referencePoint - (only) fixpoint of the (linear) tradeoff function
   * @param precision - passed into {@link getSlope}
   */
  private getTradeoffFunction(referencePoint: [number, number], precision = 0) {
    return (x: number) => this.getSlope(precision) * (x - referencePoint[0]) + referencePoint[1];
  }

  /**
   * Get the inverse of the shown tradeoff function.
   *
   * @remarks
   * The {@link referencePoint} must be given in normalized form (coordinates must be in [0,1]).
   *
   * @see {@link getTradeoffFunction}
   *
   * @param referencePoint - corresponds to parameter `referencePoint` of {@link getTradeoffFunction}
   * @param precision - corresponds to parameter `precision` of {@link getTradeoffFunction}
   */
  private getInverseTradeoffFunction(referencePoint: [number, number], precision = 0) {
    return (y: number) => (y - referencePoint[1]) / this.getSlope(precision) + referencePoint[0];
  }

  openTradeoffModal() {
    const activeReferencePoint = this.getActiveReferencePoint();
    const tradeoffFunction = this.getTradeoffFunction(activeReferencePoint, 0);

    const refObjectiveReverseUtilFct = getInverseUtilityFunctionClamped(this.objectiveRef);
    const objectiveReverseUtilFct = getInverseUtilityFunctionClamped(this.objective);

    let initialValues: [[number, number], [number, number]] = [
      [0, 1],
      [1, 0],
    ];

    if (this.decisionData.weights.manualTradeoffs[this.objectiveIdx]) {
      // noinspection UnnecessaryLocalVariableJS
      const existingTradeoff = this.decisionData.weights.manualTradeoffs[this.objectiveIdx];

      [[initialValues[1][0], initialValues[0][0]], [initialValues[1][1], initialValues[0][1]]] = existingTradeoff;

      initialValues[0] = initialValues[0].map(v => refObjectiveReverseUtilFct(v)) as [number, number];
      initialValues[1] = initialValues[1].map(v => objectiveReverseUtilFct(v)) as [number, number];
    } else {
      initialValues = [
        [refObjectiveReverseUtilFct(activeReferencePoint[1]), refObjectiveReverseUtilFct(tradeoffFunction(this.comparisonPointX))],
        [objectiveReverseUtilFct(activeReferencePoint[0]), objectiveReverseUtilFct(this.comparisonPointX)],
      ];
    }

    this.dialog
      .open(TradeoffComparisonComponent, {
        data: {
          objectiveIndex1: this.objectiveRefIdx,
          objectiveIndex2: this.objectiveIdx,
          initialValues,
        },
      })
      .afterClosed()
      .subscribe(() => {
        this.onChanges();

        if (this.isTradeoffActive) {
          const [pointA, pointB] = this.manualTradeoffNormalized;
          this.comparisonPointX = pointB[0];
          this.drawChart(pointA, true);
          this.onReferencePointChange();
        }
      });
  }
}
