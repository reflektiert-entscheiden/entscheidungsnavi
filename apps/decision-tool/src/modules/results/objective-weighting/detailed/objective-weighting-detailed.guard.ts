import { inject } from '@angular/core';
import { CanActivateFn, createUrlTreeFromSnapshot } from '@angular/router';
import { inRange } from 'lodash';
import { ObjectiveWeight } from '@entscheidungsnavi/decision-data';
import { DecisionData } from '@entscheidungsnavi/decision-data';

export const objectiveWeightingDetailedGuard: CanActivateFn = (route, _state) => {
  const decisionData = inject(DecisionData);
  const referenceObjectiveIdx = decisionData.weights.tradeoffObjectiveIdx;

  const activeIdx = +route.params['objectiveId'];

  if (
    referenceObjectiveIdx == null ||
    isNaN(activeIdx) ||
    activeIdx === referenceObjectiveIdx ||
    !inRange(activeIdx, 0, decisionData.objectives.length)
  ) {
    // Go back to the overview page
    return createUrlTreeFromSnapshot(route, ['../..']);
  }

  // Make sure the tradeoff weights for both the refence and the active objective are set.
  if (decisionData.weights.tradeoffWeights[referenceObjectiveIdx] == null) {
    const gew = decisionData.weights.preliminaryWeights[referenceObjectiveIdx];
    decisionData.weights.tradeoffWeights[referenceObjectiveIdx] = new ObjectiveWeight(gew.value, gew.precision);
  }
  if (decisionData.weights.tradeoffWeights[activeIdx] == null) {
    const gew = decisionData.weights.preliminaryWeights[activeIdx];
    decisionData.weights.tradeoffWeights[activeIdx] = new ObjectiveWeight(gew.value, gew.precision);
  }

  // Mark the tradeoff as verified, because we've opened the details page
  decisionData.weights.unverifiedWeights[activeIdx] = false;

  return true;
};
