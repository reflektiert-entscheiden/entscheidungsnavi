import { round } from 'lodash';
import { ObjectiveWeight } from '@entscheidungsnavi/decision-data';

export function getPercentages(gewichtung: ObjectiveWeight[], decimalPlaces = 1): Array<[number, number]> {
  if (gewichtung.some(g => !Number.isFinite(g.value))) {
    return gewichtung.map(() => [undefined, undefined]);
  }

  const sum = gewichtung
    .map(g => g.value)
    .reduce((acc, value) => {
      return (acc || 0) + value;
    });
  if (sum === 0) {
    return gewichtung.map(() => [0, 0]);
  } else {
    return gewichtung.map(w => [round(((w.value ?? 0) / sum) * 100, decimalPlaces), round((w.precision / sum) * 100, decimalPlaces)]);
  }
}
