import { Component } from '@angular/core';
import { DecisionData } from '@entscheidungsnavi/decision-data';
import { DisplayAtMaxWidth } from '../../../../app/interfaces/display-at-max-width';
import { HelpMenuProvider, helpPage } from '../../../../app/help/help';
import { Help1Component } from '../../help/objective-weighting/help1/help1.component';
import { Help2Component } from '../../help/objective-weighting/help2/help2.component';
import { HelpBackgroundComponent } from '../../help/help-background/help-background.component';
import { Navigation, NavLine, navLineElement } from '../../../shared/navline';
import { DecisionDataState } from '../../../shared/decision-data-state';

@Component({
  selector: 'dt-objective-weighting',
  templateUrl: 'objective-weighting-main.component.html',
  styleUrls: ['objective-weighting-main.component.scss', '../../../hints.scss'],
})
@DisplayAtMaxWidth
export class ObjectiveWeightingMainComponent implements Navigation, HelpMenuProvider {
  navLine = new NavLine({
    left: [navLineElement().back('/results/steps/1').build()],
    right: [navLineElement().continue('/results').build()],
  });

  helpMenu = [
    helpPage()
      .name($localize`So funktioniert's`)
      .component(Help1Component)
      .build(),
    helpPage()
      .name($localize`Weitere Hinweise`)
      .component(Help2Component)
      .build(),
    helpPage()
      .name($localize`Hintergrundwissen zum Schritt 5`)
      .component(HelpBackgroundComponent)
      .build(),
    helpPage()
      .name($localize`Videos`)
      .youtube(4)
      .build(),
  ];

  selectedObjective = -1;

  get objective() {
    return this.decisionData.objectives[this.selectedObjective];
  }

  constructor(
    protected decisionData: DecisionData,
    protected decisionDataState: DecisionDataState,
  ) {}
}
