import { Component, EventEmitter, Output } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { OnDestroyObservable } from '@entscheidungsnavi/tools';
import { Observable, takeUntil } from 'rxjs';
import { getCurrentUrl } from '@entscheidungsnavi/widgets';
import { DecisionData } from '@entscheidungsnavi/decision-data';

@Component({
  selector: 'dt-objective-weighting-navigation',
  template: `
    <dt-select-line [align]="'start'">
      <dt-select
        [selectedOptions]="selectedObjective"
        (selectedOptionsChange)="navigateTo($event)"
        [disabled]="referenceObjectiveIndex == null"
      >
        <dt-select-label i18n>Zielgewicht</dt-select-label>
        <dt-select-option [value]="-1" i18n>Übersicht</dt-select-option>
        @for (objective of decisionData.objectives; track objective; let index = $index) {
          @if (index !== referenceObjectiveIndex) {
            <dt-select-option [value]="index">
              {{ objective.name }}
            </dt-select-option>
          }
        }
      </dt-select>
    </dt-select-line>
  `,
  styles: [
    `
      dt-select-line {
        margin-bottom: 8px;
      }
    `,
  ],
})
export class ObjectiveWeightingNavigationComponent {
  private readonly detailUrlRegex = /detailed\/(\d+)$/;

  @OnDestroyObservable()
  private onDestroy$: Observable<void>;

  selectedObjective = -1;
  @Output() selectedObjectiveChange = new EventEmitter<number>(true);

  get referenceObjectiveIndex() {
    return this.decisionData.weights.tradeoffObjectiveIdx;
  }

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    protected decisionData: DecisionData,
  ) {
    getCurrentUrl()
      .pipe(takeUntil(this.onDestroy$))
      .subscribe(url => {
        const match = this.detailUrlRegex.exec(url);
        this.selectedObjective = match != null ? +match[1] : -1;
        this.selectedObjectiveChange.emit(this.selectedObjective);
      });
  }

  navigateTo(position: number) {
    this.router.navigate(position === -1 ? ['.'] : ['detailed', position], { relativeTo: this.route });
  }
}
