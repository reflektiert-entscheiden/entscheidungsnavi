import { ChangeDetectionStrategy, Component, EventEmitter, Input, OnChanges, Output, SimpleChanges, TrackByFunction } from '@angular/core';
import { range, sum } from 'lodash';
import { DecisionData } from '@entscheidungsnavi/decision-data';
import { SelectLineComponent } from '@entscheidungsnavi/widgets/select';
import { WidgetsModule } from '@entscheidungsnavi/widgets';
import { CommonModule } from '@angular/common';
import { ExplainableModule } from '../../shared/explanation/explainable.module';

@Component({
  selector: 'dt-chart-ranking',
  styleUrls: ['chart-ranking.component.scss'],
  templateUrl: 'chart-ranking.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
  imports: [CommonModule, SelectLineComponent, WidgetsModule, ExplainableModule],
  standalone: true,
})
export class ChartRankingComponent implements OnChanges {
  private static sortWithRespectTo(valuesToSort: number[], ranking: number[]) {
    return valuesToSort.sort((i: number, j: number) => ranking[j] - ranking[i]);
  }

  @Input()
  explainable = false;

  @Input()
  showHeader = true;

  @Input()
  numberDigitsInfo = '1.2-2';

  /*
    N = number of alternatives (decisionData.alternatives.length)
    M = number of objectives (decisionData.objectives.length)
    P = number of selected alternatives (selectedAlternatives.length)
  */

  // Array of size N with utility values of all alternatives in decisionData.
  @Input() utilityValues: number[];

  // Optional array of size N with utility values that should be considered as reference values for ranking changes.
  @Input() initialUtilityValues?: number[];

  // Array of size P that contains the utility values for all alternatives
  // that result from ignoring all objectives marked by ignoredObjectives.
  adjustedUtilityValues: number[];

  /*
    Optional matrix of size N x M that partitions the alternative's utility values into
    the contributing utility values from each objective in isolation.

    Its format is as follows:

    partitionedUtilityValues[selectedAlternativeIndex] = [
      utility value for alternative A from objective 0,
      ...,
      utility value for alternative A in objective M - 1
    ]

    A = selectedAlternatives[selectedAlternativeIndex]
  */
  @Input() weightedUtilityMatrix?: number[][];

  // Array of size M with names that are shown on hover (if displayMode = 'detailed').
  get objectiveNames() {
    return this.decisionData.objectives.map(objective => objective.name);
  }

  @Input() allowIgnoringObjectives = false;

  // Array of size N that determines if an objective should be ignored or not.
  // Ignored objectives are excluded in the calculation of adjustedUtilityValues and
  // are hidden in the partitioning of the utility value (if displayMode = 'detailed').
  ignoredObjectives: boolean[] = [];

  private ignoredObjectivesCount = 0;

  @Input() enableAlternativeSelection = false;

  // Array of size P of indices that are referencing alternatives in the decisionData.alternatives array.
  // Must not be smaller than minAlternatives.
  private _selectedAlternatives: number[] = range(this.decisionData.alternatives.length);

  get selectedAlternatives() {
    return this._selectedAlternatives;
  }
  @Input()
  set selectedAlternatives(newSelectedAlternatives: number[]) {
    this._selectedAlternatives = newSelectedAlternatives;
    if (this.utilityValues) this.updateData({ initialOrderOfAlternatives: true, orderOfAlternatives: true });
    this.selectedAlternativesChange.emit(newSelectedAlternatives);
  }

  @Output() selectedAlternativesChange = new EventEmitter<number[]>();

  // Minimum number of alternatives that have to stay selected.
  @Input() minAlternatives = 1;

  // Currently selected objective (if displayMode = 'detailed').
  hoverPartition = -1;

  // The alternative index of the winner alternative.
  winnerAlternativeIndex: number;

  // Array of size P that indicates the rank change of an alternative index.
  // Difference is computed between initialOrderOfAlternatives and orderOfAlternatives.
  rankingChangeArray: number[] = [];

  // Defines the display order of the alternatives (descending).
  orderOfAlternatives: number[];

  // Snapshot of orderOfAlternatives updated on initialization and alternative selection change.
  // If initialValues or allowIgnoringColumns is set this contains the original order.
  initialOrderOfAlternatives: number[];

  @Input() displayMode: 'simple' | 'detailed' = 'simple';
  @Output() displayModeChange = new EventEmitter();

  @Input() orderMode: 'utilityValues' | 'gutFeeling' = 'utilityValues';
  @Output() orderModeChange = new EventEmitter();

  // Whether the Objective Names are still on the screen
  namesVisible = true;

  colors = [
    'hsl(208deg 35% 40%)',
    'hsl(0deg 50% 40%)',
    'hsl(28deg 75% 45%)',
    'hsl(448deg 35% 40%)',
    'hsl(268deg 35% 40%)',
    'hsl(174 50% 40%)',
  ];

  isMobile = false;

  get showRankingChange() {
    return !!this.initialUtilityValues || (this.canIgnoreObjectives && this.orderMode === 'utilityValues');
  }

  get selectedAlternativeIndicesOrdered() {
    const alternativeIndices =
      this.orderMode === 'gutFeeling' ? this.orderOfAlternatives : this.initialOrderOfAlternatives ?? this.orderOfAlternatives;

    return alternativeIndices.filter(index => this.selectedAlternatives.includes(index));
  }

  get shownValues() {
    return this.allowIgnoringObjectives && this.ignoredObjectivesCount > 0 ? this.adjustedUtilityValues : this.utilityValues;
  }

  get canIgnoreObjectives() {
    return this.allowIgnoringObjectives && this.displayMode === 'detailed';
  }

  get gridColumnTemplate() {
    const columns = [];

    // Ranking
    columns.push('min-content');

    // Ranking Change
    if (this.showRankingChange && !this.isMobile) {
      columns.push('min-content');
    }

    // Caption
    columns.push('fit-content(50%)');

    // Value
    columns.push('1fr');

    return columns.join(' ');
  }

  get gridRowTemplate() {
    return `repeat(${this.selectedAlternatives.length}, minmax(30px, 1fr))`;
  }

  constructor(public decisionData: DecisionData) {}

  // Track Functions
  trackByItem: TrackByFunction<any> = (_: number, item: any) => item;
  trackByRankingChange: TrackByFunction<any> = (index: number, _: any) => this.rankingChangeArray?.[index];

  setOrder(newOrderMode: 'utilityValues' | 'gutFeeling') {
    this.orderMode = newOrderMode;
    this.orderModeChange.emit(newOrderMode);
    this.updateData({ initialOrderOfAlternatives: false, orderOfAlternatives: true });
  }

  setDisplay(newDisplayMode: 'simple' | 'detailed') {
    this.displayMode = newDisplayMode;
    this.displayModeChange.emit(newDisplayMode);
    if (this.allowIgnoringObjectives) {
      this.updateData();
    }
  }

  getRankingChange(alternativeIndex: number) {
    return this.showRankingChange ? this.rankingChangeArray?.[alternativeIndex] ?? 0 : 0;
  }

  toggleObjective(objectiveIndex: number) {
    if (!this.canIgnoreObjectives) return;

    this.ignoredObjectives[objectiveIndex] = !this.ignoredObjectives[objectiveIndex];
    this.ignoredObjectivesCount += this.ignoredObjectives[objectiveIndex] ? 1 : -1;

    this.updateData({ initialOrderOfAlternatives: false, orderOfAlternatives: true });
  }

  getUtilityValuePartitionContainerWidth(alternativeIndex: number) {
    return `calc(${this.shownValues[alternativeIndex] * 100}% + ${6 * this.ignoredObjectivesCount}px)`;
  }

  *nonZeroUtilityValuePartitions(alternativeIndex: number) {
    if (!this.weightedUtilityMatrix || !this.weightedUtilityMatrix[alternativeIndex]) return;

    for (let i = 0; i < this.weightedUtilityMatrix[alternativeIndex].length; i++) {
      const value = this.weightedUtilityMatrix[alternativeIndex][i];
      if (value > 0 || this.ignoredObjectives[i]) {
        yield [value, i, this.ignoredObjectives[i]] as const;
      }
    }
  }

  ngOnChanges(changes: SimpleChanges) {
    const toBeUpdatedWithForce = {
      initialOrderOfAlternatives: false,
      orderOfAlternatives: false,
    };

    toBeUpdatedWithForce.initialOrderOfAlternatives = !!changes['selectedAlternatives'];
    toBeUpdatedWithForce.orderOfAlternatives = !!changes['utilityValues'] || !!changes['partitionedValues'];

    this.updateData(toBeUpdatedWithForce);
  }

  updateData(withForce = { orderOfAlternatives: false, initialOrderOfAlternatives: false }) {
    this.updateAdjustedUtilityValues();
    this.updateInitialOrderOfAlternatives(withForce.initialOrderOfAlternatives);
    this.updateWinner();
    this.updateOrderOfAlternatives(withForce.orderOfAlternatives);
    this.updateRankingChangeArray();
  }

  private updateAdjustedUtilityValues() {
    this.adjustedUtilityValues = this.decisionData.alternatives.map((_, alternativeIndex) => {
      if (this.canIgnoreObjectives) {
        return Math.max(
          0,
          this.utilityValues[alternativeIndex] -
            sum(this.weightedUtilityMatrix[alternativeIndex].filter((_, objectiveIndex) => this.ignoredObjectives[objectiveIndex])),
        );
      } else {
        return this.utilityValues[alternativeIndex];
      }
    });
  }

  private updateInitialOrderOfAlternatives(force = false) {
    if (!this.initialOrderOfAlternatives || force) {
      this.initialOrderOfAlternatives = ChartRankingComponent.sortWithRespectTo(
        range(this.decisionData.alternatives.length),
        this.decisionData.alternatives.map(
          (_, alternativeIndex) => this.initialUtilityValues?.[alternativeIndex] ?? this.utilityValues[alternativeIndex],
        ),
      );
    }
  }

  private updateWinner() {
    let winnerValue = -1;

    this.winnerAlternativeIndex = -1;
    for (const alternativeIndex of this.selectedAlternatives) {
      if (this.adjustedUtilityValues[alternativeIndex] > winnerValue) {
        winnerValue = this.adjustedUtilityValues[alternativeIndex];
        this.winnerAlternativeIndex = alternativeIndex;
      }
    }
  }

  private updateOrderOfAlternatives(force = false) {
    if (!this.orderOfAlternatives || force) {
      this.orderOfAlternatives = ChartRankingComponent.sortWithRespectTo(
        range(this.decisionData.alternatives.length),
        this.orderMode === 'utilityValues' ? this.adjustedUtilityValues : range(this.decisionData.alternatives.length),
      );

      if (this.orderMode === 'gutFeeling') {
        this.orderOfAlternatives.reverse();
      }
    }
  }

  private updateRankingChangeArray() {
    if (this.initialOrderOfAlternatives && (this.initialUtilityValues || this.allowIgnoringObjectives)) {
      const selectedAlternativesOriginalRanking = this.initialOrderOfAlternatives.filter(alternativeIndex =>
        this.selectedAlternatives.includes(alternativeIndex),
      );
      const selectedAlternativesNewRanking = this.orderOfAlternatives.filter(alternativeIndex =>
        this.selectedAlternatives.includes(alternativeIndex),
      );
      this.rankingChangeArray = range(this.decisionData.alternatives.length).map(
        alternativeIndex =>
          selectedAlternativesOriginalRanking.indexOf(alternativeIndex) - selectedAlternativesNewRanking.indexOf(alternativeIndex),
      );
    }
  }
}
