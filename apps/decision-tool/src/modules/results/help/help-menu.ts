import { NaviStep, NaviStepNames } from '@entscheidungsnavi/decision-data';
import { helpPage } from '../../../app/help/help';
import { NavigationStepMetaData } from '../../shared/navigation/educational-navigation';
import { HelpBackgroundComponent } from './help-background/help-background.component';
import { HelpMainEducational1Component } from './help-main-educational/help-main-educational-1.component';
import { HelpMainEducational2Component } from './help-main-educational/help-main-educational-2/help-main-educational-2.component';

type ResultsHelpContext =
  | 'sensitivity-analysis'
  | 'pro-contra'
  | 'risk-comparison'
  | 'robustness-check'
  | 'cost-utility-analysis'
  | 'tornado-diagram'
  | 'objective-weights-analysis';

export function getHelpMenu(_steps: { [key in NaviStep]: NavigationStepMetaData & NaviStepNames }, context?: ResultsHelpContext) {
  return [
    helpPage()
      .component(HelpMainEducational1Component)
      .name($localize`So funktioniert's`)
      .context(context)
      .build(),
    helpPage()
      .name($localize`Weitere Hinweise`)
      .component(HelpMainEducational2Component)
      .build(),
    helpPage()
      .name($localize`Hintergrundwissen`)
      .component(HelpBackgroundComponent)
      .build(),
    helpPage()
      .name($localize`Videos`)
      .youtube(4)
      .build(),
  ];
}
