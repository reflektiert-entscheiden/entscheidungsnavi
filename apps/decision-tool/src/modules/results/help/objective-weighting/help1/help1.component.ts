import { Component } from '@angular/core';
import { OnDestroyObservable } from '@entscheidungsnavi/tools';
import { getCurrentUrl } from '@entscheidungsnavi/widgets';
import { Observable, takeUntil } from 'rxjs';

@Component({
  templateUrl: './help1.component.html',
  styleUrls: ['../../../../hints.scss'],
})
export class Help1Component {
  onDetailedPage: boolean;

  @OnDestroyObservable()
  private onDestroy$: Observable<void>;

  constructor() {
    getCurrentUrl()
      .pipe(takeUntil(this.onDestroy$))
      .subscribe(url => {
        this.onDetailedPage = url.startsWith('/results/steps/2/detailed');
      });
  }
}
