import { Component } from '@angular/core';
import { LanguageService } from '../../../../../app/data/language.service';

@Component({
  templateUrl: './help-main-educational-2.component.html',
  styleUrls: ['./help-main-educational-2.component.scss', '../../../../hints.scss'],
})
export class HelpMainEducational2Component {
  constructor(protected languageService: LanguageService) {}
}
