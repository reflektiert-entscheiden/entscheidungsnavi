import { Component, Inject } from '@angular/core';
import { HELP_PAGE_CONTEXT } from '../../../shared/help-page-context.token';

@Component({
  templateUrl: './help-main-educational-1.component.html',
  styleUrls: ['../../../hints.scss'],
})
export class HelpMainEducational1Component {
  constructor(@Inject(HELP_PAGE_CONTEXT) protected context: string) {}
}
