import { Component, Input, OnInit } from '@angular/core';
import { DecisionData, getAlternativeUtilities } from '@entscheidungsnavi/decision-data';
import '@entscheidungsnavi/tools/number-rounding';
import { PersistentSetting, PersistentSettingParent } from '@entscheidungsnavi/widgets';
import { EducationalNavigationService } from '../../shared/navigation/educational-navigation.service';
import { HelpMenuProvider } from '../../../app/help/help';
import { ExplanationService } from '../../shared/decision-quality';
import { getHelpMenu } from '../help';
import { Navigation, NavLine, navLineElement } from '../../shared/navline';
import { LanguageService } from '../../../app/data/language.service';

@Component({
  selector: 'dt-results-main',
  templateUrl: './results-overview.component.html',
  styleUrls: ['./results-overview.component.scss', '../../hints.scss'],
})
@PersistentSettingParent('ResultsOverview')
export class ResultsOverviewComponent implements OnInit, Navigation, HelpMenuProvider {
  @Input()
  showStepNumber = true;

  navLine = new NavLine({
    left: [navLineElement().back('/bewertung/zielgewichtung').build()],
    middle: [
      navLineElement()
        .label($localize`Sensitivitätsanalyse`)
        .link('/ergebnis/auswertung-sensitivitaetsanalyse')
        .cypressId('sensitivity-analysis')
        .build(),
      navLineElement()
        .label($localize`Pros und Kontras`)
        .link('/ergebnis/auswertung-pro-contra')
        .cypressId('pro-contra')
        .build(),
      navLineElement()
        .label($localize`Robustheitstest`)
        .link(() => (this.robustnessCheckEnabled ? '/results/robustness-check' : null))
        .disabled(() => !this.robustnessCheckEnabled)
        .tooltip(() =>
          this.robustnessCheckEnabled
            ? undefined
            : $localize`Der Robustheitstest funktioniert nur, wenn Präzisionsintervalle eingestellt oder Einflussfaktoren verwendet wurden`,
        )
        .cypressId('robustness-check')
        .build(),
      navLineElement()
        .label($localize`Risikovergleich`)
        .link('/results/risk-comparison')
        .cypressId('risk-comparison')
        .build(),
      navLineElement()
        .label($localize`Zielgewichtsanalyse`)
        .link('/results/objective-weight-analysis')
        .disabled(() => !this.objectiveWeightAnalysisEnabled)
        .tooltip(() =>
          this.objectiveWeightAnalysisEnabled
            ? undefined
            : $localize`Die Zielgewichtsanalyse funktioniert nur in Projekten mit mindestens zwei Zielen`,
        )
        .cypressId('objective-weight-analysis')
        .build(),
      navLineElement()
        .label($localize`Kosten-Nutzen-Analyse`)
        .disabled(() => this.decisionData.objectives.findIndex(obj => !obj.isVerbal) === -1)
        .tooltip(() =>
          this.decisionData.objectives.findIndex(obj => !obj.isVerbal) !== -1
            ? undefined
            : $localize`Die Kosten-Nutzen-Analyse ist nicht für verbale Ziele definiert`,
        )
        .condition(() => this.decisionData.projectMode === 'educational')
        .link('/results/cost-utility-analysis')
        .cypressId('cost-utility-analysis')
        .build(),
      navLineElement()
        .label($localize`Tornadodiagramm`)
        .condition(() => this.decisionData.projectMode === 'educational')
        .link('/results/utility-based-tornado-diagram')
        .cypressId('utility-based-tornado-diagram')
        .build(),
      this.explanationService.generateAssessmentButton('LOGICALLY_CORRECT_REASONING'),
    ],
    right: [
      navLineElement()
        .continue('/finishproject')
        .tooltip($localize`Projekt abschließen`)
        .cypressId('finish-project')
        .build(),
    ],
  });

  @PersistentSetting('project') displayMode: 'simple' | 'detailed' = 'simple';
  @PersistentSetting('project') orderMode: 'utilityValues' | 'gutFeeling' = 'utilityValues';

  utilityValues: number[];
  weightedUtilityMatrix: number[][];

  helpMenu = getHelpMenu(this.languageService.steps);

  robustnessCheckEnabled = false;
  readonly objectiveWeightAnalysisEnabled: boolean;

  constructor(
    private currentProgressService: EducationalNavigationService,
    private languageService: LanguageService,
    private decisionData: DecisionData,
    private explanationService: ExplanationService,
  ) {
    this.objectiveWeightAnalysisEnabled = this.decisionData.objectives.length > 1;
  }

  ngOnInit() {
    if ((this.decisionData.resultSubstepProgress ?? 0) < 2) {
      this.decisionData.resultSubstepProgress = 2;
      this.currentProgressService.updateProgress();
    }
    this.updateAlternativenRanking();

    this.robustnessCheckEnabled = Object.values(this.decisionData.getInaccuracies()).some(isInaccurate => isInaccurate);
  }

  updateAlternativenRanking() {
    this.weightedUtilityMatrix = this.decisionData.getWeightedUtilityMatrix();
    this.utilityValues = getAlternativeUtilities(this.weightedUtilityMatrix);
  }
}
