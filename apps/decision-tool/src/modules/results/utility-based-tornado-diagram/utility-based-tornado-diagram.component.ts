import { Component, Input, OnInit, ViewChild } from '@angular/core';
import {
  calculateIndicatorValue,
  UserDefinedInfluenceFactor,
  DecisionData,
  generatePredefinedScenarios,
  getOutcomeUtilityFunction,
  InfluenceFactor,
  Objective,
  ObjectiveInput,
  Outcome,
  PredefinedInfluenceFactor,
  PredefinedScenario,
} from '@entscheidungsnavi/decision-data';
import { TornadoDiagramComponent } from '@entscheidungsnavi/widgets/tornado-diagram/tornado-diagram.component';
import {
  BackgroundAreas,
  orderAndMergeTornadoPoints,
  ScaleAxis,
  TornadoData,
  TornadoBar,
  TornadoPoint,
} from '@entscheidungsnavi/widgets/tornado-diagram/tornado-helper';
import { normalizeContinuous } from '@entscheidungsnavi/tools';
import { getInfluenceFactorStateName } from '@entscheidungsnavi/widgets';
import { cloneDeep, max, min, sum, zip } from 'lodash';
import { Navigation, NavLine, navLineElement } from '../../shared/navline';
import { DisplayAtMaxWidth } from '../../../app/interfaces/display-at-max-width';
import { HelpMenuProvider } from '../../../app/help/help';
import { getHelpMenu } from '../help';
import { LanguageService } from '../../../app/data/language.service';

@Component({
  selector: 'dt-utility-based-tornado-diagram',
  styles: [
    `
      dt-select-line {
        margin-bottom: 20px;
      }
    `,
  ],
  templateUrl: './utility-based-tornado-diagram.component.html',
})
@DisplayAtMaxWidth
export class UtilityBasedTornadoDiagramComponent implements Navigation, OnInit, HelpMenuProvider {
  @ViewChild(TornadoDiagramComponent) tornadoDiagram: TornadoDiagramComponent;

  @Input()
  showTitle = true;

  // the utility function of each objective
  objectiveUFs: ((value: ObjectiveInput) => number)[];
  // utility functions who ignore verbal indicators, i.e. they treat verbal indicators as normal indicators
  // and expect a single value instead of a list of category ids
  objectiveUFsForVerbalizedIndicators: ((value: ObjectiveInput) => number)[];
  objectiveWeights: number[];

  /* Select box values. */
  selectedAlternativeIdx = 0;
  referenceAlternativeIdx = -1;

  alternativeUtilities: number[];

  tornadoData: TornadoData;
  scaleAxis: ScaleAxis;
  areas: BackgroundAreas;

  readonly navLine = new NavLine({
    left: [navLineElement().back('/results').build()],
  });

  helpMenu = getHelpMenu(this.languageService.steps, 'tornado-diagram');

  readonly alternativeDisabledTooltip = $localize`Alternativen, die keine Unsicherheiten enthalten, können nicht ausgewählt werden.`;

  constructor(
    protected decisionData: DecisionData,
    private languageService: LanguageService,
  ) {}

  ngOnInit() {
    this.objectiveUFs = this.decisionData.objectives.map(objective => objective.getUtilityFunction());
    this.objectiveUFsForVerbalizedIndicators = this.decisionData.objectives.map(objective => {
      if (!objective.isIndicator || !objective.indicatorData.indicators.some(ind => ind.isVerbalized)) {
        // non indicator objectives or objectives that do not include a verbal indicator have the same Utility Function
        return objective.getUtilityFunction();
      }
      // for objectives with verbalized indicators, we compute utility functions that ignore the layer of verbal categories
      // i.e. accept a single indicator value instead of a list of category ids
      const newObjective = cloneDeep(objective);
      newObjective.indicatorData.indicators.forEach(indicator => {
        if (indicator.isVerbalized) {
          indicator.verbalIndicatorCategories = [];
        }
      });
      return newObjective.getUtilityFunction();
    });
    this.objectiveWeights = this.decisionData.weights.getWeightValues();
    normalizeContinuous(this.objectiveWeights);
    this.alternativeUtilities = this.decisionData.getAlternativeUtilities();
    // Select the first alternative that includes an uncertainty
    for (let alternativeIdx = 0; alternativeIdx < this.decisionData.alternatives.length; alternativeIdx++) {
      if (this.alternativeContainsUncertainty(alternativeIdx)) {
        this.selectedAlternativeIdx = alternativeIdx;
        break;
      }
    }
    this.createTornadoDiagram();
  }

  protected createTornadoDiagram() {
    const tornadoData: TornadoData = {
      bars: [],
      totalExpectedValue: null,
    };
    this.scaleAxis = {
      label: $localize`bedingter Nutzenerwartungswert`,
      unit: '',
      from: 0,
      to: 0,
    };

    if (this.referenceAlternativeIdx === -1) {
      // Single alternative mode
      // defaultArea and totalExpectedValue = alternativeUtility
      tornadoData.totalExpectedValue = {
        value: this.alternativeUtilities[this.selectedAlternativeIdx] * 100,
        tooltipInfo: $localize`Nutzenerwartungswert für Alternative „${this.decisionData.alternatives[this.selectedAlternativeIdx].name}“`,
      };
      this.areas = {
        defaultAreaLabel: this.decisionData.alternatives[this.selectedAlternativeIdx].name,
      };
    } else {
      // leftArea, rightArea and totalExpectedValue = difference in alternativeUtilities
      tornadoData.totalExpectedValue = {
        value: (this.alternativeUtilities[this.selectedAlternativeIdx] - this.alternativeUtilities[this.referenceAlternativeIdx]) * 100,
        tooltipInfo: $localize`Differenz des Nutzenerwartungswertes für Alternative
        „${this.decisionData.alternatives[this.selectedAlternativeIdx].name}“
        und Alternative „${this.decisionData.alternatives[this.referenceAlternativeIdx].name}“`,
      };
      this.areas = {
        leftAreaLabel: this.decisionData.alternatives[this.referenceAlternativeIdx].name,
        rightAreaLabel: this.decisionData.alternatives[this.selectedAlternativeIdx].name,
      };
      this.scaleAxis.label = $localize`Differenz des bedingten Nutzenerwartungswertes`;
    }

    this.calculateTornadoPoints(tornadoData); // TornadoBars
    this.calculateTornadoLimits(tornadoData); // scaleAxis
    this.tornadoData = tornadoData; // trigger dt-tornado-diagram input
  }

  private calculateTornadoPoints(tornadoData: TornadoData) {
    this.calculateBarsForCustomIF(tornadoData); // for both selectedAlternative and referenceAlternative
    this.calculateBarsForPredefinedIF(tornadoData, true); // for selectedAlternative
    if (this.referenceAlternativeIdx !== -1) this.calculateBarsForPredefinedIF(tornadoData, false); // for referenceAlternative
  }

  /* Custom Influence Factor */

  // computes the TornadoBars for each UserDefinedInfluenceFactor
  private calculateBarsForCustomIF(tornadoData: TornadoData) {
    this.decisionData.influenceFactors.forEach(influenceFactor => {
      // We check if the uncertainty (this concrete influenceFactor) is present in ANY of the Outcomes
      // of both the selectedAlternative and the referenceAlternative
      const selectedAlternativeUncertainty = this.decisionData.outcomes[this.selectedAlternativeIdx].some(
        outcome => outcome.influenceFactor === influenceFactor,
      );
      const referenceAlternativeUncertainty =
        this.referenceAlternativeIdx !== -1 &&
        this.decisionData.outcomes[this.referenceAlternativeIdx].some(outcome => outcome.influenceFactor === influenceFactor);
      if (selectedAlternativeUncertainty !== referenceAlternativeUncertainty) {
        // present only in ONE of the alternatives
        this.calculateBarsForCustomIFSingle(influenceFactor, tornadoData, selectedAlternativeUncertainty);
      } else if (selectedAlternativeUncertainty && referenceAlternativeUncertainty) {
        // present in BOTH alternatives
        this.calculateBarsForCustomIFMultiple(influenceFactor, tornadoData);
      }
    });
  }

  // handles the utilities of the single alternative where the influence factor is present
  // could be either the selectedAlternative or the referenceAlternative
  // useSelectedAlternative helps distinguish between the two
  private calculateBarsForCustomIFSingle(
    influenceFactor: UserDefinedInfluenceFactor,
    tornadoData: TornadoData,
    useSelectedAlternative: boolean,
  ) {
    const alternativeIdx = useSelectedAlternative ? this.selectedAlternativeIdx : this.referenceAlternativeIdx;
    // compute the alternative weighted utility for each state of influenceFactor
    let stateUtilityCombinations = new Map<number, { utility: number; probability: number }>();
    influenceFactor.getStates().forEach((state, stateIdx) => {
      const utilityVector: number[] = [];
      for (let objectiveIdx = 0; objectiveIdx < this.decisionData.objectives.length; objectiveIdx++) {
        const outcome = this.decisionData.outcomes[alternativeIdx][objectiveIdx];
        utilityVector.push(this.getOutcomeUtility(outcome, objectiveIdx, stateIdx, influenceFactor));
      }
      stateUtilityCombinations.set(stateIdx, { utility: this.alternativeWeightedUtility(utilityVector), probability: state.probability });
    });
    // sort in asc order based on alternative utility, so that points can later be merged if equal
    stateUtilityCombinations = new Map([...stateUtilityCombinations.entries()].sort((a, b) => a[1].utility - b[1].utility));

    tornadoData.bars.push({
      alternativeBoxes: [
        {
          isPrimary: useSelectedAlternative,
          alternativeName: this.decisionData.alternatives[alternativeIdx].name,
        },
      ], // single-colored
      label: influenceFactor.name,
      unit: '',
      points: this.calculateCustomTornadoPointLabels(stateUtilityCombinations, influenceFactor, useSelectedAlternative),
      children: [],
      areChildrenExpanded: false,
    });
  }

  // handles the utilities in both alternatives where the influence factor is present
  private calculateBarsForCustomIFMultiple(influenceFactor: UserDefinedInfluenceFactor, tornadoData: TornadoData) {
    // compute combinations, subtract their utility value from each other (per alternative) and sort them
    let stateUtilityDifferences = new Map<number, { utility: number; probability: number }>();
    influenceFactor.getStates().forEach((state, stateIdx) => {
      const utilityVectorSelected: number[] = [];
      const utilityVectorReference: number[] = [];
      for (let objectiveIdx = 0; objectiveIdx < this.decisionData.objectives.length; objectiveIdx++) {
        const outcomeSelected = this.decisionData.outcomes[this.selectedAlternativeIdx][objectiveIdx];
        const outcomeReference = this.decisionData.outcomes[this.referenceAlternativeIdx][objectiveIdx];
        utilityVectorSelected.push(this.getOutcomeUtility(outcomeSelected, objectiveIdx, stateIdx, influenceFactor));
        utilityVectorReference.push(this.getOutcomeUtility(outcomeReference, objectiveIdx, stateIdx, influenceFactor));
      }
      // here stateUtilityDifferences contains the difference in the utilities of the two alternatives
      stateUtilityDifferences.set(stateIdx, {
        utility: (this.alternativeWeightedUtility(utilityVectorSelected) - this.alternativeWeightedUtility(utilityVectorReference)) * 100,
        probability: state.probability,
      });
    });
    stateUtilityDifferences = new Map([...stateUtilityDifferences.entries()].sort((a, b) => a[1].utility - b[1].utility));

    tornadoData.bars.push({
      alternativeBoxes: [
        { isPrimary: true, alternativeName: this.decisionData.alternatives[this.selectedAlternativeIdx].name },
        { isPrimary: false, alternativeName: this.decisionData.alternatives[this.referenceAlternativeIdx].name },
      ], // multi-colored
      label: influenceFactor.name,
      unit: '',
      points: this.calculateCustomTornadoPointLabels(stateUtilityDifferences, influenceFactor),
      children: [],
      areChildrenExpanded: false,
    });
  }

  // repetitive logic for both CustomRows handlers
  // the existence of useSelectedAlternative helps distinguish between the two
  private calculateCustomTornadoPointLabels(
    stateUtilityCombinations: Map<number, { utility: number; probability: number }>,
    influenceFactor: UserDefinedInfluenceFactor,
    useSelectedAlternative?: boolean,
  ) {
    // when useSelectedAlternative != null, stateUtilityCombinations is a Map of utility differences
    // otherwise it's the state utilities of the given alternative (could be selectedAlternative or referenceAlternative)
    const tornadoPoints: TornadoPoint[] = [];
    stateUtilityCombinations.forEach((stateUtilityCombination, stateIdx) => {
      const tornadoPointLabel = getInfluenceFactorStateName(influenceFactor, stateIdx, true);
      const tooltipRows = [{ stateName: tornadoPointLabel, probability: stateUtilityCombination.probability }];
      let tornadoPointValue;
      if (useSelectedAlternative == null) {
        // uncertainty in BOTH alternatives, tornadoPointValue already computed in calculateBarsForCustomIFMultiple()
        tornadoPointValue = stateUtilityCombination.utility;
      } else {
        // uncertainty in only ONE of the alternatives
        tornadoPointValue = this.computeTornadoPointValue(useSelectedAlternative, stateUtilityCombination.utility);
      }
      tornadoPoints.push({
        tornadoPointLabel,
        tornadoPointValue,
        tooltipCombinations: [{ probability: stateUtilityCombination.probability / 100, tooltipRows }],
      });
    });

    return orderAndMergeTornadoPoints(tornadoPoints, influenceFactor, 0.1);
  }

  /* Predefined Influence Factor */

  // computes the TornadoBars for a Predefined Influence Factor (either for selectedAlternative or referenceAlternative)
  // useSelectedAlternative helps distinguish between the two
  private calculateBarsForPredefinedIF(tornadoData: TornadoData, useSelectedAlternative: boolean) {
    const alternativeIdx = useSelectedAlternative ? this.selectedAlternativeIdx : this.referenceAlternativeIdx;
    const utilityVector = this.decisionData.outcomes[alternativeIdx].map((outcome, objectiveIdx) =>
      this.getOutcomeUtility(outcome, objectiveIdx),
    );
    const currentAlternativeWeightedUtility = this.alternativeWeightedUtility(utilityVector);

    this.decisionData.outcomes[alternativeIdx].forEach((outcome, objectiveIdx) => {
      if (outcome.influenceFactor instanceof PredefinedInfluenceFactor) {
        const states = outcome.influenceFactor.getStates();
        const objective = this.decisionData.objectives[objectiveIdx];

        // sum of utilities of objectives that aren't subject to change in this iteration (all but objectiveIdx)
        const remainingAlternativeWeightedUtility =
          currentAlternativeWeightedUtility - this.getOutcomeUtility(outcome, objectiveIdx) * this.objectiveWeights[objectiveIdx];
        const scenarios = generatePredefinedScenarios(
          outcome.values,
          states.map(s => s.probability / 100),
          true,
        );

        // Add bar to tornadoDiagram
        if (!objective.isIndicator) {
          tornadoData.bars.push({
            alternativeBoxes: [
              {
                isPrimary: useSelectedAlternative,
                alternativeName: this.decisionData.alternatives[alternativeIdx].name,
              },
            ], // single-colored
            label: $localize`Worst-Best im Ziel` + ' ' + objective.name,
            unit: '',
            points: this.generatePredefinedTornadoPointsNumVerbal(
              remainingAlternativeWeightedUtility,
              objectiveIdx,
              scenarios,
              useSelectedAlternative,
            ),
            children: [],
            areChildrenExpanded: false,
          });
        } else {
          tornadoData.bars.push({
            alternativeBoxes: [
              {
                isPrimary: useSelectedAlternative,
                alternativeName: this.decisionData.alternatives[alternativeIdx].name,
              },
            ], // single-colored
            label: $localize`Worst-Best im Ziel` + ' ' + objective.name,
            unit: '',
            points: this.generatePredefinedTornadoPointsIndicator(
              remainingAlternativeWeightedUtility,
              objectiveIdx,
              scenarios,
              useSelectedAlternative,
            ),
            // indicator bars for Predefined IF include children for each indicator, that can be expanded/collapsed
            children: this.generatePredefinedBarChildrenIndicator(
              remainingAlternativeWeightedUtility,
              objectiveIdx,
              useSelectedAlternative,
            ),
            areChildrenExpanded: false,
          });
        }
      }
    });
  }

  private generatePredefinedTornadoPointsNumVerbal(
    remainingAlternativeWeightedUtility: number,
    objectiveIdx: number,
    scenarios: PredefinedScenario[],
    useSelectedAlternative: boolean,
  ) {
    const alternativeIdx = useSelectedAlternative ? this.selectedAlternativeIdx : this.referenceAlternativeIdx;
    const scenarioUtilityValues = scenarios.map(scenario => this.objectiveUFs[objectiveIdx](scenario.value));
    const objective = this.decisionData.objectives[objectiveIdx];
    const outcome = this.decisionData.outcomes[alternativeIdx][objectiveIdx];
    const states = outcome.influenceFactor.getStates();

    const tornadoPoints: TornadoPoint[] = [];
    scenarioUtilityValues.forEach((scenarioUtilityValue, stateIdx) => {
      // we add each scenario's weighted utility to the remaining utilities and compute the tornadoPointValue
      const alternativeWeightedUtility = remainingAlternativeWeightedUtility + scenarioUtilityValue * this.objectiveWeights[objectiveIdx];

      const tornadoPointLabel = getInfluenceFactorStateName(outcome.influenceFactor, stateIdx, true);
      const tooltipRows = [
        {
          stateName: tornadoPointLabel,
          stateValue: this.getOutcomeValue(outcome, objective, stateIdx),
        },
      ];
      const tornadoPointValue = this.computeTornadoPointValue(useSelectedAlternative, alternativeWeightedUtility);

      tornadoPoints.push({
        tornadoPointLabel,
        tornadoPointValue,
        tooltipCombinations: [{ probability: states[stateIdx].probability / 100, tooltipRows }],
      });
    });

    return orderAndMergeTornadoPoints(tornadoPoints, outcome.influenceFactor, 0.1);
  }

  private generatePredefinedTornadoPointsIndicator(
    remainingAlternativeWeightedUtility: number,
    objectiveIdx: number,
    scenarios: PredefinedScenario[],
    useSelectedAlternative: boolean,
  ) {
    const alternativeIdx = useSelectedAlternative ? this.selectedAlternativeIdx : this.referenceAlternativeIdx;
    const objective = this.decisionData.objectives[objectiveIdx];
    const outcome = this.decisionData.outcomes[alternativeIdx][objectiveIdx];
    const tornadoPoints: TornadoPoint[] = [];

    // calculate values for entire outcome
    for (const scenario of scenarios) {
      const uf = getOutcomeUtilityFunction(objective.getUtilityFunction());
      const alternativeWeightedUtility = remainingAlternativeWeightedUtility + uf([scenario.value]) * this.objectiveWeights[objectiveIdx];

      // Worst state
      const worstIncluded = scenario.stateIndices.includes(0);
      // Worst state merged with Median state
      const mergedWorstIncluded = scenario.stateIndices.some(
        (stateIdx, indicatorIdx) =>
          stateIdx === 1 && outcome.values[0][indicatorIdx].toString() === outcome.values[1][indicatorIdx].toString(),
      );
      // Best state
      const bestIncluded = scenario.stateIndices.includes(2);
      // Best state merged with Median state
      const mergedBestIncluded = scenario.stateIndices.some(
        (stateIdx, indicatorIdx) =>
          stateIdx === 1 && outcome.values[2][indicatorIdx].toString() === outcome.values[1][indicatorIdx].toString(),
      );

      let stateForScenario;
      if ((worstIncluded || mergedWorstIncluded) && !bestIncluded) {
        // includes Worst, but not Best therefore summarize as Worst
        stateForScenario = 0;
      } else if ((bestIncluded || mergedBestIncluded) && !worstIncluded) {
        // includes Best, but not Worst therefore summarize as Best
        stateForScenario = 2;
      }

      const tornadoPointLabel =
        stateForScenario != null ? getInfluenceFactorStateName(outcome.influenceFactor, stateForScenario, true) : '';
      // in this case (predefined IF + Indicator objective), tooltipCombinations includes the state and value of each indicator
      const tooltipRows = scenario.stateIndices.map((stateIdx, indicatorIdx) => ({
        indicatorName: objective.indicatorData.indicators[indicatorIdx].name,
        stateName: this.getStateName(outcome, stateIdx, indicatorIdx),
        stateValue: this.getOutcomeValue(outcome, objective, stateIdx, indicatorIdx),
      }));
      const tornadoPointValue = this.computeTornadoPointValue(useSelectedAlternative, alternativeWeightedUtility);

      tornadoPoints.push({
        tornadoPointLabel,
        tornadoPointValue,
        tooltipCombinations: [{ probability: scenario.probability, tooltipRows }],
      });
    }

    return orderAndMergeTornadoPoints(tornadoPoints, outcome.influenceFactor, 0.1);
  }

  private generatePredefinedBarChildrenIndicator(
    remainingAlternativeWeightedUtility: number,
    objectiveIdx: number,
    useSelectedAlternative: boolean,
  ) {
    const alternativeIdx = useSelectedAlternative ? this.selectedAlternativeIdx : this.referenceAlternativeIdx;
    const objective = this.decisionData.objectives[objectiveIdx];
    const outcome = this.decisionData.outcomes[alternativeIdx][objectiveIdx];
    const states = outcome.influenceFactor.getStates();
    const children: TornadoBar[] = objective.indicatorData.indicators.map(indicator => ({
      label: indicator.name, // indicator name for child entry and Predefined IF
      unit: '',
      points: [],
    }));

    // calculate expected value for each indicator
    const indicatorExpectedValues = outcome.generateExpectedValuesVector(objective);

    // for every indicator (indicatorIdx) a point is made by combining:
    // 1) the outcome value for this indicator at a given state - outcome.values[stateIdx][indicatorIdx]
    // 2) the expected values of the remaining indicators - indicatorExpectedValues[otherIndicatorIdx]
    for (let indicatorIdx = 0; indicatorIdx < objective.indicatorData.indicators.length; indicatorIdx++) {
      for (let stateIdx = 0; stateIdx < states.length; stateIdx++) {
        // clone expected values as outcome values
        const values = indicatorExpectedValues.map(v => [v]);
        // replace the expected value of the current indicatorIdx with its actual value
        values[indicatorIdx][0] = calculateIndicatorValue(
          outcome.values[stateIdx][indicatorIdx],
          objective.indicatorData.indicators[indicatorIdx],
        );
        // compute the weighted utility for the alternative
        const outcomeUtility = this.objectiveUFsForVerbalizedIndicators[objectiveIdx](values);
        const alternativeWeightedUtility = remainingAlternativeWeightedUtility + outcomeUtility * this.objectiveWeights[objectiveIdx];

        const tornadoPointLabel = getInfluenceFactorStateName(outcome.influenceFactor, stateIdx, true);
        const tooltipRows = [
          {
            stateName: tornadoPointLabel,
            stateValue: this.getOutcomeValue(outcome, objective, stateIdx, indicatorIdx),
            probability: states[stateIdx].probability,
          },
        ];
        const tornadoPointValue = this.computeTornadoPointValue(useSelectedAlternative, alternativeWeightedUtility);

        children[indicatorIdx].points.push({
          tornadoPointLabel,
          tornadoPointValue,
          tooltipCombinations: [{ probability: states[stateIdx].probability / 100, tooltipRows }],
        });
      }

      children[indicatorIdx].points = orderAndMergeTornadoPoints(children[indicatorIdx].points, outcome.influenceFactor);
    }

    return children;
  }

  /**
   * @param useSelectedAlternative - boolean, determines whether to use selectedAlternative or referenceAlternative
   * @param utilityValue - number, the utility value of the given alternative (selected or reference)
   *
   * Either returns the utilityValue directly or computes the difference with the other utilityValue (of the other alternative)
   * The result is also multiplied by 100.
   */
  private computeTornadoPointValue(useSelectedAlternative: boolean, utilityValue: number) {
    let tornadoPointValue;
    if (useSelectedAlternative) {
      // uncertainty in selected alternative
      if (this.referenceAlternativeIdx === -1) {
        tornadoPointValue = utilityValue * 100; // nothing to compare with
      } else {
        // subtract utility of reference alternative
        tornadoPointValue = (utilityValue - this.alternativeUtilities[this.referenceAlternativeIdx]) * 100;
      }
    } else {
      // uncertainty in reference alternative
      tornadoPointValue = (this.alternativeUtilities[this.selectedAlternativeIdx] - utilityValue) * 100;
    }
    return tornadoPointValue;
  }

  // computes outcome utility, but selects a certain state when the influenceFactor matches the one provided
  // otherwise computes the expected value as usual
  private getOutcomeUtility(outcome: Outcome, objectiveIdx: number, stateIdx?: number, influenceFactor?: InfluenceFactor) {
    if (outcome.influenceFactor === influenceFactor && influenceFactor != null && stateIdx != null) {
      return this.objectiveUFs[objectiveIdx](outcome.values[stateIdx]);
    } else {
      const uf = getOutcomeUtilityFunction(
        this.decisionData.objectives[objectiveIdx].getUtilityFunction(),
        outcome.influenceFactor
          ? (callback, outcomeValues) => outcome.influenceFactor.iterateScenarios(outcomeValues, callback, true)
          : undefined,
      );
      return uf(outcome.values);
    }
  }

  // computes the total weighted alternative utility based on a utilityVector
  private alternativeWeightedUtility(utilityVector: number[]) {
    return sum(zip(this.objectiveWeights, utilityVector).map(([objectiveWeight, utilityValue]) => objectiveWeight * utilityValue));
  }

  // set limits to the longest tornado line
  private calculateTornadoLimits(tornadoData: TornadoData) {
    const allTornadoPointValues = tornadoData.bars.map(bar => bar.points.map(v => v.tornadoPointValue)).flat(2);
    this.scaleAxis.from = min(allTornadoPointValues);
    this.scaleAxis.to = max(allTornadoPointValues);
    if (this.referenceAlternativeIdx !== -1) {
      // make sure a small part of each area is always visible, when in comparison mode
      this.scaleAxis.from = Math.min(this.scaleAxis.from, -5);
      this.scaleAxis.to = Math.max(this.scaleAxis.to, 5);
    }
  }

  // whether an alternative includes an ACTUAL uncertainty (when all states are equal it doesn't count as un uncertainty)
  protected alternativeContainsUncertainty(alternativeIdx: number) {
    return this.decisionData.outcomes[alternativeIdx].some((outcome, objectiveIdx) =>
      outcome.includesNonTrivialUncertainty(this.decisionData.objectives[objectiveIdx]),
    );
  }

  private getOutcomeValue(outcome: Outcome, objective: Objective, stateIdx: number, indicatorIdx?: number) {
    const value = outcome.values[stateIdx][indicatorIdx ?? 0][0];
    const unit = indicatorIdx != null ? objective.indicatorData.indicators[indicatorIdx].unit : objective.unit();
    return `${value}${unit !== '' ? ' ' + unit : ''}`;
  }

  // returns the name of the state, but merges equal states (e.g. Best = Median)
  private getStateName(outcome: Outcome, stateIdx: number, indicatorIdx: number) {
    if (stateIdx === 1) {
      let name = '';
      const worstValue = outcome.values[0][indicatorIdx].toString();
      const medianValue = outcome.values[1][indicatorIdx].toString();
      const bestValue = outcome.values[2][indicatorIdx].toString();
      if (worstValue === medianValue) name += getInfluenceFactorStateName(outcome.influenceFactor, 0, true) + '/';
      name += getInfluenceFactorStateName(outcome.influenceFactor, 1, true);
      if (bestValue === medianValue) name += '/' + getInfluenceFactorStateName(outcome.influenceFactor, 2, true);
      return name;
    }
    return getInfluenceFactorStateName(outcome.influenceFactor, stateIdx, true);
  }
}
