import { Component, Input } from '@angular/core';
import { InfluenceFactorStateMap } from '@entscheidungsnavi/decision-data';
import { MinMax } from '../../../../worker/robustness-check-messages';

@Component({
  selector: 'dt-chart-rangposition-part',
  styleUrls: ['./chart-rangposition-part.component.scss'],
  templateUrl: './chart-rangposition-part.component.html',
})
export class ChartRangpositionPartComponent {
  @Input() value: number;
  @Input() tooltipInfo: {
    drawnCs: MinMax[];
    drawnWeights: MinMax[];
    stateCounts: InfluenceFactorStateMap<number[]>;
  };
}
