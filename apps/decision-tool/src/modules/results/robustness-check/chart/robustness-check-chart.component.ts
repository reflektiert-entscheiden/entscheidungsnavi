import { ChangeDetectionStrategy, Component, Input, OnChanges, SimpleChanges } from '@angular/core';
import { DecisionData } from '@entscheidungsnavi/decision-data';
import { range } from 'lodash';
import { RobustnessWorkerResult } from '../../../../worker/robustness-check.service';
import { BoxPlotData } from './chart-nutzenerwartungswert.component';

@Component({
  selector: 'dt-robustness-check-chart',
  templateUrl: './robustness-check-chart.component.html',
  styleUrls: ['./robustness-check-chart.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class RobustnessCheckChartComponent implements OnChanges {
  readonly barHeight = 40;
  readonly barSpace = 20;
  readonly chartHeightPercent = 100;

  @Input() result: RobustnessWorkerResult;
  @Input() selectedAlternatives: number[];
  @Input() expectedUtilityValues: number[]; // for ALL alternatives, not only the selected ones

  rankedResultIDs: number[];
  positions: number[][];
  scores: number[];
  alternativeBoxPlotData: BoxPlotData[];

  scale = range(0, 101, 10);

  // height of a row in the alternatives chart
  get totalBarHeight() {
    return this.barHeight + this.barSpace;
  }

  get resBotPadding() {
    return (this.totalBarHeight * (100 / this.chartHeightPercent - 1) * this.rankedResultIDs.length) / 2;
  }

  constructor(protected decisionData: DecisionData) {}

  ngOnChanges(_changes: SimpleChanges) {
    if (this.result && this.selectedAlternatives && this.expectedUtilityValues) {
      this.updateResult();
    }
  }

  private updateResult() {
    // calculate score (average position)
    this.scores = this.result.alternatives.map(row => row.positionDistribution.reduce((sum, v, i) => sum + v * (i + 1)));

    // calculate ranked ID mapping (displayed position -> calculationIDs idx)
    this.rankedResultIDs = this.scores
      .map((score, i) => [score, i])
      .sort((a, b) => a[0] - b[0])
      .map(v => v[1]);

    this.alternativeBoxPlotData = this.rankedResultIDs.map(cIdx => [
      this.expectedUtilityValues[this.selectedAlternatives[cIdx]],
      this.result.alternatives[cIdx].minUtility * 100,
      this.result.alternatives[cIdx].maxUtility * 100,
      this.calculatePercentile(this.result.alternatives[cIdx].utilityHistogram, 0.25) * 100,
      this.calculatePercentile(this.result.alternatives[cIdx].utilityHistogram, 0.75) * 100,
    ]);
    this.positions = this.rankedResultIDs.map(idx => this.result.alternatives[idx].positionDistribution);
  }

  private calculatePercentile(utilityHistogram: number[], percentage: number) {
    const getTotalValueForIndex = (index: number) => {
      let iterationIndex = 0;

      for (let a = 0; a < utilityHistogram.length; a++) {
        for (let b = 0; b < utilityHistogram[a]; b++) {
          if (index === iterationIndex) {
            return a / (utilityHistogram.length - 1);
          }
          iterationIndex++;
        }
      }
    };

    const n = utilityHistogram.reduce((sum, v) => sum + v, 0);

    const np = n * percentage;

    let percentile = 0;

    if (Number.isInteger(np)) {
      percentile = (1 / 2) * (getTotalValueForIndex(np) + getTotalValueForIndex(np + 1));
    } else {
      percentile = getTotalValueForIndex(Math.floor(np + 1));
    }

    return percentile;
  }
}
