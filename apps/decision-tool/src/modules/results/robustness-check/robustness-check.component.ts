import { Component, Input, OnInit, TemplateRef, ViewChild, NgZone } from '@angular/core';
import { atLeastOneValidator } from '@entscheidungsnavi/widgets';
import { flatten, isEqual, range } from 'lodash';
import { OnDestroyObservable } from '@entscheidungsnavi/tools';
import { Observable, takeUntil, throttleTime } from 'rxjs';
import { NonNullableFormBuilder } from '@angular/forms';
import {
  CompositeUserDefinedInfluenceFactor,
  DecisionData,
  generateListOfUnderlyingUserDefinedFactors,
  InfluenceFactor,
  InfluenceFactorStateMap,
} from '@entscheidungsnavi/decision-data';
import { RobustnessCheckService, RobustnessWorkerResult } from '../../../worker/robustness-check.service';
import { HelpMenuProvider } from '../../../app/help/help';
import { getHelpMenu } from '../help';
import { Navigation, NavLine, navLineElement } from '../../shared/navline';
import { Debug } from '../../../app/debug/debug-template';
import { LanguageService } from '../../../app/data/language.service';
import { ROBUSTNESS_CHECK_HISTOGRAM_BIN_COUNT } from '../../../worker/robustness-check-messages';

@Component({
  selector: 'dt-robustness-check',
  templateUrl: './robustness-check.component.html',
  styleUrls: ['./robustness-check.component.scss'],
  providers: [RobustnessCheckService],
})
export class RobustnessCheckComponent implements OnInit, Navigation, HelpMenuProvider, Debug {
  @OnDestroyObservable()
  private onDestroy$: Observable<void>;

  // When the maximum change in any value of the position distribution per iteration drops
  // below this threshold, we consider the calculation "done".
  private readonly changeThreshold = 1e-7;

  @Input()
  showTitle = true;

  readonly navLine = new NavLine({
    left: [navLineElement().back('/results').build()],
  });

  readonly expectedUtilityValues: number[];
  readonly sortedAlternativeIndices: number[]; // Sorted by their expected utility
  readonly projectInaccuracies: ReturnType<typeof this.decisionData.getInaccuracies>;

  readonly paramForm = this.fb.group({
    alternatives: this.fb.array(
      this.decisionData.alternatives.map(() => false),
      atLeastOneValidator,
    ),
    utilities: [true],
    objectiveWeights: [true],
    probabilities: [true],
    predefinedScenarios: [true],
    userDefinedScenarioIDs: this.fb.array(
      this.decisionData.influenceFactors.map(influenceFactor => !this.isInfluenceFactorComposite(influenceFactor)),
    ),
  });

  currentResult: RobustnessWorkerResult;
  isPaused: boolean;
  isDone: boolean; // Switches to true once the change rate falls below the threshold for the first time
  activeParams: typeof this.paramForm.value;
  activeInfluenceFactorToggles: boolean[];

  private lastUpdateTime: number;
  iterationsPerSecond: number;
  changePerIteration: number;
  elapsedTime: number;

  get areParamsUnchanged() {
    return isEqual(this.activeParams, this.paramForm.value);
  }

  get activeAlternativeIndices() {
    return this.activeParams.alternatives.map((active, index) => (active ? index : -1)).filter(index => index !== -1);
  }

  get includesCompositeInfluenceFactors() {
    return this.decisionData.influenceFactors.some(uF => this.isInfluenceFactorComposite(uF));
  }

  get state(): 'done' | 'calculating' | 'paused' {
    if (!this.isPaused) return 'calculating';
    else if (this.isDone) return 'done';
    else return 'paused';
  }

  get buttonState(): 'has-changes' | 'calculating' | 'paused' {
    if (!this.areParamsUnchanged) return 'has-changes';
    else if (this.isPaused) return 'paused';
    else return 'calculating';
  }

  get currentInfluenceFactorToggles() {
    return this.paramForm.controls.userDefinedScenarioIDs.controls.map(control => control.value);
  }

  readonly helpMenu = getHelpMenu(this.languageService.steps, 'robustness-check');

  @ViewChild('debug', { static: true })
  debugTemplate: TemplateRef<unknown>;
  readonly hasDebugTemplate = true;

  constructor(
    protected decisionData: DecisionData,
    private languageService: LanguageService,
    protected robustnessCheckService: RobustnessCheckService,
    private fb: NonNullableFormBuilder,
    private zone: NgZone,
  ) {
    this.expectedUtilityValues = this.decisionData.getAlternativeUtilities();
    this.sortedAlternativeIndices = range(this.decisionData.alternatives.length).sort(
      (idx1, idx2) => this.expectedUtilityValues[idx2] - this.expectedUtilityValues[idx1],
    );

    this.projectInaccuracies = this.decisionData.getInaccuracies();
  }

  ngOnInit() {
    this.sortedAlternativeIndices
      .slice(0, 3)
      .forEach(alternativeIndex => this.paramForm.controls.alternatives.at(alternativeIndex).setValue(true));

    if (!this.projectInaccuracies.weights) {
      this.paramForm.controls.objectiveWeights.setValue(false);
      this.paramForm.controls.objectiveWeights.disable();
    }
    if (!this.projectInaccuracies.probabilities) {
      this.paramForm.controls.probabilities.setValue(false);
      this.paramForm.controls.probabilities.disable();
    }
    if (!this.projectInaccuracies.utilityNumericalIndicator && !this.projectInaccuracies.utilityVerbal) {
      this.paramForm.controls.utilities.setValue(false);
      this.paramForm.controls.utilities.disable();
    }
    if (!this.projectInaccuracies.predefinedInfluenceFactors) {
      this.paramForm.controls.predefinedScenarios.setValue(false);
      this.paramForm.controls.predefinedScenarios.disable();
    }
    // composite influence factors cannot be selected by the user
    this.decisionData.influenceFactors.forEach((influenceFactor, index) => {
      if (this.isInfluenceFactorComposite(influenceFactor)) this.paramForm.controls.userDefinedScenarioIDs.controls[index].disable();
    });
    this.checkForChangesInCompositeInfluenceFactors();

    this.activeParams = this.paramForm.value;
    this.activeInfluenceFactorToggles = this.currentInfluenceFactorToggles;

    this.robustnessCheckService.onUpdate$
      .pipe(throttleTime(1_000, undefined, { leading: true, trailing: true }), takeUntil(this.onDestroy$))
      .subscribe(() => {
        this.zone.run(() => {
          this.updateResults();
        });
      });
    this.start();
  }

  start() {
    if (this.paramForm.invalid) return;

    const v = this.paramForm.value;
    const selectedAlternatives = v.alternatives.map((checked, index) => (checked ? index : -1)).filter(index => index !== -1);

    const sortedSelectedAlternatives = this.sortedAlternativeIndices.filter(alternativIndex =>
      selectedAlternatives.includes(alternativIndex),
    );

    this.lastUpdateTime = performance.now();
    this.currentResult = {
      iterationCount: 0,
      alternatives: selectedAlternatives.map(alternativeIndex => {
        const expectedPosition = sortedSelectedAlternatives.indexOf(alternativeIndex);
        return {
          avgUtility: 0,
          minUtility: 0,
          maxUtility: 1,
          // Initially a unit matrix
          positionDistribution: selectedAlternatives.map((_, positionIndex) => (positionIndex === expectedPosition ? 1 : 0)),
          utilityHistogram: new Array<number>(ROBUSTNESS_CHECK_HISTOGRAM_BIN_COUNT).fill(0),
        };
      }),
      alternativeAtPositionInfos: selectedAlternatives.map(() =>
        selectedAlternatives.map(() => ({
          drawnCs: [],
          drawnWeights: [],
          stateCounts: new InfluenceFactorStateMap(),
        })),
      ),
    };

    this.iterationsPerSecond = null;
    this.changePerIteration = null;
    this.isPaused = false;
    this.isDone = false;
    this.elapsedTime = 0;

    this.paramForm.markAsPristine();
    this.activeParams = this.paramForm.value;
    this.activeInfluenceFactorToggles = this.currentInfluenceFactorToggles;

    const userDefinedIds = this.currentInfluenceFactorToggles
      .map((isToggled, index) => (isToggled ? index : -1))
      .filter(index => index !== -1);

    this.robustnessCheckService.startWorker({
      // Get the indices of checked alternatives in decisionData (not in rankedIDs)
      selectedAlternatives,
      parameters: {
        influenceFactorScenarios: {
          predefined: v.predefinedScenarios,
          userDefinedIds,
        },
        probabilities: v.probabilities,
        utilityFunctions: v.utilities,
        objectiveWeights: v.objectiveWeights,
      },
      objectivesCount: this.decisionData.objectives.length,
    });
  }

  pauseCalculation() {
    if (this.currentResult.iterationCount === 0) return;

    this.robustnessCheckService.pauseWorker();
    this.isPaused = true;
    this.updateResults();
  }

  resumeCalculation() {
    this.robustnessCheckService.resumeWorker();
    this.lastUpdateTime = performance.now();
    this.isPaused = false;
  }

  private updateResults() {
    const newResult = this.robustnessCheckService.getCurrentResult();
    const newIterations = newResult.iterationCount - this.currentResult.iterationCount;

    if (newIterations === 0) return;

    const currentUpdateTime = performance.now();
    this.iterationsPerSecond = (newIterations / (currentUpdateTime - this.lastUpdateTime)) * 1000;
    this.elapsedTime += currentUpdateTime - this.lastUpdateTime;

    // The sum of changes in the position distribution between this and last update
    const maxChange = Math.max(
      ...flatten(
        newResult.alternatives.map((alternativeResult, alternativeIndex) =>
          alternativeResult.positionDistribution.map((value, valueIndex) =>
            Math.abs(value - this.currentResult.alternatives[alternativeIndex].positionDistribution[valueIndex]),
          ),
        ),
      ),
    );
    // Changes normalized by the number of iterations in this update
    this.changePerIteration = maxChange / newIterations;

    this.currentResult = newResult;
    this.lastUpdateTime = currentUpdateTime;

    if (!this.isDone && this.changePerIteration < this.changeThreshold) {
      this.isDone = true;
      this.pauseCalculation();
    }
  }

  protected isInfluenceFactorComposite(influenceFactor: InfluenceFactor): influenceFactor is CompositeUserDefinedInfluenceFactor {
    return influenceFactor instanceof CompositeUserDefinedInfluenceFactor;
  }

  // composite influence factors are selected iff all underlying factors are selected
  protected checkForChangesInCompositeInfluenceFactors() {
    this.decisionData.influenceFactors.forEach((influenceFactor, index) => {
      if (influenceFactor instanceof CompositeUserDefinedInfluenceFactor) {
        const underlyingFactors = generateListOfUnderlyingUserDefinedFactors(influenceFactor);
        this.paramForm.controls.userDefinedScenarioIDs.controls[index].setValue(
          underlyingFactors.every(underlyingFactor => this.paramForm.controls.userDefinedScenarioIDs.controls[underlyingFactor.id].value),
        );
      }
    });
  }
}
