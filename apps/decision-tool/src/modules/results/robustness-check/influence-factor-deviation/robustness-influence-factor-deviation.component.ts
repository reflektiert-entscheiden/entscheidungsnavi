import { ChangeDetectionStrategy, Component, Input, OnChanges, SimpleChanges } from '@angular/core';
import {
  UserDefinedInfluenceFactor,
  DecisionData,
  IfKey,
  InfluenceFactorStateMap,
  PREDEFINED_INFLUENCE_FACTORS,
  PredefinedIfKey,
  SimpleUserDefinedInfluenceFactor,
} from '@entscheidungsnavi/decision-data';
import { sum } from 'lodash';
import { PREDEFINED_INFLUENCE_FACTOR_NAMES } from '@entscheidungsnavi/widgets';
import { MinMax } from '../../../../worker/robustness-check-messages';

@Component({
  selector: 'dt-robustness-influence-factor-deviation',
  templateUrl: './robustness-influence-factor-deviation.component.html',
  styleUrls: ['./robustness-influence-factor-deviation.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class RobustnessInfluenceFactorDeviationComponent implements OnChanges {
  @Input() tooltipInfo: {
    drawnCs: MinMax[];
    drawnWeights: MinMax[];
    stateCounts: InfluenceFactorStateMap<number[]>;
  };

  selectedTab: 'influence-factors' | 'other-uncertainties' = 'influence-factors';

  relevantInfluenceFactors: Array<{
    key: IfKey;
    stateIndices: number[];
    frequency: number;
    expected: number;
  }>;

  get drawnWeights() {
    const result = new Map<number, MinMax>();
    this.tooltipInfo.drawnWeights.forEach((drawnWeight, objectiveIdx) => {
      if (this.decisionData.weights.getWeight(objectiveIdx).precision > 0 && drawnWeight != null) {
        result.set(objectiveIdx, {
          min: drawnWeight.min,
          max: drawnWeight.max,
        });
      }
    });
    return result;
  }

  get drawnCs() {
    const result = new Map<number, MinMax>();
    this.tooltipInfo.drawnCs.forEach((drawnC, objectiveIdx) => {
      if (
        drawnC != null &&
        ((this.decisionData.objectives[objectiveIdx].isNumerical &&
          this.decisionData.objectives[objectiveIdx].numericalData.utilityfunction.precision > 0) ||
          (this.decisionData.objectives[objectiveIdx].isIndicator &&
            this.decisionData.objectives[objectiveIdx].indicatorData.utilityfunction.precision > 0))
      ) {
        result.set(objectiveIdx, {
          min: drawnC.min,
          max: drawnC.max,
        });
      }
    });
    return result;
  }

  constructor(protected decisionData: DecisionData) {}

  ngOnChanges(changes: SimpleChanges) {
    if ('tooltipInfo' in changes) this.updateMostDeviatingInfluenceFactors();
  }

  isPredefinedKey(key: IfKey): key is PredefinedIfKey {
    return typeof key !== 'number';
  }

  getName(value: (typeof this.relevantInfluenceFactors)[number]) {
    if (this.isPredefinedKey(value.key)) {
      const objective = this.decisionData.objectives[value.key.objectiveIndex];
      return objective.name + (objective.isIndicator ? `/${objective.indicatorData.indicators[value.key.indicatorIndex].name}` : '');
    } else {
      return this.decisionData.influenceFactors[value.key].name;
    }
  }

  getPredefinedDescription(key: PredefinedIfKey) {
    return (
      '(' +
      PREDEFINED_INFLUENCE_FACTOR_NAMES[key.id].name +
      ' ' +
      $localize`in Alternative` +
      ' ' +
      this.decisionData.alternatives[key.alternativeIndex].name +
      ')'
    );
  }

  getStateNames(value: (typeof this.relevantInfluenceFactors)[number]) {
    const key = value.key;

    if (this.isPredefinedKey(key)) {
      return value.stateIndices.map(stateIndex => PREDEFINED_INFLUENCE_FACTOR_NAMES[key.id].stateNames[stateIndex]);
    }

    return this.getCustomStateName(this.decisionData.influenceFactors[key], value.stateIndices);
  }

  private getCustomStateName(iF: UserDefinedInfluenceFactor, stateIndices: number[]): string[] {
    if (iF instanceof SimpleUserDefinedInfluenceFactor) {
      return stateIndices.map(stateIndex => iF.states[stateIndex].name);
    } else {
      return stateIndices.map(stateIndex => {
        const [firstBaseIndex, secondBaseIndex] = iF.getBaseStateIndices(stateIndex);
        return (
          this.getCustomStateName(iF.baseFactors[0], [firstBaseIndex]) + '/' + this.getCustomStateName(iF.baseFactors[1], [secondBaseIndex])
        );
      });
    }
  }

  private updateMostDeviatingInfluenceFactors() {
    this.relevantInfluenceFactors = [];

    for (const [key, stateFrequencies] of this.tooltipInfo.stateCounts.entries()) {
      const relevantStateIndices = stateFrequencies
        .map((frequency, stateIndex) => (frequency > 0 ? stateIndex : -1))
        .filter(index => index !== -1);

      if (relevantStateIndices.length === stateFrequencies.length) {
        // All states have frequency > 0, so this influence factor is not relevant
        continue;
      }

      const ifStates = this.isPredefinedKey(key)
        ? PREDEFINED_INFLUENCE_FACTORS[key.id].states
        : this.decisionData.influenceFactors[key].getStates();

      const frequency = sum(relevantStateIndices.map(index => stateFrequencies[index])),
        expected = sum(relevantStateIndices.map(index => ifStates[index].probability / 100));

      this.relevantInfluenceFactors.push({
        key,
        stateIndices: relevantStateIndices,
        frequency,
        expected,
      });
    }

    // Sort by expected probability (asc)
    this.relevantInfluenceFactors.sort((a, b) => a.expected - b.expected);
  }
}
