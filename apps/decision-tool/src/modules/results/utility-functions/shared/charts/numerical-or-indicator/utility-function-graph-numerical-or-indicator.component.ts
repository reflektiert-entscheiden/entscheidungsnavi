import { Component, EventEmitter, Input, Output } from '@angular/core';
import { getUtilityFunction, Objective } from '@entscheidungsnavi/decision-data';
import '@entscheidungsnavi/tools/number-rounding';
import { fromEvent, merge } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { Interval } from '@entscheidungsnavi/tools';
import { UtilityFunctionNumericalOrIndicatorData } from '../../utility-function-numerical-or-indicator-data';

@Component({
  selector: 'dt-utilityfunction-graph-numerical-or-indicator',
  templateUrl: 'utility-function-graph-numerical-or-indicator.component.html',
  styleUrls: ['utility-function-graph-numerical-or-indicator.component.scss'],
})
export class UtilityFunctionGraphNumericalOrIndicatorComponent {
  @Input() objective: Objective;
  @Input() showIntervals = true;
  @Input() showYAxisLabel = false;
  @Output() changeC = new EventEmitter<number>();

  @Input() data: UtilityFunctionNumericalOrIndicatorData;

  draggingGraph = false;

  get noWidth() {
    return this.data.intervals.levelInterval.isPoint();
  }

  get normalizedAndOrderedLeftInterval(): Interval {
    return this.normalizeObjectiveValueInterval(this.data.intervals.levelInterval);
  }

  get normalizedMidUtilityInterval(): Interval {
    return this.normalizeObjectiveValueInterval(this.data.intervals.midUtilityInterval);
  }

  get utilityValuesOfMidUtilityInterval(): Interval {
    const utilityFunctionWithSmallerCurvature = getUtilityFunction(this.data.utilityFunction.c - this.data.utilityFunction.precision);
    const utilityFunctionWithBiggerCurvature = getUtilityFunction(this.data.utilityFunction.c + this.data.utilityFunction.precision);

    return this.normalizeObjectiveValueInterval(this.data.intervals.midUtilityInterval)
      .mapStart(utilityFunctionWithBiggerCurvature)
      .mapEnd(utilityFunctionWithSmallerCurvature);
  }

  get isInaccurate() {
    return this.data.utilityFunction.precision !== 0;
  }

  get xLabels() {
    const rangeOfObjectiveValues = this.objective.getRangeInterval();

    const part = (rangeOfObjectiveValues.end - rangeOfObjectiveValues.start) / 5;

    const labels: string[] = [];

    labels.push(rangeOfObjectiveValues.start + '');

    for (let i = 1; i < 5; i++) {
      labels.push((rangeOfObjectiveValues.start + i * part).round(1) + '');
    }

    labels.push(rangeOfObjectiveValues.end + '');
    const unit = this.objective.unit();

    return labels.map(value => value + ' ' + unit);
  }

  get graphOfUtilityFunctionWithLowerCurvature() {
    return this.generateFunctionPoints(this.data.utilityFunction.c - this.data.utilityFunction.precision);
  }

  get graphOfUtilityFunctionWithHigherCurvature() {
    return this.generateFunctionPoints(this.data.utilityFunction.c + this.data.utilityFunction.precision);
  }

  get tooltipDots() {
    const c = this.data.utilityFunction.c;

    const dots = this.generateTooltips(c + this.data.utilityFunction.precision);

    if (this.showIntervals) {
      dots.push({ ...this.getTooltip(this.normalizedMidUtilityInterval.start, c + this.data.utilityFunction.precision), red: true });
    }

    if (this.isInaccurate) {
      dots.push(...this.generateTooltips(this.data.utilityFunction.c - this.data.utilityFunction.precision));
      if (this.showIntervals) {
        dots.push({ ...this.getTooltip(this.normalizedMidUtilityInterval.end, c - this.data.utilityFunction.precision), red: true });
      }
    }

    return dots;
  }

  trackTooltips = (index: number, _: any) => index;

  private getScaleValueInPercent(objectiveValue: number): number {
    const rangeOfObjectiveValues = this.objective.getRangeInterval();
    const normalizedValue = (objectiveValue - rangeOfObjectiveValues.start) / rangeOfObjectiveValues.length;
    return normalizedValue * 100;
  }

  private normalizeObjectiveValueInterval(objectiveValueInterval: Interval): Interval {
    return objectiveValueInterval.map(this.getScaleValueInPercent.bind(this)).map(objectiveValueEndpoint => objectiveValueEndpoint / 100);
  }

  mouseDownCatcher(element: HTMLElement, downEvent: MouseEvent) {
    const bounds = element.getBoundingClientRect();

    const rectX = bounds.left;
    const rectY = bounds.top;
    const rectWidth = bounds.width;
    const rectHeight = bounds.height;

    const update = (event: MouseEvent) => {
      if (event.clientX >= rectX && event.clientY >= rectY) {
        event.preventDefault();
        const x = event.clientX - rectX;
        const y = event.clientY - rectY;

        if (x < rectWidth && y < rectHeight) {
          const nX = x / rectWidth;
          const nY = 1 - y / rectHeight;

          const functionData = this.data.utilityFunction;
          let currentC = functionData.c;
          let utilityFunction = getUtilityFunction(currentC);

          let currentY = Math.round(utilityFunction(nX) * 100) / 100;

          const stepSize = 0.1;

          const currentDistance = Math.abs(nY - utilityFunction(nX));

          if (currentY > nY) {
            while (nY < currentY && currentC > -25) {
              currentC -= stepSize;
              utilityFunction = getUtilityFunction(currentC);
              currentY = utilityFunction(nX);

              currentC = Math.round(currentC * 10) / 10;
            }
          } else if (currentY < nY) {
            while (nY > currentY && currentC < 25) {
              currentC += stepSize;
              utilityFunction = getUtilityFunction(currentC);
              currentY = utilityFunction(nX);

              currentC = Math.round(currentC * 10) / 10;
            }
          }

          const newDistance = Math.abs(nY - utilityFunction(nX));

          if (newDistance < currentDistance) {
            this.changeC.emit(currentC);
          }
        }
      }
    };

    this.draggingGraph = true;

    update(downEvent);

    fromEvent(element, 'mousemove')
      .pipe(takeUntil(merge(fromEvent(element, 'mouseup'), fromEvent(element, 'mouseleave'))))
      .subscribe({ next: event => update(event as MouseEvent), complete: () => (this.draggingGraph = false) });
  }

  private generateFunctionPoints(c: number) {
    const points: [number, number][] = [];

    const utilityFunction = getUtilityFunction(c);

    for (let i = 0; i <= 100; i++) {
      const result = utilityFunction(i / 100);
      points.push([i, 100 - result * 100]);
    }

    return `M 0 100 ${points.map(p => `L ${p[0]} ${p[1]}`).join(' ')}`;
  }

  private generateTooltips(c: number) {
    const tooltips: { x: number; y: number; objectiveValue: string; utilityValue: string; red: boolean }[] = [];

    for (let i = 0; i <= 20; i++) {
      tooltips.push(this.getTooltip(i / 20, c));
    }

    return tooltips;
  }

  private getTooltip(normalizedValue: number, c: number) {
    const unit = this.objective.unit();
    const utilityFunction = getUtilityFunction(c);
    const rangeOfObjectiveValues = this.objective.getRangeInterval();
    return {
      x: normalizedValue * 100,
      y: (1 - utilityFunction(normalizedValue)) * 100,
      utilityValue: utilityFunction(normalizedValue).round(2) + '',
      objectiveValue: (rangeOfObjectiveValues.start + rangeOfObjectiveValues.length * normalizedValue).round(2) + ' ' + unit,
      red: false,
    };
  }
}
