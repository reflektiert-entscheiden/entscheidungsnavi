import { Component, Input } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { DecisionData } from '@entscheidungsnavi/decision-data';
import { Objective } from '@entscheidungsnavi/decision-data';
import '@entscheidungsnavi/tools/number-rounding';
import { showManualTradeoffNotification } from '../../../../../shared/manual-tradeoff-notification';

// TODO: Clean up
@Component({
  selector: 'dt-utilityfunction-graph-verbal',
  templateUrl: 'utility-function-graph-verbal.component.html',
  styleUrls: ['utility-function-graph-verbal.component.scss'],
})
export class UtilityFunctionGraphVerbalComponent {
  @Input() objective: Objective;
  @Input() showYAxisLabel = false;

  @Input() selectedLeft: number;
  @Input() selectedMiddle: number;
  @Input() selectedRight: number;

  @Input() hasChanges = true;
  @Input() huge = false;
  @Input() readonly = false;

  get gridColumns() {
    return `min-content min-content repeat(${this.objective.verbalData.options.length}, 1fr)`;
  }

  get hasPrecision() {
    return this.objective.verbalData.precision > 0;
  }

  constructor(
    private decisionData: DecisionData,
    private snackBar: MatSnackBar,
  ) {}

  getPathLine(line: 1 | 2) {
    const divider = 100 / this.objective.verbalData.options.length;

    return `M ${divider / 2} 100 ${this.objective.verbalData.options
      .map((_, i) => {
        let mod =
          i != 0 && i != this.objective.verbalData.options.length - 1
            ? this.relativeDeviation(this.objective.verbalData.utilities[i], this.objective.verbalData.precision)
            : 0;

        if (line === 2) {
          mod = -mod;
        }

        return `L ${i * divider + divider / 2} ${100 - (this.objective.verbalData.utilities[i] + mod)}`;
      })
      .join(' ')}`;
  }

  displayUtility(index: number) {
    return this.objective.verbalData.hasCustomUtilityValues
      ? this.objective.verbalData.utilities[index]
      : Math.floor(this.objective.verbalData.utilities[index]);
  }

  inputChange(input: any, index: number, value: number) {
    if (value < index) {
      value = this.objective.verbalData.utilities[index] = index;
    } else if (value > 100 - (this.objective.verbalData.utilities.length - index - 1)) {
      value = this.objective.verbalData.utilities[index] = 100 - (this.objective.verbalData.utilities.length - index - 1);
    }

    input.value = value;

    this.objective.verbalData.utilities[index] = value;
    this.newBarValue(index, value);
  }

  newBarValue(index: number, value: number) {
    if (value < index) {
      value = this.objective.verbalData.utilities[index] = index;
    } else if (value > 100 - (this.objective.verbalData.utilities.length - index - 1)) {
      value = this.objective.verbalData.utilities[index] = 100 - (this.objective.verbalData.utilities.length - index - 1);
    }

    for (let i = 1; i < this.objective.verbalData.utilities.length - 1; i++) {
      if (i < index && this.objective.verbalData.utilities[i] >= value - (index - i)) {
        this.objective.verbalData.utilities[i] = Math.max(0, value - (index - i));
      } else if (i > index && this.objective.verbalData.utilities[i] < value + (i - index)) {
        this.objective.verbalData.utilities[i] = Math.min(100, value + (i - index));
      }
    }

    this.objective.verbalData.hasCustomUtilityValues = true;
    if (this.decisionData.onUtilityFunctionChange(this.decisionData.objectives.indexOf(this.objective))) {
      showManualTradeoffNotification(this.snackBar);
    }
  }

  relativeDeviation(probability: number, praezision: number) {
    return praezision * Math.min(1 - probability / 100, probability / 100);
  }
}
