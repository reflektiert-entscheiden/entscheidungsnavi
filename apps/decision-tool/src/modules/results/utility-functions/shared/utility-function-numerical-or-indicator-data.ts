import { getInverseUtilityFunction, getUtilityFunction, Objective, ObjectiveType, UtilityFunction } from '@entscheidungsnavi/decision-data';
import { Interval } from '@entscheidungsnavi/tools';

export class UtilityFunctionNumericalOrIndicatorData {
  intervals: {
    levelInterval: Interval;
    midUtilityInterval: Interval;
    ok: boolean;
  } = {
    levelInterval: null,
    midUtilityInterval: null,
    ok: false,
  };

  riskPremium: Interval = null;

  levelIntervalMidpoint: number = null;
  choosingMinProbabilityWithSmallerCurvature: number = null;
  choosingMinProbabilityWithBiggerCurvature: number = null;

  get utilityFunction(): UtilityFunction {
    switch (this.objective.objectiveType) {
      case ObjectiveType.Numerical:
        return this.objective.numericalData.utilityfunction;
      case ObjectiveType.Indicator:
        return this.objective.indicatorData.utilityfunction;
      default:
        throw new Error(`Unsupported objective type: ${this.objective.objectiveType}`);
    }
  }

  constructor(private objective: Objective) {}

  updateData() {
    const projectOnObjectiveValueRange = (objectiveValueNormalized: number) =>
      objectiveValueNormalized * rangeOfObjectiveValues.length + rangeOfObjectiveValues.start;

    const utilityFunction = this.utilityFunction;

    const c = utilityFunction.c ?? 0;
    const precision = utilityFunction.precision ?? 0;
    const width = utilityFunction.width ?? 0.5;
    const level = utilityFunction.level ?? 0.5;

    const rangeOfObjectiveValues = this.objective.getRangeInterval();
    const levelIntervalNormalized = new Interval(level - level * width, level - level * width + width);
    const midUtilityIntervalNormalized = this.getIntervalOfMidUtility(levelIntervalNormalized, c, precision);

    // update level interval
    this.intervals.levelInterval = levelIntervalNormalized.map(projectOnObjectiveValueRange);

    // update mid utility interval
    this.intervals.midUtilityInterval = midUtilityIntervalNormalized.map(projectOnObjectiveValueRange);
    this.intervals.ok = levelIntervalNormalized.includes(midUtilityIntervalNormalized, true);

    // update the probability of choosing the lower bound of the level interval
    [this.choosingMinProbabilityWithSmallerCurvature, this.choosingMinProbabilityWithBiggerCurvature] =
      this.calculateChoosingMinProbabilities(c, precision, levelIntervalNormalized);

    // update the midpoint of the level interval and based on that calculate the risk premium values
    this.levelIntervalMidpoint = projectOnObjectiveValueRange(levelIntervalNormalized.midpoint);

    this.riskPremium = new Interval(
      this.levelIntervalMidpoint - this.intervals.midUtilityInterval.ordered().end,
      this.levelIntervalMidpoint - this.intervals.midUtilityInterval.ordered().start,
    );
  }

  getIntervalOfMidUtility(levelInterval: Interval, c: number, precision: number): Interval {
    const utilityFunctionWithSmallerCurvature = getUtilityFunction(c - precision);
    const inverseUtilityFunctionWithSmallerCurvature = getInverseUtilityFunction(c - precision);

    const utilityFunctionWithBiggerCurvature = getUtilityFunction(c + precision);
    const inverseUtilityFunctionWithBiggerCurvature = getInverseUtilityFunction(c + precision);

    const projectedLevelIntervalWithSmallerCurvature = levelInterval.map(utilityFunctionWithSmallerCurvature);
    const projectedLevelIntervalWithBiggerCurvature = levelInterval.map(utilityFunctionWithBiggerCurvature);

    // take the midpoints of the projected level intervals and calculate their preimages.
    return new Interval(
      inverseUtilityFunctionWithBiggerCurvature(projectedLevelIntervalWithBiggerCurvature.midpoint),
      inverseUtilityFunctionWithSmallerCurvature(projectedLevelIntervalWithSmallerCurvature.midpoint),
    );
  }

  calculateChoosingMinProbabilities(
    c: number,
    precision: number,
    levelIntervalNormalized: Interval,
  ): [choosingMinProbabilityWithSmallerCurvature: number, choosingMinProbabilityWithBiggerCurvature: number] {
    // c - precision
    const utilityFunctionWithSmallerCurvature = getUtilityFunction(c - precision);

    const utilityOfLevelIntervalWithSmallerCurvature = levelIntervalNormalized.map(utilityFunctionWithSmallerCurvature);

    const choosingMinProbabilityWithSmallerCurvature =
      (utilityFunctionWithSmallerCurvature(levelIntervalNormalized.midpoint) - utilityOfLevelIntervalWithSmallerCurvature.start) /
      utilityOfLevelIntervalWithSmallerCurvature.length;

    // c + precision
    const utilityFunctionWithBiggerCurvature = getUtilityFunction(c + precision);

    const utilityOfLevelIntervalWithBiggerCurvature = levelIntervalNormalized.map(utilityFunctionWithBiggerCurvature);

    const choosingMinProbabilityWithBiggerCurvature =
      (utilityFunctionWithBiggerCurvature(levelIntervalNormalized.midpoint) - utilityOfLevelIntervalWithBiggerCurvature.start) /
      utilityOfLevelIntervalWithBiggerCurvature.length;

    return [choosingMinProbabilityWithSmallerCurvature, choosingMinProbabilityWithBiggerCurvature];
  }
}
