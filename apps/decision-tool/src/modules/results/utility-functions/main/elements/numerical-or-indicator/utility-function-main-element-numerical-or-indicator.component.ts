import { Component, Input, OnChanges, SimpleChanges } from '@angular/core';
import { DecisionData } from '@entscheidungsnavi/decision-data';
import { Objective, ObjectiveType, UtilityFunction } from '@entscheidungsnavi/decision-data';
import { clamp, round } from 'lodash';
import { MatSnackBar } from '@angular/material/snack-bar';
import { UtilityFunctionNumericalOrIndicatorData } from '../../../shared/utility-function-numerical-or-indicator-data';
import { showManualTradeoffNotification } from '../../../../../shared/manual-tradeoff-notification';

@Component({
  selector: 'dt-utility-function-main-element-numerical-or-indicator',
  templateUrl: './utility-function-main-element-numerical-or-indicator.component.html',
  styleUrls: ['./utility-function-main-element-numerical-or-indicator.component.scss'],
})
export class UtilityFunctionElementNumericalOrIndicatorComponent implements OnChanges {
  readonly cMax = 25;
  readonly pMax = 10;

  @Input() objective: Objective;
  @Input() readonly = false;

  utilityFunction: UtilityFunction;
  graphData: UtilityFunctionNumericalOrIndicatorData;

  constructor(
    private decisionData: DecisionData,
    private snackBar: MatSnackBar,
  ) {}

  updatePrecision(newPrecision: number) {
    if (newPrecision == null) {
      return;
    }

    this.utilityFunction.precision = clamp(round(newPrecision, 1), 0, this.pMax);
    this.onChange();
  }

  updateC(newC: number) {
    if (newC == null) {
      return;
    }

    this.utilityFunction.c = clamp(round(newC, 1), -this.cMax, this.cMax);
    if (this.decisionData.onUtilityFunctionChange(this.decisionData.objectives.indexOf(this.objective))) {
      showManualTradeoffNotification(this.snackBar);
    }
    this.onChange();
  }

  adjustPrecision(change: number) {
    this.updatePrecision(this.utilityFunction.precision + change);
  }

  adjustC(change: number) {
    this.updateC(this.utilityFunction.c + change);
  }

  ngOnChanges(changes: SimpleChanges) {
    if ('objective' in changes) {
      this.utilityFunction = this.getUtilityFunction();

      this.graphData = new UtilityFunctionNumericalOrIndicatorData(this.objective);
      this.graphData.updateData();
    }
  }

  onChange() {
    this.graphData.updateData();
  }

  private getUtilityFunction(): UtilityFunction {
    switch (this.objective.objectiveType) {
      case ObjectiveType.Numerical:
        return this.objective.numericalData.utilityfunction;
      case ObjectiveType.Indicator:
        return this.objective.indicatorData.utilityfunction;
      default:
        throw new Error(`Unsupported objective type: ${this.objective.objectiveType}`);
    }
  }
}
