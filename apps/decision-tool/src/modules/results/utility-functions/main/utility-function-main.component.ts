import { Component } from '@angular/core';
import { DecisionData, ObjectiveType } from '@entscheidungsnavi/decision-data';
import { HelpMenuProvider, helpPage } from '../../../../app/help/help';
import { Help1Component } from '../../help/utility-functions/help1/help1.component';
import { Help2Component } from '../../help/utility-functions/help2/help2.component';
import { HelpBackgroundComponent } from '../../help/help-background/help-background.component';
import { Navigation, NavLine, navLineElement } from '../../../shared/navline';

@Component({
  templateUrl: './utility-function-main.component.html',
  styleUrls: ['./utility-function-main.component.scss'],
})
export class UtilityFunctionMainComponent implements Navigation, HelpMenuProvider {
  navLine = new NavLine({
    left: [navLineElement().back('/impactmodel').build()],
    right: [navLineElement().continue('/results/steps/2').build()],
  });

  helpMenu = [
    helpPage()
      .name($localize`So funktioniert's`)
      .component(Help1Component)
      .build(),
    helpPage()
      .name($localize`Weitere Hinweise`)
      .component(Help2Component)
      .build(),
    helpPage()
      .name($localize`Hintergrundwissen zum Schritt 5`)
      .component(HelpBackgroundComponent)
      .build(),
    helpPage()
      .name($localize`Videos`)
      .youtube(4)
      .build(),
  ];

  selectedObjective = -1;

  get objective() {
    return this.selectedObjective !== -1 ? this.decisionData.objectives[this.selectedObjective] : null;
  }

  get explanation() {
    switch (this.objective.objectiveType) {
      case ObjectiveType.Numerical:
        return this.objective.numericalData.utilityfunction.explanation;
      case ObjectiveType.Verbal:
        return this.objective.verbalData.utilityFunctionExplanation;
      case ObjectiveType.Indicator:
        return this.objective.indicatorData.utilityfunction.explanation;
    }
  }

  set explanation(value: string) {
    switch (this.objective.objectiveType) {
      case ObjectiveType.Numerical:
        this.objective.numericalData.utilityfunction.explanation = value;
        break;
      case ObjectiveType.Verbal:
        this.objective.verbalData.utilityFunctionExplanation = value;
        break;
      case ObjectiveType.Indicator:
        this.objective.indicatorData.utilityfunction.explanation = value;
        break;
    }
  }

  constructor(private decisionData: DecisionData) {}

  verbalizeDisplayOptionsCount() {
    return this.objective.isVerbal ? $localize`drei` : $localize`vier`;
  }
}
