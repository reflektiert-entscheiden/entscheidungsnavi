import { Component, Input, OnChanges, SimpleChanges } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { DecisionData } from '@entscheidungsnavi/decision-data';
import { Objective } from '@entscheidungsnavi/decision-data';
import { clamp, round } from 'lodash';
import { showManualTradeoffNotification } from '../../../../shared/manual-tradeoff-notification';

@Component({
  selector: 'dt-utility-function-detailed-verbal',
  templateUrl: 'utility-function-detailed-verbal.component.html',
  styleUrls: ['../utility-function-detailed.scss'],
})
export class UtilityFunctionDetailedVerbalComponent implements OnChanges {
  @Input() objective: Objective;
  @Input() readonly = false;

  readonly pMax = 50;
  readonly cMax = 25;

  sigDigits = 2;

  selectedMiddle: number;
  selectedWidth: number;
  selectedLevel: number;

  constructor(
    private decisionData: DecisionData,
    private snackBar: MatSnackBar,
  ) {}

  ngOnChanges(changes: SimpleChanges) {
    if ('objective' in changes) {
      const optionCount = this.objective.verbalData.options.length;

      this.selectedMiddle = 0;
      this.selectedLevel = Math.floor(optionCount / 2) - 1;
      this.selectedWidth = 2;
    }
  }

  updateC(newC: number) {
    this.objective.verbalData.c = clamp(round(newC, 1), -this.cMax, this.cMax);
    this.objective.verbalData.adjustUtilityToFunction();
    if (this.decisionData.onUtilityFunctionChange(this.decisionData.objectives.indexOf(this.objective))) {
      showManualTradeoffNotification(this.snackBar);
    }
  }

  unlockC() {
    this.objective.verbalData.hasCustomUtilityValues = false;
    this.objective.verbalData.adjustUtilityToFunction();
  }

  adjustC(change: number) {
    this.updateC(this.objective.verbalData.c + change);
  }

  updatePrecision(newPrecision: number) {
    this.objective.verbalData.precision = clamp(round(newPrecision, 1), 0, this.pMax);
  }

  adjustPrecision(change: number) {
    this.updatePrecision(this.objective.verbalData.precision + change);
  }

  updateSigDigits(newValue: number) {
    this.sigDigits = clamp(newValue, 0, 10);
  }

  firstIndex() {
    return this.selectedLevel;
  }

  middleIndex() {
    return this.selectedLevel + 1 + this.selectedMiddle;
  }

  lastIndex() {
    return this.selectedLevel + this.selectedWidth;
  }

  intervalFirstName() {
    return this.objective.verbalData.options[this.firstIndex()];
  }

  intervalMiddleName() {
    return this.objective.verbalData.options[this.middleIndex()];
  }

  intervalLastName() {
    return this.objective.verbalData.options[this.lastIndex()];
  }

  intervalFirstPercent() {
    const firstUtility = this.objective.verbalData.utilities[this.firstIndex()];
    const middleUtility = this.objective.verbalData.utilities[this.middleIndex()];
    const lastUtility = this.objective.verbalData.utilities[this.lastIndex()];

    return ((middleUtility - firstUtility) / (lastUtility - firstUtility)) * 100;
  }

  intervalSecondPercent() {
    return 100 - this.intervalFirstPercent();
  }

  levelMin() {
    return 0;
  }

  levelMax() {
    return this.objective.verbalData.options.length - 1 - this.selectedWidth;
  }

  widthMin() {
    return 2;
  }

  widthMax() {
    return Math.floor(this.objective.verbalData.options.length - 1);
  }

  widthChange(value: number) {
    this.selectedLevel += Math.min(0, this.objective.verbalData.options.length - 1 - (this.selectedLevel + value));
    this.selectedMiddle = Math.min(value - 2, this.selectedMiddle);
  }

  round(input: number) {
    return input.round(this.sigDigits);
  }
}
