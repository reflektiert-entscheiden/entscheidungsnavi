import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { DecisionData } from '@entscheidungsnavi/decision-data';
import { DecisionDataState } from '../../../shared/decision-data-state';

@Component({
  selector: 'dt-utility-function-detailed',
  templateUrl: './utility-function-detailed.component.html',
})
export class UtilityFunctionDetailedComponent implements OnInit {
  objectiveIdx: number;

  get objective() {
    return this.decisionData.objectives[this.objectiveIdx];
  }

  constructor(
    private decisionData: DecisionData,
    private route: ActivatedRoute,
    protected decisionDataState: DecisionDataState,
  ) {}

  ngOnInit() {
    // get the ZielID from the URL/ActivatedRoute
    this.route.params.subscribe((params: Params) => {
      this.objectiveIdx = +params['objectiveId'];
    });
  }
}
