import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ResultsOverviewComponent } from './main';
import {
  ObjectiveWeightingDetailedComponent,
  objectiveWeightingDetailedGuard,
  ObjectiveWeightingMainComponent,
  ObjectiveWeightingOverviewComponent,
} from './objective-weighting';
import { ProContraComponent } from './pro-contra';
import { RobustnessCheckComponent } from './robustness-check';
import { SensitivityAnalysisComponent } from './sensitivity-analysis';
import {
  UtilityFunctionDetailedComponent,
  utilityFunctionDetailedGuard,
  UtilityFunctionMainComponent,
  UtilityFunctionOverviewComponent,
} from './utility-functions';
import { ValidateObjectiveWeightingGuard } from './guards/validate-objective-weighting.guard';
import { ValidateUtilityFunctionsGuard } from './guards/validate-utility-functions.guard';
import { RiskComparisonComponent } from './risk-comparison/risk-comparison.component';
import { ObjectiveWeightsAnalysisComponent } from './objective-weights-analysis/objective-weights-analysis.component';
import { atLeastTwoObjectivesGuard } from './guards/at-least-two-objectives.guard';
import { CostUtilityAnalysisComponent } from './cost-utility-analysis/cost-utility-analysis.component';
import { UtilityBasedTornadoDiagramComponent } from './utility-based-tornado-diagram/utility-based-tornado-diagram.component';

const routes: Routes = [
  {
    path: 'hints',
    redirectTo: 'steps',
  },
  {
    path: 'steps/1',
    component: UtilityFunctionMainComponent,
    children: [
      {
        path: '',
        component: UtilityFunctionOverviewComponent,
      },
      {
        path: 'detailed/:objectiveId',
        canActivate: [utilityFunctionDetailedGuard],
        component: UtilityFunctionDetailedComponent,
      },
    ],
  },
  {
    path: 'steps/2',
    canActivate: [ValidateUtilityFunctionsGuard],
    component: ObjectiveWeightingMainComponent,
    children: [
      {
        path: '',
        pathMatch: 'full',
        component: ObjectiveWeightingOverviewComponent,
      },
      {
        path: 'detailed/:objectiveId',
        canActivate: [objectiveWeightingDetailedGuard],
        component: ObjectiveWeightingDetailedComponent,
      },
    ],
  },
  {
    path: '',
    canActivate: [ValidateObjectiveWeightingGuard, ValidateUtilityFunctionsGuard],
    children: [
      {
        path: '',
        pathMatch: 'full',
        component: ResultsOverviewComponent,
      },
      {
        path: 'main',
        component: ResultsOverviewComponent,
      },
      { path: 'pro-contra', component: ProContraComponent },
      { path: 'sensitivity-analysis', component: SensitivityAnalysisComponent },
      {
        path: 'robustness-check',
        component: RobustnessCheckComponent,
      },
      {
        path: 'risk-comparison',
        component: RiskComparisonComponent,
      },
      {
        path: 'objective-weight-analysis',
        canActivate: [atLeastTwoObjectivesGuard('/results')],
        component: ObjectiveWeightsAnalysisComponent,
      },
      {
        path: 'cost-utility-analysis',
        component: CostUtilityAnalysisComponent,
      },
      {
        path: 'utility-based-tornado-diagram',
        component: UtilityBasedTornadoDiagramComponent,
      },
    ],
  },
  // ------------------------- REDIRECTS FOR LEGACY PATHS -------------------------
  {
    path: 'auswertung-uebersicht',
    redirectTo: 'main',
  },
  {
    path: 'auswertung-relativer-vergleich',
    redirectTo: 'pro-contra',
  },
  {
    path: 'auswertung-pro-contra',
    redirectTo: 'pro-contra',
  },
  {
    path: 'auswertung-sensitivitaetsanalyse',
    redirectTo: 'sensitivity-analysis',
  },
  {
    path: 'auswertung-robustheitstest',
    redirectTo: 'robustness-check',
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ResultsRoutingModule {}
