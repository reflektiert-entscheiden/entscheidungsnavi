import { Component, Input, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { DecisionData, getUtilityMatrixMeta } from '@entscheidungsnavi/decision-data';
import { range, round, sortBy } from 'lodash';
import { PolarChartItem } from '@entscheidungsnavi/widgets/charts';
import { DisplayAtMaxWidth } from '../../../app/interfaces/display-at-max-width';
import { HelpMenuProvider } from '../../../app/help/help';
import { getHelpMenu } from '../help';
import { Navigation, NavLine, navLineElement } from '../../shared/navline';
import { LanguageService } from '../../../app/data/language.service';

/*
    Note:
    - bar bar-chart is only available with at least two *alternatives*.
    - polar bar-chart is only available with at least three *objectives*.
 */

@Component({
  selector: 'dt-pro-and-contra',
  styleUrls: ['./pro-contra.component.scss'],
  templateUrl: './pro-contra.component.html',
})
@DisplayAtMaxWidth
export class ProContraComponent implements OnInit, Navigation, HelpMenuProvider {
  @ViewChild('helpContent') helpContent: TemplateRef<any>;

  @Input()
  showTitle = true;

  protected objectiveOrder: 'default' | 'weights' = 'default';
  protected sortedObjectiveIndices: number[]; // order in which objectives should be shown
  protected objectiveNames: string[];

  private alternativeRanking: number[]; // sorted alternative indices by utility value
  protected indicesOfCheckedAlternatives: number[];
  protected checkedAlternatives: PolarChartItem[];

  protected referenceAlternativeIndex = -1;

  protected utilityMatrix: number[][];

  protected display: 'bar' | 'polar' = 'polar';

  navLine = new NavLine({ left: [navLineElement().back('/results').build()] });

  helpMenu = getHelpMenu(this.languageService.steps, 'pro-contra');

  constructor(
    protected decisionData: DecisionData,
    private languageService: LanguageService,
  ) {}

  ngOnInit() {
    if (this.decisionData.objectives.length < 3) {
      this.display = 'bar';
    }

    this.objectiveNames = this.decisionData.objectives.map(objective => objective.name);

    this.utilityMatrix = getUtilityMatrixMeta(this.decisionData.outcomes, this.decisionData.objectives);

    this.alternativeRanking = this.getAlternativeRanking();

    this.indicesOfCheckedAlternatives = this.alternativeRanking.slice(0, 6).map(rank => rank - 1);

    this.handleObjectiveOrderUpdate();
    this.handleAlternativeToggledUpdate();
  }

  protected getAlternativeRanking() {
    const utilityValues = this.decisionData.getAlternativeUtilities().map(v => round(v, 2));

    const alternativeOrderingForRanking = range(this.decisionData.alternatives.length).sort(
      (alternativeIndexA, alternativeIndexB) => utilityValues[alternativeIndexB] - utilityValues[alternativeIndexA],
    );

    return range(this.decisionData.alternatives.length).map(
      alternativeIndex => alternativeOrderingForRanking.indexOf(alternativeIndex) + 1,
    );
  }

  protected handleAlternativeToggledUpdate() {
    this.indicesOfCheckedAlternatives = sortBy(this.indicesOfCheckedAlternatives);
    this.checkedAlternatives = this.indicesOfCheckedAlternatives.map(alternativeIndex => ({
      id: alternativeIndex,
      label: this.decisionData.alternatives[alternativeIndex].name,
      values: this.utilityMatrix[alternativeIndex],
    }));

    if (!this.indicesOfCheckedAlternatives.includes(this.referenceAlternativeIndex)) {
      this.referenceAlternativeIndex = -1;
    }
  }

  protected handleObjectiveOrderUpdate() {
    this.sortedObjectiveIndices = range(this.decisionData.objectives.length);

    if (this.objectiveOrder === 'weights') {
      const objectiveWeightValues = this.decisionData.weights.getWeightValues();

      this.sortedObjectiveIndices.sort(
        (objectiveIndex1, objectiveIndex2) => objectiveWeightValues[objectiveIndex2] - objectiveWeightValues[objectiveIndex1],
      );
    }
  }
}
