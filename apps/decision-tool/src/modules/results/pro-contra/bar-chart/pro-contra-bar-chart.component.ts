import { Component, computed, input, signal } from '@angular/core';
import { DecisionData, Objective, ObjectiveType, Outcome } from '@entscheidungsnavi/decision-data';

@Component({
  selector: 'dt-pro-contra-bar-chart',
  styleUrls: ['./pro-contra-bar-chart.component.scss'],
  templateUrl: './pro-contra-bar-chart.component.html',
})
export class ProContraBarChartComponent {
  shownAlternativeIndices = input.required<number[]>();
  referenceAlternativeIndex = input<number>(-1);
  sortedObjectiveIndices = input.required<number[]>();

  hoveredObjectiveIndex = signal(-1);

  // Contains all possible rows, sorted by the utility of the alternative.
  rows = computed(() => {
    const alternativeUtilities = this.decisionData.getAlternativeUtilities();

    const alternativesWithIndices = this.decisionData.alternatives.map((alternative, alternativeIndex) => ({
      alternative,
      alternativeIndex,
    }));
    alternativesWithIndices.sort((a, b) => alternativeUtilities[b.alternativeIndex] - alternativeUtilities[a.alternativeIndex]);

    const alternativesWithRanks = alternativesWithIndices.map((element, index) => ({ ...element, rank: index }));

    return alternativesWithRanks.map(({ alternative, alternativeIndex, rank }) => {
      const weightedUtility = this.weightedUtilityMatrix[alternativeIndex];
      return { alternative, alternativeIndex, rank, weightedUtility };
    });
  });

  // Contains all rows that are shown, with the difference values to the reference.
  filteredRowsWithDifference = computed(() => {
    const shownAlternativeIndices = this.shownAlternativeIndices();

    const getReferenceValueFor = (objectiveIndex: number) => {
      if (this.referenceAlternativeIndex() >= 0) {
        return this.weightedUtilityMatrix[this.referenceAlternativeIndex()][objectiveIndex];
      } else {
        return (
          this.weightedUtilityMatrix.reduce((sum, row, alternativeIndex) => {
            if (!shownAlternativeIndices.includes(alternativeIndex)) {
              return sum;
            }

            return sum + row[objectiveIndex];
          }, 0) / shownAlternativeIndices.length
        );
      }
    };

    return this.rows()
      .filter(row => shownAlternativeIndices.includes(row.alternativeIndex))
      .map(row => {
        const difference = row.weightedUtility.map((value, objectiveIndex) => value - getReferenceValueFor(objectiveIndex));
        return { ...row, difference };
      });
  });

  // Contains the maximum absolute difference between a shown alternative and its reference.
  maxDifference = computed(() => {
    return Math.max(
      ...this.filteredRowsWithDifference()
        .map(row => row.difference)
        .flat()
        .map(Math.abs),
    );
  });

  // Maps objective indices to the order in which they are displayed.
  objectiveOrderMapping = computed(() => {
    return this.decisionData.objectives.map((_, objectiveIndex) =>
      this.sortedObjectiveIndices().findIndex(index => index === objectiveIndex),
    );
  });

  // Contains verbal descriptions of every outcome.
  outcomeDescriptions = computed(() => {
    return this.decisionData.alternatives.map((alternative, alternativeIndex) =>
      this.decisionData.objectives.map((objective, objectiveIndex) => {
        let outcomeText = '';
        const outcome = this.decisionData.outcomes[alternativeIndex][objectiveIndex];

        if (outcome.processed) {
          const minValue = this.getOutcomeValueExtrema('min', objective, outcome);
          const maxValue = this.getOutcomeValueExtrema('max', objective, outcome);

          outcomeText += minValue;
          if (outcome.influenceFactor && maxValue !== minValue) {
            outcomeText += ' ' + $localize`bis` + ' ' + maxValue;
          }
        } else {
          outcomeText = '?';
        }

        // Append unit if possible.
        if (objective.objectiveType === ObjectiveType.Numerical && objective.numericalData.unit) {
          outcomeText += ' ' + objective.numericalData.unit;
        } else if (objective.objectiveType === ObjectiveType.Indicator) {
          outcomeText += ' ' + objective.indicatorData.aggregatedUnit;
        }

        return outcomeText;
      }),
    );
  });

  gridColumnTemplate: string;

  private readonly weightedUtilityMatrix: number[][];

  constructor(protected decisionData: DecisionData) {
    this.gridColumnTemplate = `minmax(250px, max-content) repeat(${this.decisionData.objectives.length}, 200px`;
    this.weightedUtilityMatrix = this.decisionData.getWeightedUtilityMatrix();
  }

  protected abs(value: number) {
    return Math.abs(value);
  }

  getOutcomeValueExtrema(extrema: 'min' | 'max', objective: Objective, outcome: Outcome): string {
    if (objective.objectiveType === ObjectiveType.Indicator) {
      const aggregationFunction = objective.indicatorData.aggregationFunction;
      const aggregatedValues = outcome.values.map(value => aggregationFunction(value));
      const minOrMaxIndicatorOutcomeValue = extrema === 'min' ? Math.min(...aggregatedValues) : Math.max(...aggregatedValues);
      return '' + minOrMaxIndicatorOutcomeValue.round(2);
    } else {
      const minOrMaxOutcomeValue = extrema === 'min' ? Math.min(...outcome.values.flat(2)) : Math.max(...outcome.values.flat(2));

      if (objective.objectiveType === ObjectiveType.Verbal) {
        return objective.verbalData.options[minOrMaxOutcomeValue - 1];
      } else {
        return '' + minOrMaxOutcomeValue;
      }
    }
  }
}
