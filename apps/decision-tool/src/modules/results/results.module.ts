import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { KatexModule } from '@entscheidungsnavi/widgets/katex';
import { ChartsModule, PolarChartComponent } from '@entscheidungsnavi/widgets/charts';
import {
  ConfirmPopoverDirective,
  HoverPopOverDirective,
  NoteBtnComponent,
  NoteBtnPresetPipe,
  LimitAwareNumberPipe,
  OverflowDirective,
  RichTextEditorComponent,
  RiskProfileChartComponent,
  ScrollShadowComponent,
  ScrollShadowInnerComponent,
  ToggleButtonComponent,
  WidthTriggerDirective,
} from '@entscheidungsnavi/widgets';
import { SelectLineComponent } from '@entscheidungsnavi/widgets/select';
import { TornadoDiagramComponent } from '@entscheidungsnavi/widgets/tornado-diagram/tornado-diagram.component';
import { SharedModule } from '../shared/shared.module';
import { NoteHoverComponent } from '../shared/note-hover/note-hover.component';
import { ChartRankingComponent } from './chart-ranking';
import { ResultsOverviewComponent } from './main';
import {
  DoubleSliderComponent,
  ObjectiveWeightingDetailedComponent,
  ObjectiveWeightingMainComponent,
  ObjectiveWeightingNavigationComponent,
  ObjectiveWeightingOverviewComponent,
  ReferenceObjectiveSelectionComponent,
  ScaleDescriptorComponent,
  ScaleValuesComponent,
  TradeoffComparisonComponent,
} from './objective-weighting';
import { ProContraBarChartComponent, ProContraComponent } from './pro-contra';
import { ResultsRoutingModule } from './results-routing.module';
import {
  ChartNutzenerwartungswertComponent,
  ChartRangpositionPartComponent,
  RobustnessCheckChartComponent,
  RobustnessCheckComponent,
  RobustnessInfluenceFactorDeviationComponent,
} from './robustness-check';
import { SensitivityAnalysisComponent } from './sensitivity-analysis';
import {
  UtilityFunctionDetailedComponent,
  UtilityFunctionDetailedNumericalOrIndicatorComponent,
  UtilityFunctionDetailedVerbalComponent,
  UtilityFunctionElementNumericalOrIndicatorComponent,
  UtilityFunctionElementVerbalComponent,
  UtilityFunctionGraphNumericalOrIndicatorComponent,
  UtilityFunctionGraphVerbalComponent,
  UtilityFunctionMainComponent,
  UtilityFunctionOverviewComponent,
} from './utility-functions';
import { OutcomeSelectionModalComponent } from './sensitivity-analysis/outcome-selection-modal/outcome-selection-modal.component';
import { MatrixPopoverComponent } from './sensitivity-analysis/matrix-popover/matrix-popover.component';
import { Help1Component as Hint1Help1Component } from './help/utility-functions/help1/help1.component';
import { Help2Component as Hint1Help2Component } from './help/utility-functions/help2/help2.component';
import { Help1Component as Hint2Help1Component } from './help/objective-weighting/help1/help1.component';
import { Help2Component as Hint2Help2Component } from './help/objective-weighting/help2/help2.component';
import { HelpBackgroundComponent } from './help/help-background/help-background.component';
import { HelpMainEducational1Component } from './help/help-main-educational/help-main-educational-1.component';
import { UtilityFunctionNavigationComponent } from './utility-functions/main/utility-function-navigation.component';
import { RiskComparisonComponent } from './risk-comparison/risk-comparison.component';
import { HelpMainEducational2Component } from './help/help-main-educational/help-main-educational-2/help-main-educational-2.component';
import { CostUtilityAnalysisComponent } from './cost-utility-analysis/cost-utility-analysis.component';
import { UtilityBasedTornadoDiagramComponent } from './utility-based-tornado-diagram/utility-based-tornado-diagram.component';

@NgModule({
  declarations: [
    // Utility Function
    UtilityFunctionMainComponent,
    UtilityFunctionDetailedVerbalComponent,
    UtilityFunctionDetailedComponent,
    UtilityFunctionDetailedNumericalOrIndicatorComponent,
    UtilityFunctionGraphNumericalOrIndicatorComponent,
    UtilityFunctionElementNumericalOrIndicatorComponent,
    UtilityFunctionElementVerbalComponent,
    UtilityFunctionGraphVerbalComponent,
    UtilityFunctionOverviewComponent,
    UtilityFunctionNavigationComponent,

    // Objective Weighting
    ObjectiveWeightingMainComponent,
    ObjectiveWeightingNavigationComponent,
    ObjectiveWeightingOverviewComponent,
    ObjectiveWeightingDetailedComponent,
    ReferenceObjectiveSelectionComponent,
    TradeoffComparisonComponent,
    DoubleSliderComponent,
    ScaleDescriptorComponent,
    ScaleValuesComponent,

    // Main results
    ResultsOverviewComponent,
    RobustnessCheckComponent,
    ChartNutzenerwartungswertComponent,
    ChartRangpositionPartComponent,
    SensitivityAnalysisComponent,
    ProContraBarChartComponent,
    Hint1Help1Component,
    Hint1Help2Component,
    Hint2Help1Component,
    Hint2Help2Component,
    HelpBackgroundComponent,
    ProContraComponent,
    HelpMainEducational1Component,
    OutcomeSelectionModalComponent,
    MatrixPopoverComponent,
    RobustnessCheckChartComponent,
    RobustnessInfluenceFactorDeviationComponent,
    RiskComparisonComponent,
    HelpMainEducational2Component,
    CostUtilityAnalysisComponent,

    UtilityBasedTornadoDiagramComponent,
  ],
  imports: [
    CommonModule,
    ResultsRoutingModule,
    SharedModule,
    KatexModule,
    ChartsModule,
    OverflowDirective,
    NoteBtnPresetPipe,
    NoteBtnComponent,
    HoverPopOverDirective,
    PolarChartComponent,
    RichTextEditorComponent,
    NoteHoverComponent,
    WidthTriggerDirective,
    ConfirmPopoverDirective,
    RiskProfileChartComponent,
    SelectLineComponent,
    ChartRankingComponent,
    ScrollShadowComponent,
    ScrollShadowInnerComponent,
    ToggleButtonComponent,
    LimitAwareNumberPipe,
    TornadoDiagramComponent,
  ],
  exports: [
    SensitivityAnalysisComponent,
    ProContraComponent,
    RobustnessCheckComponent,
    UtilityFunctionNavigationComponent,
    UtilityFunctionOverviewComponent,
    UtilityFunctionDetailedComponent,
    ObjectiveWeightingOverviewComponent,
    ObjectiveWeightingDetailedComponent,
    ObjectiveWeightingNavigationComponent,
    ResultsOverviewComponent,
    RiskComparisonComponent,
    CostUtilityAnalysisComponent,
    UtilityBasedTornadoDiagramComponent,
  ],
})
export class ResultsModule {}
