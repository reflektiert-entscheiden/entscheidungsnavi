import { Component, Input, TemplateRef, ViewChild } from '@angular/core';
import {
  AggregationFunction,
  AggregationSetting,
  applyWeightsToUtilityMatrix,
  calculateIndicatorValue,
  CompositeUserDefinedInfluenceFactor,
  UserDefinedInfluenceFactor,
  DecisionData,
  getAlternativeUtilities,
  getIndicatorAggregationFunction,
  getIndicatorUtilityFunction,
  getNumericalUtilityFunction,
  getOutcomeUtilityFunction,
  getVerbalUtilityFunction,
  Indicator,
  InfluenceFactor,
  iterateUserDefinedScenarios,
  NumericalObjectiveData,
  Objective,
  ObjectiveType,
  ObjectiveWeight,
  Outcome,
  PREDEFINED_INFLUENCE_FACTORS,
  PredefinedInfluenceFactor,
  SimpleUserDefinedInfluenceFactor,
} from '@entscheidungsnavi/decision-data';
import { isEqual, range, round, sortBy, throttle, zip } from 'lodash';
import { SelectComponent } from '@entscheidungsnavi/widgets/select';
import { MatDialog } from '@angular/material/dialog';
import { assertUnreachable } from '@entscheidungsnavi/tools/assert-unreachable';
import { Interval } from '@entscheidungsnavi/tools';
import { PersistentSetting, PersistentSettingParent } from '@entscheidungsnavi/widgets';
import { HelpMenuProvider } from '../../../app/help/help';
import { getHelpMenu } from '../help/help-menu';
import { Navigation, NavLine, navLineElement } from '../../shared/navline';
import { LanguageService } from '../../../app/data/language.service';
import {
  InfluenceFactorSA,
  OutcomeSA,
  UtilityFunctionNumericalSA,
  UtilityFunctionVerbalSA,
  WeightsOfIndicatorsSA,
  WeightsOfObjectivesSA,
} from './classes';
import { OutcomeSelectionModalComponent } from './outcome-selection-modal/outcome-selection-modal.component';
import { GlobalVariablesSA } from './classes/global-variables-sa';

/**
 * value: the central parameter which can be modified in the sensitivity analysis; no global or permanent parameter
 *
 * initValue: the initial value of a value the results from the different steps of the decision tool; constant value
 *
 * precision: the precision of a certain parameter; constant value
 *
 * precisionInterval: upper and lower limit of the precision; constant value
 */

type ExtendedOutcomeSA = {
  outcomeSA: OutcomeSA;
  objectiveIndex: number;
  alternativeIndex: number;
};

@Component({
  selector: 'dt-sensitivity-analysis',
  templateUrl: './sensitivity-analysis.component.html',
  styleUrls: ['./sensitivity-analysis.component.scss'],
})
@PersistentSettingParent('SensitivityAnalysis')
export class SensitivityAnalysisComponent implements Navigation, HelpMenuProvider {
  @Input()
  showTitle = true;

  objectives: Objective[];
  influenceFactors: InfluenceFactor[];
  userDefinedInfluenceFactors: SimpleUserDefinedInfluenceFactor[];
  outcomes: Outcome[][];
  weights: ObjectiveWeight[];

  // split objectives into numerical and verbal (from decision_data)
  objectivesIndicator: Objective[];

  // results and parameters for chart WITHOUT sensitivity analysis
  utilityValues: number[]; // utility values from the unchanged data: e.g. the third value belongs to the alternative at the third index

  // names
  alternativeNames: string[];
  selectedAlternativeNames: string[];

  // results and parameters for chart WITH sensitivity analysis must be of size of the number of ALL alternatives
  weightedUtilityMatrix: number[][];
  alternativeUtilities: number[];

  // tracks whether a division of zero happens during calculations
  hasDivisionByZero: boolean;
  hasOutOfBoundsIndicatorValues: boolean;

  // selected alternatives for sensitivity analysis
  private _selectedAlternatives: number[];

  @PersistentSetting('project') displayMode: 'simple' | 'detailed' = 'simple';

  get selectedAlternatives() {
    return this._selectedAlternatives;
  }

  set selectedAlternatives(newSelectedAlternatives: number[]) {
    // This is two way binded to chart-ranking which fires the change emitter when we set this to the sorted array instance.
    // So if we don't actually need to sort it we just use the same array to break the otherwise recursive chain.
    const sortedNewSelectedAlternatives = sortBy(newSelectedAlternatives);
    if (!isEqual(sortedNewSelectedAlternatives, newSelectedAlternatives)) {
      // utility values are referring to the selected alternatives in *sorted* order.
      this._selectedAlternatives = sortBy(newSelectedAlternatives);
    } else {
      this._selectedAlternatives = newSelectedAlternatives;
    }
  }

  // arrays for storing all values and precisions for the sensitivity analysis
  outcomesSA: OutcomeSA[][] = [];
  weightsOfIndicatorsSA: WeightsOfIndicatorsSA[] = [];
  weightsOfObjectivesSA: WeightsOfObjectivesSA;
  utilityFunctionsNumericalSA: UtilityFunctionNumericalSA[] = [];
  utilityFunctionsVerbalSA: UtilityFunctionVerbalSA[] = []; // nested
  influenceFactorsSA: InfluenceFactorSA[] = []; // nested
  globalVariablesSA: GlobalVariablesSA;

  flattenedTransposedOutcomes: ExtendedOutcomeSA[] = [];

  @PersistentSetting('project')
  showOutcomes = true;
  @PersistentSetting('project')
  showObjectiveWeights = true;

  @PersistentSetting('project')
  selectedOutcomes: boolean[][] = [[]];
  @PersistentSetting('project')
  selectedUtilityFunctions: number[];
  @PersistentSetting('project')
  selectedInfluenceFactors: (number | string)[];
  @PersistentSetting('project')
  selectedIndicatorWeights: number[];

  @PersistentSetting('project')
  showGlobalVariables = true;

  // steps for the respective range-input components
  stepWeightOfObjectives = 0.1;
  stepUtilityFunctionsNumerical = 0.1;
  stepUtilityFunctionsVerbal = 1;
  stepInfluenceFactors = 1;

  objectiveWeightsUpperBound: { initialValue: number; currentValue: number }[] = [];

  readonly weightLowerBound = 0; // always 0
  readonly absoluteWeightUpperBound = 1000; // limited to 1000, could be lower
  readonly minimalNumberOfSliderValues = 100;
  readonly machinePrecison = 0.000000000001;

  get selectedParameters(): any {
    const selectedOptions = [];
    if (this.showObjectiveWeights) {
      selectedOptions.push('objective-weights');
    }
    if (this.showOutcomes) {
      selectedOptions.push('outcomes');
    }
    if (this.showGlobalVariables) {
      selectedOptions.push('globalVariables');
    }
    return {
      [SelectComponent.ROOT_GROUP]: selectedOptions,
      'influence-factors': this.selectedInfluenceFactors,
      objectives: this.selectedUtilityFunctions,
      'indicator-weights': this.selectedIndicatorWeights,
    };
  }

  set selectedParameters({
    [SelectComponent.ROOT_GROUP]: selectedOptions,
    'influence-factors': selectedInfluenceFactors,
    objectives: selectedUtilityFunctions,
    'indicator-weights': selectedIndicatorWeights,
  }: any) {
    this.showObjectiveWeights = selectedOptions.includes('objective-weights');
    this.showOutcomes = selectedOptions.includes('outcomes');
    this.showGlobalVariables = selectedOptions.includes('globalVariables');
    this.selectedInfluenceFactors = selectedInfluenceFactors;
    this.selectedUtilityFunctions = selectedUtilityFunctions;
    this.selectedIndicatorWeights = selectedIndicatorWeights;
    this.resetHiddenBlocks();
    this.intCalculateAnalysisUtilityValues();
  }

  // Sticky Chart
  chartStuck = false;

  throttledCalculation: () => void;

  navLine = new NavLine({
    left: [navLineElement().back('/results').build()],
    middle: [
      navLineElement()
        .label($localize`Parameter zurücksetzen`)
        .icon('settings_backup_restore')
        .onClick(() => this.resetSensitivityValues())
        .build(),
    ],
  });

  helpMenu = getHelpMenu(this.languageService.steps, 'sensitivity-analysis');

  @ViewChild('helpContent') helpContent: TemplateRef<any>;

  constructor(
    private matDialog: MatDialog,
    protected decisionData: DecisionData,
    private languageService: LanguageService,
  ) {
    this.objectives = this.decisionData.objectives;
    this.influenceFactors = [...this.decisionData.influenceFactors, ...Object.values(PREDEFINED_INFLUENCE_FACTORS)];
    this.userDefinedInfluenceFactors = this.decisionData.getUserDefinedInfluenceFactors();
    this.outcomes = this.decisionData.outcomes;
    this.weights = this.decisionData.weights.getWeights();

    // init of first page before sensitivity analysis
    this.utilityValues = this.decisionData.getAlternativeUtilities().map(v => v.round(4));

    // split objectives into numerical, verbal and indicator
    this.objectivesIndicator = this.decisionData.objectives.filter(objective => objective.isIndicator);

    this.selectedAlternatives = sortBy(range(this.decisionData.alternatives.length));

    this.alternativeNames = this.decisionData.alternatives.map(a => a.name);

    this.initSensitivityAnalysisData();
    // initialize parameter selection (before Persistent Setting kicks in)
    this.verifyParameterSelection();

    // transpose outcomes and add them to outcomeBlocks
    zip(...this.outcomes).forEach((outcomeRow, objectiveIndex) => {
      outcomeRow.forEach((outcome, alternativeIndex) => {
        this.flattenedTransposedOutcomes.push({
          outcomeSA: this.outcomesSA[alternativeIndex][objectiveIndex],
          objectiveIndex,
          alternativeIndex,
        });
      });
    });

    this.calculateAnalysisUtilityValues();

    /* Initialize maximum weight for each objective weights block. */
    this.objectives.forEach(objective => {
      if (objective.isIndicator) {
        const maxWeight = Math.max(...objective.indicatorData.indicators.map(ind => ind.coefficient));
        this.objectiveWeightsUpperBound.push({ initialValue: round(maxWeight * 2, 12), currentValue: round(maxWeight * 2, 12) });
      }
    });
  }

  verifyParameterSelection() {
    /* Parameterauswahl im Wirkungsmodell button. */
    if (
      this.selectedOutcomes == null ||
      this.selectedOutcomes.length !== this.decisionData.alternatives.length ||
      this.selectedOutcomes[0]?.length !== this.objectives.length
    ) {
      this.selectedOutcomes = [...Array(this.decisionData.alternatives.length)].map(() => Array(this.objectives.length).fill(false));
    }
    /* Parameter dropdown. */
    if (this.selectedUtilityFunctions == null) {
      this.selectedUtilityFunctions = range(this.objectives.length);
    } else {
      this.selectedUtilityFunctions = this.selectedUtilityFunctions.filter(num => num < this.objectives.length);
    }

    if (this.selectedInfluenceFactors == null) {
      this.selectedInfluenceFactors = this.userDefinedInfluenceFactors.map(iF => iF.id);
    } else {
      this.selectedInfluenceFactors = this.selectedInfluenceFactors.filter(num =>
        this.userDefinedInfluenceFactors.some(iF => iF.id === num),
      );
    }

    if (this.selectedIndicatorWeights == null) {
      this.selectedIndicatorWeights = range(this.objectivesIndicator.length);
    } else {
      this.selectedIndicatorWeights = this.selectedIndicatorWeights.filter(num => num < this.objectivesIndicator.length);
    }

    if (this.showGlobalVariables && this.globalVariablesSA.activeVariables.length === 0) {
      this.showGlobalVariables = false;
    }
  }

  /**
   * calculate new utility values from the sensitivity analysis
   */
  calculateAnalysisUtilityValues() {
    if (!this.throttledCalculation) {
      this.throttledCalculation = throttle(() => this.intCalculateAnalysisUtilityValues(), 20);
    }

    this.throttledCalculation();
  }

  intCalculateAnalysisUtilityValues() {
    // extract data from {weightOfObjectives, utilityFunctionsNumerical, utilityFunctionsVerbal, influenceFactors}
    // for the calculation
    const iteratorNum = this.utilityFunctionsNumericalSA.values();
    const iteratorVer = this.utilityFunctionsVerbalSA.values();
    let indicatorObjectiveIndex = -1;

    this.hasOutOfBoundsIndicatorValues = false;

    const objectiveUf = this.objectives.map(objective => {
      switch (objective.objectiveType) {
        case ObjectiveType.Numerical: {
          const c = iteratorNum.next().value.value;
          return getNumericalUtilityFunction(c, objective.numericalData.from, objective.numericalData.to);
        }
        case ObjectiveType.Indicator: {
          indicatorObjectiveIndex++;
          const c = iteratorNum.next().value.value;

          const aggregationSetting: AggregationSetting = objective.indicatorData.useCustomAggregation
            ? { formula: objective.indicatorData.customAggregationFormula, globalVariables: this.globalVariablesSA.variables }
            : { best: objective.indicatorData.defaultAggregationBest, worst: objective.indicatorData.defaultAggregationWorst };

          const aggregationFunction = getIndicatorAggregationFunction(
            objective.indicatorData.indicators.map((indicator, indicatorIndex) => ({
              coefficient: this.selectedIndicatorWeights?.includes(indicatorObjectiveIndex)
                ? this.weightsOfIndicatorsSA[indicatorObjectiveIndex].indicatorCoefficients[indicatorIndex]
                : indicator.coefficient,
              min: indicator.min,
              max: indicator.max,
              verbalIndicatorCategories: [], // We treat verbalized indicators as normal indicators for the purposes of this analysis
            })),
            aggregationSetting,
            true,
          );

          const automaticLimits = objective.indicatorData.useCustomAggregation && objective.indicatorData.automaticCustomAggregationLimits;
          const worst = automaticLimits
            ? aggregationFunction(objective.indicatorData.indicators.map(indicator => [indicator.min]))
            : objective.indicatorData.defaultAggregationWorst;
          const best = automaticLimits
            ? aggregationFunction(objective.indicatorData.indicators.map(indicator => [indicator.max]))
            : objective.indicatorData.defaultAggregationBest;

          const valueInterval = new Interval(worst, best).ordered();

          const clampedAggregationFunction: AggregationFunction = (values: number[][]) => {
            const aggregate = aggregationFunction(values);

            if (!valueInterval.includes(aggregate)) {
              this.hasOutOfBoundsIndicatorValues = true;
              return valueInterval.clampTo(aggregate);
            }

            return aggregate;
          };

          return getIndicatorUtilityFunction(c, clampedAggregationFunction, worst, best, true);
        }
        case ObjectiveType.Verbal: {
          const utilities = iteratorVer.next().value.values;
          return getVerbalUtilityFunction(utilities);
        }
        default:
          assertUnreachable(objective.objectiveType);
      }
    });

    const outcomeUf = this.outcomes.map(ocsForAlternative =>
      ocsForAlternative.map((outcome, objectiveIndex) => {
        if (outcome.influenceFactor instanceof SimpleUserDefinedInfluenceFactor) {
          const probabilities = this.influenceFactorsSA[outcome.influenceFactor.id].values.map(value => value / 100);
          return getOutcomeUtilityFunction(objectiveUf[objectiveIndex], (callback, outcomeValues) =>
            iterateUserDefinedScenarios(outcomeValues, probabilities, callback),
          );
        } else if (outcome.influenceFactor instanceof CompositeUserDefinedInfluenceFactor) {
          const clonedIF = this.transformCombinedIF(outcome.influenceFactor);
          return getOutcomeUtilityFunction(objectiveUf[objectiveIndex], (callback, outcomeValues) =>
            clonedIF.iterateScenarios(outcomeValues, callback, true),
          );
        } else if (outcome.influenceFactor instanceof PredefinedInfluenceFactor) {
          return getOutcomeUtilityFunction(objectiveUf[objectiveIndex], (callback, outcomeValues) =>
            outcome.influenceFactor.iterateScenarios(outcomeValues, callback, true),
          );
        } else {
          return getOutcomeUtilityFunction(objectiveUf[objectiveIndex]);
        }
      }),
    );

    this.weightedUtilityMatrix = this.decisionData.alternatives.map((_, alternativeIndex) =>
      this.outcomes[alternativeIndex].map((outcome, objectiveIndex) => {
        const outcomeValue =
          this.selectedOutcomes[alternativeIndex]?.[objectiveIndex] == null
            ? outcome.values
            : this.outcomesSA[alternativeIndex][objectiveIndex].values.map(valForState => valForState.map(valForInd => [valForInd]));
        return outcomeUf[alternativeIndex][objectiveIndex](outcomeValue).round(4);
      }),
    );
    applyWeightsToUtilityMatrix(this.weightedUtilityMatrix, this.weightsOfObjectivesSA.getWeights());

    this.alternativeUtilities = getAlternativeUtilities(this.weightedUtilityMatrix);
    this.hasDivisionByZero = this.alternativeUtilities.includes(NaN);

    this.selectedAlternativeNames = this.alternativeNames.filter((_, i) => this.selectedAlternatives.includes(i));
  }

  private transformCombinedIF(iF: CompositeUserDefinedInfluenceFactor): CompositeUserDefinedInfluenceFactor {
    const baseFactors = iF.baseFactors.map(baseFactor => {
      if (baseFactor instanceof CompositeUserDefinedInfluenceFactor) {
        return this.transformCombinedIF(baseFactor);
      } else {
        const probabilities = this.influenceFactorsSA[baseFactor.id].values;
        return new SimpleUserDefinedInfluenceFactor(
          '',
          probabilities.map(probability => ({ name: '', probability })),
        );
      }
    }) as [UserDefinedInfluenceFactor, UserDefinedInfluenceFactor];

    return new CompositeUserDefinedInfluenceFactor(-1, '', baseFactors, iF.crossImpacts, '');
  }

  initSensitivityAnalysisData() {
    this.globalVariablesSA = new GlobalVariablesSA(this.decisionData.globalVariables, this.decisionData.objectives);
    this.weightsOfObjectivesSA = new WeightsOfObjectivesSA(this.weights);
    this.objectives.forEach(objective => {
      if (objective.isNumerical) {
        this.utilityFunctionsNumericalSA.push(new UtilityFunctionNumericalSA(objective.numericalData.utilityfunction));
      } else if (objective.isVerbal) {
        this.utilityFunctionsVerbalSA.push(new UtilityFunctionVerbalSA(objective.verbalData));
      } else if (objective.isIndicator) {
        this.utilityFunctionsNumericalSA.push(new UtilityFunctionNumericalSA(objective.indicatorData.utilityfunction));
      }
    });
    this.influenceFactors.forEach(influenceFactor => this.influenceFactorsSA.push(new InfluenceFactorSA(influenceFactor)));
    this.outcomes.forEach((outcomeRow, i) => {
      this.outcomesSA[i] = [];
      outcomeRow.forEach((outcome, objectiveIndex) => {
        const ufIdx = this.influenceFactors.indexOf(outcome.influenceFactor);

        const o = this.objectives[objectiveIndex];
        const values = outcome.values.map(valsForState =>
          valsForState.map((valsForIndicator, indicatorIndex) => {
            // Strip all verbalized indicators. We treat them as normal indicators in this analysis.
            if (o.isIndicator && o.indicatorData.indicators[indicatorIndex].isVerbalized) {
              return calculateIndicatorValue(valsForIndicator, o.indicatorData.indicators[indicatorIndex]);
            } else {
              return valsForIndicator[0];
            }
          }),
        );

        this.outcomesSA[i].push(new OutcomeSA(values, ufIdx >= 0 ? this.influenceFactorsSA[ufIdx] : null));
      });
    });
    this.objectivesIndicator.forEach(objective => {
      this.weightsOfIndicatorsSA.push(new WeightsOfIndicatorsSA(objective.indicatorData));
    });
  }

  openOutcomeSelectionModal() {
    this.matDialog
      .open(OutcomeSelectionModalComponent, { data: this.selectedOutcomes })
      .afterClosed()
      .subscribe(() => {
        this.resetHiddenOutcomes();
        this.intCalculateAnalysisUtilityValues();
      });
  }

  getOutcomesForObjective(objective: Objective) {
    const objectiveIdx = this.objectives.indexOf(objective);
    if (objectiveIdx > -1) {
      return this.outcomes.map(outcomesRow => outcomesRow[objectiveIdx]);
    }
    return [];
  }

  getOutcomesForInfluenceFactor(influenceFactor: InfluenceFactor) {
    const ocs = [];
    for (const outcomeRow of this.outcomes) {
      for (const outcome of outcomeRow) {
        if (outcome.influenceFactor === influenceFactor) {
          ocs.push(outcome);
        }
      }
    }
    return ocs;
  }

  change() {
    this.calculateAnalysisUtilityValues();
  }

  // TODO: include in class
  utilityFunctionsNumericalSAValuesChanged(): boolean {
    return this.utilityFunctionsNumericalSA.every(el => !el.valueChanged());
  }

  /**
   * reset array weightsOfObjectivesSA to its initial values
   */
  resetWeightsOfObjectivesSA() {
    this.weightsOfObjectivesSA.resetValues();
    this.change();
  }

  /**
   * reset array utilityFunctionsNumericalSA to its initial values
   */
  resetUtilityFunctionsNumericalSA() {
    this.utilityFunctionsNumericalSA.forEach(el => el.resetValue());
    this.change();
  }

  /**
   * reset array utilityFunctionsVerbalSA to its initial values
   */
  resetUtilityFunctionsVerbalSA() {
    this.utilityFunctionsVerbalSA.forEach(el => el.resetValues());
    this.change();
  }

  /**
   * reset array influenceFactorsSA to its initial values
   */
  resetInfluenceFactorsSA() {
    this.influenceFactorsSA.forEach(el => el.resetValues());
    this.change();
  }

  /**
   * reset array outcomesSA to its initial values
   */
  resetOutcomesSA() {
    this.outcomesSA.forEach(outcomesSARow => {
      outcomesSARow.forEach(outcomeSA => {
        outcomeSA.resetValue();
      });
    });
    this.change();
  }

  /**
   * reset array objectivesSA to its initial values
   */
  resetWeightsOfIndicatorsSA() {
    this.weightsOfIndicatorsSA.forEach((weightOfIndicatorSA, i) => {
      weightOfIndicatorSA.resetValue();
      this.objectiveWeightsUpperBound[i].currentValue = this.objectiveWeightsUpperBound[i].initialValue;
    });
    this.change();
  }

  /**
   * reset the sensitivity values to their initial values
   */
  resetSensitivityValues() {
    this.resetWeightsOfObjectivesSA();
    this.resetUtilityFunctionsNumericalSA();
    this.resetUtilityFunctionsVerbalSA();
    this.resetInfluenceFactorsSA();
    this.resetOutcomesSA();
    this.resetWeightsOfIndicatorsSA();
    this.globalVariablesSA.resetValue();
  }

  /**
   * Make the Chart have a shadow when sticky
   */
  chartStick() {
    this.chartStuck = true;
  }

  chartUnStick() {
    this.chartStuck = false;
  }
  calculateSliderStep(from: number, to: number) {
    const difference = Math.abs(to - from);
    const exactStep = this.fixRoundingError(difference / this.minimalNumberOfSliderValues);
    const maxBoundaryDecimals = Math.max(this.countDecimals(from), this.countDecimals(to));
    // limit step at 10^-10
    return 10 ** Math.min(-Math.ceil(-Math.log10(exactStep)), -maxBoundaryDecimals);
  }

  private countDecimals(value: number) {
    return value % 1 ? value.toString().split('.')[1].length : 0;
  }

  scrollHorizontally(direction: number, contentWrapper: HTMLElement) {
    const horizontallyScrollableElements = contentWrapper.querySelectorAll('.control-field-slider-grid');
    horizontallyScrollableElements.forEach(element => {
      /* Scroll horizontally with (sliderWidth + sliderMargin) px. */
      element.scrollLeft += ((element.firstChild as HTMLElement)?.offsetWidth + 20 || 0) * direction;
    });
  }

  changeWeightUB(multiplier: number, index: number) {
    this.objectiveWeightsUpperBound[index].currentValue = this.fixRoundingError(
      Math.max(Math.min(this.objectiveWeightsUpperBound[index].currentValue * multiplier, this.absoluteWeightUpperBound), 0.25),
    );
  }

  getScaleMin(scaleData: NumericalObjectiveData | Indicator) {
    if (scaleData instanceof NumericalObjectiveData) {
      return Math.min(scaleData.from, scaleData.to);
    } else {
      return Math.min(scaleData.min, scaleData.max);
    }
  }

  getScaleMax(scaleData: NumericalObjectiveData | Indicator) {
    if (scaleData instanceof NumericalObjectiveData) {
      return Math.max(scaleData.from, scaleData.to);
    } else {
      return Math.max(scaleData.min, scaleData.max);
    }
  }

  isAnySelectedUtilityFunctionNumerical() {
    return this.selectedUtilityFunctions.some(
      objectiveIdx => this.objectives[objectiveIdx].isNumerical || this.objectives[objectiveIdx].isIndicator,
    );
  }

  getObjectiveIndex(selectedNumericalIndex: number, objectiveType: 'numerical' | 'verbal') {
    let currentObjectiveIndex = -1;
    for (let i = 0; i < this.objectives.length; i++) {
      if (
        ((this.objectives[i].isNumerical || this.objectives[i].isIndicator) && objectiveType === 'numerical') ||
        (this.objectives[i].isVerbal && objectiveType === 'verbal')
      ) {
        currentObjectiveIndex++;
      }
      if (currentObjectiveIndex === selectedNumericalIndex) {
        if (this.selectedUtilityFunctions.includes(i)) {
          return i;
        } else {
          return -1;
        }
      }
    }
    return -1;
  }

  resetHiddenBlocks() {
    // reset hidden weight objectives
    if (!this.showObjectiveWeights) {
      this.resetWeightsOfObjectivesSA();
    }

    // reset hidden utility function (numerical & indicator)
    for (let i = 0; i < this.utilityFunctionsNumericalSA.length; i++) {
      if (this.getObjectiveIndex(i, 'numerical') === -1) {
        this.utilityFunctionsNumericalSA[i].resetValue();
      }
    }

    // reset hidden utility function (verbal)
    for (let i = 0; i < this.utilityFunctionsVerbalSA.length; i++) {
      if (this.getObjectiveIndex(i, 'verbal') === -1) {
        this.utilityFunctionsVerbalSA[i].resetValues();
      }
    }

    // reset hidden influence factors
    for (let i = 0; i < this.decisionData.influenceFactors.length; i++) {
      if (!this.selectedInfluenceFactors.includes(i)) {
        this.influenceFactorsSA[i].resetValues();
      }
    }

    // reset hidden indicator weights
    for (let i = 0; i < this.weightsOfIndicatorsSA.length; i++) {
      if (!this.selectedIndicatorWeights.includes(i)) {
        this.weightsOfIndicatorsSA[i].resetValue();
      }
    }

    // reset ALL outcomes
    if (!this.showOutcomes) {
      for (let i = 0; i < this.flattenedTransposedOutcomes.length; i++) {
        this.flattenedTransposedOutcomes[i].outcomeSA.resetValue();
      }
    }
  }

  resetHiddenOutcomes() {
    for (let i = 0; i < this.selectedOutcomes.length; i++) {
      for (let j = 0; j < this.selectedOutcomes[i].length; j++) {
        if (!this.selectedOutcomes[i][j]) {
          this.outcomesSA[i][j].resetValue();
        }
      }
    }
  }

  onValueChange(newValue: number, o: ExtendedOutcomeSA, stateIdx: number, indicatorIdx?: number) {
    const objective = this.objectives[o.objectiveIndex];
    let min, max;
    if (objective.isNumerical) {
      min = objective.numericalData.from;
      max = objective.numericalData.to;
    } else if (objective.isVerbal) {
      min = 1;
      max = objective.verbalData.options.length;
    } else if (objective.isIndicator) {
      min = objective.indicatorData.indicators[indicatorIdx].min;
      max = objective.indicatorData.indicators[indicatorIdx].max;
    }

    if (indicatorIdx != null) {
      o.outcomeSA.values[stateIdx][indicatorIdx] = newValue; // update value (special for indicator scales)
    } else {
      o.outcomeSA.values[stateIdx][0] = newValue; // update value
    }

    // update neighbouring values
    if (this.outcomes[o.alternativeIndex][o.objectiveIndex].influenceFactor instanceof PredefinedInfluenceFactor) {
      // update values on the "left"
      for (let i = stateIdx - 1; i >= 0; i--) {
        if (indicatorIdx != null) {
          if (Math.sign(o.outcomeSA.values[i][indicatorIdx] - o.outcomeSA.values[i + 1][indicatorIdx]) === Math.sign(max - min)) {
            o.outcomeSA.values[i][indicatorIdx] = o.outcomeSA.values[i + 1][indicatorIdx]; // move neighboring slider
          }
        } else {
          if (Math.sign(o.outcomeSA.values[i][0] - o.outcomeSA.values[i + 1][0]) === Math.sign(max - min)) {
            o.outcomeSA.values[i][0] = o.outcomeSA.values[i + 1][0]; // move neighboring slider
          }
        }
      }

      // update values on the "right"
      for (let i = stateIdx + 1; i < o.outcomeSA.values.length; i++) {
        if (indicatorIdx != null) {
          if (Math.sign(o.outcomeSA.values[i][indicatorIdx] - o.outcomeSA.values[i - 1][indicatorIdx]) === Math.sign(min - max)) {
            o.outcomeSA.values[i][indicatorIdx] = o.outcomeSA.values[i - 1][indicatorIdx]; // move neighboring slider
          }
        } else {
          if (Math.sign(o.outcomeSA.values[i][0] - o.outcomeSA.values[i - 1][0]) === Math.sign(min - max)) {
            o.outcomeSA.values[i][0] = o.outcomeSA.values[i - 1][0]; // move neighboring slider
          }
        }
      }
    }
    this.change();
  }

  /* Fixes machine error in calculations. */
  private fixRoundingError(n: number) {
    return Math.round(n * (1 / this.machinePrecison)) / (1 / this.machinePrecison);
  }
}
