import { Interval } from '@entscheidungsnavi/tools';

// TODO: replace/extend example (hard to understand) with proper definition

/**
 * The relative precision depends on the current value. If the value is 10 in a value range of [0, 100] a precision
 * of 50% describes an interval from 5 to 15. If the value is 90 a precision of 50% means form 85 to 95.
 *
 * @param value - ???
 * @param precision - ???
 *
 * @returns - the relative precision
 */
export function relativePrecision(value: number, precision: number): number {
  return value > 0 ? precision * Math.min(1 - value / 100, value / 100) : 0;
}

/**
 * Creates an interval [{@link value} - {@link precision}, {@link value} + {@link precision}]
 * and clamps it with respect to {@link valueRangeLimits}.
 *
 * @example
 * ```
 * getPrecisionInterval({ min: -1, max: 1}, 0, 1);   // {min: -1, max: 1}
 * getPrecisionInterval({ min: -1, max: 1}, 0, 2);   // {min: -1, max: 1}
 * getPrecisionInterval({ min: -1, max: 1}, 0, 0.5); // {min: -0.5, max: 0.5}
 * getPrecisionInterval({ min: -1, max: 1}, 1, 0.5); // {min: 0.5, max: 1}
 * ```
 *
 * @returns the upper and lower limit of the clamped precision interval
 */
export function getPrecisionInterval(valueRangeLimits: Interval, value: number, precision: number): Interval {
  return new Interval(
    Math.min(valueRangeLimits.end, Math.max(valueRangeLimits.start, value - precision)),
    Math.min(valueRangeLimits.end, Math.max(valueRangeLimits.start, value + precision)),
  );
}

export function getRelativePrecisionInterval(valueRangeLimits: Interval, value: number, precision: number): Interval {
  return new Interval(
    Math.min(valueRangeLimits.end, Math.max(valueRangeLimits.start, value - relativePrecision(value, precision))),
    Math.min(valueRangeLimits.end, Math.max(valueRangeLimits.start, value + relativePrecision(value, precision))),
  );
}
