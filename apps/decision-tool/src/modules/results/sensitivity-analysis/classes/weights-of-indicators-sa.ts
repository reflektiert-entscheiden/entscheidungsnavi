import { IndicatorObjectiveData } from '@entscheidungsnavi/decision-data';

export class WeightsOfIndicatorsSA {
  // SA = Sensitivity Analysis
  public indicatorCoefficients: number[] = [];
  public initIndicatorCoefficients: number[] = []; // constant value in sensitivity analysis

  constructor(indicatorData: IndicatorObjectiveData) {
    indicatorData.indicators.forEach(indicator => {
      this.indicatorCoefficients.push(indicator.coefficient);
      this.initIndicatorCoefficients.push(indicator.coefficient);
    });
  }

  public resetValue() {
    this.indicatorCoefficients = this.initIndicatorCoefficients.slice();
  }

  public valueChanged(): boolean {
    return !this.indicatorCoefficients.every((value, index) => value === this.initIndicatorCoefficients[index]);
  }
}
