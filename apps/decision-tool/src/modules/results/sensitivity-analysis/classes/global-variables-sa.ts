import { isEqual, uniq } from 'lodash';
import { Interval } from '@entscheidungsnavi/tools';
import { GlobalVariables, GlobalVariablesMap, Objective, parseFormula } from '@entscheidungsnavi/decision-data';
import { SymbolNode } from 'mathjs';

export class GlobalVariablesSA {
  variables: GlobalVariablesMap;
  visibleLimit: Interval;

  readonly initVariables: GlobalVariablesMap;
  // Array of all variable names that are in use
  readonly activeVariables: string[];
  // Maps variables names to the names of objectives which use them and have automatic limits enabled
  readonly nonCompliantObjectivesForVariable = new Map<string, string[]>();
  // The limit for manually set values
  readonly settingLimit = new Interval(-Infinity, Infinity);

  constructor(globalVariables: GlobalVariables, objectives: Objective[]) {
    this.initVariables = new Map(globalVariables.map);
    this.resetValue();

    const min = Math.min(...this.initVariables.values()),
      max = Math.max(...this.initVariables.values());

    this.visibleLimit = min === max ? new Interval(min, min + 1) : new Interval(min < 0 ? min * 2 : 0, max > 0 ? max * 2 : 0);

    // All custom aggregation objectives with their corresponding variable names
    const customAggregationObjectives = objectives
      .filter(objective => objective.isIndicator && objective.indicatorData.useCustomAggregation)
      .map(customFormulaObjective => {
        const formulaNode = parseFormula(customFormulaObjective.indicatorData.customAggregationFormula);
        const variables = formulaNode
          .filter(node => node.type === 'SymbolNode' && this.variables.has((node as SymbolNode).name))
          .map(node => (node as SymbolNode).name);

        return [customFormulaObjective, variables] as const;
      });

    this.activeVariables = uniq(customAggregationObjectives.flatMap(([, variables]) => variables));

    customAggregationObjectives.forEach(([objective, variables]) => {
      if (!objective.indicatorData.automaticCustomAggregationLimits) return;

      variables.forEach(variable => {
        if (!this.nonCompliantObjectivesForVariable.has(variable)) {
          this.nonCompliantObjectivesForVariable.set(variable, []);
        }

        this.nonCompliantObjectivesForVariable.get(variable).push(objective.name);
      });
    });
  }

  resetValue() {
    this.variables = new Map(this.initVariables);
  }

  setValue(name: string, value: number) {
    this.variables.set(name, value);

    // Update the value limits
    const min = Math.min(...this.variables.values()),
      max = Math.max(...this.variables.values());

    this.visibleLimit = new Interval(Math.min(this.visibleLimit.start, min), Math.max(this.visibleLimit.end, max));
  }

  moveLowerBound() {
    this.visibleLimit = new Interval(this.visibleLimit.start - this.visibleLimit.length / 2, this.visibleLimit.end);
  }

  moveUpperBound() {
    this.visibleLimit = new Interval(this.visibleLimit.start, this.visibleLimit.end + this.visibleLimit.length / 2);
  }

  valuesChanged() {
    return !isEqual(this.variables, this.initVariables);
  }
}
