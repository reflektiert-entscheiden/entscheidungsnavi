import { UtilityFunction } from '@entscheidungsnavi/decision-data';
import { Interval } from '@entscheidungsnavi/tools';
import * as calculation from './../calculation/sensitivity-analysis';

export class UtilityFunctionNumericalSA {
  // SA = Sensitivity Analysis
  public valueRangeLimits = new Interval(-25, 25);
  public value: number;
  public initValue: number; // constant value in sensitivity analysis
  public precision: number; // constant value in sensitivity analysis
  public precisionInterval: Interval; // constant value in sensitivity analysis

  constructor(utilityFunction: UtilityFunction) {
    this.value = utilityFunction.c;
    this.initValue = utilityFunction.c;
    this.precision = utilityFunction.precision;
    this.precisionInterval = calculation.getPrecisionInterval(this.valueRangeLimits, utilityFunction.c, utilityFunction.precision);
  }

  public resetValue() {
    this.value = this.initValue;
  }

  public valueChanged(): boolean {
    return this.value !== this.initValue;
  }
}
