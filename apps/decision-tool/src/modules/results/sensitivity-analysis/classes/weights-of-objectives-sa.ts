import { throttle } from 'lodash';
import { ObjectiveWeight } from '@entscheidungsnavi/decision-data';
import { normalizeDiscrete, normalizeDiscretePrioritized } from '@entscheidungsnavi/tools';
import { WeightOfObjectiveSA } from './weight-of-objective-sa';

export class WeightsOfObjectivesSA {
  // SA = Sensitivity Analysis
  public weightsOfObjectives: WeightOfObjectiveSA[];

  private throttledNormalize = throttle((changedIdx: number) => this.normalize(changedIdx), 20);

  constructor(weights: ObjectiveWeight[]) {
    const sum = weights.reduce((prev, weight) => prev + weight.value, 0);
    const precisions = weights.map(weight => (weight.precision * 100) / sum);
    const values = normalizeDiscrete(
      weights.map(weight => weight.value),
      100,
      0.1,
    );
    this.weightsOfObjectives = values.map((value, i) => new WeightOfObjectiveSA(value, precisions[i]));
  }

  public resetValues() {
    this.weightsOfObjectives.forEach(el => el.resetValue());
    // this.updateShownValue();
  }

  public getWeights(): number[] {
    return this.weightsOfObjectives.map(weightOfObjective => weightOfObjective.value);
  }

  public valuesChanged(): boolean {
    return this.weightsOfObjectives.every(el => !el.valueChanged());
  }

  setValue(value: number, idx: number) {
    this.weightsOfObjectives[idx].value = value;
    this.weightsOfObjectives[idx].changedValue = value;

    this.throttledNormalize(idx);
  }

  private normalize(changedIdx: number) {
    const baseValues = this.weightsOfObjectives.map((val, _i) => (val.changedValue != null ? val.changedValue : val.initValue));
    const priorities = this.weightsOfObjectives.map((val, i) => (i === changedIdx ? 3 : val.changedValue != null ? 2 : 1));
    normalizeDiscretePrioritized(baseValues, priorities, 100, 0.1).forEach((val, i) => (this.weightsOfObjectives[i].value = val));
  }
}
