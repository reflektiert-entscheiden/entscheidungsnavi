import { cloneDeep } from 'lodash';
import { InfluenceFactorSA } from './influence-factor-sa';

export class OutcomeSA {
  // We purposefully DO NOT look at indicator stages here. These can not be modified in the sensitivity analysis.
  public values: number[][]; // [influenceFactorStateIndex][indicatorIndex]
  public initValues: number[][]; // constant value in sensitivity analysis

  public influenceFactor: InfluenceFactorSA;

  constructor(values: number[][], influenceFactor?: InfluenceFactorSA) {
    this.values = cloneDeep(values);
    this.initValues = cloneDeep(values);
    this.influenceFactor = influenceFactor;
  }

  public resetValue() {
    this.values = cloneDeep(this.initValues);
  }

  public valueChanged(): boolean {
    return !this.values.every((objectiveInput, i) => {
      if (Array.isArray(objectiveInput)) {
        return objectiveInput.every((numberValue, j) => numberValue === this.initValues[i][j]);
      }
      return objectiveInput === this.initValues[i];
    });
  }
}
