import { VerbalObjectiveData } from '@entscheidungsnavi/decision-data';
import { Interval } from '@entscheidungsnavi/tools';
import * as calculation from './../calculation/sensitivity-analysis';

export class UtilityFunctionVerbalSA {
  // SA = Sensitivity Analysis
  public valueRangeLimits = new Interval(0, 100);
  public values: number[];
  public initValues: number[]; // constant value in sensitivity analysis
  public precision: number; // constant value in sensitivity analysis
  public precisionIntervals: Interval[]; // constant value in sensitivity analysis
  public valueLimits: Interval[];

  constructor(objectiveVerbal: VerbalObjectiveData) {
    this.values = objectiveVerbal.utilities.map(utility => utility);
    this.initValues = objectiveVerbal.utilities.map(utility => utility);
    this.precision = objectiveVerbal.precision;
    this.precisionIntervals = objectiveVerbal.utilities.map(value =>
      calculation.getRelativePrecisionInterval(this.valueRangeLimits, value, objectiveVerbal.precision),
    );
    this.valueLimits = this.setValueLimits(objectiveVerbal.utilities.map(utility => utility));
  }

  public resetValues() {
    this.values = this.initValues.slice();
  }

  public valuesChanged(): boolean {
    return this.values.every((el, idx) => el === this.initValues[idx]);
  }

  // a value with smaller index must be smaller than a the value with the next higher index
  public valuesChange(currentIdx: number) {
    for (let i = currentIdx - 1; i >= 0; i--) {
      if (this.values[i] >= this.values[i + 1]) {
        this.values[i] = this.values[i + 1] - 1;
      }
    }

    for (let i = currentIdx + 1; i < this.values.length; i++) {
      if (this.values[i] <= this.values[i - 1]) {
        this.values[i] = this.values[i - 1] + 1;
      }
    }
  }

  // a value with smaller index must be smaller than a the value with the next higher index
  private setValueLimits(values: number[]): Interval[] {
    return values.map((_val, idx) => {
      return new Interval(idx, this.valueRangeLimits.end - (this.values.length - idx - 1));
    });
  }
}
