import { InfluenceFactor, SimpleUserDefinedInfluenceFactor } from '@entscheidungsnavi/decision-data';
import { InfluenceFactorNamePipe, StateNamePipe } from '@entscheidungsnavi/widgets';
import { range } from 'lodash';
import { Interval, normalizeDiscretePrioritized } from '@entscheidungsnavi/tools';
import { getRelativePrecisionInterval } from '../calculation/sensitivity-analysis';

export class InfluenceFactorSA {
  // SA = Sensitivity Analysis
  public name: string;
  public valueRangeLimits = new Interval(0, 100);
  public values: number[];
  public stateNames: string[];
  public initValues: number[]; // constant value in sensitivity analysis
  public changedValues: number[];
  public precision?: number; // constant value in sensitivity analysis
  public precisionIntervals?: Interval[]; // constant value in sensitivity analysis
  public readonly rawInfluenceFactor: InfluenceFactor;

  constructor(influenceFactor: InfluenceFactor) {
    this.rawInfluenceFactor = influenceFactor;

    const states = influenceFactor.getStates();
    this.values = states.map(state => state.probability);
    this.initValues = states.map(state => state.probability);
    this.changedValues = states.map(() => null);

    if (influenceFactor instanceof SimpleUserDefinedInfluenceFactor) {
      this.name = influenceFactor.name;
      this.stateNames = influenceFactor.states.map(state => state.name);
      this.precision = influenceFactor.precision;
      this.precisionIntervals = influenceFactor.states.map(state =>
        getRelativePrecisionInterval(this.valueRangeLimits, state.probability, influenceFactor.precision),
      );
    } else {
      this.name = new InfluenceFactorNamePipe().transform(influenceFactor);
      this.stateNames = range(states.length).map(stateIdx => new StateNamePipe().transform(influenceFactor, stateIdx));
    }
  }

  public resetValues() {
    this.values = this.initValues.slice();
    this.changedValues = this.initValues.map(() => null);
  }

  public valuesChanged(): boolean {
    return this.values.every((el, idx) => el === this.initValues[idx]);
  }

  public setValue(value: number, idx: number) {
    this.values[idx] = value;
    this.changedValues[idx] = value;
    // use initValues as base for unchanged values, change
    const baseValues = this.changedValues.map((val, i) => (val != null ? val : this.initValues[i]));
    // the value currently set has priority 3, all changed values have priority 2, initial values have priority 1
    const priorities = this.changedValues.map((changed, i) => (i === idx ? 3 : changed != null ? 2 : 1));
    this.values = normalizeDiscretePrioritized(baseValues, priorities);
  }
}
