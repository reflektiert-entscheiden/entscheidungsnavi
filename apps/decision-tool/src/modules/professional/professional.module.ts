import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SelectLineComponent } from '@entscheidungsnavi/widgets/select';
import {
  ElementRefDirective,
  HoverPopOverDirective,
  ScrollShadowComponent,
  ScrollShadowInnerComponent,
  ToggleButtonComponent,
  WidgetsModule,
  WidthTriggerDirective,
} from '@entscheidungsnavi/widgets';
import { DecisionStatementModule } from '../decision-statement/decision-statement.module';
import { ImpactModelModule } from '../impact-model/impact-model.module';
import { ResultsModule } from '../results/results.module';
import { FinishProjectModule } from '../../app/main/finish-project/finish-project.module';
import { SharedModule } from '../shared/shared.module';
import { ObjectivesModule } from '../objectives/objectives.module';
import { AlternativesModule } from '../alternatives/alternatives.module';
import { ObjectiveWeightsAnalysisComponent } from '../results/objective-weights-analysis/objective-weights-analysis.component';
import { ChartRankingComponent } from '../results/chart-ranking';
import { ObjectiveHierarchyComponent } from '../objectives/objective-hierarchy/objective-hierarchy.component';
import { PfMainComponent } from './main/pf-main.component';
import { PfStructureAndEstimateMainComponent } from './structure-and-estimate/main/pf-structure-and-estimate-main.component';
import { PfEvaluateAndDecideMainComponent } from './evaluate-and-decide/main/pf-evaluate-and-decide-main.component';
import { PfSensitivityAnalysisComponent } from './evaluate-and-decide/pf-sensitivity-analysis.component';
import { PfProAndContraComponent } from './evaluate-and-decide/pf-pro-and-contra.component';
import { PfRobustnessCheckComponent } from './evaluate-and-decide/pf-robustness-check.component';
import { PfFinishProjectComponent } from './evaluate-and-decide/pf-finish-project.component';
import { PfDecisionstatementAssumptionsComponent } from './structure-and-estimate/pf-decisionstatement-assumptions.component';
import { PfObjectiveAspectHierarchyComponent } from './structure-and-estimate/pf-objective-aspect-hierarchy.component';
import { PfAlternativesWeakPointsComponent } from './structure-and-estimate/pf-alternatives-weak-points.component';
import { PfAlternativeObjectiveFocusedSearchComponent } from './structure-and-estimate/pf-alternative-objective-focused-search.component';
import { PfAlternativesLeverMethodComponent } from './structure-and-estimate/pf-alternatives-lever-method.component';
import { PfObjectiveCheckListsComponent } from './structure-and-estimate/pf-objective-check-lists.component';
import { PfObjectiveListComponent } from './structure-and-estimate/pf-objective-list.component';
import { PfAlternativesComponent } from './structure-and-estimate/pf-alternatives.component';
import { PfUtilityFunctionsMainComponent } from './evaluate-and-decide/pf-utility-functions-main.component';
import { PfObjectiveWeightingMainComponent } from './evaluate-and-decide/pf-objective-weighting-main.component';
import { PfInfluenceFactorsComponent } from './structure-and-estimate/pf-influence-factors.component';
import { PfToolComponent } from './main/navigation/tool-category/tool-category.component';
import { ProfessionalRoutingModule } from './professional-routing.module';
import { PfRiskComparisonComponent } from './evaluate-and-decide/pf-risk-comparison.component';
import { PfCostUtilityAnalysisComponent } from './evaluate-and-decide/pf-cost-utility-analysis.component';
import { PfUtilityBasedTornadoDiagramComponent } from './evaluate-and-decide/pf-utility-based-tornado-diagram.component';

@NgModule({
  declarations: [
    PfMainComponent,
    PfStructureAndEstimateMainComponent,
    PfUtilityFunctionsMainComponent,
    PfSensitivityAnalysisComponent,
    PfUtilityBasedTornadoDiagramComponent,
    PfProAndContraComponent,
    PfRobustnessCheckComponent,
    PfRiskComparisonComponent,
    PfCostUtilityAnalysisComponent,
    PfEvaluateAndDecideMainComponent,
    PfFinishProjectComponent,
    PfDecisionstatementAssumptionsComponent,
    PfObjectiveAspectHierarchyComponent,
    PfObjectiveWeightingMainComponent,
    PfAlternativesWeakPointsComponent,
    PfAlternativeObjectiveFocusedSearchComponent,
    PfAlternativesLeverMethodComponent,
    PfObjectiveCheckListsComponent,
    PfObjectiveListComponent,
    PfAlternativesComponent,
    PfInfluenceFactorsComponent,
    PfToolComponent,
    ObjectiveWeightsAnalysisComponent,
  ],
  imports: [
    CommonModule,
    ProfessionalRoutingModule,
    DecisionStatementModule,
    ImpactModelModule,
    WidgetsModule,
    ResultsModule,
    FinishProjectModule,
    SharedModule,
    ObjectivesModule,
    AlternativesModule,
    WidthTriggerDirective,
    HoverPopOverDirective,
    SelectLineComponent,
    ToggleButtonComponent,
    ScrollShadowComponent,
    ScrollShadowInnerComponent,
    ChartRankingComponent,
    ObjectiveHierarchyComponent,
    ElementRefDirective,
  ],
})
export class ProfessionalModule {}
