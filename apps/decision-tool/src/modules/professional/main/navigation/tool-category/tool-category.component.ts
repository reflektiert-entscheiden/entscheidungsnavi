import { Component, EventEmitter, Input, Output } from '@angular/core';
import { DecisionData } from '@entscheidungsnavi/decision-data';
import { EvaluateAndDecideTool, StructureAndEstimateTool, Tool } from '../../pf-main.component';

const VERBALIZED_TOOLS: { [key in Tool]: string } = {
  'sae-overview': $localize`Übersicht`,

  // Tools in "Structure and estimate".
  'sae-assumptions': $localize`Annahmen und Abgrenzungen`,
  'sae-objective-list': $localize`Listenansicht`,
  'sae-alternative-list': $localize`Übersicht`,
  'sae-hierarchy': $localize`Hierarchie`,
  'sae-checklists': $localize`Beispiele und Vorschläge`,
  'sae-weak-points': $localize`Schwachpunktanalyse`,
  'sae-objective-focused-search': $localize`Zielfokussierte Suche`,
  'sae-lever-method': $localize`Stellhebelmethode`,
  'sae-influence-factors': $localize`Einflussfaktoren`,

  // Tools in "Evaluate and decide".
  'ead-overview': $localize`Übersicht`,
  'ead-utility-functions': $localize`Nutzenfunktionen`,
  'ead-objective-weighting': $localize`Zielgewichtung`,
  'ead-sensitivity-analysis': $localize`Sensitivitätsanalyse`,
  'ead-pros-and-cons': $localize`Pros und Kontras`,
  'ead-robustness-check': $localize`Robustheitstest`,
  'ead-risk-comparison': $localize`Risikovergleich`,
  'ead-objective-weight-analysis': $localize`Zielgewichtsanalyse`,
  'ead-cost-utility-analysis': $localize`Kosten-Nutzen-Analyse`,
  'ead-utility-based-tornado-diagram': $localize`Tornadodiagramm`,
};

@Component({
  selector: 'dt-pf-tool',
  templateUrl: './tool-category.component.html',
  styleUrls: ['./tool-category.component.scss'],
})
export class PfToolComponent {
  @Input()
  tool: Tool;

  @Input()
  activeTool: Tool;

  @Output()
  activeToolChange = new EventEmitter<Tool>();

  @Input()
  categoryName: string;

  @Input()
  toolsInCategory?: Tool[];

  @Output()
  toolSelected = new EventEmitter<Tool>();

  get isSingle() {
    return this.toolsInCategory === undefined;
  }

  get toolInCategorySelected() {
    return !this.isSingle && this.toolsInCategory.includes(this.activeTool);
  }

  constructor(private decisionData: DecisionData) {}

  verbalizeTool(tool: StructureAndEstimateTool | EvaluateAndDecideTool) {
    return VERBALIZED_TOOLS[tool];
  }

  protected isOptionDisabled(tool: Tool) {
    return (
      (tool === 'ead-cost-utility-analysis' && this.decisionData.objectives.findIndex(objective => !objective.isVerbal) === -1) ||
      (tool === 'ead-objective-weight-analysis' && this.decisionData.objectives.length < 2)
    );
  }

  protected getDisabledTooltip(tool: Tool) {
    switch (tool) {
      case 'ead-cost-utility-analysis':
        return this.isOptionDisabled(tool) ? $localize`Die Kosten-Nutzen-Analyse ist nicht für verbale Ziele definiert` : '';
      case 'ead-objective-weight-analysis':
        return this.isOptionDisabled(tool) ? $localize`Die Zielgewichtsanalyse ist nur mit mindestens zwei Zielen verfügbar` : '';
      default:
        return '';
    }
  }
}
