import { Component } from '@angular/core';
import { DisplayAtMaxWidth } from '../../../app/interfaces/display-at-max-width';

@Component({
  selector: 'dt-pf-objective-weighting-main',
  template: `
    <div class="dt-limit-width">
      <dt-step-description [withBiggerMargin]="true">
        <ng-template #description>
          @switch (selectedObjective === -1) {
            @case (true) {
              <ng-container i18n
                >Auf dieser Seite kannst Du eine pauschale Gewichtung Deiner Ziele vornehmen. Für eine fundierte Gewichtung der Ziele wähle
                bitte ein Referenzziel und klicke in die resultierenden Trade-offs mit den anderen Zielen.</ng-container
              >
            }
            @default {
              <ng-container i18n
                >Hier kannst Du angeben, wie viel Verschlechterung Du in einem Ziel hinnimmst, um eine Verbesserung in einem anderen Ziel zu
                erreichen. Mit den Buttons unter dem Diagramm kannst Du den abgebildeten Trade-off und das Präzisionsintervall anpassen oder
                auch einen konkreten Trade-off selbst angeben. Rechts wird die Bedeutung der Abbildung auf zwei Arten erklärt.</ng-container
              >
            }
          }
        </ng-template>
      </dt-step-description>

      <dt-objective-weighting-navigation (selectedObjectiveChange)="selectedObjective = $event"></dt-objective-weighting-navigation>
    </div>

    <div [class.dt-limit-width]="selectedObjective !== -1">
      <router-outlet></router-outlet>
    </div>
  `,
  styles: [
    `
      dt-step-description ::ng-deep .text-container {
        line-height: 1.2;
      }
    `,
  ],
})
@DisplayAtMaxWidth
export class PfObjectiveWeightingMainComponent {
  selectedObjective = -1;
}
