import { Component } from '@angular/core';

@Component({
  selector: 'dt-pf-cost-utility-analysis',
  template: '<dt-cost-utility-analysis [showTitle]="false"></dt-cost-utility-analysis>',
  styles: [],
})
export class PfCostUtilityAnalysisComponent {}
