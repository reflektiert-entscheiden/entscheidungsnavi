import { Component } from '@angular/core';

@Component({
  selector: 'dt-pf-utility-functions-main',
  template: `
    <div class="dt-limit-width">
      <dt-step-description [mainDescriptionOnly]="true">
        <ng-template #description>
          @switch (selectedObjective) {
            @case (-1) {
              <ng-container i18n
                >Auf dieser Übersichtsseite kannst Du Deine Nutzenfunktionen durch Klicken in die Diagramme verändern. In eine Detailansicht
                gelangst Du, indem Du auf die Zielnamen klickst.</ng-container
              >
            }
            @default {
              <ng-container i18n
                >Du kannst die Nutzenfunktion durch Klicken in die Abbildung und über die Buttons unterhalb der Abbildung anpassen. Die
                Bedeutung der Kurvenform wird Dir rechts in den vier Darstellungsoptionen erläutert.</ng-container
              >
            }
          }
        </ng-template>
      </dt-step-description>

      <dt-utility-function-navigation (selectedObjectiveChange)="selectedObjective = $event"></dt-utility-function-navigation>
    </div>

    <router-outlet></router-outlet>
  `,
  styles: [
    `
      :host {
        height: 100%;
        display: flex;
        flex-direction: column;
      }

      dt-select-line {
        display: block;
        margin-bottom: 16px;
      }
    `,
  ],
})
export class PfUtilityFunctionsMainComponent {
  selectedObjective = -1;
}
