import { Component } from '@angular/core';

@Component({
  selector: 'dt-pf-finish-project',
  template: '<dt-finish-project></dt-finish-project>',
})
export class PfFinishProjectComponent {}
