import { Component } from '@angular/core';
import { DisplayAtMaxWidth } from '../../../app/interfaces/display-at-max-width';

@Component({
  selector: 'dt-pf-pro-and-contra',
  template: '<dt-pro-and-contra [showTitle]="false"></dt-pro-and-contra>',
  styles: [
    `
      :host {
        display: flex;
        flex-direction: column;
        height: 100%;
      }

      dt-pro-and-contra {
        padding-bottom: 25px;
      }
    `,
  ],
})
@DisplayAtMaxWidth
export class PfProAndContraComponent {}
