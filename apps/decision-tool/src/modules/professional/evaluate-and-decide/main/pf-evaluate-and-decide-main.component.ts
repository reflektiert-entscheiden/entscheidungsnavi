import { AfterViewInit, Component, OnInit } from '@angular/core';
import { DecisionData, getAlternativeUtilities } from '@entscheidungsnavi/decision-data';
import { ActivatedRoute } from '@angular/router';
import { PersistentSetting, PersistentSettingParent } from '@entscheidungsnavi/widgets';
import { DisplayAtMaxWidth } from '../../../../app/interfaces/display-at-max-width';

@Component({
  selector: 'dt-professional-ead-main',
  templateUrl: './pf-evaluate-and-decide-main.component.html',
  styleUrls: ['./pf-evaluate-and-decide-main.component.scss'],
})
@DisplayAtMaxWidth
@PersistentSettingParent('PfEvaluateAndDecideMain')
export class PfEvaluateAndDecideMainComponent implements OnInit, AfterViewInit {
  initialUtilityValues: number[];
  utilityValues: number[];
  weightedUtilityMatrix: number[][];

  @PersistentSetting('project') displayMode: 'simple' | 'detailed' = 'simple';
  @PersistentSetting('project') orderMode: 'utilityValues' | 'gutFeeling' = 'utilityValues';

  alternativeNames: string[];
  objectiveNames: string[];

  showCheckedAlternatives = false;
  checkedAlternatives: boolean[];

  constructor(
    private route: ActivatedRoute,
    private decisionData: DecisionData,
  ) {}

  ngAfterViewInit(): void {
    setTimeout(() => this.scrollToObjectiveWeights(this.route.snapshot.fragment), 0);
  }

  scrollToObjectiveWeights(fragment: string) {
    const element = document.querySelector('#' + fragment);
    if (element) element.scrollIntoView({ block: 'start', behavior: 'smooth', inline: 'nearest' });
  }

  ngOnInit() {
    this.checkedAlternatives = this.decisionData.alternatives.map(() => true);

    this.alternativeNames = this.decisionData.alternatives.map(a => a.name);
    this.objectiveNames = this.decisionData.objectives.map(o => o.name);

    this.recalculateObjectiveValues();
    this.initialUtilityValues = this.utilityValues;
  }

  recalculateObjectiveValues() {
    this.weightedUtilityMatrix = this.decisionData.getWeightedUtilityMatrix();
    this.utilityValues = getAlternativeUtilities(this.weightedUtilityMatrix);
  }
}
