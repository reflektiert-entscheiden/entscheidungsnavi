import { Component } from '@angular/core';

@Component({
  selector: 'dt-pf-sensitivity-analysis',
  template: '<dt-sensitivity-analysis [showTitle]="false"> </dt-sensitivity-analysis>',
  styles: [
    `
      :host dt-sensitivity-analysis {
        display: block;
        padding-bottom: 40px;

        ::ng-deep dt-chart-ranking {
          padding-top: 0;
        }
      }
    `,
  ],
})
export class PfSensitivityAnalysisComponent {}
