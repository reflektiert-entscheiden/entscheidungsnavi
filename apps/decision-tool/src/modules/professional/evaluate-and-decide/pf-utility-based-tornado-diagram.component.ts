import { Component } from '@angular/core';

@Component({
  selector: 'dt-pf-utility-based-tornado-diagram',
  template: '<dt-utility-based-tornado-diagram [showTitle]="false"></dt-utility-based-tornado-diagram>',
  styles: [],
})
export class PfUtilityBasedTornadoDiagramComponent {}
