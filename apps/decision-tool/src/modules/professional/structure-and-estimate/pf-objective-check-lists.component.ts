import { Component } from '@angular/core';
import { DisplayAtMaxWidth } from '../../../app/interfaces/display-at-max-width';

@Component({
  template: `<dt-objective-hint-4 [showTitle]="false" data-cy="structure-and-estimate-tool-checklists-checklist"></dt-objective-hint-4>`,
  styles: [
    `
      dt-objective-hint-4 {
        padding-bottom: 20px;
      }
    `,
  ],
})
@DisplayAtMaxWidth
export class PfObjectiveCheckListsComponent {}
