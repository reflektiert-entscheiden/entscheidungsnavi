import { Component } from '@angular/core';

@Component({
  template: `
    <dt-step-description [mainDescriptionOnly]="true">
      <ng-template #description>
        <ng-container i18n
          >In dieser Listenansicht kannst Du die Ziele anordnen und beschreiben. Eine gut dokumentierte Definition der Ziele in den
          Erklärungsfeldern hilft Dir bei der Abschätzung der Ergebnisse.</ng-container
        >
      </ng-template>
    </dt-step-description>
    <dt-objective-list data-cy="structure-and-estimate-tool-objective-list-objective-list"></dt-objective-list>
  `,
  styles: [
    `
      dt-objective-list {
        display: block;
        padding-bottom: 25px;
      }
    `,
  ],
})
export class PfObjectiveListComponent {}
