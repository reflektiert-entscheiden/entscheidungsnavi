import { Component } from '@angular/core';

@Component({
  template: '<dt-alternatives [showTitle]="false" data-cy="structure-and-estimate-tool-alternative-list-alternatives"></dt-alternatives>',
  styles: [
    `
      dt-alternatives {
        display: block;
        padding-bottom: 25px;
      }
    `,
  ],
})
export class PfAlternativesComponent {}
