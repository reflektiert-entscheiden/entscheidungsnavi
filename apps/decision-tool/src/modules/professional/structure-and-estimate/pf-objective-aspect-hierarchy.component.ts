import { Component } from '@angular/core';
import { DisplayAtMaxWidth } from '../../../app/interfaces/display-at-max-width';

@Component({
  template: `
    <dt-step-description [mainDescriptionOnly]="true">
      <ng-template #description>
        <ng-container i18n
          >Hier kannst Du Deine Ziele in weitere Unterziele oder Teilaspekte aufschlüsseln und auf dieser Basis eine transparente
          Gesamtstruktur Deines Zielsystems entwickeln. In die Ergebnismatrix werden nur die Ziele aus der ersten Hierarchieebene (die
          Fundamentalziele) übernommen. Zur einfacheren Bedienung kannst Du auch mit Shortcuts arbeiten.</ng-container
        >
      </ng-template>
    </dt-step-description>
    <dt-objective-hierarchy data-cy="structure-and-estimate-tool-hierarchy-hierarchy"></dt-objective-hierarchy>
  `,
  styles: [
    `
      :host {
        display: flex;
        flex-direction: column;
        height: 100%;
      }

      dt-objective-hierarchy {
        border: 1px solid lightgray;
        flex: 1 0 0;
        min-height: 200px;
      }
    `,
  ],
})
@DisplayAtMaxWidth
export class PfObjectiveAspectHierarchyComponent {}
