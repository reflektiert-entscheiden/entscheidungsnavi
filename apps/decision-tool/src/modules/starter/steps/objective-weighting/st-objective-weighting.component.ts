import { Component } from '@angular/core';

import { DecisionData } from '@entscheidungsnavi/decision-data';
import { WidgetsModule } from '@entscheidungsnavi/widgets';
import { range } from 'lodash';
import { StarterHeadingComponent } from '../../common/st-heading.component';
import { StarterNavigationButtonsComponent } from '../../common/st-navigation-buttons.component';
import { StarterHelpExpansionPanelComponent } from '../../common/st-help-expansion-panel.component';

@Component({
  templateUrl: './st-objective-weighting.component.html',
  styleUrls: ['./st-objective-weighting.component.scss'],
  standalone: true,
  imports: [StarterHeadingComponent, StarterNavigationButtonsComponent, StarterHelpExpansionPanelComponent, WidgetsModule],
})
export class StarterObjectiveWeightingComponent {
  readonly maxWeight = 10;
  readonly gridLines = range(0, this.maxWeight + 1);
  readonly gridLabels = range(0, this.maxWeight + 1, 2).reverse();

  visibleWeights: number[];

  constructor(protected decisionData: DecisionData) {
    this.decisionData.resultSubstepProgress = Math.max(this.decisionData.resultSubstepProgress, 1);

    const weightValues = this.decisionData.weights.getWeightValues().filter(value => value != null);
    const maxWeight = Math.max(...weightValues, 0);

    // Make sure the visible weights are in the range [0, 10]
    if (maxWeight > 0) {
      this.visibleWeights = weightValues.map(value => (value != null ? Math.round((value / maxWeight) * this.maxWeight) : null));
    } else {
      this.visibleWeights = this.decisionData.objectives.map(() => null);
    }
  }

  updateWeights() {
    if (this.visibleWeights.some(weight => weight == null)) return;

    this.visibleWeights.forEach((weight, index) => {
      this.decisionData.weights.changePreliminaryWeightValue(index, weight);
    });
    this.decisionData.weights.normalizePreliminaryWeights();

    if (this.decisionData.weights.tradeoffObjectiveIdx == null) {
      this.decisionData.weights.setDefaultTradeoffObjective(this.decisionData.objectives);
    }
  }
}
