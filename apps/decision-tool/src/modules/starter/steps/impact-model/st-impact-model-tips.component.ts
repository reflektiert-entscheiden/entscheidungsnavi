import { Component } from '@angular/core';

import { ModalModule } from '@entscheidungsnavi/widgets/modal/modal.module';
import { MatButtonModule } from '@angular/material/button';
import { MatDialogRef } from '@angular/material/dialog';
import { MatRippleModule } from '@angular/material/core';
import { WidthTriggerDirective } from '@entscheidungsnavi/widgets';
import { STARTER_IMPACT_MODEL_TIPS } from './starter-impact-model-tips';

@Component({
  template: `
    <dt-modal-content (closeClick)="dialogRef.close()" [flexContent]="true">
      <ng-template #title i18n>Mögliche Biases bei Einschätzungen</ng-template>
      <ng-template #content>
        <p i18n
          >Folgende sechs Fallen im Zusammenhang mit Einschätzungen (Biases) lassen sich recht häufig beobachten. Schau Dir deshalb die
          Tipps an, wie Du diese vermeiden kannst.</p
        >
        <div class="biases-wrapper" [dtWidthTrigger]="{ 'mobile-mode': 500 }">
          <div class="biases-navigation">
            @for (tip of tips; track tip; let tipIndex = $index) {
              <div class="navigation-item" (mouseenter)="hoveredTipIndex = tipIndex" (mouseleave)="hoveredTipIndex = null">
                <button
                  matRipple
                  class="navigation-button"
                  (click)="activeTipIndex = tipIndex"
                  [class.active]="activeTipIndex === tipIndex && hoveredTipIndex == null"
                  >{{ tip.name }}</button
                >
              </div>
            }
          </div>
          <div class="bias-view">
            @for (tip of tips; track tip; let tipIndex = $index) {
              <div class="content" [class.visible]="visibleTipIndex === tipIndex">
                <h4>{{ tip.heading }}</h4>
                @for (text of tip.paragraphs; track text) {
                  <p>{{ text }}</p>
                }
              </div>
            }
          </div>
        </div>
      </ng-template>
      <ng-template #footer>
        <button mat-raised-button color="accent-less" (click)="dialogRef.close()" i18n>Schließen</button>
      </ng-template>
    </dt-modal-content>
  `,
  styles: [
    //language=SCSS
    `
      @use '@angular/material' as mat;
      @use 'variables' as dt;

      .biases-wrapper {
        flex: 0 1 auto;

        display: flex;
        flex-direction: row;
        gap: 16px;

        text-align: center;
      }

      .navigation-item {
        padding: 8px 0;
      }

      .navigation-button {
        display: block;
        min-width: 100%;

        padding: 8px 16px;

        &.active,
        &:hover {
          background-color: #e0e0e0;
        }
      }

      .bias-view {
        flex: 1 1 auto;
        margin-top: 8px;
        padding: 16px 24px;

        display: grid;

        .content {
          grid-area: 1/1/2/2;

          &:not(.visible) {
            visibility: hidden;
          }
        }
      }

      .navigation-button,
      .bias-view {
        @include mat.elevation(1);
        border-radius: 5px;
        background-color: #f5f5f5;
      }

      .biases-wrapper.mobile-mode {
        flex-direction: column;

        .biases-navigation {
          overflow-x: auto;

          display: flex;

          padding: 8px calc(dt.$modal-padding - 8px);
          margin: 0 -#{dt.$modal-padding};
        }

        .navigation-item {
          padding: 0 8px;
        }

        .bias-view {
          margin-top: 0;
        }
      }
    `,
  ],
  standalone: true,
  imports: [ModalModule, MatButtonModule, MatRippleModule, WidthTriggerDirective],
})
export class StarterImpactModelTipsComponent {
  readonly tips = STARTER_IMPACT_MODEL_TIPS;

  activeTipIndex: number;
  hoveredTipIndex: number;

  get visibleTipIndex() {
    return this.hoveredTipIndex ?? this.activeTipIndex;
  }

  constructor(protected dialogRef: MatDialogRef<StarterImpactModelTipsComponent>) {}
}
