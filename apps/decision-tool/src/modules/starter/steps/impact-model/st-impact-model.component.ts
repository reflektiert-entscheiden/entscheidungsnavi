import { Component } from '@angular/core';

import { DecisionData } from '@entscheidungsnavi/decision-data';
import { MatButtonModule } from '@angular/material/button';
import { MatMenuModule } from '@angular/material/menu';
import { range } from 'lodash';
import { MatRippleModule } from '@angular/material/core';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatDialog } from '@angular/material/dialog';
import { StarterHeadingComponent } from '../../common/st-heading.component';
import { StarterNavigationButtonsComponent } from '../../common/st-navigation-buttons.component';
import { StarterHelpExpansionPanelComponent } from '../../common/st-help-expansion-panel.component';
import { StarterImpactModelTipsComponent } from './st-impact-model-tips.component';

@Component({
  templateUrl: './st-impact-model.component.html',
  styleUrls: [`./st-impact-model.component.scss`],
  standalone: true,
  imports: [
    StarterHeadingComponent,
    StarterNavigationButtonsComponent,
    StarterHelpExpansionPanelComponent,
    MatButtonModule,
    MatMenuModule,
    MatRippleModule,
    MatTooltipModule,
  ],
})
export class StarterImpactModelComponent {
  protected readonly possibleOutcomes = range(0, 11);

  constructor(
    protected decisionData: DecisionData,
    private dialog: MatDialog,
  ) {}

  openTips() {
    this.dialog.open(StarterImpactModelTipsComponent);
  }
}
