/* eslint-disable max-len */
export const STARTER_IMPACT_MODEL_TIPS: ReadonlyArray<
  Readonly<{
    name: string;
    heading: string;
    paragraphs: ReadonlyArray<string>;
  }>
> = [
  {
    name: $localize`Dissonanzfalle`,
    heading: $localize`Was ist die Dissonanzfalle?`,
    paragraphs: [
      $localize`Wir alle wollen grundsätzlich Dissonanzen zwischen unserem Denken und Handeln vermeiden und uns keine Fehler eingestehen. Unterbewusst versuchen wir daher, ungünstige getroffene Entscheidungen im Nachhinein unabhängig von der Sinnhaftigkeit zwanghaft noch zum Erfolg zu führen.`,
      $localize`Beachte aber, dass durch die Auswahl einer Alternative niemals Vergangenes verändert werden kann. Insofern gib bei allen Einschätzungen in der Matrix nur die Ergebnisse an, die in Zukunft mit der Alternative verknüpft sein werden.`,
    ],
  },
  {
    name: $localize`Plausibilitätsfalle`,
    heading: $localize`Was ist die Plausibilitätsfalle?`,
    paragraphs: [
      $localize`Wir alle neigen sehr schnell dazu, etwas als wahr aufzufassen, wenn es sich plausibel anhört und in ein bestimmtes Denkschema passt.`,
      $localize`Lass Dich deshalb nicht vorschnell von Plausibilitäten beeindrucken. Hinterfrage alles und lass Dich lediglich von eindeutigen Fakten überzeugen. Wenn etwas plausibel klingt, muss es noch lange nicht wahr sein!`,
    ],
  },
  {
    name: $localize`Scheuklappenfalle`,
    heading: $localize`Was ist die Scheuklappenfalle?`,
    paragraphs: [
      $localize`Wir kennen immer nur einen kleinen Ausschnitt unserer Umwelt mit allen Gegebenheiten und Wahrheiten und sind ggf. durch unsere Erfahrungen verankert.`,
      $localize`Suche deshalb bewusst nach Informationen und Meinungen außerhalb Deines normalen Blickfeldes, um möglichst realistische Einschätzungen abzugeben. Vergegenwärtige Dir, dass Du nicht weißt, was Du nicht weißt und dass ein bisschen Zurückhaltung bei der Einschätzung angebracht ist, was stimmt und was nicht.`,
    ],
  },
  {
    name: $localize`Selbstüberschätzungsfalle`,
    heading: $localize`Was ist die Selbstüberschätzungsfalle?`,
    paragraphs: [
      $localize`Wenn wir unsere eigenen Fähigkeiten einschätzen, kann häufig eine Überschätzung beobachtet werden. Auch sind wir häufig zu sehr davon überzeugt, dass unsere Einschätzungen besser sind als die Einschätzungen, die andere abgeben.`,
      $localize`Hinterfrage also für alle Eintragungen in der Matrix Deine subjektiven Bewertungen nochmal genauer. Setze Dich damit auseinander, warum Du Dich ggf. sicher fühlst und andere Meinungen ausschließen kannst.`,
    ],
  },
  {
    name: $localize`Überreaktionsfalle`,
    heading: $localize`Was ist die Überreaktionsfalle?`,
    paragraphs: [
      $localize`Wir alle greifen verstärkt nur auf leicht verfügbare Informationen zurück, d.h. aktuelle, anschauliche oder häufig präsentierte Informationen. Deshalb werden andere, ggf. genauso wichtige Informationen leicht vernachlässigt und es kommt deshalb zu einer Überreaktion auf die verfügbaren Informationen.`,
      $localize`Achte also darauf, ob Du Dich in Deiner Situation möglicherweise zu sehr auf solche verfügbaren Informationen stützt und deshalb vielleicht eine verzerrte Einschätzung abgibst. Versuche stets alle Fakten heranzuziehen und insbesondere diejenigen, die Dir nicht auf dem Tablett präsentiert werden.`,
    ],
  },
  {
    name: $localize`Darstellungsfalle`,
    heading: $localize`Was ist die Darstellungsfalle?`,
    paragraphs: [
      $localize`Die Art und Weise, wie uns Sachverhalte oder Informationen präsentiert werden, kann einen Einfluss auf unsere Wahrnehmung und unser Verhalten nehmen („Framing“). Es macht eben einen Unterschied, ob uns ein halb volles oder ein halb leeres Glas präsentiert wird.`,
      $localize`Mache Dir bewusst, dass eine Information durch unterschiedliche Arten der Darstellung anders auf Dich wirken kann und versuche stets, den wahren Aussagegehalt einer Information zu identifizieren.`,
    ],
  },
];
