import { Component } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { RouterLink } from '@angular/router';
import { SelectLineComponent } from '@entscheidungsnavi/widgets/select';
import { DecisionData, Objective } from '@entscheidungsnavi/decision-data';
import { FormsModule } from '@angular/forms';
import { MatIconModule } from '@angular/material/icon';

import { CdkDrag, CdkDragDrop, CdkDragHandle, CdkDropList } from '@angular/cdk/drag-drop';
import { MatDialog } from '@angular/material/dialog';
import { AutoFocusDirective } from '@entscheidungsnavi/widgets';
import { StarterHelpExpansionPanelComponent } from '../../common/st-help-expansion-panel.component';
import { StarterHeadingComponent } from '../../common/st-heading.component';
import { StarterNavigationButtonsComponent } from '../../common/st-navigation-buttons.component';
import { StarterFundamentalObjectivesModalComponent } from './st-fundamental-objectives-modal.component';

@Component({
  templateUrl: 'st-objectives.component.html',
  styleUrls: ['st-objectives.component.scss'],
  imports: [
    MatButtonModule,
    RouterLink,
    StarterHelpExpansionPanelComponent,
    SelectLineComponent,
    StarterHeadingComponent,
    FormsModule,
    MatIconModule,
    CdkDropList,
    CdkDrag,
    CdkDragHandle,
    AutoFocusDirective,
    StarterNavigationButtonsComponent,
  ],
  standalone: true,
})
export class StarterObjectivesComponent {
  enableAutoFocus = false;

  get errorMessage() {
    if (this.decisionData.objectives.some(objective => objective.name.length === 0)) return $localize`Du musst alle Ziele benennen`;
    else return $localize`Du musst mindestens ein Ziel definieren`;
  }

  constructor(
    protected decisionData: DecisionData,
    private dialog: MatDialog,
  ) {
    decisionData.objectiveAspects.subStepProgression = 'RESULT';

    if (this.decisionData.objectives.length === 0) {
      this.addObjective();
    }
  }

  addObjective(name?: string) {
    this.enableAutoFocus = true;

    const objective = new Objective(name);
    objective.numericalData.from = 0;
    objective.numericalData.to = 10;
    this.decisionData.addObjective(objective);
  }

  drop(event: CdkDragDrop<unknown>) {
    this.decisionData.moveObjective(event.previousIndex, event.currentIndex);
  }

  openFundamentalObjectives() {
    this.dialog
      .open<StarterFundamentalObjectivesModalComponent, null, string[]>(StarterFundamentalObjectivesModalComponent)
      .afterClosed()
      .subscribe(objectiveNames => {
        if (!objectiveNames) return;

        if (this.decisionData.objectives.length > 0 && this.decisionData.objectives.at(-1).name.length === 0) {
          this.decisionData.removeObjective(this.decisionData.objectives.length - 1);
        }

        this.enableAutoFocus = false;
        objectiveNames.forEach(objectiveName => this.addObjective(objectiveName));
      });
  }
}
