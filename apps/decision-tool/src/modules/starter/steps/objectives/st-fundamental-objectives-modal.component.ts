import { Component } from '@angular/core';

import { ModalModule } from '@entscheidungsnavi/widgets/modal/modal.module';
import { MatButtonModule } from '@angular/material/button';
import { MatDialogRef } from '@angular/material/dialog';
import { GroupedQuickstartObjectiveDto, QuickstartService } from '@entscheidungsnavi/api-client';
import { LocalizedStringPipe, Tag, TagInputComponent } from '@entscheidungsnavi/widgets';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { OverlayProgressBarDirective } from '@entscheidungsnavi/widgets/overlay-progress-bar';
import { catchError, defer, EMPTY, finalize, MonoTypeOperatorFunction, switchMap, tap } from 'rxjs';
import { MatRadioModule } from '@angular/material/radio';
import { MatRippleModule } from '@angular/material/core';
import { MatDividerModule } from '@angular/material/divider';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { DecisionData } from '@entscheidungsnavi/decision-data';
import { noop } from 'lodash';
import { LanguageService } from '../../../../app/data/language.service';
import { StarterContextService } from '../../common/st-context-service.service';

@Component({
  template: `
    <dt-modal-content (closeClick)="dialogRef.close()" [flexContent]="true" [contentPadding]="false">
      <ng-template #title i18n>Beispiele für Fundamentalziele</ng-template>
      <ng-template #content>
        <dt-tag-input
          [allTags]="tags"
          [(selectedTagIds)]="tagIdFilter"
          (selectedTagIdsChange)="onTagFilterChange()"
          [dtOverlayProgress]="isLoading"
          i18n
          >Nach Tags filtern</dt-tag-input
        >
        <mat-divider></mat-divider>
        @switch (hasError) {
          @case (true) {
            <mat-error class="dt-error" i18n
              >Beim Laden der Beispiele ist ein Fehler aufgetreten. Bitte versuche es später erneut.</mat-error
            >
          }
          @case (false) {
            <div class="objective-list">
              @for (objective of visibleObjectives; track objective) {
                <div class="objective" (click)="toggleObjective(objective)" matRipple>
                  <mat-checkbox [checked]="selectedObjectiveNames.has(objective.name)"></mat-checkbox>
                  <div>{{ objective.name }}</div>
                </div>
              }
              @if (visibleObjectives.length === 0) {
                <div class="no-objectives-found" i18n>Keine Ziele entsprechen dem Filter.</div>
              }
              @if (objectivePaginationHasMore) {
                <button mat-raised-button color="accent" (click)="loadMore()" i18n>Mehr laden</button>
              }
            </div>
          }
        }
      </ng-template>
      <ng-template #footer>
        <button mat-raised-button color="accent" [disabled]="selectedObjectiveNames.size === 0" (click)="confirm()" i18n
          >Ausgewählte Ziele übernehmen</button
        >
        <button mat-raised-button color="accent-less" (click)="dialogRef.close()" i18n>Schließen</button>
      </ng-template>
    </dt-modal-content>
  `,
  styles: [
    //language=SCSS
    `
      mat-error {
        max-width: fit-content;
        margin: 16px auto;
      }

      mat-spinner {
        display: block;
        margin: 32px auto;
      }

      dt-tag-input {
        margin: 16px;
      }

      .objective-list {
        flex: 0 1 auto;
        min-height: 0;
        overflow-y: auto;
        padding: 12px 0;

        button {
          margin: 8px 16px;
          display: block;
        }
      }

      .objective {
        padding: 4px 16px;

        display: flex;
        align-items: center;
        gap: 8px;

        cursor: pointer;

        &:hover {
          background-color: rgba(0, 0, 0, 0.1);
        }
      }

      .no-objectives-found {
        font-style: italic;
        margin: 0 16px;
      }
    `,
  ],
  standalone: true,
  imports: [
    ModalModule,
    MatButtonModule,
    MatFormFieldModule,
    MatProgressSpinnerModule,
    TagInputComponent,
    OverlayProgressBarDirective,
    MatRadioModule,
    MatRippleModule,
    LocalizedStringPipe,
    MatDividerModule,
    MatCheckboxModule,
  ],
})
export class StarterFundamentalObjectivesModalComponent {
  isLoading = true;
  hasError = false;

  tags: Tag[] = [];
  tagIdFilter: string[] = [];

  objectives: GroupedQuickstartObjectiveDto[] = [];
  // We filter objectives whose name is already present in decision data
  visibleObjectives: GroupedQuickstartObjectiveDto[] = [];
  objectivePaginationHasMore = false;

  // We use the names as keys because of the grouping
  selectedObjectiveNames = new Set<string>();

  constructor(
    protected dialogRef: MatDialogRef<StarterFundamentalObjectivesModalComponent, string[]>,
    private quickstartService: QuickstartService,
    private decisionData: DecisionData,
    private languageService: LanguageService,
    private starterContextService: StarterContextService,
  ) {
    quickstartService
      .getTags()
      .pipe(
        this.loadingInterceptor(),
        switchMap(tags => {
          this.tags = tags;
          this.tagIdFilter = this.starterContextService.filterTagIds.slice();
          return this.reloadObjectives();
        }),
      )
      .subscribe(() => {
        // If we do not show enough objectives with the context filter, we reset it
        if (this.objectives.length < 5) {
          this.tagIdFilter = [];
          this.reloadObjectives().subscribe();
        }
      });
  }

  onTagFilterChange() {
    this.starterContextService.filterTagIds = this.tagIdFilter.slice();
    this.reloadObjectives().subscribe();
  }

  private reloadObjectives() {
    return this.requestObjectives(0).pipe(
      this.loadingInterceptor(),
      tap(objectives => {
        this.objectives = objectives.items;
        this.updateVisibleObjectives();
        this.selectedObjectiveNames.forEach(objectiveName => {
          if (!this.visibleObjectives.some(objective => objective.name === objectiveName)) {
            this.selectedObjectiveNames.delete(objectiveName);
          }
        });
        this.tags.forEach(tag => {
          tag.isProductive = objectives.activeTags.includes(tag.id);
        });
        this.objectivePaginationHasMore = this.objectives.length < objectives.count;
      }),
    );
  }

  loadMore() {
    this.requestObjectives(this.objectives.length)
      .pipe(this.loadingInterceptor())
      .subscribe(objectives => {
        this.objectives.push(...objectives.items);
        this.updateVisibleObjectives();
        this.objectivePaginationHasMore = this.objectives.length < objectives.count;
      });
  }

  private requestObjectives(paginationOffset: number) {
    return this.quickstartService.getGroupedObjectives(
      { tags: this.tagIdFilter, level: 'only-fundamental' },
      {
        limit: 50,
        offset: paginationOffset,
      },
      this.languageService.isEnglish ? 'nameEn' : 'nameDe',
      {
        sortBy: 'accumulatedScore',
        sortDirection: 'desc',
      },
    );
  }

  private loadingInterceptor<T>(): MonoTypeOperatorFunction<T> {
    return observable => {
      return defer(() => {
        this.isLoading = true;
        return observable;
      }).pipe(
        catchError(() => {
          this.hasError = true;
          return EMPTY;
        }),
        finalize(() => (this.isLoading = false)),
      );
    };
  }

  private updateVisibleObjectives() {
    // Filter out all objectives that already appear in DD
    this.visibleObjectives = this.objectives.filter(objective =>
      this.decisionData.objectives.every(ddObjective => ddObjective.name !== objective.name),
    );
  }

  toggleObjective(objective: GroupedQuickstartObjectiveDto) {
    if (this.selectedObjectiveNames.has(objective.name)) {
      this.selectedObjectiveNames.delete(objective.name);
    } else {
      this.selectedObjectiveNames.add(objective.name);
    }
  }

  confirm() {
    const selectedObjectives = this.objectives.filter(objective => this.selectedObjectiveNames.has(objective.name));

    this.quickstartService
      .trackObjective(selectedObjectives.flatMap(groupedObjective => groupedObjective.items).map(objective => objective.id))
      .subscribe({ error: noop });

    this.dialogRef.close(selectedObjectives.map(objective => objective.name));
  }
}
