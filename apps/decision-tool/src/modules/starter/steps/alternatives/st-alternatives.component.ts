import { Component } from '@angular/core';

import { CdkDrag, CdkDragDrop, CdkDragHandle, CdkDropList } from '@angular/cdk/drag-drop';
import { Alternative, DecisionData } from '@entscheidungsnavi/decision-data';
import { FormsModule } from '@angular/forms';
import { MatIconModule } from '@angular/material/icon';
import { AutoFocusDirective } from '@entscheidungsnavi/widgets';
import { MatButtonModule } from '@angular/material/button';
import { MatDialog } from '@angular/material/dialog';
import { StarterHeadingComponent } from '../../common/st-heading.component';
import { StarterHelpExpansionPanelComponent } from '../../common/st-help-expansion-panel.component';
import { StarterNavigationButtonsComponent } from '../../common/st-navigation-buttons.component';
import { StarterAlternativeSearchModalComponent } from './st-alternative-search-modal.component';

@Component({
  templateUrl: './st-alternatives.component.html',
  styleUrls: ['./st-alternatives.component.scss'],
  standalone: true,
  imports: [
    StarterHeadingComponent,
    CdkDrag,
    FormsModule,
    MatIconModule,
    CdkDropList,
    AutoFocusDirective,
    StarterHelpExpansionPanelComponent,
    StarterNavigationButtonsComponent,
    MatButtonModule,
    CdkDragHandle,
  ],
})
export class StarterAlternativesComponent {
  enableAutoFocus = false;

  get errorMessage() {
    if (this.decisionData.alternatives.some(alternative => alternative.name.length === 0))
      return $localize`Du musst alle Alternativen benennen`;
    else return $localize`Du musst mindestens zwei Alternativen definieren`;
  }

  constructor(
    protected decisionData: DecisionData,
    private dialog: MatDialog,
  ) {
    decisionData.hintAlternatives.subStepProgression = 'RESULT';

    if (this.decisionData.alternatives.length === 0) {
      this.addAlternative();
    }
  }

  addAlternative() {
    this.enableAutoFocus = true;
    this.decisionData.addAlternative();
  }

  drop(event: CdkDragDrop<unknown>) {
    this.decisionData.moveAlternative(event.previousIndex, event.currentIndex);
  }

  openAlternativeSearch() {
    this.dialog
      .open<StarterAlternativeSearchModalComponent, null, string[]>(StarterAlternativeSearchModalComponent)
      .afterClosed()
      .subscribe(alternatives => {
        if (!alternatives) return;

        if (this.decisionData.alternatives.length > 0 && this.decisionData.alternatives.at(-1).name.length === 0) {
          this.decisionData.removeAlternative(this.decisionData.alternatives.length - 1);
        }

        alternatives.forEach(alternativeName => this.decisionData.addAlternative({ alternative: new Alternative(3, alternativeName) }));
      });
  }
}
