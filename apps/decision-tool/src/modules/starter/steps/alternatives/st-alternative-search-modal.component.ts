import { Component, ElementRef, ViewChild } from '@angular/core';

import { ModalModule } from '@entscheidungsnavi/widgets/modal/modal.module';
import { MatButtonModule } from '@angular/material/button';
import { MatDialogRef } from '@angular/material/dialog';
import { MatInputModule } from '@angular/material/input';
import { FormsModule } from '@angular/forms';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import { PopOverService } from '@entscheidungsnavi/widgets';
import { AlternativeSearchComponent } from '../../../alternatives/alternative-search/alternative-search.component';

@Component({
  template: `
    <dt-modal-content (closeClick)="discard()">
      <ng-template #title i18n>Zielfokussierte Alternativensuche</ng-template>
      <ng-template #content>
        <p i18n
          >Überlege in jedem Ziel, wie Du nur bezogen auf dieses Ziel ein gutes Ergebnis erreichen könntest. Anschließend kannst Du
          vielleicht einige Deiner Ideen kombinieren und so eine neue attraktive Handlungsalternative benennen.</p
        >
        <dt-alternative-search></dt-alternative-search>
        <div class="add-alternative-field-wrapper">
          <input
            class="browser-default mat-elevation-z1"
            placeholder="Eine neue kreative Alternative wäre…"
            [(ngModel)]="newAlternativeName"
            (blur)="addAlternative()"
            (keydown.enter)="addAlternative()"
          />
          @if (newAlternativeName.length > 0) {
            <button mat-icon-button><mat-icon>add</mat-icon></button>
          }
        </div>
        <ul class="new-alternatives-list">
          @for (alternative of newAlternatives; track alternative; let index = $index) {
            <li class="mat-elevation-z1">
              <span>{{ alternative }}</span>
              <button mat-icon-button (click)="deleteAlternative(index)"><mat-icon>delete</mat-icon></button>
            </li>
          }
        </ul>
      </ng-template>
      <ng-template #footer>
        <button mat-raised-button color="accent" (click)="confirm()" [disabled]="newAlternatives.length === 0" i18n
          >Neu formulierte Alternativen übernehmen</button
        >
        <button mat-raised-button color="accent-less" (click)="discard()" #discardButton i18n>Schließen</button>
      </ng-template>
    </dt-modal-content>
  `,
  styles: [
    //language=SCSS
    `
      @use 'variables' as dt;

      .add-alternative-field-wrapper {
        position: relative;

        input {
          display: block;
          width: 100%;
          height: 40px;

          padding: 0 40px 0 16px;
          margin-top: 12px;

          border: 1px solid lightgrey;
          background-color: white;
          border-radius: 5px;
          overflow: hidden;

          outline: none;

          &:focus {
            border-color: dt.$orange;
          }
        }

        button {
          position: absolute;
          right: 0;
          top: 0;
        }
      }

      .new-alternatives-list {
        list-style: none;

        li {
          margin-top: 8px;
          display: flex;
          align-items: center;
          padding-left: 8px;
          background-color: dt.$alternative;
          border: 1px solid dt.$blue;
          border-radius: 5px;

          span {
            flex: 1 1 auto;
            overflow: hidden;
            text-overflow: ellipsis;
            white-space: nowrap;
          }
        }
      }
    `,
  ],
  standalone: true,
  imports: [ModalModule, MatButtonModule, AlternativeSearchComponent, MatInputModule, FormsModule, MatIconModule, MatListModule],
})
export class StarterAlternativeSearchModalComponent {
  newAlternativeName = '';

  newAlternatives: string[] = [];

  @ViewChild('discardButton', { read: ElementRef }) discardButton: ElementRef<HTMLElement>;

  constructor(
    private dialogRef: MatDialogRef<StarterAlternativeSearchModalComponent, string[]>,
    private popOverService: PopOverService,
  ) {}

  addAlternative() {
    if (this.newAlternativeName.length > 0) {
      this.newAlternatives.push(this.newAlternativeName);
      this.newAlternativeName = '';
    }
  }

  deleteAlternative(position: number) {
    this.newAlternatives.splice(position, 1);
  }

  confirm() {
    this.dialogRef.close(this.newAlternatives);
  }

  async discard() {
    if (
      this.newAlternatives.length > 0 &&
      !(await this.popOverService.confirm(this.discardButton, $localize`Neuen Alternativen verwerfen?`, {
        confirmText: $localize`Verwerfen`,
        position: [{ originX: 'center', overlayX: 'center', originY: 'top', overlayY: 'bottom' }],
      }))
    ) {
      return;
    }

    this.dialogRef.close();
  }
}
