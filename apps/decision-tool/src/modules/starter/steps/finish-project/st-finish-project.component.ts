import { Component } from '@angular/core';

import { MatButtonModule } from '@angular/material/button';
import { RouterLink } from '@angular/router';
import { Alternative, DecisionData } from '@entscheidungsnavi/decision-data';
import { MatDialog } from '@angular/material/dialog';
import { MatIconModule } from '@angular/material/icon';
import { StarterHeadingComponent } from '../../common/st-heading.component';
import { ChartRankingComponent } from '../../../results/chart-ranking';
import { StarterPolarChartModalComponent } from './st-polar-chart-modal.component';
import { StarterFinishProjectModalComponent } from './st-finish-project-modal.component';

@Component({
  templateUrl: './st-finish-project.component.html',
  styleUrls: ['./st-finish-project.component.scss'],
  standalone: true,
  imports: [StarterHeadingComponent, MatButtonModule, RouterLink, ChartRankingComponent, MatIconModule],
})
export class StarterFinishProjectComponent {
  readonly alternativeUtilities: number[];
  bestAlternative: Alternative;

  constructor(
    protected decisionData: DecisionData,
    private dialog: MatDialog,
  ) {
    this.decisionData.resultSubstepProgress = Math.max(this.decisionData.resultSubstepProgress, 2);

    this.alternativeUtilities = this.decisionData.getAlternativeUtilities();

    const bestAlternativeIndex = this.alternativeUtilities.indexOf(Math.max(...this.alternativeUtilities));
    this.bestAlternative = this.decisionData.alternatives[bestAlternativeIndex];
  }

  openPolarChart() {
    this.dialog.open(StarterPolarChartModalComponent);
  }

  openFinishProject() {
    this.dialog.open(StarterFinishProjectModalComponent);
  }
}
