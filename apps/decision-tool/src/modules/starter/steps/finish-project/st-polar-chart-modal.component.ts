import { Component } from '@angular/core';

import { ModalModule } from '@entscheidungsnavi/widgets/modal/modal.module';
import { MatDialogRef } from '@angular/material/dialog';
import { MatButtonModule } from '@angular/material/button';
import { DecisionData } from '@entscheidungsnavi/decision-data';
import { PolarChartComponent, PolarChartItem } from '@entscheidungsnavi/widgets/charts';
import { MatRippleModule } from '@angular/material/core';

@Component({
  template: `
    <dt-modal-content (closeClick)="dialogRef.close()" [contentPadding]="false">
      <ng-template #title i18n>Pros und Kontras</ng-template>
      <ng-template #content>
        @switch (canShow) {
          @case (true) {
            <p i18n
              >In diesem Diagramm kannst Du erkennen, welche Alternative in welchem Ziel Vor- und Nachteile gegenüber den anderen hat.</p
            >
            <dt-polar-chart [dimensionLabels]="objectiveNames" [items]="polarChartData" [maxValue]="1"></dt-polar-chart>
          }
          @case (false) {
            <p class="italic" i18n>Das Pro und Kontra Diagramm kann nur mit mindestens drei Zielen angezeigt werden.</p>
          }
        }
      </ng-template>
      <ng-template #footer>
        <button mat-raised-button color="accent-less" (click)="dialogRef.close()" i18n>Schließen</button>
      </ng-template>
    </dt-modal-content>
  `,
  styles: [
    //language=SCSS
    `
      p {
        margin: 16px;
        text-align: center;
      }

      dt-polar-chart {
        padding: 16px;
      }
    `,
  ],
  standalone: true,
  imports: [ModalModule, MatButtonModule, PolarChartComponent, MatRippleModule],
})
export class StarterPolarChartModalComponent {
  readonly objectiveNames: string[];
  readonly polarChartData: PolarChartItem[];
  readonly canShow: boolean;

  constructor(
    protected dialogRef: MatDialogRef<StarterPolarChartModalComponent>,
    protected decisionData: DecisionData,
  ) {
    this.canShow = this.decisionData.objectives.length > 2;

    if (this.canShow) {
      const utilityMatrix = this.decisionData.getUtilityMatrix();
      this.objectiveNames = this.decisionData.objectives.map(objective => objective.name);
      this.polarChartData = this.decisionData.alternatives.map((_, alternativeIndex) => ({
        id: alternativeIndex,
        label: this.decisionData.alternatives[alternativeIndex].name,
        values: utilityMatrix[alternativeIndex],
      }));
    }
  }
}
