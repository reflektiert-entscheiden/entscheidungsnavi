import { Component } from '@angular/core';

import { ModalModule } from '@entscheidungsnavi/widgets/modal/modal.module';
import { MatDividerModule } from '@angular/material/divider';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { MatButtonModule } from '@angular/material/button';
import { AuthService } from '@entscheidungsnavi/api-client';
import { firstValueFrom } from 'rxjs';
import { LoginModalComponent } from '../../../../app/main/userarea';
import { ProjectService } from '../../../../app/data/project';
import { SaveAsModalComponent } from '../../../../app/navigation';
import { ModeTransitionService } from '../../../shared/mode-transition/mode-transition.service';

@Component({
  template: `
    <dt-modal-content (closeClick)="dialogRef.close()">
      <ng-template #title i18n>Projekt abschließen</ng-template>
      <ng-template #content>
        @switch (isProjectSaved) {
          @case (false) {
            <p i18n
              >Dein Projekt ist <b>nicht gespeichert</b>. Wir empfehlen Dir, Dein Projekt online zu speichern. Alternativ kannst Du es auch
              als Datei herunterladen.</p
            >
          }
          @case (true) {
            <p i18n
              >Du hast die Starter-Variante des Entscheidungsnavi abgeschlossen. Diese Variante ist bewusst sehr einfach gehalten. Wenn du
              Deine Entscheidung noch mehr reflektieren möchtest, empfehlen wir die umfangreichere Educational Variante.</p
            >
          }
        }
      </ng-template>
      <ng-template #footer>
        @switch (isProjectSaved) {
          @case (false) {
            <button mat-raised-button color="accent" (click)="saveOnline()" i18n>Online speichern</button>
            <button mat-raised-button color="accent" (click)="downloadFile()" i18n>Als Datei herunterladen</button>
          }
          @case (true) {
            <button mat-raised-button color="accent" (click)="switchToEducational()" i18n>In die Educational Variante wechseln</button>
          }
        }
        <button mat-raised-button color="accent-less" (click)="dialogRef.close()" i18n>Zurück zum Projekt</button>
      </ng-template>
    </dt-modal-content>
  `,
  styles: [
    //language=SCSS
    `
      p {
        margin-bottom: 0;
      }
    `,
  ],
  standalone: true,
  imports: [ModalModule, MatDividerModule, MatButtonModule],
})
export class StarterFinishProjectModalComponent {
  get isProjectSaved() {
    return this.projectService.getProject().isProjectSaved();
  }

  constructor(
    protected projectService: ProjectService,
    private authService: AuthService,
    protected dialogRef: MatDialogRef<StarterFinishProjectModalComponent>,
    private dialog: MatDialog,
    private modeTransitionService: ModeTransitionService,
  ) {}

  async saveOnline() {
    if (!this.authService.loggedIn) {
      const result = await firstValueFrom(this.dialog.open(LoginModalComponent).afterClosed());
      if (!result) return;
    }

    this.dialog.open(SaveAsModalComponent);
  }

  downloadFile() {
    this.projectService.getProject().exportFile();
  }

  async switchToEducational() {
    await this.modeTransitionService.transitionIntoEducational();
    this.dialogRef.close();
  }
}
