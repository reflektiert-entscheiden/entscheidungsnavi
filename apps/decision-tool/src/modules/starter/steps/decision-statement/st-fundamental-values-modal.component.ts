import { Component, ViewChild } from '@angular/core';

import { ModalModule } from '@entscheidungsnavi/widgets/modal/modal.module';
import { MatButtonModule } from '@angular/material/button';
import { MatDialogRef } from '@angular/material/dialog';
import { FundamentalValuesComponent } from '../../../decision-statement/fundamental-values/fundamental-values.component';

@Component({
  template: `
    <dt-modal-content [flexContent]="true" [contentPadding]="false" (closeClick)="dialogRef.close()">
      <ng-template #title i18n>Fundamentale Werte</ng-template>
      <ng-template #content>
        <dt-fundamental-values [allowEditing]="false"></dt-fundamental-values>
      </ng-template>
      <ng-template #footer>
        <button mat-raised-button color="accent" (click)="fundamentalValues.sortValues()" i18n>Alle Werte nach Wichtigkeit ordnen</button>
        <button mat-raised-button color="accent" (click)="dialogRef.close()" i18n>Zurück zur Entscheidungsfrage</button>
      </ng-template>
    </dt-modal-content>
  `,
  styles: [
    //language=SCSS
    `
      dt-fundamental-values {
        flex: 0 1 auto;
      }
    `,
  ],
  standalone: true,
  imports: [ModalModule, MatButtonModule, FundamentalValuesComponent],
})
export class StarterFundamentalValuesModalComponent {
  @ViewChild(FundamentalValuesComponent) fundamentalValues: FundamentalValuesComponent;

  constructor(protected dialogRef: MatDialogRef<StarterFundamentalValuesModalComponent>) {}
}
