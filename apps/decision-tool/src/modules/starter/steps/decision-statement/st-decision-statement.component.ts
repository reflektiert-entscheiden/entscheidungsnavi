import { Component } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { RouterLink } from '@angular/router';
import { DecisionData } from '@entscheidungsnavi/decision-data';
import { MatDialog } from '@angular/material/dialog';
import { SelectLineComponent } from '@entscheidungsnavi/widgets/select';
import { StarterHeadingComponent } from '../../common/st-heading.component';
import { StarterHelpExpansionPanelComponent } from '../../common/st-help-expansion-panel.component';
import { StarterNavigationButtonsComponent } from '../../common/st-navigation-buttons.component';
import { StarterQuickstartQuestionsModalComponent } from './st-quickstart-questions-modal.component';
import { StarterFundamentalValuesModalComponent } from './st-fundamental-values-modal.component';

@Component({
  templateUrl: 'st-decision-statement.component.html',
  styleUrls: ['st-decision-statement.component.scss'],
  standalone: true,
  imports: [
    StarterHeadingComponent,
    MatButtonModule,
    StarterHelpExpansionPanelComponent,
    RouterLink,
    SelectLineComponent,
    StarterNavigationButtonsComponent,
  ],
})
export class StarterDecisionStatementComponent {
  initialDecisionStatement = this.decisionData.decisionStatement.statement;

  get isValid() {
    return this.decisionData.validateStep('decisionStatement');
  }

  constructor(
    protected decisionData: DecisionData,
    private dialog: MatDialog,
  ) {
    decisionData.decisionStatement.subStepProgression = 'RESULT';
  }

  updateDecisionStatement(event: Event) {
    this.decisionData.decisionStatement.statement = (event.target as HTMLDivElement).innerText.trim();
  }

  openExamples() {
    this.dialog
      .open<StarterQuickstartQuestionsModalComponent, null, string>(StarterQuickstartQuestionsModalComponent)
      .afterClosed()
      .subscribe(result => {
        if (result) {
          this.decisionData.decisionStatement.statement = result;
          this.initialDecisionStatement = result;
        }
      });
  }

  openFundamentalValues() {
    this.dialog.open(StarterFundamentalValuesModalComponent);
  }
}
