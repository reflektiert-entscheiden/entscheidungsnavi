import { Component, Inject, LOCALE_ID } from '@angular/core';

import { ModalModule } from '@entscheidungsnavi/widgets/modal/modal.module';
import { MatDialogRef } from '@angular/material/dialog';
import { MatButtonModule } from '@angular/material/button';
import { QuickstartQuestionDto, QuickstartService } from '@entscheidungsnavi/api-client';
import { takeUntilDestroyed } from '@angular/core/rxjs-interop';
import { LocalizedStringPipe, localizeString, Tag, TagInputComponent } from '@entscheidungsnavi/widgets';
import { catchError, EMPTY, forkJoin } from 'rxjs';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatInputModule } from '@angular/material/input';
import { MatRadioModule } from '@angular/material/radio';
import { MatRippleModule } from '@angular/material/core';
import { MatDividerModule } from '@angular/material/divider';
import { StarterContextService } from '../../common/st-context-service.service';

@Component({
  template: `
    <dt-modal-content [flexContent]="true" [contentPadding]="false" (closeClick)="dialogRef.close()">
      <ng-template #title i18n>Beispiele für Entscheidungsfragen</ng-template>
      <ng-template #content>
        @switch (state) {
          @case ('loading') {
            <mat-spinner></mat-spinner>
          }
          @case ('error') {
            <mat-error class="dt-error" i18n
              >Beim Laden der Beispiele ist ein Fehler aufgetreten. Bitte versuche es später erneut.</mat-error
            >
          }
          @case ('loaded') {
            <dt-tag-input [allTags]="tags" [(selectedTagIds)]="tagIdFilter" (selectedTagIdsChange)="onTagFilterChange()" i18n
              >Nach Tags filtern</dt-tag-input
            >
            <mat-divider></mat-divider>
            <div class="quickstart-question-list">
              @for (question of filteredQuestions; track question) {
                <div class="question" (click)="selectedQuickstartQuestion = question" matRipple>
                  <mat-radio-button [checked]="question === selectedQuickstartQuestion"></mat-radio-button>
                  <div>{{ question.name | localizedString }}</div>
                </div>
              }
            </div>
          }
        }
      </ng-template>
      <ng-template #footer>
        <button mat-raised-button color="accent" [disabled]="selectedQuickstartQuestion == null" (click)="confirm()" i18n
          >Ausgewählte Entscheidungsfrage übernehmen</button
        >
        <button mat-raised-button color="accent-less" (click)="dialogRef.close()" i18n>Schließen</button>
      </ng-template>
    </dt-modal-content>
  `,
  styles: [
    //language=SCSS
    `
      mat-error {
        max-width: fit-content;
        margin: 16px auto;
      }

      mat-spinner {
        display: block;
        margin: 32px auto;
      }

      dt-tag-input {
        margin: 16px;
      }

      .quickstart-question-list {
        padding: 12px 0;
        flex: 0 1 auto;
        overflow-y: auto;
      }

      .question {
        padding: 4px 16px;

        display: flex;
        align-items: center;
        gap: 8px;

        cursor: pointer;

        &:hover {
          background-color: rgba(0, 0, 0, 0.1);
        }
      }
    `,
  ],
  standalone: true,
  imports: [
    ModalModule,
    MatButtonModule,
    MatProgressSpinnerModule,
    TagInputComponent,
    MatInputModule,
    MatRadioModule,
    MatRippleModule,
    LocalizedStringPipe,
    MatDividerModule,
  ],
})
export class StarterQuickstartQuestionsModalComponent {
  hasError = false;
  questions: QuickstartQuestionDto[];
  tags: Tag[];

  tagIdFilter: string[] = [];
  filteredQuestions: QuickstartQuestionDto[];

  selectedQuickstartQuestion: QuickstartQuestionDto;

  get state(): 'loading' | 'loaded' | 'error' {
    if (this.hasError) return 'error';
    else if (this.questions) return 'loaded';
    else return 'loading';
  }

  constructor(
    protected dialogRef: MatDialogRef<StarterQuickstartQuestionsModalComponent, string>,
    private quickstartService: QuickstartService,
    private starterContextService: StarterContextService,
    @Inject(LOCALE_ID) private locale: string,
  ) {
    forkJoin([this.quickstartService.getQuestions(), this.quickstartService.getTags()])
      .pipe(takeUntilDestroyed())
      .subscribe({
        next: ([questions, tags]) => {
          this.questions = questions;
          this.tags = tags;

          this.tagIdFilter = this.starterContextService.filterTagIds.slice();
          this.updateFilteredQuestions();

          // If we have too few questions with our tag filter, we reset it
          if (this.filteredQuestions.length < 2) {
            this.tagIdFilter = [];
            this.updateFilteredQuestions();
          }
        },
        error: () => (this.hasError = true),
      });
  }

  onTagFilterChange() {
    this.starterContextService.filterTagIds = this.tagIdFilter.slice();
    this.updateFilteredQuestions();
  }

  updateFilteredQuestions() {
    this.filteredQuestions = this.questions.filter(question => this.tagIdFilter.every(tag => question.tags.includes(tag)));
    this.updateTagProductivity();
  }

  private updateTagProductivity() {
    this.tags.forEach(tag => {
      tag.isProductive = this.filteredQuestions.some(question => question.tags.includes(tag.id));
    });
  }

  confirm() {
    this.quickstartService
      .trackQuestionUsage(this.selectedQuickstartQuestion.id)
      .pipe(catchError(() => EMPTY))
      .subscribe();
    this.dialogRef.close(localizeString(this.selectedQuickstartQuestion.name, this.locale));
  }
}
