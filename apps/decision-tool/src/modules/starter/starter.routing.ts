import { Route } from '@angular/router';
import { NAVI_STEP_ORDER } from '@entscheidungsnavi/decision-data';
import { StarterMainComponent } from './main/st-main.component';
import { starterStepGuard } from './starter-step.guard';
import { STARTER_STEPS } from './starter-steps';

export default [
  { path: '', component: StarterMainComponent },
  ...NAVI_STEP_ORDER.map(stepName => ({
    path: STARTER_STEPS[stepName].link,
    component: STARTER_STEPS[stepName].component,
    canActivate: [starterStepGuard(stepName)],
  })),
] satisfies Route[];
