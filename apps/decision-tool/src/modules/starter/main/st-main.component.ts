import { Component } from '@angular/core';

import { MatIconModule } from '@angular/material/icon';
import { MatRippleModule } from '@angular/material/core';
import { MatButtonModule } from '@angular/material/button';
import { RouterLink } from '@angular/router';
import { DecisionData, NAVI_STEP_ORDER } from '@entscheidungsnavi/decision-data';
import { AsyncPipe } from '@angular/common';
import { STARTER_STEPS } from '../starter-steps';
import { StarterHeading, StarterHeadingComponent } from '../common/st-heading.component';
import { AutoSaveService } from '../../../app/data/project/auto-save.service';

@Component({
  templateUrl: 'st-main.component.html',
  styleUrls: ['st-main.component.scss'],
  standalone: true,
  imports: [MatIconModule, MatRippleModule, MatButtonModule, RouterLink, StarterHeadingComponent, AsyncPipe],
})
export class StarterMainComponent {
  readonly heading: StarterHeading = {
    title: $localize`Mein Weg zur reflektierten Entscheidung`,
    task: $localize`Eine reflektierte Entscheidung umfasst die folgenden Schritte.`,
  };

  readonly orderedSteps = NAVI_STEP_ORDER.map(step => ({ ...STARTER_STEPS[step], id: step }));
  readonly currentStepIndex: number;

  constructor(
    decisionData: DecisionData,
    protected autoSaveService: AutoSaveService,
  ) {
    const firstStepWithErrors = decisionData.getFirstStepWithErrors();
    this.currentStepIndex = firstStepWithErrors ? NAVI_STEP_ORDER.indexOf(firstStepWithErrors) : NAVI_STEP_ORDER.length - 1;
  }
}
