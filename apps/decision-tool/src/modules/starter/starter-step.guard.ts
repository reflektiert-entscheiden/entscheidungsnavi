import { DecisionData, NAVI_STEP_ORDER, NaviStep } from '@entscheidungsnavi/decision-data';
import { CanActivateFn, Router } from '@angular/router';
import { inject } from '@angular/core';
import { getStartUrlForProjectMode } from '../shared/navigation/navigation';

export function starterStepGuard(step: NaviStep): CanActivateFn {
  return () => {
    const decisionData = inject(DecisionData);

    const stepIndex = NAVI_STEP_ORDER.indexOf(step);
    if (stepIndex === 0) return true;

    const isValid = decisionData.getFirstStepWithErrors(NAVI_STEP_ORDER[stepIndex - 1]) == null;
    if (isValid) return true;

    return inject(Router).parseUrl(getStartUrlForProjectMode('starter'));
  };
}
