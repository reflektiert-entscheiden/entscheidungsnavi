import { NaviStep } from '@entscheidungsnavi/decision-data';
import { Route } from '@angular/router';
import { StarterDecisionStatementComponent } from './steps/decision-statement/st-decision-statement.component';
import { StarterObjectivesComponent } from './steps/objectives/st-objectives.component';
import { StarterAlternativesComponent } from './steps/alternatives/st-alternatives.component';
import { StarterImpactModelComponent } from './steps/impact-model/st-impact-model.component';
import { StarterObjectiveWeightingComponent } from './steps/objective-weighting/st-objective-weighting.component';
import { StarterFinishProjectComponent } from './steps/finish-project/st-finish-project.component';

export interface StarterStepMetadata {
  title: string;
  question: string;
  task: string;
  icon: string;
  editButtonText: string;
  link: string;
  component: Route['component'];
}

export const STARTER_STEPS: { [key in NaviStep]: Readonly<StarterStepMetadata> } = {
  decisionStatement: {
    title: $localize`Meine Entscheidungsfrage`,
    question: $localize`Was will ich genau entscheiden?`,
    task: $localize`Formuliere möglichst exakt Deine Entscheidungsfrage.`,
    icon: 'contact_support',
    editButtonText: $localize`Starten`,
    link: 'decision-statement',
    component: StarterDecisionStatementComponent,
  },
  objectives: {
    title: $localize`Meine Ziele`,
    question: $localize`Was will ich erreichen bzw. auf was kommt es mir im Kern an?`,
    task: $localize`Benenne alle Deine Ziele.
    Achte darauf, dass sich Deine Ziele nicht inhaltlich überlappen und Deine Liste frei von Redundanzen ist.`,
    icon: 'track_changes',
    editButtonText: $localize`Fortfahren`,
    link: 'objectives',
    component: StarterObjectivesComponent,
  },
  alternatives: {
    title: $localize`Meine Alternativen`,
    question: $localize`Was könnte ich theoretisch alles machen?`,
    task: $localize`Führe alle Deine Alternativen auf.`,
    icon: 'alt_route',
    editButtonText: $localize`Fortfahren`,
    link: 'alternatives',
    component: StarterAlternativesComponent,
  },
  impactModel: {
    title: $localize`Meine Einschätzungen`,
    question: $localize`Was würde passieren, wenn ich mich für einen bestimmten Weg entscheide?`,
    task: $localize`Schätze ein, wie gut Deine Alternativen in den Zielen sind.
    Verteile einfach Punktwerte von 0 (schlechteste Ausprägung) bis 10 (beste Ausprägung).`,
    icon: 'view_compact',
    editButtonText: $localize`Fortfahren`,
    link: 'consequences',
    component: StarterImpactModelComponent,
  },
  results: {
    title: $localize`Meine Abwägung`,
    question: $localize`Was ist mir besonders wichtig und auf was könnte ich eher verzichten?`,
    task: $localize`Bewerte die Wichtigkeit Deiner Ziele.`,
    icon: 'assessment',
    editButtonText: $localize`Fortfahren`,
    link: 'objective-weighting',
    component: StarterObjectiveWeightingComponent,
  },
  finishProject: {
    title: $localize`Meine reflektierte Entscheidung`,
    question: $localize`Was habe ich entschieden und warum?`,
    task: $localize`Geschafft! Jetzt musst Du Deine Entscheidung nur noch umsetzen.`,
    icon: 'fact_check',
    editButtonText: $localize`Einsehen`,
    link: 'finish-project',
    component: StarterFinishProjectComponent,
  },
};
