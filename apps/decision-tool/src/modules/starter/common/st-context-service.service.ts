import { Injectable } from '@angular/core';
import { ProjectService } from '../../../app/data/project';

@Injectable({ providedIn: 'root' })
export class StarterContextService {
  // The tag id filter for the current starter project
  filterTagIds: string[];

  constructor(projectService: ProjectService) {
    projectService.project$.subscribe(() => this.reset());
  }

  private reset() {
    this.filterTagIds = [];
  }
}
