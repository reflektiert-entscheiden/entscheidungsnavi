import { Component, Input } from '@angular/core';

import { NaviStep } from '@entscheidungsnavi/decision-data';
import { STARTER_STEPS } from '../starter-steps';

export interface StarterHeading {
  title: string;
  task: string;
}

@Component({
  selector: 'dt-starter-heading',
  template: `
    <h1>{{ displayedHeading.title }}</h1>
    <div>{{ displayedHeading.task }}</div>
  `,
  styles: [
    //language=SCSS
    `
      @use 'variables' as dt;

      $title-padding: 40px;

      :host {
        display: block;
        text-align: center;
        margin: calc($title-padding - dt.$starter-padding) 24px $title-padding;
      }

      h1 {
        font-weight: 500;
      }
    `,
  ],
  standalone: true,
  imports: [],
})
export class StarterHeadingComponent {
  @Input({ required: true }) heading: NaviStep | StarterHeading;

  get displayedHeading() {
    return typeof this.heading === 'string' ? STARTER_STEPS[this.heading] : this.heading;
  }
}
