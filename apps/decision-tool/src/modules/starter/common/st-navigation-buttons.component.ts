import { Component, Input } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { RouterLink } from '@angular/router';
import { DecisionData, NaviStep } from '@entscheidungsnavi/decision-data';
import { MatTooltipModule } from '@angular/material/tooltip';

import { MatIconModule } from '@angular/material/icon';

@Component({
  selector: 'dt-starter-navigation-buttons',
  template: `
    <button mat-raised-button color="accent-less" routerLink="..">
      <mat-icon>arrow_back</mat-icon>
      <ng-container i18n>Zurück zur Übersicht</ng-container>
    </button>
    <div [matTooltip]="invalidTooltip" [matTooltipDisabled]="isValid || !invalidTooltip">
      <button mat-raised-button [disabled]="!isValid" color="accent" routerLink=".." data-cy="starter-finish-button">
        <mat-icon>done</mat-icon>
        <ng-container i18n>Schritt abschließen</ng-container>
      </button>
    </div>
  `,
  styles: [
    `
      :host {
        display: flex;
        justify-content: center;
        flex-wrap: wrap;
        gap: 8px 4px;

        margin-top: 24px;
      }
    `,
  ],
  imports: [MatButtonModule, RouterLink, MatTooltipModule, MatIconModule],
  standalone: true,
})
export class StarterNavigationButtonsComponent {
  @Input({ required: true }) step: NaviStep;
  @Input() invalidTooltip: string;

  get isValid() {
    return this.decisionData.validateStep(this.step)[0];
  }

  constructor(private decisionData: DecisionData) {}
}
