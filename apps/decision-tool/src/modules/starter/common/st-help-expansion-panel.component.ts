import { Component, HostBinding } from '@angular/core';

import { MatIconModule } from '@angular/material/icon';
import { animate, state, style, transition, trigger } from '@angular/animations';
import { MatButtonModule } from '@angular/material/button';
import { MatRippleModule } from '@angular/material/core';

@Component({
  selector: 'dt-starter-help-expansion-panel',
  template: `
    <button class="header" (click)="expanded = !expanded" matRipple>
      <mat-icon>help</mat-icon>
      <div class="text-content" i18n>Hilfe zu diesem Schritt</div>
      <mat-icon class="expand-collapse-icon">expand_more</mat-icon>
    </button>
    <div class="content-wrapper" [@contentExpanded]="expanded">
      <div class="content">
        <ng-content></ng-content>
        <div class="buttons">
          <ng-content select="button"></ng-content>
        </div>
      </div>
    </div>
  `,
  styles: [
    //language=SCSS
    `
      @use 'variables' as dt;
      @use '@angular/material' as mat;

      :host {
        @include mat.elevation(4);

        display: block;
        margin: 24px auto;
        border-radius: 5px;
        overflow: hidden;
      }

      .header {
        padding: 0 16px;
        width: 100%;
        height: 48px;

        transition: height 0.3s ease-in-out;

        display: flex;
        align-items: center;
        text-align: start;
        gap: 12px;

        background-color: dt.$help-blue-lighten-8;

        .text-content {
          flex: 1 0 auto;
          font-weight: 500;
        }
      }

      .expand-collapse-icon {
        transition: transform 0.3s ease-in-out;
      }

      .content-wrapper {
        overflow: hidden;
      }

      .content {
        padding: 16px 24px;
        background-color: white;
        border-top: 1px solid #cbcbcb;
      }

      .buttons {
        display: flex;
        justify-content: center;
        flex-wrap: wrap;
        gap: 8px 4px;
      }

      :host.expanded {
        .header {
          height: 56px;
        }

        .expand-collapse-icon {
          transform: rotate(-180deg);
        }
      }
    `,
  ],
  standalone: true,
  imports: [MatIconModule, MatButtonModule, MatRippleModule],
  animations: [
    trigger('contentExpanded', [
      state('true', style({ height: '*' })),
      state('false', style({ height: 0 })),
      transition('true <=> false', [animate('300ms ease-in-out')]),
    ]),
  ],
})
export class StarterHelpExpansionPanelComponent {
  @HostBinding('class.dt-starter-limit-width') readonly limitWidth = true;
  @HostBinding('class.expanded') expanded = false;
}
