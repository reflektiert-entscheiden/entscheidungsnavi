import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { DecisionData } from '@entscheidungsnavi/decision-data';

export interface DeleteObjectiveModalData {
  objectiveIndex: number;
  isHierarchy: boolean;
}

@Component({
  templateUrl: './delete-objective-modal.component.html',
  styleUrls: ['./delete-objective-modal.component.scss'],
})
export class DeleteObjectiveModalComponent {
  get name() {
    return this.decisionData.objectives[this.data.objectiveIndex].name;
  }

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: DeleteObjectiveModalData,
    private dialogRef: MatDialogRef<DeleteObjectiveModalComponent>,
    private decisionData: DecisionData,
  ) {}

  close(confirmDelete = false) {
    this.dialogRef.close(confirmDelete);
  }
}
