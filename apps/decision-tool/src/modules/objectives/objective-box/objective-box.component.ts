import { Component, ElementRef, EventEmitter, Input, OnChanges, Output, SimpleChanges } from '@angular/core';
import { Objective } from '@entscheidungsnavi/decision-data';
import { PopOverService, SimpleHierarchyNode } from '@entscheidungsnavi/widgets';
import { Tree } from '@entscheidungsnavi/tools';
import { TransferService } from '../../../app/transfer';
import { AppSettingsService } from '../../../app/data/app-settings.service';

@Component({
  selector: 'dt-objective-box',
  templateUrl: './objective-box.component.html',
  styleUrls: ['./objective-box.component.scss'],
})
export class ObjectiveBoxComponent implements OnChanges {
  @Input() objective: Objective;

  @Input() readonly = false;

  @Input() expanded: boolean;
  @Output() expandedChange = new EventEmitter<boolean>();

  @Output() deleteClick = new EventEmitter<void>();

  aspectTree: Tree<SimpleHierarchyNode>;

  get showSendButton() {
    return this.appSettings.showSendButtons;
  }

  constructor(
    private appSettings: AppSettingsService,
    private transferService: TransferService,
    private popOverService: PopOverService,
  ) {}

  ngOnChanges(changes: SimpleChanges) {
    if ('objective' in changes) {
      this.createTree();
    }
  }

  sendObjective(element: ElementRef<HTMLElement>) {
    this.transferService.broadcastObjective(this.objective).then(() => {
      this.popOverService.whistle(element, $localize`Ziel gesendet!`);
    });
  }

  private createTree() {
    this.aspectTree = new Tree<SimpleHierarchyNode>(
      { name: '', style: { 'background-color': '#5d666f', color: 'white' } },
      this.objective.aspects.children.map(child => child.map(node => ({ name: node.name, comment: node.comment }))),
    );
  }
}
