import { Component } from '@angular/core';
import { LanguageService } from '../../../../../app/data/language.service';
import { YoutubeVideosService } from '../../../../../app/data/youtube-videos.service';

@Component({
  templateUrl: './help2.component.html',
  styleUrls: ['../../../../hints.scss'],
})
export class Help2Component {
  constructor(
    protected languageService: LanguageService,
    protected videosService: YoutubeVideosService,
  ) {}
}
