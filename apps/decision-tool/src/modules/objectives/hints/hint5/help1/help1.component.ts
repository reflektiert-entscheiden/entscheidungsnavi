import { Component } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ObjectiveHierarchyShortcutsComponent } from '../../../objective-hierarchy-shortcuts/objective-hierarchy-shortcuts.component';

@Component({
  templateUrl: './help1.component.html',
  styleUrls: ['../../../../hints.scss'],
})
export class Help1Component {
  constructor(private dialog: MatDialog) {}

  openShortcuts() {
    this.dialog.open(ObjectiveHierarchyShortcutsComponent);
  }
}
