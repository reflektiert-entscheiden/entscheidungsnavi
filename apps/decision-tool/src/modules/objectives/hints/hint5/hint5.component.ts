import { Component, Injector } from '@angular/core';
import { helpPage } from '../../../../app/help/help';
import { HelpBackgroundComponent } from '../help-background/help-background.component';
import { AbstractObjectiveHintComponent } from '../objective-hint.component';
import { DisplayAtMaxWidth } from '../../../../app/interfaces/display-at-max-width';
import { Help1Component } from './help1/help1.component';
import { Help2Component } from './help2/help2.component';

@Component({
  templateUrl: './hint5.component.html',
  styleUrls: ['../hint3-5.component.scss'],
})
@DisplayAtMaxWidth
export class ZieleHint5Component extends AbstractObjectiveHintComponent {
  get helpMenu() {
    return [
      helpPage()
        .name($localize`So funktioniert's`)
        .component(Help1Component)
        .build(),
      helpPage()
        .name($localize`Weitere Hinweise`)
        .component(Help2Component)
        .build(),
      helpPage()
        .name($localize`Hintergrundwissen zum Schritt 2`)
        .component(HelpBackgroundComponent)
        .build(),
      helpPage()
        .name($localize`Videos`)
        .youtube(1)
        .build(),
    ];
  }

  constructor(injector: Injector) {
    super(injector);
    this.pageKey = 5;
  }
}
