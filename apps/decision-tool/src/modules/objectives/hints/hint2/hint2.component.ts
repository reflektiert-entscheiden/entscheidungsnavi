import { Component, Injector } from '@angular/core';
import { helpPage } from '../../../../app/help/help';
import { Help1Component } from '../hint1/help1/help1.component';
import { HelpBackgroundComponent } from '../help-background/help-background.component';
import { AbstractObjectiveHintComponent } from '../objective-hint.component';
import { Help2Component } from '../hint1/help2/help2.component';

@Component({
  templateUrl: './hint2.component.html',
  styles: [''],
})
export class ZieleHint2Component extends AbstractObjectiveHintComponent {
  get helpMenu() {
    return [
      helpPage()
        .name($localize`So funktioniert's`)
        .component(Help1Component)
        .build(),
      helpPage()
        .name($localize`Weitere Hinweise`)
        .component(Help2Component)
        .build(),
      helpPage()
        .name($localize`Hintergrundwissen zum Schritt 2`)
        .component(HelpBackgroundComponent)
        .build(),
      helpPage()
        .name($localize`Videos`)
        .youtube(1)
        .build(),
    ];
  }

  constructor(injector: Injector) {
    super(injector);
    this.pageKey = 2;
  }
}
