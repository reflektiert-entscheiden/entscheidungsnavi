import { Directive, Injector, OnInit } from '@angular/core';
import { DecisionData, NaviSubStep, ObjectiveElement, OBJECTIVES_STEPS } from '@entscheidungsnavi/decision-data';
import { Tree } from '@entscheidungsnavi/tools';
import { EducationalNavigationService } from '../../shared/navigation/educational-navigation.service';
import { HelpMenu, HelpMenuProvider } from '../../../app/help/help';
import { Navigation, NavLine, navLineElement } from '../../shared/navline';
import { DecisionDataState } from '../../shared/decision-data-state';

@Directive()
// eslint-disable-next-line @angular-eslint/directive-class-suffix
export abstract class AbstractObjectiveHintComponent implements OnInit, Navigation, HelpMenuProvider {
  navLine = new NavLine({
    left: [
      navLineElement()
        .back(() => (this.pageKey === 1 ? '/decisionstatement' : `/objectives/steps/${this.pageKey - 1}`))
        .build(),
    ],
    right: [
      navLineElement()
        .continue(() => (this.pageKey === 5 ? '/objectives' : `/objectives/steps/${this.pageKey + 1}`))
        .build(),
    ],
  });

  abstract helpMenu: HelpMenu;
  pageKey: number; // 1-based
  decisionData: DecisionData;

  protected currentProgressService: EducationalNavigationService;
  aspects: string[];
  aspectsFromTree: ObjectiveElement[];

  createdAspects = false;

  protected decisionDataState: DecisionDataState;

  get naviSubStep(): NaviSubStep {
    return { step: 'objectives', subStepIndex: this.pageKey - 1 };
  }

  protected constructor(injector: Injector) {
    this.currentProgressService = injector.get(EducationalNavigationService);
    this.decisionData = injector.get(DecisionData);
    this.decisionDataState = injector.get(DecisionDataState);
  }

  ngOnInit() {
    if (this.pageKey > OBJECTIVES_STEPS.indexOf(this.decisionData.objectiveAspects.subStepProgression) + 1) {
      this.decisionData.objectiveAspects.subStepProgression = OBJECTIVES_STEPS[this.pageKey - 1];
    }

    this.currentProgressService.updateProgress();

    if (
      this.decisionData.objectiveAspects.listOfAspects.length === 0 &&
      this.decisionData.objectiveAspects.getAspectTree($localize`Zielhierarchie`).children.length === 0 &&
      this.pageKey === 1
    ) {
      this.decisionData.objectiveAspects.generateInitialAspects();
      this.createdAspects = true;
    }

    const tree = this.decisionData.objectiveAspects.getAspectTree($localize`Zielhierarchie`);

    this.aspectsFromTree = [];
    for (const child of tree.children) {
      this.getAspectsFromTree(child);
    }
  }

  getAspectsFromTree(tree: Tree<ObjectiveElement>) {
    this.aspectsFromTree.push(tree.value);
    for (const child of tree.children) {
      this.getAspectsFromTree(child);
    }
  }
}
