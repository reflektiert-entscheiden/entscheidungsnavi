import { ChangeDetectionStrategy, Component, Input, OnInit } from '@angular/core';
import { AttachedObjectiveData, DecisionData } from '@entscheidungsnavi/decision-data';

@Component({
  selector: 'dt-attached-objective-data',
  template: ` <ul class="dt-plain-list">
    @for (id of data; track id) {
      <li>
        @switch (id) {
          @case ('ideas') {
            <span i18n>Ideen für Alternativen</span>
          }
          @case ('scale') {
            <span i18n>Messskalen</span>
          }
          @case ('outcomes') {
            <span i18n>Einträge im Wirkungsmodell</span>
          }
          @case ('weights') {
            <span i18n>Zielgewichtung</span>
          }
          @case ('utility') {
            <span i18n>Benutzerdefinierte Nutzenfunktionen</span>
          }
          @default {
            <span>
              {{ id }}
            </span>
          }
        }
      </li>
    }
  </ul>`,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AttachedObjectiveDataComponent implements OnInit {
  @Input()
  objectiveIndex: number;

  data: AttachedObjectiveData[];

  constructor(private decisionData: DecisionData) {}

  ngOnInit() {
    this.data = this.decisionData.getAttachedObjectiveData(this.objectiveIndex);
  }
}
