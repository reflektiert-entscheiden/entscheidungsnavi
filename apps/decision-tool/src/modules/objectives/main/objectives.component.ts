import { Component, TemplateRef, ViewChild } from '@angular/core';
import { DecisionData, Objective, OBJECTIVES_STEPS } from '@entscheidungsnavi/decision-data';
import { takeUntilDestroyed } from '@angular/core/rxjs-interop';
import { PersistentSetting, PersistentSettingParent } from '@entscheidungsnavi/widgets';
import { ExplanationService } from '../../shared/decision-quality';
import { EducationalNavigationService } from '../../shared/navigation/educational-navigation.service';
import { HelpMenuProvider, helpPage } from '../../../app/help/help';
import { TransferService } from '../../../app/transfer';
import { HelpBackgroundComponent } from '../hints/help-background/help-background.component';
import { DisplayAtMaxWidth } from '../../../app/interfaces/display-at-max-width';
import { ObjectiveListComponent } from '../objective-list/objective-list.component';
import { Navigation, NavLine, navLineElement } from '../../shared/navline';

@Component({
  templateUrl: './objectives.component.html',
  styleUrls: ['./objectives.component.scss', '../../hints.scss'],
})
@DisplayAtMaxWidth
@PersistentSettingParent('Objectives')
export class ObjectivesComponent implements Navigation, HelpMenuProvider {
  navLine = new NavLine({
    left: [navLineElement().back('/objectives/steps/5').build()],
    middle: [this.explanationService.generateAssessmentButton('MEANINGFUL_RELIABLE_INFORMATION')],
    right: [navLineElement().continue('/alternatives/steps/1').build()],
  });

  helpMenu = [
    helpPage()
      .name($localize`Ergebnisseite`)
      .template(() => this.helpTemplate)
      .build(),
    helpPage()
      .name($localize`Hintergrundwissen zum Schritt 2`)
      .component(HelpBackgroundComponent)
      .build(),
    helpPage()
      .name($localize`Videos`)
      .youtube(1)
      .build(),
  ];

  @PersistentSetting('project')
  displayVariant: 'list' | 'tree' = 'tree';

  allExpanded = true;
  allCollapsed = false;

  @ViewChild('helpTemplate') helpTemplate: TemplateRef<any>;
  @ViewChild(ObjectiveListComponent) objectiveList: ObjectiveListComponent;

  constructor(
    private decisionData: DecisionData,
    private currentProgressService: EducationalNavigationService,
    private explanationService: ExplanationService,
    transferService: TransferService,
  ) {
    transferService.onReceived$.pipe(takeUntilDestroyed()).subscribe(receivedObject => {
      if (receivedObject instanceof Objective) {
        transferService.openNotification(receivedObject);
      }
    });

    this.decisionData.objectiveAspects.subStepProgression = OBJECTIVES_STEPS[OBJECTIVES_STEPS.length - 1];
    this.currentProgressService.updateProgress();

    if (this.decisionData.objectives.length === 0) {
      this.decisionData.addObjective();
    }
  }

  setDisplay(newDisplay: 'list' | 'tree') {
    this.displayVariant = newDisplay;
  }

  expandAll() {
    this.objectiveList.expandAll();
  }

  collapseAll() {
    this.objectiveList.collapseAll();
  }

  updateExpansionStatus(expansionstatus: { allExpanded: boolean; allCollapsed: boolean }) {
    this.allExpanded = expansionstatus.allExpanded;
    this.allCollapsed = expansionstatus.allCollapsed;
  }
}
