import { Component, ElementRef, EventEmitter, Input, OnInit, Output, TemplateRef, ViewChild } from '@angular/core';
import { DecisionData, ObjectiveElement } from '@entscheidungsnavi/decision-data';
import { MatDialog } from '@angular/material/dialog';
import { OnDestroyObservable, Tree } from '@entscheidungsnavi/tools';
import { Observable } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { PersistentSetting, PersistentSettingParent } from '@entscheidungsnavi/widgets';
import { DecisionDataState } from '../../shared/decision-data-state';

@Component({
  selector: 'dt-objective-aspect-list',
  templateUrl: './objective-aspect-list.component.html',
  styleUrls: ['./objective-aspect-list.component.scss'],
})
@PersistentSettingParent('ObjectiveAspectList')
export class ObjectiveAspectListComponent implements OnInit {
  @OnDestroyObservable()
  private onDestroy$: Observable<void>;

  listOfAspects: ObjectiveElement[];
  tree: Tree<ObjectiveElement>;
  aspectsFromTree: ObjectiveElement[] = [];
  listOfDeletedAspects: ObjectiveElement[];

  @PersistentSetting()
  filter: 'all' | 'onlyCurrent' | 'currentAndPrevious' = 'all';

  @Input()
  pageKey: number;

  @Output()
  dirty = new EventEmitter();

  @ViewChild('newAspect', { static: true })
  newAspectInputElement: ElementRef<HTMLInputElement>;

  @ViewChild('confirmDeleteModal', { static: true })
  confirmDeleteModal: TemplateRef<any>;

  newAspectValue: string;

  private dragIndex: number;
  private oldDragIndex: number;
  private enterTarget: EventTarget = null;

  mergedAspectsWithIndexes: { idx: number; isInTree: boolean; isDeleted: boolean; aspect: ObjectiveElement }[];

  get readonly() {
    return this.decisionDataState.isProjectReadonly;
  }

  constructor(
    protected decisionData: DecisionData,
    private dialog: MatDialog,
    private decisionDataState: DecisionDataState,
  ) {}

  ngOnInit() {
    this.listOfAspects = this.decisionData.objectiveAspects.listOfAspects;
    this.listOfDeletedAspects = this.decisionData.objectiveAspects.listOfDeletedAspects;
    this.tree = this.decisionData.objectiveAspects.getAspectTree($localize`Zielhierarchie`);
    this.updateMergedAspects();
    this.setMissingOrderIdxs();
    this.decisionData.objectiveAspectAdded$.pipe(takeUntil(this.onDestroy$)).subscribe(() => {
      this.updateMergedAspects();
    });
    this.decisionData.objectiveAdded$.pipe(takeUntil(this.onDestroy$)).subscribe(() => {
      this.tree = this.decisionData.objectiveAspects.getAspectTree($localize`Zielhierarchie`);
      this.updateMergedAspects();
    });
  }

  updateAspectsFromTree() {
    // Breadth First Search
    this.aspectsFromTree = [];
    let nodesToVisit = [this.tree];
    while (nodesToVisit.length > 0) {
      const currentNode = nodesToVisit.shift();
      nodesToVisit = nodesToVisit.concat(currentNode.children);
      this.aspectsFromTree.push(currentNode.value);
    }
    this.aspectsFromTree.shift(); // remove the root
  }

  deleteAspectFromTree(deleteIdx: number) {
    // deleting an element using BFS index
    let nodesToVisit = [this.tree];
    const parents = [];
    let index = 0;
    while (nodesToVisit.length > 0) {
      const currentNode = nodesToVisit.shift();
      parents.push({ node: currentNode, childrenNumber: currentNode.children.length });
      nodesToVisit = nodesToVisit.concat(currentNode.children);
      if (index === deleteIdx + 1) {
        let sum = 0;
        if (index <= parents[0].childrenNumber) {
          // aspect with depth 2, remove objective
          this.decisionData.removeObjective(index - 1);
        }
        for (const p of parents) {
          sum += p.childrenNumber;
          if (index <= sum) {
            p.node.children.splice(p.childrenNumber - (sum - index) - 1, 1); // remove aspect from tree
            return;
          }
        }
        return;
      }
      index++;
    }
  }

  setAndFocusInput(value: string) {
    this.newAspectValue = value;
    this.newAspectInputElement.nativeElement.focus();
  }

  addAspect() {
    if (this.newAspectValue) {
      const newAspect = new ObjectiveElement(this.newAspectValue);
      newAspect.createdInSubStep = this.pageKey;
      newAspect.orderIdx = this.getNextOrderIdx();
      this.listOfAspects.push(newAspect);
      this.updateMergedAspects();

      this.newAspectValue = '';
      this.newAspectInputElement.nativeElement.focus();

      this.dirty.emit();
    }
  }

  deleteAspect(index: number) {
    if (this.mergedAspectsWithIndexes[index].isInTree) {
      this.dialog.open(this.confirmDeleteModal, {
        data: {
          index: index,
          name: this.mergedAspectsWithIndexes[index].aspect.name,
        },
      });
    } else {
      if (!this.mergedAspectsWithIndexes[index].isDeleted) {
        // remove ObjectiveElement(aspect)
        this.listOfAspects.splice(this.mergedAspectsWithIndexes[index].idx, 1);
      } else {
        // remove ObjectiveElement(aspect) from bin
        this.listOfDeletedAspects.splice(this.mergedAspectsWithIndexes[index].idx, 1);
      }
    }
    this.updateMergedAspects();
    this.dirty.emit();
  }

  currentlyDragging(index: number) {
    return this.dragIndex === index;
  }

  onNameChange(newName: string, index: number) {
    this.mergedAspectsWithIndexes[index].aspect.name = newName;
    if (!newName && !this.mergedAspectsWithIndexes[index].isInTree) {
      this.deleteAspect(index);
    }
  }

  setMissingOrderIdxs() {
    let orderIdx = 0;
    for (const element of this.mergedAspectsWithIndexes) {
      if (element.aspect.orderIdx == null) {
        element.aspect.orderIdx = orderIdx;
        orderIdx++;
      } else {
        orderIdx = element.aspect.orderIdx + 1;
      }
    }
  }

  getNextOrderIdx() {
    let maxOrderIdx = null;
    for (const element of this.mergedAspectsWithIndexes) {
      if (
        (element.aspect.orderIdx != null && maxOrderIdx != null && element.aspect.orderIdx > maxOrderIdx) ||
        (element.aspect.orderIdx != null && maxOrderIdx == null)
      ) {
        maxOrderIdx = element.aspect.orderIdx;
      }
    }
    if (maxOrderIdx != null) {
      return maxOrderIdx + 1;
    }
    return 0;
  }

  compareOrder(
    a: { idx: number; isInTree: boolean; isDeleted: boolean; aspect: ObjectiveElement },
    b: { idx: number; isInTree: boolean; isDeleted: boolean; aspect: ObjectiveElement },
  ) {
    if (a.aspect.orderIdx < b.aspect.orderIdx) {
      return -1;
    }
    if (a.aspect.orderIdx > b.aspect.orderIdx) {
      return 1;
    }
    return 0;
  }

  isHidden(element: { idx: number; isInTree: boolean; isDeleted: boolean; aspect: ObjectiveElement }) {
    if (this.decisionData.projectMode === 'professional') return element.isDeleted;

    switch (this.filter) {
      case 'all':
        return element.isDeleted;
      case 'onlyCurrent': {
        const fromThisStep = element.aspect.createdInSubStep === this.pageKey;

        return !fromThisStep;
      }
      case 'currentAndPrevious': {
        if (element.aspect.createdInSubStep === 0) {
          return true;
        }

        const fromPreviousOrThisStep = element.aspect.createdInSubStep <= this.pageKey;
        return !fromPreviousOrThisStep;
      }
    }
  }

  dragOver(event: DragEvent, index: number) {
    if (this.dragIndex == null || index == null) {
      return;
    }

    this.moveAspectInArray(this.dragIndex, index);
    event.dataTransfer.dropEffect = 'move';
    event.preventDefault();
    this.dragIndex = index;
    this.dirty.emit();
  }

  moveAspectInArray(from: number, to: number) {
    if (from === to || from > this.mergedAspectsWithIndexes.length - 1 || to > this.mergedAspectsWithIndexes.length - 1) {
      return;
    }
    const toOrderIdx = this.mergedAspectsWithIndexes[to].aspect.orderIdx;
    if (from < to) {
      for (let i = to; i > from; i--) {
        this.mergedAspectsWithIndexes[i].aspect.orderIdx = this.mergedAspectsWithIndexes[i - 1].aspect.orderIdx;
      }
    } else {
      for (let i = to; i < from; i++) {
        this.mergedAspectsWithIndexes[i].aspect.orderIdx = this.mergedAspectsWithIndexes[i + 1].aspect.orderIdx;
      }
    }
    this.mergedAspectsWithIndexes[from].aspect.orderIdx = toOrderIdx;
    this.updateMergedAspects();
  }

  updateMergedAspects() {
    this.updateAspectsFromTree();
    this.mergedAspectsWithIndexes = this.aspectsFromTree.map((aspect: ObjectiveElement, index: number) => {
      return { idx: index, isInTree: true, isDeleted: false, aspect: aspect };
    });
    this.listOfAspects.map((aspect: ObjectiveElement, index: number) => {
      this.mergedAspectsWithIndexes.push({ idx: index, isInTree: false, isDeleted: false, aspect: aspect });
    });
    this.listOfDeletedAspects.map((aspect: ObjectiveElement, index: number) => {
      this.mergedAspectsWithIndexes.push({ idx: index, isInTree: false, isDeleted: true, aspect: aspect });
    });
    this.mergedAspectsWithIndexes.sort(this.compareOrder);
  }

  dragStart(event: DragEvent, index: number, aspectBoxInnerWrapper: HTMLElement) {
    event.dataTransfer.setData('text', this.mergedAspectsWithIndexes[index].aspect.name || ' ');
    event.dataTransfer.effectAllowed = 'move';
    // to set not grayed drag image in safari
    // to update drag image in chrome and firefox
    aspectBoxInnerWrapper.style.opacity = '0.99';
    event.dataTransfer.setDragImage(aspectBoxInnerWrapper, event.offsetX, event.offsetY);
    this.oldDragIndex = index;
    this.dragIndex = index;
  }

  dragEnter(event: DragEvent) {
    this.enterTarget = event.target;
  }

  dragLeave(event: DragEvent) {
    if (this.enterTarget === event.target && this.dragIndex != null) {
      this.moveAspectInArray(this.dragIndex, this.oldDragIndex);
      this.dragIndex = this.oldDragIndex;
    }
  }

  dragEnd() {
    this.dragIndex = undefined;
  }

  drop(event: DragEvent) {
    this.dragIndex = undefined;
    event.preventDefault();
  }
}
