import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  ElementRefDirective,
  HoverPopOverDirective,
  LocalizedStringPipe,
  NoteBtnComponent,
  NoteBtnPresetPipe,
  SimpleHierarchyComponent,
  TagInputComponent,
  WidthTriggerDirective,
} from '@entscheidungsnavi/widgets';
import { SharedModule } from '../shared/shared.module';
import { NoteHoverComponent } from '../shared/note-hover/note-hover.component';
import { AttachedObjectiveDataComponent, ObjectivesComponent } from './main';
import { ZieleHint1Component, ZieleHint2Component, ZieleHint3Component, ZieleHint4Component, ZieleHint5Component } from './hints';
import { ObjectiveAspectListComponent } from './aspect-list/objective-aspect-list.component';
import { ObjectivesRoutingModule } from './objectives-routing.module';
import { HelpBackgroundComponent } from './hints/help-background/help-background.component';
import { Help1Component as Hint1Help1Component } from './hints/hint1/help1/help1.component';
import { Help2Component as Hint1Help2Component } from './hints/hint1/help2/help2.component';
import { Help2Component as Hint3Help2Component } from './hints/hint3/help2/help2.component';
import { Help2Component as Hint5Help2Component } from './hints/hint5/help2/help2.component';
import { Help1Component as Hint3Help1Component } from './hints/hint3/help1/help1.component';
import { Help1Component as Hint5Help1Component } from './hints/hint5/help1/help1.component';
import { ObjectiveBoxComponent } from './objective-box/objective-box.component';
import { DeleteObjectiveModalComponent } from './delete-objective-modal/delete-objective-modal.component';
import { ObjectiveListComponent } from './objective-list/objective-list.component';
import { ObjectiveHierarchyComponent } from './objective-hierarchy/objective-hierarchy.component';
import { ObjectiveHierarchyShortcutsComponent } from './objective-hierarchy-shortcuts/objective-hierarchy-shortcuts.component';

const DECLARE_AND_EXPORT = [ZieleHint4Component, ObjectiveAspectListComponent, ObjectiveListComponent, AttachedObjectiveDataComponent];
@NgModule({
  declarations: [
    ...DECLARE_AND_EXPORT,
    DeleteObjectiveModalComponent,
    ObjectiveBoxComponent,
    ObjectivesComponent,
    ZieleHint1Component,
    ZieleHint2Component,
    ZieleHint3Component,
    ZieleHint5Component,
    ObjectiveAspectListComponent,
    ObjectivesComponent,
    HelpBackgroundComponent,
    Hint1Help1Component,
    Hint1Help2Component,
    Hint3Help2Component,
    Hint5Help2Component,
    Hint3Help1Component,
    Hint5Help1Component,
  ],
  imports: [
    CommonModule,
    ObjectivesRoutingModule,
    SharedModule,
    NoteBtnComponent,
    NoteBtnPresetPipe,
    HoverPopOverDirective,
    NoteHoverComponent,
    WidthTriggerDirective,
    LocalizedStringPipe,
    ObjectiveHierarchyComponent,
    TagInputComponent,
    SimpleHierarchyComponent,
    ObjectiveHierarchyShortcutsComponent,
    ElementRefDirective,
  ],
  exports: [...DECLARE_AND_EXPORT],
})
export class ObjectivesModule {}
