import { Directive, EventEmitter, Host, Injector, Output } from '@angular/core';
import {
  ConfirmModalComponent,
  ConfirmModalData,
  HierarchyAction,
  HierarchyInterfaceDirective,
  PopOverService,
  SimpleHierarchyDragData,
  SimpleHierarchyNode,
} from '@entscheidungsnavi/widgets';
import { ObjectiveElement } from '@entscheidungsnavi/decision-data';
import { assertUnreachable, LocationSequence, Tree } from '@entscheidungsnavi/tools';
import { firstValueFrom } from 'rxjs';
import { takeUntilDestroyed } from '@angular/core/rxjs-interop';
import { MatDialog } from '@angular/material/dialog';
import { ObjectiveHierarchyContext } from './common/objective-hierarchy-context';
import { ObjectiveHierarchyAction } from './actions/action';
import { InsertAction } from './actions/insert';
import { MoveAction } from './actions/move';
import { CompoundAction } from './actions/compound';
import { DeleteAction } from './actions/delete';
import { ObjectiveHierarchyInternalDragData } from './drag/internal-drag';
import { ObjectiveHierarchyExternalDragData } from './drag/external-drag';

@Directive({ selector: 'dt-hierarchy[dtObjectiveHierarchyAction]', standalone: true })
export class ObjectiveHierarchyActionDirective {
  @Output() simpleHierarchyDataDrop = new EventEmitter<SimpleHierarchyDragData<SimpleHierarchyNode>>();

  constructor(
    @Host() hierarchyInterface: HierarchyInterfaceDirective<ObjectiveElement>,
    private context: ObjectiveHierarchyContext,
    private injector: Injector,
    private dialog: MatDialog,
    private popOverService: PopOverService,
  ) {
    hierarchyInterface.action.pipe(takeUntilDestroyed()).subscribe(action => this.executeHierarchyAction(action));
  }

  async executeHierarchyAction(action: HierarchyAction) {
    const parsedAction = await this.parseHierarchyAction(action);

    if (parsedAction) {
      await this.context.executeAction(parsedAction);
    }
  }

  private async parseHierarchyAction(action: HierarchyAction): Promise<ObjectiveHierarchyAction | null> {
    switch (action.type) {
      case 'insert':
        return InsertAction.create(action.location, new Tree(new ObjectiveElement()), this.injector);
      case 'delete':
        return MoveAction.create(action.location, { list: 'trash', position: 0 }, this.injector);
      case 'move':
        return MoveAction.create(action.from, action.to, this.injector);
      case 'compound':
        return CompoundAction.create(await Promise.all(action.actions.map(action => this.parseHierarchyAction(action))), this.injector);
      case 'drop':
        return await this.parseDropAction(action.event, action.location, action.element);
      default:
        assertUnreachable(action);
    }
  }

  private async parseDropAction(
    event: DragEvent,
    location: LocationSequence,
    element: HTMLElement,
  ): Promise<ObjectiveHierarchyAction | null> {
    return (
      this.handleInternalDrop(event, location) ||
      this.handleExternalDrop(event, location) ||
      (await this.handleSimpleHierarchyDrop(event, element, location)) ||
      this.handlePlainTextDrop(event, location)
    );
  }

  private handleInternalDrop(event: DragEvent, location: LocationSequence): ObjectiveHierarchyAction | null {
    const internalData = ObjectiveHierarchyInternalDragData.fromEvent(event, this.context.hierarchyUUID);

    if (internalData != null) {
      const actions = internalData.data.locations.map(loc => MoveAction.create(loc, location, this.injector)).reverse();
      return CompoundAction.create(actions, this.injector);
    }

    return null;
  }

  private handleExternalDrop(event: DragEvent, location: LocationSequence): ObjectiveHierarchyAction | null {
    const externalData = ObjectiveHierarchyExternalDragData.fromEvent(event);

    if (externalData?.data.type === 'list') {
      const actions = externalData.data.aspects.map(aspect => InsertAction.create(location, new Tree(aspect), this.injector)).reverse();
      return CompoundAction.create(actions, this.injector);
    } else if (externalData?.data.type === 'tree') {
      const actions = externalData.data.trees.map(tree => InsertAction.create(location, tree, this.injector)).reverse();
      return CompoundAction.create(actions, this.injector);
    }

    return null;
  }

  private async handleSimpleHierarchyDrop(
    event: DragEvent,
    element: HTMLElement,
    location: LocationSequence,
  ): Promise<ObjectiveHierarchyAction | null> {
    const simpleHierarchyData = SimpleHierarchyDragData.fromEvent(event);

    if (simpleHierarchyData?.elements.some(element => element.location.isRoot())) {
      const confirm = await firstValueFrom(
        this.dialog
          .open<ConfirmModalComponent, ConfirmModalData>(ConfirmModalComponent, {
            data: {
              title: $localize`Zielhierarchie überschreiben`,
              prompt: $localize`Bist Du sicher, dass Du Deine Zielhierarchie komplett überschreiben willst?
                Dadurch werden alle mit dem Zielen verknüpften Daten gelöscht! Du kannst diese Aktion nicht rückgängig machen.`,
              buttonConfirm: $localize`Ja, überschreiben`,
            },
          })
          .afterClosed(),
      );

      if (!confirm) return null;

      const rootElement = simpleHierarchyData.elements.find(element => element.location.isRoot());
      this.simpleHierarchyDataDrop.emit(new SimpleHierarchyDragData([rootElement]));

      const deleteActions = this.context.decisionData.objectives.map(() =>
        DeleteAction.create(new LocationSequence([0]), this.injector, true),
      );
      const insertActions = rootElement.tree.children
        .map(child => InsertAction.create(new LocationSequence([0]), child, this.injector))
        .reverse();
      return CompoundAction.create([...deleteActions, ...insertActions], this.injector);
    } else if (simpleHierarchyData) {
      const keepChildren =
        simpleHierarchyData.elements.every(element => element.tree.children.length === 0) ||
        (await this.popOverService.confirm(element, $localize`Was möchtest Du übernehmen?`, {
          confirmText: $localize`Gesamten Ast`,
          confirmColor: null,
          discardText: $localize`Nur dieses Element`,
        }));

      if (!keepChildren) {
        // Clear all the children
        simpleHierarchyData.elements.forEach(element => (element.tree.children = []));
      }

      this.simpleHierarchyDataDrop.emit(simpleHierarchyData);
      const actions = simpleHierarchyData.elements
        .map(element => InsertAction.create(location, element.tree.map(parseObjectiveElement), this.injector))
        .reverse();
      return CompoundAction.create(actions, this.injector);
    }

    return null;
  }

  private handlePlainTextDrop(event: DragEvent, location: LocationSequence): ObjectiveHierarchyAction | null {
    const plainTextData = event.dataTransfer.getData('text/plain');

    if (plainTextData) {
      const names = plainTextData
        .split('\n')
        .map(item => item.trim())
        .filter(Boolean);
      const actions = names.map(name => InsertAction.create(location, new Tree(new ObjectiveElement(name)), this.injector)).reverse();
      return CompoundAction.create(actions, this.injector);
    }

    return null;
  }
}

function parseObjectiveElement(simpleNode: SimpleHierarchyNode) {
  return new ObjectiveElement(simpleNode.name, undefined, undefined, undefined, undefined, undefined, undefined, simpleNode.comment);
}
