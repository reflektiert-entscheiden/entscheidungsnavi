import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  EventEmitter,
  HostListener,
  Injector,
  Input,
  Output,
  ViewChild,
} from '@angular/core';

import {
  AfterViewInitDirective,
  AutoFocusDirective,
  CollapsibleTinyComponent,
  ColorPickerDirective,
  DEFAULT_COLOR_PRESET,
  HierarchyComponent,
  HierarchyNodeStateDirective,
  HierarchyToolbarComponent,
  KeyBindHandlerDirective,
  NoteBtnComponent,
  NoteBtnPresetPipe,
  PlatformDetectService,
  PopOverService,
  SimpleHierarchyDragData,
  SimpleHierarchyNode,
} from '@entscheidungsnavi/widgets';
import { FormsModule } from '@angular/forms';
import { MatIconModule } from '@angular/material/icon';
import { MatDividerModule } from '@angular/material/divider';
import { MatButtonModule } from '@angular/material/button';
import { LocationSequence, Tree } from '@entscheidungsnavi/tools';
import { ObjectiveElement } from '@entscheidungsnavi/decision-data';
import { MatDialog } from '@angular/material/dialog';
import { uniq } from 'lodash';
import { Clipboard } from '@angular/cdk/clipboard';
import { MatTooltipModule } from '@angular/material/tooltip';
import { ObjectiveHierarchyShortcutsComponent } from '../objective-hierarchy-shortcuts/objective-hierarchy-shortcuts.component';
import { ObjectiveHierarchyContext } from './common/objective-hierarchy-context';
import { ObjectiveHierarchyBoxesComponent } from './boxes/objective-hierarchy-boxes.component';
import { InsertAction } from './actions/insert';
import { CompoundAction } from './actions/compound';
import { ObjectiveHierarchyActionDirective } from './objective-hierarchy-action.directive';
import { ObjectiveHierarchyInternalDragData } from './drag/internal-drag';
import { ObjectiveHierarchyExternalDragData } from './drag/external-drag';

@Component({
  selector: 'dt-objective-hierarchy',
  templateUrl: './objective-hierarchy.component.html',
  styleUrls: ['./objective-hierarchy.component.scss'],
  standalone: true,
  changeDetection: ChangeDetectionStrategy.OnPush,
  imports: [
    FormsModule,
    MatIconModule,
    MatDividerModule,
    MatButtonModule,
    HierarchyComponent,
    AfterViewInitDirective,
    NoteBtnComponent,
    AutoFocusDirective,
    CollapsibleTinyComponent,
    ObjectiveHierarchyBoxesComponent,
    HierarchyToolbarComponent,
    ColorPickerDirective,
    MatTooltipModule,
    ObjectiveHierarchyActionDirective,
    NoteBtnPresetPipe,
  ],
  providers: [ObjectiveHierarchyContext],
  hostDirectives: [KeyBindHandlerDirective],
})
export class ObjectiveHierarchyComponent {
  @Input() brainstormingShown = true;

  @Output() simpleHierarchyDataDrop = new EventEmitter<SimpleHierarchyDragData<SimpleHierarchyNode>>();

  @ViewChild(HierarchyComponent, { read: ElementRef }) hierarchy: ElementRef<HTMLElement>;
  @ViewChild(HierarchyNodeStateDirective) nodeState: HierarchyNodeStateDirective<ObjectiveElement>;

  get isInFullscreen() {
    return (document.fullscreenElement || (document as any).webkitFullscreenElement) === this.myElement.nativeElement;
  }

  constructor(
    protected context: ObjectiveHierarchyContext,
    private injector: Injector,
    private myElement: ElementRef<HTMLElement>,
    private cdRef: ChangeDetectorRef,
    private dialog: MatDialog,
    private popOverService: PopOverService,
    platformDetectService: PlatformDetectService,
    keyBindHandler: KeyBindHandlerDirective,
    private clipboard: Clipboard,
    private elementRef: ElementRef<HTMLElement>,
  ) {
    keyBindHandler.register({
      key: 'z',
      ctrlKey: true,
      callback: () => this.context.undo(),
      conditions: [() => !this.context.readonly],
    });
    keyBindHandler.register({
      key: 'y',
      ctrlKey: true,
      callback: () => this.context.redo(),
      conditions: [() => !this.context.readonly],
    });

    if (platformDetectService.macOS) {
      keyBindHandler.register({
        key: 'c',
        metaKey: true,
        callback: () => this.copyToClipboard(),
      });
    } else {
      keyBindHandler.register({
        key: 'c',
        ctrlKey: true,
        callback: () => this.copyToClipboard(),
      });
    }
  }

  readonly textPresetColorsGetter = () => this.getPresetColors('text');
  readonly backgroundPresetColorsGetter = () => this.getPresetColors('text');

  hierarchyNodeDragStart(event: DragEvent, locations: LocationSequence[]) {
    const trees = locations.map(loc => this.context.tree().getNode(loc));

    const plainTextData = trees.map(node => node.value.name).join('\n');
    event.dataTransfer.setData('text/plain', plainTextData);

    ObjectiveHierarchyInternalDragData.writeToEvent(event, this.context.hierarchyUUID, { treeLocations: locations });
    ObjectiveHierarchyExternalDragData.writeToEvent(event, { trees });
  }

  hierarchyNodeDragOver(event: DragEvent) {
    if (ObjectiveHierarchyInternalDragData.isInEvent(event, this.context.hierarchyUUID)) {
      event.dataTransfer.dropEffect = 'move';
    } else if (
      ObjectiveHierarchyExternalDragData.isInEvent(event) ||
      SimpleHierarchyDragData.isInEvent(event) ||
      event.dataTransfer.types.includes('text/plain')
    ) {
      event.dataTransfer.dropEffect = 'copy';
    }
  }

  private getPresetColors(colorTarget: 'text' | 'background') {
    const treePreset = uniq([
      '#D3D3D3',
      '#5D666F',
      ...this.context
        .tree()
        .flat()
        .map(node => (colorTarget === 'text' ? node.textColor : node.backgroundColor))
        .filter(color => color != null)
        .map(color => color.toUpperCase()),
    ]);

    const defaultColors = DEFAULT_COLOR_PRESET.slice();
    while (treePreset.length < 16 && defaultColors.length > 0) {
      const color = defaultColors.shift();

      if (!treePreset.includes(color)) {
        treePreset.push(color);
      }
    }

    return treePreset;
  }

  openHelp() {
    this.dialog.open(ObjectiveHierarchyShortcutsComponent);
  }

  toggleFullscreen() {
    const dummyPromise = Promise.resolve();

    if (this.isInFullscreen) {
      const doc = document as any;

      let promise: Promise<void>;
      if (doc.exitFullscreen) {
        promise = document.exitFullscreen();
      } else if (doc.webkitExitFullscreen) {
        promise = doc.webkitExitFullscreen();
      }

      (promise ?? dummyPromise).then(() => this.cdRef.detectChanges());
    } else {
      const element = this.myElement.nativeElement as any;

      let promise: Promise<void>;
      if (element.requestFullscreen) {
        promise = element.requestFullscreen();
      } else if (element.webkitRequestFullscreen) {
        promise = element.webkitRequestFullscreen();
      } else if (element.webkitRequestFullScreen) {
        promise = element.webkitRequestFullScreen();
      }

      (promise ?? dummyPromise).then(() => this.cdRef.detectChanges());
    }
  }

  copyToClipboard() {
    const focused = this.nodeState.focused();
    if (focused.length === 0) return;

    const string = focused.map(location => this.context.tree().getNode(location).value.name).join('\n');
    if (this.clipboard.copy(string)) {
      this.popOverService.whistle(this.hierarchy, $localize`Elemente kopiert!`, 'done');
    } else {
      this.popOverService.whistle(this.hierarchy, $localize`Kopieren fehlgeschlagen. Möglicherweise ist der Text zu lang.`, 'error');
    }
  }

  @HostListener('document:paste', ['$event'])
  async pasteFromClipboard(event: ClipboardEvent) {
    if (!event.clipboardData) return;

    const focused = this.nodeState.focused();
    if (focused.length === 0) {
      return;
    }

    const target: HTMLElement = document.activeElement as HTMLElement;

    if (target instanceof HTMLInputElement || target instanceof HTMLTextAreaElement || target.isContentEditable) {
      return;
    }

    event.preventDefault();

    if (this.context.readonly) return;

    if (focused.length > 1) {
      this.popOverService.whistle(this.hierarchy, $localize`Wähle einen einzelnen Knoten zum Einfügen aus`);
      return;
    }

    const data = event.clipboardData.getData('text/plain');

    const names = data
      .split('\n')
      .map(item => item.trim())
      .filter(Boolean);
    if (names.length === 0) return;

    const parentLoc = focused[0];
    const childCount = this.context.tree().getNode(parentLoc).children.length;

    const actions = names.map((name, index) =>
      InsertAction.create(parentLoc.getChild(childCount + index), new Tree(new ObjectiveElement(name)), this.injector),
    );
    await this.context.executeAction(CompoundAction.create(actions, this.injector));

    this.popOverService.whistle(this.hierarchy, $localize`Erfolgreich eingefügt!`, 'done');
  }

  public updateComment(
    loc: LocationSequence,
    oldComment: string,
    oldScaleComment: string,
    event: { comment: string; secondComment: string },
  ) {
    if (event.comment !== oldComment || event.secondComment != oldScaleComment) {
      this.context.updateComment(loc, event.comment, event.secondComment);
    }
  }
}
