import { LocationSequence } from '@entscheidungsnavi/tools';

export type ObjectiveListType = 'brainstorming' | 'trash';
export type ObjectiveListLocation = { list: ObjectiveListType; position: number };

export type ListOrTreeLocation = ObjectiveListLocation | LocationSequence;
