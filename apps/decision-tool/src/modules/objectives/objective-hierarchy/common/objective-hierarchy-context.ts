import { computed, effect, Injectable, Injector, signal, OnDestroy } from '@angular/core';
import { DecisionData } from '@entscheidungsnavi/decision-data';
import { assertUnreachable, isRichTextEmpty, LocationSequence } from '@entscheidungsnavi/tools';
import { v4 as uuidv4 } from 'uuid';
import { takeUntilDestroyed } from '@angular/core/rxjs-interop';
import { IssueRaisingEventsService } from '../../../../app/issue-raising/issue-raising-events.service';
import { ChangeColorAction, CompoundAction, ObjectiveHierarchyAction, RenameAction, DeleteAction, UpdateCommentAction } from '../actions';
import { DecisionDataState } from '../../../shared/decision-data-state';
import { ObjectiveListType } from './objective-hierarchy-types';

interface ActionStack {
  // Newest actions are at the end, oldest actions at the start
  stack: ObjectiveHierarchyAction[];
  // The current negative index within the action stack. -1 is the last stack entry, -2 the second to last, ...
  index: number;
}

@Injectable()
export class ObjectiveHierarchyContext implements OnDestroy {
  readonly hierarchyUUID = uuidv4();

  readonly tree = signal(this.createTree());

  readonly brainstormingAspects = signal(this.decisionData.objectiveAspects.listOfAspects);
  readonly deletedAspects = signal(this.decisionData.objectiveAspects.listOfDeletedAspects);

  private readonly actionStack = signal<ActionStack>({ stack: [], index: -1 });

  readonly canUndo = computed(() => {
    const actionStack = this.actionStack();
    return actionStack.stack.length + actionStack.index >= 0;
  });

  readonly canRedo = computed(() => {
    const actionStack = this.actionStack();
    return actionStack.index < -1;
  });

  get readonly() {
    return this.decisionDataState.isProjectReadonly;
  }

  constructor(
    public readonly decisionData: DecisionData,
    private injector: Injector,
    issueRaisingService: IssueRaisingEventsService,
    private decisionDataState: DecisionDataState,
  ) {
    issueRaisingService.objectiveAdded$.pipe(takeUntilDestroyed()).subscribe(() => this.tree.set(this.createTree()));

    effect(() => {
      this.decisionData.objectiveAspects.listOfAspects = this.brainstormingAspects();
    });
    effect(() => {
      this.decisionData.objectiveAspects.listOfDeletedAspects = this.deletedAspects();
    });
  }

  ngOnDestroy() {
    const emptyObjectives: LocationSequence[] = [];

    this.tree().iterate((node, location) => {
      if (node.value.name.length === 0 && isRichTextEmpty(node.value.comment)) {
        emptyObjectives.push(location);
      }
    });

    for (let pos = emptyObjectives.length - 1; pos >= 0; pos--) {
      const node = this.tree().getNode(emptyObjectives[pos]);
      if (node.children.length === 0 && !(emptyObjectives[pos].getLevel() === 1)) {
        this.executeAction(DeleteAction.create(emptyObjectives[pos], this.injector));
      }
    }
  }

  private createTree() {
    return this.decisionData.objectiveAspects.getAspectTree($localize`Zielhierarchie`);
  }

  getList(list: ObjectiveListType) {
    switch (list) {
      case 'brainstorming':
        return this.brainstormingAspects;
      case 'trash':
        return this.deletedAspects;
      default:
        assertUnreachable(list);
    }
  }

  async executeAction(action: ObjectiveHierarchyAction) {
    const result = await action.execute();
    if (!result) return;

    this.actionStack.update(({ stack, index }) => {
      if (index < -1) {
        // Cut off the rest of the action stack -> we can't redo
        stack = stack.slice(0, index + 1);
        index = -1;
      }

      stack.push(action);

      return { stack, index };
    });
  }

  async undo() {
    if (!this.canUndo()) return;

    const actionStack = this.actionStack();
    await actionStack.stack.at(actionStack.index).undo();

    this.actionStack.update(({ stack, index }) => ({ stack, index: index - 1 }));
  }

  async redo() {
    if (!this.canRedo()) return;

    const actionStack = this.actionStack();
    const result = await actionStack.stack.at(actionStack.index + 1).execute();

    if (!result) return;

    this.actionStack.update(({ stack, index }) => ({ stack, index: index + 1 }));
  }

  async renameNode(locationSequence: LocationSequence, newName: string) {
    await this.executeAction(RenameAction.create(locationSequence, newName, this.injector));
  }

  async updateComment(locationSequence: LocationSequence, newComment: string, newScaleComment: string) {
    await this.executeAction(UpdateCommentAction.create(locationSequence, newComment, newScaleComment, this.injector));
  }

  async clearFormatting(locations: LocationSequence[]) {
    const actions = locations.flatMap(location => [
      ChangeColorAction.create(location, 'text', null, this.injector),
      ChangeColorAction.create(location, 'background', null, this.injector),
    ]);
    await this.executeAction(CompoundAction.create(actions, this.injector));
  }

  async changeNodeColor(locations: LocationSequence[], target: 'text' | 'background', newColor: string | null) {
    if (newColor == null) return;

    const actions = locations.map(location => ChangeColorAction.create(location, target, newColor, this.injector));
    await this.executeAction(CompoundAction.create(actions, this.injector));
  }
}
