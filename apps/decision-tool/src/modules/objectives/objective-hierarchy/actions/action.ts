import { inject } from '@angular/core';
import { firstValueFrom, map } from 'rxjs';
import { MatDialog } from '@angular/material/dialog';
import { DeleteObjectiveModalComponent, DeleteObjectiveModalData } from '../../delete-objective-modal/delete-objective-modal.component';
import { ObjectiveHierarchyContext } from '../common/objective-hierarchy-context';

export abstract class ObjectiveHierarchyAction {
  protected readonly context = inject(ObjectiveHierarchyContext);
  protected readonly dialog = inject(MatDialog);

  protected confirmObjectiveDeletion(objectiveIndex: number): Promise<boolean> {
    const attachedData = this.context.decisionData.getAttachedObjectiveData(objectiveIndex);
    if (attachedData.length === 0) {
      return Promise.resolve(true);
    }

    const result$ = this.dialog
      .open<DeleteObjectiveModalComponent, DeleteObjectiveModalData>(DeleteObjectiveModalComponent, {
        data: { objectiveIndex, isHierarchy: true },
      })
      .afterClosed()
      .pipe(map(Boolean));

    return firstValueFrom(result$);
  }

  abstract execute(): Promise<boolean>;
  abstract undo(): Promise<void>;
}
