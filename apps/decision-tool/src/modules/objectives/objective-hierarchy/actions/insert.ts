import { LocationSequence, Tree } from '@entscheidungsnavi/tools';
import { Objective, ObjectiveElement } from '@entscheidungsnavi/decision-data';
import { Injector, runInInjectionContext } from '@angular/core';
import { clone } from 'lodash';
import { ObjectiveHierarchyAction } from './action';

export class InsertAction extends ObjectiveHierarchyAction {
  static create(location: LocationSequence, element: Tree<ObjectiveElement>, injector: Injector): InsertAction {
    return runInInjectionContext(injector, () => new InsertAction(location, element));
  }

  private constructor(
    private readonly location: LocationSequence,
    private readonly element: Tree<ObjectiveElement>,
  ) {
    super();
  }

  async execute() {
    if (this.location.getLevel() === 1) {
      // We have an objective, thus we also need to modify decision data
      const objectivePosition = this.location.value[0];

      const newObjective = new Objective(this.element.value.name);
      newObjective.aspects = this.element;

      this.context.decisionData.addObjective(newObjective, objectivePosition);
    }

    this.context.tree.update(tree => {
      tree.insertNode(this.location, this.element);
      return clone(tree);
    });

    return true;
  }

  async undo() {
    if (this.location.getLevel() === 1) {
      // We have an objective, thus we also need to modify decision data
      const objectivePosition = this.location.value[0];
      this.context.decisionData.removeObjective(objectivePosition);
    }

    this.context.tree.update(tree => {
      tree.removeNode(this.location);
      return clone(tree);
    });
  }
}
