import { LocationSequence } from '@entscheidungsnavi/tools';
import { Injector, runInInjectionContext } from '@angular/core';
import { clone } from 'lodash';
import { ObjectiveHierarchyAction } from './action';

export class ChangeColorAction extends ObjectiveHierarchyAction {
  static create(location: LocationSequence, target: 'text' | 'background', newColor: string, injector: Injector): ChangeColorAction {
    return runInInjectionContext(injector, () => new ChangeColorAction(location, target, newColor));
  }

  private oldColor: string;

  private constructor(
    private location: LocationSequence,
    private target: 'text' | 'background',
    private newColor: string,
  ) {
    super();
  }

  async execute() {
    this.context.tree.update(tree => {
      const element = tree.getNode(this.location).value;

      if (this.target === 'text') {
        this.oldColor = element.textColor;
        element.textColor = this.newColor;
      } else {
        this.oldColor = element.backgroundColor;
        element.backgroundColor = this.newColor;
      }

      return clone(tree);
    });

    return true;
  }

  async undo() {
    this.context.tree.update(tree => {
      const element = tree.getNode(this.location).value;

      if (this.target === 'text') {
        element.textColor = this.oldColor;
      } else {
        element.backgroundColor = this.oldColor;
      }

      return clone(tree);
    });
  }
}
