import { Objective, ObjectiveElement } from '@entscheidungsnavi/decision-data';
import { LocationSequence, moveInArray, Tree } from '@entscheidungsnavi/tools';
import { Injector, runInInjectionContext } from '@angular/core';
import { clone } from 'lodash';
import { ListOrTreeLocation, ObjectiveListLocation } from '../common/objective-hierarchy-types';
import { ObjectiveHierarchyAction } from './action';

export class MoveAction extends ObjectiveHierarchyAction {
  static create(from: ListOrTreeLocation, to: ListOrTreeLocation, injector: Injector): MoveAction {
    return runInInjectionContext(injector, () => new MoveAction(from, to));
  }

  private objectiveBackup: Objective;
  private treeElementBackup: Tree<ObjectiveElement>;

  private constructor(
    private readonly from: ListOrTreeLocation,
    private readonly to: ListOrTreeLocation,
  ) {
    super();
  }

  private createObjectiveFromTree(tree: Tree<ObjectiveElement>) {
    const newObjective = new Objective();
    newObjective.name = tree.value.name;
    newObjective.comment = tree.value.comment;
    newObjective.placeholder = tree.value.placeholder;
    newObjective.aspects = tree;
    return newObjective;
  }

  private async moveWithinTree(treeFrom: LocationSequence, treeTo: LocationSequence) {
    if (treeFrom.getLevel() === 1 && treeTo.getLevel() === 1) {
      this.context.decisionData.moveObjective(treeFrom.value[0], treeTo.value[0]);
    } else if (treeFrom.getLevel() === 1) {
      // We are deleting an objective
      const objectivePosition = treeFrom.value[0];

      if (!(await this.confirmObjectiveDeletion(objectivePosition))) {
        return false;
      }

      this.objectiveBackup = this.context.decisionData.objectives[objectivePosition];
      this.context.decisionData.removeObjective(objectivePosition);
    } else if (treeTo.getLevel() === 1) {
      // We are inserting an objective
      const element = this.context.tree().getNode(treeFrom);

      const newObjective = this.createObjectiveFromTree(element);

      this.context.decisionData.addObjective(newObjective, treeTo.value[0]);
    }

    this.context.tree.update(tree => {
      const node = tree.removeNode(treeFrom);
      tree.insertNode(treeTo, node);
      return clone(tree);
    });

    return true;
  }

  private restoreWithinTree(treeFrom: LocationSequence, treeTo: LocationSequence) {
    if (treeFrom.getLevel() === 1 && treeTo.getLevel() === 1) {
      this.context.decisionData.moveObjective(treeTo.value[0], treeFrom.value[0]);
    } else if (treeFrom.getLevel() === 1) {
      if (this.objectiveBackup == null) {
        throw new Error(`Mal-executed MoveAction: expected objective backup to be non-null for loc ${treeFrom.value}`);
      }

      this.context.decisionData.addObjective(this.objectiveBackup, treeFrom.value[0]);
    } else if (treeTo.getLevel() === 1) {
      this.context.decisionData.removeObjective(treeTo.value[0]);
    }

    this.context.tree.update(tree => {
      const node = tree.removeNode(treeTo);
      tree.insertNode(treeFrom, node);
      return clone(tree);
    });
  }

  private async moveFromTreeToList(treeFrom: LocationSequence, listTo: ObjectiveListLocation) {
    if (treeFrom.getLevel() === 1) {
      if (!(await this.confirmObjectiveDeletion(treeFrom.value[0]))) {
        return false;
      }

      this.objectiveBackup = this.context.decisionData.objectives[treeFrom.value[0]];
      this.context.decisionData.removeObjective(treeFrom.value[0]);
    }

    this.context.tree.update(tree => {
      this.treeElementBackup = tree.removeNode(treeFrom);
      return clone(tree);
    });

    const list = this.context.getList(listTo.list);
    list.update(list => {
      return [...list.slice(0, listTo.position), ...this.treeElementBackup.flat(), ...list.slice(listTo.position)];
    });

    return true;
  }

  private restoreFromTreeToList(treeFrom: LocationSequence, listTo: ObjectiveListLocation) {
    if (treeFrom.getLevel() === 1) {
      if (this.objectiveBackup == null) {
        throw new Error(`Mal-executed MoveAction: expected objective backup to be non-null for loc ${treeFrom.value}`);
      }

      this.context.decisionData.addObjective(this.objectiveBackup, treeFrom.value[0]);
    }

    if (this.treeElementBackup == null) {
      throw new Error(`Mal-executed MoveAction: expected tree element backup to be non-null for loc ${treeFrom.value}`);
    }

    const list = this.context.getList(listTo.list);
    list.update(list => {
      return [...list.slice(0, listTo.position), ...list.slice(listTo.position + this.treeElementBackup.size())];
    });

    this.context.tree.update(tree => {
      tree.insertNode(treeFrom, this.treeElementBackup);
      return clone(tree);
    });
  }

  private moveFromListToTree(listFrom: ObjectiveListLocation, treeTo: LocationSequence) {
    const list = this.context.getList(listFrom.list);
    let newTree: Tree<ObjectiveElement>;

    list.update(list => {
      newTree = new Tree(list[listFrom.position]);
      return [...list.slice(0, listFrom.position), ...list.slice(listFrom.position + 1)];
    });

    this.context.tree.update(tree => {
      tree.insertNode(treeTo, newTree);
      return clone(tree);
    });

    if (treeTo.getLevel() === 1) {
      const objective = this.createObjectiveFromTree(newTree);
      this.context.decisionData.addObjective(objective, treeTo.value[0]);
    }
  }

  private restoreFromListToTree(listFrom: ObjectiveListLocation, treeTo: LocationSequence) {
    const list = this.context.getList(listFrom.list);

    let value: ObjectiveElement;
    this.context.tree.update(tree => {
      value = tree.removeNode(treeTo).value;
      return clone(tree);
    });

    list.update(list => {
      return [...list.slice(0, listFrom.position), value, ...list.slice(listFrom.position)];
    });

    if (treeTo.getLevel() === 1) {
      this.context.decisionData.removeObjective(treeTo.value[0]);
    }
  }

  private moveWithinLists(listFrom: ObjectiveListLocation, listTo: ObjectiveListLocation) {
    const source = this.context.getList(listFrom.list);
    const target = this.context.getList(listTo.list);

    if (source === target) {
      source.update(list => {
        list = list.slice();
        moveInArray(list, listFrom.position, listTo.position);
        return list;
      });
    } else {
      let item: ObjectiveElement;
      source.update(list => {
        item = list[listFrom.position];
        return [...list.slice(0, listFrom.position), ...list.slice(listFrom.position + 1)];
      });
      target.update(list => {
        return [...list.slice(0, listTo.position), item, ...list.slice(listTo.position)];
      });
    }
  }
  async execute() {
    if (this.from instanceof LocationSequence && this.to instanceof LocationSequence) {
      return await this.moveWithinTree(this.from, this.to);
    } else if (this.from instanceof LocationSequence && !(this.to instanceof LocationSequence)) {
      return await this.moveFromTreeToList(this.from, this.to);
    } else if (!(this.from instanceof LocationSequence) && this.to instanceof LocationSequence) {
      this.moveFromListToTree(this.from, this.to);
      return true;
    } else if (!(this.from instanceof LocationSequence) && !(this.to instanceof LocationSequence)) {
      this.moveWithinLists(this.from, this.to);
      return true;
    } else {
      throw new Error('Unreachable case');
    }
  }

  async undo() {
    if (this.from instanceof LocationSequence && this.to instanceof LocationSequence) {
      this.restoreWithinTree(this.from, this.to);
    } else if (this.from instanceof LocationSequence && !(this.to instanceof LocationSequence)) {
      this.restoreFromTreeToList(this.from, this.to);
    } else if (!(this.from instanceof LocationSequence) && this.to instanceof LocationSequence) {
      this.restoreFromListToTree(this.from, this.to);
    } else if (!(this.to instanceof LocationSequence) && !(this.from instanceof LocationSequence)) {
      this.moveWithinLists(this.to, this.from);
    } else {
      throw new Error('Unreachable case');
    }
  }
}
