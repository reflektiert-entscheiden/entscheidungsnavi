import { Injector, runInInjectionContext } from '@angular/core';
import { ObjectiveHierarchyAction } from './action';

export class CompoundAction extends ObjectiveHierarchyAction {
  static create(actions: ObjectiveHierarchyAction[], injector: Injector): CompoundAction {
    return runInInjectionContext(injector, () => new CompoundAction(actions));
  }

  private constructor(private readonly actions: ObjectiveHierarchyAction[]) {
    super();
  }

  async execute() {
    let lastExecutedIndex: number;

    for (lastExecutedIndex = 0; lastExecutedIndex < this.actions.length; lastExecutedIndex++) {
      const result = await this.actions[lastExecutedIndex].execute();

      if (!result) {
        break;
      }
    }

    if (lastExecutedIndex < this.actions.length) {
      // We aborted -> undo what we've done so far
      while (lastExecutedIndex > 0) {
        await this.actions[--lastExecutedIndex].undo();
      }

      return false;
    }

    return true;
  }

  async undo() {
    for (let i = this.actions.length - 1; i >= 0; i--) {
      await this.actions[i].undo();
    }
  }
}
