import { Objective, ObjectiveElement } from '@entscheidungsnavi/decision-data';
import { LocationSequence, Tree } from '@entscheidungsnavi/tools';
import { Injector, runInInjectionContext } from '@angular/core';
import { clone } from 'lodash';
import { ListOrTreeLocation, ObjectiveListLocation } from '../common/objective-hierarchy-types';
import { ObjectiveHierarchyAction } from './action';

export class DeleteAction extends ObjectiveHierarchyAction {
  static create(location: ListOrTreeLocation, injector: Injector, skipConfirmation = false): DeleteAction {
    return runInInjectionContext(injector, () => new DeleteAction(location, skipConfirmation));
  }

  private elementBackup: Objective | Tree<ObjectiveElement> | ObjectiveElement;

  private constructor(
    private readonly location: ListOrTreeLocation,
    private readonly skipConfirmation: boolean,
  ) {
    super();
  }

  private deleteFromList(listLocation: ObjectiveListLocation) {
    this.context.getList(listLocation.list).update(list => {
      this.elementBackup = list[listLocation.position];
      return [...list.slice(0, listLocation.position), ...list.slice(listLocation.position + 1)];
    });

    return true;
  }

  private undoFromList(listLoc: ObjectiveListLocation) {
    if (!(this.elementBackup instanceof ObjectiveElement)) {
      throw new Error(`Mal-executed DeleteAction: expected objective element, got ${this.elementBackup.constructor.name}`);
    }

    const objectiveElement = this.elementBackup;
    this.context.getList(listLoc.list).update(list => {
      return [...list.slice(0, listLoc.position), objectiveElement, ...list.slice(listLoc.position)];
    });
  }

  private async deleteFromTree(treeLoc: LocationSequence) {
    if (treeLoc.getLevel() === 1) {
      const objectivePosition = treeLoc.value[0];

      if (!this.skipConfirmation && !(await this.confirmObjectiveDeletion(objectivePosition))) {
        return false;
      }

      this.elementBackup = this.context.decisionData.objectives[objectivePosition];
      this.context.decisionData.removeObjective(objectivePosition);
    } else {
      this.elementBackup = this.context.tree().getNode(treeLoc);
    }

    this.context.tree.update(tree => {
      tree.removeNode(treeLoc);
      return clone(tree);
    });

    return true;
  }

  private undoFromTree(treeLoc: LocationSequence) {
    if (treeLoc.getLevel() === 1) {
      if (!(this.elementBackup instanceof Objective)) {
        throw new Error(`Mal-executed DeleteAction: expected objective, got ${this.elementBackup.constructor.name}`);
      }

      const objective = this.elementBackup;
      const objectivePosition = treeLoc.value[0];

      this.context.decisionData.addObjective(objective, objectivePosition);
      this.context.tree.update(tree => {
        tree.insertNode(treeLoc, objective.aspects);
        return clone(tree);
      });
    } else {
      if (!(this.elementBackup instanceof Tree)) {
        throw new Error(`Mal-executed DeleteAction: expected tree, got ${this.elementBackup.constructor.name}`);
      }

      const treeElement = this.elementBackup;
      this.context.tree.update(tree => {
        tree.insertNode(treeLoc, treeElement);
        return clone(tree);
      });
    }
  }

  async execute() {
    if (this.location instanceof LocationSequence) {
      return await this.deleteFromTree(this.location);
    } else {
      return this.deleteFromList(this.location);
    }
  }

  async undo() {
    if (this.location instanceof LocationSequence) {
      this.undoFromTree(this.location);
    } else {
      this.undoFromList(this.location);
    }
  }
}
