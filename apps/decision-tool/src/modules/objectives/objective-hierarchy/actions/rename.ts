import { LocationSequence } from '@entscheidungsnavi/tools';
import { Injector, runInInjectionContext } from '@angular/core';
import { clone } from 'lodash';
import { ObjectiveHierarchyAction } from './action';

export class RenameAction extends ObjectiveHierarchyAction {
  static create(location: LocationSequence, newName: string, injector: Injector): RenameAction {
    return runInInjectionContext(injector, () => new RenameAction(location, newName));
  }

  private oldName: string;

  private constructor(
    private location: LocationSequence,
    private newName: string,
  ) {
    super();
  }

  async execute() {
    if (this.location.getLevel() === 1) {
      const objectivePosition = this.location.value[0];

      this.oldName = this.context.decisionData.objectives[objectivePosition].name;
      this.context.decisionData.objectives[objectivePosition].name = this.newName;
    } else {
      this.oldName = this.context.tree().getNode(this.location).value.name;
    }

    this.context.tree.update(tree => {
      tree.getNode(this.location).value.name = this.newName;
      return clone(tree);
    });

    return true;
  }

  async undo() {
    if (this.location.getLevel() === 1) {
      const objectivePosition = this.location.value[0];
      this.context.decisionData.objectives[objectivePosition].name = this.oldName;
    }

    this.context.tree.update(tree => {
      tree.getNode(this.location).value.name = this.oldName;
      return clone(tree);
    });
  }
}
