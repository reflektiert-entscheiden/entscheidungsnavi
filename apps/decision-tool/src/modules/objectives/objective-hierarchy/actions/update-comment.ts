import { LocationSequence } from '@entscheidungsnavi/tools';
import { Injector, runInInjectionContext } from '@angular/core';
import { clone } from 'lodash';
import { ObjectiveHierarchyAction } from './action';

export class UpdateCommentAction extends ObjectiveHierarchyAction {
  static create(location: LocationSequence, newComment: string, newScaleComment: string, injector: Injector): UpdateCommentAction {
    return runInInjectionContext(injector, () => new UpdateCommentAction(location, newComment, newScaleComment));
  }

  private oldComment: string;

  private constructor(
    private location: LocationSequence,
    private newComment: string,
    private newScaleComment?: string,
  ) {
    super();
  }

  async execute() {
    if (this.location.getLevel() === 1) {
      const objectivePosition = this.location.value[0];

      this.oldComment = this.context.decisionData.objectives[objectivePosition].comment;
      this.context.decisionData.objectives[objectivePosition].comment = this.newComment;
      this.context.decisionData.objectives[objectivePosition].scaleComment = this.newScaleComment;
    } else {
      this.oldComment = this.context.tree().getNode(this.location).value.comment;
    }

    this.context.tree.update(tree => {
      tree.getNode(this.location).value.comment = this.newComment;
      tree.getNode(this.location).value.scaleComment = this.newScaleComment;
      return clone(tree);
    });

    return true;
  }

  async undo() {
    if (this.location.getLevel() === 1) {
      const objectivePosition = this.location.value[0];
      this.context.decisionData.objectives[objectivePosition].comment = this.oldComment;
    }

    this.context.tree.update(tree => {
      tree.getNode(this.location).value.comment = this.oldComment;
      return clone(tree);
    });
  }
}
