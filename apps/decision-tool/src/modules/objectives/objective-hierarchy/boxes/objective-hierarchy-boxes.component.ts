import { ChangeDetectionStrategy, Component, ElementRef, inject, Injector, Input, ViewChild } from '@angular/core';

import {
  CollapsibleTinyComponent,
  ElementRefDirective,
  HierarchyInterfaceDirective,
  WidthTriggerDirective,
} from '@entscheidungsnavi/widgets';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { takeUntilDestroyed } from '@angular/core/rxjs-interop';
import { ObjectiveHierarchyContext } from '../common/objective-hierarchy-context';
import { DeleteAction } from '../actions/delete';
import { CompoundAction } from '../actions/compound';
import { ListOrTreeLocation, ObjectiveListType } from '../common/objective-hierarchy-types';
import { MoveAction } from '../actions/move';
import { ObjectiveHierarchyInternalDragData } from '../drag/internal-drag';
import { ObjectiveHierarchyExternalDragData } from '../drag/external-drag';

@Component({
  selector: 'dt-objective-hierarchy-boxes',
  templateUrl: './objective-hierarchy-boxes.component.html',
  styleUrls: ['./objective-hierarchy-boxes.component.scss'],
  standalone: true,
  changeDetection: ChangeDetectionStrategy.OnPush,
  imports: [CollapsibleTinyComponent, MatButtonModule, MatIconModule, ElementRefDirective],
  hostDirectives: [WidthTriggerDirective],
})
export class ObjectiveHierarchyBoxesComponent {
  @Input() trashBinOpen = true;
  @Input() brainstormingOpen = true;
  @Input() brainstormingShown = true;

  private isSmallScreen = false;

  @ViewChild('trashBinBox', { read: ElementRef }) trashBinBox: ElementRef<HTMLElement>;
  @ViewChild('brainstormingBox', { read: ElementRef }) brainstormingBox: ElementRef<HTMLElement>;

  private interface = inject<HierarchyInterfaceDirective<unknown>>(HierarchyInterfaceDirective);

  constructor(
    protected context: ObjectiveHierarchyContext,
    private injector: Injector,
    widthTrigger: WidthTriggerDirective,
  ) {
    widthTrigger.dtWidthTrigger = { 'small-screen': 1000 };
    widthTrigger.dtWidthTriggerStateChange.pipe(takeUntilDestroyed()).subscribe(state => (this.isSmallScreen = state['small-screen']));
  }

  private getListElement(list: ObjectiveListType) {
    return list === 'trash' ? this.trashBinBox.nativeElement : this.brainstormingBox.nativeElement;
  }

  setListOpen(list: ObjectiveListType, open: boolean) {
    if (list === 'trash') {
      this.trashBinOpen = open;
    } else {
      this.brainstormingOpen = open;
    }

    if (open && this.isSmallScreen) {
      // Close the other list if we are on a small screen
      if (list === 'trash') {
        this.brainstormingOpen = false;
      } else {
        this.trashBinOpen = false;
      }
    }
  }

  dragOverList(event: DragEvent, list: ObjectiveListType) {
    event.preventDefault();

    // We only allow internal moves to the lists
    if (this.context.readonly || !ObjectiveHierarchyInternalDragData.isInEvent(event, this.context.hierarchyUUID)) {
      event.dataTransfer.dropEffect = 'none';
      return;
    }

    // We can't move the root to the lists
    if (this.interface?.activeDrag?.data?.some(location => location.isRoot())) {
      event.dataTransfer.dropEffect = 'none';
      return;
    }

    event.dataTransfer.dropEffect = 'move';
    this.getListElement(list).setAttribute('drag', 'true');
  }

  clearListDragIndicator(list: ObjectiveListType) {
    this.getListElement(list).removeAttribute('drag');
  }

  async dropOnList(event: DragEvent, list: ObjectiveListType) {
    event.preventDefault();

    if (this.context.readonly) return;

    const dragData = ObjectiveHierarchyInternalDragData.fromEvent(event, this.context.hierarchyUUID);
    if (dragData == null) return;

    await this.executeMoveOnDrop(dragData.data.locations, list, 0);

    this.clearListDragIndicator(list);
  }

  dragOverElement(event: DragEvent, listElement: HTMLLIElement) {
    event.preventDefault();

    if (this.context.readonly || !ObjectiveHierarchyInternalDragData.isInEvent(event, this.context.hierarchyUUID)) {
      event.dataTransfer.dropEffect = 'none';
      return;
    }

    const y = event.offsetY;
    const height = listElement.offsetHeight;

    listElement.setAttribute('drag', y > height / 2 ? 'down' : 'up');

    event.dataTransfer.dropEffect = 'move';
  }

  clearElementDragIndicator(listElement: HTMLLIElement) {
    listElement.removeAttribute('drag');
  }

  async dropOnElement(event: DragEvent, listElement: HTMLLIElement, position: number, list: ObjectiveListType) {
    event.preventDefault();
    // Prevent the event from bubbling up to the containing box
    event.stopPropagation();

    if (this.context.readonly) return;

    const dragData = ObjectiveHierarchyInternalDragData.fromEvent(event, this.context.hierarchyUUID);
    if (dragData == null) return;

    const y = event.offsetY;
    const height = listElement.offsetHeight;

    const offset = y > height / 2 ? 1 : 0;

    await this.executeMoveOnDrop(dragData.data.locations, list, position + offset);

    this.clearElementDragIndicator(listElement);
    this.clearListDragIndicator(list);
  }

  dragStartElement(event: DragEvent, position: number, list: ObjectiveListType) {
    ObjectiveHierarchyInternalDragData.writeToEvent(event, this.context.hierarchyUUID, { listLocations: [{ list, position }] });
    ObjectiveHierarchyExternalDragData.writeToEvent(event, { aspects: [this.context.getList(list)()[position]] });
  }

  private async executeMoveOnDrop(locations: ListOrTreeLocation[], list: ObjectiveListType, targetPosition: number) {
    const actions = locations.map(loc => MoveAction.create(loc, { list, position: targetPosition }, this.injector)).reverse();
    await this.context.executeAction(CompoundAction.create(actions, this.injector));
  }

  async clearTrashBin() {
    const actions = this.context.deletedAspects().map(() => DeleteAction.create({ list: 'trash', position: 0 }, this.injector));
    await this.context.executeAction(CompoundAction.create(actions, this.injector));
  }

  async deleteFromTrashBin(position: number) {
    await this.context.executeAction(DeleteAction.create({ list: 'trash', position }, this.injector));
  }
}
