import { ObjectiveElement } from '@entscheidungsnavi/decision-data';
import { pick } from 'lodash';

const EXTERNAL_OBJECTIVE_ELEMENT_PROPERTIES = ['name', 'comment', 'backgroundColor', 'textColor'] as const;
export type ExternalObjectiveElement = Pick<ObjectiveElement, (typeof EXTERNAL_OBJECTIVE_ELEMENT_PROPERTIES)[number]>;

export function toExternalObjectiveElement(element: ObjectiveElement): ExternalObjectiveElement {
  return pick(element, EXTERNAL_OBJECTIVE_ELEMENT_PROPERTIES);
}

export function parseObjectiveElement(element: ExternalObjectiveElement): ObjectiveElement {
  return new ObjectiveElement(
    element.name,
    undefined,
    element.backgroundColor,
    element.textColor,
    undefined,
    undefined,
    undefined,
    element.comment,
  );
}
