import { ITree, Tree } from '@entscheidungsnavi/tools';
import { ObjectiveElement } from '@entscheidungsnavi/decision-data';
import { ExternalObjectiveElement, parseObjectiveElement, toExternalObjectiveElement } from './external-objective-element';

type DragData = { trees: ITree<ExternalObjectiveElement>[] } | { aspects: ExternalObjectiveElement[] };

export class ObjectiveHierarchyExternalDragData {
  private static readonly dragDataType = 'application/decision-tool-objective-aspects';

  static writeToEvent(event: DragEvent, data: { trees: Tree<ObjectiveElement>[] } | { aspects: ObjectiveElement[] }) {
    if ('aspects' in data) {
      const mappedAspects = data.aspects.map(toExternalObjectiveElement);
      event.dataTransfer.setData(
        ObjectiveHierarchyExternalDragData.dragDataType,
        JSON.stringify({ aspects: mappedAspects } satisfies DragData),
      );
    } else {
      const mappedTrees = data.trees.map(tree => tree.map(toExternalObjectiveElement));
      event.dataTransfer.setData(
        ObjectiveHierarchyExternalDragData.dragDataType,
        JSON.stringify({ trees: mappedTrees } satisfies DragData),
      );
    }
  }

  static fromEvent(event: DragEvent): ObjectiveHierarchyExternalDragData | null {
    const data = event.dataTransfer.getData(ObjectiveHierarchyExternalDragData.dragDataType);
    if (!data) return null;

    try {
      const parsedData: DragData = JSON.parse(data);

      // This is an external drag, we validate the data (to some extent)
      if ('aspects' in parsedData && Array.isArray(parsedData.aspects)) {
        return new ObjectiveHierarchyExternalDragData({
          type: 'list',
          aspects: parsedData.aspects.map(parseObjectiveElement),
        });
      }

      if ('trees' in parsedData && Array.isArray(parsedData.trees)) {
        return new ObjectiveHierarchyExternalDragData({
          type: 'tree',
          trees: parsedData.trees.map(tree => Tree.from(tree).map(parseObjectiveElement)),
        });
      }

      return null;
    } catch {
      return null;
    }
  }

  static isInEvent(event: DragEvent) {
    return event.dataTransfer.types.includes(ObjectiveHierarchyExternalDragData.dragDataType);
  }

  constructor(public readonly data: { type: 'tree'; trees: Tree<ObjectiveElement>[] } | { type: 'list'; aspects: ObjectiveElement[] }) {}
}
