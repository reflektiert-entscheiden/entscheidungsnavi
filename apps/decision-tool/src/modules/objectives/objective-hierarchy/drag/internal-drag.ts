import { LocationSequence } from '@entscheidungsnavi/tools';
import { ObjectiveListLocation } from '../common/objective-hierarchy-types';

type DragData = { treeLocations: ReadonlyArray<number>[] } | { listLocations: ObjectiveListLocation[] };

export class ObjectiveHierarchyInternalDragData {
  private static getDragDataType(hierarchyUUID: string) {
    return `application/decision-tool-objective-hierarchy-${hierarchyUUID}`;
  }

  static writeToEvent(
    event: DragEvent,
    hierarchyUUID: string,
    data: { treeLocations: LocationSequence[] } | { listLocations: ObjectiveListLocation[] },
  ) {
    if ('listLocations' in data) {
      event.dataTransfer.setData(
        ObjectiveHierarchyInternalDragData.getDragDataType(hierarchyUUID),
        JSON.stringify({ listLocations: data.listLocations } satisfies DragData),
      );
    } else {
      event.dataTransfer.setData(
        ObjectiveHierarchyInternalDragData.getDragDataType(hierarchyUUID),
        JSON.stringify({ treeLocations: data.treeLocations.map(loc => loc.value) } satisfies DragData),
      );
    }
  }

  static fromEvent(event: DragEvent, hierarchyUUID: string): ObjectiveHierarchyInternalDragData | null {
    const data = event.dataTransfer.getData(ObjectiveHierarchyInternalDragData.getDragDataType(hierarchyUUID));
    if (!data) return null;

    try {
      const parsedData: DragData = JSON.parse(data);

      // Since this is an internal drag, we can assume that the locations are valid
      if ('treeLocations' in parsedData) {
        return new ObjectiveHierarchyInternalDragData({
          type: 'tree',
          locations: parsedData.treeLocations.map(loc => new LocationSequence(loc)),
        });
      }

      if ('listLocations' in parsedData) {
        return new ObjectiveHierarchyInternalDragData({
          type: 'list',
          locations: parsedData.listLocations,
        });
      }

      return null;
    } catch {
      return null;
    }
  }

  static isInEvent(event: DragEvent, hierarchyUUID: string) {
    return event.dataTransfer.types.includes(ObjectiveHierarchyInternalDragData.getDragDataType(hierarchyUUID));
  }

  constructor(
    public readonly data: { type: 'tree'; locations: LocationSequence[] } | { type: 'list'; locations: ObjectiveListLocation[] },
  ) {}
}
