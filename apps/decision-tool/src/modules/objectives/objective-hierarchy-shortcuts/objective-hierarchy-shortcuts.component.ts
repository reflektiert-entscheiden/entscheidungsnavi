import { Component } from '@angular/core';
import { PlatformDetectService, WidgetsModule } from '@entscheidungsnavi/widgets';
import { MatIconModule } from '@angular/material/icon';

import { MatDialogRef } from '@angular/material/dialog';

@Component({
  templateUrl: './objective-hierarchy-shortcuts.component.html',
  styleUrls: ['./objective-hierarchy-shortcuts.component.scss'],
  imports: [MatIconModule, WidgetsModule],
  standalone: true,
})
export class ObjectiveHierarchyShortcutsComponent {
  protected readonly isMacOS: boolean;

  constructor(
    platformDetectService: PlatformDetectService,
    protected dialogRef: MatDialogRef<ObjectiveHierarchyShortcutsComponent>,
  ) {
    this.isMacOS = platformDetectService.macOS;
  }
}
