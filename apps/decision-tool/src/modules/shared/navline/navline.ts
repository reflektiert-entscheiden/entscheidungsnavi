import { ElementRef } from '@angular/core';
import { negate, stubTrue } from 'lodash';
import { ButtonColor } from '@entscheidungsnavi/widgets';

export type IconPosition = 'left' | 'right';

export interface Navigation {
  navLine: NavLine | null;
}

export function hasNavline(element: any): element is Navigation {
  return !!element.navLine;
}

type NavLineCollection = { visibilityCondition: () => boolean; elements: NavLineElement[] };

type NavLineSlot = 'left' | 'middle' | 'right';

export class NavLine {
  private slotToNavLineCollections: Record<NavLineSlot, NavLineCollection[]> = {
    left: [],
    middle: [],
    right: [],
  };

  constructor(defaults: { left?: NavLineElement[]; middle?: NavLineElement[]; right?: NavLineElement[] } = {}) {
    this.addWhen(stubTrue, defaults);
  }

  addWhen(
    visibilityCondition: () => boolean,
    { left, middle, right }: { left?: NavLineElement[]; middle?: NavLineElement[]; right?: NavLineElement[] },
  ) {
    if (left) this.slotToNavLineCollections.left.push({ visibilityCondition, elements: left });
    if (middle) this.slotToNavLineCollections.middle.push({ visibilityCondition, elements: middle });
    if (right) this.slotToNavLineCollections.right.push({ visibilityCondition, elements: right });
    return this;
  }

  getElements(slot: NavLineSlot = 'middle') {
    return this.slotToNavLineCollections[slot];
  }

  getVisibleElements(slot: NavLineSlot = 'middle') {
    // Every collection as well as every element in every collection may be disabled
    return this.slotToNavLineCollections[slot].flatMap(ec =>
      ec.visibilityCondition() ? ec.elements.filter(element => element.isVisible()) : [],
    );
  }
}

class NavLineElement {
  static builder = class {
    readonly element: NavLineElement;

    constructor() {
      this.element = new NavLineElement();
    }

    back(link?: string | (() => string)) {
      return this.link(link)
        .icon('navigate_before', 'left')
        .tooltip($localize`Zurück`);
    }

    continue(link?: string | (() => string)) {
      return this.link(link)
        .icon('navigate_next', 'right')
        .tooltip($localize`Weiter`);
    }

    label(label: string | (() => string)) {
      if (typeof label === 'string') {
        this.element.label = label;
        this.element.labelFunction = undefined;
        this.element.hasDynamicLabel = false;
      } else {
        this.element.label = undefined;
        this.element.labelFunction = label;
        this.element.hasDynamicLabel = true;
      }

      return this;
    }

    tooltip(tooltip: string | (() => string)) {
      if (typeof tooltip === 'string') {
        this.element.tooltip = tooltip;
        this.element.tooltipFunction = undefined;
        this.element.hasDynamicTooltip = false;
      } else {
        this.element.tooltip = undefined;
        this.element.tooltipFunction = tooltip;
        this.element.hasDynamicTooltip = true;
      }

      return this;
    }

    link(link: string | (() => string)) {
      if (typeof link === 'string') {
        this.element.routerLink = link;
        this.element.routerLinkFunction = undefined;
        this.element.hasDynamicRouterLink = false;
      } else if (typeof link === 'function') {
        this.element.routerLink = undefined;
        this.element.routerLinkFunction = link;
        this.element.hasDynamicRouterLink = true;
      }

      return this;
    }

    icon(name: string, position: IconPosition = 'right') {
      this.element.icon = name;
      this.element.iconPosition = position;

      return this;
    }

    color(buttonColor: ButtonColor | (() => ButtonColor)) {
      if (typeof buttonColor === 'string') {
        this.element.buttonColor = buttonColor;
        this.element.buttonColorFunction = undefined;
        this.element.hasDynamicButtonColor = false;
      } else {
        this.element.buttonColor = undefined;
        this.element.buttonColorFunction = buttonColor;
        this.element.hasDynamicButtonColor = true;
      }

      return this;
    }

    cypressId(id: string) {
      this.element.cypressId = id;

      return this;
    }

    disabled(predicate: () => boolean) {
      this.element.disabled = predicate;

      return this;
    }

    condition(predicate: () => boolean) {
      this.element.visible = predicate;

      return this;
    }

    onClick(handler: (elementRef: ElementRef<HTMLElement>) => void) {
      this.element.onClick = handler;

      return this;
    }

    orElse(secondElement: (builder: typeof NavLineElement.builder.prototype) => void) {
      const secondBuilder = navLineElement();
      secondBuilder.condition(negate(this.element.visible));

      secondElement(secondBuilder);

      return [this.build(), secondBuilder.build()];
    }

    build() {
      const hasLabel = this.element.label || this.element.hasDynamicLabel;

      if (!hasLabel && !this.element.icon) throw new Error('Cannot create a NavLineElement with neither an icon nor a label.');

      if (!hasLabel && !this.element.tooltip) throw new Error('An icon-only button must have a tooltip.');

      return this.element;
    }
  };

  private label: string;
  private labelFunction: () => string;
  private hasDynamicLabel: boolean;

  private tooltip: string;
  private tooltipFunction: () => string;
  private hasDynamicTooltip: boolean;

  private routerLink: string;
  private routerLinkFunction: () => string;
  private hasDynamicRouterLink: boolean;

  private buttonColor: ButtonColor = 'accent';
  private buttonColorFunction: () => ButtonColor;
  private hasDynamicButtonColor: boolean;

  private icon: string;
  private iconPosition: IconPosition = 'right';

  private cypressId: string;

  private disabled: () => boolean;
  private visible: () => boolean;

  private onClick: (elementRef: ElementRef<HTMLElement>) => void;

  // We only allow building NavLineElements in the Builder. Only there we benefit from a validation at the end.
  // eslint-disable-next-line @typescript-eslint/no-empty-function
  private constructor() {}

  getLabel() {
    return this.hasDynamicLabel ? this.labelFunction() : this.label;
  }

  getTooltip() {
    return this.hasDynamicTooltip ? this.tooltipFunction() : this.tooltip;
  }

  getRouterLink() {
    return this.hasDynamicRouterLink ? this.routerLinkFunction() : this.routerLink;
  }

  getIcon() {
    return this.icon;
  }

  getIconPosition() {
    return this.iconPosition;
  }

  getButtonColor() {
    return this.hasDynamicButtonColor ? this.buttonColorFunction() : this.buttonColor;
  }

  getCypressId() {
    return this.cypressId;
  }

  isDisabled() {
    return this.disabled ? this.disabled() : false;
  }

  isVisible() {
    return this.visible ? this.visible() : true;
  }

  handleClick(elementRef: ElementRef<HTMLElement>) {
    if (this.onClick) {
      this.onClick(elementRef);
    }
  }
}

export function navLineElement() {
  return new NavLineElement.builder();
}
