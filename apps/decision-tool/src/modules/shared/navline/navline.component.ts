import { Component, Input } from '@angular/core';
import { NavLine } from './navline';

@Component({
  selector: 'dt-navline',
  styleUrls: ['./navline.component.scss'],
  templateUrl: './navline.component.html',
})
export class NavlineComponent {
  @Input() navLine: NavLine | null;
}
