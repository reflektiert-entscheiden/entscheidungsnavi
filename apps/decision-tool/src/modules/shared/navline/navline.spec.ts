import { navLineElement } from './navline';

function getNavLineWithDefaultValues() {
  return navLineElement().label('foo').build();
}

// Checks everything unrelated to label, icon and tooltip.
function buildAndDoBasicChecks(builderToCheckElementFrom: ReturnType<typeof navLineElement>) {
  let navLineToCheck = null;

  for (const isVisible of [false, true]) {
    for (const isDisabled of [false, true]) {
      const mockLinkFunction = jest.fn(() => `/foo/bar`);
      const mockDisabledCondition = jest.fn(() => isDisabled);
      const mockVisibilityCondition = jest.fn(() => isVisible);
      const mockClickHandler = jest.fn();

      navLineToCheck = builderToCheckElementFrom
        .link(mockLinkFunction)
        .disabled(mockDisabledCondition)
        .condition(mockVisibilityCondition)
        .onClick(mockClickHandler)
        .build();

      const navLineWithDefaultValues = getNavLineWithDefaultValues();

      expect(navLineToCheck.getRouterLink()).toStrictEqual(mockLinkFunction());

      expect(navLineToCheck.getButtonColor()).toStrictEqual(navLineWithDefaultValues.getButtonColor());

      expect(navLineToCheck.isDisabled()).toStrictEqual(isDisabled);

      expect(navLineToCheck.isVisible()).toStrictEqual(isVisible);

      navLineToCheck.handleClick(null);
    }
  }

  return navLineToCheck;
}

describe('NavLineElement.Builder', () => {
  it('instantiates without an error', () => {
    expect(() => navLineElement()).not.toThrow();
  });

  it('builds with a minimal configuration', () => {
    expect(() => navLineElement().label('foo').build()).not.toThrow();
    expect(() =>
      navLineElement()
        .label(() => 'foo')
        .build(),
    ).not.toThrow();
    expect(() => navLineElement().icon('foo-button').tooltip('bar').build()).not.toThrow();
  });

  it('rejects an invalid minimal configuration', () => {
    expect(navLineElement().build).toThrow();
    expect(navLineElement().icon('foo-button').build).toThrow();
  });

  it('builds with static label but without an icon', () => {
    const navLine = buildAndDoBasicChecks(navLineElement().label('foo'));

    const navLineWithDefaultValues = getNavLineWithDefaultValues();

    expect(navLine.getLabel()).toStrictEqual('foo');

    expect(navLine.getIcon()).toStrictEqual(navLine.getIcon());
    expect(navLine.getIconPosition()).toStrictEqual(navLineWithDefaultValues.getIconPosition());
    expect(navLine.getTooltip()).toStrictEqual(navLine.getTooltip());
  });

  it('builds with dynamic label but without an icon', () => {
    const mockDynamicLabel = jest.fn(() => 'foo');

    const navLine = buildAndDoBasicChecks(navLineElement().label(mockDynamicLabel));

    const navLineWithDefaultValues = getNavLineWithDefaultValues();

    expect(navLine.getLabel()).toStrictEqual(mockDynamicLabel());

    expect(navLine.getIcon()).toStrictEqual(navLineWithDefaultValues.getIcon());
    expect(navLine.getIconPosition()).toStrictEqual(navLineWithDefaultValues.getIconPosition());

    expect(navLine.getTooltip()).toStrictEqual(navLineWithDefaultValues.getTooltip());
  });

  it('builds with static label and an icon', () => {
    for (const leftOrRightIconPosition of ['left', 'right']) {
      const navLine = buildAndDoBasicChecks(
        navLineElement()
          .icon('bar', leftOrRightIconPosition as 'left' | 'right')
          .label('foo'),
      );

      expect(navLine.getLabel()).toStrictEqual('foo');

      expect(navLine.getIcon()).toStrictEqual('bar');
      expect(navLine.getIconPosition()).toStrictEqual(leftOrRightIconPosition);

      expect(navLine.getTooltip()).toStrictEqual(navLine.getTooltip());
    }
  });

  it('builds with dynamic label, icon and tooltip', () => {
    for (const leftOrRightIconPosition of ['left', 'right']) {
      const mockDynamicLabel = jest.fn(() => 'foobar');

      const navLine = buildAndDoBasicChecks(
        navLineElement()
          .icon('bar', leftOrRightIconPosition as 'left' | 'right')
          .label(mockDynamicLabel)
          .tooltip('foo'),
      );

      expect(navLine.getLabel()).toStrictEqual(mockDynamicLabel());

      expect(navLine.getIcon()).toStrictEqual('bar');
      expect(navLine.getIconPosition()).toStrictEqual(leftOrRightIconPosition);

      expect(navLine.getTooltip()).toStrictEqual('foo');
    }
  });
});
