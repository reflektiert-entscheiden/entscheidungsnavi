import { ChangeDetectionStrategy, Component, ElementRef, EventEmitter, inject, Input, Output } from '@angular/core';
import { ButtonColor } from '@entscheidungsnavi/widgets';
import { IconPosition } from '../navline';

@Component({
  selector: 'dt-navline-element',
  styleUrls: ['./navline-element.component.scss'],
  templateUrl: './navline-element.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class NavLineElementComponent {
  @Input()
  label: string;

  @Input()
  tooltip: string;

  @Input()
  disabled: boolean;

  @Input()
  icon: string;

  @Input()
  iconPosition: IconPosition;

  @Input()
  color: ButtonColor;

  @Input()
  minimized: boolean;

  @Input()
  link: string;

  @Input()
  cypressId: string;

  @Output()
  buttonClick = new EventEmitter<ElementRef<HTMLElement>>();

  elementRef: ElementRef<HTMLElement> = inject(ElementRef);

  get iconOnly() {
    return !this.label || this.minimized;
  }

  get cypressIdWithPrefix() {
    if (!this.cypressId) {
      return undefined;
    }
    return `navline-element-${this.cypressId}`;
  }
}
