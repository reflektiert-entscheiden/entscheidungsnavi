import { Component, Input } from '@angular/core';
import { EC1num, EC1str, EC2str, EC3str, ErrorMsg } from '@entscheidungsnavi/decision-data';

@Component({
  selector: 'dt-error-msg',
  templateUrl: './error-msg.component.html',
  styles: ['p { margin: 0;text-align: left; }'],
})
export class ErrorMsgComponent {
  @Input() error: ErrorMsg;

  // This is done to enable type checking in the template
  get ec0() {
    return !('args' in this.error) ? this.error : undefined;
  }
  get ec1() {
    return 'args' in this.error && this.error.args.length === 1 ? (this.error as EC1num | EC1str) : undefined;
  }
  get ec2() {
    return 'args' in this.error && this.error.args.length === 2 ? (this.error as EC2str) : undefined;
  }
  get ec3() {
    return 'args' in this.error && this.error.args.length === 3 ? (this.error as EC3str) : undefined;
  }
}
