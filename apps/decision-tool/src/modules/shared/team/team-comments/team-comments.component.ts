import { ChangeDetectorRef, Component, Host, HostBinding, Input, NgZone, OnInit, Optional, TrackByFunction } from '@angular/core';
import { TeamComment, doesTeamMemberHaveOwnerRights } from '@entscheidungsnavi/api-types';
import { OnDestroyObservable } from '@entscheidungsnavi/tools';
import { NoteBtnComponent } from '@entscheidungsnavi/widgets';
import { MatSnackBar } from '@angular/material/snack-bar';
import { catchError, exhaustMap, filter, interval, map, Observable, of, startWith, takeUntil } from 'rxjs';
import { ProjectService } from '../../../../app/data/project';
import { TeamProject } from '../../../../app/data/project/team-project';
import { getTeamMemberDisplayName } from '../team-member-name.component';

@Component({
  selector: 'dt-team-comments',
  templateUrl: './team-comments.component.html',
  styleUrls: ['./team-comments.component.scss'],
})
export class TeamCommentsComponent implements OnInit {
  @Input()
  object: { uuid: string };

  @Input()
  firstUnreadCommentId: string;

  @Input()
  noteBtnRef: NoteBtnComponent;

  @OnDestroyObservable()
  onDestroy$: Observable<void>;

  @HostBinding('class.active')
  get isTeamProject() {
    return this.projectService.getProject(TeamProject) != null;
  }

  get objectId() {
    return this.object.uuid;
  }

  get comments() {
    if (!this.isTeamProject) {
      return [];
    }

    return this.team.comments.filter(c => c.objectId === this.objectId).reverse();
  }

  get userMemberId() {
    return this.teamProject.userMember.id;
  }

  get userMember() {
    return this.teamProject.userMember;
  }

  get teamProject() {
    return this.projectService.getProject(TeamProject);
  }

  get team() {
    return this.teamProject.team;
  }

  constructor(
    @Host() @Optional() private noteBtn: NoteBtnComponent,
    private projectService: ProjectService,
    private cdRef: ChangeDetectorRef,
    private snackBar: MatSnackBar,
    private zone: NgZone,
  ) {}

  trackCommentById: TrackByFunction<TeamComment> = (_, comment) => comment.id;

  ngOnInit() {
    if (!this.noteBtn) {
      this.noteBtn = this.noteBtnRef;
    }

    if (this.noteBtn) {
      this.noteBtn.hasTeamComments = this.comments.length > 0;
    }

    this.zone.runOutsideAngular(() => {
      interval(5000)
        .pipe(
          startWith(0),
          map(() => this.isTeamProject),
          filter(isTeamProject => isTeamProject && (this.noteBtn == null || this.noteBtn.isOpen)),
          exhaustMap(_ => this.teamProject.updateCommentsFor(this.objectId).pipe(catchError(_ => of()))),
          takeUntil(this.onDestroy$),
        )
        .subscribe(_ => {
          this.zone.run(() => {
            this.commentsChanged();
          });
        });
    });
  }

  commentsChanged() {
    if (this.noteBtn) {
      this.noteBtn.hasTeamComments = this.comments.length > 0;
    }

    this.cdRef.markForCheck();
  }

  getAuthorDisplayNameForComment(comment: TeamComment) {
    const member = this.teamProject.getTeamMemberFromMemberId(comment.authorMember);
    return getTeamMemberDisplayName(member);
  }

  canDeleteComment(comment: TeamComment) {
    return comment.authorMember === this.teamProject.userMember.id || doesTeamMemberHaveOwnerRights(this.teamProject.userMember);
  }

  addComment(input: HTMLInputElement) {
    const content = input.value;

    if (!content) {
      return;
    }

    this.teamProject.addComment(this.objectId, content).subscribe({
      next: () => this.commentsChanged(),
      error: () => this.snackBar.open($localize`Beim Hinzufügen des Kommentars ist ein Fehler aufgetreten`, $localize`Ok`),
    });

    input.value = '';
  }

  deleteComment(commentId: string) {
    this.teamProject.deleteComment(commentId).subscribe({
      next: () => this.commentsChanged(),
      error: () => this.snackBar.open($localize`Beim Löschen des Kommentars ist ein Fehler aufgetreten`, $localize`Ok`),
    });
  }
}
