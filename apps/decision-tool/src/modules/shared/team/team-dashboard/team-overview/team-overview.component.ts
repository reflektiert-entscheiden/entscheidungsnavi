import { Component, Inject, Input, LOCALE_ID, OnInit, ViewChild } from '@angular/core';
import { TeamsService } from '@entscheidungsnavi/api-client';
import { MatSnackBar } from '@angular/material/snack-bar';
import { assertUnreachable } from '@entscheidungsnavi/tools';
import { tap, concatMap } from 'rxjs';
import { OverlayProgressBarDirective } from '@entscheidungsnavi/widgets/overlay-progress-bar';
import { TeamCalc } from '@entscheidungsnavi/decision-data';
import {
  TeamTaskDto,
  TeamMemberDto,
  TeamDto,
  EditTokenInfo,
  TakeEditTokenInfo,
  doesTeamMemberHaveOwnerRights,
  doesTeamMemberHaveEditorRights,
} from '@entscheidungsnavi/api-types';
import { ProjectService } from '../../../../../app/data/project';
import { TeamProject } from '../../../../../app/data/project/team-project';

@Component({
  selector: 'dt-team-overview',
  templateUrl: './team-overview.component.html',
  styleUrls: ['./team-overview.component.scss'],
})
export class TeamOverviewComponent implements OnInit {
  @Input()
  teamId: string;

  userTasks: { task: TeamTaskDto; assignedTo: TeamMemberDto }[];

  team: TeamDto;
  teamCalc: TeamCalc;

  @ViewChild('whiteboardProgress')
  whiteboardProgress: OverlayProgressBarDirective;

  editTokenData: [info: EditTokenInfo, takeInfo: TakeEditTokenInfo];

  protected loaded = false;

  protected editingWhiteboard = false;

  get doesUserHaveOwnerRights() {
    return doesTeamMemberHaveOwnerRights(this.userMember);
  }

  get doesUserHaveEditorRights() {
    return doesTeamMemberHaveEditorRights(this.userMember);
  }

  get teamProject() {
    return this.projectService.getProject(TeamProject);
  }

  get userMember() {
    return this.teamProject.userMember;
  }

  get tokenExpiryText() {
    const token = this.teamProject.validEditToken;

    if (!token) {
      return null;
    }

    const team = this.team;

    if (this.editTokenData[0].state !== 'valid') {
      return null;
    }

    const canTokenExpireForHolder =
      team.editRightExpiresFor === 'all' || (team.editRightExpiresFor === 'editor' && this.editTokenData[0].holder.role === 'editor');

    if (!canTokenExpireForHolder) {
      return null;
    }

    const timeFormat = new Intl.RelativeTimeFormat(this.locale, { style: 'long' });

    const expiresAt = new Date(token.expiresAt);

    const expiresInSeconds = Math.abs((expiresAt.getTime() - Date.now()) / 1000);

    const expiresInMinutes = expiresInSeconds / 60;

    if (expiresInMinutes > 1) {
      return $localize`Verfällt ${timeFormat.format(Math.round(expiresInMinutes), 'minute')}`;
    } else {
      return $localize`Verfällt ${timeFormat.format(Math.round(expiresInSeconds), 'second')}`;
    }
  }

  get editRightStatusTooltip() {
    if (this.editTokenData[0].state !== 'valid') {
      return;
    }

    const status = this.editTokenData[1];

    if (status.canTake === true) {
      return;
    }

    switch (status.reason) {
      case 'no-editor':
        return $localize`Du bist kein Editor`;
      case 'already-holding':
        return $localize`Du hast bereits das Bearbeitungsrecht`;
      case 'cant-take-from-holder':
        return $localize`Du kannst das Bearbeitungsrecht nicht vom aktuellen Inhaber übernehmen`;
      default:
        assertUnreachable(status.reason);
    }
  }

  constructor(
    private projectService: ProjectService,
    private teamsService: TeamsService,
    private snackBar: MatSnackBar,
    @Inject(LOCALE_ID) private locale: string,
  ) {}

  ngOnInit() {
    this.teamsService.getTeam(this.teamId).subscribe({
      next: team => {
        this.team = team;
        this.teamCalc = new TeamCalc(team);
        this.loaded = true;

        const userMember = this.team.members.find(userMemberId => userMemberId.id === this.teamProject.userMemberId);

        this.userTasks = userMember?.tasks.map(task => ({ task, assignedTo: userMember })) ?? [];

        this.setEditTokenInfo();
      },
      error: () => this.snackBar.open($localize`Fehler beim Laden der Teamdaten.`, 'Ok', { duration: 5000 }),
    });
  }

  private setEditTokenInfo() {
    this.editTokenData = [this.team.editTokenInfo, this.team.canMemberTakeEditToken(this.userMember)];
  }

  toggleWhiteboardEdit() {
    this.editingWhiteboard = !this.editingWhiteboard;

    if (!this.editingWhiteboard) {
      this.teamsService
        .updateTeam(this.team.id, { whiteboard: this.team.whiteboard })
        .pipe(this.whiteboardProgress.reactivePipe())
        .subscribe({
          error: () => this.snackBar.open($localize`Fehler beim Speichern des Whiteboards`, 'Ok', { duration: 5000 }),
        });
    }
  }

  returnEditToken() {
    return this.teamProject.returnEditToken().subscribe({
      next: () => {
        this.team.editToken = null;
        this.setEditTokenInfo();
      },
      error: () => {
        this.snackBar.open($localize`Beim Zurückgeben des Bearbeitungsrechts ist ein Fehler aufgetreten.`, $localize`Ok`);
      },
    });
  }

  takeEditToken() {
    this.teamsService
      .takeEditToken(this.team.id)
      .pipe(
        tap({
          next: token => {
            if (token.holder !== this.teamProject.userMember.id) {
              throw new Error('Token holder is not the current user');
            }

            this.team.editToken = token;
            this.setEditTokenInfo();
          },
        }),
        concatMap(_ => this.projectService.loadTeamProject(this.team.id, { forceUnload: true })),
      )
      .subscribe({
        error: () => {
          this.snackBar.open($localize`Beim Anfordern des Bearbeitungsrechts ist ein Fehler aufgetreten.`, $localize`Ok`);
        },
      });
  }
}
