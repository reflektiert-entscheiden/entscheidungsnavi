import { Overlay, OverlayRef } from '@angular/cdk/overlay';
import { ComponentPortal } from '@angular/cdk/portal';
import { Injectable } from '@angular/core';
import { ProjectService } from '../../../../app/data/project';
import { TeamProject } from '../../../../app/data/project/team-project';
import { TeamDashboardComponent } from './team-dashboard.component';

@Injectable({ providedIn: 'root' })
export class TeamDashboardService {
  private openOverlayRef: OverlayRef = null;

  constructor(
    private overlay: Overlay,
    private projectService: ProjectService,
  ) {
    projectService.project$.subscribe(project => {
      if (project instanceof TeamProject) {
        this.openTeamDashboard();
      } else {
        if (this.openOverlayRef !== null && this.openOverlayRef.hasAttached()) {
          this.openOverlayRef.dispose();
        }
      }
    });
  }

  openTeamDashboard() {
    if (this.openOverlayRef !== null && this.openOverlayRef.hasAttached()) {
      return;
    }

    this.openOverlayRef = this.overlay.create();
    const userProfilePortal = new ComponentPortal(TeamDashboardComponent);
    const componentRef = this.openOverlayRef.attach(userProfilePortal);
    componentRef.instance.overlayRef = this.openOverlayRef;
  }
}
