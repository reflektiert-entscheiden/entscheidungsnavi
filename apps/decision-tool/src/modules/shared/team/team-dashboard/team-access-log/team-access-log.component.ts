import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { TeamsService } from '@entscheidungsnavi/api-client';
import { MatSnackBar } from '@angular/material/snack-bar';
import { getTeamMemberDisplayName } from '../../team-member-name.component';
import { ProjectService } from '../../../../../app/data/project';
import { TeamProject } from '../../../../../app/data/project/team-project';

type TableRow = { memberName: string; from: Date; to: Date };

@Component({
  selector: 'dt-team-access-log',
  templateUrl: './team-access-log.component.html',
  styleUrls: ['./team-access-log.component.scss'],
})
export class TeamAccessLogComponent implements OnInit {
  @Input({ required: true })
  teamId: string;

  dataSource: MatTableDataSource<TableRow>;

  @ViewChild(MatSort) set sort(sort: MatSort) {
    this.dataSource.sort = sort;
  }

  constructor(
    private projectService: ProjectService,
    private teamsService: TeamsService,
    private snackBar: MatSnackBar,
  ) {
    this.dataSource = new MatTableDataSource();
  }

  ngOnInit() {
    const teamProject = this.projectService.getProject(TeamProject);
    if (!teamProject) {
      return;
    }

    this.teamsService.getTeam(this.teamId).subscribe({
      next: ({ accessLog }) => {
        this.dataSource.data = accessLog.editToken
          .filter(entry => entry.to != null)
          .map(entry => {
            const member = teamProject.getTeamMemberFromMemberId(entry.member);
            const memberName = member ? getTeamMemberDisplayName(member) : $localize`Unbekanntes Mitglied`;
            return {
              memberName,
              from: entry.from,
              to: entry.to,
            };
          });
      },
      error: () => {
        this.snackBar.open($localize`Fehler beim Laden des Zugriffsprotokolls`, 'Ok', { duration: 5000 });
      },
    });
  }
}
