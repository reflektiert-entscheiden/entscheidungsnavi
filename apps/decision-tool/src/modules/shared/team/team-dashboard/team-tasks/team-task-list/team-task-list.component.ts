import { Component, Input, OnChanges, OnInit, SimpleChanges, ViewChild } from '@angular/core';
import { TeamsService } from '@entscheidungsnavi/api-client';
import { MatDialog } from '@angular/material/dialog';
import { MatTableDataSource } from '@angular/material/table';
import { MatSort, SortDirection } from '@angular/material/sort';
import { _isNumberValue } from '@angular/cdk/coercion';
import { DatePipe } from '@angular/common';
import { TeamCalc } from '@entscheidungsnavi/decision-data';
import { TeamDto, TeamTaskDto, TeamMemberDto } from '@entscheidungsnavi/api-types';
import { TeamTaskModalComponent, TeamTaskModalData } from '../team-task-modal/team-task-modal.component';
import { TeamTaskState, TeamTaskStates, TeamsTasksService } from '../team-tasks.service';
import { getTeamMemberDisplayName } from '../../../team-member-name.component';

@Component({
  selector: 'dt-team-task-list',
  templateUrl: './team-task-list.component.html',
  styleUrls: ['./team-task-list.component.scss'],
})
export class TeamTaskListComponent implements OnInit, OnChanges {
  @Input()
  team: TeamDto;

  @Input()
  tasks: { task: TeamTaskDto; assignedTo: TeamMemberDto }[];

  @Input()
  shownColumns: ('type' | 'assignedTo' | 'created' | 'modified' | 'state')[] = ['type', 'state'];

  @Input()
  showFilter = true;

  @Input()
  defaultSortColumn: (typeof this.shownColumns)[number] = 'state';

  @Input()
  defaultSortDirection: SortDirection = 'asc';

  @Input()
  teamCalc: TeamCalc;

  protected tableRows: {
    task: TeamTaskDto;
    taskName: string;
    taskNameSubtitle?: string;
    assignedTo: TeamMemberDto;
    state: [TeamTaskState, string, string];
  }[];
  dataSource: MatTableDataSource<(typeof this.tableRows)[number]>;

  @ViewChild(MatSort) set sort(sort: MatSort) {
    this.dataSource.sort = sort;
  }

  constructor(
    protected teamsTasksService: TeamsTasksService,
    private dialog: MatDialog,
    private teamsService: TeamsService,
    private datePipe: DatePipe,
  ) {
    this.dataSource = new MatTableDataSource();
    this.dataSource.sortingDataAccessor = this.sortingDataAccessor;
    this.dataSource.filterPredicate = (data, filter) => {
      const filterLower = filter.toLowerCase();
      return (
        data.taskName.toLowerCase().includes(filterLower) ||
        getTeamMemberDisplayName(data.assignedTo).toLowerCase().includes(filterLower) ||
        data.state[1].toLowerCase().includes(filterLower) ||
        data.state[2]?.toLowerCase().includes(filterLower) ||
        this.datePipe.transform(data.task.createdAt, 'short').toLowerCase().includes(filterLower) ||
        this.datePipe.transform(data.task.updatedAt, 'short').toLowerCase().includes(filterLower)
      );
    };
  }

  sortingDataAccessor = (data: (typeof this.tableRows)[number], sortHeaderId: string): string | number => {
    if (sortHeaderId === 'state') {
      return TeamTaskStates.indexOf(data.state[0]);
    }

    let value = null;
    if (sortHeaderId.indexOf('.') !== -1) {
      const ids = sortHeaderId.split('.');
      value = (data as any)[ids[0]][ids[1]];
    } else {
      value = (data as any)[sortHeaderId];
    }

    return _isNumberValue(value) ? Number(value) : value;
  };

  ngOnInit() {
    this.updateTableRows();
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.tasks && !changes.tasks.firstChange) {
      this.updateTableRows();
    }
  }

  openTaskModal({ task, assignedTo }: { task: TeamTaskDto; assignedTo: TeamMemberDto }) {
    this.dialog
      .open<TeamTaskModalComponent, TeamTaskModalData>(TeamTaskModalComponent, {
        data: { team: this.team, task, member: assignedTo, aggregatedProject: this.teamCalc.aggregatedProject },
      })
      .afterClosed()
      .subscribe(_ => this.updateTableRows());
  }

  private updateTableRows() {
    const unknownObjective = $localize`Unbekanntes Ziel`;
    this.tableRows = this.tasks.map(({ task, assignedTo }) => ({
      task,
      taskName: this.teamsTasksService.getTaskTypeName(task),
      taskNameSubtitle:
        task.type === 'objective-scale'
          ? $localize`für Ziel „${
              this.teamCalc.aggregatedProject.objectives.find(objective => objective.uuid === task.objectiveUUID)?.name ?? unknownObjective
            }“`
          : null,
      assignedTo,
      state: this.teamsTasksService.getTaskState(task, this.teamCalc.aggregatedProject),
    }));

    this.dataSource.data = this.tableRows;
  }
}
