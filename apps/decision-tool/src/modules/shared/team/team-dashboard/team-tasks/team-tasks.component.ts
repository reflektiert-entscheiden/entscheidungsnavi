import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Observable, lastValueFrom, switchMap, takeUntil, tap } from 'rxjs';
import { TeamsService } from '@entscheidungsnavi/api-client';
import { FormControl, FormGroup } from '@angular/forms';
import { OnDestroyObservable } from '@entscheidungsnavi/tools';
import { OverlayProgressBarDirective } from '@entscheidungsnavi/widgets/overlay-progress-bar';
import { MatSnackBar } from '@angular/material/snack-bar';
import { TeamCalc } from '@entscheidungsnavi/decision-data';
import { TeamTaskDto, TeamMemberDto, TeamDto } from '@entscheidungsnavi/api-types';
import { ProjectService } from '../../../../../app/data/project';
import {
  TeamAssignTaskModalComponent,
  TeamAssignTaskModalData,
  TeamAssignTaskModalReturnType,
} from './team-assign-task-modal/team-assign-task-modal.component';
import { TeamsTasksService } from './team-tasks.service';

type TaskWithMember = {
  task: TeamTaskDto;
  assignedTo: TeamMemberDto;
};

@Component({
  selector: 'dt-team-tasks',
  templateUrl: './team-tasks.component.html',
  styleUrls: ['./team-tasks.component.scss'],
})
export class TeamTasksComponent implements OnInit {
  @Input()
  teamId: string;

  allTasks: TaskWithMember[];

  lockedTasksFormGroup = new FormGroup({
    lockedWeightTasks: new FormControl<boolean>(false),
    lockedOpinionTasks: new FormControl<boolean>(false),
  });

  @OnDestroyObservable()
  onDestroy$: Observable<void>;

  @ViewChild('optionSaveProgress')
  optionSaveProgress: OverlayProgressBarDirective;

  protected team: TeamDto;
  protected teamCalc: TeamCalc;

  constructor(
    private projectService: ProjectService,
    private dialog: MatDialog,
    protected teamTasksService: TeamsTasksService,
    protected teamsService: TeamsService,
    private snackBar: MatSnackBar,
  ) {}

  ngOnInit() {
    this.teamsService.getTeam(this.teamId).subscribe({
      next: team => {
        const members = team.members;

        this.team = team;
        this.teamCalc = new TeamCalc(this.team);
        this.allTasks = [];

        for (const member of members) {
          const tasks = member.tasks;

          this.allTasks.push(...tasks.map(task => ({ task, assignedTo: member })));
        }

        this.lockedTasksFormGroup.controls.lockedOpinionTasks.setValue(team.lockedOpinionTasks, { emitEvent: false });
        this.lockedTasksFormGroup.controls.lockedWeightTasks.setValue(team.lockedWeightTasks, { emitEvent: false });
        this.lockedTasksFormGroup.valueChanges
          .pipe(
            switchMap(values =>
              this.teamsService.updateTeam(this.teamId, values).pipe(
                tap({
                  next: () => {
                    this.team.lockedWeightTasks = values.lockedWeightTasks;
                    this.team.lockedOpinionTasks = values.lockedOpinionTasks;
                  },
                }),
                this.optionSaveProgress.reactivePipe(),
              ),
            ),
            takeUntil(this.onDestroy$),
          )
          .subscribe();
      },
      error: () => {
        this.snackBar.open($localize`Fehler beim Laden der Teamdaten.`, 'Ok', { duration: 5000 });
      },
    });
  }

  async assignTask() {
    const createdTasks = await lastValueFrom(
      this.dialog
        .open<TeamAssignTaskModalComponent, TeamAssignTaskModalData, TeamAssignTaskModalReturnType>(TeamAssignTaskModalComponent, {
          data: {
            teamId: this.teamId,
            teamCalc: this.teamCalc,
            members: this.team.members,
          },
        })
        .afterClosed(),
    );

    if (createdTasks) {
      this.allTasks = [...this.allTasks, ...createdTasks];
    }
  }
}
