import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { TeamsService } from '@entscheidungsnavi/api-client';
import { AlternativeOpinions } from '@entscheidungsnavi/api-types';
import { Alternative } from '@entscheidungsnavi/decision-data';
import { cloneDeep } from 'lodash';

export type TeamTaskOpinionModalData = {
  teamId: string;
  teamMemberId: string;
  alternatives: Alternative[];
  alternativeOpinions: AlternativeOpinions;
};

export type TeamTaskOpinionModalReturn = AlternativeOpinions;

type StableAlternativeOpnions = AlternativeOpinions | Record<string, { sentiment: boolean | 'unset'; comment: string }>;

@Component({
  templateUrl: './team-task-opinion-modal.component.html',
  styleUrls: ['./team-task-opinion-modal.component.scss'],
})
export class TeamTaskOpinionModalComponent {
  protected readonly alternatives: Alternative[];
  protected readonly alternativeOpinions: StableAlternativeOpnions;

  constructor(
    @Inject(MAT_DIALOG_DATA) protected data: TeamTaskOpinionModalData,
    protected dialogRef: MatDialogRef<TeamTaskOpinionModalComponent>,
    private teamsService: TeamsService,
  ) {
    this.alternatives = data.alternatives;
    this.alternativeOpinions = cloneDeep(data.alternativeOpinions);

    this.data.alternatives.forEach(alternative => {
      if (!this.alternativeOpinions[alternative.uuid]) {
        this.alternativeOpinions[alternative.uuid] = {};
      }
    });
  }

  close(save: boolean) {
    for (const [alternativeUUID, opinion] of Object.entries(this.alternativeOpinions)) {
      if (opinion?.sentiment === 'unset') {
        this.alternativeOpinions[alternativeUUID] = {};
      }
    }
    this.dialogRef.close(save ? this.alternativeOpinions : undefined);
  }

  toggleOpinion(alternativeUUUID: string, sentiment: boolean) {
    if (this.alternativeOpinions[alternativeUUUID]?.sentiment === sentiment) {
      this.alternativeOpinions[alternativeUUUID].sentiment = 'unset';
    } else {
      this.alternativeOpinions[alternativeUUUID] = {
        sentiment: sentiment,
        comment: this.alternativeOpinions[alternativeUUUID]?.comment ?? '',
      };
    }
  }
}
