import { Component, Inject, Input } from '@angular/core';
import { TeamDto, TeamMemberDto, TeamTaskDto, TeamTaskSingletonMap, doesTeamMemberHaveOwnerRights } from '@entscheidungsnavi/api-types';
import { TeamsService } from '@entscheidungsnavi/api-client';
import { MatDialogRef, MAT_DIALOG_DATA, MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { DecisionData } from '@entscheidungsnavi/decision-data';
import { HttpErrorResponse } from '@angular/common/http';
import { ConfirmModalComponent, ConfirmModalData } from '@entscheidungsnavi/widgets';
import { lastValueFrom } from 'rxjs';
import { TeamTaskState, TeamsTasksService } from '../team-tasks.service';
import { ProjectService } from '../../../../../../app/data/project';
import { TeamProject } from '../../../../../../app/data/project/team-project';

export type TeamTaskModalData = { team: TeamDto; task: TeamTaskDto; member: TeamMemberDto; aggregatedProject: DecisionData };

@Component({
  templateUrl: './team-task-modal.component.html',
  styleUrls: ['./team-task-modal.component.scss'],
})
export class TeamTaskModalComponent {
  team: TeamDto;

  @Input()
  task: TeamTaskDto;

  @Input()
  member: TeamMemberDto;

  aggregatedProject: DecisionData;

  taskState: [TeamTaskState, string, string];

  get hasEditRight() {
    const teamProject = this.projectService.getProject(TeamProject);

    if (!teamProject) {
      return false;
    }

    return teamProject.validEditToken?.holder === teamProject.userMember.id;
  }

  get canApplyTask() {
    const teamProject = this.projectService.getProject(TeamProject);

    if (!teamProject) {
      return false;
    }

    return !this.isTaskSingleton && doesTeamMemberHaveOwnerRights(teamProject.userMember);
  }

  get isTaskSingleton() {
    return TeamTaskSingletonMap[this.task.type];
  }

  get isEditingLocked() {
    const teamProject = this.projectService.getProject(TeamProject);

    if (!teamProject) {
      return true;
    }

    return (
      teamProject.userMember.role !== 'owner' &&
      ((this.task.type === 'objective-weights' && this.team.lockedWeightTasks) ||
        (this.task.type === 'opinion' && this.team.lockedOpinionTasks))
    );
  }

  constructor(
    protected teamsTasksService: TeamsTasksService,
    protected teamAPIService: TeamsService,
    @Inject(MAT_DIALOG_DATA) data: TeamTaskModalData,
    private dialogRef: MatDialogRef<TeamTaskModalComponent>,
    private snackBar: MatSnackBar,
    private projectService: ProjectService,
    private dialog: MatDialog,
  ) {
    this.team = data.team;
    this.task = data.task;
    this.member = data.member;

    this.aggregatedProject = data.aggregatedProject;
    this.taskState = this.teamsTasksService.getTaskState(this.task, data.aggregatedProject);
  }

  applyTask() {
    return this.teamsTasksService.applyTask(this.task, this.member.id).subscribe({
      next: () => {
        this.snackBar.open($localize`Aufgabe erfolgreich übernommen`, 'Ok', { duration: 5000 });
        this.task.applied = true;
        this.taskState = this.teamsTasksService.getTaskState(this.task, this.aggregatedProject);
      },
      error: error => {
        if (error instanceof HttpErrorResponse && error.status === 403) {
          this.snackBar.open(
            $localize`Du kannst die Aufgabe nicht übernehmen, da das Bearbeitungsrecht grade von jemand anderem gehalten wird.`,
            'Ok',
            { duration: 5000 },
          );
        } else {
          this.snackBar.open(
            $localize`Beim Übernehmen der Aufgabe ist ein unbekannter Fehler aufgetreten.
          Gehe sicher, dass Du aktuell das Bearbeitungsrecht besitzt und versuche es eventuell später erneut.`,
            'Ok',
            {
              duration: 5000,
            },
          );
        }
      },
    });
  }

  close() {
    this.dialogRef.close();
  }

  async submitTask() {
    if (this.task.submitted) {
      return;
    }

    if (
      !(await lastValueFrom(
        this.dialog
          .open<ConfirmModalComponent, ConfirmModalData, boolean>(ConfirmModalComponent, {
            data: {
              title: $localize`Aufgabe abgeben`,
              prompt: $localize`Möchtest Du die Aufgabe wirklich abgeben? Du kannst sie danach nicht weiter bearbeiten.`,
              buttonConfirm: $localize`Ja, abgeben`,
              buttonDeny: $localize`Nein, abbrechen`,
              buttonConfirmColor: 'accent',
            },
          })
          .afterClosed(),
      ))
    ) {
      return;
    }

    this.teamAPIService.updateTask(this.team.id, this.member.id, this.task.id, { submitted: true }).subscribe({
      next: () => {
        this.task.submitted = true;
        this.taskState = this.teamsTasksService.getTaskState(this.task, this.aggregatedProject);
      },
      error: () => this.snackBar.open($localize`Fehler beim Speichern der Aufgabe`),
    });
  }

  async editTask() {
    await this.teamsTasksService.editTask(this.task, this.member);
    this.taskState = this.teamsTasksService.getTaskState(this.task, this.aggregatedProject);
  }
}
