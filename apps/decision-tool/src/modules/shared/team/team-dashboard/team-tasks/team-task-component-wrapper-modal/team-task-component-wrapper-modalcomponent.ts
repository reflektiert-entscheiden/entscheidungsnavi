import { Component, ComponentRef, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ComponentPortal, ComponentType } from '@angular/cdk/portal';
import { TeamTaskDto } from '@entscheidungsnavi/api-types';
import { TeamsTasksService } from '../team-tasks.service';

export type TeamTaskModalWrapperData = {
  component: ComponentType<unknown>;
  readonly: boolean;
  task: TeamTaskDto;
  inputs?: Record<string, unknown>;
};

@Component({
  templateUrl: './team-task-component-wrapper-modal.component.html',
  styleUrls: ['./team-task-component-wrapper-modal.component.scss'],
})
export class TeamTaskComponentWrapperModalComponent {
  task: TeamTaskDto;

  portal: ComponentPortal<unknown>;

  readonly inputs: Record<string, unknown>;

  protected readonly readonly: boolean;

  constructor(
    protected teamsTasksService: TeamsTasksService,
    @Inject(MAT_DIALOG_DATA) data: TeamTaskModalWrapperData,
    private dialogRef: MatDialogRef<TeamTaskComponentWrapperModalComponent>,
  ) {
    this.portal = new ComponentPortal(data.component);
    this.task = data.task;
    this.inputs = data.inputs;
    this.readonly = data.readonly;
  }

  close(shouldSave: boolean) {
    this.dialogRef.close(shouldSave);
  }

  portalAttached(ref: ComponentRef<unknown>) {
    if (this.inputs) {
      Object.assign(ref.instance, this.inputs);
    }
  }
}
