import { Injectable, Injector } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { DecisionData, ObjectiveType } from '@entscheidungsnavi/decision-data';
import { readText } from '@entscheidungsnavi/decision-data/export';
import { omit } from 'lodash';
import { catchError, concatMap, lastValueFrom, tap, throwError } from 'rxjs';
import { TeamsService } from '@entscheidungsnavi/api-client';
import {
  AbstractTeamTaskDto,
  AlternativeOpinions,
  TeamMemberDto,
  TeamTaskDto,
  TeamTaskObjectiveBrainstorming,
  TeamTaskObjectiveScaleDto,
  TeamTaskObjectiveWeightsDto,
  TeamTaskOpinionDto,
  TeamTaskType,
} from '@entscheidungsnavi/api-types';
import { MatSnackBar } from '@angular/material/snack-bar';
import { assertUnreachable } from '@entscheidungsnavi/tools';
import { DecisionDataExportService } from '../../../../../app/data/decision-data-export.service';
import { ProjectService } from '../../../../../app/data/project';
import { TeamProject } from '../../../../../app/data/project/team-project';
import { DecisionDataState, StaticDecisionDataState } from '../../../decision-data-state';
import {
  TeamTaskComponentWrapperModalComponent,
  TeamTaskModalWrapperData,
} from './team-task-component-wrapper-modal/team-task-component-wrapper-modalcomponent';
import {
  TeamTaskOpinionModalComponent,
  TeamTaskOpinionModalData,
  TeamTaskOpinionModalReturn,
} from './team-task-opinion-modal/team-task-opinion-modal.component';

export const TeamTaskStates = ['pending', 'in-progress', 'done', 'applied', 'submitted'] as const;
export type TeamTaskState = (typeof TeamTaskStates)[number];

const TASK_TITLES: Record<TeamTaskState, string> = {
  pending: $localize`Ausstehend`,
  'in-progress': $localize`In Bearbeitung`,
  done: $localize`Vollständig`,
  applied: $localize`Übernommen`,
  submitted: $localize`Abgegeben`,
};

@Injectable({ providedIn: 'root' })
export class TeamsTasksService {
  get team() {
    return this.projectService.getProject(TeamProject).team;
  }

  constructor(
    private dialog: MatDialog,
    private injector: Injector,
    private projectService: ProjectService,
    private teamService: TeamsService,
    private snackBar: MatSnackBar,
    private exportService: DecisionDataExportService,
    private decisionDataExportService: DecisionDataExportService,
    private decisionDataState: DecisionDataState,
  ) {}

  getTaskTypeName(task: TeamTaskDto | TeamTaskType) {
    const taskType = task instanceof AbstractTeamTaskDto ? task.type : task;
    if (taskType === 'objective-brainstorming') {
      return $localize`Ziel-Brainstorming`;
    } else if (taskType === 'objective-scale') {
      return $localize`Zielskala`;
    } else if (taskType === 'objective-weights') {
      return $localize`Zielgewichte`;
    } else if (taskType === 'opinion') {
      return $localize`Meinungen`;
    } else {
      assertUnreachable(taskType);
    }
  }

  async editTask(task: TeamTaskDto, member: TeamMemberDto) {
    const mainProjectData = (await lastValueFrom(this.teamService.getTeam(this.team.id)).catch(() => null))?.mainProjectData;
    if (!mainProjectData) {
      this.snackBar.open($localize`Beim Laden der Aufgabe ist ein Fehler aufgetreten.`);
      return;
    }

    const mainProject = readText(mainProjectData);
    const taskIsReadonly = task.submitted;

    const baseInjector = Injector.create({
      providers: [
        {
          provide: DecisionDataState,
          useValue: new StaticDecisionDataState(taskIsReadonly, false),
        },
      ],
      parent: this.injector,
    });

    if (task.type === 'objective-scale') {
      const decisionData = readText(task.data);

      const component = (await import('../../../../impact-model/objective-scale/objective-scale-modal.component'))
        .ObjectiveScaleModalComponent;

      const shouldSave = await lastValueFrom(
        this.dialog
          .open(component, {
            data: { objectiveIdx: 0 },
            injector: Injector.create({
              providers: [
                {
                  provide: DecisionData,
                  useValue: decisionData,
                },
              ],
              parent: baseInjector,
            }),
          })
          .afterClosed(),
      );

      if (shouldSave) {
        this.saveDataTask(decisionData, member, task);
      }
    } else if (task.type === 'objective-brainstorming') {
      const decisionData = readText(task.data);

      const component = (await import('../../../../objectives/aspect-list/objective-aspect-list.component')).ObjectiveAspectListComponent;

      const shouldSave = await lastValueFrom(
        this.dialog
          .open<TeamTaskComponentWrapperModalComponent, TeamTaskModalWrapperData>(TeamTaskComponentWrapperModalComponent, {
            data: {
              component,
              task,
              readonly: taskIsReadonly,
            },
            injector: Injector.create({
              providers: [
                {
                  provide: DecisionData,
                  useValue: decisionData,
                },
              ],
              parent: baseInjector,
            }),
          })
          .afterClosed(),
      );

      if (shouldSave) {
        this.saveDataTask(decisionData, member, task);
      }
    } else if (task.type === 'objective-weights') {
      if (mainProject.objectives.length === 0) {
        this.snackBar.open($localize`Es sind keine Ziele im Projekt definiert.`, 'Ok', { duration: 5000 });
        return;
      }

      const decisionData = new DecisionData();
      decisionData.projectMode = 'starter';

      for (const objective of mainProject.objectives) {
        decisionData.addObjective(objective);
      }

      const existingObjectiveWeights = task.weights;

      for (const [objectiveUUID, weight] of Object.entries(existingObjectiveWeights)) {
        const objectiveIndex = decisionData.objectives.findIndex(o => o.uuid === objectiveUUID);

        if (objectiveIndex >= 0) {
          if (weight) {
            decisionData.weights.changePreliminaryWeightValue(objectiveIndex, weight);
          }
        }
      }

      const component = (await import('../../../../results/objective-weighting/overview/objective-weighting-overview.component'))
        .ObjectiveWeightingOverviewComponent;

      const shouldSave = await lastValueFrom(
        this.dialog
          .open(TeamTaskComponentWrapperModalComponent, {
            data: {
              component,
              task,
              inputs: {
                showNoteButtons: false,
              },
            },
            injector: Injector.create({
              providers: [
                {
                  provide: DecisionData,
                  useValue: decisionData,
                },
              ],
              parent: baseInjector,
            }),
          })
          .afterClosed(),
      );

      if (shouldSave) {
        this.saveObjectiveWeightsTask(decisionData, member, task);
      }
    } else if (task.type === 'opinion') {
      const newOpinions = await lastValueFrom(
        this.dialog
          .open<TeamTaskOpinionModalComponent, TeamTaskOpinionModalData, TeamTaskOpinionModalReturn>(TeamTaskOpinionModalComponent, {
            data: {
              alternatives: mainProject.alternatives,
              alternativeOpinions: task.opinions,
              teamId: this.team.id,
              teamMemberId: member.id,
            },
          })
          .afterClosed(),
      );

      if (newOpinions) {
        this.saveOpinionTask(member, task, newOpinions);
      }
    } else {
      assertUnreachable(task);
    }
  }

  getTaskState(task: TeamTaskDto, relativeToProject: DecisionData): [state: TeamTaskState, title: string, subTitle: string] {
    if (task.applied) {
      return ['applied', TASK_TITLES.applied, null];
    }

    if (task.submitted) {
      return ['submitted', TASK_TITLES.submitted, null];
    }

    if ('data' in task && !task.data) {
      return ['pending', TASK_TITLES.pending, null];
    }

    if (task.type === 'objective-weights') {
      const weights = task.weights;

      const totalObjectiveCount = relativeToProject.objectives.length;
      const objectiveWithWeightsCount = relativeToProject.objectives.filter(objective => weights[objective.uuid] != null).length;

      const state = totalObjectiveCount === objectiveWithWeightsCount ? 'done' : 'in-progress';

      return [state, TASK_TITLES[state], $localize`${objectiveWithWeightsCount}/${totalObjectiveCount} Ziele gewichtet`];
    } else if (task.type === 'opinion') {
      const opinions = task.opinions;

      const alternativesSeen = relativeToProject.alternatives.filter(alternative => opinions[alternative.uuid] != null).length;

      const state = alternativesSeen === relativeToProject.alternatives.length ? 'done' : 'in-progress';

      return [state, TASK_TITLES[state], $localize`${alternativesSeen}/${relativeToProject.alternatives.length} Alternativen bewertet`];
    } else if (task.submitted) {
      return ['done', TASK_TITLES.done, null];
    } else {
      return ['in-progress', TASK_TITLES['in-progress'], null];
    }
  }

  applyTask(task: TeamTaskDto, memberId: string) {
    if (!('data' in task)) {
      return throwError(() => new Error($localize`In der Aufgabe sind keine Daten hinterlegt.`));
    }

    const teamProject = this.projectService.getProject(TeamProject);

    if (!teamProject) {
      return throwError(() => new Error($localize`Es ist kein Team-Projekt geladen.`));
    }

    const team = teamProject.team;
    const taskProject = readText(task.data);

    return this.projectService.forceReloadProject().pipe(
      concatMap(_ => {
        const decisionData = this.injector.get(DecisionData);
        if (task.type === 'objective-brainstorming') {
          for (const brainstormedObjective of taskProject.objectiveAspects.listOfAspects) {
            decisionData.objectiveAspects.listOfAspects.push(brainstormedObjective);
          }
        } else if (task.type === 'objective-scale') {
          const objectiveId = task.objectiveUUID;

          const taskObjective = taskProject.objectives.find(o => o.uuid === objectiveId);
          const targetObjective = decisionData.objectives.find(o => o.uuid === objectiveId);

          if (!targetObjective) {
            throw new Error($localize`Das Ziel für das die Skala definiert wurde existiert nicht mehr.`);
          }

          decisionData.changeObjectiveType(decisionData.objectives.indexOf(targetObjective), taskObjective.objectiveType);

          if (taskObjective.objectiveType === ObjectiveType.Numerical) {
            targetObjective.numericalData = taskObjective.numericalData;
          } else if (taskObjective.objectiveType === ObjectiveType.Verbal) {
            targetObjective.verbalData = taskObjective.verbalData;
          } else if (taskObjective.objectiveType === ObjectiveType.Indicator) {
            targetObjective.indicatorData = taskObjective.indicatorData;
            Object.assign(targetObjective.indicatorData, omit(taskObjective.indicatorData, 'utilityfunction'));
          } else {
            assertUnreachable(taskObjective.objectiveType);
          }
        }

        return this.teamService.updateTeam(team.id, { mainProjectData: this.exportService.dataToText(null) });
      }),
      concatMap(_ => this.teamService.updateTask(team.id, memberId, task.id, { applied: true })),
      catchError(error => this.projectService.forceReloadProject().pipe(concatMap(_ => throwError(() => error)))),
    );
  }

  private saveOpinionTask(member: TeamMemberDto, task: TeamTaskOpinionDto, newOpinions: AlternativeOpinions) {
    task.opinions = newOpinions;

    this.teamService
      .updateTask(this.team.id, member.id, task.id, { opinions: newOpinions })
      .subscribe({ error: () => this.snackBar.open($localize`Fehler beim Speichern der Meinungen`, 'Ok', { duration: 5000 }) });
  }

  private saveObjectiveWeightsTask(decisionData: DecisionData, member: TeamMemberDto, task: TeamTaskObjectiveWeightsDto) {
    const weights: Record<string, number> = {};

    for (let objectiveIndex = 0; objectiveIndex < decisionData.objectives.length; objectiveIndex++) {
      const uuid = decisionData.objectives[objectiveIndex].uuid;
      const weight = decisionData.weights.getWeight(objectiveIndex);

      if (weight) {
        weights[uuid] = weight.value;
      }
    }

    task.weights = weights;

    this.teamService
      .updateTask(this.team.id, member.id, task.id, { weights })
      .subscribe({ error: () => this.snackBar.open($localize`Fehler beim Speichern der Gewichte`, 'Ok', { duration: 5000 }) });
  }

  private saveDataTask(
    decisionData: DecisionData,
    member: TeamMemberDto,
    task: TeamTaskObjectiveBrainstorming | TeamTaskObjectiveScaleDto,
  ) {
    const dataAsString = this.decisionDataExportService.dataToText(null, decisionData);
    task.data = dataAsString;

    this.teamService
      .updateTask(this.team.id, member.id, task.id, { data: dataAsString })
      .pipe(
        tap({
          next: () => {
            task.data = dataAsString;
          },
        }),
      )
      .subscribe({
        error: () => this.snackBar.open($localize`Fehler beim Speichern der Aufgabe`, 'Ok', { duration: 5000 }),
      });
  }
}
