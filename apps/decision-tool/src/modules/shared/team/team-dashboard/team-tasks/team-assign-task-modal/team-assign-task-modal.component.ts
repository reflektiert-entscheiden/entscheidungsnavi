import { Component, Inject, ViewChild } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { TeamTaskSingletonMap, TeamTaskTypes, TeamTaskType, TeamMemberDto, TeamTaskDto } from '@entscheidungsnavi/api-types';
import { MatSelectionList } from '@angular/material/list';
import { TeamsService } from '@entscheidungsnavi/api-client';
import { catchError, of, zip } from 'rxjs';
import { MatSnackBar } from '@angular/material/snack-bar';
import { TeamCalc } from '@entscheidungsnavi/decision-data';
import { TeamsTasksService } from '../team-tasks.service';

export type TeamAssignTaskModalData = { teamId: string; members: readonly TeamMemberDto[]; teamCalc: TeamCalc; taskType?: TeamTaskType };
export type TeamAssignTaskModalReturnType = { task: TeamTaskDto; assignedTo: TeamMemberDto }[];

@Component({
  templateUrl: './team-assign-task-modal.component.html',
  styleUrls: ['./team-assign-task-modal.component.scss'],
})
export class TeamAssignTaskModalComponent {
  taskTypeForm = new FormGroup({
    taskType: new FormControl<TeamTaskType>(null, [Validators.required]),
  });

  objectiveScaleOption = new FormGroup({
    objectiveUUID: new FormControl<string>(null, [Validators.required]),
  });

  @ViewChild('selectedMembers')
  selectedMembersList: MatSelectionList;

  taskTypes: readonly { name: string; type: TeamTaskType }[];

  taskTypeLocked = false;

  protected readonly teamId: string;
  protected readonly members: readonly TeamMemberDto[];
  protected readonly teamCalc: TeamCalc;

  get singletonTaskSelected() {
    return !!TeamTaskSingletonMap[this.selectedTaskType];
  }

  get formValid() {
    return (
      this.taskTypeForm.valid &&
      (this.taskOptions == null || this.taskOptions.valid) &&
      this.selectedMembersList != null &&
      this.selectedMembersList.selectedOptions.selected.length > 0
    );
  }

  get selectedTaskType() {
    return this.taskTypeForm.value.taskType;
  }

  get taskOptions() {
    if (this.selectedTaskType === 'objective-scale') {
      return this.objectiveScaleOption;
    }

    return null;
  }

  get selectedTaskName(): string {
    return this.taskTypes.find(tt => tt.type == this.selectedTaskType)?.name;
  }

  constructor(
    private dialogRef: MatDialogRef<TeamAssignTaskModalComponent>,
    private teamsService: TeamsService,
    private snackBar: MatSnackBar,
    private teamTasksService: TeamsTasksService,
    @Inject(MAT_DIALOG_DATA) data: TeamAssignTaskModalData,
  ) {
    this.taskTypes = TeamTaskTypes.map(type => ({ type, name: this.teamTasksService.getTaskTypeName(type) }));

    this.teamId = data.teamId;
    this.members = data.members;
    this.teamCalc = data.teamCalc;

    if (data.taskType) {
      this.taskTypeForm.controls.taskType.setValue(data.taskType);
      this.taskTypeLocked = true;
    }
  }

  closeWithAssignedTask() {
    if (!this.formValid) {
      return;
    }

    const taskType = this.selectedTaskType;

    const assignees: TeamMemberDto[] = this.selectedMembersList?.selectedOptions.selected.map(s => s.value);

    const taskData: Record<string, unknown> = {};

    if (taskType === 'objective-scale') {
      taskData['objectiveUUID'] = this.objectiveScaleOption.value.objectiveUUID;
    }

    zip(
      ...assignees.map(member => this.teamsService.assignTask(this.teamId, member.id, taskType, taskData).pipe(catchError(_ => of(null)))),
    ).subscribe(results => {
      const resultsWithMembers = results.map((task, index) => ({ task, assignedTo: assignees[index] }));
      const createdTasksWithMembers = resultsWithMembers.filter(({ task }) => task != null);

      if (createdTasksWithMembers.length === 0) {
        this.snackBar.open($localize`Beim Zuweisen der Aufgaben ist ein Fehler aufgetreten.`, 'Ok', { duration: 5000 });
      } else if (createdTasksWithMembers.length !== assignees.length) {
        this.snackBar.open($localize`Beim Zuweisen einiger Aufgaben ist ein Fehler aufgetreten.`, 'Ok', { duration: 5000 });
      }

      if (createdTasksWithMembers.length > 0) {
        this.close(createdTasksWithMembers);
      }
    });
  }

  close(result?: TeamAssignTaskModalReturnType) {
    this.dialogRef.close(result);
  }

  shouldShowMember(member: TeamMemberDto) {
    if (!this.singletonTaskSelected) {
      return true;
    } else {
      return !member.tasks.some(task => task.type === this.selectedTaskType);
    }
  }
}
