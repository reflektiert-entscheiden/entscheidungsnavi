export function turnPointsIntoLine(xCount: number, yValues: number[], yBound: number) {
  if (xCount === 0) {
    return '';
  }

  const points: [number, number][] = [];

  let x = 0;
  const part = 100 / (xCount - 1);

  for (let i = 0; i < xCount; i++) {
    points.push([x, 100 - (100 / yBound) * yValues[i] * 100]);
    x += part;
  }

  return `M ${points[0][0]} ${points[0][1]} ${points
    .slice(1)
    .map(p => `L ${p[0]} ${p[1]}`)
    .join(' ')}`;
}
