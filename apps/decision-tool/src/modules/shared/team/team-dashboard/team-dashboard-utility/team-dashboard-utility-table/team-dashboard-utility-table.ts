import { Component, Input, OnInit } from '@angular/core';
import { TeamDto } from '@entscheidungsnavi/api-types';
import { TeamCalc } from '@entscheidungsnavi/decision-data';

@Component({
  selector: 'dt-team-dashboard-utility-table',
  templateUrl: './team-dashboard-utility-table.html',
  styleUrls: ['./team-dashboard-utility-table.scss'],
})
export class TeamDashboardUtilityTableComponent implements OnInit {
  @Input()
  team: TeamDto;

  @Input()
  teamCalc: TeamCalc;

  protected aggregatedUtilityValues: number[];
  protected memberUtilityValues: number[][];
  protected memberAlternativeRanking: number[][];
  protected memberBestAlternativeUtility: number[];
  protected alternativeRanking: number[];

  get gridColumns() {
    return `minmax(20ch, 700px) auto repeat(${this.team.members.length}, auto)`;
  }

  ngOnInit(): void {
    this.calcData();
  }

  calcData() {
    this.aggregatedUtilityValues = this.teamCalc.aggregatedProject.getAlternativeUtilities();

    const alternativesSortedByUtility = this.aggregatedUtilityValues
      .map((utilityValue, alternativeIndex) => ({ utilityValue, alternativeIndex }))
      .sort((a, b) => b.utilityValue - a.utilityValue);

    this.alternativeRanking = this.teamCalc.aggregatedProject.alternatives.map((_, alternativeIndex) => {
      return alternativesSortedByUtility.findIndex(u => u.alternativeIndex === alternativeIndex);
    });

    this.memberUtilityValues = this.teamCalc.memberProjects.map(project => {
      if (project == null) {
        return this.aggregatedUtilityValues.map(_ => -1);
      }

      return project.getAlternativeUtilities();
    });

    this.memberAlternativeRanking = this.teamCalc.memberProjects.map(project => {
      if (project == null) {
        return this.teamCalc.aggregatedProject.alternatives.map(_ => -1);
      }
      const utilityValues = project.getAlternativeUtilities();
      const utilityValuesWithIndices = utilityValues.map((utilityValue, alternativeIndex) => ({ utilityValue, alternativeIndex }));
      utilityValuesWithIndices.sort((a, b) => b.utilityValue - a.utilityValue);

      return this.teamCalc.aggregatedProject.alternatives.map((_, alternativeIndex) => {
        return utilityValuesWithIndices.findIndex(u => u.alternativeIndex === alternativeIndex) + 1;
      });
    });

    this.memberBestAlternativeUtility = this.memberUtilityValues.map(values => Math.max(...values));
  }
}
