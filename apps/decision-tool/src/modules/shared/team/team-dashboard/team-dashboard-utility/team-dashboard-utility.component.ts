import { Component, Input, OnInit } from '@angular/core';
import { TeamsService } from '@entscheidungsnavi/api-client';
import { MatSnackBar } from '@angular/material/snack-bar';
import { TeamCalc } from '@entscheidungsnavi/decision-data';
import { TeamDto } from '@entscheidungsnavi/api-types';

@Component({
  selector: 'dt-team-dashboard-utility',
  templateUrl: './team-dashboard-utility.component.html',
  styleUrls: ['./team-dashboard-utility.component.scss'],
})
export class TeamDashboardUtilityComponent implements OnInit {
  @Input()
  teamId: string;

  protected team: TeamDto;
  protected teamCalc: TeamCalc;

  protected valid = false;
  protected errored = false;

  constructor(
    private teamsService: TeamsService,
    private snackBar: MatSnackBar,
  ) {}

  ngOnInit() {
    this.teamsService.getTeam(this.teamId).subscribe({
      next: team => {
        this.team = team;
        this.teamCalc = new TeamCalc(team);

        this.valid = this.teamCalc.aggregatedProject.getFirstStepWithErrors() == null;
      },
      error: () => {
        this.errored = true;
        this.snackBar.open($localize`Fehler beim Laden der Teamdaten.`, 'Ok', { duration: 5000 });
      },
    });
  }
}
