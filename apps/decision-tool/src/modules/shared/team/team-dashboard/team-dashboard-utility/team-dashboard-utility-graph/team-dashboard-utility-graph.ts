import { Component, Input, OnInit } from '@angular/core';
import { TeamDto } from '@entscheidungsnavi/api-types';
import { TeamCalc } from '@entscheidungsnavi/decision-data';
import { turnPointsIntoLine } from '../../calc';

type GraphData = {
  members: {
    vertices: [number, number][];
    line: string;
    valid: boolean;
  }[];
  averages: {
    weighted: {
      line: string;
      vertices: [number, number][];
    };
    unweighted: {
      line: string;
      vertices: [number, number][];
    };
  };
};

@Component({
  selector: 'dt-team-dashboard-utility-graph',
  templateUrl: './team-dashboard-utility-graph.html',
  styleUrls: ['./team-dashboard-utility-graph.scss'],
})
export class TeamDashboardUtilityGraphComponent implements OnInit {
  @Input()
  team: TeamDto;

  @Input()
  teamCalc: TeamCalc;

  yScale: number[];

  hoveringLine: number | 'weighted' | 'unweighted' = -1;

  graphMode: 'lines' | 'bars' = 'lines';

  showAverage: 'none' | 'weighted' | 'unweighted' | 'both' = 'weighted';

  shownMembers: number[] = [];

  gridBackground: string;

  alternativeNames: string[];

  protected maxRounded = 1;
  protected graphData: GraphData;

  get graphPadding() {
    return this.graphMode === 'lines' ? 20 : 100;
  }

  ngOnInit() {
    this.calcData();
  }

  isNumber(value: unknown): value is number {
    return typeof value === 'number';
  }

  calcData() {
    this.graphData = {
      members: [],
      averages: {
        weighted: {
          line: '',
          vertices: [],
        },
        unweighted: {
          line: '',
          vertices: [],
        },
      },
    };

    const aggregateUtilityValues = this.teamCalc.aggregatedProject.getAlternativeUtilities();

    const alternativesSortedByUtility = aggregateUtilityValues
      .map((utilityValue, alternativeIndex) => ({ utilityValue, alternativeIndex }))
      .sort((a, b) => b.utilityValue - a.utilityValue);

    this.alternativeNames = alternativesSortedByUtility.map(a => this.teamCalc.aggregatedProject.alternatives[a.alternativeIndex].name);

    const memberUtilityValues = this.teamCalc.memberProjects.map(project => {
      if (project == null) {
        return this.teamCalc.aggregatedProject.alternatives.map(_ => -1);
      }

      return project.getAlternativeUtilities();
    });

    const max = memberUtilityValues.reduce((acc, values) => Math.max(acc, ...values), 0) * 100;

    // Round max to nearest 10
    this.maxRounded = Math.ceil(max / 10) * 10;

    // yScale is every 10 up to maxRounded
    this.yScale = Array.from({ length: this.maxRounded / 10 + 1 }, (_, i) => i * 10).reverse();

    this.gridBackground = `${100 / (aggregateUtilityValues.length - 1)}% ${100 / (this.yScale.length - 1)}%`;

    const part = 100 / (this.alternativeNames.length - 1);

    for (const project of this.teamCalc.memberProjects) {
      if (project == null) {
        this.graphData.members.push({
          vertices: [],
          line: '',
          valid: false,
        });
        continue;
      }

      const vertices: [number, number][] = [];

      const utilityValues = project.getAlternativeUtilities();

      let x = 0;

      for (const alternative of alternativesSortedByUtility) {
        vertices.push([x, 100 - (100 / this.maxRounded) * utilityValues[alternative.alternativeIndex] * 100]);
        x += part;
      }

      this.graphData.members.push({
        line: `M ${vertices[0][0]} ${vertices[0][1]} ${vertices
          .slice(1)
          .map(p => `L ${p[0]} ${p[1]}`)
          .join(' ')}`,
        vertices,
        valid: true,
      });
    }

    this.graphData.averages = {
      weighted: {
        line: turnPointsIntoLine(
          aggregateUtilityValues.length,
          aggregateUtilityValues.sort((a, b) => b - a),
          this.maxRounded,
        ),
        vertices: aggregateUtilityValues
          .sort((a, b) => b - a)
          .map((v, i) => [i * (100 / (aggregateUtilityValues.length - 1)), 100 - (100 / this.maxRounded) * v * 100]),
      },
      unweighted: {
        line: turnPointsIntoLine(
          aggregateUtilityValues.length,
          this.teamCalc.equalAggregatedProject.getAlternativeUtilities().sort((a, b) => b - a),
          this.maxRounded,
        ),
        vertices: this.teamCalc.equalAggregatedProject
          .getAlternativeUtilities()
          .sort((a, b) => b - a)
          .map((v, i) => [i * (100 / (aggregateUtilityValues.length - 1)), 100 - (100 / this.maxRounded) * v * 100]),
      },
    };

    this.shownMembers = this.team.members.map((_, i) => i).filter(i => this.graphData.members[i].valid);
  }

  graphPositionToOriginalValue(adjustedValue: number) {
    return (100 - adjustedValue) / (100 / this.maxRounded);
  }
}
