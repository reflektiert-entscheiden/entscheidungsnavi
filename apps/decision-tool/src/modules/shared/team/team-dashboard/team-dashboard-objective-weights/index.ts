export * from './team-dashboard-objective-weights-graph';
export * from './team-dashboard-objective-weights-table';
export { TeamDashboardObjectiveWeightsComponent } from './team-dashboard-objective-weights.component';
