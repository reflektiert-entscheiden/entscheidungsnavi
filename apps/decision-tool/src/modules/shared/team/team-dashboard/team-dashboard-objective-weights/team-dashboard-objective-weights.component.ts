import { Component, Input, OnInit } from '@angular/core';
import { TeamsService } from '@entscheidungsnavi/api-client';
import { MatSnackBar } from '@angular/material/snack-bar';
import { TeamCalc } from '@entscheidungsnavi/decision-data';
import { TeamDto } from '@entscheidungsnavi/api-types';

@Component({
  selector: 'dt-team-dashboard-objective-weights',
  templateUrl: './team-dashboard-objective-weights.component.html',
  styleUrls: ['./team-dashboard-objective-weights.component.scss'],
})
export class TeamDashboardObjectiveWeightsComponent implements OnInit {
  @Input()
  teamId: string;

  protected team: TeamDto;
  protected teamCalc: TeamCalc;

  protected errored = false;

  constructor(
    private teamsService: TeamsService,
    private snackBar: MatSnackBar,
  ) {}

  ngOnInit() {
    this.teamsService.getTeam(this.teamId).subscribe({
      next: team => {
        this.team = team;
        this.teamCalc = new TeamCalc(team);
      },
      error: () => {
        this.errored = true;
        this.snackBar.open($localize`Fehler beim Laden der Teamdaten.`, 'Ok', { duration: 5000 });
      },
    });
  }

  protected recalc() {
    this.teamCalc = new TeamCalc(this.team);
  }
}
