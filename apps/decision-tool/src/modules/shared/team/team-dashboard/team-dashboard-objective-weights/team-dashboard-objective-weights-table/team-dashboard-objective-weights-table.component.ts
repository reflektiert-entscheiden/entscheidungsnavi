import { ChangeDetectionStrategy, Component, EventEmitter, Input, OnChanges, Output } from '@angular/core';
import { TeamsService } from '@entscheidungsnavi/api-client';
import { debounce } from 'lodash';
import { normalizeContinuous } from '@entscheidungsnavi/tools';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatDialog } from '@angular/material/dialog';
import { TeamCalc } from '@entscheidungsnavi/decision-data';
import { TeamDto, TeamMemberDto } from '@entscheidungsnavi/api-types';
import { TeamAssignTaskModalComponent } from '../../team-tasks';
import {
  TeamAssignTaskModalData,
  TeamAssignTaskModalReturnType,
} from '../../team-tasks/team-assign-task-modal/team-assign-task-modal.component';

@Component({
  selector: 'dt-team-dashboard-objective-weights-table',
  templateUrl: './team-dashboard-objective-weights-table.component.html',
  styleUrls: ['./team-dashboard-objective-weights-table.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TeamDashboardObjectiveWeightsTableComponent implements OnChanges {
  @Input()
  team: TeamDto;

  @Input()
  teamCalc: TeamCalc;

  @Output()
  valuesChanged = new EventEmitter<void>();

  protected everyoneHasObjectiveWeightTask: boolean;
  protected normalizedAggregateWeightValues: number[];
  protected normalizedMemberWeightValues: number[][];

  get weightsGridColumns() {
    return `1fr ${this.team.importanceShares.map(_ => 'auto').join(' ')} auto ${this.teamCalc.aggregatedProject.objectives
      .map(_ => '0.8fr')
      .join(' ')}`;
  }

  get weightsGridWeightsHeaderColumns() {
    return `${3 + this.team.importanceShares.length} / -1`;
  }

  constructor(
    private teamsService: TeamsService,
    private snackBar: MatSnackBar,
    private dialog: MatDialog,
  ) {
    this.syncImportanceToServer = debounce(this.syncImportanceToServer, 1000);
  }

  ngOnChanges() {
    this.normalizedAggregateWeightValues =
      this.teamCalc.aggregatedProject?.weights.getWeightValues() ?? this.teamCalc.aggregatedProject?.objectives.map(_ => -1);
    normalizeContinuous(this.normalizedAggregateWeightValues);

    this.normalizedMemberWeightValues = this.teamCalc.memberProjects.map(memberProject => {
      if (memberProject == null) {
        return this.teamCalc.aggregatedProject.objectives.map(_ => -1);
      }

      const memberWeights = memberProject.weights.getWeightValues();
      normalizeContinuous(memberWeights);
      return memberWeights;
    });

    this.everyoneHasObjectiveWeightTask = this.team.members.every(member => this.hasObjectiveWeightTask(member));
  }

  trackByIndex<T>(index: number, _item: T) {
    return index;
  }

  hasObjectiveWeightTask(member: TeamMemberDto) {
    return member.tasks.some(task => task.type === 'objective-weights');
  }

  addImportanceShare(index: number) {
    const value = Math.min(25, this.team.importanceShares[0]);

    this.team.importanceShares.splice(index + 1, 0, value);
    this.team.importanceShareNames.splice(index + 1, 0, 'Neue Spalte');

    this.team.members.forEach(member => member.importance.splice(index + 1, 0, 100));

    this.adjustFirstImportanceColumn();
    this.importanceChanged();
  }

  removeImportanceShare(index: number) {
    this.team.importanceShares.splice(index, 1);
    this.team.importanceShareNames.splice(index, 1);

    this.team.members.forEach(member => member.importance.splice(index, 1));

    this.adjustFirstImportanceColumn();
    this.importanceChanged();
  }

  setImportanceShare(columnIndex: number, value: number) {
    this.team.importanceShares[columnIndex] = value;

    this.adjustFirstImportanceColumn();

    this.importanceChanged();
  }

  setMemberImportanceValue(memberIndex: number, importanceColumnIndex: number, value: number) {
    this.team.members[memberIndex].importance[importanceColumnIndex] = value;
    this.importanceChanged();
  }

  importanceChanged() {
    this.syncImportanceToServer();
    this.valuesChanged.emit();
  }

  syncImportanceToServer() {
    this.teamsService
      .updateImportance(this.team.id, {
        importanceShares: this.team.importanceShares,
        memberImportanceValues: this.team.members.map(member => member.importance),
        importanceShareNames: this.team.importanceShareNames,
      })
      .subscribe({
        error: () =>
          this.snackBar.open($localize`Beim Speichern der Gewichtungen ist ein Fehler aufgetreten.`, $localize`Ok`, { duration: 5000 }),
      });
  }

  getImportanceColumnName(columnIndex: number) {
    if (columnIndex === 0) {
      return $localize`Bedeutung`;
    }

    return this.team.importanceShareNames[columnIndex];
  }

  private adjustFirstImportanceColumn() {
    const lambdaSum = this.team.importanceShares.slice(1).reduce((sum, column) => sum + column, 0);
    this.team.importanceShares[0] = 100 - lambdaSum;
  }

  public assignObjectiveWeightTask() {
    this.dialog
      .open<TeamAssignTaskModalComponent, TeamAssignTaskModalData, TeamAssignTaskModalReturnType>(TeamAssignTaskModalComponent, {
        data: {
          teamId: this.team.id,
          teamCalc: this.teamCalc,
          members: this.team.members,
          taskType: 'objective-weights',
        },
      })
      .afterClosed()
      .subscribe(result => {
        if (result) {
          for (const { task, assignedTo } of result) {
            this.team.members.find(member => member.id === assignedTo.id)?.tasks.push(task);
          }

          this.valuesChanged.emit();
        }
      });
  }
}
