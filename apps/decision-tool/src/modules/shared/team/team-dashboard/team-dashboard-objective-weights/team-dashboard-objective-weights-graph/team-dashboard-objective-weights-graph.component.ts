import { ChangeDetectionStrategy, Component, Input, OnChanges, QueryList, ViewChildren } from '@angular/core';
import { TeamDto } from '@entscheidungsnavi/api-types';
import { TeamCalc } from '@entscheidungsnavi/decision-data';
import { normalizeContinuous } from '@entscheidungsnavi/tools';
import { HoverPopOverDirective } from '@entscheidungsnavi/widgets';
import { turnPointsIntoLine } from '../../calc';

type GraphData = {
  members: {
    vertices: [number, number][];
    line: string;
    valid: boolean;
  }[];
  averages: {
    weighted: {
      line: string;
      vertices: [number, number][];
    };
    unweighted: {
      line: string;
      vertices: [number, number][];
    };
  };
  consensusCutoffPoints: number[][][];
};

type GroupedMemberIndexes = {
  rounded: number;
  vertices: [number, number];
  memberIndexes: number[];
};

@Component({
  selector: 'dt-team-dashboard-objective-weights-graph',
  templateUrl: './team-dashboard-objective-weights-graph.component.html',
  styleUrls: ['./team-dashboard-objective-weights-graph.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TeamDashboardObjectiveWeightsGraphComponent implements OnChanges {
  @Input()
  team: TeamDto;

  @Input()
  teamCalc: TeamCalc;

  hoveringLine: number | 'weighted' | 'unweighted' = -1;

  evaluationMode: 'default' | 'consensus' = 'default';

  protected maxWeightRounded = 1;

  consensusCount = 2;

  yScale: number[];

  @ViewChildren(HoverPopOverDirective)
  memberPopover: QueryList<HoverPopOverDirective>;

  showAverage: 'none' | 'weighted' | 'unweighted' | 'both' = 'weighted';
  shownMembers: number[] = [];

  canShowSomething = true;

  averageWeightsByObjectives: number[] = [];
  roundedWeightsWithMemberIndex: { valid: boolean; vertices: [number, number][]; roundedWeights: number[]; index: number }[] = [];
  memberIndexesGroupedByFurthestValues: GroupedMemberIndexes[][] = [];

  protected graphData: GraphData;

  get objectiveNames() {
    return this.teamCalc.aggregatedProject.objectives.map(o => o.name);
  }

  get gridBackground() {
    return `${100 / (this.objectiveNames.length - 1)}% ${100 / (this.yScale.length - 1)}%`;
  }

  get membersWithValidProjects() {
    return this.graphData.members.filter(m => m.valid).length;
  }

  isNumber(value: unknown): value is number {
    return typeof value === 'number';
  }

  ngOnChanges() {
    this.calcData();
  }

  private calcData() {
    this.graphData = {
      members: [],
      averages: {
        weighted: {
          line: '',
          vertices: [],
        },
        unweighted: {
          line: '',
          vertices: [],
        },
      },
      consensusCutoffPoints: [],
    };

    const maxWeight =
      Math.max(
        ...[...this.teamCalc.memberProjects, this.teamCalc.aggregatedProject].map(project => {
          if (project == null) {
            return 0;
          }

          const weights = project.weights.getWeightValues().slice();
          normalizeContinuous(weights);

          return Math.max(...weights);
        }),
      ) * 100;

    // Round max to nearest 10
    this.maxWeightRounded = Math.ceil(maxWeight / 10) * 10;

    // yScale is every 10 up to maxRounded
    this.yScale = Array.from({ length: this.maxWeightRounded / 10 + 1 }, (_, i) => i * 10).reverse();

    for (const project of this.teamCalc.memberProjects) {
      if (project == null) {
        this.graphData.members.push({
          vertices: [],
          line: '',
          valid: false,
        });
        continue;
      }

      const weights = project.weights.getWeightValues().slice();
      normalizeContinuous(weights);

      const vertices: [number, number][] = [];

      let x = 0;
      const part = 100 / (project.objectives.length - 1);

      for (let i = 0; i < project.objectives.length; i++) {
        vertices.push([x, 100 - (100 / this.maxWeightRounded) * weights[i] * 100]);
        x += part;
      }

      this.graphData.members.push({
        vertices,
        line:
          vertices.length > 0
            ? `M ${vertices[0][0]} ${vertices[0][1]} ${vertices
                .slice(1)
                .map(p => `L ${p[0]} ${p[1]}`)
                .join(' ')}`
            : '',
        valid: true,
      });
    }

    const normalizedAggregatedWeights = this.teamCalc.aggregatedProject.weights.getWeightValues().slice();
    normalizeContinuous(normalizedAggregatedWeights);

    const normalizedUnweightedAggregatedWeights = this.teamCalc.equalAggregatedProject.weights.getWeightValues().slice();
    normalizeContinuous(normalizedUnweightedAggregatedWeights);

    this.graphData.averages = {
      unweighted: {
        line: turnPointsIntoLine(this.objectiveNames.length, normalizedUnweightedAggregatedWeights, this.maxWeightRounded),
        vertices: normalizedUnweightedAggregatedWeights.map((v, i) => [
          i * (100 / (this.objectiveNames.length - 1)),
          100 - (100 / this.maxWeightRounded) * v * 100,
        ]),
      },
      weighted: {
        line: turnPointsIntoLine(this.objectiveNames.length, normalizedAggregatedWeights, this.maxWeightRounded),
        vertices: normalizedAggregatedWeights.map((v, i) => [
          i * (100 / (this.objectiveNames.length - 1)),
          100 - (100 / this.maxWeightRounded) * v * 100,
        ]),
      },
    };

    this.shownMembers = this.team.members.map((_, i) => i).filter(memberIndex => this.graphData.members[memberIndex].valid);
    this.averageWeightsByObjectives = this.graphData.averages.weighted.vertices.map(v => v[1]);
    this.roundedWeightsWithMemberIndex = this.graphData.members.map((m, index) => {
      const roundedWeights: number[] = [];
      m.vertices.forEach(v => {
        roundedWeights.push(Math.round(this.graphPositionToOriginalValue(v[1])));
      });
      return {
        valid: m.valid,
        vertices: m.vertices,
        roundedWeights,
        index,
      };
    });

    this.groupMemberIndexesByWeights(this.objectiveNames.length);
  }

  public trigger(objectiveIndex: number) {
    const dotsPerObjective = Math.min(this.membersWithValidProjects, this.memberIndexesGroupedByFurthestValues[objectiveIndex].length);
    let dotsFromPreviousObjectives = 0;
    for (let i = 0; i < objectiveIndex; i++) {
      dotsFromPreviousObjectives += this.memberIndexesGroupedByFurthestValues[i].length;
    }
    this.memberPopover.forEach((p, i) => {
      if (i > dotsFromPreviousObjectives - 1 && i < dotsFromPreviousObjectives + dotsPerObjective) {
        p.open(0);
      }
    });
  }

  public graphPositionToOriginalValue(adjustedValue: number) {
    return (100 - adjustedValue) / (100 / this.maxWeightRounded);
  }

  public groupMemberIndexesByWeights(numberOfObjectives: number): void {
    this.memberIndexesGroupedByFurthestValues = []; // for every objective
    for (let objectiveIndex = 0; objectiveIndex < numberOfObjectives; objectiveIndex++) {
      // calculate the average weight for this objective
      const roundedAverageWeight = Math.round(this.graphPositionToOriginalValue(this.averageWeightsByObjectives[objectiveIndex]));

      // get valid members memberindexes and their rounded weights
      const roundedWithMemberIndex = this.roundedWeightsWithMemberIndex
        .filter(o => o.valid)
        .map(o => {
          return { rounded: o?.roundedWeights[objectiveIndex], index: o.index, vertices: o.vertices[objectiveIndex] };
        });

      //get all rounded weights without duplicates
      const roundedObjectiveValues = Array.from(new Set(roundedWithMemberIndex.map(o => o.rounded)));

      //calculate the furthest above and below average weights
      const aboveAverage = roundedObjectiveValues.filter((val: number) => val > roundedAverageWeight).sort((a: number, b: number) => b - a);
      const belowAverage = roundedObjectiveValues.filter((val: number) => val < roundedAverageWeight).sort((a: number, b: number) => a - b);

      const furthestAbove = aboveAverage.slice(0, this.consensusCount);
      const furthestBelow = belowAverage.slice(0, this.consensusCount);

      const roundedFurthestValues = [...furthestAbove, ...furthestBelow];
      this.memberIndexesGroupedByFurthestValues[objectiveIndex] = [];

      roundedFurthestValues.forEach(roundedValue => {
        // get all member indexes with the same rounded weight
        const membersWithSameRoundedWeights = roundedWithMemberIndex.filter(o => o.rounded === roundedValue);
        const verticesFromFirstMember = membersWithSameRoundedWeights[0].vertices;
        const memberIndexes = membersWithSameRoundedWeights.map(o => o.index);
        // add the member indexes to the group
        this.memberIndexesGroupedByFurthestValues[objectiveIndex].push({
          rounded: roundedValue,
          memberIndexes,
          vertices: verticesFromFirstMember,
        });
      });
    }
  }
}
