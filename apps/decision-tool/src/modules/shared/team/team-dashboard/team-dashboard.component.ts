import { Component, ViewChild } from '@angular/core';
import { Observable, Subject, finalize } from 'rxjs';
import { OnDestroyObservable } from '@entscheidungsnavi/tools';
import { OverlayProgressBarDirective } from '@entscheidungsnavi/widgets/overlay-progress-bar';
import { DecisionData } from '@entscheidungsnavi/decision-data';
import { OverlayRef } from '@angular/cdk/overlay';
import { trigger, animate, style, transition } from '@angular/animations';
import { MatSnackBar } from '@angular/material/snack-bar';
import { doesTeamMemberHaveOwnerRights } from '@entscheidungsnavi/api-types';
import { ProjectService } from '../../../../app/data/project';
import { TeamProject } from '../../../../app/data/project/team-project';

@Component({
  selector: 'dt-team-dashboard',
  templateUrl: './team-dashboard.component.html',
  styleUrls: ['./team-dashboard.component.scss'],
  animations: [
    // the pop in / out animation for dt-team-dashboard
    trigger('appear', [
      transition(':enter', [
        style({ opacity: '0', scale: '80%', transformOrigin: '80% 0%' }),
        animate('150ms ease-in', style({ opacity: 1, scale: '100%', transformOrigin: '80% 0%' })),
      ]),
    ]),
  ],
})
export class TeamDashboardComponent {
  @ViewChild('mainProjectLoadingIndicator')
  mainProjectLoadingIndicator: OverlayProgressBarDirective;

  @OnDestroyObservable()
  protected onDestroy$: Observable<void>;

  public overlayRef: OverlayRef;

  protected whiteboardChange = new Subject<string>();

  get team() {
    return this.teamProject.team;
  }

  get teamProject() {
    return this.projectService.getProject(TeamProject);
  }

  get isUserTeamOwner() {
    return this.teamProject.userMember.role === 'owner';
  }

  get doesUserHaveOwnerRights() {
    return doesTeamMemberHaveOwnerRights(this.teamProject.userMember);
  }

  constructor(
    private projectService: ProjectService,
    protected decisionData: DecisionData,
    private snackBar: MatSnackBar,
  ) {}

  reopen() {
    this.overlayRef.detach();
    this.overlayRef.dispose();

    this.projectService.loadTeamProject(this.team.id, { forceUnload: true }).subscribe({
      error: () =>
        this.snackBar.open($localize`Beim Öffnen des Dashboards ist ein unbekannter Fehler aufgetreten.`, 'Ok', { duration: 5000 }),
    });
  }

  close() {
    const canSave = this.teamProject.canSave;
    this.projectService
      .loadTeamProject(this.team.id, { forceUnload: true })
      .pipe(
        finalize(() => {
          this.overlayRef.detach();
          this.overlayRef.dispose();
        }),
      )
      .subscribe({
        next: () => {
          if (canSave && !this.teamProject.canSave) {
            this.snackBar.open($localize`Dir wurde das Bearbeitungsrecht entzogen.`, 'Ok', { duration: 5000 });
          }
        },
        error: () => this.snackBar.open($localize`Fehler beim Aktualisieren des Teamprojekts.`, 'Ok', { duration: 5000 }),
      });
  }
}
