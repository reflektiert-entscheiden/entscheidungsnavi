import { Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { isEmail } from 'class-validator';
import { MatDialog as MatDialog } from '@angular/material/dialog';
import { TeamsService } from '@entscheidungsnavi/api-client';
import { TeamDto, TeamMemberDto, TeamRole, TeamRoles } from '@entscheidungsnavi/api-types';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ConfirmModalComponent, PopOverService } from '@entscheidungsnavi/widgets';
import { filter, firstValueFrom, lastValueFrom } from 'rxjs';
import { MatTableDataSource } from '@angular/material/table';
import { MatSort } from '@angular/material/sort';
import { _isNumberValue } from '@angular/cdk/coercion';
import { MatSelect } from '@angular/material/select';
import { getTeamMemberDisplayName } from '../../team-member-name.component';
import {
  TeamMemberInviteModalComponent,
  TeamMemberInviteModalData,
  TeamMemberInviteModalResultData,
} from './team-member-invite-modal/team-member-invite-modal.component';

@Component({
  selector: 'dt-team-members',
  templateUrl: './team-members.component.html',
  styleUrls: ['./team-members.component.scss'],
})
export class TeamMembersComponent implements OnInit {
  @Input()
  teamId: string;

  @Input()
  userMemberId: string;

  @Output()
  lostModeratorRight = new EventEmitter<void>();

  protected team: TeamDto;

  dataSource: MatTableDataSource<TeamMemberDto>;
  currentlyEditingEmailOf: TeamMemberDto | null = null;

  private userMember: TeamMemberDto;

  @ViewChild(MatSort) set sort(sort: MatSort) {
    this.dataSource.sort = sort;
  }

  get realMemberCount() {
    return this.team.members.filter(member => member.user).length;
  }

  get availableTeamRoles() {
    return TeamRoles;
  }

  constructor(
    private dialog: MatDialog,
    private teamsService: TeamsService,
    private snackBar: MatSnackBar,
    private popOverService: PopOverService,
  ) {
    this.dataSource = new MatTableDataSource();
    this.dataSource.sortingDataAccessor = this.sortingDataAccessor;

    const originalFilterPredicate = this.dataSource.filterPredicate;
    this.dataSource.filterPredicate = (data, filter) => {
      const filterLower = filter.toLowerCase();

      return (
        originalFilterPredicate(data, filter) ||
        (data instanceof TeamMemberDto &&
          (getTeamMemberDisplayName(data).toLowerCase().includes(filterLower) || data.user?.email.toLowerCase().includes(filterLower)))
      );
    };

    const originalSort = this.dataSource.sortData;
    this.dataSource.sortData = (data, sort) => {
      const originalSorted = originalSort(data, sort);

      originalSorted.sort((a, b) => {
        const aIndex = a.isInvitee ? 1 : -1;
        const bIndex = b.isInvitee ? 1 : -1;

        return aIndex - bIndex;
      });

      return originalSorted;
    };
  }

  sortingDataAccessor = (data: TeamMemberDto, sortHeaderId: string): string | number => {
    let value = null;
    if (sortHeaderId.indexOf('.') !== -1) {
      const ids = sortHeaderId.split('.');
      value = (data as any)[ids[0]]?.[ids[1]];
    } else {
      value = (data as any)[sortHeaderId];
    }

    return _isNumberValue(value) ? Number(value) : value;
  };

  ngOnInit(): void {
    this.teamsService.getTeam(this.teamId).subscribe({
      next: team => {
        this.team = team;
        this.dataSource.data = this.team.members;
        this.userMember = this.team.members.find(m => m.id === this.userMemberId);
      },
      error: () => this.snackBar.open($localize`Fehler beim Laden der Mitgliederdaten`, 'Ok', { duration: 5000 }),
    });
  }

  inviteMember() {
    this.dialog
      .open<TeamMemberInviteModalComponent, TeamMemberInviteModalData, TeamMemberInviteModalResultData>(TeamMemberInviteModalComponent, {
        data: { team: this.team },
      })
      .afterClosed()
      .pipe(filter(Boolean))
      .subscribe(memberOrInvite => {
        this.team.members = [...this.team.members, memberOrInvite];
        this.dataSource.data = this.team.members;
      });
  }

  async turnMemberVirtual(member: TeamMemberDto) {
    if (
      !(await lastValueFrom(
        this.dialog
          .open(ConfirmModalComponent, {
            data: {
              title: $localize`Teammitglied in virtuelles Mitglied umwandeln`,
              prompt: $localize`Möchtest Du „${getTeamMemberDisplayName(member)}” wirklich in ein virtuelles Mitglied umwandeln?
      Das Mitglied verliert hierdurch den Zugriff auf das Team.`,
              buttonConfirm: $localize`Ja, umwandeln`,
              buttonDeny: $localize`Nein, abbrechen`,
            },
          })
          .afterClosed(),
      ))
    ) {
      return;
    }

    this.teamsService.updateTeamMember(this.team.id, member.id, { isVirtual: true }).subscribe({
      next: () => {
        member.user = null;
      },
      error: () => {
        this.snackBar.open($localize`Fehler beim Aktualisieren des Teammitglieds`, 'Ok', { duration: 5000 });
      },
    });
  }

  resendInvite(button: any, member: TeamMemberDto) {
    const elementRef = button._elementRef;
    this.teamsService.resendInvite(this.team.id, member.id).subscribe({
      next: () => {
        this.popOverService.whistle(elementRef, $localize`Einladung wurde erneut versendet!`, 'done');
      },
      error: () => {
        this.popOverService.whistle(
          elementRef,
          $localize`Es gab ein Problem beim Senden der Einladung. Bitte versuche es erneut.`,
          'error',
        );
      },
    });
  }

  getDeleteTooltip(member: TeamMemberDto) {
    return member.isInvitee ? $localize`Ausstehende Einladung löschen` : $localize`Teammitglied aus Team entfernen`;
  }

  canUserChangeRoleTo(member: TeamMemberDto, role: TeamRole) {
    if (member.isInvitee && role === 'owner') {
      return false;
    }

    const userRole = this.userMember.role;
    const memberRole = member.role;

    return TeamRoles.indexOf(userRole) <= TeamRoles.indexOf(memberRole) && TeamRoles.indexOf(userRole) <= TeamRoles.indexOf(role);
  }

  async updateRole(member: TeamMemberDto, role: TeamRole, select: MatSelect) {
    if (role === 'owner') {
      if (
        !(await firstValueFrom(
          this.dialog
            .open(ConfirmModalComponent, {
              data: {
                title: $localize`Teammitglied zum Besitzer machen`,
                prompt: $localize`Möchtest Du „${getTeamMemberDisplayName(member)}” wirklich zum Besitzer machen?
            Hierdurch verlierst Du Deine Besitzerrechte und wirst zum Moderator herabgestuft.
            Du kannst diese Aktion nicht rückgängig machen`,
                buttonConfirm: $localize`Ja, Besitzer ändern`,
                buttonDeny: $localize`Nein, abbrechen`,
              },
            })
            .afterClosed(),
        ))
      ) {
        select.writeValue(member.role);
        return;
      }
    }

    this.teamsService.updateTeamMember(this.team.id, member.id, { role }).subscribe({
      next: () => {
        member.role = role;

        if (role === 'owner') {
          this.userMember.role = 'co-owner';
        }
      },
      error: () => {
        this.snackBar.open($localize`Fehler beim Aktualisieren des Teammitglieds`, 'Ok', { duration: 5000 });
        select.writeValue(member.role);
      },
    });
  }

  async deleteMember(memberOrInvite: TeamMemberDto) {
    if (
      await lastValueFrom(
        this.dialog
          .open(ConfirmModalComponent, {
            data: {
              title: $localize`Teammitglied entfernen`,
              prompt: $localize`Möchtest Du „${getTeamMemberDisplayName(memberOrInvite)}” wirklich aus dem Team entfernen?
              Hierdurch gehen sämtliche mit diesem Mitglied verknüpften Daten wie Aufgaben, Kommentare & Meinungen zu Alternativen verloren.

              Falls Du diese Daten behalten möchtest, kannst Du das Mitglied alternativ auch in ein virtuelles umwandeln.`,
              buttonConfirm: $localize`Ja, entfernen`,
              buttonDeny: $localize`Nein, abbrechen`,
            },
          })
          .afterClosed(),
      )
    )
      this.teamsService.deleteTeamMember(this.team.id, memberOrInvite.id).subscribe({
        next: () => {
          this.team.members = this.team.members.filter(m => m.id !== memberOrInvite.id);
          this.dataSource.data = this.team.members;
        },
        error: () => {
          this.snackBar.open($localize`Fehler beim Entfernen des Teammitglieds`);
        },
      });
  }

  updateEmail(whistleElRef: HTMLElement, newEmail: string) {
    const renamingElement = this.currentlyEditingEmailOf;

    if (newEmail.trim() === '' || newEmail === renamingElement.invite.email) {
      this.currentlyEditingEmailOf = null;
      return;
    }

    if (!isEmail(newEmail)) {
      this.popOverService.whistle(whistleElRef, $localize`Bitte gib eine gültige E-Mail-Adresse ein.`, 'error');
      return;
    }

    if (this.team.members.some(m => m.user?.email === newEmail)) {
      this.popOverService.whistle(whistleElRef, $localize`Es gibt bereits ein Teammitglied mit dieser E-Mail-Adresse.`, 'error');
      return;
    }

    if (this.team.members.some(m => m.invite?.email === newEmail)) {
      this.popOverService.whistle(whistleElRef, $localize`Es wurde bereits eine Einladung an diese E-Mail-Adresse gesendet.`, 'error');
      return;
    }

    this.teamsService.updateTeamMember(this.team.id, renamingElement.id, { email: newEmail }).subscribe({
      next: () => {
        renamingElement.invite.email = newEmail;

        this.popOverService.whistle(
          whistleElRef,
          $localize`Die E-Mail-Adresse wurde geändert und die Einladung wurde an die neue Adresse versendet.`,
          'done',
        );
        this.currentlyEditingEmailOf = null;
      },
      error: () => {
        this.popOverService.whistle(whistleElRef, $localize`Fehler beim Aktualisieren des Teammitglieds`, 'error');
      },
    });
  }
}
