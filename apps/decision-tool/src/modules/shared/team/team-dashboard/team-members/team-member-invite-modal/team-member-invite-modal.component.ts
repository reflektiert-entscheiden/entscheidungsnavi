import { Component, Inject, ViewChild } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Observable, finalize } from 'rxjs';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { TeamsService } from '@entscheidungsnavi/api-client';
import { OverlayProgressBarDirective } from '@entscheidungsnavi/widgets/overlay-progress-bar';
import { TeamDto, TeamMemberDto } from '@entscheidungsnavi/api-types';
import { PopOverService } from '@entscheidungsnavi/widgets';
import { MatButton } from '@angular/material/button';
import { ProjectService } from '../../../../../../app/data/project';
import { TeamProject } from '../../../../../../app/data/project/team-project';

export type TeamMemberInviteModalData = {
  team: TeamDto;
};

export type TeamMemberInviteModalResultData = TeamMemberDto | null;

@Component({
  templateUrl: './team-member-invite-modal.component.html',
  styleUrls: ['./team-member-invite-modal.component.scss'],
})
export class TeamMemberInviteModalComponent {
  virtualMember = false;

  virtualForm = new FormGroup({
    name: new FormControl('', [Validators.required]),
  });

  realForm = new FormGroup({
    email: new FormControl('', [Validators.required, Validators.email]),
  });

  readonly teamProject = this.projectService.getProject(TeamProject);
  readonly teamId = this.teamProject?.team.id;
  private team: TeamDto;

  @ViewChild(OverlayProgressBarDirective)
  progressIndicator: OverlayProgressBarDirective;

  get activeForm() {
    return this.virtualMember ? this.virtualForm : this.realForm;
  }

  constructor(
    private dialogRef: MatDialogRef<TeamMemberInviteModalComponent>,
    private teamsService: TeamsService,
    private projectService: ProjectService,
    private popOverService: PopOverService,
    @Inject(MAT_DIALOG_DATA) private data: { team: TeamDto },
  ) {
    this.team = this.data.team;
  }

  close() {
    this.dialogRef.close();
  }

  inviteOrCreate(buttonElRef: MatButton) {
    const whistleElRef = buttonElRef._elementRef;
    if (!this.activeForm.valid) {
      return;
    }

    if (!this.teamProject) {
      return;
    }

    if (!this.virtualMember && this.team.members.some(m => m.invite?.email === this.realForm.value.email)) {
      this.popOverService.whistle(whistleElRef, $localize`Es wurde bereits eine Einladung an diese E-Mail-Adresse gesendet.`, 'error');
      return;
    }

    if (!this.virtualMember && this.team.members.some(m => m.user?.email === this.realForm.value.email)) {
      this.popOverService.whistle(whistleElRef, $localize`Es gibt bereits ein Teammitglied mit dieser E-Mail-Adresse.`, 'error');
      return;
    }

    this.activeForm.disable();

    const observable: Observable<TeamMemberDto> = this.virtualMember
      ? this.teamsService.createVirtualMember(this.teamId, this.virtualForm.value.name)
      : this.teamsService.createInvitedMember(this.teamId, this.realForm.value.email);

    observable
      .pipe(
        this.progressIndicator.reactivePipe(),
        finalize(() => this.activeForm.enable()),
      )
      .subscribe({
        next: memberOrInvite => {
          this.dialogRef.close(memberOrInvite);
        },
        error: () => {
          this.popOverService.whistle(
            whistleElRef,
            this.virtualMember
              ? $localize`Fehler beim Erstellen des virtuellen Teammitglieds`
              : $localize`Fehler beim Versenden der Einladung`,
            'error',
          );
        },
      });
  }

  formChanged() {
    if (this.virtualMember) {
      this.virtualForm.patchValue({ name: this.realForm.value.email }, { emitEvent: false });
    } else {
      this.realForm.patchValue({ email: this.virtualForm.value.name }, { emitEvent: false });
    }
  }
}
