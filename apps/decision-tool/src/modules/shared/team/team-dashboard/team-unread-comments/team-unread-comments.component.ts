import { Component, Input, OnInit } from '@angular/core';
import { TeamsService } from '@entscheidungsnavi/api-client';
import { MatSnackBar } from '@angular/material/snack-bar';
import { DecisionData } from '@entscheidungsnavi/decision-data';
import { groupBy } from 'lodash';
import { MatDialog } from '@angular/material/dialog';
import { TeamDto, TeamCommentDto } from '@entscheidungsnavi/api-types';
import { ProjectService } from '../../../../../app/data/project';
import { TeamProject } from '../../../../../app/data/project/team-project';
import { TeamCommentObjectInfo, TeamCommentsService as TeamCommentsService } from '../../team-comments.service';
import { getTeamMemberDisplayName } from '../../team-member-name.component';
import {
  TeamUnreadCommentsModalComponent,
  TeamUnreadCommentsModalData,
} from './team-unread-comments-modal/team-unread-comments-modal.component';

@Component({
  selector: 'dt-team-unread-comments',
  templateUrl: './team-unread-comments.component.html',
  styleUrls: ['./team-unread-comments.component.scss'],
})
export class TeamUnreadCommentsComponent implements OnInit {
  @Input()
  teamId: string;

  private readonly userMemberId: string;

  state: 'loading' | 'loaded' | 'error' = 'loading';

  protected unreadObjects: { name: string; subName?: string; uuid: string; count: number; firstUnreadCommentId: string }[];

  get hasUnreadComments() {
    return this.unreadObjects && this.unreadObjects.length > 0;
  }

  constructor(
    private projectService: ProjectService,
    private teamsService: TeamsService,
    private snackBar: MatSnackBar,
    private decisionData: DecisionData,
    private teamCommentsService: TeamCommentsService,
    private dialog: MatDialog,
  ) {
    this.userMemberId = projectService.getProject(TeamProject).userMember.id;
  }

  ngOnInit() {
    this.teamsService.getTeam(this.teamId).subscribe({
      next: team => {
        const comments = team.comments;
        const userMember = team.members.find(member => member.id === this.userMemberId);
        const unreadComments = comments.filter(comment => userMember.unreadComments.includes(comment.id));

        this.unreadObjects = this.groupUnreadComments(team, unreadComments);

        this.state = 'loaded';
      },
      error: _ => {
        this.state = 'error';
      },
    });
  }

  showComments(row: (typeof this.unreadObjects)[number]) {
    this.unreadObjects.splice(this.unreadObjects.indexOf(row), 1);
    this.unreadObjects = [...this.unreadObjects];
    this.dialog.open<TeamUnreadCommentsModalComponent, TeamUnreadCommentsModalData>(TeamUnreadCommentsModalComponent, {
      data: {
        objectUUID: row.uuid,
        firstUnreadCommentId: row.firstUnreadCommentId,
        name: row.name,
        subName: row.subName,
      },
    });
  }

  private groupUnreadComments(team: TeamDto, comments: TeamCommentDto[]) {
    const commentsWithInfo: { comment: TeamCommentDto; memberName: string; objectInfo: TeamCommentObjectInfo }[] = [];

    for (const comment of comments) {
      if (comment.objectId === 'whiteboard') {
        continue;
      }

      const info = this.teamCommentsService.getObjectInfoForComment(comment);

      const member = team.members.find(member => member.id === comment.authorMember);

      if (!member) {
        continue;
      }

      if (info) {
        commentsWithInfo.push({ comment, memberName: getTeamMemberDisplayName(member), objectInfo: info });
      }
    }

    const grouped = groupBy(commentsWithInfo, ({ comment }) => comment.objectId);

    return Object.keys(grouped).map(key => {
      const oneObject = grouped[key][0];

      return {
        name: oneObject.objectInfo.name,
        subName: oneObject.objectInfo.subName,
        uuid: oneObject.comment.objectId,
        count: grouped[key].length,
        firstUnreadCommentId: oneObject.comment.id,
      };
    });
  }
}
