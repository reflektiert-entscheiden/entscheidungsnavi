import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';

export type TeamUnreadCommentsModalData = { objectUUID: string; name: string; subName?: string; firstUnreadCommentId: string };

@Component({
  templateUrl: './team-unread-comments-modal.component.html',
  styleUrls: ['./team-unread-comments-modal.component.scss'],
})
export class TeamUnreadCommentsModalComponent {
  constructor(
    private dialogRef: MatDialogRef<TeamUnreadCommentsModalComponent>,
    @Inject(MAT_DIALOG_DATA) public data: TeamUnreadCommentsModalData,
  ) {}

  close() {
    this.dialogRef.close();
  }
}
