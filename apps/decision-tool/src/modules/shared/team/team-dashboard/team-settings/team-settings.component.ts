import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { TeamsService } from '@entscheidungsnavi/api-client';
import { ConfirmModalComponent, ConfirmModalData } from '@entscheidungsnavi/widgets';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Team, TeamConfigTargetGroup } from '@entscheidungsnavi/api-types';
import { FormControl, FormGroup } from '@angular/forms';
import { OverlayProgressBarDirective } from '@entscheidungsnavi/widgets/overlay-progress-bar';
import { OnDestroyObservable } from '@entscheidungsnavi/tools';
import { filter, concatMap, switchMap, Observable, takeUntil, tap, catchError, EMPTY } from 'rxjs';
import { ProjectService } from '../../../../../app/data/project';

@Component({
  selector: 'dt-team-settings',
  templateUrl: './team-settings.component.html',
  styleUrls: ['./team-settings.component.scss'],
})
export class TeamSettingsComponent implements OnInit {
  @Input()
  team: Team;

  @Input()
  isUserOwner: boolean;

  editRightForm = new FormGroup({
    canTakeEditRightFrom: new FormControl<TeamConfigTargetGroup>('all'),
    editRightExpiresFor: new FormControl<TeamConfigTargetGroup>('all'),
  });

  @ViewChild('editRightProgress')
  editRightProgress: OverlayProgressBarDirective;

  @OnDestroyObservable()
  onDestroy$: Observable<void>;

  constructor(
    private dialog: MatDialog,
    private teamAPIService: TeamsService,
    private projectService: ProjectService,
    private snackBar: MatSnackBar,
  ) {}

  ngOnInit(): void {
    this.applySettingsFromTeam();

    this.editRightForm.valueChanges
      .pipe(
        tap(_ => this.editRightForm.disable({ emitEvent: false })),
        switchMap(changedValues =>
          this.teamAPIService.updateTeam(this.team.id, changedValues).pipe(
            tap({
              next: () => {
                this.team.canTakeEditRightFrom = changedValues.canTakeEditRightFrom;
                this.team.editRightExpiresFor = changedValues.editRightExpiresFor;
                this.editRightForm.enable({ emitEvent: false });
              },
            }),
            catchError(_ => {
              this.snackBar.open($localize`Fehler beim Speichern der Änderungen.`, $localize`Ok`, { duration: 5000 });
              this.editRightForm.enable({ emitEvent: false });
              this.applySettingsFromTeam();

              return EMPTY;
            }),
            this.editRightProgress.reactivePipe(),
          ),
        ),
        takeUntil(this.onDestroy$),
      )
      .subscribe();
  }

  private applySettingsFromTeam() {
    this.editRightForm.patchValue(
      {
        canTakeEditRightFrom: this.team.canTakeEditRightFrom,
        editRightExpiresFor: this.team.editRightExpiresFor,
      },
      { emitEvent: false },
    );
  }

  deleteTeam() {
    this.dialog
      .open<ConfirmModalComponent, ConfirmModalData>(ConfirmModalComponent, {
        data: {
          title: $localize`Team löschen`,
          prompt: $localize`Bist Du sicher, dass Du das Team „${this.team.name}” löschen möchten?
              Jegliche Daten, inklusive des Projekts, werden unwiderruflich gelöscht.`,
          template: 'delete',
        },
      })
      .afterClosed()
      .pipe(
        filter(confirmed => confirmed),
        concatMap(_ => this.teamAPIService.deleteTeam(this.team.id)),
        concatMap(_ => this.projectService.closeProject()),
      )
      .subscribe({
        error: () => {
          this.snackBar.open($localize`Beim Löschen des Teams ist ein Fehler aufgetreten.`, $localize`Ok`);
        },
      });
  }
}
