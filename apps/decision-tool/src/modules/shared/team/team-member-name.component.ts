import { Component, Input } from '@angular/core';
import { TeamMemberDto } from '@entscheidungsnavi/api-types';

@Component({
  selector: 'dt-team-member-name',
  template: ` <span [class.italic]="!this.member.user?.name && !this.member.name">{{ displayName }}</span> `,
  styles: [``],
})
export class TeamMemberNameComponent {
  @Input()
  member: TeamMemberDto;

  @Input()
  showEmailWhenNoName = true;

  get displayName() {
    return getTeamMemberDisplayName(this.member, this.showEmailWhenNoName);
  }
}

export function getTeamMemberDisplayName(member: TeamMemberDto, showEmailWhenNoName: boolean = true) {
  if (member == null) {
    return $localize`Unbekanntes Mitglied`;
  }

  let email: string;
  if (showEmailWhenNoName) {
    email = member.isInvitee ? member.invite.email : member.user?.email;

    if (member.isInvitee) {
      email += $localize` (Eingeladen)`;
    }
  }

  return member.user?.name ?? member.name ?? email ?? $localize`Kein Benutzername vergeben`;
}
