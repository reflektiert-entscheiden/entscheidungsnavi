import { Injectable } from '@angular/core';
import { TeamCommentDto } from '@entscheidungsnavi/api-types';
import {
  Alternative,
  CompositeUserDefinedInfluenceFactor,
  Objective,
  Outcome,
  TeamUUIDs,
  SimpleUserDefinedInfluenceFactor,
  DecisionData,
  NaviStep,
  Screw,
} from '@entscheidungsnavi/decision-data';
import { assertUnreachable } from '@entscheidungsnavi/tools';
import { LanguageService } from '../../../app/data/language.service';

type ObjectType = 'alternative' | 'objective' | 'influence-factor' | 'outcome' | 'step' | 'screw' | 'whiteboard';
export type TeamCommentObjectInfo = { name: string; type: ObjectType; subName?: string };

@Injectable({ providedIn: 'root' })
export class TeamCommentsService {
  constructor(
    private languageService: LanguageService,
    private decisionData: DecisionData,
  ) {}

  getObjectInfoForComment(comment: TeamCommentDto): TeamCommentObjectInfo {
    const objectId = comment.objectId;

    if (objectId === 'whiteboard') {
      return { name: $localize`Whiteboard`, type: 'whiteboard' };
    } else if (objectId in TeamUUIDs.STEPS_INVERTED) {
      const step: NaviStep = (TeamUUIDs.STEPS_INVERTED as any)[objectId];
      return { name: $localize`Navi Schritt`, type: 'step', subName: this.languageService.steps[step].name };
    } else {
      const objectInfo = this.decisionData.findObject(objectId);

      if (objectInfo) {
        const { object: decisionDataObject, index } = objectInfo;
        if (decisionDataObject instanceof Alternative) {
          return { name: $localize`Alternative`, type: 'alternative', subName: decisionDataObject.name };
        } else if (decisionDataObject instanceof Objective) {
          return { name: $localize`Ziel`, type: 'objective', subName: decisionDataObject.name };
        } else if (
          decisionDataObject instanceof SimpleUserDefinedInfluenceFactor ||
          decisionDataObject instanceof CompositeUserDefinedInfluenceFactor
        ) {
          return { name: $localize`Einflussfaktor`, type: 'influence-factor', subName: decisionDataObject.name };
        } else if (decisionDataObject instanceof Outcome) {
          const alternativeIndex = Math.floor(index / this.decisionData.objectives.length);
          const objectiveIndex = index % this.decisionData.objectives.length;

          const alternative = this.decisionData.alternatives[alternativeIndex];
          const objective = this.decisionData.objectives[objectiveIndex];

          return { name: $localize`Wirkungsprognose`, type: 'outcome', subName: `${objective.name} - ${alternative.name}` };
        } else if (decisionDataObject instanceof Screw) {
          return { name: $localize`Wichtige Stellhebel`, type: 'screw', subName: decisionDataObject.name };
        } else {
          assertUnreachable(decisionDataObject);
        }
      }
    }

    return null;
  }
}
