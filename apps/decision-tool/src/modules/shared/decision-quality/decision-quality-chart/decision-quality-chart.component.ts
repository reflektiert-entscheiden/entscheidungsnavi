import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { CriteriaValues, DECISION_QUALITY_CRITERIA, DecisionQualityCriterion } from '@entscheidungsnavi/decision-data';
import { DecisionData } from '@entscheidungsnavi/decision-data';
import { range } from 'lodash';
import { ExplanationService, verbalizeDQCriteria } from '../explanation.service';

@Component({
  selector: 'dt-decision-quality-chart',
  templateUrl: './decision-quality-chart.component.html',
  styleUrls: ['./decision-quality-chart.component.scss'],
})
export class DecisionQualityChartComponent implements OnInit {
  @Input() allowRevise = false;

  @Output() closeParent: EventEmitter<void> = new EventEmitter();

  lockedValues: boolean[];
  criteriaValues: CriteriaValues;
  dqCriteria = DECISION_QUALITY_CRITERIA;

  get xScale() {
    return range(0, 11);
  }

  get total() {
    return Math.min(...Object.keys(this.criteriaValues).map((key: DecisionQualityCriterion) => this.criteriaValues[key]));
  }

  get minIndex() {
    let minIndex = -1;
    Object.values(this.criteriaValues).forEach((value, index) => {
      if (this.total === value) {
        minIndex = index;
        return;
      }
    });
    return minIndex;
  }

  get gridRows() {
    return `auto repeat(${3 + Object.keys(this.criteriaValues).length}, 45px) auto`;
  }

  constructor(
    private explanationService: ExplanationService,
    private decisionData: DecisionData,
  ) {}

  ngOnInit() {
    this.criteriaValues = this.decisionData.decisionQuality.criteriaValues;

    const invalidImpactMatrix = !(
      this.decisionData.validateObjectiveScales()[0] &&
      this.decisionData.validateOutcomes()[0] &&
      this.decisionData.outcomes.length > 0
    );
    const invalidWeights = !this.decisionData.validateWeights()[0];
    this.lockedValues = [
      // 1
      !this.decisionData.validateDecisionStatement()[0],
      // 2
      !this.decisionData.validateObjectives()[0],
      // 3
      !this.decisionData.validateAlternatives()[0],
      // 4
      invalidImpactMatrix,
      // 5
      invalidWeights,
      // 6
      invalidWeights,
    ];
  }

  verbalize(criterion: DecisionQualityCriterion) {
    return verbalizeDQCriteria(criterion);
  }

  openExplanation(index: number, criterion: DecisionQualityCriterion) {
    if (this.lockedValues[index] && this.criteriaValues[criterion] === -1) {
      return;
    }

    const dialogRef = this.explanationService.openExplanationModalFor(criterion, this.criteriaValues[criterion], this.allowRevise, false);

    dialogRef.afterClosed().subscribe((closeParent: boolean) => {
      if (closeParent) {
        this.closeParent.emit();
      }

      this.criteriaValues = this.decisionData.decisionQuality.criteriaValues;
    });
  }
}
