import { Injectable } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { DecisionData, DecisionQualityCriterion, NaviStep } from '@entscheidungsnavi/decision-data';
import { takeUntil } from 'rxjs';
import { navLineElement } from '../navline';
import { TrackingService } from '../../../app/data/tracking.service';
import { StarterExplanationModalComponent } from './starter-explanation-modal';

// May be also exported to some other place.
export function verbalizeDQCriteria(criteria: DecisionQualityCriterion): string {
  switch (criteria) {
    case 'APPROPRIATE_FRAME':
      return $localize`Sinnvolle Entscheidungsfrage`;
    case 'MEANINGFUL_RELIABLE_INFORMATION':
      return $localize`Klarheit über Fundamentalziele`;
    case 'CREATIVE_DOABLE_ALTERNATIVES':
      return $localize`Kreative Handlungsoptionen`;
    case 'CLEAR_VALUES':
      return $localize`Zweckmäßige Informationen`;
    case 'LOGICALLY_CORRECT_REASONING':
      return $localize`Logik und Begründbarkeit`;
    case 'COMMITMENT_TO_FOLLOW_THROUGH':
      return $localize`Entschlossenheit der Umsetzung`;
  }
}

export type Explanation = {
  // Longer explanation subdivided in paragraphs.
  content: string[];
  reviseButtonText?: string;
  stepToRevise?: NaviStep;
};

export type ExplanationModalData = {
  criteriaToExplain: DecisionQualityCriterion;
  criteriaValue: number;
  explanation: Explanation;
};

@Injectable({
  providedIn: 'root',
})
export class ExplanationService {
  private static readonly criteriaToExplanation = new Map<DecisionQualityCriterion, Explanation>()
    .set('APPROPRIATE_FRAME', {
      content: [
        $localize`Hast Du in Deiner Entscheidungsfrage eindeutig klargestellt,
        was Du genau entscheiden willst? Ist deutlich gemacht,
        was Du als gegeben ansiehst und was Du bewusst ausklammerst?
        Ist die Entscheidungsfrage ausreichend breit formuliert bzw.
        die Breite sinnvoll gewählt?`,
      ],
      reviseButtonText: $localize`Entscheidungsfrage im Educational Modus (Schritt 1) überarbeiten`,
      stepToRevise: 'decisionStatement',
    })
    .set('MEANINGFUL_RELIABLE_INFORMATION', {
      content: [
        $localize`Konntest Du nach einer kritischen und reflektierenden
        Beschäftigung mit Deinen Wünschen, Bedürfnissen, Vorlieben
        und Abneigungen genau herausfinden, was Dir im Kern wirklich
        wichtig ist? Konntest Du diese Erkenntnisse in wenigen passenden
        Begriffen als Fundamentalziele ausformulieren?`,
      ],
      reviseButtonText: $localize`Fundamentalziele im Educational Modus (Schritt 2) überarbeiten`,
      stepToRevise: 'objectives',
    })
    .set('CREATIVE_DOABLE_ALTERNATIVES', {
      content: [
        $localize`Hast Du genügend Anstrengungen unternommen, um alle machbaren
        und potenziell attraktiven Handlungsmöglichkeiten, die unter Deiner
        Kontrolle stehen, zu identifizieren? Sind hierbei auch Alternativen,
        die signifikant anders sind als nur die naheliegenden Alternativen?
        Warst Du ausreichend kreativ?`,
      ],
      reviseButtonText: $localize`Handlungsoptionen im Educational Modus (Schritt 3) überarbeiten`,
      stepToRevise: 'alternatives',
    })
    .set('CLEAR_VALUES', {
      content: [
        $localize`Hast Du Dich ausreichend um das Einholen nützlicher Informationen
        gekümmert? Konntest Du Dich zur Abschätzung, wie sich die Handlungsalternativen
        in den Zielen auswirken, auf gute und verlässliche Informationsquellen beziehen?
        Hast Du darauf geachtet, dass Deine Einschätzungen nicht durch Biases verzerrt
        angegeben wurden? Hast Du eventuell vorliegende Unsicherheiten adäquat berücksichtigt?`,
      ],
      reviseButtonText: $localize`Wirkungsmodell im Educational Modus (Schritt 4) überarbeiten`,
      stepToRevise: 'impactModel',
    })
    .set('LOGICALLY_CORRECT_REASONING', {
      content: [
        $localize`Kannst Du Deine Entscheidung logisch und nachvollziehbar begründen, d. h.
        kannst Du erläutern, warum die von Dir gewählte Handlungsoption in einer Abwägung der
        positiven und ggf. negativen Aspekte die Beste für Dich ist? Bist Du sicher, dass
        die von Dir angegebenen Zielgewichte und ggf. auch Nutzenbewertungen Deinen wirklichen
        Präferenzen entsprechen?`,
      ],
      reviseButtonText: $localize`Evaluation im Educational Modus (Schritt 5) überarbeiten`,
      stepToRevise: 'results',
    })
    .set('COMMITMENT_TO_FOLLOW_THROUGH', {
      content: [
        $localize`Inwieweit bist Du bereit, diese Entscheidung auch wirklich umzusetzen?
        Bist Du entschlossen, Dich auch so zu verhalten, wie es die Entscheidung vorgibt?
        Hast Du einen guten Plan, wie Du Hindernisse überwinden wirst?`,
      ],
    });

  constructor(
    private dialog: MatDialog,
    private decisionData: DecisionData,
    private trackingService: TrackingService,
  ) {}

  isRevisable(criteriaToExplain: DecisionQualityCriterion) {
    return ExplanationService.criteriaToExplanation.get(criteriaToExplain).stepToRevise !== undefined;
  }

  openExplanationModalFor(criteriaToExplain: DecisionQualityCriterion, criteriaValue: number, allowRevise = false, readonly = true) {
    const explanation = ExplanationService.criteriaToExplanation.get(criteriaToExplain);

    if (explanation === undefined) throw new Error(`No explanation registered yet for criteria '${criteriaToExplain}'.`);

    const dialogRef = this.dialog.open(StarterExplanationModalComponent, {
      data: {
        criteriaData: { criteriaToExplain, criteriaValue, explanation },
        allowRevise: allowRevise && this.isRevisable(criteriaToExplain),
      },
    });

    if (!readonly) {
      dialogRef.componentInstance.valueChange.subscribe((value: number) => {
        this.decisionData.decisionQuality.criteriaValues[criteriaToExplain] = value;
        this.decisionData.decisionQuality.criteriaValues = { ...this.decisionData.decisionQuality.criteriaValues };
        // update pipe input instance without making the pipe inpure
        //inpure pipe causes (click) of "Qualität bewerten" not to work in project-information.component
      });
    }

    return dialogRef;
  }

  generateAssessmentButton(criterion: DecisionQualityCriterion) {
    const element = navLineElement()
      .onClick(() => {
        this.trackingService.trackEvent('open ' + criterion, { category: 'decision quality' });
        const dialogRef = this.openExplanationModalFor(criterion, this.decisionData.decisionQuality.criteriaValues[criterion]);
        dialogRef.componentInstance.valueChange.pipe(takeUntil(dialogRef.afterClosed())).subscribe((value: number) => {
          this.decisionData.decisionQuality.criteriaValues[criterion] = value;
        });
      })
      .color(() => (this.decisionData.decisionQuality.criteriaValues[criterion] === -1 ? 'accent' : 'accent-less'))
      .label($localize`Qualität bewerten`)
      .disabled(() => {
        const disabledCondition = this.decisionData.decisionQuality.criteriaValues[criterion] === -1;
        switch (criterion) {
          case 'APPROPRIATE_FRAME':
            return disabledCondition && !this.decisionData.validateDecisionStatement()[0];
          case 'MEANINGFUL_RELIABLE_INFORMATION':
            return disabledCondition && !this.decisionData.validateObjectives()[0];
          case 'CREATIVE_DOABLE_ALTERNATIVES':
            return disabledCondition && !this.decisionData.validateAlternatives()[0];
          case 'CLEAR_VALUES':
          case 'LOGICALLY_CORRECT_REASONING':
            return (
              disabledCondition &&
              !(
                this.decisionData.validateObjectiveScales()[0] &&
                this.decisionData.validateOutcomes()[0] &&
                this.decisionData.outcomes.length > 0
              )
            );
        }
      })
      .build();
    return element;
  }
}
