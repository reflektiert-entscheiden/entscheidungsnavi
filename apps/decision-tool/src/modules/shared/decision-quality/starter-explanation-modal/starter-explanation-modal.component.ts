import { Component, EventEmitter, Inject, Output } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { Explanation, ExplanationModalData, verbalizeDQCriteria } from '../explanation.service';
import { EducationalNavigationService } from '../../navigation/educational-navigation.service';
import { ModeTransitionService } from '../../mode-transition/mode-transition.service';
import { DecisionDataState } from '../../decision-data-state';

@Component({
  templateUrl: './starter-explanation-modal.component.html',
  styleUrls: ['./starter-explanation-modal.component.scss'],
})
export class StarterExplanationModalComponent {
  @Output() valueChange: EventEmitter<number> = new EventEmitter();
  criteriaToExplain: string;
  criteriaValue: number;
  explanation: Explanation;
  topLabels = [$localize`Weiterer Aufwand erforderlich`, $localize`Weiterer Aufwand nicht erforderlich`];

  allowRevise = false;

  constructor(
    public currentProgressService: EducationalNavigationService,
    private modeTransitionService: ModeTransitionService,
    private dialogRef: MatDialogRef<StarterExplanationModalComponent>,
    @Inject(MAT_DIALOG_DATA) data: { criteriaData: ExplanationModalData; allowRevise: boolean },
    protected decisionDataState: DecisionDataState,
  ) {
    this.criteriaToExplain = verbalizeDQCriteria(data.criteriaData.criteriaToExplain);
    this.criteriaValue = data.criteriaData.criteriaValue;
    this.explanation = data.criteriaData.explanation;
    this.allowRevise = data.allowRevise;
  }

  close(closeParent = false) {
    this.dialogRef.close(closeParent);
  }

  revise() {
    this.close(true);
    this.modeTransitionService.transitionIntoEducational(this.explanation.stepToRevise);
  }

  forwardValueChange(value: number) {
    this.valueChange.emit(value);
  }
}
