import { Component, ContentChild, HostBinding, Input, TemplateRef } from '@angular/core';
import { DecisionData } from '@entscheidungsnavi/decision-data';
import { SideBySideService } from '../../../app/navigation-layout/side-by-side.service';

@Component({
  selector: 'dt-step-description',
  templateUrl: './step-description.component.html',
  styleUrls: ['./step-description.component.scss'],
})
export class StepDescriptionComponent {
  @Input()
  @HostBinding('class.with-margin')
  withBiggerMargin = true;

  @Input()
  mainDescriptionOnly = false;

  @ContentChild('description', { static: true })
  descriptionTemplate: TemplateRef<unknown>;

  @ContentChild('subDescription', { static: true })
  subDescriptionTemplate: TemplateRef<unknown>;

  get isAssistantOpen() {
    return this.sideBySideService.state === 'help';
  }

  constructor(
    protected decisionData: DecisionData,
    private sideBySideService: SideBySideService,
  ) {}

  openAssistant() {
    this.sideBySideService.openHelp();
  }
}
