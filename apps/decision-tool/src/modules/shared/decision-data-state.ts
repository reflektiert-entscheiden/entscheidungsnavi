import { Injectable } from '@angular/core';
import { ProjectService } from '../../app/data/project';
import { TeamProject } from '../../app/data/project/team-project';

// eslint-disable-next-line @typescript-eslint/naming-convention
export abstract class DecisionDataState {
  public abstract get isProjectReadonly(): boolean;
  public abstract get areObjectiveWeightsReadonly(): boolean;
}

export class StaticDecisionDataState extends DecisionDataState {
  override get isProjectReadonly() {
    return this.isProjectReadonlyValue;
  }

  override get areObjectiveWeightsReadonly() {
    return this.areObjectiveWeightsReadonlyValue;
  }

  constructor(
    private readonly isProjectReadonlyValue: boolean,
    private readonly areObjectiveWeightsReadonlyValue: boolean,
  ) {
    super();
  }
}

@Injectable()
export class DecisionDataStateService extends DecisionDataState {
  override get isProjectReadonly() {
    const teamProject = this.projectService.getProject(TeamProject);

    return teamProject && teamProject.validEditToken?.holder !== teamProject.userMember.id;
  }

  override get areObjectiveWeightsReadonly() {
    const teamProject = this.projectService.getProject(TeamProject);

    return this.isProjectReadonly || teamProject?.team.hasAggregatedWeights;
  }

  constructor(private projectService: ProjectService) {
    super();
  }
}
