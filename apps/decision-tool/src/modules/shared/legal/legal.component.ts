import { Component, Input } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ENVIRONMENT } from '../../../environments/environment';
import { AboutUsModalComponent } from '../../../app/about-us-modal/about-us-modal.component';
import { ServiceWorkerService } from '../../../services/service-worker.service';

@Component({
  selector: 'dt-legal',
  templateUrl: './legal.component.html',
  styleUrls: ['./legal.component.scss'],
})
export class LegalComponent {
  @Input()
  horizontalLayout = false;

  readonly year: string;

  get version() {
    return ENVIRONMENT.version;
  }

  get environmentType() {
    return ENVIRONMENT.type;
  }

  get buildTimestamp() {
    return ENVIRONMENT.buildTimestamp;
  }

  constructor(
    private dialog: MatDialog,
    protected serviceWorkerService: ServiceWorkerService,
  ) {
    this.year = new Date().getFullYear() + '';
  }

  openAboutUs() {
    this.dialog.open(AboutUsModalComponent);
  }
}
