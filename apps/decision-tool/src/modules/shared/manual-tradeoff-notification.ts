import { MatSnackBar } from '@angular/material/snack-bar';

export function showManualTradeoffNotification(snackBar: MatSnackBar) {
  snackBar.open($localize`Der explizit eingegebene Trade-off wurde entfernt!`, 'Ok');
}
