import { Component, Input, ElementRef, ChangeDetectorRef } from '@angular/core';

@Component({
  selector: 'dt-extendable-header',
  templateUrl: './extendable-header.component.html',
  styleUrls: ['./extendable-header.component.scss'],
})
export class ExtendableHeaderComponent {
  showContent = false;
  @Input() isLast = false;
  @Input() headerName: string;
  @Input() variation: 'large' | 'small';
  @Input() set initialOpening(val: boolean) {
    this.showContent = val;
  }

  constructor(
    private elementRef: ElementRef,
    private cdRef: ChangeDetectorRef,
  ) {}

  changeVisibility() {
    this.showContent = !this.showContent;

    this.cdRef.detectChanges();

    if (this.showContent) {
      this.elementRef.nativeElement.scrollIntoView({ behavior: 'smooth' });
    }
  }
}
