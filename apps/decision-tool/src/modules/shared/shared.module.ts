import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  AutoFocusDirective,
  ElementRefDirective,
  HoverPopOverDirective,
  OverflowDirective,
  RichTextEditorComponent,
  WidgetsModule,
  WidthTriggerDirective,
} from '@entscheidungsnavi/widgets';
import { RouterLink } from '@angular/router';
import { SelectLineComponent } from '@entscheidungsnavi/widgets/select';
import { NaviStepPipe } from './navigation/navi-step.pipe';
import { NaviSubStepPipe } from './navigation/navi-sub-step.pipe';
import { ErrorModalComponent, ErrorMsgComponent } from './error';
import { ProjectModeDirective } from './project-mode.directive';
import { StepTitleComponent } from './step-title/step-title.component';
import { JsonErrorModalComponent } from './error/json-error-modal/json-error-modal.component';
import { StepDescriptionComponent } from './step-description/step-description.component';
import { DisableInputScrollDirective } from './disable-input-scroll.directive';
import { DecisionQualityChartComponent, StarterExplanationModalComponent } from './decision-quality';
import { SwitchProfessionalEducationalModalComponent } from './mode-transition';
import { ExplainableModule } from './explanation/explainable.module';
import { LeaveStarterModalComponent } from './mode-transition/modals/leave-starter-modal/leave-starter-modal.component';
import { ExtendableHeaderComponent } from './extendable-header/extendable-header.component';
import { DisableInputAutocompletionDirective } from './disable-input-autocompletion.directive';
import { TeamCommentsComponent, TeamDashboardUtilityGraphComponent, TeamDashboardUtilityTableComponent } from './team';
import { TeamProjectProgressComponent } from './team/team-project-progress';
import { NavlineComponent, NavLineElementComponent } from './navline';
import { TeamDashboardComponent } from './team/team-dashboard';
import {
  TeamDashboardObjectiveWeightsComponent,
  TeamDashboardObjectiveWeightsGraphComponent,
  TeamDashboardObjectiveWeightsTableComponent,
} from './team/team-dashboard/team-dashboard-objective-weights';
import { TeamMemberInviteModalComponent, TeamMembersComponent } from './team/team-dashboard/team-members';
import {
  TeamAssignTaskModalComponent,
  TeamTaskComponentWrapperModalComponent,
  TeamTaskModalComponent,
  TeamTasksComponent,
} from './team/team-dashboard/team-tasks';
import { LegalComponent } from './legal/legal.component';
import { TeamSettingsComponent } from './team/team-dashboard/team-settings/team-settings.component';
import { TeamTaskListComponent } from './team/team-dashboard/team-tasks/team-task-list/team-task-list.component';
import { TeamDashboardUtilityComponent } from './team/team-dashboard/team-dashboard-utility';
import { TeamTaskOpinionModalComponent } from './team/team-dashboard/team-tasks/team-task-opinion-modal/team-task-opinion-modal.component';
import { TeamUnreadCommentsComponent, TeamUnreadCommentsModalComponent } from './team/team-dashboard/team-unread-comments';
import { TeamOverviewComponent } from './team/team-dashboard/team-overview/team-overview.component';
import { TeamMemberNameComponent } from './team/team-member-name.component';
import { TeamAccessLogComponent } from './team/team-dashboard/team-access-log';
import { DecisionDataState, DecisionDataStateService } from './decision-data-state';

const DECLARE_AND_EXPORT = [
  DecisionQualityChartComponent,
  DisableInputScrollDirective,
  DisableInputAutocompletionDirective,
  ErrorModalComponent,
  ErrorMsgComponent,
  NaviStepPipe,
  NaviSubStepPipe,
  ProjectModeDirective,
  StarterExplanationModalComponent,
  StepDescriptionComponent,
  StepTitleComponent,
  SwitchProfessionalEducationalModalComponent,
  ExtendableHeaderComponent,
  TeamCommentsComponent,
  TeamProjectProgressComponent,
  NavlineComponent,
  LegalComponent,
  TeamDashboardComponent,
  TeamDashboardObjectiveWeightsComponent,
  TeamDashboardUtilityGraphComponent,
  TeamMembersComponent,
  TeamTasksComponent,
  TeamAssignTaskModalComponent,
  TeamMemberInviteModalComponent,
  TeamTaskModalComponent,
  TeamTaskComponentWrapperModalComponent,
  TeamSettingsComponent,
  TeamTaskListComponent,
  TeamDashboardObjectiveWeightsTableComponent,
  TeamDashboardObjectiveWeightsGraphComponent,
  TeamDashboardUtilityComponent,
  TeamDashboardUtilityTableComponent,
  TeamTaskOpinionModalComponent,
  TeamUnreadCommentsComponent,
  TeamUnreadCommentsModalComponent,
  TeamOverviewComponent,
  TeamMemberNameComponent,
  TeamAccessLogComponent,
];

@NgModule({
  imports: [
    CommonModule,
    ExplainableModule,
    HoverPopOverDirective,
    WidgetsModule,
    OverflowDirective,
    RichTextEditorComponent,
    WidthTriggerDirective,
    RouterLink,
    SelectLineComponent,
    ElementRefDirective,
    AutoFocusDirective,
  ],
  declarations: [...DECLARE_AND_EXPORT, LeaveStarterModalComponent, JsonErrorModalComponent, NavLineElementComponent],
  exports: [...DECLARE_AND_EXPORT, ExplainableModule, WidgetsModule],
  providers: [
    {
      provide: DecisionDataState,
      useClass: DecisionDataStateService,
    },
  ],
})
export class SharedModule {}
