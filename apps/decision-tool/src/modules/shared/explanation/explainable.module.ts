import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { WidgetsModule } from '@entscheidungsnavi/widgets';
import { CriteriaExplanationOpenButtonComponent } from './criteria-explanation-open-button.component';
import { SimpleExplanationOpenButtonComponent } from './simple-explanation-open-button.component';
import { ExplainableDirective, HighlightComponent } from './explainable.directive';
import { ExplanationButtonDirective } from './explanation-button.directive';

const DECLARE_AND_EXPORT = [
  CriteriaExplanationOpenButtonComponent,
  SimpleExplanationOpenButtonComponent,
  ExplainableDirective,
  ExplanationButtonDirective,
];

@NgModule({
  declarations: [...DECLARE_AND_EXPORT, HighlightComponent],
  exports: [...DECLARE_AND_EXPORT],
  imports: [CommonModule, WidgetsModule],
})
export class ExplainableModule {}
