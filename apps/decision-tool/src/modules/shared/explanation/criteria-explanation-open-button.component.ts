import { Component, Input } from '@angular/core';
import { DecisionQualityCriterion } from '@entscheidungsnavi/decision-data';
import { DecisionData } from '@entscheidungsnavi/decision-data';
import { ExplanationService } from '../decision-quality';

@Component({
  selector: 'dt-explanation-criteria-open-button',
  template: '<dt-explanation-simple-open-button (explain)="openExplanationModal()"></dt-explanation-simple-open-button>',
})
export class CriteriaExplanationOpenButtonComponent {
  @Input()
  criteriaToExplain: DecisionQualityCriterion;

  constructor(
    protected explanationService: ExplanationService,
    private decisionData: DecisionData,
  ) {}

  openExplanationModal() {
    this.explanationService.openExplanationModalFor(
      this.criteriaToExplain,
      this.decisionData.decisionQuality.criteriaValues[this.criteriaToExplain],
      this.explanationService.isRevisable(this.criteriaToExplain),
      false,
    );
  }
}
