import {
  ChangeDetectorRef,
  Component,
  ComponentRef,
  Directive,
  ElementRef,
  HostBinding,
  Input,
  OnChanges,
  OnDestroy,
  Renderer2,
  SimpleChanges,
  ViewContainerRef,
} from '@angular/core';
import { DomPortalOutlet } from '@angular/cdk/portal';
import { ExplanationButtonDirective } from './explanation-button.directive';

@Component({
  template: '',
  styles: [
    `
      @use 'variables';

      :host {
        position: absolute;
        border: 2px solid variables.$orange;
        inset: 0;

        &:not(.visible) {
          display: none;
        }
      }
    `,
  ],
})
export class HighlightComponent {
  @HostBinding('class.visible')
  private _visible: boolean;

  @Input()
  get visible() {
    return this._visible;
  }

  set visible(newVisible) {
    this._visible = newVisible;
    this.cdr.markForCheck();
  }

  constructor(private cdr: ChangeDetectorRef) {}
}

@Directive({
  selector: '[dtExplainable]',
})
export class ExplainableDirective implements OnDestroy, OnChanges {
  @Input()
  explanationTrigger: ExplanationButtonDirective;

  @Input()
  explainable = true;

  @Input()
  triggerAnchor: 'middle' | 'bottom-left' = 'middle';

  private explanationTriggerOutlet: DomPortalOutlet | null = null;
  private explanationTriggerPortalHost: HTMLDivElement | null;

  private highlightComponentRef: ComponentRef<HighlightComponent> | null = null;

  constructor(
    private elementRef: ElementRef,
    private viewContainerRef: ViewContainerRef,
    private render2: Renderer2,
  ) {}

  // Should be idempotent.
  createOverlays() {
    this.elementRef.nativeElement.style.position = 'relative';

    if (this.highlightComponentRef === null) {
      this.highlightComponentRef = this.viewContainerRef.createComponent(HighlightComponent);
      this.render2.setStyle(this.highlightComponentRef.location.nativeElement, 'z-index', 102);
      this.highlightComponentRef.instance.visible = false;
      this.render2.appendChild(this.elementRef.nativeElement, this.highlightComponentRef.location.nativeElement);
    }

    if (this.explanationTriggerOutlet === null) {
      this.explanationTriggerPortalHost = this.render2.createElement('div');
      this.render2.setStyle(this.explanationTriggerPortalHost, 'position', 'absolute');
      this.render2.setStyle(this.explanationTriggerPortalHost, 'z-index', 103); // Must be higher than the highlight.
      this.render2.appendChild(this.elementRef.nativeElement, this.explanationTriggerPortalHost);

      this.explanationTriggerOutlet = new DomPortalOutlet(this.explanationTriggerPortalHost, undefined, undefined, undefined, document);
    }
  }

  // Should be idempotent.
  private showHighlight() {
    this.highlightComponentRef.instance.visible = true;
  }

  // Should be idempotent.
  private hideHighlight() {
    this.highlightComponentRef.instance.visible = false;
  }

  // Should be idempotent.
  private showAndAttachExplanationTrigger() {
    this.explanationTrigger.visible = true;

    if (this.explanationTriggerOutlet.hasAttached()) return;

    const explanationTriggerTemplatePortal = this.explanationTrigger.getExplanationButtonPortal();

    explanationTriggerTemplatePortal.element.addEventListener('mouseenter', this.showHighlight.bind(this));
    explanationTriggerTemplatePortal.element.addEventListener('mouseleave', this.hideHighlight.bind(this));

    this.explanationTriggerOutlet.attach(explanationTriggerTemplatePortal);

    this.explanationTriggerPortalHost.style.top = '0';
    this.explanationTriggerPortalHost.style.left = '0';

    if (this.triggerAnchor === 'middle') {
      this.explanationTriggerPortalHost.style.transform = 'translate(-50%, -50%)';
    } else {
      this.explanationTriggerPortalHost.style.transform = 'translate(100%, -100%)';
    }
  }

  // Should be idempotent.
  private hideExplanationTrigger() {
    this.explanationTrigger.visible = false;
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes['explainable'] || changes['explanationTrigger']) {
      if (changes['explanationTrigger'] && this.explanationTrigger) {
        this.createOverlays();
      }

      if (this.explanationTrigger !== null && this.explainable) {
        this.showAndAttachExplanationTrigger();
      } else {
        this.hideHighlight();
        if (this.explanationTrigger !== null) {
          this.hideExplanationTrigger();
        } else {
          this.explanationTriggerOutlet.detach();
        }
      }
    }
  }

  ngOnDestroy() {
    this.explanationTriggerOutlet?.dispose();
  }
}
