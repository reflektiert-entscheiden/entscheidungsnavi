import { Injectable } from '@angular/core';
import { DecisionData, NaviStep, NaviSubStep, ProjectMode } from '@entscheidungsnavi/decision-data';
import { Router } from '@angular/router';
import { firstValueFrom, lastValueFrom } from 'rxjs';
import { MatDialog } from '@angular/material/dialog';
import { EducationalNavigationService } from '../navigation/educational-navigation.service';
import { InterpolationService } from '../../../services/interpolation.service';
import { educationalSubStepToUrl } from '../navigation/educational-navigation';
import { LeaveStarterModalComponent } from './modals/leave-starter-modal/leave-starter-modal.component';
import { SwitchProfessionalEducationalModalComponent } from '.';

@Injectable({
  providedIn: 'root',
})
export class ModeTransitionService {
  constructor(
    private dialog: MatDialog,
    private router: Router,
    private decisionData: DecisionData,
    private educationalNavigationService: EducationalNavigationService,
    private stepsInterpolatingService: InterpolationService,
  ) {}

  /**
   * Navigates to educational mode.
   *
   * @param toSpecificStep - Optional step we transition to.
   * @returns A Promise that resolves to true when navigation succeeds, to false when navigation fails, or is rejected on error.
   */
  async transitionIntoEducational(toSpecificStep?: NaviStep): Promise<boolean> {
    const from = this.decisionData.projectMode;

    if (from === 'educational') {
      if (toSpecificStep == null) {
        return true;
      }

      return this.educationalNavigationService.navigateToStep(toSpecificStep);
    }

    if (from === 'starter') {
      const confirmed = await firstValueFrom(
        this.dialog.open(LeaveStarterModalComponent, { data: { target: 'educational' } }).beforeClosed(),
      );

      if (!confirmed) {
        return false;
      }

      const firstStepWithError = this.decisionData.getFirstStepWithErrors();

      this.educationalNavigationService.updateProgress();
      return await this.handleTransition(
        () => this.educationalNavigationService.navigateToStep(toSpecificStep ?? firstStepWithError ?? 'results'),
        'educational',
      );
    }

    // We use beforeClosed on the modal to make sure we navigate and modify the history _before_ the modal closes and calls `history.pop()`.
    // Otherwise, the asynchronous nature of that call will revert our navigation.
    const selectedStepToNavigateTo: NaviStep =
      toSpecificStep ??
      (await lastValueFrom(
        this.dialog
          .open<SwitchProfessionalEducationalModalComponent, void, NaviStep>(SwitchProfessionalEducationalModalComponent)
          .beforeClosed(),
      ));

    if (selectedStepToNavigateTo == null) return false;

    const subStepToNavigateTo: NaviSubStep = { step: selectedStepToNavigateTo };

    if (!['impactModel', 'finishProject'].includes(selectedStepToNavigateTo)) {
      subStepToNavigateTo.subStepIndex = 0;
    }

    // Set a decision statement from professional
    if (!this.decisionData.decisionStatement.statement) {
      this.decisionData.decisionStatement.statement = InterpolationService.DECISION_STATEMENT_DEFAULT;
    }

    this.educationalNavigationService.unlockProgress();

    const targetUrl = educationalSubStepToUrl(subStepToNavigateTo);

    return this.handleTransition(targetUrl, 'educational');
  }

  /**
   * Navigates to starter mode.
   *
   * @returns A Promise that resolves to true when navigation succeeds, to false when navigation fails, or is rejected on error.
   */
  async transitionIntoProfessional(): Promise<boolean> {
    const from = this.decisionData.projectMode;
    if (from === 'professional') return true;

    if (from === 'starter') {
      const confirmed = await firstValueFrom(
        this.dialog.open(LeaveStarterModalComponent, { data: { target: 'professional' } }).beforeClosed(),
      );

      if (!confirmed) {
        return false;
      }
    }

    const firstStepWithError = this.decisionData.getFirstStepWithErrors();

    let targetUrl: string;

    if (!firstStepWithError) {
      // Enough data to enter evaluation (second tab). Do it.
      targetUrl = '/professional/evaluate-and-decide';
      this.stepsInterpolatingService.interpolateForSecondProfessionalTab();
    } else {
      // User has to do some work in the first tab until he/she can enter the second one.
      targetUrl = '/professional/structure-and-estimate';
      this.stepsInterpolatingService.interpolateForFirstProfessionalTab();
    }

    return await this.handleTransition(targetUrl, 'professional');
  }

  /**
   * @returns A Promise that resolves to true when navigation succeeds, to false when navigation fails, or is rejected on error.
   */
  private async handleTransition(
    targetUrlOrNavigationFn: string | (() => Promise<boolean>),
    modeToTransitionTo: ProjectMode,
  ): Promise<boolean> {
    console.log(`Transitioning to ${modeToTransitionTo}...`);

    const oldMode = this.decisionData.projectMode;
    this.decisionData.projectMode = modeToTransitionTo;

    const navigationFn =
      typeof targetUrlOrNavigationFn === 'string' ? () => this.router.navigateByUrl(targetUrlOrNavigationFn) : targetUrlOrNavigationFn;

    if (await navigationFn()) {
      // Navigation successful
      return true;
    } else {
      // Navigation aborted. Reset the project mode.
      console.log('Transition aborted.');
      this.decisionData.projectMode = oldMode;

      return false;
    }
  }
}
