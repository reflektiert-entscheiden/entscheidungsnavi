import { Component } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { DecisionData, NAVI_STEP_ORDER, NaviStep } from '@entscheidungsnavi/decision-data';
import { range } from 'lodash';
import { LanguageService } from '../../../../../app/data/language.service';

@Component({
  templateUrl: './switch-professional-educational-modal.component.html',
  styleUrls: ['./switch-professional-educational-modal.component.scss'],
})
export class SwitchProfessionalEducationalModalComponent {
  readonly stepLabels: string[];
  readonly farthestReachableStepIndex: number;
  readonly tooltipsForDisabledSteps = [
    null,
    null,
    null,
    null,
    $localize`Für diesen Schritt benötigst Du das vollständig ausgefüllte Wirkungsmodell, valide Zielgewichte und Nutzenfunktionen.`,
  ];

  indexOfChosenStep = -1;
  readonly disabledSteps: number[];

  constructor(
    private dialogRef: MatDialogRef<SwitchProfessionalEducationalModalComponent>,
    private decisionData: DecisionData,
    languageService: LanguageService,
  ) {
    const firstStepWithError = this.decisionData.getFirstStepWithErrors();

    const farthestReachable: NaviStep = firstStepWithError || 'finishProject';

    this.indexOfChosenStep = NAVI_STEP_ORDER.indexOf(farthestReachable);
    const stepPrefix = (step: NaviStep) => $localize`Schritt` + ' ' + (NAVI_STEP_ORDER.indexOf(step) + 1) + ': ';
    this.stepLabels = NAVI_STEP_ORDER.map(step => (step === 'finishProject' ? '' : stepPrefix(step)) + languageService.steps[step].name);
    this.farthestReachableStepIndex = NAVI_STEP_ORDER.indexOf(farthestReachable);
    this.disabledSteps = range(this.farthestReachableStepIndex + 1, NAVI_STEP_ORDER.length);
  }

  close(shouldContinue: boolean) {
    this.dialogRef.close(shouldContinue ? NAVI_STEP_ORDER[this.indexOfChosenStep] : null);
  }
}
