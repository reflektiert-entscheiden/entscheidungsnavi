import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  templateUrl: './leave-starter-modal.component.html',
  styleUrls: ['./leave-starter-modal.component.scss'],
})
export class LeaveStarterModalComponent {
  get targetModeName() {
    return this.data.target === 'educational' ? 'Educational' : 'Professional';
  }

  constructor(
    public modal: MatDialogRef<LeaveStarterModalComponent>,
    @Inject(MAT_DIALOG_DATA) private data: { target: 'educational' | 'professional' },
  ) {}
}
