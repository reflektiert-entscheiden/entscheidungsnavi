import { Pipe, PipeTransform } from '@angular/core';
import { NaviSubStep } from '@entscheidungsnavi/decision-data';
import { LanguageService } from '../../../app/data/language.service';

@Pipe({
  name: 'naviSubStep',
})
export class NaviSubStepPipe implements PipeTransform {
  constructor(private languageService: LanguageService) {}
  transform(value: NaviSubStep): string {
    return this.languageService.steps[value.step].subSteps[value.subStepIndex];
  }
}
