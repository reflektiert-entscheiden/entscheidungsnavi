import { educationalSubStepToUrl, urlToEducationalSubStep } from './educational-navigation';

describe('navigation-step', () => {
  describe('urlToNaviSubStep', () => {
    it('works for main steps', () => {
      expect(urlToEducationalSubStep('/decisionstatement')).toEqual({ step: 'decisionStatement' });
      expect(urlToEducationalSubStep('/impactmodel')).toEqual({ step: 'impactModel' });
      expect(urlToEducationalSubStep('/impactmodel/uncertaintyfactors')).toEqual({ step: 'impactModel' });
      expect(urlToEducationalSubStep('/results')).toEqual({ step: 'results' });
    });

    it('works for substeps', () => {
      expect(urlToEducationalSubStep('/decisionstatement/steps/1')).toEqual({ step: 'decisionStatement', subStepIndex: 0 });
      expect(urlToEducationalSubStep('/decisionstatement/steps/3')).toEqual({ step: 'decisionStatement', subStepIndex: 2 });
      expect(urlToEducationalSubStep('/alternatives/steps/4')).toEqual({ step: 'alternatives', subStepIndex: 3 });
      expect(urlToEducationalSubStep('/results/steps/2')).toEqual({ step: 'results', subStepIndex: 1 });
    });
  });

  describe('naviSubStepToUrl', () => {
    it('works for main steps', () => {
      expect(educationalSubStepToUrl({ step: 'decisionStatement' })).toBe('/decisionstatement');
      expect(educationalSubStepToUrl({ step: 'impactModel' })).toBe('/impactmodel');
      expect(educationalSubStepToUrl({ step: 'results' })).toBe('/results');
    });

    it('works for substeps', () => {
      expect(educationalSubStepToUrl({ step: 'decisionStatement', subStepIndex: 0 })).toBe('/decisionstatement/steps/1');
      expect(educationalSubStepToUrl({ step: 'decisionStatement', subStepIndex: 2 })).toBe('/decisionstatement/steps/3');
      expect(educationalSubStepToUrl({ step: 'alternatives', subStepIndex: 3 })).toBe('/alternatives/steps/4');
      expect(educationalSubStepToUrl({ step: 'results', subStepIndex: 1 })).toBe('/results/steps/2');
    });
  });
});
