import { NaviStep, NaviSubStep } from '@entscheidungsnavi/decision-data';

export interface NavigationStepMetaData {
  routerLink: string;
  icon: string;
}

export const EDUCATIONAL_STEPS_META_DATA: { [key in NaviStep]: NavigationStepMetaData } = {
  decisionStatement: {
    routerLink: '/decisionstatement',
    icon: 'contact_support',
  },
  objectives: {
    routerLink: '/objectives',
    icon: 'track_changes',
  },
  alternatives: {
    routerLink: '/alternatives',
    icon: 'alt_route',
  },
  impactModel: {
    routerLink: '/impactmodel',
    icon: 'view_compact',
  },
  results: {
    routerLink: '/results',
    icon: 'assessment',
  },
  finishProject: {
    routerLink: '/finishproject',
    icon: 'fact_check',
  },
};

export function urlToEducationalSubStep(url: string): NaviSubStep | null {
  const result = Object.entries(EDUCATIONAL_STEPS_META_DATA).find(step => url.startsWith(step[1].routerLink)) as [
    NaviStep,
    NavigationStepMetaData,
  ];

  if (result != null) {
    const [stepId, step] = result;
    let subStepIndex: number;
    const restUrl = url.substring(step.routerLink.length + 1);

    const hint = /^steps\/(\d)/.exec(restUrl);
    if (hint) {
      // (the urls are 1-based, the substep indices are 0-based)
      subStepIndex = parseInt(hint[1]) - 1;
    }
    // all other sub-urls are assumed to be equal to the main step

    return { step: stepId, subStepIndex };
  }

  return null;
}

export function educationalSubStepToUrl(subStep: NaviSubStep): string {
  let url = EDUCATIONAL_STEPS_META_DATA[subStep.step].routerLink;
  if (subStep.subStepIndex != null) {
    url += `/steps/${subStep.subStepIndex + 1}`;
  }
  return url;
}
