import { Pipe, PipeTransform } from '@angular/core';
import { NaviStep } from '@entscheidungsnavi/decision-data';
import { LanguageService } from '../../../app/data/language.service';

@Pipe({
  name: 'naviStep',
})
export class NaviStepPipe implements PipeTransform {
  constructor(private languageService: LanguageService) {}
  transform(value: NaviStep): string {
    return this.languageService.steps[value].name;
  }
}
