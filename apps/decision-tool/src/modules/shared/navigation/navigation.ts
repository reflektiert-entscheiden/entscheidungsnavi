import { ProjectMode } from '@entscheidungsnavi/decision-data';
import { assertUnreachable } from '@entscheidungsnavi/tools/assert-unreachable';
import { EDUCATIONAL_STEPS_META_DATA } from './educational-navigation';

export function urlToProjectMode(url: string): ProjectMode | null {
  const [firstUrlSegment] = url.substring(1).split('/');

  if (firstUrlSegment === 'starter') {
    return 'starter';
  } else if (firstUrlSegment === 'professional') {
    return 'professional';
  } else if (
    Object.values(EDUCATIONAL_STEPS_META_DATA)
      .map(step => step.routerLink.substring(1))
      .includes(firstUrlSegment)
  ) {
    return 'educational';
  }

  return null;
}

export function getStartUrlForProjectMode(projectMode: ProjectMode) {
  switch (projectMode) {
    case 'starter':
      return '/starter';
    case 'educational':
      return '/decisionstatement/steps/1';
    case 'professional':
      return '/professional';
    default:
      assertUnreachable(projectMode);
  }
}
