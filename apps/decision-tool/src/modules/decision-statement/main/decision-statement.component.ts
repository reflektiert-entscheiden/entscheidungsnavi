import { Component, Input, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { DECISION_STATEMENT_STEPS, DecisionData } from '@entscheidungsnavi/decision-data';
import { EducationalNavigationService } from '../../shared/navigation/educational-navigation.service';
import { HelpMenuProvider, helpPage } from '../../../app/help/help';
import { ExplanationService } from '../../shared/decision-quality';
import { HelpBackgroundComponent } from '../hints/help-background/help-background.component';
import { Navigation, NavLine, navLineElement } from '../../shared/navline';
import { DecisionDataState } from '../../shared/decision-data-state';

@Component({
  selector: 'dt-decision-statement',
  templateUrl: './decision-statement.component.html',
  styleUrls: ['./decision-statement.component.scss'],
})
export class DecisionStatementComponent implements OnInit, HelpMenuProvider, Navigation {
  navLine = new NavLine({
    left: [
      navLineElement()
        .back(() => '/decisionstatement/steps/4')
        .build(),
    ],
    middle: [this.explanationService.generateAssessmentButton('APPROPRIATE_FRAME')],
    right: [navLineElement().continue('/objectives/steps/1').build()],
  });

  @Input()
  showTitle = true;

  helpMenu = [
    helpPage()
      .name($localize`Ergebnisseite`)
      .template(() => this.helpTemplate)
      .build(),
    helpPage()
      .name($localize`Hintergrundwissen zum Schritt 1`)
      .component(HelpBackgroundComponent)
      .build(),
    helpPage()
      .name($localize`Videos`)
      .youtube(0)
      .build(),
  ];

  @ViewChild('helpTemplate') helpTemplate: TemplateRef<any>;

  constructor(
    protected decisionData: DecisionData,
    private currentProgressService: EducationalNavigationService,
    private explanationService: ExplanationService,
    protected decisionDataState: DecisionDataState,
  ) {}

  ngOnInit() {
    this.decisionData.decisionStatement.subStepProgression = DECISION_STATEMENT_STEPS[DECISION_STATEMENT_STEPS.length - 1];
    this.currentProgressService.updateProgress();

    if (this.decisionData.decisionStatement.statement == null) {
      this.decisionData.decisionStatement.statement = this.decisionData.decisionStatement.statement_attempt_2 ?? '';
      this.currentProgressService.updateProgress();
    }

    if (this.decisionData.decisionStatement.preNotes_2.length > 0 && this.decisionData.decisionStatement.preNotesFinal.length === 0) {
      this.decisionData.decisionStatement.preNotesFinal = this.decisionData.decisionStatement.preNotes_2.filter(note => note.length > 0);
    }

    if (this.decisionData.decisionStatement.afterNotes_2.length > 0 && this.decisionData.decisionStatement.afterNotesFinal.length === 0) {
      this.decisionData.decisionStatement.afterNotesFinal = this.decisionData.decisionStatement.afterNotes_2.filter(
        note => note.length > 0,
      );
    }
  }
}
