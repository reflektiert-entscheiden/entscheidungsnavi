import { Component, Input, OnInit, ViewChildren, QueryList, ElementRef } from '@angular/core';
import { CdkDragDrop, moveItemInArray } from '@angular/cdk/drag-drop';

@Component({
  selector: 'dt-decision-statement-note-list',
  templateUrl: './decision-statement-note-list.component.html',
  styleUrls: ['./decision-statement-note-list.component.scss'],
})
export class DecisionStatementNoteListComponent implements OnInit {
  @Input() readonly = false;
  @Input() noteArray: string[];

  @ViewChildren('noteInput')
  noteInputs: QueryList<ElementRef<HTMLInputElement>>;

  ngOnInit() {
    this.noteArray.forEach((entry, index) => {
      if (entry === '') this.noteArray.splice(index, 1);
    });
    // always start with an empty line
    if (this.noteArray.length === 0) this.noteArray.push('');
  }

  newRow(index?: number) {
    if (index) {
      this.noteArray.splice(index, 0, '');
    } else {
      this.noteArray.push('');
    }
    requestAnimationFrame(() => {
      this.noteInputs.toArray()[index ? index : this.noteInputs.length - 1].nativeElement.focus();
    });
  }

  moveNotes(event: CdkDragDrop<string[]>) {
    moveItemInArray(this.noteArray, event.previousIndex, event.currentIndex);
  }

  removeNote(index: number) {
    this.noteArray.splice(index, 1);
  }
}
