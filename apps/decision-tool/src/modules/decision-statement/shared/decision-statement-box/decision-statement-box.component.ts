import { Component, ElementRef, EventEmitter, Input, Output, ViewChild } from '@angular/core';
import { DecisionData } from '@entscheidungsnavi/decision-data';

@Component({
  selector: 'dt-decision-statement-box',
  templateUrl: './decision-statement-box.component.html',
  styleUrls: ['./decision-statement-box.component.scss'],
})
export class DecisionStatementBoxComponent {
  @ViewChild('decisionStatementInput') decisionStatementInputRef: ElementRef<HTMLDivElement>;

  @Input() decisionStatement: string;
  @Input() decisionStatementHint: string;

  @Input() readonly = false;

  @Output() decisionStatementChange = new EventEmitter<string>();

  get dropDownMode() {
    return this.decisionStatementHint != null;
  }

  constructor(protected decisionData: DecisionData) {}

  commitDecisionStatement() {
    let newDecisionStatement = this.decisionStatementInputRef.nativeElement.textContent;

    newDecisionStatement = newDecisionStatement.replace(/\n/g, ' ');

    if (this.decisionData.isProfessional()) {
      newDecisionStatement ||= this.decisionStatement;
    }

    this.decisionStatement = newDecisionStatement;
    this.decisionStatementChange.emit(newDecisionStatement);
  }
}
