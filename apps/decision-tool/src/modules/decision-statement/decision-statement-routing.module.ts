import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DecisionStatementHint1Component } from './hints/hint1/decision-statement-hint1.component';
import { DecisionStatementHint2Component } from './hints/hint2/decision-statement-hint2.component';
import { DecisionStatementHint3Component } from './hints/hint3/decision-statement-hint3.component';
import { DecisionStatementComponent } from './main/decision-statement.component';
import { DecisionStatementHint4Component } from './hints/hint4/decision-statement-hint4.component';

const routes: Routes = [
  {
    path: '',
    component: DecisionStatementComponent,
  },
  {
    path: 'hints',
    redirectTo: 'steps',
  },
  {
    path: 'steps',
    pathMatch: 'full',
    redirectTo: 'steps/1',
  },
  {
    path: 'steps',
    children: [
      {
        path: '1',
        component: DecisionStatementHint1Component,
      },
      {
        path: '2',
        component: DecisionStatementHint2Component,
      },
      {
        path: '3',
        component: DecisionStatementHint3Component,
      },
      {
        path: '4',
        component: DecisionStatementHint4Component,
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DecisionStatementRoutingModule {}
