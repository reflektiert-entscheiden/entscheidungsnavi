import { Component, Input, OnDestroy } from '@angular/core';
import { DecisionData, Value } from '@entscheidungsnavi/decision-data';
import { sum } from 'lodash';
import { catchError, EMPTY, of } from 'rxjs';
import { QuickstartService } from '@entscheidungsnavi/api-client';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatButtonModule } from '@angular/material/button';
import { MatDividerModule } from '@angular/material/divider';
import { MatIconModule } from '@angular/material/icon';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';

import { ReactiveFormsModule } from '@angular/forms';
import { OverflowDirective, WidgetsModule } from '@entscheidungsnavi/widgets';
import { SharedModule } from '../../shared/shared.module';
import { LanguageService } from '../../../app/data/language.service';
import { DecisionDataState } from '../../shared/decision-data-state';

@Component({
  selector: 'dt-fundamental-values',
  templateUrl: './fundamental-values.component.html',
  styleUrls: ['./fundamental-values.component.scss'],
  imports: [
    MatButtonModule,
    MatDividerModule,
    MatIconModule,
    MatProgressSpinnerModule,
    ReactiveFormsModule,
    SharedModule,
    WidgetsModule,
    OverflowDirective,
  ],
  standalone: true,
})
export class FundamentalValuesComponent implements OnDestroy {
  @Input() allowEditing = true;

  isLoading = false;
  groupSize = 0;
  private hasReset = false;

  get state(): 'loading' | 'empty' | 'ready' {
    if (this.isLoading) return 'loading';
    else if (this.decisionData.decisionStatement.values.length === 0) return 'empty';
    else return 'ready';
  }

  constructor(
    protected decisionData: DecisionData,
    protected decisionDataState: DecisionDataState,
    private quickstartService: QuickstartService,
    private languageService: LanguageService,
    private snackBar: MatSnackBar,
  ) {
    if (this.decisionData.decisionStatement.values.length === 0) {
      this.resetValues();
    } else {
      this.sortValues();
    }
  }

  ngOnDestroy() {
    this.performTracking();
  }

  private performTracking() {
    // Only track if we loaded a set of new values this time
    if (!this.hasReset || this.groupSize === 0) return;

    // The average weight over ALL set values, not only the ones from the backend
    const sumOfWeights = sum(this.decisionData.decisionStatement.values.map(value => value.val).filter(value => value != null));

    // All values that stem from our backend list
    const valuesFromBackend = this.decisionData.decisionStatement.values.filter(value => value.valueId != null);

    if (valuesFromBackend.length > 0 && sumOfWeights > 0) {
      this.quickstartService
        .trackValueUsage({
          values: valuesFromBackend.map(value => ({
            id: value.valueId,
            // Divide the values by the average to account for different weight scaling of users
            // (some might only select values between 50 and 60, some between 10 and 90).
            score: (value.val || 0) / sumOfWeights,
          })),
        })
        .pipe(catchError(() => EMPTY))
        .subscribe();
    }
  }

  resetValues() {
    this.isLoading = true;
    this.quickstartService
      .getValues()
      .pipe(
        catchError(() => {
          this.snackBar.open($localize`Fehler beim Laden der Standard-Werteliste. Die Werteliste wurde stattdessen geleert.`, 'Ok');
          return of([]);
        }),
      )
      .subscribe(values => {
        // clone the default values to decisionData
        this.decisionData.decisionStatement.values = values.map(
          value => new Value(this.languageService.isEnglish ? value.name.en : value.name.de, null, value.id),
        );

        this.hasReset = true;
        this.isLoading = false;
        this.groupSize = 0;
      });
  }

  addValue(index?: number) {
    this.decisionData.decisionStatement.addValue(index);
  }

  removeValue(index: number) {
    this.decisionData.decisionStatement.removeValue(index);
    if (index < this.groupSize) {
      // if the value is in the top group, lower the group size, so that no other value moves to the group
      this.groupSize--;
    }
  }

  updateGroupSize() {
    this.groupSize = Math.min(this.decisionData.decisionStatement.values.filter(val => val.val != null).length, 5);
  }

  sortValues() {
    this.decisionData.decisionStatement.sortValues();
    this.updateGroupSize();
  }
}
