import { Directive, Injector, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { DECISION_STATEMENT_STEPS, DecisionData, NaviSubStep } from '@entscheidungsnavi/decision-data';
import { EducationalNavigationService } from '../../shared/navigation/educational-navigation.service';
import { HelpMenu, HelpMenuProvider } from '../../../app/help/help';
import { Navigation, NavLine, navLineElement } from '../../shared/navline';
import { DecisionDataState } from '../../shared/decision-data-state';

@Directive()
// eslint-disable-next-line @angular-eslint/directive-class-suffix
export abstract class AbstractDecisionStatementHint implements OnInit, Navigation, HelpMenuProvider {
  navLine = new NavLine({
    left: [
      navLineElement()
        .back(() => (this.page === 1 ? '/decisionstatement/steps/1' : `/decisionstatement/steps/${this.page - 1}`))
        .disabled(() => this.page === 1)
        .build(),
    ],
    right: [
      navLineElement()
        .continue(() => `/decisionstatement/steps/${this.page + 1}`)
        .build(),
    ],
  });

  abstract helpMenu: HelpMenu;
  page: number; // 1-based
  decisionData: DecisionData;

  protected currentProgressService: EducationalNavigationService;

  protected dialog: MatDialog;

  protected decisionDataState: DecisionDataState;

  get naviSubStep(): NaviSubStep {
    return { step: 'decisionStatement', subStepIndex: this.page - 1 };
  }

  protected constructor(injector: Injector) {
    this.currentProgressService = injector.get(EducationalNavigationService);
    this.decisionData = injector.get(DecisionData);
    this.dialog = injector.get(MatDialog);
    this.decisionDataState = injector.get(DecisionDataState);
  }

  ngOnInit() {
    if (this.page > DECISION_STATEMENT_STEPS.indexOf(this.decisionData.decisionStatement.subStepProgression) + 1) {
      this.decisionData.decisionStatement.subStepProgression = DECISION_STATEMENT_STEPS[this.page - 1];
    }

    this.currentProgressService.updateProgress();
  }
}
