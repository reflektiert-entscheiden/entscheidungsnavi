import { Component, Injector, OnInit } from '@angular/core';
import { Value } from '@entscheidungsnavi/decision-data';
import { AbstractDecisionStatementHint } from '../decision-statement-hint';
import { helpPage } from '../../../../app/help/help';
import { Help1Component } from '../hint4/help1/help1.component';
import { Help2Component } from '../hint4/help2/help2.component';
import { HelpBackgroundComponent } from '../help-background/help-background.component';
import { Navigation, NavLine, navLineElement } from '../../../shared/navline';

@Component({
  templateUrl: './decision-statement-hint4.component.html',
  styleUrls: ['./decision-statement-hint4.component.scss'],
})
export class DecisionStatementHint4Component extends AbstractDecisionStatementHint implements OnInit, Navigation {
  override navLine = new NavLine({
    left: [navLineElement().back('/decisionstatement/steps/3').build()],
    right: [navLineElement().continue('/decisionstatement').build()],
  });

  importantValues: Value[];

  suggestionNotes: string[];
  hasSuggestionNotes: boolean;

  get helpMenu() {
    return [
      helpPage()
        .name($localize`So funktioniert's`)
        .component(Help1Component)
        .build(),
      helpPage()
        .name($localize`Weitere Hinweise`)
        .component(Help2Component)
        .build(),
      helpPage()
        .name($localize`Hintergrundwissen zum Schritt 1`)
        .component(HelpBackgroundComponent)
        .build(),
      helpPage()
        .name($localize`Videos`)
        .youtube(0)
        .build(),
    ];
  }

  constructor(injector: Injector) {
    super(injector);
    this.page = 4;
  }

  override ngOnInit() {
    super.ngOnInit();

    if (this.decisionData.decisionStatement.statement == null) {
      this.decisionData.decisionStatement.statement = this.decisionData.decisionStatement.statement_attempt ?? '';
    }

    if (this.decisionData.decisionStatement.preNotes.length > 0 && this.decisionData.decisionStatement.preNotes_2.length === 0) {
      this.decisionData.decisionStatement.preNotes_2 = this.decisionData.decisionStatement.preNotes.filter(note => note.length > 0);
    }

    if (this.decisionData.decisionStatement.afterNotes.length > 0 && this.decisionData.decisionStatement.afterNotes_2.length === 0) {
      this.decisionData.decisionStatement.afterNotes_2 = this.decisionData.decisionStatement.afterNotes.filter(note => note.length > 0);
    }

    this.importantValues = this.decisionData.decisionStatement.getImportantValues();

    this.suggestionNotes = this.decisionData.decisionStatement.notes;
    this.hasSuggestionNotes = this.suggestionNotes.some(n => n);
  }

  track(index: number) {
    return index;
  }

  addNote(index: number, notes: 'pre' | 'after') {
    this.getArray(notes).splice(index, 0, '');
  }

  removeNote(index: number, notes: 'pre' | 'after') {
    this.getArray(notes).splice(index, 1);
  }

  getArray(name: 'pre' | 'after') {
    return name === 'pre' ? this.decisionData.decisionStatement.preNotes : this.decisionData.decisionStatement.afterNotes;
  }
}
