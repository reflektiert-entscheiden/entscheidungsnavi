import { Component, Injector } from '@angular/core';
import { AbstractDecisionStatementHint } from '../decision-statement-hint';
import { helpPage } from '../../../../app/help/help';
import { Help1Component } from '../hint2/help1/help1.component';
import { HelpBackgroundComponent } from '../help-background/help-background.component';

@Component({
  templateUrl: './decision-statement-hint2.component.html',
  styleUrls: ['./decision-statement-hint2.component.scss'],
})
export class DecisionStatementHint2Component extends AbstractDecisionStatementHint {
  get helpMenu() {
    return [
      helpPage()
        .name($localize`So funktioniert's`)
        .component(Help1Component)
        .build(),
      helpPage()
        .name($localize`Hintergrundwissen zum Schritt 1`)
        .component(HelpBackgroundComponent)
        .build(),
      helpPage()
        .name($localize`Videos`)
        .youtube(0)
        .build(),
    ];
  }

  constructor(injector: Injector) {
    super(injector);
    this.page = 2;
  }
}
