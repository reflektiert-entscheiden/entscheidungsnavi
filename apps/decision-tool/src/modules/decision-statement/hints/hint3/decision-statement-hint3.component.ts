import { Component, Injector, OnInit, ViewChild } from '@angular/core';
import { MatTabGroup } from '@angular/material/tabs';
import { AbstractDecisionStatementHint } from '../decision-statement-hint';
import { helpPage } from '../../../../app/help/help';
import { HelpBackgroundComponent } from '../help-background/help-background.component';
import { NavLine, navLineElement } from '../../../shared/navline';
import { Help1Component } from './help1/help1.component';

@Component({
  templateUrl: './decision-statement-hint3.component.html',
  styleUrls: ['./decision-statement-hint3.component.scss'],
})
export class DecisionStatementHint3Component extends AbstractDecisionStatementHint implements OnInit {
  override navLine = new NavLine({
    left: [
      ...navLineElement()
        .back('/decisionstatement/steps/2')
        .condition(() => this.suggestionTabs.selectedIndex === 0)
        .orElse(builder =>
          builder
            .back()
            .label($localize`Vorherige Frage`)
            .onClick(() => {
              this.suggestionTabs.selectedIndex = this.suggestionTabs.selectedIndex - 1;
            }),
        ),
    ],
    right: [
      ...navLineElement()
        .continue('/decisionstatement/steps/4')
        .condition(() => this.suggestionTabs.selectedIndex === this.suggestionTabs._tabs.length - 1)
        .orElse(builder =>
          builder
            .continue()
            .label($localize`Nächste Frage`)
            .onClick(() => {
              this.suggestionTabs.selectedIndex = this.suggestionTabs.selectedIndex + 1;
            }),
        ),
    ],
  });

  notes: string[];

  @ViewChild('suggestionTabs', { static: true })
  suggestionTabs: MatTabGroup;

  get helpMenu() {
    return [
      helpPage()
        .name($localize`So funktioniert's`)
        .component(Help1Component)
        .build(),
      helpPage()
        .name($localize`Hintergrundwissen zum Schritt 1`)
        .component(HelpBackgroundComponent)
        .build(),
      helpPage()
        .name($localize`Videos`)
        .youtube(0)
        .build(),
    ];
  }

  constructor(injector: Injector) {
    super(injector);
    this.page = 3;
  }

  override ngOnInit() {
    super.ngOnInit();

    this.notes = this.decisionData.decisionStatement.notes;
    // Initialize all notes so that the progress calculation works properly
    this.notes.forEach((value, i) => {
      this.notes[i] = value ?? '';
    });
  }
}
