import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { HoverPopOverDirective, OverflowDirective, PlainTextPasteDirective, WidthTriggerDirective } from '@entscheidungsnavi/widgets';
import { SharedModule } from '../shared/shared.module';
import { NoteHoverComponent } from '../shared/note-hover/note-hover.component';
import { DecisionStatementRoutingModule } from './decision-statement-routing.module';
import { DecisionStatementHint1Component } from './hints/hint1/decision-statement-hint1.component';
import { DecisionStatementHint2Component } from './hints/hint2/decision-statement-hint2.component';
import { DecisionStatementHint3Component } from './hints/hint3/decision-statement-hint3.component';
import { DecisionStatementComponent } from './main/decision-statement.component';
import { DecisionStatementHint4Component } from './hints/hint4/decision-statement-hint4.component';
import { DecisionStatementNoteListComponent } from './shared/note-list/decision-statement-note-list.component';
import { DecisionStatementBoxComponent } from './shared/decision-statement-box/decision-statement-box.component';
import { HelpBackgroundComponent } from './hints/help-background/help-background.component';
import { Help1Component as Hint1Help1Component } from './hints/hint1/help1/help1.component';
import { Help2Component as Hint1Help2Component } from './hints/hint1/help2/help2.component';
import { Help2Component as Hint4Help2Component } from './hints/hint4/help2/help2.component';
import { FundamentalValuesComponent } from './fundamental-values/fundamental-values.component';

@NgModule({
  declarations: [
    DecisionStatementComponent,
    DecisionStatementNoteListComponent,
    DecisionStatementHint1Component,
    DecisionStatementHint2Component,
    DecisionStatementHint3Component,
    DecisionStatementHint4Component,
    DecisionStatementBoxComponent,
    HelpBackgroundComponent,
    Hint1Help1Component,
    Hint1Help2Component,
    Hint4Help2Component,
  ],
  imports: [
    CommonModule,
    DecisionStatementRoutingModule,
    SharedModule,
    HoverPopOverDirective,
    NoteHoverComponent,
    OverflowDirective,
    WidthTriggerDirective,
    PlainTextPasteDirective,
    FundamentalValuesComponent,
  ],
  exports: [DecisionStatementBoxComponent, DecisionStatementComponent],
})
export class DecisionStatementModule {}
