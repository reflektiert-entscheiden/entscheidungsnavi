import {
  InfluenceFactorStateMap,
  PREDEFINED_INFLUENCE_FACTORS,
  randomizedUtilityGenerator,
  UtilityGeneratorResult,
} from '@entscheidungsnavi/decision-data';
import { readText } from '@entscheidungsnavi/decision-data/export';
import { range, times } from 'lodash';
import { ROBUSTNESS_CHECK_HISTOGRAM_BIN_COUNT, StartMessage, UpdateMessage } from './robustness-check-messages';

const STEP_DURATION = 500; // the time length of one step in ms

addEventListener('message', (e: MessageEvent<StartMessage>) => {
  const decisionData = readText(e.data.decisionData);
  const selectedOutcomes = decisionData.outcomes.filter((_val, idx) => e.data.selectedAlternatives.includes(idx));

  const generator = randomizedUtilityGenerator(
    decisionData.objectives,
    selectedOutcomes,
    decisionData.weights.getWeights(),
    e.data.parameters,
    e.data.selectedAlternatives,
  );

  // Result arrays
  const sum: number[] = new Array(selectedOutcomes.length);
  const min: number[] = new Array(selectedOutcomes.length);
  const max: number[] = new Array(selectedOutcomes.length);
  const utilityHistograms: number[][] = selectedOutcomes.map(() => new Array(ROBUSTNESS_CHECK_HISTOGRAM_BIN_COUNT));
  // matrix with count of positions for each alternative [alternative][positionCount]
  const positions: number[][] = range(selectedOutcomes.length).map(() => new Array(selectedOutcomes.length));
  // we count how often each influence factor state occurred for each position [alternative][positionCount]
  const alternativeAtPositionInfos = selectedOutcomes.map(() =>
    selectedOutcomes.map(() => ({
      drawnCs: e.data.parameters.utilityFunctions ? times(decisionData.objectives.length, () => null) : [],
      drawnWeights: e.data.parameters.objectiveWeights ? times(decisionData.objectives.length, () => null) : [],
      stateCounts: new InfluenceFactorStateMap<number[]>(),
    })),
  );

  // eslint-disable-next-line no-constant-condition
  while (true) {
    // Reset all our values
    sum.fill(0);
    min.fill(Number.MAX_SAFE_INTEGER);
    max.fill(Number.MIN_SAFE_INTEGER);
    for (const histogram of utilityHistograms) {
      histogram.fill(0);
    }
    for (const positionsForAlternative of positions) {
      positionsForAlternative.fill(0);
    }
    for (const i of alternativeAtPositionInfos) {
      for (const j of i) {
        for (const [, array] of j.stateCounts.entries()) {
          array.fill(0);
        }
      }
    }

    // Do all our iterations for this step
    const startTime = performance.now();
    let iterationCount = 0;
    while (performance.now() - startTime < STEP_DURATION) {
      // We know that this generator never terminates
      const currResult = generator.next().value as UtilityGeneratorResult;
      const currUtilities = currResult.alternativeUtilities;

      for (let alternativeID = 0; alternativeID < sum.length; alternativeID++) {
        sum[alternativeID] += currUtilities[alternativeID];
        min[alternativeID] = Math.min(min[alternativeID], currUtilities[alternativeID]);
        max[alternativeID] = Math.max(max[alternativeID], currUtilities[alternativeID]);

        const histogramBinIndex = Math.min(
          Math.floor(currUtilities[alternativeID] * ROBUSTNESS_CHECK_HISTOGRAM_BIN_COUNT),
          ROBUSTNESS_CHECK_HISTOGRAM_BIN_COUNT - 1,
        );
        utilityHistograms[alternativeID][histogramBinIndex]++;
      }

      // Alternative IDs sorted by their utility
      const sortedNews = currUtilities.map((val: number, idx: number) => ({ val, idx })).sort((a, b) => b.val - a.val);

      sortedNews.forEach((alternative, rankIdx) => {
        positions[alternative.idx][rankIdx] += 1;

        // Increment the respective influence factor states
        const alternativeAtPositionInfo = alternativeAtPositionInfos[alternative.idx][rankIdx];
        for (const [key, state] of currResult.influenceFactorStates.entries()) {
          let countArray = alternativeAtPositionInfo.stateCounts.get(key);

          if (countArray == null) {
            // Initialize the array if we have not done so yet
            countArray = new Array(
              typeof key === 'number' ? decisionData.influenceFactors[key].stateCount : PREDEFINED_INFLUENCE_FACTORS[key.id].stateCount,
            ).fill(0);
            alternativeAtPositionInfo.stateCounts.set(key, countArray);
          }

          countArray[state]++;
        }

        // weights per alternative and position
        if (e.data.parameters.objectiveWeights) {
          for (let objectiveIdx = 0; objectiveIdx < currResult.drawnWeights.length; objectiveIdx++) {
            alternativeAtPositionInfo.drawnWeights[objectiveIdx] = {
              min: Math.min(
                alternativeAtPositionInfo.drawnWeights[objectiveIdx]?.min ?? Number.MAX_SAFE_INTEGER,
                currResult.drawnWeights[objectiveIdx],
              ),
              max: Math.max(
                alternativeAtPositionInfo.drawnWeights[objectiveIdx]?.max ?? Number.MIN_SAFE_INTEGER,
                currResult.drawnWeights[objectiveIdx],
              ),
            };
          }
        }

        // C (utility function parameter) per alternative and position
        if (e.data.parameters.utilityFunctions) {
          for (let objectiveIdx = 0; objectiveIdx < currResult.drawnCs.length; objectiveIdx++) {
            if (!decisionData.objectives[objectiveIdx].isVerbal) {
              alternativeAtPositionInfo.drawnCs[objectiveIdx] = {
                min: Math.min(
                  alternativeAtPositionInfo.drawnCs[objectiveIdx]?.min ?? Number.MAX_SAFE_INTEGER,
                  currResult.drawnCs[objectiveIdx],
                ),
                max: Math.max(
                  alternativeAtPositionInfo.drawnCs[objectiveIdx]?.max ?? Number.MIN_SAFE_INTEGER,
                  currResult.drawnCs[objectiveIdx],
                ),
              };
            }
          }
        }
      });

      iterationCount++;
    }

    // Report the results for this step
    const updateMessage: UpdateMessage = {
      iterationCount,
      result: {
        sum,
        min,
        max,
        positions,
        alternativeAtPositionInfos: alternativeAtPositionInfos.map(outer =>
          outer.map(inner => ({
            drawnCs: inner.drawnCs,
            drawnWeights: inner.drawnWeights,
            stateCounts: inner.stateCounts.innerMap(),
          })),
        ),
        histograms: utilityHistograms,
      },
    };
    postMessage(updateMessage);
  }
});
