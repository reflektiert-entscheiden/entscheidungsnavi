// This is separated into a file to make it mockable in unit tests. `import.meta.url` does not work in Jest.
// https://github.com/nrwl/nx/issues/5697
export function createRobustnessWorker() {
  return new Worker(new URL('./robustness-check.worker', import.meta.url), {
    type: 'module',
    name: 'RobustnessCheckWorker',
  });
}
