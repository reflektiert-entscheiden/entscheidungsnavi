export type OWACalculationMode = 'combinations' | 'random';

export interface StartMessageOWA {
  calculationMode: OWACalculationMode;
  numberOfObjectives: number;
  possibleValues?: number[];
  prefixValuesPerWorker?: number[];
  numberOfAlternatives: number;
  utilityMatrix: number[][];
}

export interface UpdateMessageOWA {
  combinationsComputed: number;
  histogramData: number[][][];
  bestAlternativeCounters: number[];
}
