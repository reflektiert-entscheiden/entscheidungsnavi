import { Injectable, NgZone, OnDestroy } from '@angular/core';
import { Subject } from 'rxjs';
import { cloneDeep, range, sum } from 'lodash';
import { DecisionData, getUtilityMatrixMeta, OWA_HISTOGRAM_BIN_COUNT } from '@entscheidungsnavi/decision-data';
import { OWACalculationMode, StartMessageOWA, UpdateMessageOWA } from './objective-weights-analysis-messages';

export type OWADataPoint = {
  p10: number;
  p25: number;
  p50: number;
  p75: number;
  p90: number;
  min: number;
  max: number;
  avg: number;
};

export interface OWAResult {
  combinationsComputed: number;
  bestAlternativeCounters: number[]; // [alternativeIdx], tracks in how many combinations the respective alternative was the best
  diagramData: OWADataPoint[][];
}

@Injectable()
export class ObjectiveWeightsAnalysisService implements OnDestroy {
  private calculationMode: OWACalculationMode = 'combinations';
  private numberOfFixedCombinations: number;
  private workerPool: Worker[] = [];

  private readonly histogramData: number[][][];
  private readonly results: OWAResult;
  private _noWorker = false;
  private readonly precision = 0.005;
  private endlessMode: boolean;

  public isRandomComputationDone = false;
  /**
   * True iff the browser does not support web workers.
   */
  get noWorker() {
    return this._noWorker;
  }

  get workerCount() {
    // Limit us half the systems logical cores
    return Math.max(Math.round(navigator.hardwareConcurrency / 2), 1);
  }

  /**
   * Emits the latest results from the robustness workers. Emits outside the Angular Zone.
   */
  readonly onUpdate$ = new Subject<void>();

  constructor(
    private zone: NgZone,
    private decisionData: DecisionData,
  ) {
    if (!Worker) this._noWorker = true;

    this.results = {
      combinationsComputed: 0,
      diagramData: null,
      bestAlternativeCounters: Array.from({ length: decisionData.alternatives.length }, () => 0),
    };
    this.histogramData = Array.from({ length: this.decisionData.alternatives.length }, () =>
      Array.from({ length: this.decisionData.objectives.length }, () => Array.from({ length: OWA_HISTOGRAM_BIN_COUNT }, () => 0)),
    );
  }

  ngOnDestroy() {
    this.destroyWorkers();
  }

  isFixedComputationDone() {
    return this.results.combinationsComputed >= this.numberOfFixedCombinations;
  }

  private destroyWorkers() {
    this.workerPool.forEach(worker => {
      worker.removeAllListeners('message');
      worker.removeAllListeners('error');
      worker.terminate();
    });
    this.workerPool = [];
  }

  /**
   * Start a new worker calculation using the given data.
   *
   * @returns true if the calculation could be started, false otherwise
   */
  startWorker(endlessMode: boolean) {
    this.endlessMode = endlessMode;
    this.destroyWorkers();

    /* Set up data for worker */
    // we need a copy of the utility matrix, which we pass to the worker
    const utilityMatrix = getUtilityMatrixMeta(this.decisionData.outcomes, this.decisionData.objectives);

    let workersToUse = this.workerCount;
    let possibleValues: number[], prefixValues: number[][];

    if (this.calculationMode === 'combinations') {
      // determines the number of steps completed per objective (used when computing combinations)
      // e.g. 3 -> [0, 0.5, 1]; 5 -> [0, 0.25, 0.5, 0.75, 1], etc
      const stepsPerNumberOfObjectives = this.stepsPerNumberOfObjectives(this.decisionData.objectives.length);
      // equidistant steps based between 0 and 1 based on stepsPerNumberOfObjectives
      possibleValues = range(stepsPerNumberOfObjectives).map(
        x => x / (this.stepsPerNumberOfObjectives(this.decisionData.objectives.length) - 1),
      );
      this.numberOfFixedCombinations = Math.pow(possibleValues.length, this.decisionData.objectives.length);

      // prefixValues are explained inside the worker
      prefixValues = Array.from({ length: this.workerCount }, () => []);
      possibleValues.forEach((possibleValue, idx) => prefixValues[idx % this.workerCount].push(possibleValue));

      workersToUse = prefixValues.length; // divide prefixes between workers
    }

    for (let workerIdx = 0; workerIdx < workersToUse; workerIdx++) {
      const newWorker = new Worker(new URL('./objective-weights-analysis.worker', import.meta.url), {
        type: 'module',
        name: this.constructor.name,
      });

      this.zone.runOutsideAngular(() => {
        newWorker.addEventListener('message', e => this.onWorkerMessage(e));
        newWorker.addEventListener('error', e => console.error(e));
      });

      newWorker.postMessage({
        calculationMode: this.calculationMode,
        numberOfObjectives: this.decisionData.objectives.length,
        possibleValues,
        prefixValuesPerWorker: prefixValues?.[workerIdx],
        numberOfAlternatives: this.decisionData.alternatives.length,
        utilityMatrix: utilityMatrix,
      } satisfies StartMessageOWA);

      this.workerPool.push(newWorker);
    }
  }

  pauseWorker() {
    this.destroyWorkers();
  }

  // predefined number of iterations based on the number of objectives
  private stepsPerNumberOfObjectives(objectivesCount: number): number {
    if (objectivesCount < 2) throw new Error('Less than 2 objectives.');
    switch (objectivesCount) {
      case 2:
        return 1000;
      case 3:
        return 100;
      case 4:
        return 40;
      case 5:
        return 18;
      case 6:
        return 11;
      case 7:
        return 8;
      case 8:
        return 6;
      case 9:
        return 5;
      case 10:
        return 4;
      case 11:
        return 4;
      default:
        if (objectivesCount <= 14) return 3;
        else return 2;
    }
  }

  /**
   * This function runs outside the angular zone.
   */
  private onWorkerMessage(e: MessageEvent<UpdateMessageOWA>) {
    // increase overall combinationsComputed
    this.results.combinationsComputed += e.data.combinationsComputed;
    this.results.bestAlternativeCounters.forEach(
      (_, alternativeIdx) => (this.results.bestAlternativeCounters[alternativeIdx] += e.data.bestAlternativeCounters[alternativeIdx]),
    );
    // Aggregate the workers results into our current intermediate result
    e.data.histogramData.forEach((utilitiesPerAlternative, alternativeIdx) => {
      utilitiesPerAlternative.forEach((utilitiesPerObjective, objectiveIdx) => {
        utilitiesPerObjective.forEach((histogramValue, histogramIdx) => {
          this.histogramData[alternativeIdx][objectiveIdx][histogramIdx] += histogramValue;
        });
      });
    });

    let precisionReached = true;
    // compute the diagram data from the current histogram on every worker update
    this.results.diagramData = this.histogramData.map((alternativeHistogramData, alternativeIdx) =>
      alternativeHistogramData.map((histogramValues, objectiveIdx) => {
        const newDataPoint = {
          p10: this.getHistogramQuantile(histogramValues, 0.1),
          p25: this.getHistogramQuantile(histogramValues, 0.25),
          p50: this.getHistogramQuantile(histogramValues, 0.5),
          p75: this.getHistogramQuantile(histogramValues, 0.75),
          p90: this.getHistogramQuantile(histogramValues, 0.9),
          min: this.getHistogramMin(histogramValues),
          max: this.getHistogramMax(histogramValues),
          avg: this.getHistogramMean(histogramValues),
        };

        if (this.calculationMode === 'random') {
          // check if precision is reached
          if (this.results.diagramData == null || this.results.diagramData[alternativeIdx][objectiveIdx] == null) {
            precisionReached = false;
          } else if (precisionReached) {
            // don't check if precisionReached == true already
            for (const prop in newDataPoint) {
              const currentPrecision = Math.abs(
                this.results.diagramData[alternativeIdx][objectiveIdx][prop as keyof OWADataPoint] -
                  newDataPoint[prop as keyof OWADataPoint],
              );
              if (currentPrecision > this.precision) {
                precisionReached = false;
              }
            }
          }
        }

        return newDataPoint;
      }),
    );

    if (precisionReached && this.calculationMode === 'random') {
      this.isRandomComputationDone = true;
      if (!this.endlessMode) {
        // only stop computation if user hasn't manually resumed the calculation
        this.destroyWorkers();
      }
    }

    if (this.isFixedComputationDone() && this.calculationMode === 'combinations') {
      // combinations have been computed, start the random algorithm
      this.calculationMode = 'random';
      this.startWorker(false);
    }

    this.onUpdate$.next();
  }

  private getHistogramMin(histogram: number[]) {
    for (let binIdx = 0; binIdx < histogram.length; binIdx++) {
      if (histogram[binIdx] > 0) return binIdx / (OWA_HISTOGRAM_BIN_COUNT - 1);
    }
    return undefined;
  }

  private getHistogramMax(histogram: number[]) {
    for (let binIdx = histogram.length - 1; binIdx >= 0; binIdx--) {
      if (histogram[binIdx] > 0) return binIdx / (OWA_HISTOGRAM_BIN_COUNT - 1);
    }
    return undefined;
  }

  private getHistogramMean(histogram: number[]) {
    let sumTotal = 0,
      sumBins = 0;
    for (let binIdx = 0; binIdx < histogram.length; binIdx++) {
      sumTotal += binIdx * histogram[binIdx];
      sumBins += histogram[binIdx];
    }
    return sumTotal / (sumBins * (OWA_HISTOGRAM_BIN_COUNT - 1));
  }

  private getHistogramQuantile(histogram: number[], quantile: number) {
    const numberOfDataPoints = sum(histogram);
    let stopValue = quantile * numberOfDataPoints;
    let isPrecise = true;
    if (stopValue % 1 !== 0) {
      // quantile lies between bins
      isPrecise = false;
      stopValue = Math.floor(stopValue);
    }

    let currentSum = 0;
    for (let binIdx = 0; binIdx < histogram.length; binIdx++) {
      currentSum += histogram[binIdx];
      if (currentSum >= stopValue) {
        if (isPrecise) {
          return binIdx / (OWA_HISTOGRAM_BIN_COUNT - 1);
        } else {
          // return the avg of the current binIdx and the next (non-empty) binIdx
          return (binIdx + this.getNextBinIdx(histogram, binIdx)) / (2 * (OWA_HISTOGRAM_BIN_COUNT - 1));
        }
      }
    }
  }

  private getNextBinIdx(histogram: number[], currentBinIdx: number) {
    for (let nextBinIdx = currentBinIdx + 1; nextBinIdx < histogram.length; nextBinIdx++) {
      if (histogram[currentBinIdx] > 0) return nextBinIdx;
    }
    return histogram.length - 1;
  }

  getCurrentResult(): OWAResult {
    return cloneDeep(this.results);
  }
}
