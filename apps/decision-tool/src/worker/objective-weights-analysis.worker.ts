import { WeightsAnalysis } from '@entscheidungsnavi/decision-data';
import { StartMessageOWA } from './objective-weights-analysis-messages';

const STEP_DURATION = 500; // the time length of one step in ms
const ITERATIONS_BETWEEN_CHECKS = 10_000; // we check the time on every X iterations to increase performance

addEventListener('message', (e: MessageEvent<StartMessageOWA>) => {
  let startTime = performance.now();

  // begin generating combinations
  const wA = new WeightsAnalysis(
    e.data.numberOfObjectives,
    e.data.numberOfAlternatives,
    e.data.prefixValuesPerWorker,
    e.data.possibleValues,
    e.data.utilityMatrix,
  );
  if (e.data.calculationMode === 'combinations') {
    const fixedCombinationsGen = wA.generateFixedCombinations();
    let iterationCounter = 0;
    while (fixedCombinationsGen.next().value != null) {
      if (++iterationCounter % ITERATIONS_BETWEEN_CHECKS === 0 && performance.now() - startTime >= STEP_DURATION) {
        // emit values every STEP_DURATION ms
        postMessage(wA.getResult());
        // reset time, combinations counter and histogram (they are accounted for in the service on each postMessage)
        startTime = performance.now();
        wA.resetResult();
      }
    }
  } else {
    // iteratively compute RANDOM combinations and calculate the utility value for each combination
    const randomGen = wA.generateRandomCombinations();
    let iterationCounter = 0;
    // eslint-disable-next-line no-constant-condition
    while (true) {
      randomGen.next();
      if (++iterationCounter % ITERATIONS_BETWEEN_CHECKS === 0 && performance.now() - startTime >= STEP_DURATION) {
        // emit values every STEP_DURATION ms
        postMessage(wA.getResult());
        // reset time, combinations counter and histogram (they are accounted for in the service on each postMessage)
        startTime = performance.now();
        wA.resetResult();
      }
    }
  }

  // emit after recursion is done
  postMessage(wA.getResult());
});
