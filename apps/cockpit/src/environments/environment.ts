// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

import { createEnvironment } from './environment-types';

export const ENVIRONMENT = createEnvironment({
  type: 'development',
  decisionToolOrigin: 'http://localhost:4200',
});
