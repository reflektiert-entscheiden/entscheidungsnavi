import { enableProdMode } from '@angular/core';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import * as Sentry from '@sentry/angular';
import { CockpitModule } from './app/cockpit.module';
import { ENVIRONMENT } from './environments/environment';

Sentry.init({
  dsn: ENVIRONMENT.sentryDsn,
  tunnel: '/tunnel/polaris',
  release: ENVIRONMENT.version,
  environment: ENVIRONMENT.type,
  ignoreErrors: ['ResizeObserver loop limit exceeded', 'ResizeObserver loop completed with undelivered notifications.'],
  integrations: [Sentry.browserTracingIntegration()],
  tracesSampleRate: 1.0,
});

if (ENVIRONMENT.production) {
  enableProdMode();
}

platformBrowserDynamic()
  .bootstrapModule(CockpitModule)
  .catch(err => console.error(err));
