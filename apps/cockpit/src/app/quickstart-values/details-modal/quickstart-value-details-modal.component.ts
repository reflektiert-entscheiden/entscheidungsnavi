import { Component, Inject } from '@angular/core';
import { NonNullableFormBuilder, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialog, MatDialogRef } from '@angular/material/dialog';
import { QuickstartService, QuickstartValueDto } from '@entscheidungsnavi/api-client';
import { ConfirmModalComponent } from '@entscheidungsnavi/widgets';
import { filter, switchMap, tap } from 'rxjs';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  templateUrl: './quickstart-value-details-modal.component.html',
  styleUrls: ['./quickstart-value-details-modal.component.scss'],
})
export class QuickstartValueDetailsModalComponent {
  formGroup = this.fb.group({
    nameDe: [this.value?.name.de ?? '', Validators.required],
    nameEn: [this.value?.name.en ?? '', Validators.required],
  });

  constructor(
    private dialogRef: MatDialogRef<QuickstartValueDetailsModalComponent>,
    @Inject(MAT_DIALOG_DATA) protected value: QuickstartValueDto,
    private quickstartService: QuickstartService,
    private fb: NonNullableFormBuilder,
    private dialog: MatDialog,
    private snackBar: MatSnackBar,
  ) {}

  save() {
    if (this.formGroup.invalid) {
      return;
    }

    this.formGroup.disable({ emitEvent: false });
    const name = { en: this.formGroup.value.nameEn, de: this.formGroup.value.nameDe };

    (this.value != null
      ? this.quickstartService.updateValue(this.value.id, { name })
      : this.quickstartService.addValue({ name })
    ).subscribe({
      next: value => this.dialogRef.close(value),
      error: () => {
        this.formGroup.enable({ emitEvent: false });
        this.snackBar.open($localize`Fehler beim Speichern`, 'Ok');
      },
    });
  }

  delete() {
    this.dialog
      .open(ConfirmModalComponent, {
        data: {
          title: $localize`Wert löschen`,
          prompt: $localize`Bist Du sicher, dass Du den Wert
          "${this.formGroup.value.nameDe}"/"${this.formGroup.value.nameEn}"
          löschen willst? Dies kann nicht rückgängig gemacht werden.`,
          buttonConfirm: $localize`Ja, Wert löschen`,
          buttonDeny: $localize`Nein, abbrechen`,
        },
      })
      .afterClosed()
      .pipe(
        filter(Boolean),
        tap(() => this.formGroup.disable({ emitEvent: false })),
        switchMap(() => this.quickstartService.deleteValue(this.value.id)),
      )
      .subscribe({
        next: () => {
          this.dialogRef.close('deleted');
          this.snackBar.open($localize`Wert gelöscht`, undefined, { duration: 5000 });
        },
        error: () => {
          this.formGroup.enable({ emitEvent: false });
          this.snackBar.open($localize`Löschen fehlgeschlagen`, 'Ok');
        },
      });
  }

  close() {
    if (this.formGroup.enabled) this.dialogRef.close();
  }
}
