import { Component, OnInit } from '@angular/core';
import { Sort, SortDirection } from '@angular/material/sort';
import { QuickstartService, QuickstartValueDto, QuickstartValueWithStatsDto } from '@entscheidungsnavi/api-client';
import { filter } from 'rxjs';
import { QuickstartValueWithStats } from '@entscheidungsnavi/api-types';
import { MatDialog } from '@angular/material/dialog';
import { QuickstartValueDetailsModalComponent } from './details-modal/quickstart-value-details-modal.component';

type Value = QuickstartValueWithStats & { averageScore: number };

@Component({
  templateUrl: './quickstart-values.component.html',
  styleUrls: ['./quickstart-values.component.scss'],
})
export class QuickstartValuesComponent implements OnInit {
  readonly displayedColumns = ['nameDe', 'nameEn', 'countTracked', 'countAssigned', 'countInTop5', 'averageScore', 'createdAt'] as const;

  values: Array<Value>;
  hasError = false;

  activeSort: { active: (typeof QuickstartValuesComponent.prototype.displayedColumns)[number]; direction: SortDirection } = {
    active: 'averageScore',
    direction: 'desc',
  };

  constructor(
    private quickstartService: QuickstartService,
    private dialog: MatDialog,
  ) {}

  ngOnInit() {
    this.quickstartService.getValuesWithStats().subscribe({
      next: values => {
        this.values = values.map(value => ({
          ...value,
          averageScore: value.countTracked ? value.accumulatedScore / value.countTracked : 0,
        }));
        this.updateSort();
      },
      error: () => (this.hasError = true),
    });
  }

  private updateSort() {
    this.values.sort((a, b) => {
      let result = 0;

      switch (this.activeSort.active) {
        case 'nameDe':
          result = (a.name.de ?? '').localeCompare(b.name.de ?? '');
          break;
        case 'nameEn':
          result = a.name.en.localeCompare(b.name.en);
          break;
        case 'createdAt':
          result = a.createdAt.getTime() - b.createdAt.getTime();
          break;
        default:
          result = a[this.activeSort.active] - b[this.activeSort.active];
          break;
      }

      return this.activeSort.direction === 'asc' ? result : -result;
    });

    // Reassign to trigger CD on the table
    this.values = this.values.slice();
  }

  onSortChange(sort: Sort) {
    this.activeSort = sort as typeof this.activeSort;
    this.updateSort();
  }

  showDetails(value: Value) {
    this.dialog
      .open(QuickstartValueDetailsModalComponent, { data: value })
      .afterClosed()
      .pipe(filter(result => result))
      .subscribe((result: QuickstartValueDto | 'deleted') => {
        const index = this.values.findIndex(element => element.id === value.id);

        if (result === 'deleted') {
          this.values.splice(index, 1);
        } else {
          Object.assign(this.values[index], result);
        }

        this.updateSort();
      });
  }

  addValue() {
    this.dialog
      .open(QuickstartValueDetailsModalComponent)
      .afterClosed()
      .subscribe((newValue: QuickstartValueWithStatsDto) => {
        if (newValue) {
          this.values.push({ ...newValue, averageScore: 0 });
          this.updateSort();
        }
      });
  }
}
