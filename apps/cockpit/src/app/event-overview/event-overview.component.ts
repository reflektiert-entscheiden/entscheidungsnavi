import { Component } from '@angular/core';
import { SortDirection } from '@angular/material/sort';
import { AuthService, EventDto, EventManagementService } from '@entscheidungsnavi/api-client';
import { BehaviorSubject, catchError, combineLatest, map, Observable, of, shareReplay } from 'rxjs';

@Component({
  templateUrl: './event-overview.component.html',
  styleUrls: ['./event-overview.component.scss'],
})
export class EventOverviewComponent {
  protected displayedColumns = ['submissionStatus', 'name', 'code', 'registrationCount', 'submissionCount', 'createdAt'] as const;

  protected displayedEvents$: Observable<EventDto[]>;
  protected activeSort$ = new BehaviorSubject<{
    active: (typeof EventOverviewComponent.prototype.displayedColumns)[number];
    direction: SortDirection;
  }>({
    active: 'createdAt',
    direction: 'desc',
  });

  protected hasError = false;

  readonly canCreateEvents$ = this.authService.userHasRole('event-manager');

  constructor(
    eventService: EventManagementService,
    private authService: AuthService,
  ) {
    this.displayedEvents$ = combineLatest([eventService.events$, this.activeSort$]).pipe(
      map(([events, sort]) => {
        const displayedEvents = events.list.slice();

        displayedEvents.sort((a, b) => {
          let result = 0;

          switch (sort.active) {
            case 'submissionStatus':
              result = +(a.submissionStatus === 'open') - +(b.submissionStatus === 'open');
              break;
            case 'name':
              result = a.name.localeCompare(b.name);
              break;
            case 'code':
              result = a.code.localeCompare(b.code);
              break;
            case 'registrationCount':
              result = a.registrationCount - b.registrationCount;
              break;
            case 'submissionCount':
              result = a.submissionCount - b.submissionCount;
              break;
            case 'createdAt':
              result = a.createdAt.getTime() - b.createdAt.getTime();
              break;
          }

          return result * (sort.direction === 'asc' ? 1 : -1);
        });

        return displayedEvents;
      }),
      catchError(() => {
        this.hasError = true;
        return of();
      }),
      shareReplay(1),
    );
  }
}
