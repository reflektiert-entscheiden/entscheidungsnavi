import { Component } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';
import { AuthService, EventManagementService } from '@entscheidungsnavi/api-client';
import { OnDestroyObservable } from '@entscheidungsnavi/tools';
import { PersistentSetting, PersistentSettingParent } from '@entscheidungsnavi/widgets';
import { Observable, map, of, switchMap, takeUntil } from 'rxjs';
import { ENVIRONMENT } from '../../environments/environment';

@Component({
  templateUrl: './navigation-layout.component.html',
  styleUrls: ['./navigation-layout.component.scss'],
})
@PersistentSettingParent('cp-navigation-layout')
export class NavigationLayoutComponent {
  sidenavOver: boolean;
  year: string;

  @PersistentSetting()
  menuOpened = true;

  @OnDestroyObservable()
  private onDestroy$: Observable<void>;

  get version() {
    return ENVIRONMENT.version;
  }

  readonly hasEventAccess$ = this.authService.userHasRole('event-manager').pipe(
    switchMap(isManager => {
      if (isManager) {
        // Managers always have access
        return of(true);
      } else {
        // Non-managers have access only if they are editors of some event
        return this.eventManagement.events$.pipe(map(eventList => eventList.list.length > 0));
      }
    }),
  );

  constructor(
    router: Router,
    protected authService: AuthService,
    private eventManagement: EventManagementService,
  ) {
    this.year = new Date().getFullYear() + '';

    router.events.pipe(takeUntil(this.onDestroy$)).subscribe(event => {
      if (event instanceof NavigationEnd && this.sidenavOver) {
        this.menuOpened = false;
      }
    });

    // Return to the login page when the user is logged out
    authService.onLogout$
      .pipe(takeUntil(this.onDestroy$))
      .subscribe(() => router.navigate(['/login'], { queryParams: { redirectUrl: router.url } }));
  }

  toggleMenu() {
    this.menuOpened = !this.menuOpened;
  }
}
