import { Injectable } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { ConfirmModalComponent, ConfirmModalData } from '@entscheidungsnavi/widgets';
import { map } from 'rxjs';
import { CreateEventComponent } from './create-event/create-event.component';
import { EventDetailsComponent } from './event-details.component';

@Injectable({
  providedIn: 'root',
})
export class EventDeactivateGuard {
  constructor(private dialog: MatDialog) {}

  canDeactivate(component: EventDetailsComponent | CreateEventComponent, _route: ActivatedRouteSnapshot, _state: RouterStateSnapshot) {
    if (component.isDirty) {
      return this.dialog
        .open<ConfirmModalComponent, ConfirmModalData>(ConfirmModalComponent, {
          data: {
            title: $localize`Änderungen verwerfen`,
            prompt: $localize`Die Veranstaltung enthält ungespeicherte Änderungen. Möchtest Du diese wirklich verwerfen?`,
            template: 'discard',
          },
        })
        .afterClosed()
        .pipe(map(result => !!result));
    }

    return true;
  }
}
