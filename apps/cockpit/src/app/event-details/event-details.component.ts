import { HttpErrorResponse } from '@angular/common/http';
import { Component, DestroyRef, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute } from '@angular/router';
import { takeUntilDestroyed } from '@angular/core/rxjs-interop';
import { EventDto, EventManagementService } from '@entscheidungsnavi/api-client';
import { finalize, map, switchMap } from 'rxjs';
import { EventConfigurationFormService } from './event-configuration/event-configuration-form.service';

@Component({
  templateUrl: './event-details.component.html',
  styleUrls: ['./event-details.component.scss'],
  providers: [EventConfigurationFormService],
})
export class EventDetailsComponent implements OnInit {
  event: EventDto;

  error: 'network' | 'permissions';
  isDeleting = false;
  questionnaireDirty = false;

  savingInProgress = false;

  get isDirty() {
    return (this.eventConfiguration.form.dirty || this.questionnaireDirty) && !this.isDeleting;
  }

  constructor(
    private route: ActivatedRoute,
    protected eventManagementService: EventManagementService,
    protected eventConfiguration: EventConfigurationFormService,
    private snackBar: MatSnackBar,
    private destroyRef: DestroyRef,
  ) {}

  ngOnInit() {
    this.route.params
      .pipe(
        switchMap(params =>
          this.eventManagementService.events$.pipe(map(events => events.list.find(event => event.id === params['eventId']))),
        ),
        takeUntilDestroyed(this.destroyRef),
      )
      .subscribe({
        next: event => {
          if (event == null) {
            this.error = 'permissions';
            return;
          }

          this.event = event;

          this.resetConfiguration();
        },
        error: () => {
          this.error = 'network';
        },
      });
  }

  saveConfiguration() {
    this.savingInProgress = true;
    this.eventConfiguration.form.disable({ emitEvent: false });
    this.eventManagementService
      .updateEvent(this.event.id, this.eventConfiguration.getEventData())
      .pipe(finalize(() => (this.savingInProgress = false)))
      .subscribe({
        next: event => {
          this.eventConfiguration.form.enable();
          Object.assign(this.event, event);
          this.resetConfiguration();
        },
        error: error => {
          this.eventConfiguration.form.enable();
          if (error instanceof HttpErrorResponse && error.status === 409 && error.error.duplicateKey) {
            switch (error.error.duplicateKey) {
              case 'code':
                this.eventConfiguration.form.controls.code.setErrors({ codeConflict: true });
                break;
              case 'name':
                this.eventConfiguration.form.controls.name.setErrors({ nameConflict: true });
                break;
            }

            this.snackBar.open($localize`Die Eingabe enthält Fehler`, 'Ok');
          } else {
            this.snackBar.open($localize`Fehler beim Speichern der Veranstaltung`, 'Ok');
          }

          console.error(error);
        },
      });
  }

  resetConfiguration() {
    this.eventConfiguration.loadFromEvent(this.event, false, !this.eventManagementService.canEditEvent(this.event));
  }
}
