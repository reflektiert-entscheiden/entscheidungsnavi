import { CdkDragDrop } from '@angular/cdk/drag-drop';
import { Component, EventEmitter, HostListener, Input, OnInit, Output, QueryList, ViewChildren, isDevMode } from '@angular/core';
import { FormArray } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { EventDto, EventManagementService } from '@entscheidungsnavi/api-client';
import { QuestionnairePage } from '@entscheidungsnavi/api-types';
import { OnDestroyObservable } from '@entscheidungsnavi/tools';
import { ConfirmModalComponent, ConfirmModalData } from '@entscheidungsnavi/widgets';
import { finalize, firstValueFrom, map, Observable, startWith, takeUntil } from 'rxjs';
import { EventQuestionnairePreviewComponent } from '../event-questionnaire-preview/event-questionnaire-preview.component';
import { EventQuestionnaireReorderComponent } from '../event-questionnaire-reorder/event-questionnaire-reorder.component';
import { EventQuestionnaireEntryComponent } from './entry/event-questionnaire-entry.component';
import { EventQuestionnaireFormService, QuestionnaireEntryForm } from './event-questionnaire-form.service';

@Component({
  selector: 'dt-event-questionnaire-editor',
  templateUrl: './event-questionnaire-editor.component.html',
  styleUrls: ['./event-questionnaire-editor.component.scss'],
  providers: [EventQuestionnaireFormService],
})
export class EventQuestionnaireEditorComponent implements OnInit {
  @OnDestroyObservable()
  private onDestroy$: Observable<void>;

  @Input() event: EventDto;

  get form() {
    return this.formService.form;
  }

  isSaving = false;
  private isDirty = false;

  @Output() dirtyChange = new EventEmitter<boolean>();

  @ViewChildren(EventQuestionnaireEntryComponent) entryComponents: QueryList<EventQuestionnaireEntryComponent>;

  constructor(
    private dialog: MatDialog,
    private snackBar: MatSnackBar,
    private eventManagementService: EventManagementService,
    protected formService: EventQuestionnaireFormService,
  ) {
    formService.form.statusChanges
      .pipe(
        startWith(null),
        map(() => this.form.dirty),
        takeUntil(this.onDestroy$),
      )
      .subscribe(dirty => {
        if (dirty !== this.isDirty) {
          this.isDirty = dirty;
          this.dirtyChange.next(dirty);
        }
      });
  }

  ngOnInit() {
    this.reset();

    if (this.event?.questionnaireReleased) {
      this.form.disable({ emitEvent: false });
    }
  }

  reset() {
    if (this.event) {
      this.formService.loadFromEvent(this.event);
    } else {
      this.formService.clear();
    }
  }

  onDropEntry(event: CdkDragDrop<number>) {
    this.form.markAsDirty();

    const previousPage = this.form.controls.questionnaire.at(event.previousContainer.data);
    const control = previousPage.at(event.previousIndex);
    previousPage.removeAt(event.previousIndex);

    this.form.controls.questionnaire.at(event.container.data).insert(event.currentIndex, control);
  }

  private buildQuestionnaire(): QuestionnairePage[] {
    return this.form.getRawValue().questionnaire.map(page => ({ entries: page }));
  }

  async save(submit = false) {
    if (this.form.invalid) {
      this.form.markAllAsTouched();
      this.snackBar.open($localize`Einige der Angaben sind ungültig`, 'Ok', { duration: 4000 });
      return;
    }

    if (submit) {
      const result = await firstValueFrom(
        this.dialog
          .open<ConfirmModalComponent, ConfirmModalData, boolean>(ConfirmModalComponent, {
            data: {
              title: $localize`Fragebogen veröffentlichen`,
              prompt: $localize`Nach dem Veröffentlichen ist der Fragebogen gesperrt.
              Er kann weder bearbeitet noch gelöscht werden.
              Dies kann nicht rückgängig gemacht werden.`,
              buttonConfirm: $localize`Ja, veröffentlichen`,
            },
          })
          .afterClosed(),
      );

      if (!result) return;
    }

    this.isSaving = true;
    this.form.disable({ emitEvent: false });

    this.eventManagementService
      .updateEvent(this.event.id, { questionnaire: this.buildQuestionnaire(), questionnaireReleased: submit || undefined })
      .pipe(finalize(() => (this.isSaving = false)))
      .subscribe({
        next: event => {
          if (!submit) {
            this.form.enable();
          }

          Object.assign(this.event, event);
          this.form.markAsPristine();
          this.form.updateValueAndValidity();
        },
        error: () => {
          this.form.enable();
          this.snackBar.open($localize`Fehler beim Speichern des Fragebogens`, 'Ok');
        },
      });
  }

  preview() {
    this.dialog.open(EventQuestionnairePreviewComponent, { data: this.buildQuestionnaire() });
  }

  reorder() {
    this.dialog
      .open<EventQuestionnaireReorderComponent, number, number[] | false>(EventQuestionnaireReorderComponent, {
        data: this.form.value.questionnaire.length,
      })
      .afterClosed()
      .subscribe(newOrder => {
        if (!newOrder) {
          return;
        }

        this.form.markAsDirty();

        const oldOrder: FormArray<QuestionnaireEntryForm>[] = [];
        while (this.form.value.questionnaire.length > 0) {
          oldOrder.push(this.form.controls.questionnaire.at(0));
          this.form.controls.questionnaire.removeAt(0);
        }

        newOrder.forEach(oldIndex => {
          this.form.controls.questionnaire.push(oldOrder[oldIndex]);
        });
      });
  }

  expandAll() {
    this.entryComponents.forEach(component => component.expand());
  }

  collapseAll() {
    this.entryComponents.forEach(component => component.collapse());
  }

  @HostListener('window:beforeunload', ['$event'])
  beforeUnloadHook(event: BeforeUnloadEvent) {
    if (this.isDirty && !isDevMode()) {
      event.returnValue = true;
    }
  }
}
