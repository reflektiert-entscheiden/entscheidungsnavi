import { Injectable, OnDestroy } from '@angular/core';
import { FormArray, FormControl, FormGroup, NonNullableFormBuilder, Validators } from '@angular/forms';
import {
  Event,
  NumberQuestionEntry,
  OptionsQuestionEntry,
  QuestionnaireEntry,
  QuestionnaireEntryType,
  TableQuestionEntry,
  TextBlockEntry,
  TextQuestionEntry,
} from '@entscheidungsnavi/api-types';
import { assertUnreachable } from '@entscheidungsnavi/tools/assert-unreachable';
import { matchParentError, regexValidator } from '@entscheidungsnavi/widgets';
import { filter, Observable, Subject, takeUntil } from 'rxjs';
import { getEmptyQuestionnaireEntry } from './questionnaire-tools';

export type EventQuestionnaireForm = typeof EventQuestionnaireFormService.prototype.form;
export type QuestionnaireEntryForm = ReturnType<typeof EventQuestionnaireFormService.prototype.createFormGroupForEntry>;
export type TextBlockForm = ReturnType<typeof EventQuestionnaireFormService.prototype.createTextBlockForm>;
export type NumberQuestionForm = ReturnType<typeof EventQuestionnaireFormService.prototype.createNumberQuestionForm>;
export type TextQuestionForm = ReturnType<typeof EventQuestionnaireFormService.prototype.createTextQuestionForm>;
export type OptionsQuestionForm = ReturnType<typeof EventQuestionnaireFormService.prototype.createOptionsQuestionForm>;
export type TableQuestionForm = ReturnType<typeof EventQuestionnaireFormService.prototype.createTableQuestionForm>;

@Injectable()
export class EventQuestionnaireFormService implements OnDestroy {
  readonly form = this.fb.group({
    questionnaire: this.fb.array<FormArray<ReturnType<typeof this.createFormGroupForEntry>>>([]),
  });

  // A subject for every form entry that emits when the entry is destroyed
  private entryDestroyObservableMap = new Map<QuestionnaireEntryForm, Subject<void>>();

  constructor(private fb: NonNullableFormBuilder) {}

  ngOnDestroy() {
    this.clear();
  }

  clear() {
    this.form.controls.questionnaire.clear();
    this.entryDestroyObservableMap.forEach(subject$ => {
      subject$.next();
      subject$.complete();
    });
    this.entryDestroyObservableMap.clear();
  }

  loadFromEvent(event: Event) {
    this.clear();

    event.questionnaire.forEach(page => {
      this.form.controls.questionnaire.push(this.createFormArrayForPage(page.entries));
    });

    this.form.markAsPristine();
    this.form.updateValueAndValidity();
  }

  addPage() {
    this.form.markAsDirty();
    this.form.controls.questionnaire.push(this.createFormArrayForPage([]));
  }

  private createFormArrayForPage(entries: QuestionnaireEntry[]) {
    return new FormArray(
      entries.map(entry => this.createFormGroupForEntry(entry)),
      Validators.required,
    );
  }

  deletePage(pageIndex: number) {
    this.form.markAsDirty();
    this.form.controls.questionnaire.at(pageIndex).controls.forEach(entryControl => this.onEntryGroupRemoved(entryControl));
    this.form.controls.questionnaire.removeAt(pageIndex);
  }

  addEntry(pageIndex: number) {
    this.form.markAsDirty();
    this.form.controls.questionnaire.at(pageIndex).push(this.createFormGroupForEntry(getEmptyQuestionnaireEntry('textBlock')));
  }

  deleteEntry(pageIndex: number, entryIndex: number) {
    this.form.markAsDirty();
    this.onEntryGroupRemoved(this.form.controls.questionnaire.at(pageIndex).at(entryIndex));
    this.form.controls.questionnaire.at(pageIndex).removeAt(entryIndex);
  }

  changeEntryType(pageIndex: number, entryIndex: number, newType: QuestionnaireEntryType) {
    this.form.markAsDirty();
    this.onEntryGroupRemoved(this.form.controls.questionnaire.at(pageIndex).at(entryIndex));
    this.form.controls.questionnaire
      .at(pageIndex)
      .setControl(entryIndex, this.createFormGroupForEntry(getEmptyQuestionnaireEntry(newType)));
  }

  private onEntryGroupRemoved(formGroup: QuestionnaireEntryForm) {
    const subject$ = this.entryDestroyObservableMap.get(formGroup);
    if (subject$) {
      subject$.next();
      subject$.complete();
      this.entryDestroyObservableMap.delete(formGroup);
    }
  }

  createFormGroupForEntry(entry: QuestionnaireEntry) {
    switch (entry.entryType) {
      case 'textBlock':
        return this.createTextBlockForm(entry);
      case 'numberQuestion': {
        const destroySubject$ = new Subject<void>();
        const formGroup = this.createNumberQuestionForm(entry, destroySubject$.asObservable());
        this.entryDestroyObservableMap.set(formGroup, destroySubject$);
        return formGroup;
      }
      case 'textQuestion': {
        const destroySubject$ = new Subject<void>();
        const formGroup = this.createTextQuestionForm(entry, destroySubject$);
        this.entryDestroyObservableMap.set(formGroup, destroySubject$);
        return formGroup;
      }
      case 'optionsQuestion':
        return this.createOptionsQuestionForm(entry);
      case 'tableQuestion':
        return this.createTableQuestionForm(entry);
      default:
        assertUnreachable(entry);
    }
  }

  createTextBlockForm(entry: TextBlockEntry) {
    return this.fb.group({
      entryType: [entry.entryType, Validators.required],
      text: [entry.text, Validators.required],
      type: [entry.type, Validators.required],
    });
  }

  createTextQuestionForm(entry: TextQuestionEntry, entryDestroyed$: Observable<void>) {
    const formGroup = this.fb.group(
      {
        entryType: [entry.entryType, Validators.required],
        question: [entry.question],
        label: [entry.label, Validators.required],
        minLength: [entry.minLength, [Validators.min(1), Validators.pattern('^[0-9]*$')]],
        maxLength: [
          entry.maxLength,
          [Validators.min(1), matchParentError('minMaxCollapsed', entryDestroyed$), Validators.pattern('^-?[0-9]+$')],
        ],
        pattern: [entry.pattern, regexValidator],
      },
      {
        validators: (group: FormGroup<{ minLength: FormControl<number>; maxLength: FormControl<number> }>) =>
          group.value.minLength != null && group.value.maxLength != null && group.value.minLength > group.value.maxLength
            ? { minMaxCollapsed: true }
            : null,
      },
    );
    // Reset an empty pattern to null
    formGroup.controls.pattern.valueChanges
      .pipe(
        filter(newValue => newValue === ''),
        takeUntil(entryDestroyed$),
      )
      .subscribe(() => formGroup.controls.pattern.setValue(null));
    return formGroup;
  }

  createNumberQuestionForm(entry: NumberQuestionEntry, entryDestroyed$: Observable<void>) {
    return this.fb.group(
      {
        entryType: [entry.entryType, Validators.required],
        question: [entry.question],
        label: [entry.label, Validators.required],
        min: [entry.min],
        max: [entry.max, matchParentError('minMaxCollapsed', entryDestroyed$)],
        step: [entry.step],
      },
      {
        validators: (group: FormGroup<{ min: FormControl<number>; max: FormControl<number> }>) =>
          group.value.min != null && group.value.max != null && group.value.min > group.value.max ? { minMaxCollapsed: true } : null,
      },
    );
  }

  createOptionsQuestionForm(entry: OptionsQuestionEntry) {
    return this.fb.group({
      entryType: [entry.entryType, Validators.required],
      question: [entry.question],
      label: [entry.label],
      displayType: [entry.displayType, Validators.required],
      options: new FormArray(
        entry.options.map(option => this.createOptionControl(option)),
        Validators.required,
      ),
    });
  }

  createTableQuestionForm(entry: TableQuestionEntry) {
    return this.fb.group({
      entryType: [entry.entryType, Validators.required],
      baseQuestion: [entry.baseQuestion, Validators.required],
      subQuestions: new FormArray(
        entry.subQuestions.map(subQuestion => this.createSubQuestionControl(subQuestion)),
        Validators.required,
      ),
      options: new FormArray(
        entry.options.map(option => this.createOptionControl(option)),
        Validators.required,
      ),
    });
  }

  createOptionControl(option: string) {
    return this.fb.control(option, Validators.required);
  }

  createSubQuestionControl(subQuestion: string) {
    return this.fb.control(subQuestion, Validators.required);
  }
}
