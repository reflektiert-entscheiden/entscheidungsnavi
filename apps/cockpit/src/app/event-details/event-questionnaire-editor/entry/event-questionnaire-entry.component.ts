import { trigger, state, style, transition, animate } from '@angular/animations';
import { Component, EventEmitter, Output } from '@angular/core';
import { ControlContainer } from '@angular/forms';
import { QuestionnaireEntryType, QUESTIONNAIRE_ENTRY_TYPES } from '@entscheidungsnavi/api-types';
import {
  EventQuestionnaireFormService,
  NumberQuestionForm,
  OptionsQuestionForm,
  QuestionnaireEntryForm,
  TableQuestionForm,
  TextQuestionForm,
} from '../event-questionnaire-form.service';

@Component({
  selector: 'dt-event-questionnaire-entry',
  templateUrl: './event-questionnaire-entry.component.html',
  styleUrls: ['./event-questionnaire-entry.component.scss'],
  animations: [
    trigger('boxExpanded', [
      state('true', style({ height: '*' })),
      state('false', style({ height: 0 })),
      transition('true <=> false', [animate('300ms ease-in-out')]),
    ]),
    trigger('expandCollapseIndicator', [
      state('false', style({ transform: 'rotate(0)' })),
      state('true', style({ transform: 'rotate(-180deg)' })),
      transition('true <=> false', [animate('300ms ease-in-out')]),
    ]),
  ],
})
export class EventQuestionnaireEntryComponent {
  @Output() delete = new EventEmitter<void>();
  @Output() changeEntryType = new EventEmitter<QuestionnaireEntryType>();

  expanded = true;

  get formGroup() {
    return this.controlContainer.control as QuestionnaireEntryForm;
  }

  readonly entryTypes = QUESTIONNAIRE_ENTRY_TYPES;

  constructor(
    private controlContainer: ControlContainer,
    private fb: EventQuestionnaireFormService,
  ) {}

  addOption() {
    this.formGroup.markAsDirty();
    (this.formGroup as OptionsQuestionForm | TableQuestionForm).controls.options.push(this.fb.createOptionControl(''));
  }

  deleteOption(optionIndex: number) {
    this.formGroup.markAsDirty();
    (this.formGroup as OptionsQuestionForm | TableQuestionForm).controls.options.removeAt(optionIndex);
  }

  addSubQuestion() {
    this.formGroup.markAsDirty();
    (this.formGroup as TableQuestionForm).controls.subQuestions.push(this.fb.createSubQuestionControl(''));
  }

  deleteSubQuestion(subQuestionIndex: number) {
    this.formGroup.markAsDirty();
    (this.formGroup as TableQuestionForm).controls.subQuestions.removeAt(subQuestionIndex);
  }

  isTextQuestionForm(formGroup: QuestionnaireEntryForm): formGroup is TextQuestionForm {
    return formGroup.value.entryType === 'textQuestion';
  }

  isNumberQuestionForm(formGroup: QuestionnaireEntryForm): formGroup is NumberQuestionForm {
    return formGroup.value.entryType === 'numberQuestion';
  }

  isOptionsQuestionForm(formGroup: QuestionnaireEntryForm): formGroup is OptionsQuestionForm {
    return formGroup.value.entryType === 'optionsQuestion';
  }

  isTableQuestionForm(formGroup: QuestionnaireEntryForm): formGroup is TableQuestionForm {
    return formGroup.value.entryType === 'tableQuestion';
  }

  toggleExpansion() {
    this.expanded = !this.expanded;
  }

  expand() {
    this.expanded = true;
  }

  collapse() {
    this.expanded = false;
  }
}
