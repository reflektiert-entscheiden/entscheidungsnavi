import { Pipe, PipeTransform } from '@angular/core';
import { QuestionnaireEntryType } from '@entscheidungsnavi/api-types';

@Pipe({ name: 'questionnaireEntryType' })
export class QuestionnaireEntryTypePipe implements PipeTransform {
  transform(value: QuestionnaireEntryType): string {
    switch (value) {
      case 'textBlock':
        return $localize`Textausgabe`;
      case 'textQuestion':
        return $localize`Texteingabe`;
      case 'numberQuestion':
        return $localize`Zahleneingabe`;
      case 'optionsQuestion':
        return $localize`Optioneneingabe`;
      case 'tableQuestion':
        return $localize`Optioneneingabe in Tabellenform`;
    }
  }
}
