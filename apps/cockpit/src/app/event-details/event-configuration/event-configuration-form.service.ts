import { Injectable } from '@angular/core';
import { FormControl, FormGroup, NonNullableFormBuilder, Validators } from '@angular/forms';
import { CreateEventType, EventDto } from '@entscheidungsnavi/api-client';
import { OnDestroyObservable } from '@entscheidungsnavi/tools';
import { createControlDependency, matchParentError } from '@entscheidungsnavi/widgets';
import { Observable } from 'rxjs';

function toDateString(date: Date): string {
  return (
    date.getFullYear().toString() +
    '-' +
    ('0' + (date.getMonth() + 1)).slice(-2) +
    '-' +
    ('0' + date.getDate()).slice(-2) +
    'T' +
    date.toTimeString().slice(0, 5)
  );
}

function parseDateString(date: string): Date {
  const [dateString, timeString] = date.split('T');
  const dateParts = dateString.split('-').map(Number);
  const timeParts = timeString.split(':').map(Number);

  return new Date(dateParts[0], dateParts[1] - 1, dateParts[2], timeParts[0], timeParts[1]);
}

function validateStartEndDate(group: FormGroup<{ startDate: FormControl<string>; endDate: FormControl<string> }>) {
  const invalid =
    group.value.startDate && group.value.endDate && parseDateString(group.value.startDate) >= parseDateString(group.value.endDate);

  return invalid ? { dateRangeInvalid: true } : null;
}

export type EventConfigurationForm = typeof EventConfigurationFormService.prototype.form;

@Injectable()
export class EventConfigurationFormService {
  @OnDestroyObservable()
  private onDestroy$: Observable<void>;

  readonly form = this.getForm();

  constructor(private fb: NonNullableFormBuilder) {
    createControlDependency(this.form.controls.freeTextEnabled, this.form.controls.freeTextParameters, this.onDestroy$);
    createControlDependency(this.form.controls.enableRegistration, this.form.controls.code, this.onDestroy$);
  }

  getForm() {
    return this.fb.group(
      {
        name: ['', Validators.required],
        enableRegistration: [true],
        code: ['', [Validators.required, Validators.minLength(3)]],
        // datetime-local binds the value as string formatted as yyyy-MM-ddTHH:mm:ss
        startDate: [''],
        endDate: ['', matchParentError('dateRangeInvalid', this.onDestroy$)],
        requirements: this.fb.group({
          decisionStatement: [true],
          objectives: [true],
          alternatives: [true],
          impactModel: [true],
          objectiveWeights: [true],
          decisionQuality: [true],
        }),
        freeTextEnabled: [false],
        freeTextParameters: this.fb.group({
          name: ['', Validators.required],
          placeholder: [''],
          minLength: [0, Validators.min(0)],
        }),
        requireDataUsageAuthorization: [true],
      },
      { validators: validateStartEndDate },
    );
  }

  loadFromEvent(event: EventDto, toCreateNewEvent: boolean, readonly: boolean) {
    this.form.markAsPristine();
    this.form.setValue({
      name: event.name,
      enableRegistration: !!event.code,
      code: event.code ?? '',
      startDate: event.startDate ? toDateString(event.startDate) : '',
      endDate: event.endDate ? toDateString(event.endDate) : '',
      requirements: {
        decisionStatement: event.projectRequirements.requireDecisionStatement,
        objectives: event.projectRequirements.requireObjectives,
        alternatives: event.projectRequirements.requireAlternatives,
        impactModel: event.projectRequirements.requireImpactModel,
        objectiveWeights: event.projectRequirements.requireObjectiveWeights,
        decisionQuality: event.projectRequirements.requireDecisionQuality,
      },
      freeTextEnabled: event.freeTextConfig.enabled,
      freeTextParameters: {
        name: event.freeTextConfig.name,
        placeholder: event.freeTextConfig.placeholder,
        minLength: event.freeTextConfig.minLength,
      },
      requireDataUsageAuthorization: event.requireDataUsageAuthorization,
    });

    if (!toCreateNewEvent) {
      this.form.controls.requireDataUsageAuthorization.disable();
    }

    if (readonly) {
      this.form.disable();
    }
  }

  getEventData(): CreateEventType {
    const v = this.form.getRawValue();

    return {
      name: v.name,
      code: v.enableRegistration ? v.code : null,
      startDate: v.startDate ? parseDateString(v.startDate) : null,
      endDate: v.endDate ? parseDateString(v.endDate) : null,
      projectRequirements: {
        requireDecisionStatement: v.requirements.decisionStatement,
        requireObjectives: v.requirements.objectives,
        requireAlternatives: v.requirements.alternatives,
        requireImpactModel: v.requirements.impactModel,
        requireObjectiveWeights: v.requirements.objectiveWeights,
        requireDecisionQuality: v.requirements.decisionQuality,
      },
      freeTextConfig: {
        ...v.freeTextParameters,
        enabled: v.freeTextEnabled,
      },
      requireDataUsageAuthorization: v.requireDataUsageAuthorization,
    };
  }
}
