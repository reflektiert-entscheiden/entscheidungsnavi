import { Component, inject, Input, OnInit } from '@angular/core';
import { EventRegistration, EventRegistrationFilter, EventRegistrationSort } from '@entscheidungsnavi/api-types';
import { EventDto, EventManagementService, EventRegistrationDto, EventRegistrationWithDataDto } from '@entscheidungsnavi/api-client';
import { readTextWithMetadata } from '@entscheidungsnavi/decision-data/export';
import sanitize from 'sanitize-filename';
import { FileExport } from '@entscheidungsnavi/widgets';
import { EmbeddedDecisionToolModalComponent, EmbeddedDecisionToolModalData } from '@entscheidungsnavi/embedded-decision-tool';
import { BehaviorSubject, catchError, finalize, map, Observable, of, startWith, Subject, switchMap, takeUntil } from 'rxjs';
import { MatDialog } from '@angular/material/dialog';
import { NonNullableFormBuilder } from '@angular/forms';
import { Sort } from '@angular/material/sort';
import { PageEvent } from '@angular/material/paginator';
import { OnDestroyObservable } from '@entscheidungsnavi/tools';
import { isMongoId } from 'class-validator';
import { HttpErrorResponse } from '@angular/common/http';
import { DECISION_TOOL_ORIGIN } from '../../cockpit.module';
// eslint-disable-next-line max-len
import { EventRegistrationInformationModalComponent } from '../registration-information-modal/event-registration-information-modal.component';

@Component({
  selector: 'dt-event-participants-table',
  templateUrl: './event-participants-table.component.html',
  styleUrls: ['./event-participants-table.component.scss'],
})
export class EventParticipantsTableComponent implements OnInit {
  @OnDestroyObservable()
  private onDestroy$: Observable<void>;

  @Input() event: EventDto;
  eventRegistrations: EventRegistration[];

  triggerLoadSubject = new Subject<void>();
  isLoading = false;
  hasError = false;

  private _pendingActions = 0;
  atLeastOneActionPending$ = new BehaviorSubject<boolean>(false);

  totalNumberOfParticipants: number;
  page: { pageIndex: number; pageSize: number } = { pageIndex: 0, pageSize: 100 };
  sort: EventRegistrationSort = { sortBy: 'email', sortDirection: 'asc' };

  filterForm = this.fb.group({
    searchQuery: '',
    projectSubmitted: 'ignore' as 'ignore' | boolean,
  });

  private decisionToolOrigin = inject(DECISION_TOOL_ORIGIN);

  constructor(
    private dialog: MatDialog,
    private eventManagementService: EventManagementService,
    private fb: NonNullableFormBuilder,
  ) {}

  ngOnInit() {
    this.triggerLoadSubject
      .pipe(
        startWith(null),
        switchMap(() => {
          this.isLoading = true;
          const filter: EventRegistrationFilter = {};
          if (this.filterForm.value.searchQuery) {
            filter.query = this.filterForm.value.searchQuery;
          }

          if (this.filterForm.value.projectSubmitted !== 'ignore') {
            filter.projectSubmitted = this.filterForm.value.projectSubmitted;
          }

          if (filter.query && isMongoId(filter.query)) {
            return this.eventManagementService.getOneEventRegistration(this.event.id, filter.query).pipe(
              catchError(error => {
                if (error instanceof HttpErrorResponse && error.status === 404) {
                  return of(null);
                } else {
                  throw error;
                }
              }),
              map(eventRegistration => {
                if (eventRegistration == null) {
                  return { items: [], count: 0 };
                } else {
                  return { items: [eventRegistration], count: 1 };
                }
              }),
            );
          } else {
            return this.eventManagementService.getEventRegistrations(
              this.event.id,
              {
                limit: this.page.pageSize,
                offset: this.page.pageIndex * this.page.pageSize,
              },
              filter,
              this.sort,
            );
          }
        }),
        takeUntil(this.onDestroy$),
      )
      .subscribe({
        next: data => {
          this.isLoading = false;
          this.eventRegistrations = data.items;
          this.totalNumberOfParticipants = data.count;
        },
        error: () => (this.hasError = true),
      });

    this.filterForm.valueChanges.pipe(takeUntil(this.onDestroy$)).subscribe(() => {
      this.triggerLoadSubject.next();
    });
  }

  private incrementPendingActions() {
    if (++this._pendingActions === 1) {
      this.atLeastOneActionPending$.next(true);
    }
  }

  private decrementPendingActions() {
    if (--this._pendingActions === 0) {
      this.atLeastOneActionPending$.next(false);
    }
  }

  onSortChange(sort: Sort) {
    this.sort = { sortDirection: sort.direction, sortBy: sort.active } as EventRegistrationSort;
    this.triggerLoadSubject.next();
  }

  onPageChange(event: PageEvent) {
    this.page = event;
    this.triggerLoadSubject.next();
  }

  openEventRegistrationInformationModal(eventRegistration: EventRegistrationDto) {
    this.dialog.open(EventRegistrationInformationModalComponent, {
      data: { event: this.event, registrationId: eventRegistration.id },
    });
  }

  getCurrentSubmission(registrationId: string) {
    return this.eventManagementService.getOneEventRegistration(this.event.id, registrationId);
  }

  exportProject(eventRegistration: EventRegistrationDto, event: MouseEvent) {
    event.stopImmediatePropagation();
    this.incrementPendingActions();

    this.getCurrentSubmission(eventRegistration.id)
      .pipe(finalize(() => this.decrementPendingActions()))
      .subscribe((submission: EventRegistrationWithDataDto) => {
        const { projectName } = readTextWithMetadata(submission.projectData);
        const fileName = (sanitize(projectName) || 'export') + '.json';
        FileExport.download(fileName, submission.projectData);
      });
  }

  async showProject(eventRegistration: EventRegistrationDto, event: MouseEvent) {
    event.stopImmediatePropagation();
    this.incrementPendingActions();

    this.getCurrentSubmission(eventRegistration.id)
      .pipe(finalize(() => this.decrementPendingActions()))
      .subscribe((submission: EventRegistrationWithDataDto) => {
        const { projectName } = readTextWithMetadata(submission.projectData);

        this.dialog.open<EmbeddedDecisionToolModalComponent, EmbeddedDecisionToolModalData>(EmbeddedDecisionToolModalComponent, {
          data: {
            project: { name: projectName, data: submission.projectData },
            decisionToolOrigin: this.decisionToolOrigin,
          },
        });
      });
  }
}
