import { HttpErrorResponse } from '@angular/common/http';
import { Component, Inject } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { EventManagementService } from '@entscheidungsnavi/api-client';
import { EventWithStats } from '@entscheidungsnavi/api-types';

@Component({
  templateUrl: './event-transfer-owner-modal.component.html',
  styleUrls: ['./event-transfer-owner-modal.component.scss'],
})
export class EventTransferOwnerModalComponent {
  newOwnerEmailForm = new FormControl('', { validators: [Validators.required, Validators.email] });

  constructor(
    @Inject(MAT_DIALOG_DATA) protected event: EventWithStats,
    private dialogRef: MatDialogRef<EventTransferOwnerModalComponent>,
    private snackBar: MatSnackBar,
    private eventManagementService: EventManagementService,
  ) {}

  submit() {
    const newOwnerEmail = this.newOwnerEmailForm.value;
    this.newOwnerEmailForm.disable({ emitEvent: false });
    this.eventManagementService
      .updateEvent(this.event.id, {
        owner: { email: newOwnerEmail },
        editors: [...this.event.editors.filter(user => user.email !== newOwnerEmail), { email: this.event.owner.email }],
      })
      .subscribe({
        next: eventUpdate => {
          Object.assign(this.event, eventUpdate);
          this.snackBar.open($localize`Besitz erfolgreich übertragen`, 'Ok', { duration: 7000 });
          this.dialogRef.close(true);
        },
        error: error => {
          this.newOwnerEmailForm.enable();
          if (error instanceof HttpErrorResponse && error.status === 404) {
            this.newOwnerEmailForm.setErrors({ unknownUser: true });
          } else {
            this.snackBar.open($localize`Unbekannter Fehler beim Übertragen der Besitzrechte`, 'Ok');
          }
        },
      });
  }

  cancel() {
    if (this.newOwnerEmailForm.enabled) this.dialogRef.close();
  }
}
