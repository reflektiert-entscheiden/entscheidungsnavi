import { ENTER, COMMA } from '@angular/cdk/keycodes';
import { Component, EventEmitter, Input, OnChanges, Output, SimpleChanges } from '@angular/core';
import { FormControl, NonNullableFormBuilder } from '@angular/forms';
import { MatChipInputEvent } from '@angular/material/chips';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { EventDto, EventManagementService } from '@entscheidungsnavi/api-client';
import { cloneDeep, without } from 'lodash';
import { isEmail } from 'class-validator';
import { HttpErrorResponse } from '@angular/common/http';
import { EventDeleteModalComponent } from './delete-modal/event-delete-modal.component';
import { EventTransferOwnerModalComponent } from './transfer-owner-modal/event-transfer-owner-modal.component';

@Component({
  selector: 'dt-event-status',
  templateUrl: './event-status.component.html',
  styleUrls: ['./event-status.component.scss'],
})
export class EventStatusComponent implements OnChanges {
  readonly separatorKeysCodes = [ENTER, COMMA] as const;

  @Input() event: EventDto;
  @Output() isDeleting = new EventEmitter<boolean>();

  form = this.fb.group({
    editors: this.fb.control([] as string[], (control: FormControl<string[]>) =>
      control.value.every(entry => isEmail(entry)) ? null : { emailFormat: true },
    ),
    viewers: this.fb.control([] as string[], (control: FormControl<string[]>) =>
      control.value.every(entry => isEmail(entry)) ? null : { emailFormat: true },
    ),
  });

  constructor(
    protected eventManagementService: EventManagementService,
    private dialog: MatDialog,
    private fb: NonNullableFormBuilder,
    private router: Router,
  ) {}

  ngOnChanges(changes: SimpleChanges) {
    if ('event' in changes) {
      this.resetEditorsAndViewers();
    }
  }

  resetEditorsAndViewers() {
    this.form.setValue({
      editors: this.event.editors.map(editor => editor.email),
      viewers: this.event.viewers.map(viewer => viewer.email),
    });
  }

  addEditor(event: MatChipInputEvent) {
    const value = (event.value ?? '').trim();

    if (value && !this.form.value.editors.includes(value)) {
      this.form.controls.editors.setValue([...this.form.value.editors, value]);
      this.form.controls.editors.markAsDirty();
    }

    event.chipInput.clear();
  }

  addViewer(event: MatChipInputEvent) {
    const value = (event.value ?? '').trim();

    if (value && !this.form.value.viewers.includes(value)) {
      this.form.controls.viewers.setValue([...this.form.value.viewers, value]);
      this.form.controls.viewers.markAsDirty();
    }

    event.chipInput.clear();
  }

  removeEditor(editor: string) {
    this.form.controls.editors.setValue(without(this.form.value.editors, editor));
    this.form.controls.editors.markAsDirty();
  }

  removeViewer(viewer: string) {
    this.form.controls.viewers.setValue(without(this.form.value.viewers, viewer));
    this.form.controls.viewers.markAsDirty();
  }

  saveEditors() {
    this.form.disable({ emitEvent: false });
    this.eventManagementService.updateEvent(this.event.id, { editors: this.form.value.editors.map(email => ({ email })) }).subscribe({
      next: eventUpdate => {
        Object.assign(this.event, eventUpdate);
        this.form.enable();
        this.form.markAsPristine();
      },
      error: error => {
        this.form.enable();
        if (error instanceof HttpErrorResponse && error.status === 404) {
          this.form.controls.editors.setErrors({ emailNotFound: true });
        } else {
          this.form.controls.editors.setErrors({ unknownError: true });
        }
      },
    });
  }

  saveViewers() {
    this.form.disable({ emitEvent: false });
    this.eventManagementService.updateEvent(this.event.id, { viewers: this.form.value.viewers.map(email => ({ email })) }).subscribe({
      next: eventUpdate => {
        Object.assign(this.event, eventUpdate);
        this.form.enable();
        this.form.markAsPristine();
      },
      error: error => {
        this.form.enable();
        if (error instanceof HttpErrorResponse && error.status === 404) {
          this.form.controls.viewers.setErrors({ emailNotFound: true });
        } else {
          this.form.controls.viewers.setErrors({ unknownError: true });
        }
      },
    });
  }

  deleteEvent() {
    this.isDeleting.emit(true);
    this.dialog
      .open(EventDeleteModalComponent, { data: this.event })
      .afterClosed()
      .subscribe(() => this.isDeleting.emit(false));
  }

  transferOwnership() {
    this.dialog
      .open(EventTransferOwnerModalComponent, { data: this.event })
      .afterClosed()
      .subscribe(value => {
        if (value) this.resetEditorsAndViewers();
      });
  }

  duplicateEvent() {
    this.router.navigate(['/events/create'], {
      state: {
        event: {
          ...cloneDeep(this.event),
          name: $localize`${this.event.name} (Kopie)`,
        },
      },
    });
  }
}
