import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { EventDto, EventManagementService, EventRegistrationWithDataDto } from '@entscheidungsnavi/api-client';
import { readTextWithMetadata } from '@entscheidungsnavi/decision-data/export';
import { richTextLength, textSize } from '@entscheidungsnavi/tools';
import { calculateProjectStatusValues } from '@entscheidungsnavi/widgets';
import { flatten, sum } from 'lodash';

type Metadata = {
  eventRegistrationId: string;
  userId: string;
  userName: string;
  userEmail: string;
  projectName: string;
  projectSize: number;
  projectProgress: string[];
  projectMode: string;
  projectVersion: string;
  projectActiveTime: number;
  projectTotalTime: number;
  freeTextLength: number;
  submissionDate: Date;
  registrationDate: Date;
};

@Component({
  templateUrl: './event-registration-information-modal.component.html',
  styleUrls: ['./event-registration-information-modal.component.scss'],
})
export class EventRegistrationInformationModalComponent {
  event: EventDto;
  currentParticipantEventRegistration: EventRegistrationWithDataDto;
  metadata: Metadata;

  constructor(
    private dialogRef: MatDialogRef<EventRegistrationInformationModalComponent>,
    @Inject(MAT_DIALOG_DATA) data: { event: EventDto; registrationId: string },
    private eventManagementService: EventManagementService,
  ) {
    this.event = data.event;
    this.setCurrentEventRegistration(this.event.id, data.registrationId);
  }

  setCurrentEventRegistration(eventId: string, registrationId: string) {
    this.eventManagementService.getOneEventRegistration(eventId, registrationId).subscribe((submission: EventRegistrationWithDataDto) => {
      this.currentParticipantEventRegistration = submission;
      this.setMetadata(submission);
    });
  }

  setMetadata(eventRegistration: EventRegistrationWithDataDto) {
    const { data: decisionData, projectName } = eventRegistration.submitted
      ? readTextWithMetadata(eventRegistration.projectData)
      : { data: null, projectName: null };
    const activeTime = decisionData ? sum(flatten(Object.values(decisionData.timeRecording.timers)).map(timer => timer.activeTime)) : 0;
    const totalTime = decisionData ? sum(flatten(Object.values(decisionData.timeRecording.timers)).map(timer => timer.totalTime)) : 0;

    this.metadata = {
      eventRegistrationId: eventRegistration.id,
      userId: eventRegistration.user.id,
      userName: eventRegistration.user.name,
      userEmail: eventRegistration.user.email,
      freeTextLength: richTextLength(eventRegistration.freeText),
      submissionDate: eventRegistration.updatedAt,
      registrationDate: eventRegistration.createdAt,
      projectName,
      projectSize: decisionData ? textSize(eventRegistration.projectData) / 1024 / 1024 : 0,
      projectProgress: decisionData ? calculateProjectStatusValues(decisionData) : [],
      projectMode: decisionData?.projectMode,
      projectVersion: decisionData?.version,
      projectActiveTime: activeTime,
      projectTotalTime: totalTime,
    };
  }

  close() {
    this.dialogRef.close();
  }
}
