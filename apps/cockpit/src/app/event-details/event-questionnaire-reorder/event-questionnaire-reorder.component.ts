import { CdkDragDrop, moveItemInArray } from '@angular/cdk/drag-drop';
import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { range } from 'lodash';

@Component({
  templateUrl: './event-questionnaire-reorder.component.html',
  styleUrls: ['./event-questionnaire-reorder.component.scss'],
})
export class EventQuestionnaireReorderComponent {
  pageOrder: number[]; // The previous indices in their new order

  constructor(
    private dialogRef: MatDialogRef<EventQuestionnaireReorderComponent>,
    @Inject(MAT_DIALOG_DATA) pageCount: number,
  ) {
    this.pageOrder = range(pageCount);
  }

  close(save = false) {
    this.dialogRef.close(save ? this.pageOrder : false);
  }

  onDrop(event: CdkDragDrop<unknown>) {
    moveItemInArray(this.pageOrder, event.previousIndex, event.currentIndex);
  }
}
