import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { QuestionnairePage } from '@entscheidungsnavi/api-types';

@Component({
  templateUrl: './event-questionnaire-preview.component.html',
  styleUrls: ['./event-questionnaire-preview.component.scss'],
})
export class EventQuestionnairePreviewComponent {
  constructor(
    @Inject(MAT_DIALOG_DATA) protected questionnaire: QuestionnairePage[],
    protected dialogRef: MatDialogRef<EventQuestionnairePreviewComponent>,
  ) {}
}
