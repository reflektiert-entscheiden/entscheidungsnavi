import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { EventDto, EventManagementService } from '@entscheidungsnavi/api-client';
import { EventConfigurationFormService } from '../event-configuration/event-configuration-form.service';

@Component({
  templateUrl: './create-event.component.html',
  styleUrls: ['./create-event.component.scss'],
  providers: [EventConfigurationFormService],
})
export class CreateEventComponent implements OnInit {
  get eventForm() {
    return this.formService.form;
  }

  baseEvent: EventDto; // An event we are duplicating from

  isDirty = true;

  constructor(
    private router: Router,
    private eventManagementService: EventManagementService,
    private formService: EventConfigurationFormService,
    private snackBar: MatSnackBar,
  ) {}

  ngOnInit() {
    if ('event' in history.state) {
      this.baseEvent = history.state.event as EventDto;
      this.formService.loadFromEvent(this.baseEvent, true, false);
    }
  }

  submit() {
    this.eventForm.disable({ emitEvent: false });
    const createValue = {
      ...this.formService.getEventData(),
      questionnaire: this.baseEvent?.questionnaire,
    };

    this.eventManagementService.createEvent(createValue).subscribe({
      next: event => {
        this.isDirty = false;
        this.eventManagementService.refreshEvents();
        this.router.navigate(['/events', event.id]);
      },
      error: error => {
        this.eventForm.enable();
        if (error instanceof HttpErrorResponse && error.status === 409 && error.error.duplicateKey) {
          switch (error.error.duplicateKey) {
            case 'code':
              this.eventForm.controls.code.setErrors({ codeConflict: true });
              break;
            case 'name':
              this.eventForm.controls.name.setErrors({ nameConflict: true });
              break;
          }
          this.snackBar.open($localize`Die Eingabe enthält Fehler`, 'Ok');
        } else {
          this.snackBar.open($localize`Fehler beim Speichern der Veranstaltung`, 'Ok');
        }
      },
    });
  }

  cancel() {
    this.isDirty = false;
    this.router.navigate(['/events']);
  }
}
