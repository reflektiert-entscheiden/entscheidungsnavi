import { HttpErrorResponse } from '@angular/common/http';
import { Component, isDevMode, OnInit } from '@angular/core';
import { NonNullableFormBuilder, Validators } from '@angular/forms';
import { AuthService } from '@entscheidungsnavi/api-client';
import { identity } from 'lodash';
import { Observable, filter, takeUntil } from 'rxjs';
import { MatDialog } from '@angular/material/dialog';
import { RequestPasswordResetModalComponent } from '@entscheidungsnavi/widgets';
import { ActivatedRoute, Router } from '@angular/router';
import { OnDestroyObservable, SEED_USERS, SeedUser } from '@entscheidungsnavi/tools';
import { ENVIRONMENT } from '../../environments/environment';

@Component({
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {
  @OnDestroyObservable()
  private onDestroy$: Observable<void>;

  loginForm = this.fb.group({
    email: ['', Validators.required],
    password: ['', Validators.required],
  });

  protected readonly seedUsers = SEED_USERS;

  private redirectUrl = '/';

  get shouldShowSeedUsers() {
    return isDevMode() || ENVIRONMENT.type === 'preview';
  }

  constructor(
    private fb: NonNullableFormBuilder,
    private authService: AuthService,
    private route: ActivatedRoute,
    private router: Router,
    private dialog: MatDialog,
  ) {}

  ngOnInit() {
    this.route.queryParams.subscribe(params => {
      if ('redirectUrl' in params) this.redirectUrl = params['redirectUrl'];
    });

    // As soon as we are logged in, we can leave this page
    this.authService.loggedIn$.pipe(filter(identity), takeUntil(this.onDestroy$)).subscribe(() => this.afterLogin());
  }

  loginWithSeedUser(user: SeedUser) {
    this.loginForm.controls.email.setValue(user.email);
    this.loginForm.controls.password.setValue(user.password);
    this.login();
  }

  private afterLogin() {
    this.router.navigate([this.redirectUrl]);
  }

  login() {
    this.loginForm.updateValueAndValidity();

    if (this.loginForm.valid) {
      this.loginForm.disable({ emitEvent: false });

      const email = this.loginForm.value.email;
      const password = this.loginForm.value.password;

      this.authService.login(email, password).subscribe({
        error: (error: HttpErrorResponse) => {
          this.loginForm.enable();
          console.error(error);
          if (error.status === 401) {
            this.loginForm.setErrors({ 'server-error': 'credentials' });
          } else if (error.status === 429) {
            this.loginForm.setErrors({ 'server-error': 'rate-limit' });
          } else {
            this.loginForm.setErrors({ 'server-error': 'something' });
          }
        },
      });
    }
  }

  resetPassword() {
    this.dialog.open(RequestPasswordResetModalComponent);
  }
}
