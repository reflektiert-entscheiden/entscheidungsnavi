import { HttpErrorResponse } from '@angular/common/http';
import { Component, Inject } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { KlugManagerService, KlugProjectDto } from '@entscheidungsnavi/api-client';
import { finalize } from 'rxjs';

export type KlugProjectModalData = { project?: KlugProjectDto };
@Component({
  templateUrl: './klug-project-modal.component.html',
  styleUrls: ['./klug-project-modal.component.scss'],
})
export class KlugProjectModalComponent {
  protected editingProject: KlugProjectDto;
  protected createNew = false;

  protected loading = false;

  protected projectFormGroup = new FormGroup({
    token: new FormControl<string>('', [Validators.required]),
    readonly: new FormControl<boolean>(false),
    isOfficial: new FormControl<boolean>(false),
  });

  constructor(
    private dialogRef: MatDialogRef<KlugProjectModalComponent, boolean>,
    @Inject(MAT_DIALOG_DATA) protected data: KlugProjectModalData,
    protected klugManagerService: KlugManagerService,
    private snackBar: MatSnackBar,
  ) {
    this.editingProject = data?.project;

    if (!this.editingProject) {
      this.createNew = true;
      this.editingProject = new KlugProjectDto();
      this.editingProject.token = '';
      this.editingProject.isOfficial = false;
      this.editingProject.readonly = false;
    }

    this.projectFormGroup = new FormGroup({
      token: new FormControl<string>(this.editingProject.token, [Validators.required]),
      readonly: new FormControl<boolean>(this.editingProject.readonly, [Validators.required]),
      isOfficial: new FormControl<boolean>(this.editingProject.isOfficial, [Validators.required]),
    });
  }

  save() {
    if (this.projectFormGroup.invalid) {
      return;
    }

    this.loading = true;

    const activity$ = (
      this.createNew
        ? this.klugManagerService.createKlugProject(
            this.projectFormGroup.value.token,
            this.projectFormGroup.value.isOfficial,
            this.projectFormGroup.value.readonly,
          )
        : this.klugManagerService.updateKlugProject(this.editingProject.token, {
            token: this.projectFormGroup.value.token,
            isOfficial: this.projectFormGroup.value.isOfficial,
            readonly: this.projectFormGroup.value.readonly,
          })
    ).pipe(finalize(() => (this.loading = false)));

    activity$.subscribe({
      next: () => this.dialogRef.close(true),
      error: error => {
        if (error instanceof HttpErrorResponse && error.status === 409) {
          this.projectFormGroup.controls.token.setErrors({
            duplicateToken: true,
          });
        } else {
          this.snackBar.open(
            this.createNew
              ? $localize`Beim Erstellen des Projekts ist ein Fehler aufgetreten.`
              : $localize`Beim Bearbeiten des Projekts ist ein Fehler aufgetreten.`,
            'Ok',
          );
        }
      },
    });
  }

  delete() {
    this.loading = true;

    this.klugManagerService
      .deleteKlugProject(this.editingProject.token)
      .pipe(finalize(() => (this.loading = false)))
      .subscribe({
        next: () => {
          this.dialogRef.close(true);
        },
        error: () => this.snackBar.open($localize`Beim Löschen des Projekts ist ein Fehler aufgetreten.`, 'Ok'),
      });
  }

  discard() {
    this.dialogRef.close(false);
  }
}
