import { Location } from '@angular/common';
import { Component, EventEmitter, Inject, LOCALE_ID, Output } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { AuthService } from '@entscheidungsnavi/api-client';
import { AccountModalComponent } from '@entscheidungsnavi/widgets';

@Component({
  selector: 'dt-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss'],
})
export class NavbarComponent {
  @Output() menuClick = new EventEmitter<void>();
  language: 'de' | 'en';

  constructor(
    protected authService: AuthService,
    private location: Location,
    @Inject(LOCALE_ID) locale: string,
    private dialog: MatDialog,
  ) {
    this.language = locale.includes('de') ? 'de' : 'en';
  }

  openProfile() {
    this.dialog.open(AccountModalComponent);
  }

  logout() {
    this.authService.logout().subscribe();
  }

  changeLanguage(newLanguage: 'de' | 'en') {
    if (this.language !== newLanguage) {
      const url = `../${newLanguage}`;
      document.location.href = this.location.prepareExternalUrl(url + this.location.path());
    }
  }
}
