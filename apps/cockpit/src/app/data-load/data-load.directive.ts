import { Directive, Input, OnChanges, OnInit, Renderer2, TemplateRef, ViewContainerRef } from '@angular/core';
import { MatProgressSpinner } from '@angular/material/progress-spinner';
import { assertUnreachable } from '@entscheidungsnavi/tools/assert-unreachable';
import { DataLoadErrorComponent } from './data-load-error.component';

interface DataLoadContext<T> {
  $implicit: T;
  dtDataLoad: T;
}

/**
 * We borrow some code from the ngIf directive to enable type checking in the template:
 * https://angular.io/guide/structural-directives#improving-template-type-checking-for-custom-directives
 * https://github.com/angular/angular/blob/e40a640dfe54b03bfe917d08098c319b0b200d25/packages/common/src/directives/ng_if.ts#L230
 */
@Directive({
  selector: '[dtDataLoad]',
})
export class DataLoadDirective<T> implements OnChanges, OnInit {
  // eslint-disable-next-line @typescript-eslint/naming-convention
  static ngTemplateGuard_dtDataLoad: 'binding';

  static ngTemplateContextGuard<T>(
    _dir: DataLoadDirective<T>,
    ctx: unknown,
  ): ctx is DataLoadContext<Exclude<T, false | 0 | '' | null | undefined>> {
    return true;
  }

  @Input() dtDataLoad: T;
  @Input() dtDataLoadError = false;
  @Input() dtDataLoadErrorTemplate: TemplateRef<unknown>;

  private get state() {
    if (this.dtDataLoadError) return 'error' as const;
    else if (this.dtDataLoad) return 'ready' as const;
    else return 'loading' as const;
  }
  private lastState: typeof this.state;

  constructor(
    private templateRef: TemplateRef<DataLoadContext<T>>,
    private viewContainer: ViewContainerRef,
    private renderer: Renderer2,
  ) {}

  ngOnChanges() {
    // Only update on changes _after_ ngOnInit -> lastState != null
    if (this.lastState && this.lastState !== this.state) {
      this.update();
    }
  }

  ngOnInit() {
    this.update();
  }

  private update() {
    this.lastState = this.state;
    this.viewContainer.clear();

    switch (this.state) {
      case 'ready':
        this.viewContainer.createEmbeddedView<DataLoadContext<T>>(this.templateRef, {
          $implicit: this.dtDataLoad,
          dtDataLoad: this.dtDataLoad,
        });
        break;
      case 'loading': {
        const loadingSpinner = this.viewContainer.createComponent(MatProgressSpinner);
        loadingSpinner.instance.mode = 'indeterminate';
        this.renderer.setStyle(loadingSpinner.location.nativeElement, 'margin', '16px auto');
        break;
      }
      case 'error': {
        if (this.dtDataLoadErrorTemplate) {
          this.viewContainer.createEmbeddedView(this.dtDataLoadErrorTemplate);
        } else {
          this.viewContainer.createComponent(DataLoadErrorComponent);
        }
        break;
      }
      default:
        assertUnreachable(this.state);
    }
  }
}
