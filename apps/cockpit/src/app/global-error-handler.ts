import { ErrorHandler, Injectable, Injector } from '@angular/core';
import * as Sentry from '@sentry/angular';
import { AuthService, UserDto } from '@entscheidungsnavi/api-client';
import { pick } from 'lodash';

@Injectable()
export class GlobalErrorHandler implements ErrorHandler {
  sentryHandler: ErrorHandler;

  constructor(private injector: Injector) {
    this.sentryHandler = Sentry.createErrorHandler({
      showDialog: false,
      logErrors: false,
    });
  }

  async handleError(error: any) {
    let user: UserDto = null;
    try {
      user = this.injector.get(AuthService).user;
    } catch {}

    Sentry.withScope(scope => {
      if (user) {
        scope.setUser(pick(user, ['id', 'email', 'roles']));
      }
      this.sentryHandler.handleError(error);
    });

    // pass the error to the console
    console.error(error);
  }
}
