import { Component, Inject } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialog, MatDialogRef } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { QuickstartService, QuickstartTagDto } from '@entscheidungsnavi/api-client';
import { ConfirmModalComponent } from '@entscheidungsnavi/widgets';
import { filter, finalize, switchMap, tap } from 'rxjs';

@Component({
  templateUrl: './quickstart-tag-details-modal.component.html',
  styleUrls: ['./quickstart-tag-details-modal.component.scss'],
})
export class QuickstartTagDetailsModalComponent {
  formGroup = new FormGroup({
    nameEn: new FormControl(this.tag?.name.en ?? '', { validators: [Validators.required], nonNullable: true }),
    nameDe: new FormControl(this.tag?.name.de ?? '', { validators: [Validators.required], nonNullable: true }),
    id: new FormControl(this.tag?.id),
    weight: new FormControl(this.tag?.weight, [Validators.min(0), Validators.pattern(/^\d+$/)]),
  });

  constructor(
    public dialogRef: MatDialogRef<QuickstartTagDetailsModalComponent>,
    @Inject(MAT_DIALOG_DATA) public tag: QuickstartTagDto,
    private quickstartService: QuickstartService,
    private dialog: MatDialog,
    private snackBar: MatSnackBar,
  ) {}

  save() {
    if (this.formGroup.invalid) {
      return;
    }

    this.formGroup.disable({ emitEvent: false });

    const name = { en: this.formGroup.value.nameEn, de: this.formGroup.value.nameDe };
    const weight = this.formGroup.value.weight;

    (this.tag != null
      ? this.quickstartService.updateTag(this.tag.id, { name, weight })
      : this.quickstartService.addTag(name, weight)
    ).subscribe({
      next: tag => this.dialogRef.close(tag),
      error: () => {
        this.formGroup.enable({ emitEvent: false });
        this.snackBar.open($localize`Fehler beim Speichern`, 'Ok');
      },
    });
  }

  delete() {
    this.dialog
      .open(ConfirmModalComponent, {
        data: {
          title: $localize`Tag löschen`,
          prompt: $localize`Bist Du sicher, dass Du den Tag
          „${this.formGroup.value.nameDe}“/„${this.formGroup.value.nameEn}“
          löschen willst? Dadurch wird der Tag von allen Projekten entfernt.
          Dies kann nicht rückgängig gemacht werden.`,
          buttonConfirm: $localize`Ja, Tag löschen`,
          buttonDeny: $localize`Nein, abbrechen`,
        },
      })
      .afterClosed()
      .pipe(
        filter(result => result),
        tap(() => {
          this.formGroup.disable({ emitEvent: false });
        }),
        switchMap(() => this.quickstartService.deleteTag(this.tag.id)),
        finalize(() => {
          this.formGroup.enable({ emitEvent: false });
        }),
      )
      .subscribe({
        next: () => {
          this.dialogRef.close('deleted');
          this.snackBar.open($localize`Tag gelöscht`, undefined, { duration: 5000 });
        },
        error: () => {
          this.snackBar.open($localize`Löschen fehlgeschlagen`, 'Ok');
        },
      });
  }

  close() {
    if (this.formGroup.enabled) this.dialogRef.close();
  }
}
