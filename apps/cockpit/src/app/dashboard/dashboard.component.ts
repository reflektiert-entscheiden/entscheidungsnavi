import { Component } from '@angular/core';
import { AuthService } from '@entscheidungsnavi/api-client';
import { StatisticsService } from '@entscheidungsnavi/api-client';

@Component({
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
})
export class DashboardComponent {
  stats$ = this.statisticsService.getStatistics();

  constructor(
    private statisticsService: StatisticsService,
    protected authService: AuthService,
  ) {}
}
