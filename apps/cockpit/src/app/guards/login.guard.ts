import { inject } from '@angular/core';
import { Router, CanActivateFn } from '@angular/router';
import { AuthService } from '@entscheidungsnavi/api-client';
import { map } from 'rxjs';

export const loginGuard: CanActivateFn = (_, state) => {
  const authService = inject(AuthService),
    router = inject(Router);

  return authService.loggedIn$.pipe(
    map(loggedIn => loggedIn || router.createUrlTree(['/login'], { queryParams: { redirectUrl: state.url } })),
  );
};
