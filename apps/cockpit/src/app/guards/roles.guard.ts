import { inject } from '@angular/core';
import { CanActivateChildFn, CanActivateFn, Router } from '@angular/router';
import { AuthService } from '@entscheidungsnavi/api-client';
import { Role } from '@entscheidungsnavi/api-types';
import { map } from 'rxjs';

export function rolesGuard(...allowedRoles: Role[]): CanActivateFn | CanActivateChildFn {
  return () => {
    const authService = inject(AuthService),
      router = inject(Router);

    return authService.user$.pipe(
      map(user => {
        const userRoles = user?.roles ?? [];
        const allowedRolesOfUser = allowedRoles.filter(role => userRoles.includes(role));

        return allowedRolesOfUser.length > 0 ? true : router.parseUrl('/');
      }),
    );
  };
}
