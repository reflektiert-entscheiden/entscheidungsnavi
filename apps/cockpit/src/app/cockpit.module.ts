import { ErrorHandler, InjectionToken, NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { ApiClientModule } from '@entscheidungsnavi/api-client';
import {
  AutoFocusDirective,
  ChangeDetectionInfoComponent,
  ElementRefDirective,
  HierarchyComponent,
  HierarchyToolbarComponent,
  HoverPopOverDirective,
  LocalizedStringPipe,
  NoteBtnComponent,
  NoteBtnPresetPipe,
  RichTextEditorComponent,
  TagInputComponent,
  TagListComponent,
  WidgetsModule,
  WidthTriggerDirective,
} from '@entscheidungsnavi/widgets';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MAT_CHECKBOX_DEFAULT_OPTIONS } from '@angular/material/checkbox';
import { MAT_FORM_FIELD_DEFAULT_OPTIONS } from '@angular/material/form-field';
import { MAT_RADIO_DEFAULT_OPTIONS } from '@angular/material/radio';
import { MAT_SLIDE_TOGGLE_DEFAULT_OPTIONS } from '@angular/material/slide-toggle';
import { MatNativeDateModule } from '@angular/material/core';
import { OverlayProgressBarDirective } from '@entscheidungsnavi/widgets/overlay-progress-bar';
import * as Sentry from '@sentry/angular';
import { Router } from '@angular/router';
import { Angulartics2Module } from 'angulartics2';
import { ClipboardModule } from '@angular/cdk/clipboard';
import { ENVIRONMENT } from '../environments/environment';
import { CockpitComponent } from './cockpit.component';
import { CockpitRoutingModule } from './cockpit.routing';
import { LoginComponent } from './login/login.component';
import { NavbarComponent } from './navbar/navbar.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { NavigationLayoutComponent } from './navigation-layout/navigation-layout.component';
import { QuickstartProjectsComponent } from './quickstart-projects/quickstart-projects.component';
import { QuickstartTagsComponent } from './quickstart-tags/quickstart-tags.component';
import { QuickstartTagDetailsModalComponent } from './quickstart-tags/details-modal/quickstart-tag-details-modal.component';
import { QuickstartProjectDetailsModalComponent } from './quickstart-projects/details-modal/quickstart-project-details-modal.component';
import { YoutubeVideoTabsComponent } from './youtube-videos/youtube-video-tabs.component';
import { YoutubeVideosComponent } from './youtube-videos/videos/youtube-videos.component';
import { YoutubeVideoCategoriesComponent } from './youtube-videos/categories/youtube-video-categories.component';
import { EventOverviewComponent } from './event-overview/event-overview.component';
import { EventExportComponent } from './event-export/event-export.component';
import { EventDetailsComponent } from './event-details/event-details.component';
import { EventQuestionnaireEditorComponent } from './event-details/event-questionnaire-editor/event-questionnaire-editor.component';
import { EventStatusComponent } from './event-details/event-status/event-status.component';
import { EventConfigurationComponent } from './event-details/event-configuration/event-configuration.component';
import { CreateEventComponent } from './event-details/create-event/create-event.component';
import { EventQuestionnairePreviewComponent } from './event-details/event-questionnaire-preview/event-questionnaire-preview.component';
import { EventQuestionnaireReorderComponent } from './event-details/event-questionnaire-reorder/event-questionnaire-reorder.component';
import { EventQuestionnaireEntryComponent } from './event-details/event-questionnaire-editor/entry/event-questionnaire-entry.component';
import { QuestionnaireEntryTypePipe } from './event-details/questionnaire-entry-type.pipe';
import { EventDeleteModalComponent } from './event-details/event-status/delete-modal/event-delete-modal.component';
import { EventTransferOwnerModalComponent } from './event-details/event-status/transfer-owner-modal/event-transfer-owner-modal.component';
import { KlugComponent } from './klug/klug.component';
import { KlugProjectModalComponent } from './klug/klug-project-modal/klug-project-modal.component';
import { UserManagementComponent } from './user-management/user-management.component';
import { UserDetailsModalComponent } from './user-management/user-details-modal/user-details-modal.component';
import { UserProjectsComponent } from './user-management/user-projects/user-projects.component';
import { UserPropertiesComponent } from './user-management/user-properties/user-properties.component';
import { TableLayoutComponent } from './table-layout/table-layout.component';
import { TableFooterComponent } from './table-layout/table-footer/table-footer.component';
import { DataLoadDirective } from './data-load/data-load.directive';
import { DataLoadErrorComponent } from './data-load/data-load-error.component';
import { QuickstartValuesComponent } from './quickstart-values/quickstart-values.component';
import { QuickstartValueDetailsModalComponent } from './quickstart-values/details-modal/quickstart-value-details-modal.component';
import { QuickstartQuestionsComponent } from './quickstart-questions/quickstart-questions.component';
import { QuickstartQuestionDetailsModalComponent } from './quickstart-questions/details-modal/quickstart-question-details-modal.component';
import { GlobalErrorHandler } from './global-error-handler';
import { AltAndObjEventExportComponent } from './event-export/alt-and-obj-export/alt-and-obj-event-export.component';
import { ProjectFileEventExportComponent } from './event-export/project-file-export/project-file-event-export.component';
import { StandardEventExportComponent } from './event-export/standard-export/standard-event-export.component';
import { QuickstartHierarchyComponent } from './quickstart-hierarchies/hierarchy/quickstart-hierarchy.component';
import { QuickstartHierarchyDetailsModalComponent } from './quickstart-hierarchies/details/quickstart-hierarchy-details-modal.component';
import { QuickstartHierarchiesComponent } from './quickstart-hierarchies/hierarchies/quickstart-hierarchies.component';
import { QuickstartObjectivesComponent } from './quickstart-hierarchies/objectives/quickstart-objectives.component';
import { QuickstartHierarchiesOverviewComponent } from './quickstart-hierarchies/quickstart-hierarchies-overview.component';
import { QuickstartObjectivesTableComponent } from './quickstart-hierarchies/objectives/table/quickstart-objectives-table.component';
import { EventParticipantsTableComponent } from './event-details/event-participants-table/event-participants-table.component';
// eslint-disable-next-line max-len
import { EventRegistrationInformationModalComponent } from './event-details/registration-information-modal/event-registration-information-modal.component';
// eslint-disable-next-line max-len
import { YoutubeVideoCategoryDetailsModalComponent } from './youtube-videos/categories/details-modal/youtube-video-category-details-modal.component';
import { YoutubeVideoDetailsModalComponent } from './youtube-videos/videos/details-modal/youtube-video-details-modal.component';

export const DECISION_TOOL_ORIGIN = new InjectionToken<string>('Origin of the decision tool.');

@NgModule({
  declarations: [
    CockpitComponent,
    LoginComponent,
    NavbarComponent,
    DashboardComponent,
    UserManagementComponent,
    NavigationLayoutComponent,
    UserDetailsModalComponent,
    UserPropertiesComponent,
    UserProjectsComponent,
    QuickstartProjectsComponent,
    QuickstartTagsComponent,
    QuickstartTagDetailsModalComponent,
    QuickstartProjectDetailsModalComponent,
    EventOverviewComponent,
    EventExportComponent,
    AltAndObjEventExportComponent,
    EventDetailsComponent,
    EventQuestionnaireEditorComponent,
    EventStatusComponent,
    EventConfigurationComponent,
    CreateEventComponent,
    EventQuestionnairePreviewComponent,
    EventQuestionnaireReorderComponent,
    EventQuestionnaireEntryComponent,
    QuestionnaireEntryTypePipe,
    EventDeleteModalComponent,
    EventTransferOwnerModalComponent,
    KlugComponent,
    KlugProjectModalComponent,
    TableLayoutComponent,
    TableFooterComponent,
    DataLoadDirective,
    DataLoadErrorComponent,
    QuickstartValuesComponent,
    QuickstartValueDetailsModalComponent,
    StandardEventExportComponent,
    ProjectFileEventExportComponent,
    QuickstartHierarchiesComponent,
    QuickstartHierarchyDetailsModalComponent,
    QuickstartHierarchyComponent,
    QuickstartHierarchiesOverviewComponent,
    QuickstartObjectivesComponent,
    QuickstartObjectivesTableComponent,
    QuickstartQuestionsComponent,
    QuickstartQuestionDetailsModalComponent,
    EventParticipantsTableComponent,
    EventRegistrationInformationModalComponent,
    YoutubeVideoTabsComponent,
    YoutubeVideosComponent,
    YoutubeVideoDetailsModalComponent,
    YoutubeVideoCategoriesComponent,
    YoutubeVideoCategoryDetailsModalComponent,
  ],
  imports: [
    Angulartics2Module.forRoot({ pageTracking: { clearQueryParams: true } }),
    BrowserModule,
    CockpitRoutingModule,
    WidgetsModule,
    ApiClientModule,
    FormsModule,
    BrowserAnimationsModule,
    HoverPopOverDirective,
    MatNativeDateModule,
    WidthTriggerDirective,
    OverlayProgressBarDirective,
    HierarchyComponent,
    HierarchyToolbarComponent,
    NoteBtnComponent,
    NoteBtnPresetPipe,
    ChangeDetectionInfoComponent,
    RichTextEditorComponent,
    ClipboardModule,
    LocalizedStringPipe,
    TagListComponent,
    TagInputComponent,
    AutoFocusDirective,
    ElementRefDirective,
  ],
  providers: [
    { provide: ErrorHandler, useClass: GlobalErrorHandler },
    {
      provide: Sentry.TraceService,
      deps: [Router],
    },
    {
      provide: DECISION_TOOL_ORIGIN,
      useValue: ENVIRONMENT.decisionToolOrigin,
    },
    {
      provide: MAT_FORM_FIELD_DEFAULT_OPTIONS,
      useValue: { appearance: 'fill', hideRequiredMarker: true },
    },
    {
      provide: MAT_RADIO_DEFAULT_OPTIONS,
      useValue: { color: 'primary' },
    },
    {
      provide: MAT_CHECKBOX_DEFAULT_OPTIONS,
      useValue: { color: 'primary' },
    },
    {
      provide: MAT_SLIDE_TOGGLE_DEFAULT_OPTIONS,
      useValue: { color: 'primary' },
    },
  ],
  bootstrap: [CockpitComponent],
})
export class CockpitModule {
  constructor(private _: Sentry.TraceService) {}
}
