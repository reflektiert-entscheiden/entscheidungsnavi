import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Sort } from '@angular/material/sort';
import { QuickstartObjectiveWithStatsDto, QuickstartTagDto } from '@entscheidungsnavi/api-client';
import { QuickstartObjectiveSort } from '@entscheidungsnavi/api-types';

@Component({
  selector: 'dt-quickstart-objectives-table',
  templateUrl: './quickstart-objectives-table.component.html',
  styleUrls: ['./quickstart-objectives-table.component.scss'],
})
export class QuickstartObjectivesTableComponent {
  readonly defaultColumns = ['nameDe', 'nameEn', 'hierarchyName', 'level', 'accumulatedScore', 'tags'];

  @Input() items: QuickstartObjectiveWithStatsDto[];
  @Input() sort: QuickstartObjectiveSort;
  @Input() tagsById: { [id: string]: QuickstartTagDto };
  @Input() selectedTags: string[];

  @Output() tagClick = new EventEmitter<string>();
  @Output() sortChange = new EventEmitter<Sort>();
  @Output() objectiveClick = new EventEmitter<QuickstartObjectiveWithStatsDto>();

  onTagClick(tagId: string) {
    this.tagClick.emit(tagId);
  }
}
