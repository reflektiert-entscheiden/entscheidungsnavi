import { Component, ElementRef, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { NonNullableFormBuilder } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { PageEvent } from '@angular/material/paginator';
import { Sort } from '@angular/material/sort';
import {
  GroupedQuickstartObjectiveWithStatsDto,
  QuickstartObjectiveWithStatsDto,
  QuickstartService,
  QuickstartTagDto,
} from '@entscheidungsnavi/api-client';
import {
  GroupedQuickstartObjectiveSort,
  GroupedQuickstartObjectiveWithStats,
  QuickstartObjectiveFilter,
  QuickstartObjectiveGroupBy,
  QuickstartObjectiveLevelFilter,
  QuickstartObjectiveSort,
  QuickstartObjectiveWithStats,
} from '@entscheidungsnavi/api-types';
import { OnDestroyObservable } from '@entscheidungsnavi/tools';
import { PopOverService, Tag } from '@entscheidungsnavi/widgets';
import { Observable, startWith, Subject, switchMap, takeUntil } from 'rxjs';
import { MatSnackBar } from '@angular/material/snack-bar';
import { without } from 'lodash';
import {
  QuickstartHierarchyDetailsModalComponent,
  QuickstartHierarchyDetailsModalData,
  QuickstartHierarchyDetailsModalResult,
} from '../details/quickstart-hierarchy-details-modal.component';

@Component({
  selector: 'dt-quickstart-objectives',
  templateUrl: './quickstart-objectives.component.html',
  styleUrls: ['./quickstart-objectives.component.scss'],
})
export class QuickstartObjectivesComponent implements OnInit {
  @OnDestroyObservable()
  private onDestroy$: Observable<void>;

  readonly groupedColumns = ['name', 'itemCount', 'accumulatedScore'];

  isLoading = false;
  hasError = false;

  items: QuickstartObjectiveWithStatsDto[] | GroupedQuickstartObjectiveWithStatsDto[];
  itemCount: number;

  tags: Tag[];
  tagsById: { [id: string]: QuickstartTagDto };

  filterForm = this.fb.group({
    searchQuery: '',
    tags: [[] as string[]],
    level: [null as QuickstartObjectiveLevelFilter],
    groupBy: [null as QuickstartObjectiveGroupBy],
  });
  page: { pageIndex: number; pageSize: number } = { pageIndex: 0, pageSize: 100 };
  defaultSort: QuickstartObjectiveSort = { sortBy: 'accumulatedScore', sortDirection: 'desc' };
  groupedSort: GroupedQuickstartObjectiveSort = { sortBy: 'accumulatedScore', sortDirection: 'desc' };
  triggerLoadSubject = new Subject<void>();

  @ViewChild('groupedDetailsPopover') groupedDetailsTemplate: TemplateRef<any>;
  groupedDetailItems: QuickstartObjectiveWithStats[];

  constructor(
    private quickstartService: QuickstartService,
    private fb: NonNullableFormBuilder,
    private dialog: MatDialog,
    private snackBar: MatSnackBar,
    private popOverService: PopOverService,
  ) {}

  ngOnInit(): void {
    this.quickstartService.getTags().subscribe({
      next: tags => {
        this.tags = tags;
        this.tagsById = tags.reduce(
          (prev, curr) => {
            prev[curr.id] = curr;
            return prev;
          },
          {} as { [key: string]: Tag },
        );
      },
      error: () => (this.hasError = true),
    });

    this.triggerLoadSubject
      .pipe(
        startWith(null),
        switchMap(() => {
          this.isLoading = true;

          const filter: QuickstartObjectiveFilter = {};
          if (this.filterForm.value.searchQuery) {
            filter.query = this.filterForm.value.searchQuery;
          }
          if (this.filterForm.value.tags.length > 0) {
            filter.tags = this.filterForm.value.tags;
          }
          if (this.filterForm.value.level) {
            filter.level = this.filterForm.value.level;
          }

          const pagination = {
            limit: this.page.pageSize,
            offset: this.page.pageIndex * this.page.pageSize,
          };

          if (this.filterForm.value.groupBy) {
            return this.quickstartService.getGroupedObjectives(filter, pagination, this.filterForm.value.groupBy, this.groupedSort, true);
          } else {
            return this.quickstartService.getObjectives(filter, pagination, this.defaultSort, true);
          }
        }),

        takeUntil(this.onDestroy$),
      )
      .subscribe({
        next: data => {
          this.isLoading = false;
          this.items = data.items;
          this.itemCount = data.count;
        },
        error: () => (this.hasError = true),
      });

    this.filterForm.valueChanges.pipe(takeUntil(this.onDestroy$)).subscribe(() => this.triggerLoadSubject.next());
  }

  onPageChange(event: PageEvent) {
    this.page = event;
    this.triggerLoadSubject.next();
  }

  onSortChange(sort: Sort, target: 'grouped' | 'default') {
    if (target === 'default') {
      this.defaultSort = { sortDirection: sort.direction, sortBy: sort.active } as QuickstartObjectiveSort;
    } else {
      this.groupedSort = { sortDirection: sort.direction, sortBy: sort.active } as GroupedQuickstartObjectiveSort;
    }
    this.triggerLoadSubject.next();
  }

  tagClick(tagId: string) {
    if (!this.filterForm.value.tags.includes(tagId)) {
      this.filterForm.patchValue({ tags: [...this.filterForm.value.tags, tagId] });
    } else {
      this.filterForm.patchValue({ tags: without(this.filterForm.value.tags, tagId) });
    }
  }

  openModal(objective: QuickstartObjectiveWithStats) {
    this.quickstartService.getHierarchyWithStats(objective.hierarchyId).subscribe({
      next: hierarchy => {
        this.dialog
          .open<QuickstartHierarchyDetailsModalComponent, QuickstartHierarchyDetailsModalData, QuickstartHierarchyDetailsModalResult>(
            QuickstartHierarchyDetailsModalComponent,
            { data: { hierarchy, tags: this.tags } },
          )
          .afterClosed()
          .subscribe(result => {
            if (result) this.triggerLoadSubject.next();
          });
      },
      error: () => this.snackBar.open($localize`Fehler beim Laden der Hierarchie`, 'Ok'),
    });
  }

  openGroupedDetails(groupedRow: ElementRef<HTMLElement>, groupedEntry: GroupedQuickstartObjectiveWithStats) {
    this.groupedDetailItems = groupedEntry.items;
    this.popOverService.open(this.groupedDetailsTemplate, groupedRow);
  }

  isGroupedList(
    items: QuickstartObjectiveWithStatsDto[] | GroupedQuickstartObjectiveWithStatsDto[],
  ): items is GroupedQuickstartObjectiveWithStatsDto[] {
    return items?.length > 0 && items[0] instanceof GroupedQuickstartObjectiveWithStatsDto;
  }
}
