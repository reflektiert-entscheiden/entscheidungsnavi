import { Component, ElementRef, HostListener, Input, ViewChild } from '@angular/core';
import { LocalizedString, QuickstartHierarchyNodeWithStats } from '@entscheidungsnavi/api-types';
import { emptyRichText, Tree } from '@entscheidungsnavi/tools';
import {
  HierarchyAction,
  HierarchyComponent,
  HierarchyNodeStateDirective,
  KeyBindHandlerDirective,
  PlatformDetectService,
  PopOverService,
} from '@entscheidungsnavi/widgets';
import { Clipboard } from '@angular/cdk/clipboard';
import { ObjectiveElement } from '@entscheidungsnavi/decision-data';

@Component({
  selector: 'dt-quickstart-hierarchy',
  templateUrl: './quickstart-hierarchy.component.html',
  styleUrls: ['./quickstart-hierarchy.component.scss'],
  hostDirectives: [KeyBindHandlerDirective],
})
export class QuickstartHierarchyComponent {
  @Input({ required: true }) tree: Tree<QuickstartHierarchyNodeWithStats>;

  @ViewChild(HierarchyComponent, { read: ElementRef }) hierarchy: ElementRef<HTMLElement>;
  @ViewChild(HierarchyNodeStateDirective) nodeState: HierarchyNodeStateDirective<ObjectiveElement>;

  constructor(
    private clipboard: Clipboard,
    private popOverService: PopOverService,
    platformDetectService: PlatformDetectService,
    keyBindHandler: KeyBindHandlerDirective,
  ) {
    if (platformDetectService.macOS) {
      keyBindHandler.register({
        key: 'c',
        metaKey: true,
        callback: () => this.copyToClipboard(),
      });
    } else {
      keyBindHandler.register({
        key: 'c',
        ctrlKey: true,
        callback: () => this.copyToClipboard(),
      });
    }
  }

  private getNewTree(name: LocalizedString = { de: '', en: '' }) {
    return new Tree<QuickstartHierarchyNodeWithStats>({
      id: null,
      name,
      comment: { de: emptyRichText(), en: emptyRichText() },
      scaleComment: { de: emptyRichText(), en: emptyRichText() },
      accumulatedScore: 0,
    });
  }

  executeAction(action: HierarchyAction) {
    switch (action.type) {
      case 'delete':
        this.tree.removeNode(action.location);
        break;
      case 'insert':
        this.tree.insertNode(action.location, this.getNewTree());
        break;
      case 'move': {
        const node = this.tree.removeNode(action.from);
        this.tree.insertNode(action.to, node);
        break;
      }
      case 'compound':
        action.actions.forEach(action => this.executeAction(action));
        break;
    }
  }

  copyToClipboard() {
    const focused = this.nodeState.focused();
    if (focused.length === 0) return;

    const textData = focused
      .map(location => this.tree.getNode(location).value)
      .map(value => `${value.name.de} / ${value.name.en}`)
      .join('\n');

    if (this.clipboard.copy(textData)) {
      this.popOverService.whistle(this.hierarchy, $localize`Elemente kopiert!`, 'done');
    } else {
      this.popOverService.whistle(this.hierarchy, $localize`Kopieren fehlgeschlagen. Möglicherweise ist der Text zu lang.`, 'error');
    }
  }

  @HostListener('document:paste', ['$event'])
  async pasteFromClipboard(event: ClipboardEvent) {
    event.preventDefault();

    const data = event.clipboardData.getData('text/plain');

    const names = data
      .split('\n')
      .map(item => {
        const langElements = item.split('/', 2).map(str => str.trim());

        if (langElements.length !== 2) return null;

        return { de: langElements[0], en: langElements[1] } satisfies LocalizedString;
      })
      .filter(Boolean);
    if (names.length === 0) return;

    const focused = this.nodeState.focused();
    if (focused.length !== 1) {
      this.popOverService.whistle(this.hierarchy, $localize`Wähle einen einzelnen Knoten zum Einfügen aus`);
      return;
    }

    const parentLoc = focused[0];
    const childCount = this.tree.getNode(parentLoc).children.length;

    names.forEach((name, index) => {
      this.tree.insertNode(parentLoc.getChild(childCount + index), this.getNewTree(name));
    });

    this.popOverService.whistle(this.hierarchy, $localize`Erfolgreich eingefügt!`, 'done');
  }
}
