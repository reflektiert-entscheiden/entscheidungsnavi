import { Component, Inject } from '@angular/core';
import { NonNullableFormBuilder, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialog, MatDialogRef } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { QuickstartHierarchyDto, QuickstartService } from '@entscheidungsnavi/api-client';
import { QuickstartHierarchyNodeWithStats, QuickstartHierarchyWithStats, QuickstartTag } from '@entscheidungsnavi/api-types';
import { emptyRichText, Tree } from '@entscheidungsnavi/tools';
import { ConfirmModalComponent, ConfirmModalData } from '@entscheidungsnavi/widgets';
import { cloneDeep } from 'lodash';
import { filter, finalize, firstValueFrom, switchMap, tap } from 'rxjs';

const defaultQuickstartTree = new Tree<QuickstartHierarchyNodeWithStats>({
  id: null,
  name: { de: 'Ziele', en: 'Objectives' },
  comment: { de: emptyRichText(), en: emptyRichText() },
  scaleComment: { de: emptyRichText(), en: emptyRichText() },
  accumulatedScore: 0,
});

export interface QuickstartHierarchyDetailsModalData {
  hierarchy?: QuickstartHierarchyWithStats;
  tags: QuickstartTag[];
}

export type QuickstartHierarchyDetailsModalResult = false | 'deleted' | QuickstartHierarchyDto;

@Component({
  templateUrl: './quickstart-hierarchy-details-modal.component.html',
  styleUrls: ['./quickstart-hierarchy-details-modal.component.scss'],
})
export class QuickstartHierarchyDetailsModalComponent {
  readonly formGroup = this.fb.group({
    nameDe: [this.data.hierarchy?.name.de ?? '', Validators.required],
    nameEn: [this.data.hierarchy?.name.en ?? '', Validators.required],
    tags: [this.data.hierarchy?.tags.slice() ?? []],
  });

  readonly tree = Tree.from(cloneDeep(this.data.hierarchy?.tree ?? defaultQuickstartTree));

  get hasChanges() {
    return this.formGroup.touched || !this.tree.equals(this.data.hierarchy?.tree ?? defaultQuickstartTree);
  }

  get isTreeValid() {
    return this.tree.every(node => node.name.de.length > 0 && node.name.en.length > 0);
  }

  constructor(
    private dialogRef: MatDialogRef<QuickstartHierarchyDetailsModalComponent, QuickstartHierarchyDetailsModalResult>,
    @Inject(MAT_DIALOG_DATA) protected data: QuickstartHierarchyDetailsModalData,
    private fb: NonNullableFormBuilder,
    private dialog: MatDialog,
    private snackBar: MatSnackBar,
    private quickstartService: QuickstartService,
  ) {}

  save() {
    if (this.formGroup.invalid || !this.isTreeValid) {
      return;
    }

    this.formGroup.disable({ emitEvent: false });
    const tree = this.tree;
    const name = { en: this.formGroup.value.nameEn, de: this.formGroup.value.nameDe };
    const tags = this.formGroup.value.tags;

    (this.data.hierarchy != null
      ? this.quickstartService.updateHierarchy(this.data.hierarchy.id, { name, tags, tree })
      : this.quickstartService.addHierarchy({ name, tags, tree })
    ).subscribe({
      next: value => this.dialogRef.close(value),
      error: () => {
        this.formGroup.enable({ emitEvent: false });
        this.snackBar.open($localize`Fehler beim Speichern`, 'Ok');
      },
    });
  }

  async discard() {
    if (this.hasChanges) {
      const result = await firstValueFrom(
        this.dialog
          .open<ConfirmModalComponent, ConfirmModalData, boolean>(ConfirmModalComponent, {
            data: {
              title: $localize`Änderungen verwerfen`,
              prompt: $localize`Du hast Änderungen am Zielbaum vorgenommen. Möchtest Du diese Änderungen verwerfen?`,
              template: 'discard',
            },
          })
          .afterClosed(),
      );

      if (!result) return;
    }

    this.dialogRef.close(false);
  }

  delete() {
    this.dialog
      .open(ConfirmModalComponent, {
        data: {
          title: $localize`Zielbaum löschen`,
          prompt: $localize`Bist Du sicher, dass Du den Zielbaum
          „${this.formGroup.value.nameDe}“/„${this.formGroup.value.nameEn}“
          löschen willst? Dies kann nicht rückgängig gemacht werden.`,
          buttonConfirm: $localize`Ja, Zielbaum löschen`,
          buttonDeny: $localize`Nein, abbrechen`,
        },
      })
      .afterClosed()
      .pipe(
        filter(result => result),
        tap(() => {
          this.formGroup.disable({ emitEvent: false });
        }),
        switchMap(() => this.quickstartService.deleteHierarchy(this.data.hierarchy.id)),
        finalize(() => {
          this.formGroup.enable({ emitEvent: false });
        }),
      )
      .subscribe({
        next: () => {
          this.dialogRef.close('deleted');
          this.snackBar.open($localize`Zielbaum gelöscht`, undefined, { duration: 5000 });
        },
        error: () => {
          this.snackBar.open($localize`Löschen fehlgeschlagen`, 'Ok');
        },
      });
  }
}
