import { Component, Inject } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialog, MatDialogRef } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { QuickstartService, QuickstartTagDto } from '@entscheidungsnavi/api-client';
import { ConfirmModalComponent } from '@entscheidungsnavi/widgets';
import { filter, switchMap, tap } from 'rxjs';
import { QuickstartQuestion } from '@entscheidungsnavi/api-types';

@Component({
  templateUrl: './quickstart-question-details-modal.component.html',
  styleUrls: ['./quickstart-question-details-modal.component.scss'],
})
export class QuickstartQuestionDetailsModalComponent {
  formGroup = this.fb.nonNullable.group({
    nameDe: [this.question?.name.de ?? '', Validators.required],
    nameEn: [this.question?.name.en ?? '', Validators.required],
    tags: [this.question?.tags.slice() ?? []],
  });

  get question() {
    return this.data.question;
  }

  get tags() {
    return this.data.tags;
  }

  constructor(
    private dialogRef: MatDialogRef<QuickstartQuestionDetailsModalComponent>,
    @Inject(MAT_DIALOG_DATA) private data: { question: QuickstartQuestion; tags: QuickstartTagDto[] },
    private quickstartService: QuickstartService,
    private fb: FormBuilder,
    private dialog: MatDialog,
    private snackBar: MatSnackBar,
  ) {}

  save() {
    if (this.formGroup.invalid) {
      return;
    }

    this.formGroup.disable({ emitEvent: false });
    const name = { en: this.formGroup.value.nameEn, de: this.formGroup.value.nameDe };
    const tags = this.formGroup.value.tags;

    (this.question != null
      ? this.quickstartService.updateQuestion(this.question.id, { name, tags })
      : this.quickstartService.addQuestion({ name, tags })
    ).subscribe({
      next: question => this.dialogRef.close(question),
      error: () => {
        this.formGroup.enable({ emitEvent: false });
        this.snackBar.open($localize`Fehler beim Speichern`, 'Ok');
      },
    });
  }

  delete() {
    this.dialog
      .open(ConfirmModalComponent, {
        data: {
          title: $localize`Entscheidungsfrage löschen`,
          prompt: $localize`Bist Du sicher, dass Du die Entscheidungsfrage
          „${this.formGroup.value.nameDe}“/„${this.formGroup.value.nameEn}“
          löschen willst? Dies kann nicht rückgängig gemacht werden.`,
          buttonConfirm: $localize`Ja, Entscheidungsfrage löschen`,
          buttonDeny: $localize`Nein, abbrechen`,
        },
      })
      .afterClosed()
      .pipe(
        filter(Boolean),
        tap(() => this.formGroup.disable({ emitEvent: false })),
        switchMap(() => this.quickstartService.deleteQuestion(this.question.id)),
      )
      .subscribe({
        next: () => {
          this.dialogRef.close('deleted');
          this.snackBar.open($localize`Entscheidungsfrage gelöscht`, undefined, { duration: 5000 });
        },
        error: () => {
          this.formGroup.enable({ emitEvent: false });
          this.snackBar.open($localize`Löschen fehlgeschlagen`, 'Ok');
        },
      });
  }

  close() {
    if (this.formGroup.enabled) this.dialogRef.close();
  }
}
