import { ChangeDetectionStrategy, Component, computed, OnInit, signal } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Sort, SortDirection } from '@angular/material/sort';
import { QuickstartService } from '@entscheidungsnavi/api-client';
import { filter, forkJoin } from 'rxjs';
import { QuickstartQuestion, QuickstartQuestionWithStats, QuickstartValue } from '@entscheidungsnavi/api-types';
import { Tag } from '@entscheidungsnavi/widgets';
import { capitalize, omit } from 'lodash';
import { QuickstartQuestionDetailsModalComponent } from './details-modal/quickstart-question-details-modal.component';

type ActiveSort = { active: 'nameDe' | 'nameEn' | 'countAssigned' | 'tags' | 'createdAt'; direction: SortDirection };
type TagWithProductivityFlag = Tag & { isProductive: boolean };

@Component({
  templateUrl: './quickstart-questions.component.html',
  styleUrls: ['./quickstart-questions.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class QuickstartQuestionsComponent implements OnInit {
  tags = signal<Tag[]>([]);
  questions = signal<QuickstartQuestionWithStats[]>([]);

  tagFilter = signal<Tag['id'][]>([]);
  languageFilter = signal<('de' | 'en')[]>(['de']);
  nameFilter = signal<string>('');

  activeSort = signal<ActiveSort>({
    active: 'countAssigned',
    direction: 'desc',
  });

  tagsWithProductivityFlag = computed<TagWithProductivityFlag[]>(() =>
    this.tags().map(tag => ({ ...tag, isProductive: this.displayedQuestions().some(q => q.tags.includes(tag.id)) })),
  );

  tagsById = computed<{ [id: string]: TagWithProductivityFlag }>(() => {
    return this.tagsWithProductivityFlag().reduce((prev: Record<string, TagWithProductivityFlag>, curr: TagWithProductivityFlag) => {
      prev[curr.id] = curr;
      return prev;
    }, {});
  });

  sortedQuestions = computed(() => {
    return [...this.questions()].sort((a, b) => {
      let comparisonResult = 0;

      if (this.activeSort().active === 'nameDe') {
        comparisonResult = a.name.de.localeCompare(b.name.de);
      } else if (this.activeSort().active === 'nameEn') {
        comparisonResult = a.name.en.localeCompare(b.name.en);
      } else if (this.activeSort().active === 'countAssigned') {
        comparisonResult = a.countAssigned - b.countAssigned;
      } else if (this.activeSort().active === 'createdAt') {
        comparisonResult = a.createdAt.getTime() - b.createdAt.getTime();
      }

      return this.activeSort().direction === 'asc' ? comparisonResult : -comparisonResult;
    });
  });

  displayedQuestions = computed(() => {
    return (
      this.nameFilter()
        ? this.sortedQuestions().filter(
            question =>
              // Only search in German/English names if the user sees them.
              (this.showGermanNames() && question.name.de.toLowerCase().includes(this.nameFilter().toLowerCase())) ||
              (this.showEnglishNames() && question.name.en.toLowerCase().includes(this.nameFilter().toLowerCase())),
          )
        : this.sortedQuestions()
    ).filter(project => this.tagFilter().every(tagId => project.tags.includes(tagId)));
  });

  displayedColumns = computed(() => {
    return [...this.languageFilter().map(lang => 'name' + capitalize(lang)), ...['countAssigned', 'tags', 'createdAt']];
  });

  showGermanNames = computed(() => this.languageFilter().includes('de'));
  showEnglishNames = computed(() => this.languageFilter().includes('en'));

  hasError = signal(false);

  constructor(
    private quickstartService: QuickstartService,
    private dialog: MatDialog,
  ) {}

  ngOnInit() {
    forkJoin({ questions: this.quickstartService.getQuestionsWithCounts(), tags: this.quickstartService.getTags() }).subscribe({
      next: ({ questions, tags }) => {
        this.questions.set(questions);
        this.tags.set(tags);
      },
      error: () => this.hasError.set(true),
    });
  }

  onSortChange(sort: Sort) {
    this.activeSort.set(sort as ActiveSort);
  }

  showDetails(question: QuickstartQuestion) {
    this.dialog
      .open(QuickstartQuestionDetailsModalComponent, {
        data: {
          question,
          tags: this.tagsWithProductivityFlag().map(tag => omit(tag, 'isProductive')),
        },
      })
      .afterClosed()
      .pipe(filter(Boolean))
      .subscribe((result: QuickstartValue | 'deleted') =>
        this.questions.update(questions => {
          const index = questions.findIndex(element => element.id === question.id);

          if (result === 'deleted') {
            return [...questions.slice(0, index), ...questions.slice(index + 1)];
          } else {
            Object.assign(questions[index], result);
            return questions.slice();
          }
        }),
      );
  }

  tagClick(tagId: string) {
    this.tagFilter.update(tagIdFilter => {
      const tagIndex = tagIdFilter.indexOf(tagId);
      if (tagIndex === -1) {
        return [...tagIdFilter, tagId];
      } else {
        return [...tagIdFilter.slice(0, tagIndex), ...tagIdFilter.slice(tagIndex + 1)];
      }
    });
  }

  addQuestion() {
    this.dialog
      .open(QuickstartQuestionDetailsModalComponent, {
        data: {
          tags: this.tagsWithProductivityFlag().map(tag => omit(tag, 'isProductive')),
        },
      })
      .afterClosed()
      .subscribe((newQuestion: QuickstartQuestion) => {
        if (newQuestion) {
          this.questions.update(questions => [...questions, { ...newQuestion, countAssigned: 0 }]);
        }
      });
  }

  tagSelected(tagId: string) {
    return this.tagFilter().includes(tagId);
  }
}
