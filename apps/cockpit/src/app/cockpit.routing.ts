import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { DashboardComponent } from './dashboard/dashboard.component';
import { CreateEventComponent } from './event-details/create-event/create-event.component';
import { EventDeactivateGuard } from './event-details/event-deactivate.guard';
import { EventDetailsComponent } from './event-details/event-details.component';
import { EventExportComponent } from './event-export/event-export.component';
import { EventOverviewComponent } from './event-overview/event-overview.component';
import { rolesGuard } from './guards/roles.guard';
import { LoginComponent } from './login/login.component';
import { NavigationLayoutComponent } from './navigation-layout/navigation-layout.component';
import { KlugComponent } from './klug/klug.component';
import { QuickstartProjectsComponent } from './quickstart-projects/quickstart-projects.component';
import { QuickstartTagsComponent } from './quickstart-tags/quickstart-tags.component';
import { loginGuard } from './guards/login.guard';
import { UserManagementComponent } from './user-management/user-management.component';
import { QuickstartValuesComponent } from './quickstart-values/quickstart-values.component';
import { QuickstartHierarchiesOverviewComponent } from './quickstart-hierarchies/quickstart-hierarchies-overview.component';
import { QuickstartQuestionsComponent } from './quickstart-questions/quickstart-questions.component';
import { YoutubeVideoTabsComponent } from './youtube-videos/youtube-video-tabs.component';

const appRoutes: Routes = [
  {
    path: 'login',
    component: LoginComponent,
  },
  {
    path: '',
    canActivate: [loginGuard],
    component: NavigationLayoutComponent,
    children: [
      {
        path: '',
        pathMatch: 'full',
        redirectTo: 'dashboard',
      },
      {
        path: 'dashboard',
        component: DashboardComponent,
      },
      {
        path: 'users',
        canActivate: [rolesGuard('admin')],
        component: UserManagementComponent,
      },
      {
        path: '',
        canActivateChild: [rolesGuard('quickstart-manager')],
        children: [
          {
            path: 'quickstart-projects',
            component: QuickstartProjectsComponent,
          },
          {
            path: 'quickstart-tags',
            component: QuickstartTagsComponent,
          },
          {
            path: 'quickstart-values',
            component: QuickstartValuesComponent,
          },
          {
            path: 'quickstart-questions',
            component: QuickstartQuestionsComponent,
          },
          {
            path: 'quickstart-hierarchies',
            component: QuickstartHierarchiesOverviewComponent,
          },
          {
            path: 'youtube-videos',
            component: YoutubeVideoTabsComponent,
          },
        ],
      },
      {
        path: '',
        children: [
          {
            path: 'events',
            component: EventOverviewComponent,
          },
          {
            path: 'events/create',
            component: CreateEventComponent,
            canActivate: [rolesGuard('event-manager')],
            canDeactivate: [EventDeactivateGuard],
          },
          {
            path: 'events/:eventId',
            component: EventDetailsComponent,
            canDeactivate: [EventDeactivateGuard],
          },
          {
            path: 'event-export',
            component: EventExportComponent,
          },
        ],
      },
      {
        path: 'klug',
        canActivate: [rolesGuard('klug-manager')],
        component: KlugComponent,
      },
    ],
  },
  {
    path: '**',
    redirectTo: '/',
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(appRoutes, {
      useHash: false,
      preloadingStrategy: PreloadAllModules,
    }),
  ],
  exports: [RouterModule],
})
export class CockpitRoutingModule {}
