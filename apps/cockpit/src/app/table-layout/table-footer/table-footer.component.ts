import { Component, Input } from '@angular/core';

@Component({
  selector: 'dt-table-footer',
  template: `<ng-container i18n
    >Angezeigte Elemente:
    @if (visibleCount != null) {
      {{ visibleCount }} von
    }
    {{ totalCount }}</ng-container
  >`,
  styleUrls: [`./table-footer.component.scss`],
})
export class TableFooterComponent {
  @Input() visibleCount?: number;
  @Input() totalCount: number;
}
