import { ENTER, COMMA } from '@angular/cdk/keycodes';
import { Component, ElementRef, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { UntypedFormGroup, UntypedFormControl } from '@angular/forms';
import { MatAutocompleteSelectedEvent } from '@angular/material/autocomplete';
import { MatChipInputEvent } from '@angular/material/chips';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Role, ROLES } from '@entscheidungsnavi/api-types';
import { AuthService, UserDto, UsersService } from '@entscheidungsnavi/api-client';
import { ConfirmModalComponent } from '@entscheidungsnavi/widgets';
import { Observable, startWith, map, finalize, filter, tap, switchMap } from 'rxjs';

@Component({
  selector: 'dt-user-properties',
  templateUrl: './user-properties.component.html',
  styleUrls: ['./user-properties.component.scss'],
})
export class UserPropertiesComponent implements OnInit {
  readonly separatorKeysCodes: number[] = [ENTER, COMMA];

  @Input() user: UserDto;
  @Output() isBusy = new EventEmitter<boolean>();
  // Fires when the user object has changed
  @Output() userDeleted = new EventEmitter<void>();

  @ViewChild('roleInput') roleInput: ElementRef<HTMLInputElement>;

  roles: Role[];
  filteredRoles: Observable<Role[]>;
  formGroup: UntypedFormGroup;

  // True if the user is editing himself
  readonly isCurrentUser$ = this.authService.user$.pipe(map(user => user.id === this.user.id));

  constructor(
    private dialog: MatDialog,
    private snackBar: MatSnackBar,
    private usersService: UsersService,
    private authService: AuthService,
  ) {}

  ngOnInit() {
    this.reset();

    this.formGroup = new UntypedFormGroup({
      roleFilter: new UntypedFormControl(''),
    });

    this.filteredRoles = this.formGroup.get('roleFilter').valueChanges.pipe(
      startWith(null),
      map((filter: string | null) => {
        const base = ROLES.filter(role => !this.roles.includes(role));

        return filter ? base.filter(role => role.toLowerCase().includes(filter.toLowerCase())) : base;
      }),
    );
  }

  addRole(event: MatChipInputEvent) {
    const role = (event.value || '').trim() as any;

    if (ROLES.includes(role) && !this.roles.includes(role)) {
      this.roles.push(role as Role);
    }

    event.chipInput.clear();
    this.formGroup.get('roleFilter').setValue(null);
  }

  removeRole(role: Role) {
    this.isCurrentUser$.subscribe(isCurrentUser => {
      if (isCurrentUser && role === 'admin') {
        this.snackBar.open($localize`Du kannst Dir nicht selbst die Adminrechte nehmen`, 'Ok');
      } else {
        this.roles?.splice(this.roles.indexOf(role), 1);
      }
    });
  }

  roleSelected(event: MatAutocompleteSelectedEvent) {
    this.roles.push(event.option.viewValue as Role);
    this.roleInput.nativeElement.value = '';
    this.formGroup.get('roleFilter').setValue(null);
  }

  save() {
    this.formGroup.disable({ emitEvent: false });
    this.isBusy.emit(true);

    this.usersService
      .updateRoles(this.user.id, this.roles)
      .pipe(
        finalize(() => {
          this.formGroup.enable({ emitEvent: false });
          this.isBusy.emit(false);
        }),
      )
      .subscribe({
        next: () => {
          this.user.roles = this.roles;
        },
        error: () => {
          this.snackBar.open($localize`Fehler beim Speichern`, 'Ok');
        },
      });
  }

  deleteUser() {
    this.dialog
      .open(ConfirmModalComponent, {
        data: {
          title: $localize`Benutzer löschen`,
          prompt: $localize`Bist Du sicher, dass Du den Benutzer mit der E-Mail-Adresse
          ${this.user.email} löschen willst? Dadurch werden auch alle Projekte dieses Nutzers
          gelöscht. Dies kann nicht rückgängig gemacht werden.`,
          buttonConfirm: $localize`Ja, Benutzer löschen`,
          buttonDeny: $localize`Nein, abbrechen`,
        },
      })
      .afterClosed()
      .pipe(
        filter(result => result),
        tap(() => {
          this.formGroup.disable({ emitEvent: false });
          this.isBusy.emit(true);
        }),
        switchMap(() => this.usersService.deleteUser(this.user.id)),
        finalize(() => {
          this.formGroup.enable({ emitEvent: false });
          this.isBusy.emit(false);
        }),
      )
      .subscribe({
        next: () => {
          this.userDeleted.emit();
          this.snackBar.open($localize`Benutzer gelöscht`, undefined, { duration: 5000 });
        },
        error: () => {
          this.snackBar.open($localize`Löschen fehlgeschlagen`, 'Ok');
        },
      });
  }

  reset() {
    this.roles = this.user.roles?.slice() ?? [];
  }
}
