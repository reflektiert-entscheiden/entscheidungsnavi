import { Component, OnInit } from '@angular/core';
import { NonNullableFormBuilder } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { ROLES, Role, UserFilter, UserSort } from '@entscheidungsnavi/api-types';
import { UserDto, UsersService } from '@entscheidungsnavi/api-client';
import { catchError, map, Observable, of, startWith, Subject, switchMap, takeUntil } from 'rxjs';
import { OnDestroyObservable } from '@entscheidungsnavi/tools';
import { HttpErrorResponse } from '@angular/common/http';
import { isMongoId } from 'class-validator';
import { PageEvent } from '@angular/material/paginator';
import { Sort } from '@angular/material/sort';
import { UserDetailsModalComponent } from './user-details-modal/user-details-modal.component';

@Component({
  templateUrl: './user-management.component.html',
  styleUrls: ['./user-management.component.scss'],
})
export class UserManagementComponent implements OnInit {
  @OnDestroyObservable()
  private onDestroy$: Observable<void>;

  readonly displayedColumns = ['name', 'email', 'emailConfirmed', 'roles', 'createdAt'];

  isLoading = false;
  hasError = false;
  items: UserDto[];
  itemCount: number;

  filterForm = this.fb.group({
    searchQuery: '',
    emailConfirmed: 'ignore' as 'ignore' | boolean,
    roles: [[] as Role[]],
  });
  page: { pageIndex: number; pageSize: number } = { pageIndex: 0, pageSize: 100 };
  sort: UserSort = { sortBy: 'createdAt', sortDirection: 'desc' };
  triggerLoadSubject = new Subject<void>();

  roles = ROLES;

  constructor(
    private usersService: UsersService,
    private dialog: MatDialog,
    private fb: NonNullableFormBuilder,
  ) {}

  ngOnInit(): void {
    this.triggerLoadSubject
      .pipe(
        startWith(null),
        switchMap(() => {
          this.isLoading = true;

          const filter: UserFilter = {};
          if (this.filterForm.value.searchQuery) {
            filter.query = this.filterForm.value.searchQuery;
          }
          if (this.filterForm.value.emailConfirmed !== 'ignore') {
            filter.emailConfirmed = this.filterForm.value.emailConfirmed;
          }
          if (this.filterForm.value.roles.length > 0) {
            filter.roles = this.filterForm.value.roles;
          }

          if (filter.query && isMongoId(filter.query)) {
            return this.usersService.getUser(filter.query).pipe(
              catchError(error => {
                if (error instanceof HttpErrorResponse && error.status === 404) {
                  return of(null);
                } else {
                  throw error;
                }
              }),
              map(user => {
                if (user == null) {
                  return { items: [], count: 0 };
                } else {
                  return { items: [user], count: 1 };
                }
              }),
            );
          } else {
            return this.usersService.getUsers(filter, this.sort, {
              limit: this.page.pageSize,
              offset: this.page.pageIndex * this.page.pageSize,
            });
          }
        }),
        takeUntil(this.onDestroy$),
      )
      .subscribe({
        next: data => {
          this.isLoading = false;
          this.items = data.items;
          this.itemCount = data.count;
        },
        error: () => (this.hasError = true),
      });

    this.filterForm.valueChanges.pipe(takeUntil(this.onDestroy$)).subscribe(() => this.triggerLoadSubject.next());
  }

  onPageChange(event: PageEvent) {
    this.page = event;
    this.triggerLoadSubject.next();
  }

  onSortChange(sort: Sort) {
    this.sort = { sortDirection: sort.direction, sortBy: sort.active } as UserSort;
    this.triggerLoadSubject.next();
  }

  showDetails(user: UserDto) {
    this.dialog
      .open(UserDetailsModalComponent, { data: user })
      .afterClosed()
      .subscribe(result => {
        if (result) {
          this.triggerLoadSubject.next();
        }
      });
  }
}
