import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { UserDto } from '@entscheidungsnavi/api-client';

@Component({
  templateUrl: './user-details-modal.component.html',
  styleUrls: ['./user-details-modal.component.scss'],
})
export class UserDetailsModalComponent {
  propertiesIsBusy = false;

  constructor(
    @Inject(MAT_DIALOG_DATA) public user: UserDto,
    private dialogRef: MatDialogRef<UserDetailsModalComponent>,
  ) {}

  close() {
    if (this.propertiesIsBusy) {
      return;
    }

    this.dialogRef.close();
  }

  onUserDeleted() {
    this.dialogRef.close(true);
  }
}
