import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { OnlineProjectsService, ProjectDto, UserDto, UsersService } from '@entscheidungsnavi/api-client';
import { FileExport } from '@entscheidungsnavi/widgets';
import { finalize } from 'rxjs';
import sanitize from 'sanitize-filename';
import { logError } from '@entscheidungsnavi/tools';

@Component({
  selector: 'dt-user-projects',
  templateUrl: './user-projects.component.html',
  styleUrls: ['./user-projects.component.scss'],
})
export class UserProjectsComponent implements OnInit {
  @Input() user: UserDto;

  displayedColumns = ['name', 'updatedAt', 'createdAt', 'actions'];
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  isLoading = true;
  isDownloading = false;
  projects: MatTableDataSource<ProjectDto> = new MatTableDataSource();

  constructor(
    private usersService: UsersService,
    private onlineProjectsService: OnlineProjectsService,
    private snackBar: MatSnackBar,
  ) {}

  ngOnInit() {
    this.usersService.getUserProjects(this.user.id).subscribe({
      next: projects => {
        this.projects.data = projects;
        this.isLoading = false;
      },
      error: () => {
        this.snackBar.open($localize`Fehler beim Laden der Projektliste`, 'Ok');
      },
    });
    this.projects.sort = this.sort;
  }

  downloadProject(project: ProjectDto) {
    if (this.isDownloading) {
      return;
    }

    this.isDownloading = true;
    this.onlineProjectsService
      .getProject(project.id)
      .pipe(
        logError(),
        finalize(() => (this.isDownloading = false)),
      )
      .subscribe({
        next: data => {
          const name = (sanitize(project.name) || 'project') + '.json';
          FileExport.download(name, data.data);
        },
        error: () => {
          this.snackBar.open($localize`Download fehlgeschlagen`, 'Ok');
        },
      });
  }
}
