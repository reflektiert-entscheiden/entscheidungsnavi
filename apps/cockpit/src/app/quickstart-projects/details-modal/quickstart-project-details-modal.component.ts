import { Component, ElementRef, Inject, ViewChild } from '@angular/core';
import { AbstractControl, FormBuilder, ValidatorFn, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialog, MatDialogRef } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { QuickstartProjectDto, QuickstartService, QuickstartTagDto } from '@entscheidungsnavi/api-client';
import { ConfirmModalComponent, FileExport } from '@entscheidungsnavi/widgets';
import { filter, finalize, of, switchMap, tap } from 'rxjs';
import { readText } from '@entscheidungsnavi/decision-data/export';
import { HttpErrorResponse } from '@angular/common/http';
import { isMongoId } from 'class-validator';

const notMongoIdValidator: ValidatorFn = (control: AbstractControl<string>) => {
  return isMongoId(control.value) ? { 'is-mongo-id': true } : null;
};

@Component({
  templateUrl: './quickstart-project-details-modal.component.html',
  styleUrls: ['./quickstart-project-details-modal.component.scss'],
})
export class QuickstartProjectDetailsModalComponent {
  formGroup = this.fb.nonNullable.group({
    name: [this.data.project?.name ?? '', Validators.required],
    tags: [this.data.project?.tags.slice() ?? []],
    visibility: [!!this.data.project?.visible],
    shareToken: [this.data.project?.shareToken ?? '', notMongoIdValidator],
    fileUpload: this.fb.control(null, { validators: this.data.project ? [] : [Validators.required], nonNullable: false }),
  });

  @ViewChild('fileInput') fileInput: ElementRef<HTMLInputElement>;

  get fileState() {
    if (this.fileInput?.nativeElement.files.length > 0) {
      return $localize`„${this.fileInput.nativeElement.files[0].name}“ wird hochgeladen`;
    } else if (this.data.project) {
      return $localize`Vorhanden`;
    } else {
      return $localize`Keine Daten vorhanden`;
    }
  }

  constructor(
    @Inject(MAT_DIALOG_DATA) protected data: { project?: QuickstartProjectDto; tags: QuickstartTagDto[] },
    private dialogRef: MatDialogRef<QuickstartProjectDetailsModalComponent>,
    private quickstartService: QuickstartService,
    private dialog: MatDialog,
    private snackBar: MatSnackBar,
    private fb: FormBuilder,
  ) {}

  close() {
    if (this.formGroup.enabled) this.dialogRef.close();
  }

  save() {
    if (this.formGroup.invalid) {
      return;
    }

    const { name, tags, visibility: visible, shareToken, fileUpload } = this.formGroup.value;
    this.formGroup.disable({ emitEvent: false });

    const decisionData$ = fileUpload ? this.loadDecisionData(this.fileInput.nativeElement.files[0]) : of(undefined);

    decisionData$
      .pipe(
        switchMap(data => {
          const update = { name, tags, visible, data, shareToken: shareToken || null };

          return this.data.project != null
            ? this.quickstartService.updateProject(this.data.project.id, update)
            : this.quickstartService.addProject(update);
        }),
      )
      .subscribe({
        next: project => this.dialogRef.close(project),
        error: error => {
          this.formGroup.enable();

          if (error['formError']) {
            this.formGroup.setErrors(error['formError']);
          } else if (error instanceof HttpErrorResponse && error.status === 409) {
            this.formGroup.controls.shareToken.setErrors({ 'already-exists': true });
          } else {
            this.snackBar.open($localize`Unbekannter Fehler beim Speichern`, 'Ok');
          }
        },
      });
  }

  private loadDecisionData(file: File) {
    return FileExport.readFile(file).pipe(
      tap(text => {
        // Try to import the file to make sure its a valid decision data object
        try {
          readText(text);
        } catch {
          const e = new Error('Invalid project file');
          (e as any)['formError'] = { 'invalid-project': true };
          throw e;
        }
      }),
    );
  }

  delete() {
    this.dialog
      .open(ConfirmModalComponent, {
        data: {
          title: $localize`Projekt löschen`,
          prompt: $localize`Bist Du sicher, dass Du das Projekt „${this.formGroup.get('name').value}“
          löschen willst? Dies kann nicht rückgängig gemacht werden.`,
          buttonConfirm: $localize`Ja, Projekt löschen`,
          buttonDeny: $localize`Nein, abbrechen`,
        },
      })
      .afterClosed()
      .pipe(
        filter(result => result),
        tap(() => {
          this.formGroup.disable({ emitEvent: false });
        }),
        switchMap(() => this.quickstartService.deleteProject(this.data.project.id)),
        finalize(() => {
          this.formGroup.enable({ emitEvent: false });
        }),
      )
      .subscribe({
        next: () => {
          this.dialogRef.close('deleted');
          this.snackBar.open($localize`Projekt gelöscht`, undefined, { duration: 5000 });
        },
        error: () => {
          this.snackBar.open($localize`Löschen fehlgeschlagen`, 'Ok');
        },
      });
  }
}
