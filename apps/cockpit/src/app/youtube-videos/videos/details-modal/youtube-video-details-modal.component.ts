import { Component, Inject } from '@angular/core';
import { FormBuilder, FormGroup, ValidationErrors, ValidatorFn, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialog, MatDialogRef } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { YoutubeVideoCategoryDto, YoutubeVideoDto, YoutubeVideosService } from '@entscheidungsnavi/api-client';
import { ConfirmModalComponent, ConfirmModalData } from '@entscheidungsnavi/widgets';
import { filter, finalize, Observable, switchMap, tap } from 'rxjs';
import { HttpErrorResponse } from '@angular/common/http';
import { Types } from 'mongoose';

export type YoutubeVideoDetailsModalData = { video?: YoutubeVideoDto; categories: YoutubeVideoCategoryDto[] };
export type YoutubeVideoDetailsModalResult = 'deleted' | YoutubeVideoDto;
@Component({
  templateUrl: './youtube-video-details-modal.component.html',
  styleUrls: ['./youtube-video-details-modal.component.scss'],
})
export class YoutubeVideoDetailsModalComponent {
  formGroup = this.fb.nonNullable.group({
    name: [this.data.video?.name ?? '', Validators.required],
    youtubeId: [this.data.video?.youtubeId ?? '', Validators.required],
    startTime: [this.data.video?.startTime ?? null],
    stepNumbers: [this.data.video?.stepNumbers ?? []],
    categoryIds: [this.data.video?.categoryIds ?? []],
  });

  protected readonly numberOfSteps = 5;

  constructor(
    @Inject(MAT_DIALOG_DATA) protected data: YoutubeVideoDetailsModalData,
    private dialogRef: MatDialogRef<YoutubeVideoDetailsModalComponent, YoutubeVideoDetailsModalResult>,
    private youtubeVideosService: YoutubeVideosService,
    private dialog: MatDialog,
    private snackBar: MatSnackBar,
    private fb: FormBuilder,
  ) {
    this.formGroup.setValidators(this.checkForInvalidStartTime());
  }

  close() {
    if (this.formGroup.enabled) this.dialogRef.close();
  }

  save() {
    if (this.formGroup.invalid) {
      return;
    }

    const { name, youtubeId, startTime, stepNumbers, categoryIds } = this.formGroup.value;
    this.formGroup.disable({ emitEvent: false });
    const categoryIdsToObjectIds =
      categoryIds.length > 0 ? categoryIds.map(categoryIdAsString => new Types.ObjectId(categoryIdAsString)) : [];

    const videoObservable: Observable<YoutubeVideoDto> =
      this.data.video != null
        ? this.youtubeVideosService.updateVideo(this.data.video.id, {
            name,
            youtubeId,
            startTime,
            stepNumbers,
            categoryIds: categoryIdsToObjectIds,
          })
        : this.youtubeVideosService.addVideo({ name, youtubeId, startTime, stepNumbers, categoryIds: categoryIdsToObjectIds });

    videoObservable.subscribe({
      next: video => this.dialogRef.close(video),
      error: error => {
        this.formGroup.enable();

        if (error['formError']) {
          this.formGroup.setErrors(error['formError']);
        } else if (error instanceof HttpErrorResponse && error.status === 409) {
          this.formGroup.controls.youtubeId.setErrors({ 'already-exists': true });
        } else {
          this.snackBar.open($localize`Unbekannter Fehler beim Speichern`, 'Ok');
        }
      },
    });
  }

  delete() {
    this.dialog
      .open<ConfirmModalComponent, ConfirmModalData, boolean>(ConfirmModalComponent, {
        data: {
          title: $localize`Video löschen`,
          prompt: $localize`Bist Du sicher, dass Du das Video „${this.formGroup.get('name').value}“
          löschen willst? Dies kann nicht rückgängig gemacht werden.`,
          buttonConfirm: $localize`Ja, Video löschen`,
          buttonDeny: $localize`Nein, abbrechen`,
        },
      })
      .afterClosed()
      .pipe(
        filter(result => result),
        tap(() => {
          this.formGroup.disable({ emitEvent: false });
        }),
        switchMap(() => this.youtubeVideosService.deleteVideo(this.data.video.id)),
        finalize(() => {
          this.formGroup.enable({ emitEvent: false });
        }),
      )
      .subscribe({
        next: () => {
          this.dialogRef.close('deleted');
          this.snackBar.open($localize`Video gelöscht`, undefined, { duration: 5000 });
        },
        error: () => {
          this.snackBar.open($localize`Löschen fehlgeschlagen`, 'Ok');
        },
      });
  }

  private checkForInvalidStartTime(): ValidatorFn {
    return (group: FormGroup): ValidationErrors => {
      const startTimeControl = group.controls['startTime'];
      if (startTimeControl.value != null && Math.sign(startTimeControl.value) < 0) {
        // negative non-null startTime is invalid
        startTimeControl.setErrors({ 'invalid-start-time': true });
      }
      return;
    };
  }
}
