import { Component, OnInit, ViewChild } from '@angular/core';
import { YoutubeVideoCategoryDto, YoutubeVideoDto, YoutubeVideosService } from '@entscheidungsnavi/api-client';
import { MatSort, Sort } from '@angular/material/sort';
import { forkJoin } from 'rxjs';
import { MatDialog } from '@angular/material/dialog';
import { MatTableDataSource } from '@angular/material/table';
import {
  YoutubeVideoDetailsModalComponent,
  YoutubeVideoDetailsModalData,
  YoutubeVideoDetailsModalResult,
} from './details-modal/youtube-video-details-modal.component';

@Component({
  selector: 'dt-youtube-videos',
  templateUrl: './youtube-videos.component.html',
  styleUrls: ['./youtube-videos.component.scss'],
})
export class YoutubeVideosComponent implements OnInit {
  displayedColumns = ['name', 'youtubeId', 'startTime', 'step', 'category', 'updatedAt'];
  dataSource: MatTableDataSource<YoutubeVideoDto>;

  @ViewChild(MatSort) set sort(sort: MatSort) {
    if (this.dataSource != null) {
      this.dataSource.sort = sort;
    }
  }

  hasError = false;

  protected youtubeVideos: YoutubeVideoDto[] = [];
  protected videoCategories: YoutubeVideoCategoryDto[] = [];

  activeSort: Sort;

  constructor(
    private youtubeVideosService: YoutubeVideosService,
    private dialog: MatDialog,
  ) {}

  ngOnInit() {
    forkJoin({
      videos: this.youtubeVideosService.getYoutubeVideosList(),
      categories: this.youtubeVideosService.getVideoCategoriesList(),
    }).subscribe({
      next: result => {
        this.youtubeVideos = result.videos;
        this.videoCategories = result.categories;

        this.dataSource = new MatTableDataSource<YoutubeVideoDto>();
        this.updateDisplayedYoutubeVideos();
      },
      error: () => (this.hasError = true),
    });
  }

  onSortChange(sort: Sort) {
    this.activeSort = sort;
    this.updateDisplayedYoutubeVideos();
  }

  addVideo() {
    this.dialog
      .open<YoutubeVideoDetailsModalComponent, YoutubeVideoDetailsModalData, YoutubeVideoDetailsModalResult>(
        YoutubeVideoDetailsModalComponent,
        { data: { categories: this.videoCategories } },
      )
      .afterClosed()
      .subscribe(video => {
        if (video && video instanceof YoutubeVideoDto) {
          this.youtubeVideos.push(video);
          this.updateDisplayedYoutubeVideos();
        }
      });
  }

  showDetails(video: YoutubeVideoDto) {
    this.dialog
      .open<YoutubeVideoDetailsModalComponent, YoutubeVideoDetailsModalData, YoutubeVideoDetailsModalResult>(
        YoutubeVideoDetailsModalComponent,
        { data: { video, categories: this.videoCategories } },
      )
      .afterClosed()
      .subscribe(result => {
        const index = this.youtubeVideos.findIndex(element => element.id === video.id);

        if (result === 'deleted') {
          this.youtubeVideos.splice(index, 1);
        } else if (result != null) {
          this.youtubeVideos[index] = result;
        }
        this.updateDisplayedYoutubeVideos();
      });
  }

  updateDisplayedYoutubeVideos() {
    this.dataSource.data = this.youtubeVideos.map(video => {
      const categories = video.categoryIds
        .map(categoryId => this.videoCategories.find(category => category.id === categoryId.toString())?.name)
        .filter(category => category);
      const steps = video.stepNumbers.map(stepNumber => $localize`Schritt ${stepNumber + 1}`);
      return { ...video, steps: steps.join(', '), categories: categories.join(', ') };
    });
  }
}
