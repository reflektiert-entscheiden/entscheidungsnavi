import { HttpErrorResponse } from '@angular/common/http';
import { Component, Inject } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialog, MatDialogRef } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { YoutubeVideoCategoryDto, YoutubeVideosService } from '@entscheidungsnavi/api-client';
import { ConfirmModalComponent } from '@entscheidungsnavi/widgets';
import { Observable, filter, finalize, switchMap, tap } from 'rxjs';

export type YoutubeVideoCategoryDetailsModalData = { category?: YoutubeVideoCategoryDto };
export type YoutubeVideoCategoryDetailsModalResult = 'deleted' | YoutubeVideoCategoryDto;
@Component({
  templateUrl: './youtube-video-category-details-modal.component.html',
  styleUrls: ['./youtube-video-category-details-modal.component.scss'],
})
export class YoutubeVideoCategoryDetailsModalComponent {
  formGroup = this.fb.nonNullable.group({
    name: [this.data.category?.name ?? '', Validators.required],
  });

  constructor(
    @Inject(MAT_DIALOG_DATA) protected data: YoutubeVideoCategoryDetailsModalData,
    private dialogRef: MatDialogRef<YoutubeVideoCategoryDetailsModalComponent, YoutubeVideoCategoryDetailsModalResult>,
    private youtubeVideosService: YoutubeVideosService,
    private dialog: MatDialog,
    private snackBar: MatSnackBar,
    private fb: FormBuilder,
  ) {}

  close() {
    if (this.formGroup.enabled) this.dialogRef.close();
  }

  save() {
    if (this.formGroup.invalid) {
      return;
    }

    const { name } = this.formGroup.value;
    this.formGroup.disable({ emitEvent: false });

    const categoryObservable: Observable<YoutubeVideoCategoryDto> =
      this.data.category != null
        ? this.youtubeVideosService.updateCategory(this.data.category.id, { name })
        : this.youtubeVideosService.addCategory({ name });

    categoryObservable.subscribe({
      next: category => this.dialogRef.close(category),
      error: error => {
        this.formGroup.enable();

        if (error['formError']) {
          this.formGroup.setErrors(error['formError']);
        } else if (error instanceof HttpErrorResponse && error.status === 409) {
          this.formGroup.controls.name.setErrors({ 'already-exists': true });
        } else {
          this.snackBar.open($localize`Unbekannter Fehler beim Speichern`, 'Ok');
        }
      },
    });
  }

  delete() {
    this.dialog
      .open(ConfirmModalComponent, {
        data: {
          title: $localize`Kategorie löschen`,
          prompt: $localize`Bist Du sicher, dass Du die Kategorie „${this.formGroup.get('name').value}“
          löschen willst? Dies kann nicht rückgängig gemacht werden.`,
          buttonConfirm: $localize`Ja, Kategorie löschen`,
          buttonDeny: $localize`Nein, abbrechen`,
        },
      })
      .afterClosed()
      .pipe(
        filter(result => result),
        tap(() => {
          this.formGroup.disable({ emitEvent: false });
        }),
        switchMap(() => this.youtubeVideosService.deleteCategory(this.data.category.id)),
        finalize(() => {
          this.formGroup.enable({ emitEvent: false });
        }),
      )
      .subscribe({
        next: () => {
          this.dialogRef.close('deleted');
          this.snackBar.open($localize`Kategorie gelöscht`, undefined, { duration: 5000 });
        },
        error: () => {
          this.snackBar.open($localize`Löschen fehlgeschlagen`, 'Ok');
        },
      });
  }
}
