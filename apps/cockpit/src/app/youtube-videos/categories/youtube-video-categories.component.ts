import { Component, OnInit } from '@angular/core';
import { YoutubeVideoCategoryDto, YoutubeVideosService } from '@entscheidungsnavi/api-client';
import { MatDialog } from '@angular/material/dialog';
import { CdkDragDrop, moveItemInArray } from '@angular/cdk/drag-drop';
import { MatSnackBar } from '@angular/material/snack-bar';
import {
  YoutubeVideoCategoryDetailsModalComponent,
  YoutubeVideoCategoryDetailsModalData,
  YoutubeVideoCategoryDetailsModalResult,
} from './details-modal/youtube-video-category-details-modal.component';

@Component({
  selector: 'dt-youtube-video-categories',
  templateUrl: './youtube-video-categories.component.html',
  styleUrls: ['./youtube-video-categories.component.scss'],
})
export class YoutubeVideoCategoriesComponent implements OnInit {
  displayedColumns = ['drag', 'name', 'videoCount', 'updatedAt'];

  hasError = false;

  protected videoCategories: YoutubeVideoCategoryDto[];

  constructor(
    private youtubeVideosService: YoutubeVideosService,
    private dialog: MatDialog,
    private snackBar: MatSnackBar,
  ) {}

  ngOnInit() {
    this.youtubeVideosService.getVideoCategoriesList().subscribe({
      next: result => {
        this.videoCategories = result;
      },
      error: () => (this.hasError = true),
    });
  }

  protected addCategory() {
    this.dialog
      .open<YoutubeVideoCategoryDetailsModalComponent, YoutubeVideoCategoryDetailsModalData, YoutubeVideoCategoryDetailsModalResult>(
        YoutubeVideoCategoryDetailsModalComponent,
        { data: {} },
      )
      .afterClosed()
      .subscribe(category => {
        if (category && category instanceof YoutubeVideoCategoryDto) {
          this.videoCategories.push(category);
          this.updateTableData();
        }
      });
  }

  protected showDetails(category: YoutubeVideoCategoryDto) {
    this.dialog
      .open<YoutubeVideoCategoryDetailsModalComponent, YoutubeVideoCategoryDetailsModalData, YoutubeVideoCategoryDetailsModalResult>(
        YoutubeVideoCategoryDetailsModalComponent,
        { data: { category } },
      )
      .afterClosed()
      .subscribe(result => {
        const index = this.videoCategories.findIndex(element => element.id === category.id);

        if (result === 'deleted') {
          this.videoCategories.splice(index, 1);
        } else if (result != null) {
          this.videoCategories[index] = result;
        }
        this.updateTableData();
      });
  }

  protected categoryDrop(event: CdkDragDrop<unknown>) {
    if (event.previousIndex === event.currentIndex) return; // no need to send requests
    const categoryId = this.videoCategories[event.previousIndex].id;
    // pre-order elements, while request is being processed
    const videoCategoriesCopy = this.videoCategories.slice();
    moveItemInArray(videoCategoriesCopy, event.previousIndex, event.currentIndex);
    this.videoCategories = videoCategoriesCopy;

    this.youtubeVideosService
      .changeCategoryPosition({ categoryId, oldIndex: event.previousIndex, newIndex: event.currentIndex })
      .subscribe({
        next: categories => {
          // update with new backend order (should be the same as the "pre-ordered" one)
          this.videoCategories = categories;
        },
        error: () => {
          // reverse pre-order
          moveItemInArray(videoCategoriesCopy, event.currentIndex, event.previousIndex);
          this.videoCategories = videoCategoriesCopy;
          this.snackBar.open($localize`Sortieren fehlgeschlagen`, 'Ok');
        },
      });
  }

  updateTableData() {
    this.videoCategories = this.videoCategories.slice();
  }
}
