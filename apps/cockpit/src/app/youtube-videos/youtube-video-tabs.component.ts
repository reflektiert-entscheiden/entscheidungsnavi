import { Component } from '@angular/core';

@Component({
  template: `<mat-tab-group mat-stretch-tabs="false">
    <mat-tab label="Videos" i18n-label>
      <ng-template matTabContent>
        <dt-youtube-videos></dt-youtube-videos>
      </ng-template>
    </mat-tab>
    <mat-tab label="Themengebiete" i18n-label>
      <ng-template matTabContent>
        <dt-youtube-video-categories></dt-youtube-video-categories>
      </ng-template>
    </mat-tab>
  </mat-tab-group>`,
  styles: [
    `
      :host {
        flex: 1 0 0;
        min-height: 0;
      }
      mat-tab-group {
        max-height: 100%;
      }
    `,
  ],
})
export class YoutubeVideoTabsComponent {}
