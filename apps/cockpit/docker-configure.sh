#!/usr/bin/env sh

cat >/usr/share/nginx/html/de/assets/env.js <<EOL
(function (window) {
  window.cpEnv = {
    environmentType: '${ENVIRONMENT_TYPE}',
    sentryDsn: '${SENTRY_DSN}',
    decisionToolOrigin: '${DECISION_TOOL_ORIGIN}',
  };
  window.dtMatomoSiteId = '${MATOMO_SITE_ID}';
})(this);
EOL

sed "s|<DECISION_TOOL_ORIGIN>|$DECISION_TOOL_ORIGIN|" /etc/nginx/conf.d/template.default.conf >/etc/nginx/conf.d/default.conf

cp /usr/share/nginx/html/de/assets/env.js /usr/share/nginx/html/en/assets/env.js
