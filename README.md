<img src="entscheidungsnavi.svg">

The Entscheidungsnavi is a freely-accessible web application that guides users through the process of reflective decision making.
More information is available at [entscheidungsnavi.com](https://entscheidungsnavi.de).
The tool is accessible at [enavi.app](https://enavi.app).

---

# Documentation

This repository is a monorepo built with [Nx](https://nx.dev).
For detailed information on how to use Nx, we refer to its documentation.

This repository contains the following applications/projects.

| Path                                | Type    | Summary                                                     | Prod URL                                       |
| ----------------------------------- | ------- | ----------------------------------------------------------- | ---------------------------------------------- |
| [backend](apps/backend)             | NestJS  | Backend.                                                    |                                                |
| [cockpit](apps/cockpit)             | Angular | Admin frontend.                                             | [cockpit.enavi.app](https://cockpit.enavi.app) |
| [decision-tool](apps/decision-tool) | Angular | The main Entscheidungsnavi.                                 | [enavi.app](https://enavi.app)                 |
| [klug-tool](apps/klug-tool)         | Angular | A decision support tool for the KLUGentscheiden initiative. | [klugnavi.app](https://klugnavi.app)           |

Furthermore, it contains the following libraries.

| Path                                                       | Type       | Summary                                                         |
| ---------------------------------------------------------- | ---------- | --------------------------------------------------------------- |
| [libs/api-client](libs/api-client)                         | Angular    | An API client for our backend using the Angular HTTP library.   |
| [libs/api-types](libs/api-types)                           | TypeScript | API data types that are used in backend and API client.         |
| [libs/decision-data](libs/decision-data)                   | TypeScript | Contains all data relevant to one Entscheidungsnavi project.    |
| [libs/embedded-decision-tool](libs/embedded-decision-tool) | Angular    | Components for embedding the Entscheidungsnavi into other apps. |
| [libs/widgets](libs/widgets)                               | Angular    | Reusable UI components and services.                            |
| [libs/tools](libs/tools)                                   | TypeScript | Non-UI related helper code.                                     |

## ⚠️ For Windows Users

npm uses CMD to execute scripts by default.
This may cause problems.
We recommend to install PowerShell Core and use that as npm script shell:

```
winget install --id "Microsoft.PowerShell"
npm config set script-shell "pwsh.exe"
```

Alternatively, you may use Git Bash as script shell (which is installed together with Git for Windows) or work entirely in WSL.

## Basic Commands

If you did not install `nx` globally, you need to prefix all respective commands with `npx` (e.g., `npx nx serve cockpit`).
Here are some basic commands:

- **To run a project locally**: `nx serve <project>` (e.g., `nx serve decision-tool`).
  This will start a dev server on your machine.
  The URL can be found in the console output (e.g., [http://localhost:4200](http://localhost:4200) for decision-tool).
  For frontend projects, all requests to the backend will be proxied to our production backend by default.
- **To build a project like in prod**: `nx build <project> --prod --localize`.
- **To commit changes**: simply `git commit` as usual. Our pre-commit hooks will make sure your code is formatted and tested. If you wish to skip the pre-commit hooks for whatever reason, use `git commit -n <remaining commands>`.

## Running full-stack

To run full-stack (backend + decision-tool + cockpit + klug-tool), you need docker installed.

Create a `.env` file according as follows:

```
BASE_URL=http://localhost:4200
MONGODB_URI=mongodb://localhost:27017/navi?replicaSet=ra0&directConnection=true
SESSION_SECRET=badpassword
```

The backend needs a database.
To start it and seed it with some [default users](#seed-credentials), run:

```
docker compose up -d
```

Now, you can run all three apps simultaneously (e.g., in separate terminals):

```
nx serve backend
nx serve decision-tool -c local-backend
nx serve cockpit -c local-backend
nx serve klug-tool -c local-backend
```

All apps should now be running and accessible!

### Seed Credentials

The database is seeded with some users and projects by default.
The user credentials are as follows.

| Login                         | Password | Roles                |
| ----------------------------- | -------- | -------------------- |
| benutzer@entscheidungsnavi.de | 12345678 |                      |
| events@entscheidungsnavi.de   | 12345678 | event-manager        |
| admin@entscheidungsnavi.de    | 12345678 | admin, event-manager |

## E2E-Testing

For some of our e2e-tests, you need to run a local backend with an accompanying database (see [here](#running-full-stack)).

To launch Cypress for our E2E tests, use the following command:

```
npx nx e2e <project>-e2e --watch
```

## Translations (i18n)

The Decision-Tool and Cockpit are translated to English.
Text that should be translated has to be marked with `i18n` attributes (see [Angular's docs](https://angular.io/guide/i18n)).

New strings are automatically uploaded to our translation tool [Lokalise](https://lokalise.com/) from the `main` branch.
There, they need to be translated and subsequently merged into our code base.

## Testing

This project is tested with [BrowserStack](https://www.browserstack.com/).

# License

Copyright (c) 2023 [Förderverein Reflektiert Entscheiden e. V.](https://reflektiert-entscheiden.de/)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.
