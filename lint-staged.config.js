// See https://github.com/okonet/lint-staged/issues/522#issuecomment-597467820
const path = require('path');

module.exports = {
  '{{apps,libs,tools}/**/*,*}.{ts,js,json,css,scss,html,md}': files => {
    const cwd = process.cwd();
    const relPaths = files.map(file => {
      return path.relative(cwd, file);
    });

    return [
      `npm run format:write -- --files ${relPaths.join(',')}`,
      `npm run affected:lint -- --fix --parallel --files ${relPaths.join(',')}`,
      // `npm run affected:test -- --parallel --files ${relPaths.join(',')}`,
    ];
  },
};
