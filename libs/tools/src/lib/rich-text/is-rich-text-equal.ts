import { isEqual } from 'lodash';
import { isRichTextEmpty } from './is-rich-text-empty';

export function isRichTextEqual(firstRichText: string, secondRichText: string) {
  if (isRichTextEmpty(firstRichText) && isRichTextEmpty(secondRichText)) {
    return true;
  } else if (isRichTextEmpty(firstRichText) || isRichTextEmpty(secondRichText)) {
    return false;
  } else {
    return isEqual(firstRichText, secondRichText);
  }
}
