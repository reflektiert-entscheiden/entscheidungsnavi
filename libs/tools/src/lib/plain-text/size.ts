/**
 * Returns the length of {@link text} in bytes when UTF-8 encoded.
 */
export function textSize(text: string) {
  return new TextEncoder().encode(text).length;
}
