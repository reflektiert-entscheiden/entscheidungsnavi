import { range, reverse, without, zip } from 'lodash';

/**
 * This is intended to be used to clone an array (like influenceFactors from decisionData)
 * and later merge it back into the source array using predefined functions (like addInfluenceFactor from decisionData).
 */
export class ArrayCopy<T> {
  /**
   * We create a copy of the source array including indices. Returns the copied array.
   * @param source - the array from which we copy
   * @param copy - a function that creates a copy of an element in the source array.
   */
  static createArrayCopy<TS, TT>(source: TS[], copy: (element: TS, index: number) => TT): ArrayCopy<TT> {
    return new ArrayCopy(source.map((element, index) => [index, copy(element, index)] as [number, TT]));
  }

  /**
   * Creates an instance of ArrayCopy with no elements.
   */
  static createEmpty<T>(): ArrayCopy<T> {
    return new ArrayCopy([]);
  }

  /**
   * The first element is the index in the source array at the time of copying. The second is the content.
   */
  private _elements: Array<[number, T]>;

  get length() {
    return this._elements.length;
  }
  get elements(): T[] {
    return this._elements.map(([, element]) => element);
  }

  private constructor(elements: Array<[number, T]>) {
    this._elements = elements;
  }

  add(index: number, element: T) {
    this._elements.splice(index, 0, [-1, element]);
  }
  remove(index: number) {
    this._elements.splice(index, 1);
  }
  removeAll() {
    this._elements = [];
  }
  get(index: number): T {
    return this._elements[index][1];
  }
  move(from: number, to: number) {
    const element = this._elements.splice(from, 1)[0];
    this._elements.splice(to, 0, element);
  }

  /**
   * We sync source array into target array. We assume that source array was created from
   * target array and that target array has not changed since.
   *
   * @param targetArray - the array which we copy into.
   * @param add - a function to add the given element to targetArray at the given position.
   * @param set - a function to set the element in targetArray at the given position to the given state.
   * @param remove - a function to remove the element in targetArray at the specified position.
   * @param move - a function to move the element in targetArray.
   */
  mergeBack(
    targetArray: unknown[],
    add: (position: number, element: T) => void,
    set: (position: number, element: T) => void,
    remove: (position: number) => void,
    move: (fromPosition: number, toPosition: number) => void,
  ) {
    mergeBackArrayCopy(this._elements, targetArray.length, add, set, remove, move);
  }

  /**
   * Check if the array copy has changes compared to the source array. Returns true if there are changes.
   * @param source - the source array that this ArrayCopy was cloned from
   * @param compare - a function to compare an item from the source array with an item from the ArrayCopy.
   *                Returns true when the items are equal.
   */
  hasChanges<TS>(source: TS[], compare: (source: TS, current: T) => boolean) {
    // First, check if the amount of elements has changed
    if (this._elements.length !== source.length) {
      return true;
    }

    // Secondly, check whether the order of the elements has changed
    if (!this._elements.map(element => element[0]).every((element, index) => element === index)) {
      return true;
    }

    // Finally, Check that every element is unchanged
    return !zip(source, this.elements).every(([sourceEl, currentEl]) => compare(sourceEl, currentEl));
  }
}

/**
 * Sync the array copy into target array using the basic operations add, set, remove, and move.

 * We assume that source array was created from target array and that target array has not changed since.
 *
 * @param arrayCopy - the array copy created from the target.
 * @param targetArrayLength - the length of the array we should copy into.
 * @param add - a function to add the given element to targetArray at the given position.
 * @param set - a function to set the element in targetArray at the given position to the given state.
 * @param remove - a function to remove the element in targetArray at the specified position.
 * @param move - a function to move the element in targetArray.
 */
export function mergeBackArrayCopy<T>(
  arrayCopy: Array<[number, T]>,
  targetArrayLength: number,
  add: (position: number, element: T) => void,
  set: (position: number, element: T) => void,
  remove: (position: number) => void,
  move: (fromPosition: number, toPosition: number) => void,
) {
  // Sync changed items
  arrayCopy
    .filter(([oldIndex]) => oldIndex > -1)
    .forEach(([oldIndex, element]) => {
      set(oldIndex, element);
    });

  // Sync deletes: delete all indices from target that are no longer present in source
  for (const index of without(reverse(range(targetArrayLength)), ...arrayCopy.map(([oldIndex]) => oldIndex))) {
    remove(index);
    arrayCopy.filter(([oldIndex]) => oldIndex > index).forEach(element => element[0]--);
  }

  // Sync position: moved items should be moved in the target array
  let indices = arrayCopy.map(([oldIndex]) => oldIndex).filter(oldIndex => oldIndex !== -1);
  for (let i = 0; i < indices.length; i++) {
    const source = indices[i];
    if (i !== source) {
      move(source, i);

      // Update all the old indices
      indices = indices.map(oldIndex => {
        if (i <= oldIndex && oldIndex < source) {
          return oldIndex + 1;
        }

        return oldIndex;
      });
      indices[i] = i;
    }
  }

  // Sync added items
  arrayCopy
    .map(([oldIndex, element], newIndex) => [oldIndex, element, newIndex] as [number, T, number])
    .filter(([oldIndex]) => oldIndex === -1)
    .forEach(([, element, newIndex]) => {
      add(newIndex, element);
    });
}
