import { Subject } from 'rxjs';
import { materialize } from 'rxjs/operators';

/**
 *   Makes the property its attached to an observable that emits one value after the Component / Directives ngOnDestroy function got called.
 *   Useful with takeUntil() to unsubscribe from observables subscribed to in the component.
 *
 *   @example
 *```ts
 *  @OnDestroyObservable()
 *  private onDestroy$: Observable<any>;
 *```
 */
// eslint-disable-next-line @typescript-eslint/naming-convention
export function OnDestroyObservable() {
  return function (target: unknown, propertyKey: string) {
    // If there already is an ngOnDestroy in the component wrap it and call our code afterwards.
    const originalOnDestroy: () => void = (target as any)['ngOnDestroy'];

    const newOnDestroy = function (this: typeof target) {
      originalOnDestroy?.apply(this);
      (this as any)[propertyKey].complete();
    };

    (target as any)['ngOnDestroy'] = newOnDestroy;

    // Instantiate the Subject the first time it's accessed and then just make it a normal readonly property.
    Object.defineProperty(target, propertyKey, {
      get: function () {
        const onDestroy$ = new Subject();
        const onDestroyM$ = onDestroy$.pipe(materialize());

        Object.defineProperty(this, propertyKey, { writable: false, value: onDestroyM$ });

        return onDestroyM$;
      },
    });
  };
}
