import { sum } from 'lodash';

/**
 * Maps {@link number} from interval [from, to] to [0, 1].
 *
 * @example
 * `normalize(5, 5, 10)` results in 0.
 * `normalize(10, 5, 10)` results in 1.
 */
export function normalizeNumber(number: number, from: number, to: number) {
  // We need to take the absolute, because the result may be -0. This leads to unintended consequences.
  return Math.abs((number - from) / (to - from));
}

/**
 * Normalize an array of values so that they sum up to 1.
 * The normalization is done IN PLACE. This means the original array is modified.
 *
 * @param values - The array to normalize
 */
export function normalizeContinuous(values: number[]) {
  const weightSum = sum(values);
  if (weightSum === 0) {
    return;
  }

  for (let i = 0; i < values.length; i++) {
    values[i] /= weightSum;
  }
}

/**
 * Normalize an array of values such that its sum is {@link targetSum} and every value
 * is discrete with step size {@link step}.
 *
 * This means that every value will be a multiple of {@link step}.
 *
 * The normalization process removes precision from the values (because of the discrete step size).
 * Therefore, some information is lost in the process.
 *
 * @param values - The values to normalize
 * @param targetSum - The values will add up to this sum after normalization
 * @param step - All values will be multiples of this
 * @returns The normalized array
 */
export function normalizeDiscrete(values: number[], targetSum = 100, step = 1): number[] {
  let oldSum = sum(values);
  if (oldSum === 0) {
    // all values are 0, so we assume an equal distribution.
    values = values.map(() => 1);
    oldSum = values.length;
  }

  interface Value {
    value: number;
    error: number;
    idx: number;
  }

  // some helper functions
  const sumValues = (values1: Value[]) => values1.reduce((s: number, val: Value) => s + val.value, 0);
  const sortByBiggestError = (a: Value, b: Value) => (b.error === a.error ? b.idx - a.idx : b.error - a.error);
  const sortBySmallestError = (a: Value, b: Value) => (b.error === a.error ? a.idx - b.idx : a.error - b.error);
  const sortByIdx = (a: Value, b: Value) => a.idx - b.idx;

  // scale the values to the targetSum and save the rounding error and the original index
  // the values are saved as multiples of step to simplify rounding
  const newValues = values.map((value, idx): Value => {
    const newValue = (value * targetSum) / oldSum / step;
    const roundedValue = Math.round(newValue);
    const error = roundedValue - newValue;
    return { value: roundedValue, error, idx };
  });

  // if the sum is lower than targetSum, increase the value with the smallest error value
  // use Math.round to eliminate small floating point errors like 4.99999999999999
  while (sumValues(newValues) < Math.round(targetSum / step)) {
    // find the value with the smallest error value and add 1
    const valueToChange = newValues.sort(sortBySmallestError)[0];
    valueToChange.value += 1;
    valueToChange.error += 1;
  }

  // if the sum is bigger than targetSum, decrease the value with the biggest error value
  while (sumValues(newValues) > Math.round(targetSum / step)) {
    // find the value with the biggest error value and subtract 1
    const valueToChange: Value = newValues.sort(sortByBiggestError)[0];
    valueToChange.value -= 1;
    valueToChange.error -= 1;
  }

  // restore the original order and scale ("/ (1 / step)" is used since it is more stable than "* step" with small step)
  return newValues.sort(sortByIdx).map(val => val.value / (1 / step));
}

/**
 * Works like {@link normalizeDiscrete} but tries to preserve the values with higher priorities.
 *
 * @param values - The values to normalize
 * @param priorities - A priority for every value. Must have the same length as {@link values}.
 * @param targetSum - The values will add up to this sum after normalization
 * @param step - All values will be multiples of this
 * @returns The normalized array
 */
export function normalizeDiscretePrioritized(values: number[], priorities: number[], targetSum = 100, step = 1) {
  if (values.length !== priorities.length) {
    throw new Error('`priorities` must have the same length as `values` and the elements must be numbers');
  }

  const lowestPriority: number = priorities.reduce((min, cur) => (cur < min ? cur : min));
  let lowestPrioValues: number[] = values.filter((val, idx) => priorities[idx] === lowestPriority);
  let higherPrioValues: number[] = values.filter((val, idx) => priorities[idx] !== lowestPriority);

  // round higherPrioValues to multiples of step
  higherPrioValues = higherPrioValues.map(val => Math.round(val / step) / (1 / step));

  const higherPrioSum = sum(higherPrioValues);
  if (higherPrioSum >= targetSum) {
    lowestPrioValues = lowestPrioValues.map(() => 0);
    if (higherPrioSum !== targetSum) {
      const higherPriorities: number[] = priorities.filter(prio => prio !== lowestPriority);
      higherPrioValues = normalizeDiscretePrioritized(higherPrioValues, higherPriorities, targetSum, step);
    }
  } else {
    lowestPrioValues = normalizeDiscrete(lowestPrioValues, targetSum - higherPrioSum, step);
  }

  // merge lowestPrioValues and higherPrioValues
  return priorities.map(prio => (prio === lowestPriority ? lowestPrioValues.shift() : higherPrioValues.shift()));
}
