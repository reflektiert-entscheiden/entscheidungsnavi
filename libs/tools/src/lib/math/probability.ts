import { sum } from 'lodash';

/**
 *  returns a function, that maps a number in [0,1] to a number in [min, max]
 */
export function scaleFunction(min: number, max: number) {
  return (val: number) => val * (max - min) + min;
}

/**
 * Function, that maps a value in [0,1]
 * to [center - deviation, center + deviation] or [min, max], whichever is smaller.
 */
export function scaleToRangeFunction(center: number, deviation: number, min?: number, max?: number): (val: number) => number {
  min = min == null ? center - deviation : Math.max(center - deviation, min);
  max = max == null ? center + deviation : Math.min(center + deviation, max);
  return scaleFunction(min, max);
}

/**
 * Like scaleToRangeFunction, but scale the upper and lower part separately
 * ([0, 0.5) is mapped to [min, center) and [0.5, 1] to [center, max] (if center is in [min, max])).
 */
export function scaleSplitFunction(center: number, deviation: number, min?: number, max?: number): (val: number) => number {
  // Default to scaleToRangeFunction, if center is not in [min, max]
  if (min == null || max == null || center <= min || center >= max) {
    return scaleToRangeFunction(center, deviation, min, max);
  } else {
    const newMin = min == null ? center - deviation : Math.max(center - deviation, min);
    const newMax = max == null ? center + deviation : Math.min(center + deviation, max);
    return (val: number) => (val < 0.5 ? newMin + 2 * val * (center - newMin) : center + 2 * (val - 0.5) * (newMax - center));
  }
}

/**
 *
 * @param mid - the exact value
 * @param precision - the maximal deviation
 * @returns function, that maps [0,1] to [mid - precision, mid + precision] (restricted to [1, 250])
 */
export function mapToWeightFunction(mid: number, precision: number) {
  return scaleSplitFunction(mid, precision, 1, 1000);
}

/**
 *
 * @param preciseValues - the precise values
 * @param precision - the maximal deviation from the precise values
 * @returns function, that maps an array of values in [0,1] to an array of adjusted probabilities
 */
export function mapToProbsFunc(preciseValues: number[], precision: number): (values: number[]) => number[] {
  if (!precision) {
    return () => preciseValues;
  }
  const leftBoundaries = preciseValues.map(val => Math.max(0, val - precision));
  const leftRanges = preciseValues.map((val: number, idx: number) => val - leftBoundaries[idx]);
  const rightRanges = preciseValues.map(val => Math.min(100, val + precision) - val);
  const maxSum = 100 - leftBoundaries.reduce((sum: number, val: number) => sum + val);
  const mapFunction = (val: number, idx: number) => {
    if (val <= 0.5) {
      return 2 * val * leftRanges[idx];
    } else {
      return leftRanges[idx] + 2 * (val - 0.5) * rightRanges[idx];
    }
  };
  const sumFunction = (sum: number, val: number) => sum + val;

  return (randomValues: number[]): number[] => {
    // return an array of probabilities according to the provided random values
    if (randomValues.length !== leftBoundaries.length) {
      throw new Error('array length does not match');
    } else if (randomValues.reduce(sumFunction) < randomValues.length / 2) {
      throw new Error('the sum of the input array must be at least the half of its length');
    }
    const unnormalizedVals = randomValues.map(mapFunction);
    // return the normalized values
    const uvSum = unnormalizedVals.reduce((sum: number, val: number) => sum + val);
    return unnormalizedVals.map((val: number, idx: number) => (val * maxSum) / uvSum + leftBoundaries[idx]);
  };
}

export function mapToRelativeProbsFunc(preciseValues: number[], precision?: number): (values: number[]) => number[] {
  if (!precision) {
    // Composite Influence Factors have no precision, so we can return the precise values directly
    return () => preciseValues.slice();
  }
  const length = preciseValues.length;
  // get the maximal deviation ('epsilon') from the precise value (depends on the distance to 0%/100%, max for precise value = 50%)
  const epsilons = preciseValues.map(val => (precision * Math.min(100 - val, val)) / 100);
  // create an array of indexes, sorted by epsilon in ascending order
  const orderedIdxs: number[] = epsilons
    .map((val: number, idx: number) => [val, idx])
    .sort((a, b) => a[0] - b[0])
    .map(a => a[1]);
  const orderedValues = orderedIdxs.map(idx => preciseValues[idx]);
  const orderedEps = orderedIdxs.map(idx => epsilons[idx]);

  const leftBoundaries = orderedValues.map((val, idx) => val - orderedEps[idx]);
  const rightBoundaries = orderedValues.map((val, idx) => val + orderedEps[idx]);

  // minimum and maximum values, that may remain after iteration i (sum of the following boundaries)
  const minRemain: number[] = [],
    maxRemain: number[] = [];
  minRemain[length - 1] = 0;
  maxRemain[length - 1] = 0;
  for (let i = length - 2, prevMin = 0, prevMax = 0; i >= 0; i--) {
    prevMin = minRemain[i] = prevMin + leftBoundaries[i + 1];
    prevMax = maxRemain[i] = prevMax + rightBoundaries[i + 1];
  }

  // get the minimal/maximal possible value for the iteration so that all constraints can still be satisfied
  // (left/right yellow field in Excel)
  const getMin = (remaining: number, iteration: number) => Math.max(leftBoundaries[iteration], remaining - maxRemain[iteration]);
  const getMax = (remaining: number, iteration: number) => Math.min(rightBoundaries[iteration], remaining - minRemain[iteration]);

  // build a map to get the original idx
  const idxMap = new Map(orderedIdxs.map((unordered, ordered) => [ordered, unordered] as [number, number]));

  return (randomValues: number[]): number[] => {
    // return an array of probabilities according to the provided random values
    if (randomValues.length < length - 1) {
      throw new Error('not enough random values provided');
    }

    const res = new Array<number>(length);
    let remaining = 100;

    for (let i = 0; i < length; i++) {
      const min = getMin(remaining, i),
        max = getMax(remaining, i);
      let val: number;
      if (i === length - 1) {
        // use (100 - sum) instead of 'remaining' to prevent rounding errors (e.g. 100.00000000000001)
        val = 100 - sum(res);
      } else if (min === max) {
        val = min;
      } else {
        val = min + randomValues[i] * (max - min);
      }
      res[idxMap.get(i)] = val;
      remaining -= val;
    }

    return res;
  };
}

/**
 *
 * @param length - length of the array returned
 * @returns array with "length" random numbers
 */
export function getRandomArray(length: number): number[] {
  return Array.from({ length: length }, () => Math.random());
}

/**
 *
 * @param length - length of the array returned
 * @returns array with "length" random numbers and a sum of at least length/2
 */
export function getSpecialRandomArray(length: number): number[] {
  let array;
  do {
    array = getRandomArray(length);
  } while (array.reduce((sum: number, val: number) => sum + val) < length / 2);
  return array;
}

/**
 *
 * @param arrayCount - number of inner arrays
 * @param arrayLength - length of the inner arrays
 * @returns array of "arrayCount" arrays with "arrayLength" random values each
 */
export function getRandomArrays(arrayCount: number, arrayLength: number): number[][] {
  const arrays: number[][] = [];
  while (arrays.length < arrayCount) {
    // generate (arrayCount - arrays.length) random arrays,
    // but append only those with a sum >= arrayLength / 2
    arrays.push(getSpecialRandomArray(arrayLength));
  }
  return arrays;
}
