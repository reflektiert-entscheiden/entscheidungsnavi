import {
  getRandomArray,
  getRandomArrays,
  getSpecialRandomArray,
  mapToProbsFunc,
  mapToRelativeProbsFunc,
  mapToWeightFunction,
  scaleFunction,
  scaleSplitFunction,
  scaleToRangeFunction,
} from './probability';

describe('Probability', () => {
  it('scaleFunction(0, 10)', () => {
    const f = scaleFunction(0, 10);
    expect(f(0)).toBe(0);
    expect(f(0.05)).toBe(0.5);
    expect(f(0.5)).toBe(5);
    expect(f(1)).toBe(10);
  });

  it('scaleToRangeFunction(0, 5)', () => {
    const f = scaleToRangeFunction(0, 5);
    expect(f(0)).toBe(-5);
    expect(f(0.05)).toBe(-4.5);
    expect(f(0.5)).toBe(0);
    expect(f(1)).toBe(5);
  });

  it('scaleToRangeFunction(0, 5, -3, 4)', () => {
    const f = scaleToRangeFunction(1, 6, -4, 5);
    expect(f(0)).toBe(-4);
    expect(f(0.05)).toBe(-3.55);
    expect(f(0.5)).toBe(0.5);
    expect(f(1)).toBe(5);
  });

  it('scaleSplitFunction(0, 5)', () => {
    const f = scaleSplitFunction(0, 5);
    expect(f(0)).toBe(-5);
    expect(f(0.05)).toBe(-4.5);
    expect(f(0.5)).toBe(0);
    expect(f(1)).toBe(5);
  });

  it('scaleSplitFunction(0, 5, -3, 4)', () => {
    const f = scaleSplitFunction(1, 6, -4, 5);
    expect(f(0)).toBe(-4);
    expect(f(0.05)).toBe(-3.5);
    expect(f(0.5)).toBe(1);
    expect(f(1)).toBe(5);
  });

  it('mapToWeightFunction(10, 20)', () => {
    const f = mapToWeightFunction(10, 20);
    expect(f(0)).toBe(1);
    expect(f(0.5)).toBe(10);
    expect(f(1)).toBe(30);
  });

  it('mapToWeightFunction(990, 20)', () => {
    const f = mapToWeightFunction(990, 20);
    expect(f(0)).toBe(970);
    expect(f(0.5)).toBe(990);
    expect(f(1)).toBe(1000);
  });

  describe('mapToProbsFunc', () => {
    it('Test 1', () => {
      const f = mapToProbsFunc([10, 15, 15, 60], 10);
      const res = f([0, 0, 1, 1]);
      expect(res[0]).toBe(0);
      expect(res[1]).toBe(5);
      expect(res[2]).toBeCloseTo(25);
      expect(res[3]).toBe(70);
    });

    it('Test 2', () => {
      const f = mapToProbsFunc([10, 10, 20, 60], 10);
      const res = f([0, 0.5, 0.5, 1]);
      expect(res[0]).toBe(0);
      expect(res[1]).toBe(10);
      expect(res[2]).toBeCloseTo(20);
      expect(res[3]).toBe(70);
    });

    it('normalize', () => {
      const f = mapToProbsFunc([10, 15, 20, 25, 30], 10);
      const res = f([0.5, 0.5, 1, 1, 1]);
      expect(res[0]).toBe(6.25);
      expect(res[1]).toBe(11.25);
      expect(res[2]).toBeCloseTo(22.5);
      expect(res[3]).toBe(27.5);
      expect(res[4]).toBe(32.5);
    });

    it('sum Error', () => {
      const f = mapToProbsFunc([10, 15, 20, 25, 30], 10);
      expect(() => f([0, 0.5, 0.5, 0.5, 0.5])).toThrow();
      expect(() => f([0, 0, 0, 1, 1])).toThrow();
    });

    it('length Error', () => {
      const f = mapToProbsFunc([10, 15, 20, 25, 30], 10);
      expect(() => f([1, 1, 1, 1])).toThrow();
      expect(() => f([0.5, 0.5, 0.5, 0.5, 0.5, 0.5])).toThrow();
    });
  });

  describe('mapToRelativeProbsFunc', () => {
    it('precision = 0', () => {
      const f = mapToRelativeProbsFunc([10, 20, 30, 40], 0);
      const res = f([0.5, 0.5, 0.5, 0.5]);
      expect(res[0]).toBe(10);
      expect(res[1]).toBe(20);
      expect(res[2]).toBe(30);
      expect(res[3]).toBe(40);
    });

    it('precision = 0 - unordered', () => {
      const f = mapToRelativeProbsFunc([40, 20, 10, 30], 0);
      const res = f([0.5, 0.5, 0.5, 0.5]);
      expect(res[0]).toBe(40);
      expect(res[1]).toBe(20);
      expect(res[2]).toBe(10);
      expect(res[3]).toBe(30);
    });

    it('test 1', () => {
      const f = mapToRelativeProbsFunc([10, 20, 30, 40], 50);
      const res = f([0, 0, 0, 0.5]);
      expect(res[0]).toBe(5);
      expect(res[1]).toBe(10);
      expect(res[2]).toBe(25);
      expect(res[3]).toBe(60);
    });

    it('test 1 - unordered', () => {
      const f = mapToRelativeProbsFunc([30, 10, 40, 20], 50);
      const res = f([0, 0, 0, 0.5]);
      expect(res[0]).toBe(25);
      expect(res[1]).toBe(5);
      expect(res[2]).toBe(60);
      expect(res[3]).toBe(10);
    });

    it('test 2', () => {
      const f = mapToRelativeProbsFunc([10, 20, 30, 40], 50);
      const res = f([1, 1, 1, 0.5]);
      expect(res[0]).toBe(15);
      expect(res[1]).toBe(30);
      expect(res[2]).toBe(35);
      expect(res[3]).toBe(20);
    });

    it('test 2 - unordered', () => {
      const f = mapToRelativeProbsFunc([10, 20, 30, 40], 50);
      const res = f([1, 1, 1, 0.5]);
      expect(res[0]).toBe(15);
      expect(res[1]).toBe(30);
      expect(res[2]).toBe(35);
      expect(res[3]).toBe(20);
    });

    it('test 3', () => {
      const f = mapToRelativeProbsFunc([10, 20, 30, 40], 50);
      const res = f([0.5, 0.5, 0.5, 0.5]);
      expect(res[0]).toBe(10);
      expect(res[1]).toBe(20);
      expect(res[2]).toBe(30);
      expect(res[3]).toBe(40);
    });

    it('test 4', () => {
      const f = mapToRelativeProbsFunc([30, 20, 35, 15], 50);
      const res = f([0.2, 0.4, 0.6, 0.5]);
      expect(res[0]).toBe(34.6);
      expect(res[1]).toBe(18);
      expect(res[2]).toBe(36.9);
      expect(res[3]).toBe(10.5);
    });

    it('test 4 - unordered', () => {
      const f = mapToRelativeProbsFunc([35, 20, 15, 30], 50);
      const res = f([0.2, 0.4, 0.6, 0.5]);
      expect(res[0]).toBe(36.9);
      expect(res[1]).toBe(18);
      expect(res[2]).toBe(10.5);
      expect(res[3]).toBe(34.6);
    });

    it('test 5', () => {
      const f = mapToRelativeProbsFunc([35, 20, 15, 30], 50);
      const res = f([1 / 7, 1 / 3, 1 / 11, 0.5]);
      expect(res[0]).toBeCloseTo(50.335498);
      expect(res[1]).toBeCloseTo(16.666666);
      expect(res[2]).toBeCloseTo(9.642857);
      expect(res[3]).toBeCloseTo(23.354978);
      expect(res.reduce((prev, val) => prev + val, 0)).toBe(100);
    });
  });

  it('getRandomArray', () => {
    const a = getRandomArray(100);
    expect(a.length).toBe(100);
    expect(a.every((val: number) => val >= 0 && val <= 1)).toBeTruthy();
  });

  it('getRandomArray Avg in (0.4,0.6) (could fail in rare cases)', () => {
    const a = getRandomArray(100);
    const avg = a.reduce((sum: number, val: number) => sum + val) / 100;
    expect(avg > 0.4 && avg < 0.6).toBeTruthy();
  });

  it('getSpecialRandomArray', () => {
    const a = getSpecialRandomArray(100);
    expect(a.length).toBe(100);
    expect(a.every((val: number) => val >= 0 && val <= 1)).toBeTruthy();
    const sum = a.reduce((currSum: number, val: number) => currSum + val);
    expect(sum >= 50).toBeTruthy();
  });

  describe('getRandomArrays', () => {
    let a: number[][];

    beforeEach(() => (a = getRandomArrays(20, 10)));

    it('count', () => {
      expect(a.length).toBe(20);
    });

    it('length of entries', () => {
      expect(a.every((arr: number[]) => arr.length === 10)).toBeTruthy();
    });

    it('sum of entries', () => {
      const sumFunction = (sum: number, val: number) => sum + val;
      expect(a.every((arr: number[]) => arr.reduce(sumFunction) >= 5)).toBeTruthy();
    });
  });
});
