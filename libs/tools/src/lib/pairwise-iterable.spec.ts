import { pairwiseIterable } from './pairwise-iterable';

describe('pairwiseIterable()', () => {
  it('returns all pairs', () => {
    const generator = pairwiseIterable([0, 1, 2, 3]);
    const list = Array.from(generator);

    expect(list).toEqual([
      [0, 1],
      [1, 2],
      [2, 3],
    ]);
  });

  it('works for empty iterables', () => {
    const generator = pairwiseIterable([]);

    expect(Array.from(generator)).toHaveLength(0);
  });
});
