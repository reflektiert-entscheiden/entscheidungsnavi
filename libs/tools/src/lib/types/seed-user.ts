export type SeedUser = {
  email: string;
  password: string;
  name: string;
};

export const SEED_USERS: SeedUser[] = [
  {
    email: 'benutzer@entscheidungsnavi.de',
    password: '12345678',
    name: 'Benutzer',
  },
  {
    email: 'events@entscheidungsnavi.de',
    password: '12345678',
    name: 'Event Manager',
  },
  {
    email: 'quickstart@entscheidungsnavi.de',
    password: '12345678',
    name: 'Quickstart Manager',
  },
  {
    email: 'admin@entscheidungsnavi.de',
    password: '12345678',
    name: 'Admin',
  },
];
