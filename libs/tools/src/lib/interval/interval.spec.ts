import { Interval } from './interval';

describe('Interval', () => {
  describe('normalizePoint()', () => {
    it('works for basic intervals', () => {
      const interval = new Interval(0, 7);

      expect(interval.normalizePoint(0)).toBeCloseTo(0);
      expect(interval.normalizePoint(5)).toBeCloseTo(5 / 7);
      expect(interval.normalizePoint(7)).toBeCloseTo(1);
    });

    it('works for intervals that do not contain 0', () => {
      const interval = new Interval(1, 10);

      expect(interval.normalizePoint(1)).toBeCloseTo(0);
      expect(interval.normalizePoint(5)).toBeCloseTo(4 / 9);
      expect(interval.normalizePoint(10)).toBeCloseTo(1);
    });

    it('works for inverted intervals', () => {
      const interval = new Interval(10, 5);

      expect(interval.normalizePoint(10)).toBeCloseTo(0);
      expect(interval.normalizePoint(7)).toBeCloseTo(3 / 5);
      expect(interval.normalizePoint(5)).toBeCloseTo(1);
    });

    it('works for inverted negative intervals', () => {
      const interval = new Interval(-5, -10);

      expect(interval.normalizePoint(-5)).toBeCloseTo(0);
      expect(interval.normalizePoint(-7)).toBeCloseTo(2 / 5);
      expect(interval.normalizePoint(-10)).toBeCloseTo(1);
    });
  });
});
