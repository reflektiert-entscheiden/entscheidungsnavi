import { MonoTypeOperatorFunction, tap } from 'rxjs';

export function logError<T>(): MonoTypeOperatorFunction<T> {
  return tap({
    error: error => {
      const errMsg = error.message || error.toString();
      console.error(errMsg);
    },
  });
}
