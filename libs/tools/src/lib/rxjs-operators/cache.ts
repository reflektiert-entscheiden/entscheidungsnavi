import { Observable, OperatorFunction, map, shareReplay, switchAll } from 'rxjs';

/**
 * This RxJS operator implements a resettable cache for a second-order observable.
 *
 * Any time the source emits, the inner data is cleared and will not be emitted to new subscribers.
 * Any time a new subscriber subscribes, they receive the latest data (similar to {@link shareReplay}),
 * after the last reset.
 *
 * @returns An RxJS operator function
 */
export function cachedSwitchAll<T>(): OperatorFunction<Observable<T>, T> {
  return source$ => {
    return source$.pipe(
      map(innerObs$ => innerObs$.pipe(shareReplay(1))),
      shareReplay(1),
      switchAll(),
    );
  };
}
