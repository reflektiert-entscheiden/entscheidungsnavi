import { Type } from '@angular/core';

export function typeGuardFor<T>(type: Type<T>) {
  return (value: unknown): value is T => value instanceof type;
}
