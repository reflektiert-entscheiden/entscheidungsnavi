/**
 * Creates a new iterable from an input that iterates over value pairs.
 *
 * @example
 * ```
 * for(const value of pairwiseIterable([0, 1, 2])) {
 *   console.log(value);
 * }
 *
 * // Output:
 * // [0, 1]
 * // [1, 2]
 * ```
 *
 * @param input - The input iterable that is transformed
 */
export function* pairwiseIterable<T>(input: Iterable<T>) {
  let previous: T;

  for (const next of input) {
    if (previous != null) {
      yield [previous, next] satisfies [T, T];
    }

    previous = next;
  }
}
