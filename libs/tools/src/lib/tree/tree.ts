import { isEqual } from 'lodash';
import { LocationSequence } from './location-sequence';

/**
 * A simple tree interface.
 */
export interface ITree<T> {
  value: T;
  children: Array<ITree<T>>;
}

/**
 * An implementation of {@link ITree}.
 */
export class Tree<T> implements ITree<T> {
  /**
   * Creates a new tree object from another tree. Beware that this does not clone tree values.
   *
   * @param tree - The tree to clone
   * @returns An instance of tree
   */
  static from<T>(tree: ITree<T>): Tree<T> {
    return new Tree(
      tree.value,
      tree.children.map(child => Tree.from(child)),
    );
  }

  constructor(
    public value: T,
    public children: Array<Tree<T>> = [],
  ) {}

  /**
   * Get a child tree identified by a location sequence.
   *
   * @param location - The location of the element
   * @returns The child tree or null, if the location is invalid
   */
  getNode(location: LocationSequence): Tree<T> {
    // eslint-disable-next-line @typescript-eslint/no-this-alias
    let current: Tree<T> = this;

    for (const childIndex of location.value) {
      if (current.children.length <= childIndex) return null;

      current = current.children[childIndex];
    }

    return current;
  }

  removeNode(location: LocationSequence) {
    return this.getNode(location.getParent()).children.splice(location.value.at(-1), 1)[0];
  }

  insertNode(location: LocationSequence, node: Tree<T>) {
    this.getNode(location.getParent()).children.splice(location.value.at(-1), 0, node);
  }

  /**
   * Finds the next node of the node identified by {@link element}.
   *
   * @param element - The location sequence identifiying the node
   * @returns The location sequence of the next node or null, if there is none
   */
  findNextNode(element: LocationSequence): LocationSequence {
    for (let searchDepth = 1; searchDepth <= element.value.length; searchDepth++) {
      const ancestor = element.getAncestor(searchDepth);
      const childIndex = element.value.at(-searchDepth);

      const ancestorNode = this.getNode(ancestor);

      if (childIndex < ancestorNode.children.length - 1) {
        const sequence = [childIndex + 1];
        let nextNode = ancestorNode.children[childIndex + 1];

        while (nextNode.children.length > 0 && sequence.length < searchDepth) {
          sequence.push(0);
          nextNode = nextNode.children[0];
        }

        return new LocationSequence([...ancestor.value, ...sequence]);
      }
    }

    return null;
  }

  /**
   * Finds the previous node of the node identified by {@link element}.
   *
   * @param element - The location sequence identifiying the node
   * @returns The location sequence of the previous node or null, if there is none
   */
  findPreviousNode(element: LocationSequence): LocationSequence {
    for (let searchDepth = 1; searchDepth <= element.value.length; searchDepth++) {
      const ancestor = element.getAncestor(searchDepth);
      const childIndex = element.value.at(-searchDepth);

      const ancestorNode = this.getNode(ancestor);

      if (childIndex > 0) {
        const sequence = [childIndex - 1];
        let previousNode = ancestorNode.children[childIndex - 1];

        while (previousNode.children.length > 0 && sequence.length < searchDepth) {
          sequence.push(previousNode.children.length - 1);
          previousNode = previousNode.children[previousNode.children.length - 1];
        }

        return new LocationSequence([...ancestor.value, ...sequence]);
      }
    }

    return null;
  }

  /**
   * Creates a new tree by applying the given mapper function to each node.
   *
   * @param mapper - The mapper function
   * @returns A new tree
   */
  map<TN>(mapper: (value: T) => TN): Tree<TN> {
    return new Tree(
      mapper(this.value),
      this.children.map(child => child.map(mapper)),
    );
  }

  /**
   * Return a new tree that only contains nodes that satisfy the given predicate.
   *
   * Subtrees of filtered nodes are removed completely.
   *
   * @param predicate - The predicate function
   */
  filter(predicate: (value: T, location: LocationSequence) => boolean): Tree<T> {
    return new Tree(
      this.value,
      this.children
        .filter((child, childIndex) => predicate(child.value, new LocationSequence([childIndex])))
        .map((child, childIndex) => {
          const childLocation = new LocationSequence([childIndex]);

          const childPredicate = (value: T, location: LocationSequence) => {
            return predicate(value, childLocation.getChild(location));
          };

          return child.filter(childPredicate);
        }),
    );
  }

  /**
   * Returns a flat representation of the tree according to depth-first search.
   */
  flat(): Array<T> {
    return [this.value, ...this.children.flatMap(child => child.flat())];
  }

  /**
   * Returns the number of nodes in this tree, including the root.
   */
  size(): number {
    return this.children.reduce((sum, child) => sum + child.size(), 1);
  }

  /**
   * Returns the depth of the tree, i.e. the length of the longest path from the root to a leaf.
   *
   * @example
   * A single node without children has depth 0.
   */
  depth(): number {
    return this.children.reduce((max, child) => Math.max(max, child.depth()), -1) + 1;
  }

  /**
   * Checks if every element in the tree satisfies a given condition.
   *
   * @param condition - The condition function to be applied to each element.
   * @returns - True if every element satisfies the condition, false otherwise.
   */
  every(condition: (value: T) => boolean): boolean {
    return condition(this.value) && this.children.every(child => child.every(condition));
  }

  /**
   * Compares the current tree with another tree for equality.
   *
   * @param otherTree - The tree to compare with the current tree.
   * @param comparator - Optional comparator function to compare node values. Defaults to lodash's `isEqual`.
   * @returns - Returns true if the trees are equal, false otherwise.
   */
  equals(otherTree: ITree<T>, comparator: (a: T, b: T) => boolean = isEqual): boolean {
    return (
      comparator(this.value, otherTree.value) &&
      this.children.length === otherTree.children.length &&
      this.children.every((child, childIndex) => child.equals(otherTree.children[childIndex]))
    );
  }

  /**
   * Iterates over all nodes in the tree in depth-first order.
   *
   * @param callback - The callback function to be called for each node. The first argument is the node,
   *                   the second argument is the location of the node relative to this tree.
   */
  iterate(callback: (node: Tree<T>, location: LocationSequence) => void) {
    iterateRec(this, callback);

    function iterateRec(
      currentNode: Tree<T>,
      callback: (node: Tree<T>, location: LocationSequence) => void,
      location: LocationSequence = new LocationSequence([]),
    ) {
      callback(currentNode, location);

      currentNode.children.forEach((child, index) => {
        iterateRec(child, callback, location.getChild(index));
      });
    }
  }
}
