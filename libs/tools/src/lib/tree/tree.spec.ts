import { Tree } from './tree';

describe('Tree', () => {
  describe('depth()', () => {
    it('assigns 0 depth to a single node', () => {
      const tree = new Tree(0);
      expect(tree.depth()).toEqual(0);
    });

    it('assigns 1 depth to a tree with a single child', () => {
      const tree = new Tree(0, [new Tree(1)]);
      expect(tree.depth()).toEqual(1);
    });
  });
  describe('iterate()', () => {
    it('iterates over all nodes in depth-first order', () => {
      const tree = new Tree(0, [new Tree(1, [new Tree(2), new Tree(3)]), new Tree(4)]);

      const expectedOrder = [0, 1, 2, 3, 4]; // Expected order of nodes in depth-first traversal
      const actualOrder: number[] = [];

      tree.iterate(node => {
        actualOrder.push(node.value);
      });

      expect(actualOrder).toEqual(expectedOrder);
    });
  });
});
