import { isEqual } from 'lodash';

/**
 * Identifies a node in the tree.
 *
 * An empty value (`[]`) denotes the root of the tree.
 *
 * @example
 * A sequence of `[2, 1]` means that we
 * - start at the root,
 * - take its third child (2),
 * - and then in turn its second child (1).
 */
export class LocationSequence {
  /**
   * Check if the given locations can be grouped.
   *
   * @param locations - The candidates to be grouped.
   * @returns - true if the given locations can be grouped, false otherwise.
   */
  static canGroup(locations: LocationSequence[]) {
    // There must be at least one node and no node may be the root
    if (locations.length < 1 || locations[0].isRoot()) return false;

    // All the nodes must be siblings
    for (let i = 1; i < locations.length; i++) {
      if (!locations[0].isSiblingOf(locations[i])) return false;
    }

    return true;
  }

  /**
   * Filters the given array to only include objects that do not have any parent
   * LocationSequence objects in the array.
   *
   * @param locations - The locations to filter
   * @returns - The filtered locations where no location has a parent in the list
   */
  static filterChildren(locations: LocationSequence[]): LocationSequence[] {
    return locations.filter(loc => locations.find(parentLoc => parentLoc.isAncestorOf(loc)) == null);
  }

  constructor(public readonly value: ReadonlyArray<number>) {}

  getLevel() {
    return this.value.length;
  }

  isRoot() {
    return this.getLevel() === 0;
  }

  getParent() {
    return this.getAncestor(1);
  }

  getAncestor(level: number) {
    if (this.value.length < level) {
      throw new Error(`the node does not have an ${level}th ancestor`);
    }

    return new LocationSequence(this.value.slice(0, -level));
  }

  getChild(childIndexOrLocation: number | LocationSequence) {
    if (typeof childIndexOrLocation === 'number') {
      return new LocationSequence([...this.value, childIndexOrLocation]);
    } else {
      return new LocationSequence([...this.value, ...childIndexOrLocation.value]);
    }
  }

  getNextSibling() {
    return new LocationSequence([...this.value.slice(0, -1), this.value.at(-1) + 1]);
  }

  equals(candidate: LocationSequence) {
    return isEqual(this.value, candidate.value);
  }

  /**
   * Checks if we are a sibling of {@link candidate}.
   *
   * @param candidate - The sibling candidate
   * @returns true iff we are a siblings
   */
  isSiblingOf(candidate: LocationSequence) {
    return this.value.length === candidate.value.length && isEqual(this.value.slice(0, -1), candidate.value.slice(0, -1));
  }

  /**
   * Checks if we are a parent of {@link candidate}.
   *
   * @param candidate - The potential child
   * @returns true iff we are a parent of the given node
   */
  isParentOf(candidate: LocationSequence) {
    return this.value.length === candidate.value.length - 1 && isEqual(this.value, candidate.value.slice(0, this.value.length));
  }

  /**
   * Checks if we are an ancestor of the given {@link candidate} node.
   *
   * @param candidate - The potential descendant
   * @returns true iff we are an ancestor of the given node
   */
  isAncestorOf(candidate: LocationSequence) {
    return this.value.length < candidate.value.length && isEqual(this.value, candidate.value.slice(0, this.value.length));
  }

  /**
   * Checks if we are a descendant of the given {@link candidate} node.
   *
   * @param candidate - The potential ancestor
   * @returns true iff we are a descendant of the given node
   */
  isDescendantOf(candidate: LocationSequence) {
    return candidate.isAncestorOf(this);
  }

  /**
   * Return a new {@link LocationSequence} that is adjusted for the deletion of {@link node} from the tree.
   *
   * @param node - The node that is deleted
   * @returns An adjusted {@link LocationSequence}
   */
  adjustForRemovalOf(node: LocationSequence) {
    const nodeParent = node.getParent();

    if (nodeParent.isAncestorOf(this) && this.value[nodeParent.value.length] > node.value[nodeParent.value.length]) {
      return new LocationSequence([
        ...nodeParent.value,
        this.value[nodeParent.value.length] - 1,
        ...this.value.slice(nodeParent.value.length + 1),
      ]);
    }

    return this;
  }

  /**
   * Return a new {@link LocationSequence} that is adjusted for the insertion of {@link node} into the tree.
   *
   * @param node - The node that is inserted
   * @returns An adjusted {@link LocationSequence}
   */
  adjustForInsertionOf(node: LocationSequence) {
    const nodeParent = node.getParent();

    if (nodeParent.isAncestorOf(this) && this.value[nodeParent.value.length] > node.value[nodeParent.value.length]) {
      return new LocationSequence([
        ...nodeParent.value,
        this.value[nodeParent.value.length] + 1,
        ...this.value.slice(nodeParent.value.length + 1),
      ]);
    }

    return this;
  }
}
