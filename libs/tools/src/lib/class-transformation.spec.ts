import { IsNumber, IsString } from 'class-validator';
import { lastValueFrom, of } from 'rxjs';
import { transformAndValidate } from './class-transformation';

class TestDTO {
  @IsString()
  name: string;

  @IsNumber()
  age: number;
}

describe('transformAndValidate()', () => {
  it('should accept valid object', async () => {
    const inputObservable = of({ name: 'John Doe', age: 42 });

    const outputObservable = inputObservable.pipe(transformAndValidate(TestDTO));

    const outputPromise = lastValueFrom(outputObservable);

    await expect(outputPromise).resolves.toBeInstanceOf(TestDTO);
  });

  it('should reject invalid object', async () => {
    const inputObservable = of({ name: 'John Doe', age: '42' });

    const outputObservable = inputObservable.pipe(transformAndValidate(TestDTO));

    const outputPromise = lastValueFrom(outputObservable);

    await expect(outputPromise).rejects.toThrow();
  });

  it('should accept valid array', async () => {
    const inputObservable = of([
      { name: 'John Doe', age: 42 },
      { name: 'Jane Doe', age: 35 },
    ]);

    const outputObservable = inputObservable.pipe(transformAndValidate(TestDTO));

    const outputPromise = lastValueFrom(outputObservable);

    await expect(outputPromise).resolves.toEqual([expect.any(TestDTO), expect.any(TestDTO)]);
  });

  it('should reject invalid array', async () => {
    const inputObservable = of([
      { name: 'John Doe', age: 42 },
      { name: 'Jane Doe', age: '35' },
    ]);

    const outputObservable = inputObservable.pipe(transformAndValidate(TestDTO));

    const outputPromise = lastValueFrom(outputObservable);

    await expect(outputPromise).rejects.toThrow();
  });
});
