///////////////////////////////////////////////////////////////////////////////
// Event Types
///////////////////////////////////////////////////////////////////////////////

import { User } from './user';
import { SortDirection } from './sort';
import { PaginatedList } from './pagination';

/**
 * Moderator facing event
 */
export interface Event {
  id: string;

  name: string;
  code?: string;
  startDate?: Date;
  endDate?: Date;

  owner: Pick<User, 'email'>;
  editors: Pick<User, 'email'>[];
  viewers: Pick<User, 'email'>[];

  projectRequirements: EventProjectRequirements;
  freeTextConfig: EventFreeTextConfig;
  questionnaire: QuestionnairePage[];
  questionnaireReleased: boolean;

  requireDataUsageAuthorization: boolean;

  createdAt: Date;
  updatedAt: Date;
}

/**
 * Some endpoints return the event with additional stats.
 */
export interface EventWithStats extends Event {
  registrationCount: number;
  submissionCount: number;
}

/**
 * User facing view of an event
 */
export interface UserEvent {
  id: string;
  name: string;
  startDate?: Date;
  endDate?: Date;
  projectRequirements: EventProjectRequirements;
  freeTextConfig: EventFreeTextConfig;
  questionnaire?: QuestionnairePage[]; // undefined when unreleased
  requireDataUsageAuthorization: boolean;
}

export interface EventProjectRequirements {
  requireDecisionStatement: boolean;
  requireObjectives: boolean;
  requireAlternatives: boolean;
  requireImpactModel: boolean;
  requireObjectiveWeights: boolean;
  requireDecisionQuality: boolean;
}

export interface EventFreeTextConfig {
  enabled: boolean;
  name: string;
  placeholder: string;
  minLength: number;
}

export interface EventRegistration {
  id: string;
  event?: UserEvent;
  submitted: boolean;
  freeText?: string;
  projectData?: string;
  user?: Partial<Pick<User, 'name' | 'email' | 'id'>>;
  questionnaireResponses?: QuestionnaireEntryResponse[][];
  createdAt: Date;
  updatedAt: Date;
}

export interface EventRegistrationFilter {
  query?: string;
  projectSubmitted?: boolean;
}

export const EVENT_REGISTRATION_SORT_BY = ['email', 'updatedAt', 'submitted', 'id'] as const;
export type EventRegistrationSortBy = (typeof EVENT_REGISTRATION_SORT_BY)[number];

export interface EventRegistrationSort {
  sortBy?: EventRegistrationSortBy;
  sortDirection?: SortDirection;
}

export type EventRegistrationList = PaginatedList<EventRegistration>;

///////////////////////////////////////////////////////////////////////////////
// Questionnaire
///////////////////////////////////////////////////////////////////////////////

export const QUESTIONNAIRE_ENTRY_TYPES = ['textBlock', 'textQuestion', 'numberQuestion', 'optionsQuestion', 'tableQuestion'] as const;
export type QuestionnaireEntryType = (typeof QUESTIONNAIRE_ENTRY_TYPES)[number];

export interface BaseQuestionnaireEntry {
  entryType: QuestionnaireEntryType;
}

export const TEXT_BLOCK_TYPES = ['body', 'caption'] as const;
export type TextBlockType = (typeof TEXT_BLOCK_TYPES)[number];

export interface TextBlockEntry extends BaseQuestionnaireEntry {
  entryType: 'textBlock';

  text: string;
  type: TextBlockType;
}

export interface FormFieldQuestionEntry extends BaseQuestionnaireEntry {
  question: string; // The actual question being asked
  label: string; // The label of the form field
}

export interface TextQuestionEntry extends FormFieldQuestionEntry {
  entryType: 'textQuestion';

  minLength?: number;
  maxLength?: number;
  pattern?: string;
}

export interface NumberQuestionEntry extends FormFieldQuestionEntry {
  entryType: 'numberQuestion';

  min?: number;
  max?: number;
  step?: number;
}

export const OPTIONS_QUESTION_DISPLAY_TYPES = ['dropdown', 'radio'] as const;
export type OptionsQuestionDisplayType = (typeof OPTIONS_QUESTION_DISPLAY_TYPES)[number];

export interface OptionsQuestionEntry extends FormFieldQuestionEntry {
  entryType: 'optionsQuestion';

  options: string[];
  displayType: OptionsQuestionDisplayType;
}

export interface TableQuestionEntry extends BaseQuestionnaireEntry {
  entryType: 'tableQuestion';

  options: string[];
  baseQuestion: string;
  subQuestions: string[];
}

export type QuestionnaireEntry = TextBlockEntry | TextQuestionEntry | NumberQuestionEntry | OptionsQuestionEntry | TableQuestionEntry;

export interface QuestionnairePage {
  entries: QuestionnaireEntry[];
}

export type QuestionnaireEntryResponse = number | number[] | string | null;
