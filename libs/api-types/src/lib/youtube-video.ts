import { Types } from 'mongoose';
export interface YoutubeVideo {
  id: string;
  name: string;
  youtubeId: string;
  startTime?: number;
  stepNumbers: number[];
  categoryIds: Types.ObjectId[];
  createdAt: Date;
  updatedAt: Date;
}

export interface VideoCategory {
  id: string;
  orderIdx: number;
  name: string;
  createdAt: Date;
  updatedAt: Date;
}
