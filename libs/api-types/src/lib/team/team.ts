import { User } from '../user';

export interface Team {
  id: string;
  name: string;
  members: readonly TeamMember[];
  createdAt: Date;
  updatedAt: Date;
  whiteboard: string;
  canTakeEditRightFrom: TeamConfigTargetGroup;
  editRightExpiresFor: TeamConfigTargetGroup;
  editToken: TeamEditToken;
  comments: TeamComment[];
  mainProjectData: string;
  importanceShares: number[];
  importanceShareNames: string[];
  lockedWeightTasks: boolean;
  lockedOpinionTasks: boolean;
}

export type TeamProjectInfo = Pick<Team, 'id' | 'name' | 'updatedAt'>;

export type TeamPartialUserInfo = Pick<User, 'id' | 'email' | 'name'>;

export interface TeamMember {
  id: string;
  user?: TeamPartialUserInfo;
  unreadComments: string[];
  tasks: TeamTask[];
  importance: number[];
  name?: string;
  createdAt: Date;
  updatedAt: Date;
  role: TeamRole;
  invite?: TeamInvite;
}

export interface TeamComment {
  id: string;
  objectId: string;
  authorMember: string;
  content: string;
  createdAt: Date;
}

export interface TeamEditToken {
  holder: string;
  expiresAt: Date;
}

export interface TeamAccessLog {
  editToken: TeamEditTokenLogEntry[];
}

export interface TeamEditTokenLogEntry {
  member: string;
  from: Date;
  to?: Date;
}

export type AlternativeOpinions = Record<string, { sentiment: boolean; comment: string } | Record<string, never>>;

export const TeamConfigTargetGroupValues = ['all', 'editor', 'none'] as const;

export type TeamConfigTargetGroup = (typeof TeamConfigTargetGroupValues)[number];

export interface TeamInviteInfo {
  teamName: string;
  inviterName: string;
}

export interface TeamInvite {
  email: string;
  inviteToken: string;
  inviterName: string;
}

export const TeamTaskTypes = ['objective-scale', 'objective-brainstorming', 'objective-weights', 'opinion'] as const;
export type TeamTaskType = (typeof TeamTaskTypes)[number];

export const TeamTaskSingletonMap: Record<TeamTaskType, boolean> = {
  'objective-scale': false,
  'objective-brainstorming': false,
  'objective-weights': true,
  opinion: true,
};

export interface TeamTaskBase {
  id: string;

  type: TeamTaskType;

  updatedAt: Date;
  createdAt: Date;

  submitted: boolean;
  applied: boolean;
}

export type TeamTask = TeamTaskObjectiveScale | TeamTaskObjectiveBrainstorming | TeamTaskObjectiveWeights | TeamTaskOpinion;

export interface TeamTaskObjectiveScale extends TeamTaskBase {
  type: 'objective-scale';
  data?: string;
  objectiveUUID: string;
}

export interface TeamTaskObjectiveBrainstorming extends TeamTaskBase {
  type: 'objective-brainstorming';
  data?: string;
}

export interface TeamTaskObjectiveWeights extends TeamTaskBase {
  type: 'objective-weights';

  weights: Record<string, number>;
}

export interface TeamTaskOpinion extends TeamTaskBase {
  type: 'opinion';

  opinions: AlternativeOpinions;
}

export const TEAM_EDIT_RIGHT_DURATION = 1000 * 60 * 15; // 15 Minutes

export const TeamRoles = ['owner', 'co-owner', 'editor', 'user'] as const;

export type TeamRole = (typeof TeamRoles)[number];

export function doesTeamMemberHaveOwnerRights(teamMember: Pick<TeamMember, 'role'>) {
  return teamMember.role === 'owner' || teamMember.role === 'co-owner';
}

export function doesTeamMemberHaveEditorRights(teamMember: Pick<TeamMember, 'role'>) {
  return doesTeamMemberHaveOwnerRights(teamMember) || teamMember.role === 'editor';
}
