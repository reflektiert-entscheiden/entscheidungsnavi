import { Exclude, Expose, Type } from 'class-transformer';
import { IsString } from 'class-validator';
import { TeamInvite } from '..';

@Exclude()
export class TeamInviteDto implements TeamInvite {
  @Expose()
  @IsString()
  email: string;

  @Expose()
  @IsString()
  inviteToken: string;

  @Expose()
  @Type(() => String)
  inviterName: string;
}
