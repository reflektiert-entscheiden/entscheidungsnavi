import { Exclude, Expose, Type } from 'class-transformer';
import { IsNotEmpty } from 'class-validator';
import { TeamComment } from '..';

@Exclude()
export class TeamCommentDto implements TeamComment {
  @Expose()
  @Type(() => String)
  id: string;

  @Expose()
  @IsNotEmpty()
  objectId: string;

  @IsNotEmpty()
  @Expose()
  @Type(() => String)
  authorMember: string;

  @IsNotEmpty()
  @Expose()
  content: string;

  @Expose()
  @Type(() => Date)
  createdAt: Date;
}
