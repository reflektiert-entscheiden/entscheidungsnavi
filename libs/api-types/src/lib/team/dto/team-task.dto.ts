import { Exclude, Expose, Type } from 'class-transformer';
import { IsBoolean, IsDate, IsDefined, IsIn, IsObject, IsOptional, IsString, ValidateNested } from 'class-validator';
import {
  AlternativeOpinions,
  TeamTaskBase,
  TeamTaskObjectiveBrainstorming,
  TeamTaskObjectiveScale,
  TeamTaskObjectiveWeights,
  TeamTaskOpinion,
  TeamTaskType,
  TeamTaskTypes,
} from '..';

// eslint-disable-next-line @typescript-eslint/naming-convention
export const TaskDiscriminator = () =>
  Type(() => AbstractTeamTaskDto, {
    keepDiscriminatorProperty: true,
    discriminator: {
      property: 'type',
      subTypes: [
        { value: TeamTaskObjectiveScaleDto, name: 'objective-scale' },
        { value: TeamTaskObjectiveBrainstormingDto, name: 'objective-brainstorming' },
        { value: TeamTaskObjectiveWeightsDto, name: 'objective-weights' },
        { value: TeamTaskOpinionDto, name: 'opinion' },
      ],
    },
  });

@Exclude()
export class AbstractTeamTaskDto implements TeamTaskBase {
  @Expose()
  @IsString()
  id: string;

  @Expose()
  @IsIn(TeamTaskTypes)
  type: TeamTaskType;

  @Expose()
  @IsDate()
  @Type(() => Date)
  createdAt: Date;

  @Expose()
  @IsDate()
  @Type(() => Date)
  updatedAt: Date;

  @Expose()
  @IsBoolean()
  submitted: boolean;

  @Expose()
  @IsBoolean()
  applied: boolean;
}

@Exclude()
export class TeamTaskObjectiveScaleDto extends AbstractTeamTaskDto implements TeamTaskObjectiveScale {
  @Expose()
  @IsString()
  objectiveUUID: string;

  @Expose()
  @IsString()
  @IsOptional()
  data?: string;

  override type: 'objective-scale';
}

@Exclude()
export class TeamTaskObjectiveBrainstormingDto extends AbstractTeamTaskDto implements TeamTaskObjectiveBrainstorming {
  @Expose()
  @IsString()
  @IsOptional()
  data?: string;

  override type: 'objective-brainstorming';
}

@Exclude()
export class TeamTaskObjectiveWeightsDto extends AbstractTeamTaskDto implements TeamTaskObjectiveWeights {
  override type: 'objective-weights';

  @Expose()
  @IsObject()
  weights: Record<string, number>;
}

@Exclude()
export class TeamTaskOpinionDto extends AbstractTeamTaskDto implements TeamTaskOpinion {
  override type: 'opinion';

  @Expose()
  @IsObject()
  opinions: AlternativeOpinions;
}

export class TaskWrapperDto {
  @IsDefined()
  @ValidateNested()
  @TaskDiscriminator()
  task: TeamTaskDto;
}

export type TeamTaskDto = TeamTaskObjectiveScaleDto | TeamTaskObjectiveBrainstormingDto | TeamTaskObjectiveWeightsDto | TeamTaskOpinionDto;
