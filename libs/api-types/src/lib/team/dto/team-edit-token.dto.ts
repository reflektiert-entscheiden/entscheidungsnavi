import { Exclude, Expose, Type } from 'class-transformer';
import { TeamEditToken } from '..';

@Exclude()
export class TeamEditTokenDto implements TeamEditToken {
  @Expose()
  @Type(() => String)
  holder: string;

  @Expose()
  @Type(() => Date)
  expiresAt: Date;
}
