import { Exclude, Expose, Type } from 'class-transformer';
import { IsArray, IsIn, IsNumber, IsOptional, IsString, ValidateNested } from 'class-validator';
import { TeamInviteDto, TeamMember, TeamRole, TeamRoles } from '..';
import { PartialUserInfoDto } from './partial-user-info.dto';
import {
  TeamTaskObjectiveScaleDto,
  TeamTaskObjectiveBrainstormingDto,
  TeamTaskObjectiveWeightsDto,
  TeamTaskOpinionDto,
  TaskDiscriminator,
} from './team-task.dto';

// eslint-disable-next-line @typescript-eslint/naming-convention
@Exclude()
export class TeamMemberDto implements TeamMember {
  @Expose()
  @Type(() => String)
  id: string;

  @Expose()
  @IsOptional()
  @Type(() => PartialUserInfoDto)
  @ValidateNested()
  user: PartialUserInfoDto;

  @Expose()
  @ValidateNested({ each: true })
  @TaskDiscriminator()
  tasks: (TeamTaskObjectiveScaleDto | TeamTaskObjectiveBrainstormingDto | TeamTaskObjectiveWeightsDto | TeamTaskOpinionDto)[];

  @Expose()
  @Type(() => String)
  @IsString({ each: true })
  unreadComments: string[];

  @Expose()
  @IsArray()
  @IsNumber({}, { each: true })
  importance: number[];

  @Expose()
  @IsOptional()
  @IsString()
  name: string;

  @Expose()
  @IsString()
  @IsIn(TeamRoles)
  role: TeamRole;

  @Expose()
  @Type(() => Date)
  createdAt: Date;

  @Expose()
  @Type(() => Date)
  updatedAt: Date;

  @Expose()
  @IsOptional()
  @ValidateNested()
  @Type(() => TeamInviteDto)
  invite: TeamInviteDto;

  get objectiveWeights() {
    return this.tasks.find((task): task is TeamTaskObjectiveWeightsDto => task.type === 'objective-weights')?.weights;
  }

  get alternativeOpinions() {
    return this.tasks.find((task): task is TeamTaskOpinionDto => task.type === 'opinion')?.opinions;
  }

  get isInvitee() {
    return !this.user && this.invite;
  }
}
