import { Exclude, Expose, Type } from 'class-transformer';
import { IsArray, IsBoolean, IsEnum, IsNotEmpty, IsOptional, IsString, ValidateNested } from 'class-validator';
import { Team, TeamConfigTargetGroupValues, TeamMember, doesTeamMemberHaveOwnerRights } from '..';
import { TeamMemberDto } from './team-member.dto';
import { TeamEditTokenDto } from './team-edit-token.dto';
import { TeamCommentDto } from './team-comment.dto';
import { TeamAccessLogDto } from './team-access-log.dto';

export type EditTokenInfo = { state: 'none' } | { state: 'expired'; expiredAt: Date } | { state: 'valid'; holder: TeamMemberDto };
export type TakeEditTokenInfo = { canTake: true } | { canTake: false; reason: 'no-editor' | 'already-holding' | 'cant-take-from-holder' };

@Exclude()
export class TeamDto implements Team {
  @Expose()
  @Type(() => String)
  id: string;

  @IsNotEmpty()
  @IsString()
  @Expose()
  name: string;

  @Expose()
  @Type(() => TeamMemberDto)
  @ValidateNested({ each: true })
  members: TeamMemberDto[];

  @Expose()
  @Type(() => TeamCommentDto)
  @ValidateNested({ each: true })
  comments: TeamCommentDto[];

  @Expose()
  @IsString()
  mainProjectData: string;

  @IsString()
  @Expose()
  whiteboard: string;

  @IsEnum(TeamConfigTargetGroupValues)
  @Expose()
  canTakeEditRightFrom: 'all' | 'editor' | 'none';

  @IsEnum(TeamConfigTargetGroupValues)
  @Expose()
  editRightExpiresFor: 'all' | 'editor' | 'none';

  @Expose()
  @Type(() => TeamEditTokenDto)
  @ValidateNested()
  @IsOptional()
  editToken: TeamEditTokenDto;

  @Expose({ groups: ['co-owner'] })
  @Type(() => TeamAccessLogDto)
  @ValidateNested()
  @IsOptional()
  accessLog: TeamAccessLogDto;

  @Expose()
  @IsArray()
  @Type(() => Number)
  importanceShares: number[];

  @Expose()
  @IsArray()
  @Type(() => String)
  importanceShareNames: string[];

  @Expose()
  @IsBoolean()
  lockedWeightTasks: boolean;

  @Expose()
  @IsBoolean()
  lockedOpinionTasks: boolean;

  @Expose()
  @Type(() => Date)
  createdAt: Date;

  @Expose()
  @Type(() => Date)
  updatedAt: Date;

  get hasAggregatedWeights() {
    return this.members.some(member => member.tasks.some(task => task.type === 'objective-weights'));
  }

  get editTokenInfo(): EditTokenInfo {
    if (!this.editToken) {
      return { state: 'none' };
    }

    const holderId = this.editToken.holder;
    const holderMember = this.members.find(member => member.id === holderId);

    if (!holderMember) {
      return { state: 'none' };
    }

    const canTokenExpireForHolder =
      this.editRightExpiresFor === 'all' || (this.editRightExpiresFor === 'editor' && holderMember.role === 'editor');

    if (canTokenExpireForHolder && this.editToken.expiresAt.getTime() < Date.now()) {
      return { state: 'expired', expiredAt: this.editToken.expiresAt };
    }

    const member = this.members.find(member => member.id === this.editToken.holder);

    return member ? { state: 'valid', holder: member } : { state: 'none' };
  }

  canMemberTakeEditToken(member: Pick<TeamMember, 'id' | 'role'>): TakeEditTokenInfo {
    if (member.role === 'user') {
      return { canTake: false, reason: 'no-editor' };
    }

    const currentTokenInfo = this.editTokenInfo;

    if (currentTokenInfo.state !== 'valid') {
      return { canTake: true };
    }

    if (member.id === currentTokenInfo.holder.id) {
      return { canTake: false, reason: 'already-holding' };
    }

    if (doesTeamMemberHaveOwnerRights(member)) {
      return { canTake: true };
    }

    return this.canTakeEditRightFrom === 'all' || (this.canTakeEditRightFrom === 'editor' && currentTokenInfo.holder.role === 'editor')
      ? { canTake: true }
      : { canTake: false, reason: 'cant-take-from-holder' };
  }
}
