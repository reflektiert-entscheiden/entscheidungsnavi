import { Exclude, Expose } from 'class-transformer';
import { IsString } from 'class-validator';
import { TeamInviteInfo } from '..';

@Exclude()
export class TeamInviteInfoDto implements TeamInviteInfo {
  @Expose()
  @IsString()
  teamName: string;

  @Expose()
  @IsString()
  inviterName: string;
}
