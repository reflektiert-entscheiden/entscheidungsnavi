import { Exclude, Expose, Type } from 'class-transformer';
import { IsDate, IsNotEmpty, IsOptional, IsString, ValidateNested } from 'class-validator';
import { TeamAccessLog, TeamEditTokenLogEntry } from '..';

@Exclude()
export class TeamEditTokenLogEntryDto implements TeamEditTokenLogEntry {
  @Expose()
  @IsString()
  @IsNotEmpty()
  @Type(() => String)
  member: string;

  @Expose()
  @Type(() => Date)
  @IsDate()
  from: Date;

  @Expose()
  @Type(() => Date)
  @IsDate()
  @IsOptional()
  to?: Date;
}

@Exclude()
export class TeamAccessLogDto implements TeamAccessLog {
  @Expose()
  @Type(() => TeamEditTokenLogEntryDto)
  @ValidateNested({ each: true })
  editToken: TeamEditTokenLogEntryDto[];
}
