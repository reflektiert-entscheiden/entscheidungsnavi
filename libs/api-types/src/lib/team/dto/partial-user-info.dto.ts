import { Exclude, Expose, Type } from 'class-transformer';
import { IsEmail, IsOptional, IsNotEmpty, IsString } from 'class-validator';
import { TeamPartialUserInfo } from '..';

@Exclude()
export class PartialUserInfoDto implements TeamPartialUserInfo {
  @Expose()
  @Type(() => String)
  id: string;

  @IsEmail()
  @Expose()
  email: string;

  @IsOptional()
  @IsNotEmpty()
  @IsString()
  @Expose()
  name?: string;
}
