import { Exclude, Expose, Type } from 'class-transformer';
import { IsIn, IsNotEmpty, IsString } from 'class-validator';
import { TeamProjectInfo, TeamRole, TeamRoles } from '..';

@Exclude()
export class TeamProjectInfoDto implements TeamProjectInfo {
  @Expose()
  @Type(() => String)
  id: string;

  @IsNotEmpty()
  @IsString()
  @Expose()
  name: string;

  @Expose()
  @Type(() => Date)
  createdAt: Date;

  @Expose()
  @Type(() => Date)
  updatedAt: Date;
}

@Exclude()
export class TeamProjectInfoWithRoleDto extends TeamProjectInfoDto {
  @Expose()
  @IsString()
  @IsIn(TeamRoles)
  userMemberRole: TeamRole;
}
