export interface CsrfToken {
  token: string;
}

export const CSRF_ERROR = {
  statusCode: 403,
  message: 'invalid csrf token',
};
