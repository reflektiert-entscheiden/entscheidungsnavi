export const ROLES = ['admin', 'privileged-user', 'event-manager', 'klug-manager', 'quickstart-manager'] as const;

export type Role = (typeof ROLES)[number];
