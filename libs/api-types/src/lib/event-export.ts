// Parameters are decision statement, objectives, alternatives and influence factors
export const EVENT_DEFAULT_EXPORT_ATTRIBUTES = [
  'projectInformation',
  'processStatus',
  'modelParameterCounts',

  'stepExplanations',
  'objectiveExplanations',
  'alternativeExplanations',
  'influenceFactorExplanations',

  'decisionQuality',
  'timeRecording',

  'submissionQuestionnaire',
  'submissionFreeText',
] as const;
export type EventDefaultExportAttribute = (typeof EVENT_DEFAULT_EXPORT_ATTRIBUTES)[number];

export const EVENT_ALT_AND_OBJ_EXPORT_ALTERNATIVES_ATTRIBUTES = [
  'alternativesSheet',
  'defaultAnalysis',
  'linearNumericalAnalysis',
  'linearVerbalAnalysis',
  'linearBothAnalysis',
  'linearMostImportantAnalyses',
  'linearMostImportantAccumulatedAnalyses',
  'linearLeastImportantAnalyses',
  'linearLeastImportantAccumulatedAnalyses',
] as const;
export type EventAltAndObjExportAlternativesAttribute = (typeof EVENT_ALT_AND_OBJ_EXPORT_ALTERNATIVES_ATTRIBUTES)[number];

export const EVENT_ALT_AND_OBJ_EXPORT_OBJECTIVES_ATTRIBUTES = ['objectivesSheet', 'scaleInfo', 'weightInfo', 'utilityInfo'] as const;
export type EventAltAndObjExportObjectivesAttribute = (typeof EVENT_ALT_AND_OBJ_EXPORT_OBJECTIVES_ATTRIBUTES)[number];

export interface EventDefaultExportRequest {
  eventIds: string[];
  attributes: EventDefaultExportAttribute[];

  // The maximum number of parameters to return
  alternativeLimit?: number;
  objectiveLimit?: number;
  influenceFactorLimit?: number;
}

export interface EventAltAndObjExportRequest {
  eventIds: string[];
  alternativeAttributes: EventAltAndObjExportAlternativesAttribute[];
  objectiveAttributes: EventAltAndObjExportObjectivesAttribute[];
}

export interface EventProjectsExportRequest {
  eventIds: string[];
}
