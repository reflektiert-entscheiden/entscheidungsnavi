import { PaginatedList } from './pagination';
import { Role } from './role';
import { SortDirection } from './sort';

export interface User {
  id: string;
  name?: string;
  email: string;
  emailConfirmed: boolean;
  roles: Role[];
  createdAt: Date;
}

export interface UserFilter {
  query?: string;
  emailConfirmed?: boolean;
  roles?: Role[];
}

export const USER_SORT_BY = ['name', 'email', 'emailConfirmed', 'roles', 'createdAt'] as const;
export type UserSortBy = (typeof USER_SORT_BY)[number];

export interface UserSort {
  sortBy?: UserSortBy;
  sortDirection?: SortDirection;
}

export type UserList = PaginatedList<User>;
