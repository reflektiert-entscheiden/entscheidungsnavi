export interface Statistics {
  userCount: number;
  projectCount: number;
}
