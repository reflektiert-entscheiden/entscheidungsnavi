import { LocalizedString } from './localized-string';

export interface QuickstartProject {
  id: string;
  name: string;
  data?: any;
  visible: boolean;
  tags: string[];
  shareToken?: string;
  shareUrl: string;
  createdAt: Date;
  updatedAt: Date;
}

export interface QuickstartTag {
  id: string;
  name: LocalizedString;
  weight?: number;
  createdAt: Date;
  updatedAt: Date;
}

export interface QuickstartValue {
  id: string;
  name: LocalizedString;
  createdAt: Date;
  updatedAt: Date;
}

export interface QuickstartValueWithStats extends QuickstartValue {
  countInTop5: number;
  countAssigned: number;
  countTracked: number;
  accumulatedScore: number;
}

export interface QuickstartValueTracking {
  values: Array<{ id: string; score: number }>;
}

export interface QuickstartQuestion {
  id: string;
  name: LocalizedString;
  tags: string[];
  createdAt: Date;
  updatedAt: Date;
}

export interface QuickstartQuestionWithStats extends QuickstartQuestion {
  countAssigned: number;
}
