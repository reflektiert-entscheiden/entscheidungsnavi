// 10MB limit
export const PROJECT_SIZE_LIMIT = 10e6;

export interface Project {
  id: string;
  name: string;
  userId: string;
  shareToken?: string;
  createdAt: Date;
  updatedAt: Date;
}

export interface ProjectWithData extends Project {
  data: string;
}

export const PROJECT_SAVE_TYPES = ['save-as', 'restore-backup', 'resolve-conflict'] as const;
export type ProjectSaveType = (typeof PROJECT_SAVE_TYPES)[number];

// Either send data or a patch
type ProjectDataUpdate = { data: string; dataSaveType: ProjectSaveType } | { dataPatch: string; oldDataHash: number };
export type ProjectUpdate = Partial<Pick<Project, 'name'>> | ProjectDataUpdate;

export interface ProjectHistoryEntry {
  id: string;
  saveType?: ProjectSaveType;
  versionTimestamp: Date;
}
