import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { logError, transformAndValidate } from '@entscheidungsnavi/tools';
import { Observable, OperatorFunction, map } from 'rxjs';
import {
  AlternativeOpinions,
  TaskWrapperDto,
  Team,
  TeamCommentDto,
  TeamDto,
  TeamEditTokenDto,
  TeamInviteInfoDto,
  TeamMember,
  TeamMemberDto,
  TeamProjectInfoDto,
  TeamProjectInfoWithRoleDto,
  TeamRoles,
  TeamTaskDto,
  TeamTaskType,
} from '@entscheidungsnavi/api-types';

@Injectable({
  providedIn: 'root',
})
export class TeamsService {
  constructor(private http: HttpClient) {}

  createTeam(name: string, data: string) {
    return this.http.post<unknown>(`/api/teams`, { name, data }).pipe(transformAndValidate(TeamProjectInfoDto), logError());
  }

  deleteTeam(id: string) {
    return this.http.delete<void>(`/api/teams/${id}`).pipe(logError());
  }

  getTeamProjects(onlyWhereOwner = false) {
    return this.http
      .get<unknown[]>('/api/teams', {
        params: { onlyWhereOwner },
      })
      .pipe(transformAndValidate(TeamProjectInfoWithRoleDto), logError());
  }

  getTeamCommentsForObject(teamId: string, objectId: string) {
    return this.http
      .get<unknown[]>(`/api/teams/${teamId}/comments`, {
        params: { forObject: objectId },
      })
      .pipe(transformAndValidate(TeamCommentDto), logError());
  }

  deleteInvite(teamId: string, inviteId: string) {
    return this.http.delete<void>(`/api/teams/${teamId}/invite/${inviteId}`).pipe(logError());
  }

  createVirtualMember(teamId: string, name: string) {
    return this.http.post<unknown>(`/api/teams/${teamId}/members/virtual`, { name }).pipe(transformAndValidate(TeamMemberDto), logError());
  }

  createInvitedMember(teamId: string, email: string) {
    return this.http.post<unknown>(`/api/teams/${teamId}/members/invited`, { email }).pipe(transformAndValidate(TeamMemberDto), logError());
  }

  resendInvite(teamId: string, memberId: string) {
    return this.http.post<unknown>(`/api/teams/${teamId}/members/${memberId}/resend`, {});
  }

  getInviteInfo(token: string) {
    return this.http.get<unknown>(`/api/teams/members/invited/${token}`).pipe(transformAndValidate(TeamInviteInfoDto));
  }

  join(token: string) {
    return this.http.post<unknown>(`/api/teams/members/invited/${token}`, {}).pipe(transformAndValidate(TeamProjectInfoDto), logError());
  }

  leave(teamId: string) {
    return this.http.post<void>(`/api/teams/${teamId}/leave`, {}).pipe(logError());
  }

  addComment(teamId: string, objectId: string, content: string) {
    return this.http
      .post<unknown[]>(`/api/teams/${teamId}/comments`, {
        objectId,
        content,
      })
      .pipe(transformAndValidate(TeamCommentDto), logError());
  }

  deleteComment(teamId: string, commentId: string) {
    return this.http.delete<unknown[]>(`/api/teams/${teamId}/comments/${commentId}`).pipe(transformAndValidate(TeamCommentDto), logError());
  }

  getTeam(teamId: string) {
    return this.http.get<unknown>(`/api/teams/${teamId}`).pipe(transformAndValidate(TeamDto, false, [...TeamRoles]), logError());
  }

  updateTeam(
    teamId: string,
    update: Partial<
      Pick<
        Team,
        | 'whiteboard'
        | 'canTakeEditRightFrom'
        | 'editRightExpiresFor'
        | 'mainProjectData'
        | 'lockedWeightTasks'
        | 'lockedOpinionTasks'
        | 'name'
      >
    >,
  ) {
    return this.http.patch<void>(`/api/teams/${teamId}`, update);
  }

  updateImportance(
    teamId: string,
    update: Pick<Team, 'importanceShareNames' | 'importanceShares'> & { memberImportanceValues: TeamMember['importance'][] },
  ) {
    return this.http.post<unknown>(`/api/teams/${teamId}/importance`, update).pipe(logError());
  }

  takeEditToken(teamId: string) {
    return this.http.post<unknown>(`/api/teams/${teamId}/editToken/take`, {}).pipe(transformAndValidate(TeamEditTokenDto));
  }

  returnEditToken(teamId: string) {
    return this.http.delete<void>(`/api/teams/${teamId}/editToken`, {});
  }

  updateTeamMember(
    teamId: string,
    memberId: string,
    update: Partial<Pick<TeamMember, 'role'> & { email: string } & { isVirtual: boolean }>,
  ) {
    return this.http.patch<void>(`/api/teams/${teamId}/members/${memberId}`, update).pipe(logError());
  }

  deleteTeamMember(teamId: string, memberId: string) {
    return this.http.delete<void>(`/api/teams/${teamId}/members/${memberId}`).pipe(logError());
  }

  assignTask(teamId: string, memberId: string, taskType: TeamTaskType, data: { objectiveUUID?: string } = {}): Observable<TeamTaskDto> {
    return this.http
      .post<unknown>(`/api/teams/${teamId}/members/${memberId}/tasks`, { task: { type: taskType, ...data } })
      .pipe(transformTeamTask(), logError());
  }

  updateTask(
    teamId: string,
    memberId: string,
    taskId: string,
    update: { data?: string; submitted?: boolean; applied?: boolean; weights?: Record<string, number>; opinions?: AlternativeOpinions },
  ) {
    return this.http.patch<void>(`/api/teams/${teamId}/members/${memberId}/tasks/${taskId}`, update).pipe(logError());
  }

  deleteTask(teamId: string, memberId: string, taskId: string) {
    return this.http.delete<void>(`/api/teams/${teamId}/members/${memberId}/tasks/${taskId}`).pipe(logError());
  }

  getEditToken(teamId: string) {
    return this.http.get<unknown>(`/api/teams/${teamId}/editToken`).pipe(transformAndValidate(TeamEditTokenDto), logError());
  }
}

export function transformTeamTask(): OperatorFunction<unknown, TeamTaskDto> {
  return observable => {
    return observable.pipe(
      map(data => ({ task: data })),
      transformAndValidate(TaskWrapperDto),
      map(wrapper => wrapper.task),
    );
  };
}
