import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HTTP_INTERCEPTORS, provideHttpClient, withInterceptorsFromDi } from '@angular/common/http';
import { CsrfInterceptor } from './csrf/csrf.interceptor';
import { LanguageInterceptor } from './common/language.interceptor';
import { NotifyUnauthorizedInterceptor } from './auth/notify-unauthorized.interceptor';

@NgModule({
  imports: [CommonModule],
  providers: [
    NotifyUnauthorizedInterceptor,
    { provide: HTTP_INTERCEPTORS, useExisting: NotifyUnauthorizedInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: CsrfInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: LanguageInterceptor, multi: true },
    provideHttpClient(withInterceptorsFromDi()),
  ],
})
export class ApiClientModule {}
