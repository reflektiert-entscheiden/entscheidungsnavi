import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent } from '@angular/common/http';
import { Inject, Injectable, LOCALE_ID } from '@angular/core';
import { Language } from '@entscheidungsnavi/api-types';
import { Observable } from 'rxjs';

@Injectable()
export class LanguageInterceptor implements HttpInterceptor {
  private language: Language;

  constructor(@Inject(LOCALE_ID) locale: string) {
    this.language = locale.includes('de') ? 'de' : 'en';
  }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    if (request.url.startsWith('/api')) {
      // set the language parameter
      request = request.clone({ setHeaders: { 'Accept-Language': this.language } });
    }
    return next.handle(request);
  }
}
