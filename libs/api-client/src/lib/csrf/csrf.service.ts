import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { logError, cachedSwitchAll, transformAndValidate } from '@entscheidungsnavi/tools';
import { Subject, map, startWith } from 'rxjs';
import { CsrfTokenDto } from './dto/csrf-token.dto';

@Injectable({
  providedIn: 'root',
})
export class CsrfService {
  private resetToken$ = new Subject<void>();

  readonly token$ = this.resetToken$.pipe(
    startWith(null),
    map(() =>
      this.httpClient.get<unknown>('/api/auth/csrf-token').pipe(
        transformAndValidate(CsrfTokenDto),
        map(tokenDto => tokenDto.token),
        logError(),
      ),
    ),
    cachedSwitchAll(),
  );

  constructor(private httpClient: HttpClient) {}

  resetToken() {
    this.resetToken$.next();
  }
}
