import { HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, first, map, of, retry, switchMap, throwError } from 'rxjs';
import { isEqual } from 'lodash';
import { CSRF_ERROR } from '@entscheidungsnavi/api-types';
import { CsrfService } from './csrf.service';

// These http methods do not require CSRF tokens
const whitelistedMethods = ['GET', 'HEAD', 'OPTIONS'];

@Injectable()
export class CsrfInterceptor implements HttpInterceptor {
  constructor(private csrfService: CsrfService) {}

  intercept(req: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<any>> {
    if (req.url.startsWith('/api') && !whitelistedMethods.includes(req.method)) {
      return this.csrfService.token$.pipe(
        first(),
        map(token => req.clone({ setHeaders: { 'x-csrf-token': token } })),
        switchMap(req => next.handle(req)),
        retry({
          count: 1,
          delay: error => {
            // If the problem is an invalid CSRF token, we retry with a new token. Otherwise, we pass on the error.
            if (error instanceof HttpErrorResponse && error.status === 403 && isEqual(error.error, CSRF_ERROR)) {
              this.csrfService.resetToken();
              return of(null);
            }

            return throwError(() => error);
          },
        }),
      );
    }

    return next.handle(req);
  }
}
