import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import {
  GroupedQuickstartObjectiveSort,
  LocalizedString,
  PaginationParams,
  QuickstartHierarchy,
  QuickstartHierarchyFilter,
  QuickstartHierarchySort,
  QuickstartObjectiveFilter,
  QuickstartObjectiveGroupBy,
  QuickstartObjectiveSort,
  QuickstartQuestion,
  QuickstartValue,
  QuickstartValueTracking,
} from '@entscheidungsnavi/api-types';
import { logError, transformAndValidate } from '@entscheidungsnavi/tools';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { QuickstartProjectDto, QuickstartProjectWithDataDto } from './dto/quickstart-project.dto';
import { QuickstartTagListDto } from './dto/quickstart-tag-list.dto';
import { QuickstartTagDto } from './dto/quickstart-tag.dto';
import { QuickstartQuestionDto, QuickstartQuestionWithCountsDto } from './dto/quickstart-question.dto';
import { QuickstartValueListDto, QuickstartValueWithStatsDto, QuickstartValueWithStatsListDto } from './dto/quickstart-value.dto';
import { QuickstartHierarchyDto, QuickstartHierarchyListDto } from './dto/quickstart-hierarchy.dto';
import { QuickstartHierarchyListWithStatsDto, QuickstartHierarchyWithStatsDto } from './dto/quickstart-hierarchy-stats.dto';
import { QuickstartObjectiveListDto, QuickstartObjectiveListWithStatsDto } from './dto/quickstart-objective.dto';
import { GroupedQuickstartObjectiveListDto, GroupedQuickstartObjectiveListWithStatsDto } from './dto/quickstart-objective-grouped.dto';

@Injectable({
  providedIn: 'root',
})
export class QuickstartService {
  constructor(private http: HttpClient) {}

  // Projects

  getProjects(showInvisible = false) {
    return this.http
      .get<unknown[]>('/api/quickstart/projects', { params: { showInvisible } })
      .pipe(transformAndValidate(QuickstartProjectDto), logError());
  }

  getProject(handle: string) {
    return this.http
      .get<unknown>(`/api/quickstart/projects/${handle}`)
      .pipe(transformAndValidate(QuickstartProjectWithDataDto), logError());
  }

  updateProject(id: string, update: Partial<{ data: string; name: string; visible: boolean; tags: string[]; shareToken: string }>) {
    const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    return this.http
      .patch<unknown>(`/api/quickstart/projects/${id}`, update, { headers })
      .pipe(transformAndValidate(QuickstartProjectDto), logError());
  }

  addProject(data: { name: string; data: string; visible: boolean; tags?: string[]; shareToken?: string }) {
    const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    return this.http
      .post<unknown>('/api/quickstart/projects', data, { headers })
      .pipe(transformAndValidate(QuickstartProjectDto), logError());
  }

  deleteProject(id: string) {
    return this.http.delete(`/api/quickstart/projects/${id}`, { observe: 'response' }).pipe(logError());
  }

  // Tags

  getTags() {
    return this.http.get<unknown>('/api/quickstart/tags').pipe(
      map(list => ({ list })),
      transformAndValidate(QuickstartTagListDto),
      map(quickStartListDto => quickStartListDto.list),
      logError(),
    );
  }

  updateTag(id: string, update: { name?: LocalizedString; weight?: number }) {
    const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    return this.http
      .patch<unknown>(`/api/quickstart/tags/${id}`, update, { headers })
      .pipe(transformAndValidate(QuickstartTagDto), logError());
  }

  addTag(name: LocalizedString, weight?: number) {
    const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    return this.http
      .post<unknown>('/api/quickstart/tags', { name, weight }, { headers })
      .pipe(transformAndValidate(QuickstartTagDto), logError());
  }

  deleteTag(id: string) {
    return this.http.delete(`/api/quickstart/tags/${id}`, { observe: 'response' }).pipe(logError());
  }

  // Values

  getValues() {
    return this.http.get<unknown>('/api/quickstart/values').pipe(
      map(list => ({ list })),
      transformAndValidate(QuickstartValueListDto),
      map(list => list.list),
      logError(),
    );
  }

  getValuesWithStats() {
    return this.http.get<unknown>('/api/quickstart/values', { params: { includeStats: true } }).pipe(
      map(list => ({ list })),
      transformAndValidate(QuickstartValueWithStatsListDto),
      map(list => list.list),
      logError(),
    );
  }

  updateValue(id: string, update: Pick<QuickstartValue, 'name'>) {
    const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    return this.http
      .patch<unknown>(`/api/quickstart/values/${id}`, update, { headers })
      .pipe(transformAndValidate(QuickstartValueWithStatsDto), logError());
  }

  addValue(value: Pick<QuickstartValue, 'name'>) {
    const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    return this.http
      .post<unknown>('/api/quickstart/values', value, { headers })
      .pipe(transformAndValidate(QuickstartValueWithStatsDto), logError());
  }

  deleteValue(id: string) {
    return this.http.delete(`/api/quickstart/values/${id}`, { observe: 'response' }).pipe(logError());
  }

  trackValueUsage(values: QuickstartValueTracking) {
    const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    return this.http.post<void>('/api/quickstart/values/metrics', values, { headers }).pipe(logError());
  }

  // Questions
  getQuestions() {
    return this.http
      .get<unknown[]>('/api/quickstart/questions', { params: { includeCounts: false } })
      .pipe(transformAndValidate(QuickstartQuestionDto), logError());
  }

  getQuestionsWithCounts() {
    return this.http
      .get<unknown[]>('/api/quickstart/questions', { params: { includeCounts: true } })
      .pipe(transformAndValidate(QuickstartQuestionWithCountsDto), logError());
  }

  updateQuestion(id: string, updatedQuestion: Pick<QuickstartQuestion, 'name' | 'tags'>) {
    const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    return this.http
      .patch<unknown>(`/api/quickstart/questions/${id}`, updatedQuestion, { headers })
      .pipe(transformAndValidate(QuickstartQuestionDto), logError());
  }

  addQuestion(question: Pick<QuickstartQuestion, 'name' | 'tags'>) {
    const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    return this.http
      .post<unknown>('/api/quickstart/questions', question, { headers })
      .pipe(transformAndValidate(QuickstartQuestionDto), logError());
  }

  deleteQuestion(id: string) {
    return this.http.delete(`/api/quickstart/questions/${id}`, { observe: 'response' }).pipe(logError());
  }

  trackQuestionUsage(id: string) {
    return this.http.post(`/api/quickstart/questions/${id}/count`, {}).pipe(logError());
  }

  // Hierarchies

  getHierarchies(
    filter: QuickstartHierarchyFilter,
    pagination: PaginationParams,
    includeStats?: false,
    sort?: Partial<QuickstartHierarchySort>,
  ): Observable<QuickstartHierarchyListDto>;
  getHierarchies(
    filter: QuickstartHierarchyFilter,
    pagination: PaginationParams,
    includeStats: true,
    sort?: Partial<QuickstartHierarchySort>,
  ): Observable<QuickstartHierarchyListWithStatsDto>;
  getHierarchies(
    filter: QuickstartHierarchyFilter,
    pagination: PaginationParams,
    includeStats = false,
    sort?: Partial<QuickstartHierarchySort>,
  ) {
    return this.http
      .get<unknown>('/api/quickstart/hierarchies', { params: { ...pagination, ...filter, ...sort, includeStats } })
      .pipe(transformAndValidate(includeStats ? QuickstartHierarchyListWithStatsDto : QuickstartHierarchyListDto), logError());
  }

  getHierarchyWithStats(id: string) {
    return this.http
      .get<unknown>(`/api/quickstart/hierarchies/${id}`)
      .pipe(transformAndValidate(QuickstartHierarchyWithStatsDto), logError());
  }

  addHierarchy(hierarchy: Pick<QuickstartHierarchy, 'name' | 'tags' | 'tree'>) {
    return this.http.post<unknown>(`/api/quickstart/hierarchies`, hierarchy).pipe(transformAndValidate(QuickstartHierarchyDto), logError());
  }

  deleteHierarchy(id: string) {
    return this.http.delete<void>(`/api/quickstart/hierarchies/${id}`).pipe(logError());
  }

  updateHierarchy(id: string, update: Partial<Pick<QuickstartHierarchy, 'name' | 'tags' | 'tree'>>) {
    return this.http
      .patch<unknown>(`/api/quickstart/hierarchies/${id}`, update)
      .pipe(transformAndValidate(QuickstartHierarchyDto), logError());
  }

  trackObjective(objectiveIds: string[]) {
    return this.http.post<void>(`/api/quickstart/track-objectives`, { objectiveIds }).pipe(logError());
  }

  getObjectives(
    filter: QuickstartObjectiveFilter,
    pagination: PaginationParams,
    sort: Partial<QuickstartObjectiveSort>,
    includeStats?: false,
  ): Observable<QuickstartObjectiveListDto>;
  getObjectives(
    filter: QuickstartObjectiveFilter,
    pagination: PaginationParams,
    sort: Partial<QuickstartObjectiveSort>,
    includeStats: true,
  ): Observable<QuickstartObjectiveListWithStatsDto>;
  getObjectives(
    filter: QuickstartObjectiveFilter,
    pagination: PaginationParams,
    sort: Partial<QuickstartObjectiveSort>,
    includeStats = false,
  ) {
    return this.http
      .get<unknown>('/api/quickstart/objectives', { params: { ...pagination, ...filter, ...sort, includeStats } })
      .pipe(transformAndValidate(includeStats ? QuickstartObjectiveListWithStatsDto : QuickstartObjectiveListDto), logError());
  }

  getGroupedObjectives(
    filter: QuickstartObjectiveFilter,
    pagination: PaginationParams,
    groupBy: QuickstartObjectiveGroupBy,
    sort: Partial<GroupedQuickstartObjectiveSort>,
    includeStats?: false,
  ): Observable<GroupedQuickstartObjectiveListDto>;
  getGroupedObjectives(
    filter: QuickstartObjectiveFilter,
    pagination: PaginationParams,
    groupBy: QuickstartObjectiveGroupBy,
    sort: Partial<GroupedQuickstartObjectiveSort>,
    includeStats: true,
  ): Observable<GroupedQuickstartObjectiveListWithStatsDto>;
  getGroupedObjectives(
    filter: QuickstartObjectiveFilter,
    pagination: PaginationParams,
    groupBy: QuickstartObjectiveGroupBy,
    sort: Partial<GroupedQuickstartObjectiveSort>,
    includeStats = false,
  ) {
    return this.http
      .get<unknown>('/api/quickstart/objectives', { params: { ...pagination, ...filter, ...sort, includeStats, groupBy } })
      .pipe(
        transformAndValidate(includeStats ? GroupedQuickstartObjectiveListWithStatsDto : GroupedQuickstartObjectiveListDto),
        logError(),
      );
  }
}
