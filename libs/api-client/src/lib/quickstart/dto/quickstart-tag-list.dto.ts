import { Type } from 'class-transformer';
import { IsDefined, ValidateNested } from 'class-validator';
import { QuickstartTagDto } from './quickstart-tag.dto';

export class QuickstartTagListDto {
  @IsDefined()
  @ValidateNested({ each: true })
  @Type(() => QuickstartTagDto)
  list: QuickstartTagDto[];
}
