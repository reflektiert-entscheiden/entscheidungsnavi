import { QuickstartQuestionWithStats, QuickstartValue } from '@entscheidungsnavi/api-types';
import { Expose, Type } from 'class-transformer';
import { IsDate, IsDefined, IsInt, IsMongoId, ValidateNested } from 'class-validator';
import { LocalizedStringDto } from '../../common/localized-string.dto';

export class QuickstartQuestionDto implements QuickstartValue {
  @IsMongoId()
  id: string;

  @IsDefined()
  @ValidateNested()
  @Type(() => LocalizedStringDto)
  name: LocalizedStringDto;

  @Expose()
  @Type(() => String)
  @IsMongoId({ each: true })
  tags: string[];

  @IsDate()
  @Type(() => Date)
  createdAt: Date;

  @IsDate()
  @Type(() => Date)
  updatedAt: Date;
}

export class QuickstartQuestionWithCountsDto extends QuickstartQuestionDto implements QuickstartQuestionWithStats {
  @IsInt()
  countAssigned: number;
}
