import { Type } from 'class-transformer';
import { IsArray, IsDate, IsDefined, IsMongoId, IsNumber, ValidateNested } from 'class-validator';
import { QuickstartHierarchy, QuickstartHierarchyList } from '@entscheidungsnavi/api-types';
import { LocalizedStringDto } from '../../common/localized-string.dto';

export class QuickstartHierarchyTreeValueDto {
  @IsMongoId()
  id: string;

  @IsDefined()
  @Type(() => LocalizedStringDto)
  @ValidateNested()
  name: LocalizedStringDto;

  @IsDefined()
  @ValidateNested()
  @Type(() => LocalizedStringDto)
  comment: LocalizedStringDto;

  @IsDefined()
  @ValidateNested()
  @Type(() => LocalizedStringDto)
  scaleComment: LocalizedStringDto;
}

export class QuickstartHierarchyTreeDto {
  @IsDefined()
  @ValidateNested()
  @Type(() => QuickstartHierarchyTreeValueDto)
  value: QuickstartHierarchyTreeValueDto;

  @IsDefined()
  @ValidateNested({ each: true })
  @Type(() => QuickstartHierarchyTreeDto)
  children: QuickstartHierarchyTreeDto[];
}

export class QuickstartHierarchyDto implements QuickstartHierarchy {
  @IsMongoId()
  id: string;

  @IsDefined()
  @ValidateNested()
  @Type(() => LocalizedStringDto)
  name: LocalizedStringDto;

  @IsArray()
  @Type(() => String)
  @IsMongoId({ each: true })
  tags: string[];

  @IsDefined()
  @ValidateNested()
  @Type(() => QuickstartHierarchyTreeDto)
  tree: QuickstartHierarchyTreeDto;

  @IsDate()
  @Type(() => Date)
  createdAt: Date;

  @IsDate()
  @Type(() => Date)
  updatedAt: Date;
}

export class QuickstartHierarchyListDto implements QuickstartHierarchyList {
  @IsDefined()
  @ValidateNested({ each: true })
  @Type(() => QuickstartHierarchyDto)
  items: QuickstartHierarchyDto[];

  @IsDefined()
  @IsMongoId({ each: true })
  activeTags: string[];

  @IsNumber()
  count: number;
}
