import {
  LocalizedString,
  QuickstartObjective,
  QuickstartObjectiveList,
  QuickstartObjectiveListWithStats,
  QuickstartObjectiveWithStats,
} from '@entscheidungsnavi/api-types';
import { Type } from 'class-transformer';
import { IsDefined, IsInt, IsMongoId, IsNumber, ValidateNested } from 'class-validator';
import { LocalizedStringDto } from '../../common/localized-string.dto';

export class QuickstartObjectiveDto implements QuickstartObjective {
  @IsMongoId()
  id: string;

  @IsMongoId()
  hierarchyId: string;

  @IsDefined()
  @Type(() => LocalizedStringDto)
  @ValidateNested()
  hierarchyName: LocalizedString;

  @IsDefined()
  @Type(() => LocalizedStringDto)
  @ValidateNested()
  name: LocalizedStringDto;

  @IsDefined()
  @Type(() => LocalizedStringDto)
  @ValidateNested()
  comment: LocalizedStringDto;

  @IsDefined()
  @Type(() => LocalizedStringDto)
  @ValidateNested()
  scaleComment: LocalizedStringDto;

  @IsInt()
  level: number;

  @IsDefined()
  @IsMongoId({ each: true })
  tags: string[];
}

export class QuickstartObjectiveListDto implements QuickstartObjectiveList {
  @IsDefined()
  @ValidateNested({ each: true })
  @Type(() => QuickstartObjectiveDto)
  items: QuickstartObjectiveDto[];

  @IsDefined()
  @IsMongoId({ each: true })
  activeTags: string[];

  @IsInt()
  count: number;
}

export class QuickstartObjectiveWithStatsDto extends QuickstartObjectiveDto implements QuickstartObjectiveWithStats {
  @IsNumber()
  accumulatedScore: number;
}

export class QuickstartObjectiveListWithStatsDto extends QuickstartObjectiveListDto implements QuickstartObjectiveListWithStats {
  @IsDefined()
  @ValidateNested({ each: true })
  @Type(() => QuickstartObjectiveWithStatsDto)
  override items: QuickstartObjectiveWithStatsDto[];
}
