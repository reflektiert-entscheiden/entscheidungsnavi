import { PROJECT_SAVE_TYPES, ProjectHistoryEntry, ProjectSaveType } from '@entscheidungsnavi/api-types';
import { Type } from 'class-transformer';
import { IsDate, IsDefined, IsEnum, IsMongoId, IsOptional, ValidateNested } from 'class-validator';

export class ProjectHistoryEntryDto implements ProjectHistoryEntry {
  @IsMongoId()
  id: string;

  @IsOptional()
  @IsEnum(PROJECT_SAVE_TYPES)
  saveType?: ProjectSaveType;

  @IsDate()
  @Type(() => Date)
  versionTimestamp: Date;
}

export class ProjectHistoryEntryListDto {
  @IsDefined()
  @ValidateNested({ each: true })
  @Type(() => ProjectHistoryEntryDto)
  list: ProjectHistoryEntryDto[];
}
