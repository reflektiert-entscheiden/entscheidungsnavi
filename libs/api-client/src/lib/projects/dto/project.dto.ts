import { Project as ApiProject, ProjectWithData } from '@entscheidungsnavi/api-types';
import { Type } from 'class-transformer';
import { IsDate, IsMongoId, IsNotEmpty, IsOptional, IsString } from 'class-validator';

export class ProjectDto implements ApiProject {
  @IsMongoId()
  id: string;

  @IsNotEmpty()
  name: string;

  @IsMongoId()
  userId: string;

  @IsOptional()
  @IsString()
  shareToken?: string;

  @IsDate()
  @Type(() => Date)
  createdAt: Date;

  @IsDate()
  @Type(() => Date)
  updatedAt: Date;
}

export class ProjectWithDataDto extends ProjectDto implements ProjectWithData {
  @IsString()
  data: string;
}
