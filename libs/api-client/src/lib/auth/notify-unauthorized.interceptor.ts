import { Injectable } from '@angular/core';
import { HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Observable, Subject } from 'rxjs';
import { tap } from 'rxjs/operators';

@Injectable()
export class NotifyUnauthorizedInterceptor implements HttpInterceptor {
  private notifyUnauthorizedSubject$ = new Subject<void>();
  readonly notifyUnauthorized$ = this.notifyUnauthorizedSubject$.asObservable();

  /**
   * Implementation of the HttpInterceptor interface. We want to catch HTTP 401 errors and perform
   * a logout, when they occur.
   */
  intercept(req: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<any>> {
    return next.handle(req).pipe(
      tap({
        error: error => {
          if (error instanceof HttpErrorResponse && error.status === 401) {
            this.notifyUnauthorizedSubject$.next();
          }
        },
      }),
    );
  }
}
