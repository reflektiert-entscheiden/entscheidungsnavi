import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, defer, Observable, of, Subject } from 'rxjs';
import { catchError, filter, map, mergeMap, pairwise, switchMap, tap } from 'rxjs/operators';
import { Role } from '@entscheidungsnavi/api-types';
import { cachedSwitchAll, logError, transformAndValidate } from '@entscheidungsnavi/tools';
import { identity } from 'lodash';
import { CsrfService } from '../csrf/csrf.service';
import { UserDto } from './dto/user.dto';
import { NotifyUnauthorizedInterceptor } from './notify-unauthorized.interceptor';

@Injectable({ providedIn: 'root' })
export class AuthService {
  // True whenever we are logged in, or are currently in the process of logging in
  readonly loggingIn$ = new BehaviorSubject<boolean>(true);
  // This emits when a new user is loaded
  private readonly userUpdate$ = new Subject<UserDto>();

  // Emits the current user when logged in, null when logged out, and does not emit _while_ logging in
  readonly user$: Observable<UserDto | null> = this.loggingIn$.pipe(
    map(loggingIn => (loggingIn ? this.userUpdate$.asObservable() : of(null))),
    cachedSwitchAll(),
  );

  private _user: UserDto | null;

  readonly onLogin$ = this.user$.pipe(
    pairwise(),
    map(([oldUser, newUser]) => (oldUser == null ? newUser : null)),
    filter(Boolean),
  );

  readonly onLogout$ = this.user$.pipe(
    pairwise(),
    map(([oldUser, newUser]) => oldUser != null && newUser == null),
    filter(identity),
  );

  // This is true whenever we are logged in with a valid user. It is false when we are logged out.
  // It does not emit while we are in the process of logging in.
  readonly loggedIn$ = this.user$.pipe(map(user => user != null));

  get user() {
    return this._user;
  }

  get loggedIn() {
    return this.user != null;
  }

  constructor(
    private http: HttpClient,
    private csrfService: CsrfService,
    notifyUnauthorizedInterceptor: NotifyUnauthorizedInterceptor,
  ) {
    this.user$.subscribe(user => {
      this._user = user;
    });

    // If we get an HTTP 401 UNAUTHORIZED error, we assume our login has expired, and we are logged out.
    // We rethrow the error anyway, but make sure that we are properly logged out.
    notifyUnauthorizedInterceptor.notifyUnauthorized$.subscribe(() => {
      if (this.loggedIn) {
        this.loggingIn$.next(false);
        this.csrfService.resetToken();
      }
    });

    // Restore the login state by loading the user
    this.updateUser().subscribe({ error: () => this.loggingIn$.next(false) });
  }

  login(email: string, password: string) {
    const headers = new HttpHeaders({ 'Content-Type': 'application/json' });

    // login, then update user
    return defer(() => {
      this.loggingIn$.next(true);
      return this.http.post<void>('/api/auth/login', { email, password }, { headers });
    }).pipe(
      tap(() => this.csrfService.resetToken()),
      switchMap(() => this.updateUser()),
      tap({ error: () => this.loggingIn$.next(false) }),
      logError(),
    );
  }

  logout() {
    // We ignore errors when logging out
    return defer(() => {
      this.loggingIn$.next(false);
      return this.http.post<void>('/api/auth/logout', {});
    }).pipe(
      logError(),
      catchError(() => of(null)),
      tap(() => this.csrfService.resetToken()),
    );
  }

  createUser(email: string, password: string, name?: string) {
    const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    const requestBody = { email, password, name: name || undefined };
    return this.http.post<void>('/api/users', requestBody, { headers }).pipe(mergeMap(() => this.login(email, password)));
  }

  requestEmailConfirmation() {
    return this.http.post<void>('/api/user/request-email-confirmation', {}).pipe(logError());
  }

  confirmEmail(token: string) {
    return this.http.post<void>('/api/user/confirm-email', { token }).pipe(
      mergeMap(() => this.updateUser()),
      logError(),
    );
  }

  // get the logged-in user and update the user object
  private updateUser() {
    return this.http.get<unknown>('/api/user').pipe(
      transformAndValidate(UserDto),
      tap(user => this.userUpdate$.next(user)),
    );
  }

  requestPasswordReset(email: string) {
    const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    return this.http.post('/api/auth/reset', { email }, { headers, observe: 'response', responseType: 'text' }).pipe(logError());
  }

  checkResetToken(token: string) {
    return this.http.get(`/api/auth/reset/token/${token}`, { observe: 'response', responseType: 'text' }).pipe(logError());
  }

  resetPassword(token: string, newPassword: string) {
    const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    return this.http
      .put(`/api/auth/reset/token/${token}`, { password: newPassword }, { headers, observe: 'response', responseType: 'text' })
      .pipe(logError());
  }

  changePassword(oldPassword: string, newPassword: string) {
    const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    return this.http
      .patch('/api/user', { oldPassword, password: newPassword }, { headers, observe: 'response', responseType: 'text' })
      .pipe(logError());
  }

  changeUserName(name: string) {
    const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    return this.http.patch('/api/user', { name }, { headers, responseType: 'text' }).pipe(
      mergeMap(() => this.updateUser()),
      logError(),
    );
  }

  changeUserMail(email: string) {
    const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    return this.http.patch('/api/user', { email }, { headers, responseType: 'text' }).pipe(
      mergeMap(() => this.updateUser()),
      logError(),
    );
  }

  userHasRole(role: Role) {
    return this.user$.pipe(map(user => !!user?.roles?.includes(role)));
  }

  deleteUser(password: string) {
    return this.http.delete<void>('/api/user', { body: { password } }).pipe(
      tap(() => this.logout()),
      logError(),
    );
  }
}
