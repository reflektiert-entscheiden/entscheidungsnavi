import { Role, ROLES, User as ApiUser } from '@entscheidungsnavi/api-types';
import { Type } from 'class-transformer';
import { IsBoolean, IsDate, IsEnum, IsMongoId, IsNotEmpty, IsOptional, IsString } from 'class-validator';

export class UserDto implements ApiUser {
  @IsMongoId()
  id: string;

  @IsOptional()
  @IsString()
  name?: string;

  @IsString()
  @IsNotEmpty()
  email: string;

  @IsBoolean()
  emailConfirmed: boolean;

  @IsEnum(ROLES, { each: true })
  roles: Role[];

  @IsNotEmpty()
  @IsDate()
  @Type(() => Date)
  createdAt: Date;
}
