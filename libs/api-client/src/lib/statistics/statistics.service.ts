import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { logError, transformAndValidate } from '@entscheidungsnavi/tools';
import { Observable } from 'rxjs';
import { StatisticsDto } from './dto/statistics.dto';

@Injectable({
  providedIn: 'root',
})
export class StatisticsService {
  constructor(private http: HttpClient) {}

  getStatistics(): Observable<StatisticsDto> {
    return this.http.get<unknown>('/api/statistics').pipe(transformAndValidate(StatisticsDto), logError());
  }
}
