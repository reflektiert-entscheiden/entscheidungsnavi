import { EventRegistrationList } from '@entscheidungsnavi/api-types';
import { Exclude, Expose, Type } from 'class-transformer';
import { IsNumber, ValidateNested } from 'class-validator';
import { EventRegistrationDto } from './event-registration.dto';

@Exclude()
export class EventRegistrationListDto implements EventRegistrationList {
  @Expose()
  @Type(() => EventRegistrationDto)
  @ValidateNested({ each: true })
  items: EventRegistrationDto[];

  @Expose()
  @IsNumber()
  count: number;
}
