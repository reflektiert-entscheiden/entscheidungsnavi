import { Type } from 'class-transformer';
import { IsDefined, ValidateNested } from 'class-validator';
import { EventWithStatsDto } from './event.dto';

export class EventListDto {
  @IsDefined()
  @ValidateNested({ each: true })
  @Type(() => EventWithStatsDto)
  list: EventWithStatsDto[];
}
