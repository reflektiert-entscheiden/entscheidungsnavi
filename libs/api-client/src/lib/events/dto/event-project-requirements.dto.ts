import { EventProjectRequirements } from '@entscheidungsnavi/api-types';
import { IsBoolean } from 'class-validator';

export class EventProjectRequirementsDto implements EventProjectRequirements {
  @IsBoolean()
  requireDecisionStatement: boolean;

  @IsBoolean()
  requireObjectives: boolean;

  @IsBoolean()
  requireAlternatives: boolean;

  @IsBoolean()
  requireImpactModel: boolean;

  @IsBoolean()
  requireObjectiveWeights: boolean;

  @IsBoolean()
  requireDecisionQuality: boolean;
}
