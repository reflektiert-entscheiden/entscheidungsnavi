import { EventFreeTextConfig } from '@entscheidungsnavi/api-types';
import { IsBoolean, IsNumber, IsString } from 'class-validator';

export class EventFreeTextConfigDto implements EventFreeTextConfig {
  @IsBoolean()
  enabled: boolean;

  @IsString()
  name: string;

  @IsString()
  placeholder: string;

  @IsNumber()
  minLength: number;
}
