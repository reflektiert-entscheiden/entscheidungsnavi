import {
  BaseQuestionnaireEntry,
  QuestionnaireEntryType,
  QUESTIONNAIRE_ENTRY_TYPES,
  NumberQuestionEntry,
  OptionsQuestionEntry,
  TextQuestionEntry,
  OptionsQuestionDisplayType,
  OPTIONS_QUESTION_DISPLAY_TYPES,
  QuestionnairePage,
  FormFieldQuestionEntry,
  TableQuestionEntry,
  TextBlockEntry,
  TEXT_BLOCK_TYPES,
  TextBlockType,
} from '@entscheidungsnavi/api-types';
import { Type } from 'class-transformer';
import { IsEnum, IsNumber, IsOptional, IsString, ValidateNested } from 'class-validator';

export abstract class AbstractQuestionnaireEntryDto implements BaseQuestionnaireEntry {
  @IsEnum(QUESTIONNAIRE_ENTRY_TYPES)
  entryType: QuestionnaireEntryType;
}

export class TextBlockEntryDto extends AbstractQuestionnaireEntryDto implements TextBlockEntry {
  override entryType: 'textBlock';

  @IsString()
  text: string;

  @IsEnum(TEXT_BLOCK_TYPES)
  type: TextBlockType;
}

abstract class AbstractFormFieldEntryDto extends AbstractQuestionnaireEntryDto implements FormFieldQuestionEntry {
  @IsString()
  question: string;

  @IsString()
  label: string;
}

export class TextQuestionEntryDto extends AbstractFormFieldEntryDto implements TextQuestionEntry {
  override entryType: 'textQuestion';

  @IsOptional()
  @IsNumber()
  minLength?: number;

  @IsOptional()
  @IsNumber()
  maxLength?: number;

  @IsOptional()
  @IsString()
  pattern?: string;
}

export class NumberQuestionEntryDto extends AbstractFormFieldEntryDto implements NumberQuestionEntry {
  override entryType: 'numberQuestion';

  @IsOptional()
  @IsNumber()
  min?: number;

  @IsOptional()
  @IsNumber()
  max?: number;

  @IsOptional()
  @IsNumber()
  step?: number;
}

export class OptionsQuestionEntryDto extends AbstractFormFieldEntryDto implements OptionsQuestionEntry {
  override entryType: 'optionsQuestion';

  @IsString({ each: true })
  options: string[];

  @IsEnum(OPTIONS_QUESTION_DISPLAY_TYPES)
  displayType: OptionsQuestionDisplayType;
}

export class TableQuestionEntryDto extends AbstractQuestionnaireEntryDto implements TableQuestionEntry {
  override entryType: 'tableQuestion';

  @IsString({ each: true })
  options: string[];

  @IsString()
  baseQuestion: string;

  @IsString({ each: true })
  subQuestions: string[];
}

export class QuestionnairePageDto implements QuestionnairePage {
  @ValidateNested({ each: true })
  @Type(() => AbstractQuestionnaireEntryDto, {
    keepDiscriminatorProperty: true,
    discriminator: {
      property: 'entryType',
      subTypes: [
        { value: TextBlockEntryDto, name: 'textBlock' },
        { value: TextQuestionEntryDto, name: 'textQuestion' },
        { value: NumberQuestionEntryDto, name: 'numberQuestion' },
        { value: OptionsQuestionEntryDto, name: 'optionsQuestion' },
        { value: TableQuestionEntryDto, name: 'tableQuestion' },
      ],
    },
  })
  entries: (TextBlockEntryDto | TextQuestionEntryDto | NumberQuestionEntryDto | OptionsQuestionEntryDto | TableQuestionEntryDto)[];
}
