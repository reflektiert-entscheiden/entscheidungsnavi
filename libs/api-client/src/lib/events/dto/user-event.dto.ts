import { UserEvent } from '@entscheidungsnavi/api-types';
import { Type } from 'class-transformer';
import { IsBoolean, IsDate, IsOptional, IsString, ValidateNested } from 'class-validator';
import { EventFreeTextConfigDto } from './event-free-text-config.dto';
import { EventProjectRequirementsDto } from './event-project-requirements.dto';
import { QuestionnairePageDto } from './event-question.dto';

export class UserEventDto implements UserEvent {
  @IsString()
  id: string;

  @IsString()
  name: string;

  @IsOptional()
  @Type(() => Date)
  @IsDate()
  startDate?: Date;

  @IsOptional()
  @Type(() => Date)
  @IsDate()
  endDate?: Date;

  @Type(() => EventProjectRequirementsDto)
  @ValidateNested()
  projectRequirements: EventProjectRequirementsDto;

  @IsOptional()
  @Type(() => QuestionnairePageDto)
  @ValidateNested({ each: true })
  questionnaire?: QuestionnairePageDto[];

  @IsOptional()
  @Type(() => EventFreeTextConfigDto)
  @ValidateNested()
  freeTextConfig: EventFreeTextConfigDto;

  @IsOptional()
  @IsBoolean()
  requireDataUsageAuthorization: boolean;
}
