import { EventRegistration, QuestionnaireEntryResponse } from '@entscheidungsnavi/api-types';
import { Type } from 'class-transformer';
import { IsArray, IsBoolean, IsDate, IsOptional, IsString, ValidateNested } from 'class-validator';
import { UserDto } from '../../auth/dto/user.dto';
import { UserEventDto } from './user-event.dto';

export class EventRegistrationDto implements EventRegistration {
  @Type(() => String)
  id: string;

  @IsOptional()
  @ValidateNested()
  @Type(() => UserEventDto)
  event?: UserEventDto;

  @IsBoolean()
  submitted: boolean;

  @IsOptional()
  @IsString()
  projectData?: string;

  @IsOptional()
  @IsString()
  freeText?: string;

  @Type(() => UserDto)
  user: Partial<Pick<UserDto, 'name' | 'email' | 'id'>>;

  @IsOptional()
  @IsArray()
  questionnaireResponses?: QuestionnaireEntryResponse[][];

  @IsDate()
  @Type(() => Date)
  createdAt: Date;

  @IsDate()
  @Type(() => Date)
  updatedAt: Date;
}

export class EventRegistrationWithDataDto extends EventRegistrationDto {
  override projectData: string;
  override questionnaireResponses: QuestionnaireEntryResponse[][];
  override freeText: string;
}
