import { Event, EventWithStats } from '@entscheidungsnavi/api-types';
import { Type } from 'class-transformer';
import { IsArray, IsBoolean, IsDate, IsDefined, IsNotEmpty, IsNumber, IsOptional, IsString, ValidateNested } from 'class-validator';
import { EventFreeTextConfigDto } from './event-free-text-config.dto';
import { EventProjectRequirementsDto } from './event-project-requirements.dto';
import { QuestionnairePageDto } from './event-question.dto';

type SubmissionStatus = 'upcoming' | 'open' | 'expired';

export class EventDto implements Event {
  @IsNotEmpty()
  id: string;

  @IsString()
  name: string;

  @IsDefined()
  owner: { email: string };

  @IsDefined()
  @IsArray()
  editors: { email: string }[];

  @IsDefined()
  @IsArray()
  viewers: { email: string }[];

  @IsOptional()
  @IsString()
  code?: string;

  @IsOptional()
  @IsDate()
  @Type(() => Date)
  startDate?: Date;

  @IsOptional()
  @IsDate()
  @Type(() => Date)
  endDate?: Date;

  @ValidateNested()
  @Type(() => EventProjectRequirementsDto)
  projectRequirements: EventProjectRequirementsDto;

  @ValidateNested()
  @Type(() => EventFreeTextConfigDto)
  freeTextConfig: EventFreeTextConfigDto;

  @ValidateNested({ each: true })
  @Type(() => QuestionnairePageDto)
  questionnaire: QuestionnairePageDto[];

  @IsBoolean()
  questionnaireReleased: boolean;

  @IsBoolean()
  requireDataUsageAuthorization: boolean;

  @IsDate()
  @Type(() => Date)
  createdAt: Date;

  @IsDate()
  @Type(() => Date)
  updatedAt: Date;

  get submissionStatus(): SubmissionStatus {
    const currentDate = new Date();
    if (this.startDate && this.startDate > currentDate) {
      return 'upcoming';
    } else if (this.endDate && this.endDate < currentDate) {
      return 'expired';
    } else {
      return 'open';
    }
  }
}

export class EventWithStatsDto extends EventDto implements EventWithStats {
  @IsNumber()
  registrationCount: number;

  @IsNumber()
  submissionCount: number;
}
