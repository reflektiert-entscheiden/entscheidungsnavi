import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import {
  Event,
  EventAltAndObjExportRequest,
  EventDefaultExportRequest,
  EventProjectsExportRequest,
  EventRegistrationFilter,
  EventRegistrationSort,
  PaginationParams,
} from '@entscheidungsnavi/api-types';
import { logError, cachedSwitchAll, transformAndValidate } from '@entscheidungsnavi/tools';
import { combineLatest, EMPTY, map, retry, startWith, Subject } from 'rxjs';
import { AuthService } from '../auth/auth.service';
import { EventListDto } from './dto/event-list.dto';
import { EventDto } from './dto/event.dto';
import { EventRegistrationListDto } from './dto/event-registration-list.dto';
import { EventRegistrationDto, EventRegistrationWithDataDto } from './dto/event-registration.dto';

export type CreateEventType = Pick<Event, 'name' | 'code' | 'startDate' | 'endDate'> &
  Partial<
    Pick<Event, 'projectRequirements' | 'freeTextConfig' | 'questionnaire' | 'questionnaireReleased' | 'requireDataUsageAuthorization'>
  >;

type UpdateEventType = Omit<Partial<CreateEventType>, 'requireDataUsageAuthorization'> &
  Partial<Pick<Event, 'owner' | 'editors' | 'viewers'>>;

@Injectable({
  providedIn: 'root',
})
export class EventManagementService {
  private refreshEvents$ = new Subject<void>();

  readonly events$ = combineLatest([this.authService.user$, this.refreshEvents$.pipe(startWith(null))]).pipe(
    map(([user]) => (user != null ? this.fetchEvents() : EMPTY)),
    cachedSwitchAll(),
  );

  constructor(
    private http: HttpClient,
    private authService: AuthService,
  ) {}

  refreshEvents() {
    this.refreshEvents$.next();
    return this.events$;
  }

  private fetchEvents() {
    return this.http.get<unknown>('/api/events').pipe(
      retry(2),
      map(list => ({ list })),
      transformAndValidate(EventListDto),
      logError(),
    );
  }

  createEvent(event: CreateEventType) {
    return this.http.post<unknown>('/api/events', event).pipe(transformAndValidate(EventDto), logError());
  }

  updateEvent(eventId: string, update: UpdateEventType) {
    return this.http.patch<unknown>(`/api/events/${eventId}`, update).pipe(transformAndValidate(EventDto), logError());
  }

  deleteEvent(eventId: string) {
    return this.http.delete<void>(`/api/events/${eventId}`).pipe(logError());
  }

  generateDefaultExport(request: EventDefaultExportRequest) {
    return this.http.post('/api/events/default-export', request, { responseType: 'blob' }).pipe(logError());
  }

  generateAltAndObjExport(request: EventAltAndObjExportRequest) {
    return this.http.post('/api/events/alternatives-and-objectives-export', request, { responseType: 'blob' }).pipe(logError());
  }

  generateProjectsExport(request: EventProjectsExportRequest) {
    return this.http.post('/api/events/projects-export', request, { responseType: 'blob' }).pipe(logError());
  }

  getEventRegistrations(eventId: string, pagination: PaginationParams, filter?: EventRegistrationFilter, sort?: EventRegistrationSort) {
    return this.http
      .get<unknown>(`/api/events/${eventId}/registrations`, { params: { ...filter, ...sort, ...pagination } })
      .pipe(transformAndValidate(EventRegistrationListDto), logError());
  }

  getOneEventRegistration(eventId: string, registrationId: string) {
    return this.http
      .get<EventRegistrationDto>(`/api/events/${eventId}/registrations/${registrationId}`)
      .pipe(transformAndValidate(EventRegistrationWithDataDto), logError());
  }

  isMyEmail(email: string) {
    return this.authService.user?.email === email;
  }

  isEventOwner(event: EventDto) {
    return this.authService.user?.email === event.owner.email;
  }

  canEditEvent(event: EventDto) {
    return this.isEventOwner(event) || event.editors.some(editor => editor.email === this.authService.user?.email);
  }
}
