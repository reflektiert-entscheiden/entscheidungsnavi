import { VideoCategory } from '@entscheidungsnavi/api-types';
import { Type } from 'class-transformer';
import { IsDate, IsMongoId, IsNotEmpty, IsNumber, IsString } from 'class-validator';

export class YoutubeVideoCategoryDto implements VideoCategory {
  @IsMongoId()
  id: string;

  @IsNotEmpty()
  @IsString()
  name: string;

  @IsNumber()
  orderIdx: number;

  @IsNumber()
  videoCount: number;

  @IsDate()
  @Type(() => Date)
  createdAt: Date;

  @IsDate()
  @Type(() => Date)
  updatedAt: Date;
}
