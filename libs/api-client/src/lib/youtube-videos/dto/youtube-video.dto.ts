import { YoutubeVideo } from '@entscheidungsnavi/api-types';
import { Type } from 'class-transformer';
import { IsDate, IsDefined, IsMongoId, IsNotEmpty, IsNumber, IsOptional, IsString } from 'class-validator';
import { Types } from 'mongoose';

export class YoutubeVideoDto implements YoutubeVideo {
  @IsMongoId()
  id: string;

  @IsNotEmpty()
  @IsString()
  name: string;

  @IsNotEmpty()
  @IsString()
  youtubeId: string;

  @IsOptional()
  @IsNumber()
  startTime: number;

  @IsDefined()
  @IsNumber({}, { each: true })
  stepNumbers: number[];

  @IsDefined()
  @IsMongoId({ each: true })
  categoryIds: Types.ObjectId[];

  @IsDate()
  @Type(() => Date)
  createdAt: Date;

  @IsDate()
  @Type(() => Date)
  updatedAt: Date;
}
