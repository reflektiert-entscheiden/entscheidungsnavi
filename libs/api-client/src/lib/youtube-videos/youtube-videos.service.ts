import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { logError, transformAndValidate } from '@entscheidungsnavi/tools';
import { Types } from 'mongoose';
import { YoutubeVideoDto } from './dto/youtube-video.dto';
import { YoutubeVideoCategoryDto } from './dto/youtube-video-category.dto';

@Injectable({
  providedIn: 'root',
})
export class YoutubeVideosService {
  constructor(private http: HttpClient) {}

  getVideoCategoriesList() {
    return this.http.get<unknown[]>('/api/youtube-videos/categories').pipe(transformAndValidate(YoutubeVideoCategoryDto), logError());
  }

  addCategory(data: { name: string }) {
    const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    return this.http
      .post<unknown>('/api/youtube-videos/categories', data, { headers })
      .pipe(transformAndValidate(YoutubeVideoCategoryDto), logError());
  }

  updateCategory(id: string, update: { name: string }) {
    const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    return this.http
      .patch<unknown>(`/api/youtube-videos/categories/${id}`, update, { headers })
      .pipe(transformAndValidate(YoutubeVideoCategoryDto), logError());
  }

  changeCategoryPosition(orderUpdate: { categoryId: string; oldIndex: number; newIndex: number }) {
    const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    return this.http
      .post<unknown[]>(`/api/youtube-videos/categories/order`, orderUpdate, { headers })
      .pipe(transformAndValidate(YoutubeVideoCategoryDto), logError());
  }

  deleteCategory(id: string) {
    return this.http.delete(`/api/youtube-videos/categories/${id}`, { observe: 'response' }).pipe(logError());
  }

  getYoutubeVideosList() {
    return this.http.get<unknown[]>('/api/youtube-videos/videos').pipe(transformAndValidate(YoutubeVideoDto), logError());
  }

  addVideo(data: { name: string; youtubeId: string; startTime: number; stepNumbers: number[]; categoryIds: Types.ObjectId[] }) {
    const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    return this.http.post<unknown>('/api/youtube-videos/videos', data, { headers }).pipe(transformAndValidate(YoutubeVideoDto), logError());
  }

  updateVideo(
    id: string,
    update: {
      name: string;
      youtubeId: string;
      startTime: number;
      stepNumbers: number[];
      categoryIds: Types.ObjectId[];
    },
  ) {
    const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    return this.http
      .patch<unknown>(`/api/youtube-videos/videos/${id}`, update, { headers })
      .pipe(transformAndValidate(YoutubeVideoDto), logError());
  }

  deleteVideo(id: string) {
    return this.http.delete(`/api/youtube-videos/videos/${id}`, { observe: 'response' }).pipe(logError());
  }
}
