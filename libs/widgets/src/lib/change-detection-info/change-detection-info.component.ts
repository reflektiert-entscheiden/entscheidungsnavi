import { ApplicationRef, ChangeDetectorRef, Component, DoCheck, NgZone, inject } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { Router } from '@angular/router';
import 'zone.js';
import { MatButtonModule } from '@angular/material/button';
import { startJSProfile, stopJSProfile } from './js-profiler';

@Component({
  selector: 'dt-change-detection-info',
  standalone: true,
  imports: [CommonModule, MatCheckboxModule, MatButtonModule],
  templateUrl: './change-detection-info.component.html',
  styleUrls: ['./change-detection-info.component.scss'],
})
export class ChangeDetectionInfoComponent implements DoCheck {
  indicatorOpacity = 0;
  indicatorCounter = 0;

  logCDDetails = false;

  profiling = false;
  cdRuns = -1;
  avgCdTime = -1;

  router = inject(Router);
  cdRef = inject(ChangeDetectorRef);
  appRef = inject(ApplicationRef);
  zone = inject(NgZone);

  resetCounter() {
    this.indicatorCounter = -1;
  }

  ngDoCheck() {
    if (this.logCDDetails && !this.profiling && Zone.currentTask) {
      console.log('Source: ' + Zone.currentTask.source);
      this.renderLongStackTrace();
    }

    if (this.indicatorOpacity < 1 && !this.profiling) {
      this.indicatorOpacity += 0.05;
      this.indicatorCounter++;

      this.zone.runOutsideAngular(() => {
        setTimeout(() => {
          this.indicatorOpacity -= 0.05;
          this.cdRef.detectChanges();
        }, 1000);
      });
      this.cdRef.detectChanges();
    }
  }

  async profileActiveRoute() {
    startJSProfile('Routing');

    const url = this.router.url;

    const runCount = 5;

    let sum = 0;
    for (let i = 0; i < runCount; i++) {
      await this.router.navigateByUrl('/dummy');
      this.appRef.tick();
      const begin = performance.now();
      await this.router.navigateByUrl(url);
      this.appRef.tick();

      sum += performance.now() - begin;
    }
    stopJSProfile('Routing');

    console.log('Average: ' + sum / runCount + 'ms');
  }

  timeChangeDetection(doProfile: boolean) {
    this.profiling = true;

    const record = doProfile;
    const profileName = 'Change Detection';

    this.cdRuns = this.avgCdTime = -1;
    this.cdRef.detectChanges();

    if (record) {
      startJSProfile(profileName);
    }
    const start = performance.now();
    let numTicks = 0;
    while (numTicks < 5 || performance.now() - start < 500) {
      this.appRef.tick();
      numTicks++;
    }
    const end = performance.now();
    if (record) {
      stopJSProfile(profileName);
    }
    const msPerTick = (end - start) / numTicks;

    this.cdRuns = numTicks;
    this.avgCdTime = msPerTick;

    this.profiling = false;
  }

  renderLongStackTrace() {
    const frames: { error: { stack: string } }[] = (Zone.currentTask?.data as any)?.__creationTrace__;

    if (!frames) {
      console.groupCollapsed('Stacks');
      console.log('no frames');
      console.groupEnd();
      return;
    }

    const NEWLINE = '\n';

    // edit this array if you want to ignore or unignore something
    const FILTER_REGEXP: RegExp[] = [
      /checkAndUpdateView/,
      /callViewAction/,
      /execEmbeddedViewsAction/,
      /execComponentViewsAction/,
      /callWithDebugContext/,
      /debugCheckDirectivesFn/,
      /Zone/,
      /checkAndUpdateNode/,
      /debugCheckAndUpdateNode/,
      /onScheduleTask/,
      /onInvoke/,
      /updateDirectives/,
      /@angular/,
      /Observable\._trySubscribe/,
      /Observable.subscribe/,
      /SafeSubscriber/,
      /Subscriber.js.Subscriber/,
      /checkAndUpdateDirectiveInline/,
      /drainMicroTaskQueue/,
      /getStacktraceWithUncaughtError/,
      /LongStackTrace/,
      /Observable._zoneSubscribe/,
    ];

    const filterFrames = (stack: string) => {
      return stack
        .split(NEWLINE)
        .filter(frame => !FILTER_REGEXP.some(reg => reg.test(frame)))
        .join(NEWLINE);
    };

    console.groupCollapsed('Stacks');
    frames
      .filter(frame => frame.error.stack)
      .map(frame => filterFrames(frame.error.stack))
      .forEach(frame => {
        console.log(frame);
      });
    console.groupEnd();
  }
}
