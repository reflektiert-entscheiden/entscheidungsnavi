import { NgModule } from '@angular/core';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatIconModule } from '@angular/material/icon';
import { CommonModule } from '@angular/common';
import { WidgetsModule } from '../widgets.module';
import { RichTextEditorComponent } from '../rich-text-editor';
import { ModalModule } from '../modal/modal.module';
import { DynamicChangeDirectiveModule } from '../directives';
import { DiagramLegendComponent } from '../diagram-legend';
import {
  AreaComponent,
  CircleComponent,
  CircleSeriesComponent,
  LineAreaChartComponent,
  LineComponent,
  TextComponent,
  XAxisComponent,
  XAxisTicksComponent,
} from '.';

const DECLARE_AND_EXPORT = [LineAreaChartComponent];

const DECLARE = [AreaComponent, CircleComponent, CircleSeriesComponent, LineComponent, TextComponent, XAxisComponent, XAxisTicksComponent];

@NgModule({
  imports: [
    CommonModule,
    DiagramLegendComponent,
    NgxChartsModule,
    RichTextEditorComponent,
    ModalModule,
    MatProgressSpinnerModule,
    MatIconModule,
    DynamicChangeDirectiveModule,
    WidgetsModule,
  ],
  declarations: [...DECLARE_AND_EXPORT, ...DECLARE],
  exports: [...DECLARE_AND_EXPORT],
})
export class ChartsModule {}
