import {
  AfterViewInit,
  ChangeDetectorRef,
  Component,
  DoCheck,
  ElementRef,
  Input,
  NgZone,
  OnInit,
  QueryList,
  ViewChild,
  ViewChildren,
} from '@angular/core';
import { max, range } from 'lodash';
import { moveItemInArray } from '@angular/cdk/drag-drop';

import { DiagramLegendComponent } from '../../diagram-legend';

export type PolarChartItem = {
  id: number;
  label: string;
  values: number[];
};

const twoPI = 2 * Math.PI;
interface Point {
  x: number;
  y: number;
}
@Component({
  selector: 'dt-polar-chart',
  templateUrl: './polar-chart.component.html',
  styleUrls: ['./polar-chart.component.scss'],
  standalone: true,
  imports: [DiagramLegendComponent],
})
export class PolarChartComponent implements DoCheck, OnInit, AfterViewInit {
  static pointToStr(point: Point): string {
    return `${point.x} ${point.y}`;
  }

  static polarToCartesian(r: number, phi: number): Point {
    return { x: r * Math.sin(phi), y: -r * Math.cos(phi) };
  }

  static getPath(points: Point[]): string {
    let line = '';
    points.forEach((p: Point, idx: number) => {
      if (idx === 0) {
        line += 'M' + PolarChartComponent.pointToStr(p) + ' ';
      } else {
        line += 'L' + PolarChartComponent.pointToStr(p) + ' ';
      }
    });

    // path from end point to start point, closes path
    if (points.length > 2) {
      line += 'L' + PolarChartComponent.pointToStr(points[0]) + 'z';
    }
    return line;
  }

  static getPhiScale(numOfValues: number): number[] {
    return Array(numOfValues)
      .fill(0)
      .map((val: number, idx: number) => (twoPI * idx) / numOfValues);
  }

  @ViewChild('scrollContainer') scrollContainer: ElementRef<HTMLDivElement>;

  @ViewChildren('label', { read: ElementRef }) labels = new QueryList<ElementRef<HTMLDivElement>>();

  // Dimension: #Items
  private _items: PolarChartItem[];

  get items() {
    return this._items;
  }
  @Input()
  set items(newItems: PolarChartItem[]) {
    this._items = newItems;
    // If we have an item selected and the item is not provided anymore then deselect the item.
    if (this.selectedItemIndex === -1) this.selectedItemId = -1;
    this.itemColorsForSelectedItems = this.items.map(item => this.itemColors[this.getColorIndexFor(item)]);
  }

  @Input() maxValue: number;

  // Dimension: #Dimensions
  @Input() dimensionLabels: string[];

  @Input() valuesAreRelative = false;

  readonly padding = [20, 0, 20, 0];

  readonly plotLabelGap = 30;

  readonly donutHoleRadius = 22.5;

  readonly ringPointRadius = 3;

  // Dimension: max({Item.id | Item in Items})
  protected readonly itemColors: string[] = [
    'hsl(208deg 35% 40%)',
    'hsl(0deg 50% 40%)',
    'hsl(28deg 75% 45%)',
    'hsl(448deg 35% 40%)',
    'hsl(268deg 35% 40%)',
    'hsl(174 50% 40%)',
  ];

  protected itemColorsForSelectedItems = this.itemColors;

  protected selectedItemId = -1;

  protected translatePlotToCenter: string;

  protected phiTicks: Point[][];

  private phiScale: number[];

  protected radiusScale: number[];

  protected hasScrollbar = false;

  protected gridPaths: string[];

  protected donutHolePath: string;

  protected maxWidthLabel = 0;
  protected maxHeightLabel = 0;

  readonly outerRadius = 170;

  get itemLabels() {
    return this.items.map(item => item.label);
  }

  get maxPlotValue(): number {
    if (!this.valuesAreRelative) return this.maxValue;

    return this.items.reduce((currMaxValue, item) => Math.max(currMaxValue, max(item.values)), 0);
  }

  get polarChartBackgroundPath(): string {
    const pointsForBackgroundPath = this.phiScale.map((phi: number) => PolarChartComponent.polarToCartesian(this.outerRadius, phi));
    return PolarChartComponent.getPath(pointsForBackgroundPath);
  }

  get numberOfItems() {
    return this.items.length;
  }

  get numberOfDimensions() {
    return this.dimensionLabels.length;
  }

  get selectedItemIndex() {
    return this.items.findIndex(item => item.id === this.selectedItemId);
  }

  constructor(
    private ngZone: NgZone,
    private cdRef: ChangeDetectorRef,
  ) {}

  ngOnInit() {
    this.translatePlotToCenter = `translate(${this.outerRadius}, ${this.outerRadius})`;
    this.phiScale = this.getPhiScale(this.numberOfDimensions);

    this.setRadiusScale();
    this.setPhiTicks();

    this.gridPaths = this.radiusScale.map((r: number, _idx: number) => {
      const points = PolarChartComponent.getPhiScale(this.numberOfDimensions).map((phi: number) =>
        PolarChartComponent.polarToCartesian(r, phi),
      );
      return PolarChartComponent.getPath(points);
    });

    this.donutHolePath = PolarChartComponent.getPath(
      this.phiScale.map((phi: number) => PolarChartComponent.polarToCartesian(this.donutHoleRadius - 1, phi)),
    );
  }

  ngDoCheck() {
    this.hasScrollbar =
      this.scrollContainer && this.scrollContainer.nativeElement.offsetWidth - this.scrollContainer.nativeElement.clientWidth > 0;
  }

  /**
   * Get all the angles for the r values; get phi
   *
   * @param numberOfDimensions - number of dimensions
   */
  getPhiScale(numberOfDimensions: number): number[] {
    return Array(numberOfDimensions)
      .fill(0)
      .map((val: number, idx: number) => (twoPI * idx) / numberOfDimensions);
  }

  /**
   * Get distance from the center; get r
   *
   * @param valuePercentage - 0 means center of plot, 100 means border of plot
   */
  getRadialDistance(valuePercentage: number): number {
    return valuePercentage * (this.outerRadius - this.donutHoleRadius);
  }

  setRadiusScale() {
    const n = 5;
    this.radiusScale = Array(n)
      .fill(0)
      .map((val: number, idx: number) => this.donutHoleRadius + this.getRadialDistance(idx + 1) / n);
  }

  /**
   * Set ticks for angle
   */
  setPhiTicks() {
    this.phiTicks = range(this.numberOfDimensions).map(dimension => {
      const startPoint = { x: 0, y: 0 }; // from the center
      const endPoint = PolarChartComponent.polarToCartesian(this.outerRadius, this.phiScale[dimension]);
      return [startPoint, endPoint];
    });
  }

  getPath(points: Point[]) {
    return PolarChartComponent.getPath(points);
  }

  getItemsWithRingPoints(): { item: PolarChartItem; ringPoints: Point[] }[] {
    const maxVal = this.maxPlotValue;

    const itemsWithRingPoints = this.items.map(item => ({
      item,
      ringPoints: item.values.map((value: number, dimensionIndex: number) =>
        PolarChartComponent.polarToCartesian(this.donutHoleRadius + this.getRadialDistance(value / maxVal), this.phiScale[dimensionIndex]),
      ),
    }));

    // The selected alternative should be rendered last (should be placed on top).
    if (this.selectedItemId !== -1) {
      moveItemInArray(itemsWithRingPoints, this.selectedItemIndex, this.numberOfItems - 1);
    }

    return itemsWithRingPoints;
  }

  toggleSelectedItem(itemIndex: number) {
    this.selectedItemId = this.selectedItemId == itemIndex ? -1 : itemIndex;
  }

  transformForLabel(dimensionIndex: number): string {
    const phi = this.phiScale[dimensionIndex];
    const r = this.outerRadius + this.plotLabelGap;
    const p = PolarChartComponent.polarToCartesian(r, phi);

    const cardinalDirections = [0, twoPI / 4, (2 * twoPI) / 4, (3 * twoPI) / 4, twoPI];
    const directionToAdjustCoords = [
      [0, -50], // North
      [50, -50],
      [50, 0], // East
      [50, 50],
      [0, 50], // South
      [-50, 50],
      [-50, 0], // West
      [-50, -50],
    ];

    let adjustX = -1;
    let adjustY = -1;
    for (const [index, cardinalDirection] of cardinalDirections.entries()) {
      if (phi <= cardinalDirection) {
        const isInCardinalDirection = phi === cardinalDirection;
        [adjustX, adjustY] = directionToAdjustCoords[2 * index - +!isInCardinalDirection];
        break;
      }
    }

    return `translate(-50%, -50%) translate(${p.x}px, ${p.y}px) translate(${adjustX}%, ${adjustY}%)`;
  }

  getColorIndexFor(item: PolarChartItem) {
    return item.id % this.itemColors.length;
  }

  ngAfterViewInit() {
    [this.maxWidthLabel, this.maxHeightLabel] = this.labels.toArray().reduce(
      ([currMaxWidthLabel, currMaxHeightLabel], labelRef) => {
        return [
          Math.max(currMaxWidthLabel, labelRef.nativeElement.offsetWidth),
          Math.max(currMaxHeightLabel, labelRef.nativeElement.offsetHeight),
        ];
      },
      [0, 0],
    );

    this.cdRef.detectChanges();

    this.ngZone.runOutsideAngular(() => {
      requestAnimationFrame(() => {
        this.scrollContainer.nativeElement.scrollBy(
          (this.scrollContainer.nativeElement.scrollWidth - this.scrollContainer.nativeElement.clientWidth) / 2,
          (this.scrollContainer.nativeElement.scrollHeight - this.scrollContainer.nativeElement.clientHeight) / 2,
        );
      });
    });
  }
}
