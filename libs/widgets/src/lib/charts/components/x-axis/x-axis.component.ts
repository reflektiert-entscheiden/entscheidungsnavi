import { ChangeDetectionStrategy, Component, EventEmitter, Input, OnChanges, Output } from '@angular/core';
import { Orientation } from '@swimlane/ngx-charts';
import { Axis, ExtendedViewDimensions, ScaleType, XScale } from '../../common';

/**
 * This very closely resembles the eponymous component in ngx-charts.
 */

@Component({
  // eslint-disable-next-line @angular-eslint/component-selector
  selector: 'g[dt-charts-x-axis]',
  templateUrl: './x-axis.component.html',
  styleUrls: ['./x-axis.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class XAxisComponent implements OnChanges {
  @Input() xScale: XScale;
  @Input() xScaleType: ScaleType;
  @Input() xAxis: Axis;
  @Input() dims: ExtendedViewDimensions;

  @Output() dimensionsChanged = new EventEmitter<{ height: number }>();

  readonly axisOrientation = Orientation.Bottom;

  transform: string;
  labelOffset = 0;
  padding = 5;

  ngOnChanges() {
    this.transform = `translate(0, ${this.padding + this.dims.height})`;
  }

  emitTicksHeight({ height }: { height: number }) {
    const newLabelOffset = height + 25 + 5;
    if (newLabelOffset !== this.labelOffset) {
      this.labelOffset = newLabelOffset;
      setTimeout(() => {
        this.dimensionsChanged.emit({ height });
      }, 0);
    }
  }
}
