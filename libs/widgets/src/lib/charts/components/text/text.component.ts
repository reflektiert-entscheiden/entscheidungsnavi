import { animate, state, style, transition, trigger } from '@angular/animations';
import { ChangeDetectionStrategy, Component, ElementRef, Input, OnChanges, SimpleChanges, ViewChild } from '@angular/core';
import { select } from 'd3-selection';

@Component({
  // eslint-disable-next-line @angular-eslint/component-selector
  selector: 'g[dt-charts-text]',
  templateUrl: './text.component.html',
  styleUrls: ['./text.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  animations: [
    trigger('showHide', [
      transition('false => true', animate('250ms ease-in')),
      transition('true => false', animate('250ms ease-out')),
      state('true', style({ opacity: 1 })),
      state('*', style({ opacity: 0 })),
    ]),
  ],
})
export class TextComponent implements OnChanges {
  @Input() text: string;
  @Input() color: string;
  @Input() baseline: 'baseline' | 'middle' | 'hanging';
  @Input() x: number;
  @Input() y: number;
  @Input() anchor: 'start' | 'middle' | 'end';
  @Input() visible = true;
  @Input() animations = true;

  @ViewChild('element', { static: true }) element: ElementRef;

  ngOnChanges(changes: SimpleChanges) {
    if ('x' in changes || 'y' in changes || 'anchor' in changes || 'text' in changes) {
      this.redraw();
    }
  }

  private redraw() {
    const node = select(this.element.nativeElement);

    if (this.animations) {
      node.transition().duration(250);
    }

    node.attr('x', this.x).attr('y', this.y).attr('dx', this.getAnchorDx());
  }

  /**
   * Calculate the shift along the x-axis necessary for the anchor.
   * The built-in text-anchor of SVG is not used, because it can not be animated.
   */
  private getAnchorDx(): string {
    if (this.anchor === 'start') {
      return '0';
    } else if (this.anchor === 'middle') {
      return -this.element.nativeElement.getBBox().width / 2 + 'px';
    } else {
      // end
      return -this.element.nativeElement.getBBox().width + 'px';
    }
  }
}
