import { Attribute, Directive, ElementRef, Input } from '@angular/core';
import { AbstractSelectItemDirective } from './select-item.directive';

@Directive({
  // eslint-disable-next-line @angular-eslint/directive-selector
  selector: 'dt-select-option',
  providers: [{ provide: AbstractSelectItemDirective, useExisting: SelectOptionDirective }],
})
export class SelectOptionDirective extends AbstractSelectItemDirective {
  @Input()
  disabled = false;

  @Input()
  grayedOutButNotDisabled = false;

  @Input()
  showAsGroupLabel = false;

  @Input()
  tooltipWhenDisabled: string | null = null;

  @Input()
  icon: string = null;

  get text() {
    return this.elementRef.nativeElement.textContent.trim();
  }

  constructor(
    private elementRef: ElementRef,
    @Attribute('data-cy') public dataCy: string,
  ) {
    super();
  }
}
