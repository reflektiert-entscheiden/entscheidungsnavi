import {
  AfterContentInit,
  Component,
  ContentChild,
  ContentChildren,
  EventEmitter,
  HostBinding,
  Input,
  Output,
  QueryList,
} from '@angular/core';
import { cloneDeep, flatten, isArray, remove, sum } from 'lodash';
import { SelectLabelDirective } from './select-label.directive';
import { AbstractSelectItemDirective } from './select-item.directive';
import { SelectOptionDirective } from './select-option.directive';
import { SelectGroupDirective } from './select-group.directive';

/*
    TODO: checkout the links below and check whether we can properly link to `SensitivityAnalysisComponent`.
    - https://tsdoc.org/pages/tags/link/
    - https://github.com/microsoft/tsdoc/issues/9
 */

/**
 * Component that partially mimics {@link https://material.angular.io/components/select/api|MatSelect} but with a slimmer design
 * and support for option groups (via `dt-select-group`).
 *
 * @remarks
 * Currently, `dt-select` only supports one-level nesting with `dt-select-group`s which means that following is disallowed:
 *
 * @example
 * ```
 * <dt-select [multiple]="true" [(selectedOptionsGrouped)]="selectedOptionsGrouped">
 *   <dt-select-label i18n>My Select Label</dt-select-label>
 *     <dt-select-group [value]="'parent-group'">
 *       <dt-select-label i18n>Parent Group</dt-select-label>
 *       <dt-select-option [value]="'parent-option'" i18n>First Option</dt-select-option>
 *     <dt-select-group [value]="'child-group'">
 *       <dt-select-label i18n>Child Group</dt-select-label>
 *       <dt-select-option  [value]="'child-option'" i18n>First Option</dt-select-option>
 *     </dt-select-group>
 *   </dt-select-group>
 * </dt-select>
 * ```
 *
 * Using `dt-select-group` in single-mode (`multiple === false`) is also unsupported.
 *
 * @see {@link SensitivityAnalysisComponent} - how to use `dt-select` with groups.
 */

type SimpleSelection = any | any[];
type GroupedSelection = any;

@Component({
  selector: 'dt-select',
  templateUrl: './select.component.html',
  styleUrls: ['./select.component.scss'],
})
export class SelectComponent implements AfterContentInit {
  static readonly ROOT_GROUP = 'root';

  static readonly NO_SELECTED_OPTION_PLACEHOLDER = '—';
  static readonly ALL_SELECTED_PLACEHOLDER = $localize`Alle`;

  rootGroupName = SelectComponent.ROOT_GROUP;

  @ContentChild(SelectLabelDirective) label: SelectLabelDirective;
  @ContentChildren(AbstractSelectItemDirective) groupsAndOptions: QueryList<AbstractSelectItemDirective>;

  @Input() multiple = false;

  // If true, the first option will be visually separated from the rest of the options.
  @Input() seperateFirst = false;

  private groupNameToAllOptions = new Map<string, any[]>();

  private _selectedOptionsGrouped: GroupedSelection = { [SelectComponent.ROOT_GROUP]: [] } as GroupedSelection;

  get selectedOptionsGrouped(): GroupedSelection {
    return this._selectedOptionsGrouped;
  }

  @Input()
  set selectedOptionsGrouped(newSelectedOptionsGrouped: GroupedSelection) {
    this._selectedOptionsGrouped = newSelectedOptionsGrouped;
  }

  @Output() selectedOptionsGroupedChange = new EventEmitter<GroupedSelection>();

  get selectedOptions(): SimpleSelection {
    const options = flatten(Object.values(this.selectedOptionsGrouped));
    return this.multiple ? options : options[0];
  }

  @Input()
  set selectedOptions(newSelectedOptions: SimpleSelection) {
    if (!isArray(newSelectedOptions)) newSelectedOptions = [newSelectedOptions];

    this.selectedOptionsGrouped[SelectComponent.ROOT_GROUP] = newSelectedOptions;

    // We need to set textOfLastSelected if multiple === false
    if (!this.multiple && this.groupsAndOptions) {
      const item = this.groupsAndOptions.find(item => item.value === newSelectedOptions[0]);
      if (item) {
        this.textOfLastSelected = (item as SelectOptionDirective).text;
      }
    }
  }

  @Output() selectedOptionsChange = new EventEmitter<SimpleSelection>();

  textOfLastSelected: string;

  @Input()
  set disabled(value) {
    this._disabled = value;
  }

  get disabled(): boolean {
    return this._disabled || !this.groupsAndOptions || this.groupsAndOptions.length === 0;
  }

  get oneOptionHasIcon() {
    return this.groupsAndOptions.some(item => item instanceof SelectOptionDirective && item.icon != null);
  }

  private _disabled: boolean;

  openedGroups = new Set<string>([SelectComponent.ROOT_GROUP]);

  selectionListOpened = false;

  @HostBinding('attr.data-cy')
  get textForSelected(): string {
    const optionValues = this.selectedOptions;
    if (optionValues == null || optionValues.length === 0) return SelectComponent.NO_SELECTED_OPTION_PLACEHOLDER;

    if (this.multiple) {
      const numberOfOptions = sum(
        this.groupsAndOptions.map(item => (item instanceof SelectGroupDirective ? item.groupedOptions.length : 1)),
      );

      return this.selectedOptions.length === numberOfOptions
        ? SelectComponent.ALL_SELECTED_PLACEHOLDER
        : this.selectedOptions.length + ' ' + $localize`von` + ' ' + numberOfOptions;
    } else {
      return this.textOfLastSelected;
    }
  }

  isOption(selectItem: AbstractSelectItemDirective): selectItem is SelectOptionDirective {
    return selectItem instanceof SelectOptionDirective;
  }

  isGroup(selectItem: AbstractSelectItemDirective): selectItem is SelectGroupDirective {
    return selectItem instanceof SelectGroupDirective;
  }

  ngAfterContentInit(): void {
    this.groupNameToAllOptions.set(SelectComponent.ROOT_GROUP, []);

    const rootOptions: any[] = [];

    this.groupsAndOptions.forEach((item: AbstractSelectItemDirective) => {
      if (item instanceof SelectGroupDirective) {
        const groupValue = item.value;

        const groupedOptions: any[] = [];
        if (this.selectedOptionsGrouped[groupValue] === undefined) throw Error('Unknown group.');

        item.groupedOptions.forEach(optionDirective => {
          groupedOptions.push(optionDirective.value);
          if (!this.multiple && (this.selectedOptions === item.value || this.selectedOptions[0] === item.value)) {
            this.textOfLastSelected = optionDirective.text;
          }
        });

        this.groupNameToAllOptions.set(groupValue, groupedOptions);
      } else {
        rootOptions.push(item.value);
        if (!this.multiple && (this.selectedOptions === item.value || this.selectedOptions?.[0] === item.value)) {
          this.textOfLastSelected = (item as SelectOptionDirective).text;
        }
      }
    });

    this.groupNameToAllOptions.set(SelectComponent.ROOT_GROUP, rootOptions);
  }

  toggleOption(event: MouseEvent, groupName: string, option: any): void {
    if (option.disabled) {
      event.stopPropagation();
      return;
    }

    this.textOfLastSelected = option.text;
    const groupValue = groupName;

    if (this.multiple) {
      if (this.selectedOptionsGrouped[groupValue].includes(option.value)) {
        remove(this.selectedOptionsGrouped[groupValue], (x: any) => option.value === x);
      } else {
        this.selectedOptionsGrouped[groupValue].push(option.value);
      }
    } else {
      this.selectedOptionsGrouped[SelectComponent.ROOT_GROUP] = [option.value];
    }

    this.emitChange();
  }

  toggleGroup(groupName: string): void {
    if (this.selectedOptionsGrouped[groupName].length === this.groupNameToAllOptions.get(groupName).length) {
      this.selectedOptionsGrouped[groupName] = [];
    } else {
      this.selectedOptionsGrouped[groupName] = cloneDeep(this.groupNameToAllOptions.get(groupName));
    }

    this.emitChange();
  }

  toggleFoldingOfGroup(groupName: string): void {
    if (this.openedGroups.has(groupName)) {
      this.openedGroups.delete(groupName);
    } else {
      this.openedGroups.add(groupName);
    }
  }

  emitChange() {
    this.selectedOptionsGroupedChange.emit(this.selectedOptionsGrouped);
    this.selectedOptionsChange.emit(this.selectedOptions);
  }

  isGroupSelected(groupName: string): boolean {
    return this.selectedOptionsGrouped[groupName].length === this.groupNameToAllOptions.get(groupName).length;
  }

  isGroupIndeterminate(groupName: string): boolean {
    return !this.isGroupSelected(groupName) && this.selectedOptionsGrouped[groupName].length > 0;
  }

  isOptionSelected(groupName: string, value: any): boolean {
    return this.selectedOptionsGrouped[groupName].includes(value);
  }
}
