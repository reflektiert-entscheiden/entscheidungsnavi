import { ChangeDetectionStrategy, Component, inject, Input, OnInit } from '@angular/core';
import { LocationSequence } from '@entscheidungsnavi/tools';
import { CommonModule } from '@angular/common';
import { HierarchyInterfaceDirective } from '../interface.directive';

/**
 * This component shows the drag placeholder for the hierarchy.
 */
@Component({
  template: `
    @for (node of nodes; track node; let i = $index) {
      <ng-container
        *ngTemplateOutlet="
          interface.nodeTemplate;
          context: {
            $implicit: treeValues[i],
            locationSequence: node,
            focused: false
          }
        "
      ></ng-container>
    }
  `,
  styles: [
    `
      :host {
        display: flex;
        flex-direction: column;
        background-color: white;
        width: fit-content;
        opacity: 0.6;
      }
    `,
  ],
  imports: [CommonModule],
  changeDetection: ChangeDetectionStrategy.OnPush,
  standalone: true,
})
export class HierarchyDragImageComponent<T> implements OnInit {
  protected interface: HierarchyInterfaceDirective<T> = inject(HierarchyInterfaceDirective);

  @Input({ required: true }) nodes: LocationSequence[];
  protected treeValues: T[];

  ngOnInit() {
    this.treeValues = this.nodes.map(node => this.interface.tree.getNode(node).value);
  }
}
