import { LocationSequence } from '@entscheidungsnavi/tools';

interface DragData {
  locations: ReadonlyArray<number>[];
}

export class HierarchyDragData {
  private static getDragDataType(hierarchyUUID: string) {
    return `application/decision-tool-hierarchy-${hierarchyUUID}`;
  }

  static writeToEvent(event: DragEvent, hierarchyUUID: string, locations: LocationSequence[]) {
    event.dataTransfer.setData(
      HierarchyDragData.getDragDataType(hierarchyUUID),
      JSON.stringify({ locations: locations.map(loc => loc.value) } satisfies DragData),
    );
  }

  static fromEvent(event: DragEvent, hierarchyUUID: string): HierarchyDragData | null {
    const data = event.dataTransfer.getData(HierarchyDragData.getDragDataType(hierarchyUUID));
    if (!data) return null;

    try {
      const parsedData: DragData = JSON.parse(data);

      // Since this is a drag within the same hierarchy, we can assume that the locations are valid
      return new HierarchyDragData(parsedData.locations.map(loc => new LocationSequence(loc)));
    } catch {
      return null;
    }
  }

  static isInEvent(event: DragEvent, hierarchyUUID: string): boolean {
    return event.dataTransfer.types.includes(HierarchyDragData.getDragDataType(hierarchyUUID));
  }

  constructor(public readonly data: LocationSequence[]) {}
}
