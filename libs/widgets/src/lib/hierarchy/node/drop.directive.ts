import { Directive, ElementRef, HostListener, inject, Renderer2 } from '@angular/core';
import { assertUnreachable } from '@entscheidungsnavi/tools/assert-unreachable';
import { HierarchyInterfaceDirective } from '../interface.directive';
import { HierarchyNodeComponent } from './hierarchy-node.component';
import { HierarchyDragData } from './drag-data';

type DropDirection = 'above' | 'below' | 'right';

/**
 * This directive handles dropping onto a hierarchy node.
 */
@Directive({ selector: '[dtHierarchyNodeDrop]', standalone: true })
export class HierarchyNodeDropDirective<T> {
  private interface = inject<HierarchyInterfaceDirective<T>>(HierarchyInterfaceDirective);
  private node = inject<HierarchyNodeComponent<T>>(HierarchyNodeComponent);
  private elementRef = inject<ElementRef<HTMLElement>>(ElementRef);

  private renderer = inject(Renderer2);

  private dropOverlay: HTMLDivElement;

  @HostListener('dragover', ['$event'])
  dragOver(event: DragEvent) {
    event.preventDefault();

    if (this.interface.readonly) {
      event.dataTransfer.dropEffect = 'none';
      return;
    }

    const dropDirection = this.getDropDirection(event);
    const dropTarget = this.getDropTarget(dropDirection);

    // Chrome does not allow reading drag data in the dragover event, so we need to use the stored data
    const dragData = HierarchyDragData.isInEvent(event, this.interface.hierarchyUUID) ? this.interface.activeDrag : null;

    if (dragData) {
      // If we are dragging within the same hierarchy, we need to check whether we are allowed to drop here.
      // If we ourselves are being dragged, or we are a child of a dragged element, we cannot drop here
      for (const node of dragData.data) {
        if (node.equals(this.node.locationSequence) || this.node.locationSequence.isDescendantOf(node)) {
          event.dataTransfer.dropEffect = 'none';
          return;
        }
      }

      event.dataTransfer.dropEffect = 'move';
    } else {
      event.dataTransfer.dropEffect = 'none';
      this.interface.nodeDragOver.emit({ insertLocation: dropTarget, event });
    }

    // This may be set by us (if we are dragging within ourselves) or by the listener for the nodeDragOver event
    if (event.dataTransfer.dropEffect === 'none') return;

    if (!this.dropOverlay) {
      this.dropOverlay = this.renderer.createElement('div');
      this.renderer.setStyle(this.dropOverlay, 'position', 'absolute');
      this.renderer.setStyle(this.dropOverlay, 'left', '0');
      this.renderer.setStyle(this.dropOverlay, 'right', '0');
      this.renderer.setStyle(this.dropOverlay, 'top', '0');
      this.renderer.setStyle(this.dropOverlay, 'bottom', '0');
      this.renderer.setStyle(this.dropOverlay, 'pointerEvents', 'none');
      this.renderer.appendChild(this.elementRef.nativeElement, this.dropOverlay);
    }

    switch (dropDirection) {
      case 'below':
        this.renderer.setStyle(this.dropOverlay, 'boxShadow', 'inset 0px -5px 0px -2px orange');
        break;
      case 'above':
        this.renderer.setStyle(this.dropOverlay, 'boxShadow', 'inset 0px 5px 0px -2px orange');
        break;
      case 'right':
        this.renderer.setStyle(this.dropOverlay, 'boxShadow', 'inset -5px 0px 0px -2px orange');
        break;
      default:
        assertUnreachable(dropDirection);
    }
  }

  @HostListener('drop', ['$event'])
  drop(event: DragEvent) {
    event.preventDefault();

    if (this.interface.readonly) return;

    const dropDirection = this.getDropDirection(event);
    const to = this.getDropTarget(dropDirection);
    this.clearDrop();

    const dragData = HierarchyDragData.fromEvent(event, this.interface.hierarchyUUID);

    if (dragData) {
      // We have an internal move
      this.interface.move(dragData.data, to);
    } else {
      // We have some external drag data
      this.interface.drop(event, to, this.elementRef.nativeElement);
    }
  }

  @HostListener('dragleave')
  @HostListener('dragend')
  clearDrop() {
    if (this.dropOverlay) {
      this.renderer.removeChild(this.elementRef, this.dropOverlay);
      this.dropOverlay = null;
    }
  }

  private getDropTarget(direction: DropDirection) {
    switch (direction) {
      case 'above':
        return this.node.locationSequence;
      case 'below':
        return this.node.locationSequence.getNextSibling();
      case 'right':
        return this.node.locationSequence.getChild(0);
      default:
        assertUnreachable(direction);
    }
  }

  private getDropDirection(event: DragEvent): DropDirection {
    if (this.node.locationSequence.isRoot()) return 'right';

    const target = event.target as HTMLElement;

    const x = event.offsetX;
    const y = event.offsetY;
    const height = target.offsetHeight;
    const width = target.offsetWidth;

    if (x / width > 0.7) {
      return 'right';
    } else {
      return y > height / 2 ? 'below' : 'above';
    }
  }
}
