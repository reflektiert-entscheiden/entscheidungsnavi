import {
  AfterViewInit,
  ApplicationRef,
  ComponentRef,
  createComponent,
  Directive,
  ElementRef,
  EnvironmentInjector,
  HostListener,
  inject,
  Injector,
  NgZone,
  OnDestroy,
  Renderer2,
} from '@angular/core';
import { LocationSequence } from '@entscheidungsnavi/tools';
import { isEqual } from 'lodash';
import { HierarchyInterfaceDirective } from '../interface.directive';
import { HierarchyNodeStateDirective } from '../node-state.directive';
import { HierarchyDragImageComponent } from './drag-image.component';
import { HierarchyNodeComponent } from './hierarchy-node.component';
import { HierarchyDragData } from './drag-data';

/**
 * This directive handles dragging (NOT dropping) of a hierarchy node.
 */
@Directive({ selector: '[dtHierarchyNodeDrag]', standalone: true })
export class HierarchyNodeDragDirective<T> implements AfterViewInit, OnDestroy {
  private nodeState = inject<HierarchyNodeStateDirective<T>>(HierarchyNodeStateDirective);
  private interface = inject<HierarchyInterfaceDirective<T>>(HierarchyInterfaceDirective);
  private node = inject<HierarchyNodeComponent<T>>(HierarchyNodeComponent);
  private elementRef = inject<ElementRef<HTMLElement>>(ElementRef);

  private environmentInjector = inject(EnvironmentInjector);
  private elementInjector = inject(Injector);
  private appRef = inject(ApplicationRef);
  private renderer = inject(Renderer2);
  private zone = inject(NgZone);

  private dragElementComponent: ComponentRef<HierarchyDragImageComponent<T>>;

  ngAfterViewInit() {
    this.setDraggable(true);

    this.zone.runOutsideAngular(() => {
      // Bind to all inputs elements within us and disable dragging when they are focused.
      // Otherwise, the cursor position cannot be changed using the mouse.
      Array.from(this.elementRef.nativeElement.getElementsByTagName('input')).forEach(input => {
        input.addEventListener('mousedown', () => this.setDraggable(false));
        input.addEventListener('mouseup', () => this.setDraggable(true));
        input.addEventListener('blur', () => this.setDraggable(true));
      });
    });
  }

  private setDraggable(state: boolean) {
    if (state) {
      this.renderer.setAttribute(this.elementRef.nativeElement, 'draggable', 'true');
    } else {
      this.renderer.removeAttribute(this.elementRef.nativeElement, 'draggable');
    }
  }

  ngOnDestroy() {
    this.destroyDragImage();
  }

  @HostListener('dragstart', ['$event'])
  nodeDragStart(event: DragEvent) {
    if (!this.nodeState.focused().find(element => isEqual(element, this.node.locationSequence))) {
      this.nodeState.setFocus(this.node.locationSequence);
    }

    const dragElements = this.nodeState.focused();

    this.createDragImage(dragElements);
    event.dataTransfer.setDragImage(this.dragElementComponent.location.nativeElement, event.offsetX, event.offsetY);

    HierarchyDragData.writeToEvent(event, this.interface.hierarchyUUID, dragElements);
    // Chrome does not allow reading drag data in the dragover event, so we need to store the data locally
    this.interface.activeDrag = new HierarchyDragData(dragElements);
    this.interface.nodeDragStart.emit({ locations: dragElements, event });
  }

  private createDragImage(nodes: LocationSequence[]) {
    this.destroyDragImage();

    this.dragElementComponent = createComponent(HierarchyDragImageComponent<T>, {
      environmentInjector: this.environmentInjector,
      elementInjector: this.elementInjector,
    });
    this.dragElementComponent.setInput('nodes', nodes);

    this.renderer.setStyle(this.dragElementComponent.location.nativeElement, 'position', 'absolute');
    this.renderer.setStyle(this.dragElementComponent.location.nativeElement, 'top', '-500px');
    document.body.appendChild(this.dragElementComponent.location.nativeElement);
    this.appRef.attachView(this.dragElementComponent.hostView);
  }

  private destroyDragImage() {
    if (!this.dragElementComponent) return;

    this.appRef.detachView(this.dragElementComponent.hostView);
    document.body.removeChild(this.dragElementComponent.location.nativeElement);
    this.dragElementComponent.destroy();
    this.dragElementComponent = null;
  }
}
