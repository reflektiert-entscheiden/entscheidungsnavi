import {
  ChangeDetectionStrategy,
  Component,
  computed,
  effect,
  ElementRef,
  inject,
  Input,
  OnChanges,
  signal,
  SimpleChanges,
  ViewChild,
} from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatIconModule } from '@angular/material/icon';
import { LocationSequence, Tree } from '@entscheidungsnavi/tools';
import { HierarchyNodeStateDirective } from '../node-state.directive';
import { HierarchyInterfaceDirective } from '../interface.directive';
import { HierarchyDragImageComponent } from './drag-image.component';
import { HierarchyNodeDragDirective } from './drag.directive';
import { HierarchyNodeDropDirective } from './drop.directive';

@Component({
  selector: 'dt-hierarchy-node',
  templateUrl: './hierarchy-node.component.html',
  styleUrls: ['./hierarchy-node.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  standalone: true,
  imports: [CommonModule, MatIconModule, HierarchyDragImageComponent, HierarchyNodeDragDirective, HierarchyNodeDropDirective],
})
export class HierarchyNodeComponent<T> implements OnChanges {
  @Input({ required: true }) tree: Tree<T>;
  @Input({ required: true }) locationSequence: LocationSequence;

  private readonly locationSequenceSig = signal<LocationSequence>(null);
  readonly addButtonsOpen = signal(false);

  readonly focused = computed(() => {
    const location = this.locationSequenceSig();
    return location && this.nodeState.focused().find(element => element.equals(location)) != null;
  });

  readonly singleFocused = computed(() => {
    // Whether this node is the ONLY focused one
    const location = this.locationSequenceSig();
    const focused = this.nodeState.focused();
    return location && focused.length === 1 && focused[0].equals(location);
  });

  nodeState = inject<HierarchyNodeStateDirective<T>>(HierarchyNodeStateDirective);
  interface = inject<HierarchyInterfaceDirective<T>>(HierarchyInterfaceDirective);

  @ViewChild('nodeWrapper') nodeWrapper: ElementRef<HTMLDivElement>;

  constructor() {
    effect(
      () => {
        if (!this.singleFocused()) {
          this.addButtonsOpen.set(false);
        }
      },
      { allowSignalWrites: true },
    );

    effect(
      () => {
        // Blur all inputs when unfocused
        if (!this.singleFocused() && this.nodeWrapper) {
          const inputs = this.nodeWrapper.nativeElement.querySelectorAll('input');
          inputs.forEach(input => {
            input.setSelectionRange(0, 0);
            input.blur();
          });
        }
      },
      // For some reason, blurring input fields that, in turn, have a blur handler that writes to a signal, triggers the signal writes error
      { allowSignalWrites: true },
    );
  }

  ngOnChanges(changes: SimpleChanges) {
    if ('locationSequence' in changes) {
      this.locationSequenceSig.set(this.locationSequence);
    }
  }

  add(event: MouseEvent, where: 'above' | 'below' | 'right' | 'left') {
    event.stopPropagation();
    this.interface.insert(where, this.locationSequence);
    this.addButtonsOpen.set(false);
  }

  delete() {
    this.interface.delete(this.locationSequence);
  }
}
