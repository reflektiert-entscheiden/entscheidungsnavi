import { Directive, ElementRef, inject, OnInit } from '@angular/core';
import { negate } from 'lodash';
import { KeyBindHandlerDirective } from '../directives/keybind-handler.directive';
import { PlatformDetectService } from '../services/platform-detect.service';
import { HierarchyNodeStateDirective } from './node-state.directive';
import { HierarchyInterfaceDirective } from './interface.directive';
import { HierarchyComponent } from './hierarchy.component';

@Directive({ standalone: true })
export class HierarchyKeybindsDirective<T> implements OnInit {
  private hierarchy = inject<HierarchyComponent<T>>(HierarchyComponent);
  private nodeState = inject<HierarchyNodeStateDirective<T>>(HierarchyNodeStateDirective);
  private keyBindHandler = inject(KeyBindHandlerDirective);
  private interface = inject<HierarchyInterfaceDirective<T>>(HierarchyInterfaceDirective);

  private platformDetectService = inject(PlatformDetectService);
  private hierarchyRef: ElementRef<HTMLElement> = inject(ElementRef);

  ngOnInit() {
    this.registerKeyBinds();
  }

  private registerKeyBinds() {
    const noInput = (event: KeyboardEvent) => (event.target as HTMLElement).tagName.toLowerCase() !== 'input';

    const somethingSelected = () => this.nodeState.focused().length > 0;
    const singleSelected = () => this.nodeState.focused().length === 1;
    const rootSelected = () => this.nodeState.focused().some(node => node.isRoot());
    const selectedElement = () => this.nodeState.focused()[0];
    const editable = () => !this.interface.readonly;

    // Navigate Tree
    this.keyBindHandler.register({
      key: 'ArrowLeft',
      ctrlKey: false,
      callback: () => this.nodeState.navigate('left'),
      conditions: [noInput, singleSelected],
    });

    this.keyBindHandler.register({
      key: 'ArrowRight',
      ctrlKey: false,
      callback: () => this.nodeState.navigate('right'),
      conditions: [noInput, singleSelected],
    });

    this.keyBindHandler.register({
      key: 'ArrowUp',
      callback: () => this.nodeState.navigate('up'),
      conditions: [singleSelected],
    });

    this.keyBindHandler.register({
      key: 'ArrowDown',
      callback: () => this.nodeState.navigate('down'),
      conditions: [singleSelected],
    });

    // Fold up / out from element
    this.keyBindHandler.register({
      key: 'ArrowLeft',
      ctrlKey: true,
      callback: () => this.nodeState.collapseOneLevel(selectedElement()),
      conditions: [singleSelected],
    });

    this.keyBindHandler.register({
      key: 'ArrowRight',
      ctrlKey: true,
      callback: () => this.nodeState.expandOneLevel(selectedElement()),
      conditions: [singleSelected],
    });

    // Leave Edit Mode
    this.keyBindHandler.register({
      key: 'Escape',
      callback: () => this.nodeState.clearFocus(),
      conditions: [somethingSelected, noInput],
    });

    this.keyBindHandler.register({
      key: 'Enter',
      // Move focus from the input to the parent node
      callback: () => this.hierarchyRef.nativeElement.focus(),
      conditions: [singleSelected, negate(noInput)],
    });

    // Insert
    if (this.platformDetectService.macOS) {
      this.keyBindHandler.register({
        key: 'v',
        ctrlKey: true,
        callback: () => this.interface.insert('right', selectedElement()),
        conditions: [editable, singleSelected],
      });
    } else {
      this.keyBindHandler.register({
        key: 'Insert',
        callback: () => this.interface.insert('right', selectedElement()),
        conditions: [editable, singleSelected],
      });
    }

    this.keyBindHandler.register({
      key: 'Enter',
      shiftKey: true,
      callback: () => this.interface.insert('above', selectedElement()),
      conditions: [editable, singleSelected, negate(rootSelected), noInput],
    });

    this.keyBindHandler.register({
      key: 'Enter',
      shiftKey: false,
      callback: () => this.interface.insert('below', selectedElement()),
      conditions: [editable, singleSelected, negate(rootSelected), noInput],
    });

    // Delete
    this.keyBindHandler.register({
      key: 'Delete',
      callback: () => this.interface.deleteMultiple(this.nodeState.focused()),
      conditions: [editable, somethingSelected, negate(rootSelected), noInput],
    });

    // Edit
    const editSelected = (event: KeyboardEvent) => {
      const inputs = this.hierarchy.getNodeElement(selectedElement()).nodeWrapper.nativeElement.getElementsByTagName('input');
      if (inputs.length > 0) {
        inputs.item(0).focus();
        event.preventDefault();
      }
    };

    this.keyBindHandler.register({
      key: ' ',
      callback: editSelected,
      conditions: [editable, singleSelected, noInput],
    });

    this.keyBindHandler.register({
      key: 'Spacebar',
      callback: editSelected,
      conditions: [editable, singleSelected, noInput],
    });

    this.keyBindHandler.register({
      key: 'F2',
      callback: editSelected,
      conditions: [editable, singleSelected, noInput],
    });

    // Group
    this.keyBindHandler.register({
      key: 'Home',
      callback: () => this.interface.group(this.nodeState.focused()),
      conditions: [editable, somethingSelected],
    });
  }
}
