import { ContentChild, Directive, EventEmitter, Input, OnChanges, Output, SimpleChanges, TemplateRef } from '@angular/core';
import { LocationSequence, Tree } from '@entscheidungsnavi/tools';
import { assertUnreachable } from '@entscheidungsnavi/tools/assert-unreachable';
import { v4 as uuidv4 } from 'uuid';
import { Subject } from 'rxjs';
import { HierarchyDragData } from './node/drag-data';

interface Insert {
  type: 'insert';
  location: LocationSequence;
}
interface Move {
  type: 'move';
  from: LocationSequence;
  to: LocationSequence;
}
interface Delete {
  type: 'delete';
  location: LocationSequence;
}
interface CompoundAction {
  type: 'compound';
  actions: Array<Insert | Move | Delete>;
}
interface Drop {
  type: 'drop';
  event: DragEvent;
  location: LocationSequence;
  element: HTMLElement;
}

export type HierarchyAction = Insert | Move | Delete | CompoundAction | Drop;

/**
 * This directive constitutes the outside interface of the hierarchy.
 */
@Directive({ standalone: true })
export class HierarchyInterfaceDirective<T> implements OnChanges {
  // Used for drag and drop
  readonly hierarchyUUID = uuidv4();
  activeDrag: HierarchyDragData;

  readonly treeChange$ = new Subject<void>();

  @Input({ required: true }) tree: Tree<T>;
  @Input() readonly = false;
  @Output() action = new EventEmitter<HierarchyAction>();
  // This event allows the consumer to set custom drag data, e.g., for plain text
  @Output() nodeDragStart = new EventEmitter<{ locations: LocationSequence[]; event: DragEvent }>();
  // This allows the consumer to set the appropriate drag effect for EXTERNAL DRAGS (i.e., not within this hierarchy)
  @Output() nodeDragOver = new EventEmitter<{ insertLocation: LocationSequence; event: DragEvent }>();

  @ContentChild('node')
  nodeTemplate: TemplateRef<{ $implicit: T; locationSequence: LocationSequence; focused: boolean }>;

  ngOnChanges(changes: SimpleChanges) {
    if ('tree' in changes) {
      this.treeChange$.next();
    }
  }

  insert(where: 'above' | 'below' | 'right' | 'left', loc: LocationSequence) {
    switch (where) {
      case 'above':
        this.action.emit({ type: 'insert', location: loc });
        break;
      case 'below':
        this.action.emit({ type: 'insert', location: new LocationSequence([...loc.value.slice(0, -1), loc.value.at(-1) + 1]) });
        break;
      case 'right':
        this.action.emit({ type: 'insert', location: new LocationSequence([...loc.value, 0]) });
        break;
      case 'left':
        this.action.emit({
          type: 'compound',
          actions: [
            { type: 'insert', location: loc },
            {
              type: 'move',
              from: new LocationSequence([...loc.value.slice(0, -1), loc.value.at(-1) + 1]),
              to: new LocationSequence([...loc.value, 0]),
            },
          ],
        });
        break;
      default:
        assertUnreachable(where);
    }
  }

  delete(loc: LocationSequence) {
    this.action.emit({ type: 'delete', location: loc });
  }

  deleteMultiple(locs: LocationSequence[]) {
    locs = LocationSequence.filterChildren(locs);

    const actions: Delete[] = [];

    for (let i = 0; i < locs.length; i++) {
      const loc = locs[i];

      actions.push({ type: 'delete', location: locs[i] });

      for (let j = i + 1; j < locs.length; j++) {
        locs[j] = locs[j].adjustForRemovalOf(loc);
      }
    }

    this.action.emit({ type: 'compound', actions });
  }

  group(locs: LocationSequence[]) {
    if (!LocationSequence.canGroup(locs)) return;

    const parentLoc = locs[0].getParent();
    // Child indices sorted descending
    const childIndices = locs.map(loc => loc.value.at(-1)).sort((a, b) => b - a);

    // The new node will be inserted before all other existing nodes
    const newGroupedNodeLoc = new LocationSequence([...parentLoc.value, childIndices.at(-1)]);

    this.action.emit({
      type: 'compound',
      actions: [
        // Create a new node before all nodes to be grouped
        { type: 'insert', location: newGroupedNodeLoc },
        // Move all the children behind that node
        ...childIndices.map(
          childIndex =>
            ({
              type: 'move',
              from: new LocationSequence([...parentLoc.value, childIndex + 1]),
              to: new LocationSequence([...newGroupedNodeLoc.value, 0]),
            }) satisfies Move,
        ),
      ],
    });
  }

  move(from: LocationSequence[], to: LocationSequence) {
    from = LocationSequence.filterChildren(from);

    const moveActions: Move[] = [];

    // Sort by depth in the tree descending
    from.sort((a, b) => b.value.length - a.value.length);

    from.forEach((fromLoc, fromIndex) => {
      to = to.adjustForRemovalOf(fromLoc);

      moveActions.push({
        type: 'move',
        from: fromLoc,
        to,
      });

      // Adjust all remaining Locs for this action
      for (let a = fromIndex + 1; a < from.length; a++) {
        from[a] = from[a].adjustForRemovalOf(fromLoc).adjustForInsertionOf(to);
      }

      to = to.adjustForInsertionOf(to);
    });

    this.action.emit({
      type: 'compound',
      actions: moveActions,
    });
  }

  drop(event: DragEvent, location: LocationSequence, element: HTMLElement) {
    this.action.emit({ type: 'drop', event, location, element });
  }
}
