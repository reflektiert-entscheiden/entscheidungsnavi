import { Directive, inject, signal } from '@angular/core';
import { isEqual } from 'lodash';
import { assertUnreachable } from '@entscheidungsnavi/tools/assert-unreachable';
import { LocationSequence, LocationSequenceMap, Tree } from '@entscheidungsnavi/tools';
import { takeUntilDestroyed } from '@angular/core/rxjs-interop';
import { PlatformDetectService } from '../services/platform-detect.service';
import { HierarchyAction, HierarchyInterfaceDirective } from './interface.directive';

/**
 * This directive handles the focused and collapsed state of hierarchy nodes.
 */
@Directive({ standalone: true, exportAs: 'dtHierarchyNodeState' })
export class HierarchyNodeStateDirective<T> {
  // All nodes that are currently focused
  readonly focused = signal<LocationSequence[]>([]);
  // All nodes that are currently collapsed
  readonly collapsed = signal<LocationSequence[]>([]);

  // Encodes a bias towards which child we navigate using the arrow keys
  private navigationBias = new LocationSequenceMap<number>();

  private platformDetectService = inject(PlatformDetectService);
  private interface = inject<HierarchyInterfaceDirective<T>>(HierarchyInterfaceDirective);

  constructor() {
    // Set focus to the newly inserted node on insert
    this.interface.action.pipe(takeUntilDestroyed()).subscribe(action => this.processAction(action));

    // Un-focus/un-collapse deleted nodes
    this.interface.treeChange$.pipe(takeUntilDestroyed()).subscribe(() => {
      this.focused.update(focused => focused.filter(node => this.interface.tree.getNode(node) != null));
      this.collapsed.update(collapsed => collapsed.filter(node => this.interface.tree.getNode(node) != null));
    });
  }

  private processAction(action: HierarchyAction) {
    if (action.type === 'insert') {
      this.setFocus(action.location);
      this.collapsed.update(collapsed =>
        collapsed.map(node => node.adjustForInsertionOf(action.location)).filter(node => this.interface.tree.getNode(node) != null),
      );
    } else if (action.type === 'delete') {
      this.clearFocus();
      this.collapsed.update(collapsed =>
        collapsed.map(node => node.adjustForRemovalOf(action.location)).filter(node => this.interface.tree.getNode(node) != null),
      );
    } else if (action.type === 'move') {
      // Unfocus the moved nodes and their children
      this.focused.update(focused => focused.filter(node => !node.equals(action.from) && !node.isDescendantOf(action.from)));
      this.collapsed.update(collapsed =>
        collapsed
          .map(node => node.adjustForRemovalOf(action.from).adjustForInsertionOf(action.to))
          .filter(node => this.interface.tree.getNode(node) != null),
      );
    } else if (action.type === 'compound') {
      action.actions.forEach(action => this.processAction(action));
    }
  }

  /**
   * Computes the current tree without the collapsed nodes.
   *
   * @returns The tree without the collapsed nodes
   */
  private getVisibleTree() {
    const collapsed = this.collapsed();
    return this.interface.tree.filter((_, node) => !collapsed.find(collapsedNode => collapsedNode.isParentOf(node)));
  }

  clearFocus() {
    if (this.focused().length > 0) this.focused.set([]);
  }

  nodeClick(event: MouseEvent, node: LocationSequence) {
    let focused = this.focused().slice();
    const commandKeyPressed = this.platformDetectService.macOS && event.metaKey;

    if (!focused.find(element => isEqual(element, node))) {
      if (!event.ctrlKey && !event.shiftKey && !commandKeyPressed) {
        focused = [];
      }

      const lastSelectedElement = focused.at(-1);
      const isRangeSelection = event.shiftKey && focused.length > 0 && lastSelectedElement.isSiblingOf(node);

      if (isRangeSelection) {
        // Handle range selection:
        // Make sure all elements between the last selected element and the current selected element are selected

        const parent = node.getParent();
        const fromSiblingIndex = lastSelectedElement.value.at(-1);
        const toSiblingIndex = node.value.at(-1);

        for (let sibling = Math.min(fromSiblingIndex, toSiblingIndex); sibling <= Math.max(fromSiblingIndex, toSiblingIndex); sibling++) {
          const seq = new LocationSequence([...parent.value, sibling]);
          // Avoid double-pushing.
          if (!focused.find(element => isEqual(element, seq))) {
            focused.push(seq);
          }
        }
      } else {
        // Handle single selection.
        focused.push(node);
      }

      this.focused.set(focused);
    } else if (commandKeyPressed || event.ctrlKey) {
      // Handle deselection:
      // On macOS we can deselect with the command key.
      // On Windows (and other OSes) we can deselect with the control key.
      this.focused.update(focused => focused.filter(element => !isEqual(element, node)));
    }
  }

  unfocusAllChildren(...nodes: LocationSequence[]) {
    this.focused.update(focused => focused.filter(s => !nodes.some(node => s.isDescendantOf(node))));
  }

  navigate(direction: 'up' | 'down' | 'left' | 'right') {
    const focused = this.focused();

    if (focused.length !== 1) {
      return;
    }

    const selectedElement = focused[0];
    const selectedTree = this.interface.tree.getNode(selectedElement);

    let newSelectedElement: LocationSequence;

    switch (direction) {
      case 'left':
        if (!selectedElement.isRoot()) {
          newSelectedElement = selectedElement.getParent();
          this.navigationBias.set(newSelectedElement, selectedElement.value.at(-1));
        }
        break;
      case 'right':
        if (
          selectedTree.children &&
          selectedTree.children.length > 0 &&
          !this.collapsed().find(element => isEqual(element, selectedElement))
        ) {
          const bias = this.navigationBias.get(selectedElement);
          let childIndex = 0;
          if (bias != null) {
            if (bias < selectedTree.children.length) {
              childIndex = bias;
            }
            this.navigationBias.clear(selectedElement);
          }

          newSelectedElement = new LocationSequence([...selectedElement.value, childIndex]);
        }
        break;
      case 'up':
        newSelectedElement = this.interface.tree.findPreviousNode(selectedElement);
        break;
      case 'down':
        newSelectedElement = this.interface.tree.findNextNode(selectedElement);
        break;
      default:
        assertUnreachable(direction);
    }

    if (newSelectedElement) {
      this.setFocus(newSelectedElement);
    }
  }

  setFocus(node: LocationSequence) {
    this.focused.set([node]);
  }

  collapseNode(node: LocationSequence) {
    this.collapsed.update(collapsed => [...collapsed, node]);
    this.unfocusAllChildren(node);
  }

  expandNode(node: LocationSequence) {
    this.collapsed.update(collapsed => collapsed.filter(element => !isEqual(element, node)));
  }

  /**
   * Collapse the deepest level of the subtree belonging to the given node.
   *
   * @param location - The node whose subtree should be collapsed one level
   */
  collapseOneLevel(location: LocationSequence) {
    const visibleTree = this.getVisibleTree();

    const node = visibleTree.getNode(location);
    const depth = node?.depth();

    if (node == null || depth === 0) return;

    const getChildrenAtLevel = (tree: Tree<T>, level: number, location: LocationSequence): LocationSequence[] => {
      if (level === 0) return location.isRoot() ? [] : [location];

      return tree.children.flatMap((child, index) => getChildrenAtLevel(child, level - 1, location.getChild(index)));
    };

    const childrenToCollapse = getChildrenAtLevel(node, depth - 1, location);

    this.collapsed.update(collapsed => [...collapsed, ...childrenToCollapse]);
    this.unfocusAllChildren(...childrenToCollapse);
  }

  /**
   * Expand the first collapsed child layer at the given location.
   *
   * @example
   * If the tree has a collapsed node at level 1, we expand everything on that layer. All deeper layers are ignored.
   *
   * @param location - The node whose subtree should be expanded one level
   */
  expandOneLevel(location: LocationSequence) {
    const collapsedChildren = this.collapsed().filter(
      collapsedNode => collapsedNode.equals(location) || collapsedNode.isDescendantOf(location),
    );

    const minCollapsedLevel = collapsedChildren.reduce((minLevel, collapsedNode) => Math.min(minLevel, collapsedNode.getLevel()), Infinity);

    const nodesToExpand = collapsedChildren.filter(collapsedChild => collapsedChild.getLevel() === minCollapsedLevel);

    this.collapsed.update(collapsed =>
      collapsed.filter(collapsedNode => !nodesToExpand.some(nodeToExpand => nodeToExpand.equals(collapsedNode))),
    );
  }

  isCollapsed(location: LocationSequence) {
    return this.collapsed().some(collapsed => collapsed.equals(location));
  }
}
