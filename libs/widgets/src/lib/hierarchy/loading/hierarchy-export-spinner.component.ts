import { ChangeDetectionStrategy, Component } from '@angular/core';

@Component({
  template: `
    <div class="loader"></div>
    <div>
      <h2 i18n>Bild wird exportiert…</h2>
      <div class="mat-caption">Dies kann einige Sekunden dauern</div>
    </div>
  `,
  styles: [
    //language=SCSS
    `
      @use 'variables' as dt;

      :host {
        display: flex;
        justify-content: center;
        align-items: center;
        flex-wrap: wrap;

        gap: 24px;
        padding: 16px;

        background-color: white;
        border-radius: 10px;
      }

      h2 {
        margin-bottom: 8px;
      }

      .loader {
        border: 8px solid #f3f3f3;
        border-radius: 50%;
        border-top: 8px solid dt.$orange;
        width: 56px;
        height: 56px;
        animation: spin 2s linear infinite;
      }

      @keyframes spin {
        0% {
          transform: rotate(0deg);
        }
        100% {
          transform: rotate(360deg);
        }
      }
    `,
  ],
  standalone: true,
  changeDetection: ChangeDetectionStrategy.OnPush,
  imports: [],
})
export class HierarchyExportSpinnerComponent {}
