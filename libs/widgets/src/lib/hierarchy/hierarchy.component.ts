import { CommonModule } from '@angular/common';
import { ChangeDetectionStrategy, Component, ElementRef, inject, QueryList, ViewChild, ViewChildren } from '@angular/core';
import { MatIconModule } from '@angular/material/icon';
import { LocationSequence } from '@entscheidungsnavi/tools';
import { isEqual } from 'lodash';
import { toPng } from 'html-to-image';
import { saveAs } from 'file-saver';
import { MatDialog } from '@angular/material/dialog';
import { PopOverService } from '../popover';
import { PinchZoomDirective } from '../directives';
import { KeyBindHandlerDirective } from '../directives/keybind-handler.directive';
import { HierarchyNodeStateDirective } from './node-state.directive';
import { HierarchyInterfaceDirective } from './interface.directive';
import { HierarchyKeybindsDirective } from './keybinds.directive';
import { HierarchyMovementDirective } from './movement.directive';
import { HierarchyNodeComponent } from './node/hierarchy-node.component';
import { HierarchyExportSpinnerComponent } from './loading/hierarchy-export-spinner.component';

@Component({
  selector: 'dt-hierarchy',
  templateUrl: './hierarchy.component.html',
  styleUrls: ['./hierarchy.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  standalone: true,
  imports: [CommonModule, PinchZoomDirective, MatIconModule, HierarchyNodeComponent],
  hostDirectives: [
    {
      directive: HierarchyInterfaceDirective,
      inputs: ['tree', 'readonly'],
      outputs: ['action', 'nodeDragStart', 'nodeDragOver'],
    },
    HierarchyNodeStateDirective,
    KeyBindHandlerDirective,
    HierarchyKeybindsDirective,
    HierarchyMovementDirective,
  ],
})
export class HierarchyComponent<T> {
  @ViewChild('mindMap', { static: true })
  mindMap: ElementRef<HTMLDivElement>;

  @ViewChild('moveContainer', { static: true })
  moveContainer: ElementRef<HTMLDivElement>;

  @ViewChildren(HierarchyNodeComponent)
  nodes: QueryList<HierarchyNodeComponent<T>>;

  nodeState = inject<HierarchyNodeStateDirective<T>>(HierarchyNodeStateDirective);
  protected interface = inject<HierarchyInterfaceDirective<T>>(HierarchyInterfaceDirective);

  private popOverService = inject(PopOverService);
  private dialog = inject(MatDialog);

  protected readonly rootLocationSequence = new LocationSequence([]);

  getNodeElement(node: LocationSequence) {
    return this.nodes.find(item => isEqual(item.locationSequence, node));
  }

  exportImage() {
    const dialog = this.dialog.open(HierarchyExportSpinnerComponent, { disableClose: true, hasBackdrop: true });

    // We are exporting the first child of the move container
    const exportElement = this.moveContainer.nativeElement.children.item(0) as HTMLElement;

    // Call it in a timeout to make sure the dialog is open
    setTimeout(() =>
      toPng(exportElement, {
        backgroundColor: 'white',
      })
        .then(dataUrl => {
          saveAs(dataUrl, 'objective-hierarchy.png');
        })
        .catch(error => {
          console.error(error);
          this.popOverService.whistle(
            this.mindMap.nativeElement,
            $localize`Fehler beim Export des Bildes, versuche einen anderen Browser zu verwenden.`,
            'error',
          );
        })
        .finally(() => dialog.close()),
    );
  }

  protected onArmMouseDown(event: MouseEvent) {
    event.stopImmediatePropagation();
    event.preventDefault();
  }
}
