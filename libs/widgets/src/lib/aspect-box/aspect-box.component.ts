import { ChangeDetectionStrategy, ChangeDetectorRef, Component, ElementRef, EventEmitter, Input, Output, ViewChild } from '@angular/core';

@Component({
  selector: 'dt-aspect-box',
  templateUrl: './aspect-box.component.html',
  styleUrls: ['./aspect-box.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AspectBoxComponent {
  @Input() name: string;
  @Output() nameChange = new EventEmitter<string>();
  @Input() readonly = false;
  @Input() grayedOut = false;
  @Output() deleteClick = new EventEmitter<void>();
  @Input() showDeleteButton = true;
  @Input() currentlyDraggingBox = false;
  @Input() currentlyDragging = true;
  isEditing = false;

  @ViewChild('innerWrapper') innerWrapper: ElementRef<HTMLElement>;

  constructor(private cdRef: ChangeDetectorRef) {}

  onNameChange(newName: string): void {
    this.nameChange.emit(newName);
  }

  edit() {
    this.isEditing = true;
    this.cdRef.markForCheck();
  }
}
