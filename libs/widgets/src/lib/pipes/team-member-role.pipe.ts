import { Pipe, PipeTransform } from '@angular/core';
import { TeamRole } from '@entscheidungsnavi/api-types';
import { assertUnreachable } from '@entscheidungsnavi/tools';

@Pipe({
  name: 'teamMemberRole',
  standalone: true,
})
export class TeamMemberRolePipe implements PipeTransform {
  transform(role: TeamRole): any {
    switch (role) {
      case 'co-owner':
        return $localize`Co-Moderator`;
      case 'owner':
        return $localize`Besitzer`;
      case 'editor':
        return $localize`Editor`;
      case 'user':
        return $localize`Mitglied`;
      default:
        return assertUnreachable(role);
    }
  }
}
