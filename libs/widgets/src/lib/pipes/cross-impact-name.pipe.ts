import { Pipe, PipeTransform } from '@angular/core';
import { CrossImpact } from '@entscheidungsnavi/decision-data';
import { assertUnreachable } from '@entscheidungsnavi/tools/assert-unreachable';

@Pipe({ name: 'crossImpactName', standalone: true })
export class CrossImpactNamePipe implements PipeTransform {
  transform(value: CrossImpact): string {
    return getCrossImpactName(value);
  }
}

export function getCrossImpactName(value: CrossImpact): string {
  switch (value) {
    case 1:
      return $localize`negativ korreliert`;
    case 2:
      return $localize`eher negativ korreliert`;
    case 3:
      return $localize`unkorreliert`;
    case 4:
      return $localize`eher positiv korreliert`;
    case 5:
      return $localize`positiv korreliert`;
    default:
      assertUnreachable(value);
  }
}
