import { Inject, LOCALE_ID, Pipe, PipeTransform } from '@angular/core';
import { LocalizedString } from '@entscheidungsnavi/api-types';

@Pipe({ name: 'localizedString', standalone: true })
export class LocalizedStringPipe implements PipeTransform {
  constructor(@Inject(LOCALE_ID) private locale: string) {}

  transform(value: LocalizedString): string {
    return localizeString(value, this.locale);
  }
}

export function localizeString(value: LocalizedString, locale: string): string {
  return locale.includes('de') ? value.de : value.en;
}
