import { Pipe, PipeTransform } from '@angular/core';
import { calculateIndicatorValue, Indicator } from '@entscheidungsnavi/decision-data';

/**
 * Convert indicator vector to indicator value.
 *
 * @example
 * `{{ value | indicatorValue: indicator }}`
 */
@Pipe({ name: 'indicatorValue', pure: false, standalone: true })
export class IndicatorValueCalculationPipe implements PipeTransform {
  transform(values: number[], indicator: Indicator): number {
    return calculateIndicatorValue(values, indicator);
  }
}
