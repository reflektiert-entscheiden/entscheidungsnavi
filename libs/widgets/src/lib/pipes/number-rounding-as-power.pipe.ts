import { inject, Pipe, PipeTransform } from '@angular/core';
import '@entscheidungsnavi/tools/number-rounding';
import { DecimalPipe } from '@angular/common';

@Pipe({
  name: 'roundAsPower',
  standalone: true,
})
export class NumberRoundingAsPower implements PipeTransform {
  private decimalPipe = inject(DecimalPipe);

  transform(value: number, sigDigits = 0): string {
    if (value < 999999 && value > -999999) {
      return this.decimalPipe.transform(value, `1.0-${sigDigits}`);
    }

    let power = 0;
    while (value >= 1000 || value <= -1000) {
      value /= 1000;
      power += 3;
    }

    const sup = {
      '0': '⁰',
      '1': '¹',
      '2': '²',
      '3': '³',
      '4': '⁴',
      '5': '⁵',
      '6': '⁶',
      '7': '⁷',
      '8': '⁸',
      '9': '⁹',
    };
    const regex = RegExp(`[${Object.keys(sup).join('')}]`, 'g');

    return this.decimalPipe.transform(value, `1.0-${sigDigits}`) + ' × 10' + String(power).replace(regex, c => sup[c as keyof typeof sup]);
  }
}
