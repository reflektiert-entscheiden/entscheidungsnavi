import { Pipe, PipeTransform } from '@angular/core';
import { isFinite } from 'lodash';

/**
 * Returns the second entry of array if it exists and '?' otherwise.
 *
 * Returns the item at the specified index in the array, if that index
 * is a valid number and inside the array bounds. Otherwise, the
 * placeholder is returned.
 * The optional offset is added to the index.
 *
 * @example
 * `{{ 1 | safeArrayIndex: array:'?' }}`
 * @example
 * `{{ 2 | safeArrayIndex: array:'?':-1 }}`
 */

@Pipe({
  name: 'safeArrayIndex',
  // This pipe can not be pure because a changing array item can change the result
  pure: false,
})
export class SafeArrayIndexPipe implements PipeTransform {
  transform(value: number, array: any[], placeholder: string, offset = 0): string {
    if (isFinite(value)) {
      value += offset;
      if (value >= 0 && value < array.length) {
        return array[value];
      } else {
        return placeholder;
      }
    } else {
      return placeholder;
    }
  }
}
