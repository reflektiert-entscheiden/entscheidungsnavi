import { Pipe, PipeTransform } from '@angular/core';
import { isRichTextEmpty } from '@entscheidungsnavi/tools';

/**
 * Checks whether the given rich text is classified as empty.
 * Returns true if yes, false otherwise.
 */

@Pipe({ name: 'richTextEmpty', standalone: true })
export class RichTextEmptyPipe implements PipeTransform {
  transform(value: string): boolean {
    return isRichTextEmpty(value);
  }
}
