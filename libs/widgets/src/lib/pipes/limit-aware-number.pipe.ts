import { Pipe, PipeTransform } from '@angular/core';
import { DecimalPipe } from '@angular/common';

@Pipe({
  name: 'limitAwareNumber',
  standalone: true,
})
export class LimitAwareNumberPipe implements PipeTransform {
  constructor(private decimalPipe: DecimalPipe) {}

  /**
    This pipe works like the DecimalPipe but prevents values from being displayed (rounded) as the minimum or maximum value
    if they are not exactly the minimum or maximum value.

    To prevent this it first tries to transform the value using digitsInfoLimit (Which would be a digitInfo with more decimal places)
    instead of digitsInfo

    If the transformed value is still the same as the minimum or maximum value it will prepend a '\>' or '\<' respectively.
   */
  transform(value: number, min: number, max: number, digitsInfo: string, digitsInfoLimit?: string, locale?: string): string {
    if (digitsInfoLimit == null) {
      digitsInfoLimit = digitsInfo;
    }
    const valueTransform = this.decimalPipe.transform(value, digitsInfo, locale);

    const minTransform = this.decimalPipe.transform(min, digitsInfo, locale);
    const maxTransform = this.decimalPipe.transform(max, digitsInfo, locale);

    if (valueTransform === minTransform && value !== min) {
      const minTransformLimit = this.decimalPipe.transform(min, digitsInfoLimit, locale);
      const valueLimitTransform = this.decimalPipe.transform(value, digitsInfoLimit, locale);
      if (valueLimitTransform === minTransformLimit) {
        return `>${minTransform}`;
      }
      return valueLimitTransform;
    } else if (valueTransform === maxTransform && value !== max) {
      const maxTransformLimit = this.decimalPipe.transform(max, digitsInfoLimit, locale);
      const valueLimitTransform = this.decimalPipe.transform(value, digitsInfoLimit, locale);
      if (valueLimitTransform === maxTransformLimit) {
        return `<${maxTransform}`;
      }
      return valueLimitTransform;
    }

    return valueTransform;
  }
}
