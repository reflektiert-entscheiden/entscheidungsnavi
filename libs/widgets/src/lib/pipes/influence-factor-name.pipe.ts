import { Pipe, PipeTransform } from '@angular/core';
import {
  CompositeUserDefinedInfluenceFactor,
  InfluenceFactor,
  PredefinedInfluenceFactor,
  SimpleUserDefinedInfluenceFactor,
} from '@entscheidungsnavi/decision-data';
import { PREDEFINED_INFLUENCE_FACTOR_NAMES } from '../tools';

/**
 * Casts the given value to a number.
 *
 * @example
 * `{{ value | influenceFactorName }}`
 */

@Pipe({ name: 'influenceFactorName' })
export class InfluenceFactorNamePipe implements PipeTransform {
  transform(influenceFactor: InfluenceFactor): string {
    return getInfluenceFactorName(influenceFactor);
  }
}

export function getInfluenceFactorName(influenceFactor: InfluenceFactor): string {
  if (influenceFactor instanceof CompositeUserDefinedInfluenceFactor) {
    return influenceFactor.customName || $localize`Kombinierter Einflussfaktor: ${influenceFactor.defaultName}`;
  } else if (influenceFactor instanceof SimpleUserDefinedInfluenceFactor) {
    return influenceFactor.name;
  } else if (influenceFactor instanceof PredefinedInfluenceFactor) {
    return PREDEFINED_INFLUENCE_FACTOR_NAMES[influenceFactor.id].name;
  }
  return '';
}
