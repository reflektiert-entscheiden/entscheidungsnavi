import { Pipe, PipeTransform } from '@angular/core';

/**
 * Casts the given value to a number.
 *
 * @example
 * `{{ value | numberCast }}`
 */

@Pipe({ name: 'numberCast' })
export class NumberCastPipe implements PipeTransform {
  transform(value: any): number {
    return value as number;
  }
}
