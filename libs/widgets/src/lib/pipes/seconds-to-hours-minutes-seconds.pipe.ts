import { Pipe, PipeTransform } from '@angular/core';

/**
 * Example Usage: [src] = 3903 | secondsToHoursMinutesSeconds
 * Output: 01:05:03
 */

@Pipe({ name: 'secondsToHoursMinutesSeconds', standalone: true })
export class SecondsToHoursMinutesSeconds implements PipeTransform {
  transform(inputSeconds: number) {
    if (typeof inputSeconds !== 'number' || inputSeconds < 0) return '';

    const hours = Math.floor(inputSeconds / 3600),
      minutes = Math.floor((inputSeconds % 3600) / 60),
      seconds = Math.floor(inputSeconds % 60);

    function padTime(t: number) {
      return t < 10 ? '0' + t : '' + t;
    }

    return padTime(hours) + ':' + padTime(minutes) + ':' + padTime(seconds);
  }
}
