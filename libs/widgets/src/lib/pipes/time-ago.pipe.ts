import { Pipe, PipeTransform } from '@angular/core';

@Pipe({ name: 'timeAgo', pure: true, standalone: true })
export class TimeAgoPipe implements PipeTransform {
  transform(timestamp: Date) {
    if (!timestamp) {
      return null;
    }

    const seconds = Math.floor((new Date().getTime() - timestamp.getTime()) / 1000);
    if (seconds < 60) {
      return $localize`gerade eben`;
    } else if (seconds < 3600) {
      const minutes = Math.floor(seconds / 60);
      return minutes > 1 ? $localize`vor ${minutes} Minuten` : $localize`vor 1 Minute`;
    } else if (seconds < 86400) {
      const hours = Math.floor(seconds / 3600);
      return hours > 1 ? $localize`vor ${hours} Stunden` : $localize`vor 1 Stunde`;
    } else {
      const days = Math.floor(seconds / 86400);
      return days > 1 ? $localize`vor ${days} Tagen` : $localize`vor 1 Tag`;
    }
  }
}
