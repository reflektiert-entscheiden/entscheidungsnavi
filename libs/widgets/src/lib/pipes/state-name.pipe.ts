import { Pipe, PipeTransform } from '@angular/core';
import {
  CompositeUserDefinedInfluenceFactor,
  InfluenceFactor,
  PredefinedInfluenceFactor,
  SimpleUserDefinedInfluenceFactor,
} from '@entscheidungsnavi/decision-data';
import { assertUnreachable } from '@entscheidungsnavi/tools';
import { PREDEFINED_INFLUENCE_FACTOR_NAMES } from '../tools';

/**
 * Casts the given value to a number.
 *
 * @example
 * @param influenceFactor - the influence factor assigned to the outcome
 * @param stateIdx - state index of the current outcome value
 * @param isShort - it returns a shorter version of the state name. Meant to be used with predefined influence factors
 * `{{ value | stateName : influenceFactor : 2 : true }}`
 */

@Pipe({ name: 'stateName', pure: false })
export class StateNamePipe implements PipeTransform {
  transform(influenceFactor: InfluenceFactor, stateIdx: number, isShort = false): string {
    return getInfluenceFactorStateName(influenceFactor, stateIdx, isShort);
  }
}

export function getInfluenceFactorStateName(influenceFactor: InfluenceFactor, stateIdx: number, isShort = false): string {
  if (influenceFactor instanceof SimpleUserDefinedInfluenceFactor) {
    return influenceFactor.states[stateIdx].name;
  } else if (influenceFactor instanceof PredefinedInfluenceFactor) {
    return isShort
      ? PREDEFINED_INFLUENCE_FACTOR_NAMES[influenceFactor.id].shortStateNames[stateIdx]
      : PREDEFINED_INFLUENCE_FACTOR_NAMES[influenceFactor.id].stateNames[stateIdx];
  } else if (influenceFactor instanceof CompositeUserDefinedInfluenceFactor) {
    const [firstIndex, secondIndex] = influenceFactor.getBaseStateIndices(stateIdx);
    return `${getInfluenceFactorStateName(influenceFactor.baseFactors[0], firstIndex)} / ${getInfluenceFactorStateName(
      influenceFactor.baseFactors[1],
      secondIndex,
    )}`;
  } else {
    assertUnreachable(influenceFactor);
  }
}
