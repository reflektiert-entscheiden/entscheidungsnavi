import { DecimalPipe } from '@angular/common';
import { LimitAwareNumberPipe } from './limit-aware-number.pipe';

describe('LimitAwareNumberPipe', () => {
  let pipe: LimitAwareNumberPipe;
  let decimalPipe: DecimalPipe;

  beforeEach(() => {
    decimalPipe = new DecimalPipe('en-US');
    pipe = new LimitAwareNumberPipe(decimalPipe);
  });

  it('adds additional decimal places if otherwise equal to min', () => {
    expect(pipe.transform(1.234, 1, 2, '1.0-0', '1.0-1')).toBe('1.2');
  });

  it('adds additional decimal places if otherwise equal to max', () => {
    expect(pipe.transform(1.911, 1, 2, '1.0-0', '1.0-1')).toBe('1.9');
  });

  it('adds > if value is greater than min but still equal with additional decimal places', () => {
    expect(pipe.transform(1.001, 1, 2, '1.0-0', '1.0-2')).toBe('>1');
  });

  it('adds < if value is less than max but still equal with additional decimal places', () => {
    expect(pipe.transform(1.999, 1, 2, '1.0-0', '1.0-2')).toBe('<2');
  });

  it('does not affect the limits', () => {
    expect(pipe.transform(1, 1, 2, '1.0-0', '1.0-1')).toBe('1');
    expect(pipe.transform(2, 1, 2, '1.0-0', '1.0-1')).toBe('2');
  });

  it('does not affect values that are not equal to the limits', () => {
    expect(pipe.transform(4, 1, 10, '1.0-0', '1.2-2')).toBe('4');
    expect(pipe.transform(5, 1, 10, '1.0-0', '1.2-2')).toBe('5');
    expect(pipe.transform(6, 1, 10, '1.0-0', '1.2-2')).toBe('6');

    expect(pipe.transform(3.9, 1, 10, '1.0-0', '1.2-2')).toBe('4');
    expect(pipe.transform(4.1, 1, 10, '1.0-0', '1.2-2')).toBe('4');

    expect(pipe.transform(5.9, 1, 10, '1.0-0', '1.2-2')).toBe('6');
    expect(pipe.transform(6.1, 1, 10, '1.0-0', '1.2-2')).toBe('6');
  });
});
