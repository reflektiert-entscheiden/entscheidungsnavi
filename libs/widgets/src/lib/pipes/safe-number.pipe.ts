import { formatNumber } from '@angular/common';
import { Inject, LOCALE_ID, Pipe, PipeTransform } from '@angular/core';
import { isFinite } from 'lodash';

/**
 * The first parameter denotes what value should be returned when the input is not a regular finite number
 * (as defined by lodash's isFinite method). In particular, NaN, null, undefined are all not considered numbers. *
 * The second parameter is the digitsInfo input to Angular's DecimalPipe.
 *
 * @example
 * `{{ 30 | safeNumber: '?':'1.0-0 }}`
 */

@Pipe({ name: 'safeNumber', standalone: true })
export class SafeNumberPipe implements PipeTransform {
  constructor(@Inject(LOCALE_ID) private locale: string) {}

  transform(value: number, placeholder: string, format?: string): string {
    return formatNumberSafe(value, this.locale, placeholder, format);
  }
}

export function formatNumberSafe(value: number, locale: string, placeholder: string, format?: string): string {
  return isFinite(value) ? formatNumber(value, locale, format) : placeholder;
}
