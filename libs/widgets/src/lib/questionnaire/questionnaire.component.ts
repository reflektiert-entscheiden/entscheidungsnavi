import { STEPPER_GLOBAL_OPTIONS } from '@angular/cdk/stepper';
import { ChangeDetectorRef, Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges, ViewChild } from '@angular/core';
import { FormArray, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatStepper } from '@angular/material/stepper';
import { EventRegistration, QuestionnaireEntry, QuestionnairePage } from '@entscheidungsnavi/api-types';
import { OnDestroyObservable } from '@entscheidungsnavi/tools';
import { assertUnreachable } from '@entscheidungsnavi/tools/assert-unreachable';
import { map, Observable, startWith, takeUntil } from 'rxjs';

@Component({
  selector: 'dt-questionnaire',
  templateUrl: './questionnaire.component.html',
  styleUrls: ['./questionnaire.component.scss'],
  providers: [
    {
      provide: STEPPER_GLOBAL_OPTIONS,
      useValue: { showError: true },
    },
  ],
})
export class QuestionnaireComponent implements OnInit, OnChanges {
  @OnDestroyObservable()
  private onDestroy$: Observable<void>;

  @Input()
  questionnairePages: QuestionnairePage[];

  @Input()
  submission?: EventRegistration;

  @Input()
  readonly: boolean;

  @Output()
  isValid = new EventEmitter<boolean>(true);

  @ViewChild(MatStepper, { static: true })
  stepper: MatStepper;

  @Output()
  openStatus = new EventEmitter<void>();

  questionnaireForm: FormGroup<{ pages: FormArray<FormArray<FormControl<string> | FormControl<number> | FormArray>> }>;

  constructor(private cdRef: ChangeDetectorRef) {}

  ngOnInit() {
    this.questionnaireForm = new FormGroup({
      pages: new FormArray(
        this.questionnairePages.map(
          questionnairePage => new FormArray(questionnairePage.entries.map(entry => this.getFormControlFor(entry))),
        ),
      ),
    });

    if (this.readonly) {
      this.questionnaireForm.disable();
    }

    if (this.submission?.questionnaireResponses) {
      this.loadFromResponse(this.submission.questionnaireResponses);
    }

    this.stepper.steps.changes.pipe(takeUntil(this.onDestroy$)).subscribe(() => {
      this.stepper.steps.forEach((step, stepIndex) => {
        const value = this.questionnaireForm.controls.pages.at(stepIndex).value;

        if (this.containsNonNullRec(value)) {
          step._markAsInteracted();
        }
      });
      this.cdRef.detectChanges();
    });

    this.questionnaireForm.statusChanges
      .pipe(
        startWith(this.questionnaireForm.status),
        map(status => status === 'VALID'),
        takeUntil(this.onDestroy$),
      )
      .subscribe(isValid => this.isValid.emit(isValid));
  }

  ngOnChanges(changes: SimpleChanges) {
    if ('readonly' in changes && !changes['readonly'].firstChange) {
      if (changes['readonly'].currentValue) {
        this.questionnaireForm.disable();
      } else {
        this.questionnaireForm.enable();
      }
    }

    if ('submission' in changes && this.submission?.questionnaireResponses && this.questionnaireForm != null) {
      this.loadFromResponse(this.submission.questionnaireResponses);
    }
  }

  containsNonNullRec(value: unknown | unknown[]): boolean {
    if (Array.isArray(value)) {
      return value.some(childValue => this.containsNonNullRec(childValue));
    } else {
      return value != null;
    }
  }

  turnIntoResponse(): (number | string | number[] | null)[][] {
    return this.questionnaireForm.controls.pages.controls.map(page => page.controls.map(control => control.value));
  }

  loadFromResponse(response: (number | string | number[] | null)[][]) {
    this.questionnaireForm.controls.pages.controls.forEach((page, pageIndex) =>
      page.controls.forEach((control, controlIndex) => {
        const savedValue = response[pageIndex]?.[controlIndex];
        if (savedValue == null) {
          return;
        }

        if (control instanceof FormControl) {
          const castedControl: FormControl<any> = control;

          castedControl.setValue(savedValue);
          castedControl.markAsTouched();
        } else {
          const formArray = control;
          const valueArray = savedValue as number[];

          formArray.controls.forEach((control, controlIndex) => {
            control.setValue(valueArray[controlIndex]);
            control.markAsTouched();
          });
        }
      }),
    );
  }

  private getFormControlFor(entry: QuestionnaireEntry) {
    switch (entry.entryType) {
      case 'textBlock':
        return new FormControl<null>(null);
      case 'numberQuestion': {
        const validators = [Validators.required];

        if (entry.min != null) {
          validators.push(Validators.min(entry.min));
        }
        if (entry.max != null) {
          validators.push(Validators.max(entry.max));
        }

        return new FormControl<number>(null, validators);
      }
      case 'optionsQuestion':
        return new FormControl<number>(null, Validators.required);
      case 'textQuestion': {
        const validators = [Validators.required];

        if (entry.minLength != null) {
          validators.push(Validators.minLength(entry.minLength));
        }

        if (entry.maxLength != null) {
          validators.push(Validators.maxLength(entry.maxLength));
        }

        if (entry.pattern) {
          validators.push(Validators.pattern(entry.pattern));
        }

        return new FormControl<string>(null, validators);
      }
      case 'tableQuestion':
        return new FormArray(entry.subQuestions.map(_ => new FormControl<number>(null, Validators.required)));
      default:
        assertUnreachable(entry);
    }
  }
}
