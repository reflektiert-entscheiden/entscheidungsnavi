import { Component, EventEmitter, Input, Output } from '@angular/core';
import { ControlContainer } from '@angular/forms';
import { FORWARD_CONTROL_CONTAINER } from '../../form-elements';

@Component({
  selector: 'dt-evaluation-checkboxes',
  templateUrl: './evaluation-checkboxes.component.html',
  styleUrls: ['./evaluation-checkboxes.component.scss'],
  viewProviders: [FORWARD_CONTROL_CONTAINER],
})
export class EvaluationCheckboxesComponent {
  @Input()
  prompt = '';

  @Input()
  options: string[] = [];

  @Input()
  rows: string[] = [];

  @Input()
  values: number[];

  @Input()
  lastNotSpecified = false; // Whether the last option is not an actual option but just for if you don't want to answer

  @Output()
  valuesChange = new EventEmitter<number[]>();

  get gridTemplateColumns() {
    return `fit-content(25rem) repeat(${this.options.length}, 1fr)`;
  }

  constructor(public controlContainer: ControlContainer) {}

  setValue(row: number, to: number) {
    this.values[row] = to;

    this.valuesChange.emit(this.values);
  }

  isChecked(row: number, option: number) {
    return this.values[row] === option;
  }
}
