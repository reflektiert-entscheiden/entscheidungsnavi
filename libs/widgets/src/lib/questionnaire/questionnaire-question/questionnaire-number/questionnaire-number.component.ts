import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { NumberQuestionEntry } from '@entscheidungsnavi/api-types';
import { ControlContainer, FormArray, FormControl } from '@angular/forms';
import { FORWARD_CONTROL_CONTAINER } from '../../../form-elements';

@Component({
  selector: 'dt-questionnaire-question-number',
  templateUrl: './questionnaire-number.component.html',
  styleUrls: ['./questionnaire-number.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  viewProviders: [FORWARD_CONTROL_CONTAINER],
})
export class QuestionnaireQuestionNumberComponent {
  @Input()
  entry: NumberQuestionEntry;

  @Input()
  formIndex: number;

  get control() {
    return (this.controlContainer.control as FormArray).at(this.formIndex) as FormControl<number>;
  }

  get errors() {
    return this.control.errors;
  }

  constructor(protected controlContainer: ControlContainer) {}

  increment() {
    this.control.setValue(Math.min(this.entry.max ?? Infinity, this.control.value + this.entry.step));
  }

  decrement() {
    this.control.setValue(Math.max(this.entry.min ?? -Infinity, this.control.value - this.entry.step));
  }
}
