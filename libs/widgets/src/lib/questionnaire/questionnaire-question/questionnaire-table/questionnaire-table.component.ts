import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { TableQuestionEntry } from '@entscheidungsnavi/api-types';
import { FORWARD_CONTROL_CONTAINER } from '../../../form-elements';

@Component({
  selector: 'dt-questionnaire-question-table',
  templateUrl: './questionnaire-table.component.html',
  styleUrls: ['./questionnaire-table.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  viewProviders: [FORWARD_CONTROL_CONTAINER],
})
export class QuestionnaireQuestionTableComponent {
  @Input()
  entry: TableQuestionEntry;

  @Input()
  formIndex: number;
}
