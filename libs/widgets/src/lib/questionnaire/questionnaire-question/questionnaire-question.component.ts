import { AfterViewInit, Component, Input, ViewChild } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { BaseQuestionnaireEntry } from '@entscheidungsnavi/api-types';

@Component({
  selector: 'dt-questionaire-question',
  templateUrl: './questionnaire-question.component.html',
  styleUrls: ['./questionnaire-question.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      multi: true,
      useExisting: QuestionnaireQuestionComponent,
    },
  ],
})
export class QuestionnaireQuestionComponent implements ControlValueAccessor, AfterViewInit {
  @Input()
  question: BaseQuestionnaireEntry;

  @Input()
  formIndex: number;

  @ViewChild(NG_VALUE_ACCESSOR)
  valueAccessor: ControlValueAccessor;

  private value: any;
  private onChange: any;
  private onTouched: any;
  private isDisabled: boolean;

  ngAfterViewInit() {
    if (this.valueAccessor) {
      this.valueAccessor.writeValue(this.value);
      this.valueAccessor.registerOnChange(this.onChange);
      this.valueAccessor.registerOnTouched(this.onTouched);
      this.valueAccessor.setDisabledState(this.isDisabled);
    }
  }

  writeValue(obj: any): void {
    this.valueAccessor?.writeValue(obj);
    this.value = obj;
  }
  registerOnChange(fn: any): void {
    this.valueAccessor?.registerOnChange(fn);
    this.onChange = fn;
  }
  registerOnTouched(fn: any): void {
    this.valueAccessor?.registerOnTouched(fn);
    this.onTouched = fn;
  }
  setDisabledState?(isDisabled: boolean): void {
    this.valueAccessor?.setDisabledState(isDisabled);
    this.isDisabled = isDisabled;
  }
}
