import { Router } from '@angular/router';
import { AfterViewInit, Component, EventEmitter, HostListener, Input, OnDestroy, OnInit, Optional, Output } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { OnDestroyObservable } from '@entscheidungsnavi/tools';
import { Observable } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { AbstractModalConfigurationDirective } from './modal-configuration.directive';
import { ModalComponent } from './modal.component';

/**
 * This Component can either be used as direct child of ModalComponent or standalone when opened by MatDialog.
 */
@Component({
  selector: 'dt-modal-content',
  templateUrl: './modal-content.component.html',
  styleUrls: ['./modal-content.component.scss'],
})
export class ModalContentComponent extends AbstractModalConfigurationDirective implements OnInit, AfterViewInit, OnDestroy {
  /**
   * While at least one modal is open, we constantly keep a history state with this content on top of the
   * history stack. It is added by the modal which is opened first and removed by the modal which is
   * closed last.
   */
  private static readonly historyKey = 'dt-modal-content-history-entry';
  private static openModals: ModalContentComponent[] = [];

  config: AbstractModalConfigurationDirective;

  /**
   * Only needs to be provided from the outside when ModalContentComponent is used in a template which
   * is directly opened via MatDialog. In that case, MatDialogRef is not injected automatically.
   */
  @Input()
  dialogRef: MatDialogRef<any>;

  /**
   * Emitted when one of the close buttons is clicked
   */
  @Output() closeClick = new EventEmitter();

  @OnDestroyObservable()
  private onDestroy$: Observable<any>;

  constructor(
    @Optional() parent: ModalComponent,
    @Optional() dialogRef: MatDialogRef<any>,
    private router: Router,
  ) {
    super();

    this.config = parent ?? this;
    this.dialogRef = this.dialogRef ?? parent?.dialogRef ?? dialogRef;
  }

  override ngOnInit() {
    this.dialogRef.addPanelClass('dt-modal-content-mat-dialog-panel');
    this.dialogRef.updatePosition({ top: '10vh' });

    if (this.config.size === 'giant') {
      this.dialogRef.addPanelClass('dt-modal-content-mat-dialog-panel-giant');
    }

    super.ngOnInit();

    ModalContentComponent.openModals.push(this);
    if (ModalContentComponent.openModals.length === 1) {
      this.pushHistoryState();
    }
  }

  ngAfterViewInit() {
    this.dialogRef.disableClose = true;
    this.dialogRef
      .backdropClick()
      .pipe(takeUntil(this.onDestroy$))
      .subscribe(() => this.closeClick.emit());
    this.dialogRef
      .keydownEvents()
      .pipe(takeUntil(this.onDestroy$))
      .subscribe(event => {
        if (event.key === 'Escape') {
          this.closeClick.emit();
        }
        event.stopPropagation();
      });
  }

  ngOnDestroy() {
    this.cleanupHistory();
  }

  cleanupHistory() {
    // the last modal needs to clean up the history when closed
    if (
      ModalContentComponent.openModals.length === 1 &&
      this.router.getCurrentNavigation() == null &&
      history.state === ModalContentComponent.historyKey
    ) {
      // if the router is currently navigating the asynchronous nature of
      // history.back() aborts that navigation in Chrome.
      history.back();
    }

    ModalContentComponent.openModals.splice(ModalContentComponent.openModals.indexOf(this), 1);
  }

  close() {
    this.closeClick.emit();
  }

  @HostListener('window:popstate', ['$event'])
  onPopState(_event: PopStateEvent) {
    // only the top most modal needs to react
    if (ModalContentComponent.openModals[ModalContentComponent.openModals.length - 1] === this) {
      this.close();
      this.pushHistoryState();
    }
  }

  private pushHistoryState() {
    history.pushState(ModalContentComponent.historyKey, null, window.location.href);
  }
}
