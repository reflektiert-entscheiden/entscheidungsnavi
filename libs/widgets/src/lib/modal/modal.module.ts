import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatIconModule } from '@angular/material/icon';
import { MatDividerModule } from '@angular/material/divider';
import { MatButtonModule } from '@angular/material/button';
import { ConfirmModalComponent, ModalContentComponent, ModalComponent } from '.';

@NgModule({
  imports: [CommonModule, MatIconModule, MatDividerModule, MatButtonModule],
  declarations: [ModalComponent, ModalContentComponent, ConfirmModalComponent],
  exports: [ModalComponent, ModalContentComponent, ConfirmModalComponent],
})
export class ModalModule {}
