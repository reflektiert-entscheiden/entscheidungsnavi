import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ThemePalette } from '@angular/material/core';
import { AbstractModalConfigurationDirective } from '../modal-configuration.directive';

export type ConfirmModalData = {
  title: string;
  prompt: string;
  buttonConfirm?: string;
  buttonConfirmColor?: ThemePalette;
  buttonDeny?: string;
  template?: 'delete' | 'discard';
  size?: AbstractModalConfigurationDirective['size'];
};
@Component({
  templateUrl: './confirm-modal.component.html',
  styleUrls: ['./confirm-modal.component.scss'],
})
export class ConfirmModalComponent {
  get modalTitle() {
    return this.data.title;
  }

  get prompt() {
    return this.data.prompt;
  }

  get size() {
    return this.data.size ?? 'normal';
  }

  get buttonConfirm() {
    switch (this.data.template) {
      case 'delete':
        return $localize`Ja, löschen`;
      case 'discard':
        return $localize`Ja, verwerfen`;
      default:
        return this.data.buttonConfirm;
    }
  }

  get buttonConfirmColor() {
    return this.data.buttonConfirmColor ?? 'warn';
  }

  get buttonDeny() {
    return this.data.buttonDeny ?? $localize`Nein, abbrechen`;
  }

  constructor(
    private dialogRef: MatDialogRef<ConfirmModalComponent, boolean>,
    @Inject(MAT_DIALOG_DATA) private data: ConfirmModalData,
  ) {}

  close(confirm: boolean) {
    this.dialogRef.close(confirm);
  }
}
