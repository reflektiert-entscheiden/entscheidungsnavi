import { InfluenceFactor } from '@entscheidungsnavi/decision-data/classes/influence-factor/influence-factor';

export type TornadoData = {
  bars: TornadoBarWithChildren[];
  totalExpectedValue: { value: number; tooltipInfo?: string };
};

// a bar that includes bars nested inside that can be expanded/collapsed; children may not include further children
export type TornadoBarWithChildren = TornadoBar & { children: TornadoBar[]; areChildrenExpanded: boolean };

export type TornadoBar = {
  alternativeBoxes?: AlternativeBox[]; // left side (info)
  label: string; // left side (info)
  unit: string; // left side (info)
  points: TornadoPoint[]; // right side (diagram), used for shaping tornado bars and displaying values
};

// orange/blue boxes that are displayed next to the uncertainty (in the Utility Based Tornado during comparison mode)
// on hover a tooltip is displayed showing the name(s) of the related alternative(s)
type AlternativeBox = { isPrimary: boolean; alternativeName: string };

export type TornadoPoint = {
  tornadoPointLabel: string; // value displayed on top of the tornado bar
  // in the Impact Model Tornado it is a number - the outcome value in this indicator and state
  // in the Utility Based Tornado it is a string - the state name
  // (in case of multiple states and PredefinedIF state name is simplified as either Worst, Median or Best)
  tornadoPointValue: number; // value displayed on the x-axis
  // in the Impact Model Tornado it shows the outcome value after using the Indicator Aggregation Function
  // in the Utility Based one it shows the utility value for the alternative (or a difference with the utility of the compared alternative)

  // `tooltipCombinations` is a detailed info used in the tooltip that describes the combinations that the point was made of.
  // It is 2D as it may include multiple points that were merged together-
  tooltipCombinations: { probability: number; tooltipRows: TooltipRow[] }[];
};

// info about the combinations used to create the point
export type TooltipRow = { indicatorName?: string; stateName: string; stateValue?: string; probability?: number };

export type ScaleAxis = {
  label: string;
  unit: string;
  from: number;
  to: number;
};

export type BackgroundAreas = {
  defaultAreaLabel?: string;
  leftAreaLabel?: string;
  rightAreaLabel?: string;
};

/**
 * Orders and merges the given tornado points. Two values are merged their difference is less than or equal to {@link precision}.
 * Merged points have the same tornadoPointLabel and tornadoPointValue, but their tooltipCombinations are combined, resulting in a single
 * point on the diagram that has a "scrollable" via arrows tooltip.
 *
 * @param tornadoValues - values to be ordered and merged
 * @param influenceFactor - influence factor that the values belong to
 * @param precision - how close the values should be to be merged together
 */

export function orderAndMergeTornadoPoints(tornadoValues: TornadoPoint[], influenceFactor: InfluenceFactor, precision = 0) {
  tornadoValues.sort((a, b) => a.tornadoPointValue - b.tornadoPointValue);

  const mergedTornadoPoints: TornadoPoint[] = [];

  for (const currentTornadoPoint of tornadoValues) {
    const lastMergedTornadoPoint = mergedTornadoPoints.at(-1);

    if (lastMergedTornadoPoint != null && currentTornadoPoint.tornadoPointValue - lastMergedTornadoPoint.tornadoPointValue <= precision) {
      lastMergedTornadoPoint.tooltipCombinations.push(...currentTornadoPoint.tooltipCombinations);

      if (lastMergedTornadoPoint.tornadoPointLabel !== '') {
        // override merged value's tornadoPointLabel with a non-median one
        lastMergedTornadoPoint.tornadoPointLabel = currentTornadoPoint.tornadoPointLabel;
      }
    } else mergedTornadoPoints.push(currentTornadoPoint);
  }

  return mergedTornadoPoints;
}
