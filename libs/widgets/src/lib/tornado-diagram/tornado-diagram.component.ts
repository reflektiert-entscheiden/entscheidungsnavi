import {
  AfterViewInit,
  ChangeDetectorRef,
  Component,
  ElementRef,
  HostBinding,
  inject,
  Input,
  OnChanges,
  OnDestroy,
  OnInit,
  SimpleChanges,
} from '@angular/core';
import { round } from 'lodash';
import { DecimalPipe, PercentPipe, SlicePipe } from '@angular/common';
import { MatTooltip } from '@angular/material/tooltip';
import { MatIcon } from '@angular/material/icon';
import { MatButton, MatIconButton } from '@angular/material/button';
import { NumberRoundingAsPower } from '../pipes/number-rounding-as-power.pipe';
import { HoverPopOverDirective } from '../popover';
import { BackgroundAreas, ScaleAxis, TornadoBar, TornadoBarWithChildren, TornadoData, TornadoPoint } from './tornado-helper';

@Component({
  selector: 'dt-tornado-diagram',
  templateUrl: './tornado-diagram.component.html',
  styleUrls: ['./tornado-diagram.component.scss'],
  standalone: true,
  imports: [MatTooltip, MatIcon, MatIconButton, MatButton, SlicePipe, PercentPipe, NumberRoundingAsPower, HoverPopOverDirective],
})
export class TornadoDiagramComponent implements AfterViewInit, OnInit, OnChanges, OnDestroy {
  @Input()
  tornadoData: TornadoData;

  @Input()
  areas: BackgroundAreas;

  @Input()
  scaleAxis: ScaleAxis;

  @Input()
  defaultOrder = true;

  @Input()
  significantDigits: number;

  percentageSignificantDigits = 2;

  hoveredBarIdx = -1; // currently hovered bar
  hoveredPointIdx = -1; // currently hovered point

  tooltipCombinationIdx = 0; // used to navigate through tooltip combinations (with left/right arrow)

  areLimitsOverlapping: boolean[] = [];
  intersectionObservers: IntersectionObserver[] = [];

  protected visibleTornadoBars: (TornadoBar | TornadoBarWithChildren)[] = [];
  private cdRef = inject(ChangeDetectorRef);
  private hostElement = inject(ElementRef);
  private decimalPipe = inject(DecimalPipe);

  /* Width of the chart as percentage of the chart canvas. */
  readonly chartWidthPercentage = 85;
  /* Height of a grid row in px. */
  readonly gridRowHeight = 40;

  @HostBinding('class.includes-areas')
  get includesAreas() {
    return this.bothAlternativesSelected || this.hasDefaultArea;
  }

  get hasDefaultArea() {
    return this.areas?.defaultAreaLabel != null;
  }

  get bothAlternativesSelected() {
    return this.areas?.leftAreaLabel != null && this.areas?.rightAreaLabel != null;
  }

  @HostBinding('style.--number-of-visible-bars')
  get numberOfVisibleBars() {
    return this.visibleTornadoBars.length;
  }

  get chartInfoGridColumns() {
    const columns = ['minmax(300px, 2fr)']; // name
    if (this.containsUnits && !this.includesAreas) columns.push('90px'); // unit
    if (this.bothAlternativesSelected) columns.push('50px'); // alternative box (orange/blue)
    if (this.containsChildren) columns.push(this.gridRowHeight + 'px'); // expand/collapse arrow
    columns.push('minmax(700px, 7fr)');
    return columns.join(' ');
  }

  get backgroundAreaHeight() {
    // empty row + N tornado bars + scale row (double in size)
    return (this.visibleTornadoBars.length + 1) * this.gridRowHeight + this.gridRowHeight * 2;
  }

  get emptyInfoNameSpan() {
    // increase span if columns are missing
    return 1 + Number(this.containsUnits && !this.includesAreas) + Number(this.containsChildren) + Number(this.bothAlternativesSelected);
  }

  get scaleNameSpan() {
    // increase span if columns are missing
    return 1 + Number(this.containsChildren) + Number(this.bothAlternativesSelected);
  }

  get leftBackgroundAreaRatio() {
    const addedArea = (1 - this.chartWidthPercentage / 100) * (this.scaleAxis.to - this.scaleAxis.from);
    return (addedArea / 2 - this.scaleAxis.from) / (this.scaleAxis.to - this.scaleAxis.from + addedArea);
  }

  get rightBackgroundAreaRatio() {
    const addedArea = (1 - this.chartWidthPercentage / 100) * (this.scaleAxis.to - this.scaleAxis.from);
    return (this.scaleAxis.to + addedArea / 2) / (this.scaleAxis.to - this.scaleAxis.from + addedArea);
  }

  get containsChildren() {
    return this.tornadoData.bars.some(bar => bar.children.length > 0);
  }

  get containsUnits() {
    return this.tornadoData.bars.some(bar => bar.unit != '') || this.scaleAxis.unit != '';
  }

  get currentTornadoPoint() {
    return this.visibleTornadoBars[this.hoveredBarIdx].points[this.hoveredPointIdx];
  }

  get isPointHovered() {
    return this.hoveredPointIdx !== -1;
  }

  get middlePointHovered() {
    return this.hoveredPointIdx !== 0 && this.hoveredPointIdx !== this.visibleTornadoBars[this.hoveredBarIdx].points.length - 1;
  }

  get barLimitsHovered() {
    return this.hoveredBarIdx > -1 || (this.isPointHovered && !this.middlePointHovered);
  }

  ngOnInit() {
    this.orderAndComputeVisibleBars();
  }

  ngAfterViewInit() {
    this.setOverlappingLimits();
    this.setIntersectionObservers();
    this.cdRef.detectChanges();
  }

  ngOnChanges(changes: SimpleChanges) {
    if ('tornadoData' in changes || 'defaultOrder' in changes) {
      this.orderAndComputeVisibleBars(); // completely rerenders the chart
    }
  }

  ngOnDestroy() {
    for (const intersectionObserver of this.intersectionObservers) {
      intersectionObserver.disconnect();
    }
  }

  // orders tornado bars, hides trivial bars (with no real uncertainty) and flattens children
  private orderAndComputeVisibleBars() {
    // order points inside bar based on tornadoPointValue
    this.orderTornadoPoints();
    // remove bars (and children bars) with no real uncertainty
    this.removeTrivialBars();
    const sortedBars = this.tornadoData.bars.slice();
    if (this.includesAreas || !this.defaultOrder) {
      // sort utility-based tornado (always) and tornado based tornado (when defaultOrder is false)
      this.sortTornadoBars(sortedBars);
      sortedBars.forEach(bar => this.sortTornadoBars(bar.children));
    }
    // compute visible bars
    this.visibleTornadoBars = [];
    sortedBars.forEach(bar => {
      this.visibleTornadoBars.push(bar);
      if (this.isParentBar(bar) && bar.areChildrenExpanded) {
        this.visibleTornadoBars.push(...bar.children);
      }
    });
    this.updatePercentageSignificantDigits();
    this.cdRef.detectChanges();
    setTimeout(() => this.setOverlappingLimits()); // wait for bars to be rerendered
    this.setIntersectionObservers();
  }

  // checks whether a bar has a positive length (not 0), expects tornadoPointLabels to be sorted
  private isBarTrivial(tornadoBar: TornadoBar | TornadoBarWithChildren) {
    return tornadoBar.points.at(-1).tornadoPointValue === tornadoBar.points[0].tornadoPointValue;
  }

  protected collapseExpandBar(tornadoBar: TornadoBarWithChildren, areChildrenExpanded: boolean) {
    // set expand value in both data sources (tornadoData & sortedTornadoData)
    tornadoBar.areChildrenExpanded = areChildrenExpanded;
    this.orderAndComputeVisibleBars();
  }

  // order tornado points in increasing or decreasing order, depending on the objective scale
  private orderTornadoPoints() {
    const orderDirection = this.scaleAxis.from < this.scaleAxis.to ? 1 : -1;
    this.tornadoData.bars.forEach(tornadoBar => {
      tornadoBar.children.forEach(childBar => this.sortTornadoPointLabels(childBar, orderDirection));
      this.sortTornadoPointLabels(tornadoBar, orderDirection);
    });
  }

  private removeTrivialBars() {
    const barIndexesToRemove: number[] = [];
    this.tornadoData.bars.forEach((bar, barIdx) => {
      if (this.isBarTrivial(bar)) {
        barIndexesToRemove.push(barIdx);
      } else {
        const childBarIndexesToRemove: number[] = [];
        bar.children.forEach((childBar, childBarIdx) => {
          if (this.isBarTrivial(childBar)) {
            childBarIndexesToRemove.push(childBarIdx);
          }
        });
        childBarIndexesToRemove.reverse().forEach(idx => bar.children.splice(idx, 1));
      }
    });
    barIndexesToRemove.reverse().forEach(idx => this.tornadoData.bars.splice(idx, 1));
  }

  private sortTornadoPointLabels(tornadoBar: TornadoBar | TornadoBarWithChildren, orderDirection: number) {
    // sort based on tornadoPointValue
    // make sure when points have the same tornadoPointValues that they are sorted in order Worst -> Median -> Best
    const tornadoPointLabelOrder = { Worst: 1, Median: 2, Best: 3, default: Number.MAX_VALUE };
    tornadoBar.points.sort((a, b) => {
      if (a.tornadoPointValue > b.tornadoPointValue) {
        return orderDirection;
      } else if (a.tornadoPointValue < b.tornadoPointValue) {
        return -orderDirection;
      } else {
        return (
          (tornadoPointLabelOrder[a.tornadoPointLabel as keyof typeof tornadoPointLabelOrder] || tornadoPointLabelOrder.default) -
          (tornadoPointLabelOrder[b.tornadoPointLabel as keyof typeof tornadoPointLabelOrder] || tornadoPointLabelOrder.default)
        );
      }
    });
  }
  private sortTornadoBars(tornadoBars: (TornadoBar | TornadoBarWithChildren)[]) {
    tornadoBars.sort(
      (a, b) =>
        Math.abs(b.points.at(-1).tornadoPointValue - b.points[0].tornadoPointValue) -
        Math.abs(a.points.at(-1).tornadoPointValue - a.points[0].tornadoPointValue),
    );
  }

  /**
   * calculates absolute left position in percentages based on a TornadoPoint tornadoPointValue
   */
  protected calculateLeftPercentage(value: number) {
    const numerator = this.scaleAxis.to > this.scaleAxis.from ? value - this.scaleAxis.from : this.scaleAxis.from - value;
    return (
      (numerator / Math.abs(this.scaleAxis.from - this.scaleAxis.to)) * this.chartWidthPercentage + (100 - this.chartWidthPercentage) / 2
    );
  }

  /**
   * calculates bar width in percentages based on a pre-sorted array of TornadoPoints
   */
  protected calculateWidthPercentage(tornadoPoints: TornadoPoint[]) {
    return (
      (Math.abs(tornadoPoints.at(-1).tornadoPointValue - tornadoPoints[0].tornadoPointValue) /
        Math.abs(this.scaleAxis.from - this.scaleAxis.to)) *
      this.chartWidthPercentage
    );
  }

  protected calculateDashedLineTop(index: number) {
    return (index + 0.5) * this.gridRowHeight;
  }

  protected calculateDashedLineHeight(index: number) {
    return this.backgroundAreaHeight - this.calculateDashedLineTop(index) - this.gridRowHeight;
  }

  protected isBarExpandable(tornadoBar: TornadoBar | TornadoBarWithChildren) {
    return this.isParentBar(tornadoBar) && tornadoBar.children.length > 0;
  }

  protected isParentBar(tornadoBar: TornadoBar | TornadoBarWithChildren): tornadoBar is TornadoBarWithChildren {
    return 'children' in tornadoBar;
  }

  /**
   * returns a plus for positive numbers when comparing with another alternative
   */
  protected showPlus(val: number) {
    if (round(val, this.significantDigits) > 0 && this.bothAlternativesSelected) {
      return '+';
    }
    return '';
  }

  protected onPointHovered(barIdx: number, pointIdx: number) {
    this.hoveredBarIdx = barIdx;
    this.hoveredPointIdx = pointIdx;
  }

  protected isVerticalLineVisible(barIdx: number, lineIdx: number) {
    if (lineIdx === 0 || lineIdx === this.visibleTornadoBars[barIdx].points.length - 1) {
      // first or last vertical line
      return this.hoveredBarIdx === barIdx && this.barLimitsHovered;
    } else {
      // middle lines
      return this.hoveredBarIdx === barIdx && this.hoveredPointIdx === lineIdx && (this.middlePointHovered || this.hoveredPointIdx !== -1);
    }
  }

  protected isScaleValueVisible(barIdx: number, scaleValueIdx: number) {
    const isValueOverlapping =
      this.visibleTornadoBars[barIdx].points[scaleValueIdx].tornadoPointValue !== this.scaleAxis.from &&
      this.visibleTornadoBars[barIdx].points[scaleValueIdx].tornadoPointValue !== this.scaleAxis.to;
    if (scaleValueIdx === 0 || scaleValueIdx === this.visibleTornadoBars[barIdx].points.length - 1) {
      // first or last scale value
      return this.hoveredBarIdx === barIdx && this.barLimitsHovered && isValueOverlapping;
    } else {
      // middle scale values
      return this.hoveredBarIdx === barIdx && this.hoveredPointIdx === scaleValueIdx && this.isPointHovered && isValueOverlapping;
    }
  }

  private setOverlappingLimits() {
    this.areLimitsOverlapping = [];
    const bars: NodeListOf<HTMLElement> = document.querySelectorAll('.tornado-bar');
    const limitBarLabels: NodeListOf<HTMLElement> = document.querySelectorAll('.edge-label');
    for (let barIdx = 0; barIdx < this.visibleTornadoBars.length; barIdx++) {
      const leftBarLabelWidth = limitBarLabels[barIdx * 2]?.offsetWidth;
      const rightBarLabelWidth = limitBarLabels[barIdx * 2 + 1]?.offsetWidth;
      const barWidth = bars[barIdx]?.offsetWidth;
      if (leftBarLabelWidth == null || rightBarLabelWidth == null || barWidth == null) break;
      // add 10px spacing between labels
      this.areLimitsOverlapping.push(leftBarLabelWidth / 2 + rightBarLabelWidth / 2 + 10 > barWidth);
    }
  }

  private setIntersectionObservers() {
    for (const intersectionObserver of this.intersectionObservers) {
      // remove old observers
      intersectionObserver.disconnect();
    }
    this.intersectionObservers = [];

    const tornadoBarContainers = Array.from(document.querySelectorAll('.tornado-bar-container'));
    for (const tornadoBarContainer of tornadoBarContainers) {
      const limitPointLabels = Array.from(tornadoBarContainer.querySelectorAll('.edge-label'));

      for (const limitPointLabel of limitPointLabels) {
        const options = {
          root: tornadoBarContainer,
          threshold: 1,
        };

        const intersectionObserver = new IntersectionObserver(observerEntries => {
          for (const observerEntry of observerEntries) {
            if (!observerEntry.isIntersecting) {
              // label outside of diagram
              observerEntry.target.classList.add('limited-bar-label');
            }
          }
        }, options);

        intersectionObserver.observe(limitPointLabel);
      }
    }
  }

  protected summedUpTooltipProbability(tornadoPoint: TornadoPoint) {
    return tornadoPoint.tooltipCombinations.reduce((outerAcc, combination) => outerAcc + combination.probability, 0);
  }

  private updatePercentageSignificantDigits() {
    this.visibleTornadoBars.forEach(bar => {
      bar.points.forEach(point => {
        point.tooltipCombinations.forEach(combination => {
          this.percentageSignificantDigits = Math.max(
            this.percentageSignificantDigits,
            -Math.floor(Math.log10(combination.probability * 100)), // number of zeroes after decimal point
          );
        });
      });
    });
  }

  protected transformTornadoPointLabel(value: number | string): string {
    if (Number.isFinite(+value)) {
      return this.decimalPipe.transform(+value, `1.0-${this.significantDigits}`);
    } else {
      return String(value);
    }
  }
}
