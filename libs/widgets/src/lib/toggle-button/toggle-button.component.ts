import { Component, EventEmitter, Input, Output } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatTooltipModule } from '@angular/material/tooltip';

@Component({
  selector: 'dt-toggle-button',
  templateUrl: './toggle-button.component.html',
  styles: [
    `
      .icon-wrapper {
        display: flex;
        align-items: center;
      }
    `,
  ],
  standalone: true,
  imports: [MatIconModule, MatButtonModule, MatTooltipModule],
})
export class ToggleButtonComponent {
  @Input() value: boolean;
  @Input() icon: string;
  @Input() tooltipTrue: string;
  @Input() tooltipFalse: string;
  @Input() disabled = false;

  @Output() valueChange = new EventEmitter<boolean>();
}
