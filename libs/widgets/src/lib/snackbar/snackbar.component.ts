import { Component, HostBinding, Inject } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MAT_SNACK_BAR_DATA, MatSnackBarModule, MatSnackBarRef } from '@angular/material/snack-bar';

export interface SnackbarData {
  message: string;
  icon?: string;
  dismissButton?: boolean;
  loadingSpinner?: boolean;
  cypressId?: string;
}

@Component({
  templateUrl: './snackbar.component.html',
  styleUrls: ['./snackbar.component.scss'],
  standalone: true,
  imports: [MatButtonModule, MatSnackBarModule, MatProgressSpinnerModule, MatIconModule],
})
export class SnackbarComponent {
  @HostBinding('attr.data-cy')
  get cypressId() {
    return this.data.cypressId;
  }

  constructor(
    public snackBarRef: MatSnackBarRef<SnackbarComponent>,
    @Inject(MAT_SNACK_BAR_DATA) public data: SnackbarData,
  ) {}

  dismissClick() {
    this.snackBarRef.dismiss();
  }
}
