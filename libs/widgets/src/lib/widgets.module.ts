import { OverlayModule } from '@angular/cdk/overlay';
import { CommonModule, DecimalPipe } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatStepperModule } from '@angular/material/stepper';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatNativeDateModule, MatRippleModule } from '@angular/material/core';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatDividerModule } from '@angular/material/divider';
import { MAT_ICON_DEFAULT_OPTIONS, MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatMenuModule } from '@angular/material/menu';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatRadioModule } from '@angular/material/radio';
import { MatSelectModule } from '@angular/material/select';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatSortModule } from '@angular/material/sort';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatCardModule } from '@angular/material/card';
import { MatTabsModule } from '@angular/material/tabs';
import { MatTableModule } from '@angular/material/table';
import { MatChipsModule } from '@angular/material/chips';
import { MatListModule } from '@angular/material/list';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { RouterModule } from '@angular/router';
import { ColorSketchModule } from 'ngx-color/sketch';
import { QuillModule } from 'ngx-quill';
import { PortalModule } from '@angular/cdk/portal';
import { MAT_DIALOG_DEFAULT_OPTIONS, MatDialogModule } from '@angular/material/dialog';
import { MatPaginatorIntl, MatPaginatorModule } from '@angular/material/paginator';
import { Platform } from '@angular/cdk/platform';
import { EVENT_MANAGER_PLUGINS } from '@angular/platform-browser';
import { NoteBtnComponent, NoteBtnPresetPipe } from './note-btn';
import { BarSliderComponent } from './bar-slider';
import { CollapsibleComponent } from './collapsible';
import { DragDropListComponent } from './drag-drop-list';
import { FlexColumnDirective, FlexTableComponent } from './flex-table';
import { InputboxComponent, InputboxGroupedComponent, InputboxSimpleComponent } from './inputbox';
import { RangeInputComponent } from './range-input';
import { TabComponent, TabsComponent } from './tabs';
import { YoutubeModalComponent } from './youtube/youtube-modal.component';
import { TriangleComponent } from './triangle';
import {
  AutoFocusDirective,
  DynamicChangeDirectiveModule,
  InlineSVGDirective,
  OverflowDirective,
  RegexBlockDirective,
  ResizeObserverDirective,
  ScrollbarDirective,
  ScrollIntoViewDirective,
  ValidateNumberInputDirective,
  ViewIntersectionDirective,
  WidthTriggerDirective,
} from './directives';
import { OverlayProgressBarDirective } from './overlay-progress-bar';
import { VerticalSliderComponent } from './vertical-slider';
import { CopyWhistleDirective, PopOverComponent, WhistleComponent } from './popover';
import { AspectBoxComponent } from './aspect-box';
import { PasswordStrengthComponent } from './password-strength/password-strength.component';
import { PasswordFormComponent } from './form-elements/password-form';
import { ModalModule } from './modal/modal.module';
import { LoadingSnackbarComponent } from './loading-snackbar';
import { SelectComponent, SelectGroupDirective, SelectLabelDirective, SelectOptionDirective } from './select';
import {
  InfluenceFactorNamePipe,
  IsValidEmailPipe,
  LocalizedStringPipe,
  NumberCastPipe,
  NumberRoundingAsPower,
  NumberRoundingPipe,
  SafeArrayIndexPipe,
  SafeNumberPipe,
  SafeUrlPipe,
  SecondsToHoursMinutesSeconds,
  SortByPipe,
  StateNamePipe,
  ToFixedPipe,
  TeamMemberRolePipe,
} from './pipes';
import { ObjectBoxComponent } from './object-box/object-box.component';
import { CollapseAllComponent } from './collapse-all/collapse-all.component';
import { EmailConfirmationModalComponent } from './email-confirmation-modal';
import { SnackbarComponent } from './snackbar';
import {
  AccountModalComponent,
  ChangeEmailComponent,
  ChangePasswordComponent,
  ChangeUsernameComponent,
  DeleteAccountModalComponent,
} from './account-modal';
import { RequestPasswordResetModalComponent } from './request-password-reset-modal';
import { RangeInputLogComponent } from './range-input-log/range-input-log.component';
import {
  EvaluationCheckboxesComponent,
  QuestionnaireComponent,
  QuestionnaireQuestionComponent,
  QuestionnaireQuestionNumberComponent,
  QuestionnaireQuestionOptionsComponent,
  QuestionnaireQuestionTableComponent,
  QuestionnaireQuestionTextBlockComponent,
  QuestionnaireQuestionTextComponent,
} from './questionnaire';
import { PaginatorIntlService } from './paginator/paginator-intl.service';
import { ConfirmPopOverComponent } from './popover/confirm/confirm-popover.component';
import { QuillConfigurationService } from './rich-text-editor';
import { OutsideZoneEventManagerPlugin } from './outside-zone-events/outside-zone-event-manager-plugin';
import { InputSpinnerComponent } from './spinners/input-spinner.component';

const DECLARE_AND_EXPORT = [
  BarSliderComponent,
  CollapseAllComponent,
  CollapsibleComponent,
  VerticalSliderComponent,
  DragDropListComponent,
  InputboxComponent,
  InputboxGroupedComponent,
  InputboxSimpleComponent,
  RegexBlockDirective,
  RangeInputComponent,
  TabComponent,
  TabsComponent,
  PopOverComponent,
  WhistleComponent,
  CopyWhistleDirective,
  TriangleComponent,
  ViewIntersectionDirective,
  ScrollbarDirective,
  YoutubeModalComponent,
  FlexTableComponent,
  FlexColumnDirective,
  SafeArrayIndexPipe,
  SortByPipe,
  NumberCastPipe,
  StateNamePipe,
  InfluenceFactorNamePipe,
  ValidateNumberInputDirective,
  NumberRoundingPipe,
  AspectBoxComponent,
  ToFixedPipe,
  PasswordStrengthComponent,
  PasswordFormComponent,
  ScrollIntoViewDirective,
  EvaluationCheckboxesComponent,
  LoadingSnackbarComponent,
  InlineSVGDirective,
  ObjectBoxComponent,
  SelectComponent,
  SelectLabelDirective,
  SelectOptionDirective,
  SelectGroupDirective,
  InputSpinnerComponent,
  RangeInputLogComponent,
  EmailConfirmationModalComponent,
  AccountModalComponent,
  RequestPasswordResetModalComponent,
  QuestionnaireComponent,
  QuestionnaireQuestionComponent,
  QuestionnaireQuestionTextBlockComponent,
  QuestionnaireQuestionNumberComponent,
  QuestionnaireQuestionOptionsComponent,
  QuestionnaireQuestionTableComponent,
  QuestionnaireQuestionTextComponent,
  IsValidEmailPipe,
];

const COMMON_RE_EXPORTS = [
  MatMenuModule,
  MatCheckboxModule,
  MatSnackBarModule,
  MatIconModule,
  MatInputModule,
  MatDatepickerModule,
  MatNativeDateModule,
  MatProgressSpinnerModule,
  MatProgressBarModule,
  MatButtonModule,
  MatSortModule,
  MatRadioModule,
  MatTooltipModule,
  MatDividerModule,
  MatSidenavModule,
  MatToolbarModule,
  MatSelectModule,
  MatRippleModule,
  MatListModule,
  PortalModule,
  MatCardModule,
  MatTabsModule,
  MatStepperModule,
  MatExpansionModule,
  FormsModule,
  DragDropModule,
  ReactiveFormsModule,
  MatSlideToggleModule,
  MatDialogModule,
  ModalModule,
  MatPaginatorModule,
  MatTableModule,
  MatChipsModule,
  MatAutocompleteModule,
  DynamicChangeDirectiveModule,
  SafeUrlPipe,
  SecondsToHoursMinutesSeconds,
  TeamMemberRolePipe,
  SafeNumberPipe,
  NumberRoundingAsPower,
];

@NgModule({
  imports: [
    ...COMMON_RE_EXPORTS,
    OverlayProgressBarDirective,
    WidthTriggerDirective,
    ColorSketchModule,
    CommonModule,
    NoteBtnComponent,
    OverflowDirective,
    SnackbarComponent,
    OverlayModule,
    ConfirmPopOverComponent,
    LocalizedStringPipe,
    AutoFocusDirective,
    ResizeObserverDirective,
    RouterModule,
    QuillModule.forRoot(),
    NoteBtnPresetPipe,
  ],
  declarations: [
    ...DECLARE_AND_EXPORT,
    ChangeEmailComponent,
    ChangePasswordComponent,
    ChangeUsernameComponent,
    DeleteAccountModalComponent,
  ],
  exports: [
    ...DECLARE_AND_EXPORT,
    ...COMMON_RE_EXPORTS,
    OverlayProgressBarDirective,
    WidthTriggerDirective,
    ConfirmPopOverComponent,
    QuillModule,
  ],
  providers: [
    DecimalPipe,
    {
      provide: MAT_DIALOG_DEFAULT_OPTIONS,
      useValue: {
        hasBackdrop: true,
        backdropClass: 'dt-mat-dialog-backdrop',
      },
    },
    {
      provide: MAT_ICON_DEFAULT_OPTIONS,
      useValue: {
        fontSet: 'material-symbols-outlined',
      },
    },
    {
      provide: MatPaginatorIntl,
      useClass: PaginatorIntlService,
    },
    {
      provide: EVENT_MANAGER_PLUGINS,
      multi: true,
      useClass: OutsideZoneEventManagerPlugin,
    },
  ],
})
export class WidgetsModule {
  constructor(platform: Platform, quillConfigurationService: QuillConfigurationService) {
    quillConfigurationService.configureQuill();

    if (platform.WEBKIT) {
      document.body.classList.add('dt-webkit');
    }
  }
}
