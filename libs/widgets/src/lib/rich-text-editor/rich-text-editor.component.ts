import { Component, EventEmitter, Input, NgZone, Output } from '@angular/core';
import { throttle } from 'lodash';
import { QuillEditorBase, QuillEditorComponent } from 'ngx-quill';
import { ImageSpec } from 'quill-blot-formatter';
import { MatIconModule } from '@angular/material/icon';
import { ControlValueAccessor, FormsModule, NG_VALUE_ACCESSOR } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { convertImageForQuill } from '../tools/convert-image-for-quill';
import { PopOverService } from '../popover';
import { WidthTriggerDirective } from '../directives';
import { QuillConfigurationService } from './configure-quill.service';

/*
 Usage:
 <div class="note-wrapper">
    <input placeholder="Ich bin ein Inputfeld" type="text">
    <dt-note-btn [(comment)]="meineNotiz"></dt-note-btn>
 </div>
 */

@Component({
  selector: 'dt-rich-text-editor',
  templateUrl: './rich-text-editor.component.html',
  styleUrls: ['./rich-text-editor.component.scss'],
  standalone: true,
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      multi: true,
      useExisting: RichTextEditorComponent,
    },
  ],
  imports: [FormsModule, MatIconModule, QuillEditorComponent, CommonModule, WidthTriggerDirective],
})
export class RichTextEditorComponent implements ControlValueAccessor {
  @Input() text: string;
  @Input() format: 'html' | 'object' | 'text' | 'json' = 'json';
  @Output() textChange: EventEmitter<string> = new EventEmitter();

  @Input() characterCount: number;
  @Output() characterCountChange = new EventEmitter<number>();

  @Input() readonly = false;

  @Input() modalPadding = false; // Applies a padding that is equal to the padding of the title of modals

  @Input() placeholder = '';

  @Input() withToolbar = true;

  editorModules = {};
  showExtraTools = false;
  isMediumBreakpoint = false;
  isSmallBreakpoint = false;
  isMobileBreakpoint = false;

  quill: any;

  onChange: (comment: string) => void;
  onTouched: () => void;
  touched = false;

  constructor(
    private cfgQuillService: QuillConfigurationService,
    private zone: NgZone,
    private popoverService: PopOverService,
  ) {
    this.editorModules = {
      toolbar: {
        container: [
          [{ header: [1, 2, 3, 4, 5, 6, false] }],
          ['bold', 'italic', 'underline', 'strike'],
          [{ color: [] }, { background: [] }],
          [{ font: [] }],
          [{ align: [] }],
          [{ script: 'sub' }, { script: 'super' }],
          [{ indent: '-1' }, { indent: '+1' }],

          ['blockquote', 'code-block'],
          [{ list: 'ordered' }, { list: 'bullet' }],
          [{ direction: 'rtl' }],

          ['clean'],

          ['link', 'image'],
        ],
      },
      blotFormatter: {
        specs: [ImageSpec],
      },
    };
  }

  onEditorCreated(ref: any, ngxQuillInstance: QuillEditorBase) {
    this.quill = ref;

    this.cfgQuillService.configureQuillInstance(ref, ngxQuillInstance);

    const quillL = this.quill;

    const updateCount = throttle(() => {
      this.characterCount = quillL.getText().length - 1;
      this.characterCountChange.emit(this.characterCount);
    }, 500);

    updateCount();

    this.quill.on('text-change', () => {
      this.zone.run(() => {
        updateCount();
      });
    });
  }

  async uploadImage(input: HTMLInputElement, imageUploadButton: HTMLElement) {
    if (input.files[0] == null) {
      return;
    }
    const selectedFile = input.files[0];

    input.value = '';

    if (!selectedFile.type.startsWith('image/')) {
      return;
    }

    const readURL = (file: any) => {
      return new Promise<string>((res, rej) => {
        const reader = new FileReader();
        reader.onload = e => res(e.target.result as string);
        reader.onerror = e => rej(e);
        reader.readAsDataURL(file);
      });
    };

    const url = await readURL(selectedFile);

    const convertedUrl = await convertImageForQuill(url).catch(() => {
      return '';
    });

    if (!convertedUrl) {
      this.popoverService.whistle(imageUploadButton, $localize`Das Format des hochgeladenen Bildes wird nicht unterstützt.`, 'error', 3000);
      return;
    }

    const selection = this.quill.getSelection(true);
    this.quill.insertEmbed(selection.index, 'image', convertedUrl, 'user');
  }

  onTextChange(newText: string) {
    this.textChange.emit(newText);
    this.onChange?.(newText);
    this.markAsTouched();
  }

  toggleExtraTools(): void {
    this.showExtraTools = !this.showExtraTools;
  }

  /////////////////////////////////////////////////////////////////
  // ControlValueAccessor implementation
  /////////////////////////////////////////////////////////////////

  writeValue(text: string) {
    this.text = text;
  }

  registerOnChange(onChange: (newText: string) => void) {
    this.onChange = onChange;
  }

  registerOnTouched(onTouched: () => void) {
    this.onTouched = onTouched;
  }

  setDisabledState(disabled: boolean) {
    // Disabled in the form means that the value can not be changed. This corresponds to readonly
    // in this component.
    this.readonly = disabled;
  }

  private markAsTouched() {
    if (!this.touched) {
      this.onTouched?.();
      this.touched = true;
    }
  }
}
