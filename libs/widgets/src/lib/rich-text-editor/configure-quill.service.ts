import { Injectable, NgZone } from '@angular/core';
import { QuillEditorBase, QuillService } from 'ngx-quill';
import { Quill } from 'quill';
import BlotFormatter from 'quill-blot-formatter';
import Delta from 'quill-delta';
import { ColorPickerService } from '../color-picker';
import { convertImageForQuill } from '../tools/convert-image-for-quill';

/* eslint-disable @typescript-eslint/naming-convention */

@Injectable({ providedIn: 'root' })
export class QuillConfigurationService {
  constructor(
    private colorPickerService: ColorPickerService,
    private ngZone: NgZone,
    private service: QuillService,
  ) {}

  async configureQuill() {
    // eslint-disable-next-line @typescript-eslint/no-var-requires
    const quill = await import('quill').then(m => m.default);
    quill.register('modules/blotFormatter', BlotFormatter);

    const cps = this.colorPickerService;
    const ngZone = this.ngZone;

    class OurColorPicker {
      container: HTMLElement;
      label: HTMLElement;

      constructor(
        private quillInstance: any,
        select: HTMLElement,
        labelContent: any,
        private format: string,
      ) {
        ngZone.run(() => {
          this.container = document.createElement('span');
          this.container.classList.add('ql-picker');
          this.container.classList.add('ql-color');
          this.container.classList.add('ql-color-picker');

          select.style.display = 'none';

          this.label = document.createElement('span');
          this.label.classList.add('ql-picker-label');
          this.label.innerHTML = labelContent;
          this.label.tabIndex = 0;
          this.label.setAttribute('role', 'button');
          this.label.setAttribute('aria-expanded', 'false');
          this.container.appendChild(this.label);

          this.label.addEventListener('mousedown', () => {
            cps.openColorPicker(this.label, this.quillInstance.getFormat()[this.format] || '#000000').subscribe((newColor: string) => {
              while (select.firstChild) {
                select.removeChild(select.firstChild);
              }

              const option = document.createElement('option');
              option.setAttribute('value', newColor);
              select.appendChild(option);

              (select as HTMLSelectElement).selectedIndex = 0;

              select.dispatchEvent(new Event('change'));
            });
          });

          select.parentNode.insertBefore(this.container, select);
        });
      }

      update() {
        // Change the icon color to the color / background of the selection
        const colorLabel: HTMLElement = this.label.querySelector('.ql-color-label');
        const selection = this.quillInstance.getSelection(false);
        if (selection) {
          const value = this.quillInstance.getFormat(selection)[this.format] || '#000000';

          if (colorLabel) {
            if (colorLabel.tagName === 'line') {
              colorLabel.style.stroke = value;
            } else {
              colorLabel.style.fill = value;
            }
          }
        }
      }

      // eslint-disable-next-line @typescript-eslint/no-empty-function
      close() {}
    }

    const BaseTheme = quill.import('themes/snow');

    class ExtendedTheme extends BaseTheme {
      buildPickers(selects: HTMLElement[], formatIcons: any) {
        const colorSelects: HTMLElement[] = [];
        const backgroundSelects: HTMLElement[] = [];

        selects = selects.filter(s => {
          if (s.classList.contains('ql-color')) {
            s.classList.remove('ql-color');
            colorSelects.push(s);
            return false;
          }

          if (s.classList.contains('ql-background')) {
            s.classList.remove('ql-background');
            backgroundSelects.push(s);
            return false;
          }

          return true;
        });

        super.buildPickers(selects, formatIcons);

        colorSelects.forEach(cb => cb.classList.add('ql-color'));
        backgroundSelects.forEach(cb => cb.classList.add('ql-background'));

        let additional: OurColorPicker[] = [];
        this.pickers.push(
          ...(additional = [...colorSelects, ...backgroundSelects].map(select => {
            const format = select.classList.contains('ql-background') ? 'background' : 'color';

            return new OurColorPicker(this.quill, select, formatIcons[format], format);
          })),
        );

        const update = () => {
          additional.forEach(picker => {
            picker.update();
          });
        };
        this.quill.on('editor-change', update);
      }
    }

    quill.register('themes/snow', ExtendedTheme, true);

    const imageFormatAttributes = ['alt', 'height', 'width', 'style', 'data-align'];

    const BaseImageFormat = quill.import('formats/image');
    class ImageFormat extends BaseImageFormat {
      static formats(domNode: HTMLElement) {
        return imageFormatAttributes.reduce((formats, attribute) => {
          if (domNode.hasAttribute(attribute)) {
            (formats as any)[attribute] = domNode.getAttribute(attribute);
          }
          return formats;
        }, {});
      }
      format(name: string, value: any) {
        if (imageFormatAttributes.indexOf(name) > -1) {
          if (value) {
            this.domNode.setAttribute(name, value);
          } else {
            this.domNode.removeAttribute(name);
          }
        } else {
          super.format(name, value);
        }
      }
    }

    quill.register(ImageFormat, true);

    // Native Video
    const BlockEmbed = quill.import('blots/block/embed');

    class NativeVideoBlot extends BlockEmbed {
      static create(parameter: any) {
        const node: HTMLElement = super.create();
        node.setAttribute('controls', 'true');
        node.setAttribute('name', 'media');

        const sourceElement = document.createElement('source');
        sourceElement.setAttribute('src', parameter.url);

        node.appendChild(sourceElement);

        return node;
      }

      static value(node: HTMLElement) {
        return {
          url: node.children[0].getAttribute('src'),
        };
      }
    }
    NativeVideoBlot.blotName = 'native-video';
    NativeVideoBlot.tagName = 'video';

    quill.register(NativeVideoBlot);

    // Icons
    const icons = quill.import('ui/icons');
    icons['native-video'] = icons['video'];
    icons['video'] = '<i class="material-symbols-outlined" style="font-size: 18px;">video_call</i>';

    // Clipboard Scroll Fix #747
    /**
     * 1. We make the quill.container scrollable using css so we have to use that instead to reset the scrolling position.
     * 2. Replace the setTimeout(..., 1) call with requestAnimationFrame so we don't sometimes cause a flicker if the browser is too slow.
     */
    const Clipboard = quill.import('modules/clipboard');

    class OurClipboard extends Clipboard {
      constructor(quill: unknown, options: unknown) {
        super(quill, options);
      }
      onPaste(e: ClipboardEvent) {
        if (e.defaultPrevented || !this.quill.isEnabled()) return;

        for (const item of e.clipboardData.items) {
          if (item.kind === 'file') {
            const file = item.getAsFile();

            if (!file.type.startsWith('image/')) {
              continue;
            }

            // Turn to Base64
            const reader = new FileReader();

            e.preventDefault();

            reader.onload = () => {
              const base64 = reader.result as string;

              convertImageForQuill(base64)
                .catch(() => '')
                .then((convertedBase64: string) => {
                  if (convertedBase64) {
                    const range = this.quill.getSelection();

                    this.quill.insertEmbed(range.index, 'image', convertedBase64, 'user');
                  }
                });
            };

            reader.readAsDataURL(file);

            return;
          }
        }

        const range = this.quill.getSelection();
        let delta = new Delta().retain(range.index);
        const scrollTop = this.quill.container.scrollTop;
        this.container.focus();
        this.quill.selection.update('silent');
        requestAnimationFrame(() => {
          delta = delta.concat(this.convert()).delete(range.length);
          this.quill.updateContents(delta, 'user');
          // range.length contributes to delta.length()
          this.quill.setSelection(delta.length() - range.length, 'silent');
          this.quill.container.scrollTop = scrollTop;
          this.quill.focus();
        });
      }
    }

    quill.register('modules/clipboard', OurClipboard, true);
  }

  configureQuillInstance(quill: any, ngxQuillInstance: QuillEditorBase) {
    // Toolbar
    const toolbarModule = quill.getModule('toolbar');
    toolbarModule.addHandler('image', () => quill.theme.tooltip.edit('image'));
    toolbarModule.addHandler('native-video', () => quill.theme.tooltip.edit('native-video'));

    const service = this.service,
      zone = this.ngZone;

    /*
      Performance Improvements
        - Generally all of these modifications try to reduce calls that somehow convert the
          data model of a quill editor into a string because that gets REALLY expensive
          allocation wise if you have big base64 encoded images in there.
    */

    /*
      See QuillEditorBase.valueGetter for original.

      This modification (mostly) just moves the call
        `editorElement.querySelector('.ql-editor').innerHTML`
      inside the html format branch so it doesn't get called if we don't actually use the html model.
    */
    ngxQuillInstance.valueGetter = function (this: QuillEditorBase, quillEditor: Quill, editorElement: HTMLElement) {
      let modelValue = null;
      const format = this.format || service.config.format || 'html';
      if (format === 'html') {
        modelValue = editorElement.querySelector('.ql-editor').innerHTML;
        if (modelValue === '<p><br></p>' || modelValue === '<div><br></div>') {
          modelValue = null;
        }
      } else if (format === 'text') {
        modelValue = quillEditor.getText();
      } else if (format === 'object') {
        modelValue = quillEditor.getContents();
      } else if (format === 'json') {
        try {
          modelValue = JSON.stringify(quillEditor.getContents());
        } catch (e) {
          modelValue = quillEditor.getText();
        }
      }
      return modelValue;
    };

    // We have to disable the original textChangeHandler or it will still be the one used even if we override the field in QuillEditorBase.
    ngxQuillInstance.quillEditor.off('text-change', ngxQuillInstance.textChangeHandler);

    /*
      See QuillEditorBase.textChangeHandler for original.

      This modification reorders the function so we fail fast and therefore save on time if we don't have subscribers to the change events.
      It also seperates the code for ngModelChange and onContentChanged so we save on time if only one of them has subscribers.
    */
    ngxQuillInstance.textChangeHandler = function (this: QuillEditorBase, delta: Delta, oldDelta: Delta, source: string) {
      const trackChanges = this.trackChanges || service.config.trackChanges;

      const shouldTriggerOnModelChange = (source === 'user' || (trackChanges && trackChanges === 'all')) && !!this.onModelChange;
      if (shouldTriggerOnModelChange) {
        zone.run(() => {
          this.onModelChange(this.valueGetter(this.quillEditor, this.editorElem));
        });
      }

      if (this.onContentChanged.observed) {
        const text = this.quillEditor.getText();
        const content = this.quillEditor.getContents();
        let html = this.editorElem.querySelector('.ql-editor').innerHTML;
        if (html === '<p><br></p>' || html === '<div><br></div>') {
          html = null;
        }

        zone.run(() => {
          this.onContentChanged.emit({
            content: content,
            delta: delta,
            editor: this.quillEditor,
            html: html,
            oldDelta: oldDelta,
            source: source,
            text: text,
          });
        });
      }
    }.bind(ngxQuillInstance);

    // Set our textChangeHandler to run on text-change
    ngxQuillInstance.quillEditor.on('text-change', ngxQuillInstance.textChangeHandler);

    // Allow entering image & video url
    const old = quill.theme.tooltip.save;
    quill.theme.tooltip.save = function () {
      const { value } = this.textbox;
      const dataMode = this.root.getAttribute('data-mode');
      if (dataMode === 'image' && value) {
        const range = this.quill.getSelection(true);
        if (range != null) {
          this.quill.insertEmbed(range.index, 'image', value, 'user');
        }
      } else if (dataMode === 'native-video' && value) {
        const range = this.quill.getSelection(true);
        if (range != null) {
          this.quill.insertEmbed(range.index, 'native-video', { url: value }, 'user');
        }
      } else {
        old.call(this);
      }
    };

    // Properly Position Tooltips
    quill.theme.tooltip.position = function (reference: any) {
      const left = reference.left + reference.width / 2 - this.root.offsetWidth / 2;
      // root.scrollTop should be 0 if scrollContainer !== root
      const parentScroll = (this.quill.root as HTMLElement).parentElement.scrollTop;
      const actualScroll = this.quill.root.scrollTop > 0 ? this.quill.root.scrollTop : parentScroll;
      const top = reference.bottom + actualScroll;
      this.root.style.left = `${left}px`;
      this.root.style.top = `${top}px`;
      this.root.classList.remove('ql-flip');
      const containerBounds = this.boundsContainer.getBoundingClientRect();
      const rootBounds = this.root.getBoundingClientRect();
      let shift = 0;
      if (rootBounds.right > containerBounds.right) {
        shift = containerBounds.right - rootBounds.right;
        this.root.style.left = `${left + shift}px`;
      }
      if (rootBounds.left < containerBounds.left) {
        shift = containerBounds.left - rootBounds.left;
        this.root.style.left = `${left + shift}px`;
      }
      if (rootBounds.bottom > containerBounds.bottom) {
        const height = rootBounds.bottom - rootBounds.top;
        const verticalShift = reference.bottom - reference.top + height;
        this.root.style.top = `${top - verticalShift}px`;
        this.root.classList.add('ql-flip');
      }
      return shift;
    };
  }
}
