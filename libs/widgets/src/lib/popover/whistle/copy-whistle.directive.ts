import { Directive, ElementRef, HostListener } from '@angular/core';
import { PopOverService } from '../popover.service';

@Directive({
  // eslint-disable-next-line @angular-eslint/directive-selector
  selector: '[cdkCopyToClipboard]',
})
export class CopyWhistleDirective {
  constructor(
    private popOverService: PopOverService,
    private elementRef: ElementRef<HTMLElement>,
  ) {}

  @HostListener('cdkCopyToClipboardCopied', ['$event'])
  onCopied(success: boolean) {
    if (success) {
      this.popOverService.whistle(this.elementRef.nativeElement, $localize`In die Zwischenablage kopiert!`, 'done');
    } else {
      this.popOverService.whistle(
        this.elementRef.nativeElement,
        $localize`Text konnte nicht in die Zwischenablage kopiert werden.`,
        'error',
      );
    }
  }
}
