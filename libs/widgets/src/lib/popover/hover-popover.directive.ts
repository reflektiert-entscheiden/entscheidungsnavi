import { ConnectedPosition } from '@angular/cdk/overlay';
import {
  Directive,
  ElementRef,
  EventEmitter,
  HostBinding,
  HostListener,
  Input,
  NgZone,
  OnDestroy,
  OnInit,
  Output,
  Renderer2,
  TemplateRef,
} from '@angular/core';
import { first, fromEvent, merge, Subject, takeUntil, timer } from 'rxjs';
import { Platform } from '@angular/cdk/platform';
import { PopOverRef, PopOverService, ScrollStrategy } from './popover.service';

export type SimplePosition = 'bottom' | 'right' | 'left' | 'top';

/**
 * A popover that opens automatically when the user hovers over an element.
 *
 * On mobile, there are two fallbacks as there is no hovering:
 * - The popover opens when the element is clicked. This can be disabled using `dtHoverPopoverDisableClick`.
 * - The popover opens on a longpress of the element.
 */
@Directive({
  selector: '[dtHoverPopover]',
  standalone: true,
})
export class HoverPopOverDirective implements OnInit, OnDestroy {
  @Input('dtHoverPopover') popoverTemplate: TemplateRef<any>;

  @Input() dtHoverPopoverContext: any;
  @Input() dtHoverPopoverPosition: SimplePosition = 'bottom';
  @Input() dtHoverPopoverScrollStrategy: ScrollStrategy = 'noop';
  @Input() dtHoverPopoverDelay = 0;
  @Input() dtHoverPopoverDisabled = false;
  @Input() dtHoverPopoverDisableAnimation = false;
  @Input() dtHoverPopoverDisableClick = false;
  @Input() dtHoverPopoverClickthrough = false;

  @Output() popoverOpened = new EventEmitter<void>();
  @Output() popoverClosed = new EventEmitter<void>();

  private popOverRef: PopOverRef = null;

  @HostBinding('class.dt-hover-popover-open')
  get isOpen() {
    return this.popOverRef != null;
  }

  constructor(
    private popOverService: PopOverService,
    private hostRef: ElementRef<any>,
    private platform: Platform,
    private renderer: Renderer2,
    private zone: NgZone,
  ) {}

  @HostListener('pointerenter', ['$event'])
  pointerEnter(event: PointerEvent) {
    // For touch devices, we use the other events to open the popover
    if (event.pointerType === 'touch') {
      return;
    }

    this.open(this.dtHoverPopoverDelay);
  }

  @HostListener('contextmenu', ['$event'])
  contextMenu(event: PointerEvent) {
    if (event.pointerType === 'touch') {
      event.preventDefault();
      this.open(0);
    }
  }

  @HostListener('click', ['$event'])
  openClick(event: MouseEvent) {
    if (this.dtHoverPopoverDisableClick) {
      return;
    }

    event.stopImmediatePropagation();
    this.open(0);
  }

  /**
   * Safari does not fire the `contextmenu` event. Thus, we need to create our own long press.
   */
  @HostListener('touchstart', ['$event'])
  touchStart(_event: TouchEvent) {
    if (!this.platform.SAFARI) {
      return;
    }

    timer(500)
      .pipe(takeUntil(merge(fromEvent(document, 'touchmove'), fromEvent(document, 'touchend'), fromEvent(document, 'touchcancel'))))
      .subscribe(() => {
        // Prevent default on the next touchend/touchcancel
        merge(fromEvent(document, 'touchend'), fromEvent(document, 'touchcancel'))
          .pipe(first())
          .subscribe(event => event.preventDefault());

        this.open(0);
      });
  }

  ngOnInit() {
    this.renderer.setStyle(this.hostRef.nativeElement, 'user-select', 'none');
    this.renderer.setStyle(this.hostRef.nativeElement, '-webkit-user-select', 'none');
  }

  ngOnDestroy() {
    this.close();
  }

  /**
   * @param delay - Delay in ms before the popover opens.
   * @param manualControl - If true, the popover will open even if `dtHoverPopoverDisabled` is true and
   * it will not close unless `close` is called.
   */
  open(delay: number, manualControl = false) {
    if ((!manualControl && this.dtHoverPopoverDisabled) || this.popoverTemplate == null) {
      return;
    }

    const position = this.getPosition();

    const closeSubject = new Subject<void>();

    if (!manualControl) {
      this.zone.runOutsideAngular(() => {
        // Clone popover when click or mouseover outside it and the host element
        merge(fromEvent(document, 'mouseover'), fromEvent(document, 'click'))
          .pipe(takeUntil(closeSubject))
          .subscribe(event => {
            if (
              !event
                .composedPath()
                .some(element => element === this.popOverRef?.overlayRef.overlayElement || element === this.hostRef.nativeElement)
            ) {
              this.zone.run(() => {
                closeSubject.next();
                closeSubject.complete();
              });
            }
          });

        // Close popover on touch scroll anywhere on the screen
        fromEvent(document, 'touchmove')
          .pipe(takeUntil(closeSubject))
          .subscribe(() => {
            this.zone.run(() => {
              closeSubject.next();
              closeSubject.complete();
            });
          });
      });
    }

    timer(delay)
      .pipe(takeUntil(closeSubject))
      .subscribe(() => {
        if (this.popOverRef != null) {
          closeSubject.next();
          closeSubject.complete();
          return;
        }

        closeSubject.pipe(first()).subscribe(() => this.close());

        this.popOverRef = this.popOverService.open(this.popoverTemplate, this.hostRef, {
          hasBackdrop: false,
          panelClass: 'dt-hover-popover-panel',
          position,
          disableAnimation: this.dtHoverPopoverDisableAnimation,
          scrollStrategy: this.dtHoverPopoverScrollStrategy,
          templateContext: this.dtHoverPopoverContext,
          closeCallback: () => {
            closeSubject.next();
            closeSubject.complete();
          },
        });

        if (this.dtHoverPopoverClickthrough) {
          this.popOverRef.overlayRef.overlayElement.style.pointerEvents = 'none';
        }

        this.popoverOpened.emit();
      });
  }

  private getPosition() {
    // Use bottom placement as fallback,
    // even when right position is selected. On smaller screens, right positioning may not be possible.
    const position: ConnectedPosition[] = [
      { originX: 'center', originY: 'bottom', overlayX: 'center', overlayY: 'top' },
      { originX: 'start', originY: 'top', overlayX: 'start', overlayY: 'bottom' },
    ];

    if (this.dtHoverPopoverPosition === 'right') {
      position.splice(
        0,
        0,
        { originX: 'end', originY: 'center', overlayX: 'start', overlayY: 'center' },
        { originX: 'start', originY: 'center', overlayX: 'end', overlayY: 'center' },
      );
    } else if (this.dtHoverPopoverPosition === 'left') {
      position.splice(
        0,
        0,
        { originX: 'start', originY: 'center', overlayX: 'end', overlayY: 'top' },
        { originX: 'end', originY: 'center', overlayX: 'start', overlayY: 'center' },
      );
    } else if (this.dtHoverPopoverPosition === 'top') {
      position.splice(
        0,
        0,
        { originX: 'center', originY: 'top', overlayX: 'center', overlayY: 'bottom' },
        { originX: 'center', originY: 'bottom', overlayX: 'center', overlayY: 'top' },
      );
    }

    return position;
  }

  close() {
    this.popOverRef?.close();
    this.popOverRef = null;

    this.popoverClosed.emit();
  }
}
