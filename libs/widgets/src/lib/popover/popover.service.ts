import { ElementRef, Injectable, TemplateRef } from '@angular/core';
import { ConnectedPosition, Overlay, OverlayRef } from '@angular/cdk/overlay';
import { ComponentPortal, ComponentType } from '@angular/cdk/portal';
import { ThemePalette } from '@angular/material/core';
import { assertUnreachable } from '@entscheidungsnavi/tools/assert-unreachable';
import { firstValueFrom, map, Subject, takeUntil } from 'rxjs';
import { ButtonColor } from '../tools';
import { PopOverComponent } from './popover.component';
import { WhistleComponent } from './whistle';
import { ConfirmPopOverComponent } from './confirm/confirm-popover.component';

export type PopOverRef = { close: () => void; closed$: Subject<void>; overlayRef: OverlayRef };
export type ComponentPopOverRef<T> = PopOverRef & { componentInstance: T };

export type ScrollStrategy = 'reposition' | 'close' | 'noop';

type PopOverConfig = {
  closeCallback?: () => void;
  dimensions?: { width?: number; height?: number };
  position?: ConnectedPosition[];
  transformOriginOn?: string;
  hasBackdrop?: boolean;
  panelClass?: string;
  disableAnimation?: boolean;
  scrollStrategy?: ScrollStrategy;
  templateContext?: any;
};

@Injectable({ providedIn: 'root' })
export class PopOverService {
  constructor(private overlay: Overlay) {}

  open(toOpen: TemplateRef<any>, attachedTo: ElementRef<HTMLElement> | HTMLElement, config?: PopOverConfig): PopOverRef;
  open<T>(toOpen: ComponentType<T>, attachedTo: ElementRef<HTMLElement> | HTMLElement, config?: PopOverConfig): ComponentPopOverRef<T>;
  open<T>(
    toOpen: TemplateRef<any> | ComponentType<T>,
    attachedTo: ElementRef<HTMLElement> | HTMLElement,
    config?: PopOverConfig,
  ): ComponentPopOverRef<T> | PopOverRef;
  open(
    toOpen: TemplateRef<any> | ComponentType<any>,
    attachedTo: ElementRef<HTMLElement> | HTMLElement,
    {
      closeCallback = null,
      dimensions = null,
      position = [{ originX: 'center', originY: 'bottom', overlayX: 'center', overlayY: 'top' }],
      transformOriginOn = 'dt-popover',
      hasBackdrop = true,
      panelClass = null,
      disableAnimation = false,
      scrollStrategy = 'noop',
      templateContext = {},
    }: PopOverConfig = {},
  ): PopOverRef | ComponentPopOverRef<any> {
    if (attachedTo instanceof ElementRef) {
      attachedTo = attachedTo.nativeElement;
    }

    let positionStrategy = this.overlay.position().flexibleConnectedTo(attachedTo).withPositions(position);
    const scrollStrategyObj = this.getScrollStrategy(scrollStrategy);

    if (transformOriginOn) {
      positionStrategy = positionStrategy.withTransformOriginOn(transformOriginOn);
    }

    const overlayRef = this.overlay.create({
      positionStrategy: positionStrategy,
      scrollStrategy: scrollStrategyObj,
      hasBackdrop,
      backdropClass: 'dt-popover-backdrop',
      panelClass,
      width: dimensions?.width,
      height: dimensions?.height,
    });
    const closed$ = new Subject<void>();

    const close = () => {
      closed$.next();
      closed$.complete();
      overlayRef.detach();
      overlayRef.detachBackdrop();

      closeCallback?.();
    };

    overlayRef.hostElement.style.pointerEvents = 'none';

    overlayRef.backdropClick().subscribe((event: MouseEvent) => {
      event.stopImmediatePropagation();
      close();
    });

    overlayRef.keydownEvents().subscribe(event => {
      if (event.key === 'Escape') {
        event.stopImmediatePropagation();
        close();
      }
    });

    const portal = new ComponentPortal(PopOverComponent);
    const component = overlayRef.attach(portal).instance;
    component.disableAnimation = disableAnimation;

    component.popoverClose.subscribe(() => close());
    component.finishedLeaveAnimation.subscribe(() => {
      overlayRef.dispose();
    });

    if (toOpen instanceof TemplateRef) {
      component.templateRef = toOpen;
      component.templateContext = templateContext;
      return { close, closed$, overlayRef };
    } else {
      const popComponentInstance = component.portalOutlet.attach(new ComponentPortal(toOpen)).instance;

      return { close, closed$, componentInstance: popComponentInstance, overlayRef };
    }
  }

  whistle(attachedTo: ElementRef<HTMLElement> | HTMLElement, text: string, icon?: string, duration = 2000, iconColor?: ThemePalette) {
    const ref = this.showAnimation(WhistleComponent, attachedTo, 2);
    ref.componentInstance.text = text;
    ref.componentInstance.icon = icon;
    ref.componentInstance.duration = duration;
    ref.componentInstance.iconColor = iconColor;
  }

  confirm(
    attachedTo: ElementRef<HTMLElement> | HTMLElement,
    prompt: string,
    options: Partial<{ confirmText: string; confirmColor: ButtonColor; discardText: string; position: ConnectedPosition[] }> = {},
  ) {
    const closeCallback$ = new Subject<void>();
    let confirmed = false;

    const popover = this.open(ConfirmPopOverComponent, attachedTo, {
      position: options.position,
      closeCallback: () => {
        closeCallback$.next();
        closeCallback$.complete();
      },
    });

    popover.componentInstance.prompt = prompt;
    if (options.confirmText) popover.componentInstance.confirmText = options.confirmText;
    if ('confirmColor' in options) popover.componentInstance.confirmColor = options.confirmColor;
    if (options.discardText) popover.componentInstance.discardText = options.discardText;

    popover.componentInstance.discardClick.pipe(takeUntil(closeCallback$)).subscribe(() => popover.close());
    popover.componentInstance.confirmClick.pipe(takeUntil(closeCallback$)).subscribe(() => {
      confirmed = true;
      popover.close();
    });

    return firstValueFrom(closeCallback$.pipe(map(() => confirmed)));
  }

  showAnimation<T>(
    toOpen: ComponentType<T>,
    attachedTo: ElementRef<HTMLElement> | HTMLElement,
    animationsToRun?: number,
  ): ComponentPopOverRef<T>;
  showAnimation(toOpen: TemplateRef<any>, attachedTo: ElementRef<HTMLElement> | HTMLElement, animationsToRun?: number): PopOverRef;
  showAnimation<T>(
    toOpen: TemplateRef<any> | ComponentType<T>,
    attachedTo: ElementRef<HTMLElement> | HTMLElement,
    animationsToRun = 1,
  ): PopOverRef | ComponentPopOverRef<T> {
    const ref = this.open(toOpen, attachedTo, {
      hasBackdrop: false,
      disableAnimation: true,
      position: [
        { originX: 'center', originY: 'top', overlayX: 'center', overlayY: 'bottom' },
        { originX: 'center', originY: 'bottom', overlayX: 'center', overlayY: 'top' },
        { originX: 'end', originY: 'center', overlayX: 'start', overlayY: 'center' },
        { originX: 'start', originY: 'center', overlayX: 'end', overlayY: 'center' },
      ],
      transformOriginOn: '.dt-transform-origin',
    });

    ref.overlayRef.hostElement.addEventListener('animationend', () => {
      if (--animationsToRun === 0) {
        ref.close();
      }
    });

    return ref;
  }

  setPosition(left: number, top: number, overlayRef: OverlayRef) {
    const newPosition = this.overlay.position().global().left(`${left}px`).top(`${top}px`);
    overlayRef.updatePositionStrategy(newPosition);
    overlayRef.updatePosition();
  }

  getScrollStrategy(strategy: ScrollStrategy) {
    switch (strategy) {
      case 'noop':
        return this.overlay.scrollStrategies.noop();
      case 'close':
        return this.overlay.scrollStrategies.close();
      case 'reposition':
        return this.overlay.scrollStrategies.reposition();
      default:
        assertUnreachable(strategy);
    }
  }
}
