import { Component, EventEmitter, Input, Output } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { ButtonColor } from '../../tools';

@Component({
  template: `
    <div class="prompt">{{ prompt }}</div>
    <div>
      <button mat-button [color]="confirmColor" data-cy="confirm-popover-confirm-button" (click)="confirmClick.emit()">{{
        confirmText
      }}</button>
      <button mat-button (click)="discardClick.emit()">{{ discardText }}</button>
    </div>
  `,
  styles: [
    `
      @use '@angular/material' as mat;

      :host {
        @include mat.elevation(8);

        display: block;
        border-radius: 5px;
        padding: 8px 8px 0;
        margin: 8px;
        width: 275px;

        background-color: #404040;
        color: white;

        button.mat-mdc-button.mat-unthemed {
          --mdc-text-button-label-text-color: white;
          --mat-mdc-button-ripple-color: rgba(255, 255, 255, 0.1);
        }

        text-align: center;
      }

      .prompt {
        padding: 0 4px;
      }
    `,
  ],
  standalone: true,
  imports: [MatButtonModule],
})
export class ConfirmPopOverComponent {
  @Input() prompt: string;
  @Input() confirmColor: ButtonColor = 'warn';
  @Input() confirmText = $localize`Löschen`;
  @Input() discardText = $localize`Abbrechen`;

  @Output() confirmClick = new EventEmitter<void>();
  @Output() discardClick = new EventEmitter<void>();
}
