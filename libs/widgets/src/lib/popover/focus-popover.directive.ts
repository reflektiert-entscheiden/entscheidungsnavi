import { Directive, HostListener } from '@angular/core';
import { HoverPopOverDirective } from './hover-popover.directive';

@Directive({
  selector: '[dtFocusPopover]',
  standalone: true,
  hostDirectives: [
    {
      directive: HoverPopOverDirective,
      inputs: [
        'dtHoverPopover:dtFocusPopover',
        'dtHoverPopoverContext:dtFocusPopoverContext',
        'dtHoverPopoverPosition:dtFocusPopoverPosition',
        'dtHoverPopoverScrollStrategy:dtFocusPopoverScrollStrategy',
        'dtHoverPopoverDisableAnimation:dtFocusPopoverDisableAnimation',
        'dtHoverPopoverClickthrough:dtFocusPopoverClickthrough',
      ],
    },
  ],
})
export class FocusPopoverDirective {
  constructor(private hoverPopover: HoverPopOverDirective) {
    hoverPopover.dtHoverPopoverDisabled = true;
  }

  @HostListener('focusin', ['$event'])
  focusIn(event: FocusEvent) {
    if (!(event.target instanceof HTMLInputElement)) {
      return;
    }

    this.hoverPopover.open(0, true);
  }

  @HostListener('focusout')
  focusOut() {
    this.hoverPopover.close();
  }
}
