import { trigger, state, animate, style, transition } from '@angular/animations';
import { Component, ContentChild, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges, TemplateRef } from '@angular/core';

@Component({
  selector: 'dt-object-box',
  templateUrl: './object-box.component.html',
  styleUrls: ['./object-box.component.scss'],
  animations: [
    trigger('boxExpanded', [
      state('true', style({ height: '*' })),
      state('false', style({ height: 0 })),
      transition('true <=> false', [animate('300ms ease-in-out')]),
    ]),
    trigger('sidePanelExpanded', [
      state('true', style({ width: '*' })),
      state('false', style({ width: 24 })),
      transition('true <=> false', [animate('300ms ease-in-out')]),
    ]),
    trigger('expandCollapseIndicator', [
      state('false', style({ transform: 'rotate(0)' })),
      state('true', style({ transform: 'rotate(-180deg)' })),
      transition('true <=> false', [animate('300ms ease-in-out')]),
    ]),
  ],
})
export class ObjectBoxComponent implements OnInit, OnChanges {
  @ContentChild('header', { static: true }) headerTemplate: TemplateRef<unknown>;
  @ContentChild('body', { static: true }) bodyTemplate: TemplateRef<unknown>;
  @ContentChild('sidePanel', { static: true }) sidePanelTemplate: TemplateRef<unknown>;

  @Input() darkHeader = false; // Switches the foreground in the header to dark mode

  @Input() expanded = true;
  @Output() expandedChange = new EventEmitter<boolean>();

  // Sidepanel state defaults to this value on big screens. On small screens, it always defaults to collapsed.
  @Input() sidePanelExpandedByDefault = true;

  @Input() allowCollapsing = true;

  sidePanelExpanded: boolean;
  private smallScreen = false;

  ngOnInit() {
    this.resetSidePanel();
  }

  ngOnChanges(changes: SimpleChanges) {
    if ('sidePanelExpandedByDefault' in changes) {
      this.resetSidePanel();
    }
  }

  toggleExpansion() {
    this.expanded = !this.expanded;
    this.expandedChange.emit(this.expanded);
  }

  toggleSidePanel() {
    this.sidePanelExpanded = !this.sidePanelExpanded;
  }

  onSmallScreenChange(smallScreen: boolean) {
    this.smallScreen = smallScreen;
    this.resetSidePanel();
  }

  private resetSidePanel() {
    this.sidePanelExpanded = !this.smallScreen && this.sidePanelExpandedByDefault;
  }
}
