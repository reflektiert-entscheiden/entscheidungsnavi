import {
  booleanAttribute,
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  HostBinding,
  Input,
  OnChanges,
  Output,
  SimpleChanges,
  TrackByFunction,
} from '@angular/core';
import { MatChipsModule } from '@angular/material/chips';
import { QuickstartTagDto } from '@entscheidungsnavi/api-client';
import { HoverPopOverDirective } from '../popover';
import { LocalizedStringPipe } from '../pipes';

@Component({
  selector: 'dt-tag-list',
  templateUrl: './tag-list.component.html',
  styleUrls: ['./tag-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  standalone: true,
  imports: [MatChipsModule, HoverPopOverDirective, LocalizedStringPipe],
})
export class TagListComponent implements OnChanges {
  @Input({ required: true }) tagIds: string[];
  @Input({ required: true }) tagsById: { [id: string]: QuickstartTagDto };
  @Input() selectedTagIds: string[] = [];
  @HostBinding('class.single-row') @Input({ transform: booleanAttribute }) singleRow = false;

  @Output() tagClick = new EventEmitter<QuickstartTagDto>();

  protected allTags: QuickstartTagDto[];

  ngOnChanges(changes: SimpleChanges) {
    if (('tagIds' in changes || 'tagsById' in changes) && this.tagIds && this.tagsById) {
      this.allTags = this.tagIds.map(tagId => this.tagsById[tagId]).sort((a, b) => (b.weight ?? -Infinity) - (a.weight ?? -Infinity));
    }
  }

  anyTagSelected(afterTagIndex = -1) {
    return this.allTags.slice(afterTagIndex + 1).some(tag => this.selectedTagIds.includes(tag.id));
  }

  readonly trackByTagId: TrackByFunction<QuickstartTagDto> = (_, tag) => {
    return tag.id;
  };
}
