import {
  Component,
  ContentChild,
  ElementRef,
  EventEmitter,
  HostListener,
  Input,
  NgZone,
  Output,
  TemplateRef,
  ViewChild,
} from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { clamp } from 'lodash';

@Component({
  selector: 'dt-drag-drop-list',
  templateUrl: 'drag-drop-list.component.html',
  styleUrls: ['drag-drop-list.component.scss'],
})
export class DragDropListComponent {
  @Input() listElements: any[];
  @Input() allowGrouping = false;
  @Input() readonly = false;
  @Input() showPlacement = false;

  @ContentChild(TemplateRef) elementTemplate: TemplateRef<any>;

  @Output() move: EventEmitter<any> = new EventEmitter<any>();
  @Output() group: EventEmitter<any> = new EventEmitter<any>();

  @Output() moveChild: EventEmitter<any> = new EventEmitter<any>();
  @Output() extractChild: EventEmitter<any> = new EventEmitter<any>();

  // Drag and Drop Logic
  dragging: boolean; // Is the user currently dragging an element?
  currentlyDraggedIndex = -1; // Index of the element that is currently being dragged
  currentRecipientIndex = -1; // Index of the element that is currently selected to receive the dragged element.

  draggingGroup: boolean; // Is the user currently dragging an element that contains child elements?

  draggingChild: boolean; // Is the user currently dragging an element that is a child of another one?
  cdChildIndex: number; // If so this is the index of the child inside its parent

  activeDelimiter = -1; // Identifier of delimiter that is currently active (selected for potential drop)

  // Drag and Drop Visuals
  originElement: HTMLElement; // The html element of the element being dragged
  dragElement: HTMLElement; // The 'fake' element that is visually dragged by the user
  relativeX: number; // The relative position the user is dragging the element
  relativeY: number;

  // Variables used to automatically scroll up / down while dragging at the edge of the screen
  autoMoveInterval: ReturnType<typeof setInterval>;
  autoMoveY = 0;

  @ViewChild('error', { static: true })
  groupErrorRef: TemplateRef<any>;

  constructor(
    private zone: NgZone,
    private el: ElementRef,
    private snack: MatSnackBar,
  ) {
    this.dragging = false;
  }

  @HostListener('window:mousemove.outside-zone', ['$event'])
  mouseMove(event: MouseEvent) {
    if (this.dragging) {
      this.dragElement.style.left = event.clientX - this.relativeX + 'px';
      this.dragElement.style.top =
        clamp(
          event.clientY - this.relativeY,
          -this.dragElement.getBoundingClientRect().height / 2,
          document.documentElement.scrollHeight - this.dragElement.getBoundingClientRect().height / 2,
        ) + 'px';

      const move = (y: number) => {
        document.querySelector('mat-sidenav-content > .content-container > .page-container')?.scrollBy(0, y);
      };

      const margin = 20;

      this.autoMoveY = 0;

      if (event.clientY < margin) {
        this.autoMoveY = -8;
      } else if (window.innerHeight - event.clientY < margin) {
        this.autoMoveY = 8;
      }

      if (this.autoMoveInterval === undefined && this.autoMoveY !== 0) {
        this.autoMoveInterval = setInterval(() => {
          if (this.autoMoveY === 0 || !this.dragging) {
            clearInterval(this.autoMoveInterval);
            this.autoMoveInterval = undefined;
          }
          move(this.autoMoveY);
        }, 15);
      }
    }
  }

  @HostListener('window:mouseup.outside-zone', ['$event'])
  mouseUp(event: MouseEvent) {
    if (this.dragging) {
      this.dragEnd(event);
    }
  }

  @HostListener('window:mouseleave.outside-zone', ['$event'])
  mouseLeave(event: MouseEvent) {
    if (this.dragging && event.currentTarget === event.target) {
      this.dragEnd(event);
    }
  }

  dragStart(event: DragEvent, elementIndex: number) {
    const draggableElement = event.target as HTMLElement;
    const isDragHandle = draggableElement.hasAttribute?.('dtDragHandle');

    // Ensure the element with which the user dragged is actually a drag handle
    if (!isDragHandle) {
      return;
    }

    event.stopPropagation();
    event.preventDefault(); // We dont want HTML 5 Drag and Drop

    if (this.readonly || draggableElement.getAttribute('draggable') === 'false') {
      return;
    }

    this.dragging = true;
    this.currentlyDraggedIndex = elementIndex;
    this.originElement = event.currentTarget as HTMLElement;

    const closestOrigin = draggableElement.closest('[dtDragOrigin]') as HTMLElement;
    if (closestOrigin) {
      this.originElement = closestOrigin;
    }

    this.draggingGroup = draggableElement.hasAttribute('dtDragGroup');
    this.draggingChild = draggableElement.hasAttribute('dtDragChildIndex');

    if (this.draggingChild) {
      this.cdChildIndex = parseInt(draggableElement.getAttribute('dtDragChildIndex'), 10);
      this.originElement.style.opacity = '0.2';
    }

    // Preview
    const elementRect = this.originElement.getBoundingClientRect();

    this.relativeX = event.clientX - elementRect.left;
    this.relativeY = event.clientY - elementRect.top;

    this.relativeX += 10; // Equal out the Padding
    this.relativeY += 10; // Equal out the Padding

    const elementClone = this.originElement.cloneNode(true) as HTMLElement;
    elementClone.style.width = this.originElement.clientWidth + 'px';
    elementClone.style.minWidth = this.originElement.clientWidth + 'px';
    elementClone.style.maxWidth = this.originElement.clientWidth + 'px';
    elementClone.style.transition = 'box-shadow 0.2s ease 0s';

    requestAnimationFrame(() => {
      elementClone.style.boxShadow = '0px 0px 10px 0px darkgray';
    });

    // Wrap element in a bigger div to prevent borders from 'shaving' off during dragging
    this.dragElement = document.createElement('div');

    this.dragElement.style.position = 'fixed';
    this.dragElement.style.left = event.clientX - this.relativeX + 'px';
    this.dragElement.style.top = event.clientY - this.relativeY + 'px';
    this.dragElement.style.pointerEvents = 'none';
    this.dragElement.style.zIndex = '1000';
    this.dragElement.style.padding = '10px';

    this.dragElement.appendChild(elementClone);

    this.el.nativeElement.appendChild(this.dragElement);

    document.body.classList.add('dt-drag-move-cursor');
  }

  dragEnterElement(_event: MouseEvent, elementIndex: number) {
    if (this.dragging && this.currentlyDraggedIndex !== elementIndex && this.allowGrouping) {
      this.currentRecipientIndex = elementIndex;
      if (!this.draggingGroup) {
        document.body.classList.add('dt-drag-group-cursor');
      } else {
        document.body.classList.add('dt-drag-group-invalid-cursor');
      }
    }
  }

  dragLeaveElement(_event: MouseEvent, elementIndex: number) {
    if (this.dragging && this.currentlyDraggedIndex !== elementIndex && this.allowGrouping) {
      this.currentRecipientIndex = -1;
      if (!this.draggingGroup) {
        document.body.classList.remove('dt-drag-group-cursor');
      } else {
        document.body.classList.remove('dt-drag-group-invalid-cursor');
      }
    }
  }

  dragEnd(_event: MouseEvent) {
    if (this.draggingChild) {
      this.originElement.style.opacity = '1';
    }
    this.el.nativeElement.removeChild(this.dragElement);

    document.body.classList.remove('dt-drag-move-cursor');
    document.body.classList.remove('dt-drag-group-cursor');
    document.body.classList.remove('dt-drag-group-invalid-cursor');

    this.zone.run(() => {
      this.dragging = false;
      this.dragElement = undefined;
      this.originElement = undefined;
      this.currentRecipientIndex = -1;
    });
  }

  dropElement(_event: MouseEvent, elementIndex: number) {
    // elementIndex === this.currentRecipientIndex should hold.
    if (this.dragging && this.currentlyDraggedIndex !== elementIndex && this.allowGrouping) {
      if (!this.draggingGroup) {
        if (this.draggingChild) {
          this.moveChild.emit([this.currentlyDraggedIndex, this.cdChildIndex, elementIndex]);
        } else {
          this.group.emit([this.currentlyDraggedIndex, elementIndex]);
        }
      } else {
        this.snack.openFromTemplate(this.groupErrorRef, { duration: 4000 });
      }
    }
  }

  dragEnterDelimiter(event: MouseEvent, i: number) {
    if (this.dragging && (this.draggingChild || (this.currentlyDraggedIndex !== i && this.currentlyDraggedIndex !== i - 1))) {
      event.preventDefault();

      if (!this.draggingChild) {
        this.move.emit([this.currentlyDraggedIndex, this.currentlyDraggedIndex < i ? i - 1 : i]);
        this.currentlyDraggedIndex = this.currentlyDraggedIndex < i ? i - 1 : i;
      }
    }
  }

  dropDelimiter(_event: MouseEvent, i: number) {
    if (this.draggingChild) {
      this.extractChild.emit([this.currentlyDraggedIndex, this.cdChildIndex, i]);
    }
  }
}
