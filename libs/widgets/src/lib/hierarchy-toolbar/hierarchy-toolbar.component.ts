import { ChangeDetectionStrategy, Component, computed, ContentChild, HostListener, TemplateRef } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatDividerModule } from '@angular/material/divider';
import { LocationSequence } from '@entscheidungsnavi/tools';
import { MatTooltipModule } from '@angular/material/tooltip';
import { HierarchyMovementDirective } from '../hierarchy/movement.directive';
import { HierarchyNodeStateDirective } from '../hierarchy/node-state.directive';
import { HierarchyInterfaceDirective } from '../hierarchy/interface.directive';
import { HierarchyComponent } from '../hierarchy/hierarchy.component';

@Component({
  selector: 'dt-hierarchy-toolbar',
  template: `
    <ng-content select="[prepend]"></ng-content>
    <button mat-icon-button (click)="hierarchyMovement.resetView()" matTooltip="Ansicht zurücksetzen" i18n-matTooltip
      ><mat-icon>center_focus_weak</mat-icon></button
    >
    <button mat-icon-button (click)="hierarchy.exportImage()" matTooltip="Als Bild exportieren" i18n-matTooltip
      ><mat-icon>photo</mat-icon></button
    >
    <ng-content></ng-content>

    @if (focused().length > 0) {
      <div class="context-menu">
        <mat-divider vertical></mat-divider>
        <span data-cy="focused-elements-indicator" i18n
          >{{ focused().length }} {focused().length, plural, =1 {Element} other {Elemente}} ausgewählt</span
        >
        <mat-divider vertical></mat-divider>
        <button
          [disabled]="focused().length > 1"
          mat-icon-button
          (click)="hierarchyNodeState.collapseOneLevel(focused()[0])"
          matTooltip="Schrittweise einklappen"
          i18n-matTooltip
          ><mat-icon>collapse_content</mat-icon></button
        >
        <button
          [disabled]="focused().length > 1"
          mat-icon-button
          (click)="hierarchyNodeState.expandOneLevel(focused()[0])"
          matTooltip="Schrittweise ausklappen"
          i18n-matTooltip
          ><mat-icon>expand_content</mat-icon></button
        >
        @if (toolbarTemplate) {
          <mat-divider vertical></mat-divider>
          <ng-container *ngTemplateOutlet="toolbarTemplate; context: { $implicit: focused() }"></ng-container>
        }
        @if (!hierarchyInterface.readonly) {
          <mat-divider vertical></mat-divider>
          <button
            [disabled]="!canGroup()"
            mat-icon-button
            (click)="hierarchyInterface.group(focused())"
            matTooltip="Elemente gruppieren"
            i18n-matTooltip
            ><mat-icon>group_work</mat-icon></button
          >
          <button
            mat-icon-button
            (click)="hierarchyInterface.deleteMultiple(focused())"
            data-cy="toolbar-delete-button"
            matTooltip="Elemente löschen"
            i18n-matTooltip
            ><mat-icon>delete</mat-icon></button
          >
        }
      </div>
    }
  `,
  styles: [
    //language=SCSS
    `
      @use '@angular/material' as mat;

      :host {
        position: absolute;
        top: 0;
        left: 0;
        max-width: 100%;

        display: flex;
        overflow-x: auto;
        overflow-y: hidden;

        background-color: white;
        border-bottom-right-radius: 10px;
        @include mat.elevation(8);

        white-space: nowrap;
      }

      .context-menu {
        display: flex;
        background-color: #fcf4d4;

        span {
          align-self: center;
          padding: 0 8px;
        }
      }
    `,
  ],
  standalone: true,
  changeDetection: ChangeDetectionStrategy.OnPush,
  imports: [CommonModule, MatButtonModule, MatIconModule, MatDividerModule, MatTooltipModule],
})
export class HierarchyToolbarComponent {
  readonly focused = this.hierarchyNodeState.focused.asReadonly();
  readonly canGroup = computed(() => LocationSequence.canGroup(this.focused()));

  @ContentChild('nodeToolbar') toolbarTemplate: TemplateRef<{ $implicit: LocationSequence[] }>;

  constructor(
    protected hierarchy: HierarchyComponent<unknown>,
    protected hierarchyMovement: HierarchyMovementDirective<unknown>,
    protected hierarchyNodeState: HierarchyNodeStateDirective<unknown>,
    protected hierarchyInterface: HierarchyInterfaceDirective<unknown>,
  ) {}

  @HostListener('mousedown', ['$event'])
  onClick(event: MouseEvent) {
    // Prevent clicks within the toolbar from deselecting elements
    event.stopPropagation();
  }
}
