import { ChangeDetectionStrategy, Component, ElementRef, EventEmitter, Input, Output, ViewChild } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LocationSequence, Tree } from '@entscheidungsnavi/tools';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { ObjectiveElement } from '@entscheidungsnavi/decision-data';
import { Clipboard } from '@angular/cdk/clipboard';
import { MatTooltipModule } from '@angular/material/tooltip';
import { HierarchyComponent } from '../hierarchy/hierarchy.component';
import { NoteBtnComponent, NoteBtnPresetPipe } from '../note-btn';
import { RichTextEmptyPipe } from '../pipes';
import { HierarchyToolbarComponent } from '../hierarchy-toolbar/hierarchy-toolbar.component';
import { HierarchyNodeStateDirective } from '../hierarchy/node-state.directive';
import { PopOverService } from '../popover';
import { KeyBindHandlerDirective } from '../directives/keybind-handler.directive';
import { PlatformDetectService } from '../services/platform-detect.service';
import { SimpleHierarchyNode } from './simple-hierarchy-node';
import { SimpleHierarchyDragData } from './simple-hierarchy-drag';

/**
 * This component shows a simple, read-only hierarchy whose style is defined by its elements.
 */
@Component({
  selector: 'dt-simple-hierarchy',
  templateUrl: './simple-hierarchy.component.html',
  styleUrls: ['./simple-hierarchy.component.scss'],
  standalone: true,
  changeDetection: ChangeDetectionStrategy.OnPush,
  imports: [
    CommonModule,
    HierarchyComponent,
    NoteBtnComponent,
    NoteBtnPresetPipe,
    RichTextEmptyPipe,
    HierarchyToolbarComponent,
    MatButtonModule,
    MatIconModule,
    MatTooltipModule,
  ],
  hostDirectives: [KeyBindHandlerDirective],
})
export class SimpleHierarchyComponent<T extends SimpleHierarchyNode> {
  @Input({ required: true }) tree: Tree<T>;
  @Output() doubleClickNode = new EventEmitter<{ element: T; location: LocationSequence; htmlElement: HTMLElement }>();

  @ViewChild(HierarchyComponent, { read: ElementRef }) hierarchy: ElementRef<HTMLElement>;
  @ViewChild(HierarchyNodeStateDirective) nodeState: HierarchyNodeStateDirective<ObjectiveElement>;

  constructor(
    private clipboard: Clipboard,
    private popOverService: PopOverService,
    platformDetectService: PlatformDetectService,
    keyBindHandler: KeyBindHandlerDirective,
  ) {
    if (platformDetectService.macOS) {
      keyBindHandler.register({
        key: 'c',
        metaKey: true,
        callback: () => this.copyToClipboard(),
      });
    } else {
      keyBindHandler.register({
        key: 'c',
        ctrlKey: true,
        callback: () => this.copyToClipboard(),
      });
    }
  }

  nodeDragStart(locations: LocationSequence[], event: DragEvent) {
    SimpleHierarchyDragData.writeToEvent(
      event,
      locations.map(location => ({ location, tree: this.tree.getNode(location) })),
    );

    const trees = locations.map(loc => this.tree.getNode(loc));
    const plainTextData = trees.map(node => node.value.name).join('\n');
    event.dataTransfer.setData('text/plain', plainTextData);
  }

  copyToClipboard() {
    const focused = this.nodeState.focused();
    if (focused.length === 0) return;

    const textData = focused.map(location => this.tree.getNode(location).value.name).join('\n');

    if (this.clipboard.copy(textData)) {
      this.popOverService.whistle(this.hierarchy, $localize`Elemente kopiert!`, 'done');
    } else {
      this.popOverService.whistle(this.hierarchy, $localize`Kopieren fehlgeschlagen. Möglicherweise ist der Text zu lang.`, 'error');
    }
  }
}
