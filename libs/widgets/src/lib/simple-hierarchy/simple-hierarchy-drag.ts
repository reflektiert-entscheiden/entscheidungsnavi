import { ITree, LocationSequence, Tree } from '@entscheidungsnavi/tools';
import { SimpleHierarchyNode } from './simple-hierarchy-node';

type DragData<T> = Array<{ location: ReadonlyArray<number>; tree: ITree<T> }>;

export class SimpleHierarchyDragData<T extends SimpleHierarchyNode> {
  private static dragDataType = 'application/decision-tool-simple-hierarchy';

  static writeToEvent<T extends SimpleHierarchyNode>(
    event: DragEvent,
    elements: ReadonlyArray<{
      readonly location: LocationSequence;
      readonly tree: Tree<T>;
    }>,
  ) {
    event.dataTransfer.setData(
      SimpleHierarchyDragData.dragDataType,
      JSON.stringify(elements.map(element => ({ location: element.location.value, tree: element.tree })) satisfies DragData<T>),
    );
  }

  static fromEvent<T extends SimpleHierarchyNode>(event: DragEvent): SimpleHierarchyDragData<T> | null {
    const data = event.dataTransfer.getData(SimpleHierarchyDragData.dragDataType);
    if (!data) return null;

    try {
      const parsedData: DragData<T> = JSON.parse(data);
      if (!Array.isArray(parsedData)) return null;

      return new SimpleHierarchyDragData<T>(
        parsedData.map(element => ({ location: new LocationSequence(element.location), tree: Tree.from(element.tree) })),
      );
    } catch {
      return null;
    }
  }

  static isInEvent(event: DragEvent): boolean {
    return event.dataTransfer.types.includes(SimpleHierarchyDragData.dragDataType);
  }

  constructor(
    public readonly elements: ReadonlyArray<{
      readonly location: LocationSequence;
      readonly tree: Tree<T>;
    }>,
  ) {}
}
