import { NgStyle } from '@angular/common';

export interface SimpleHierarchyNode {
  name: string;
  comment?: string;
  scaleComment?: string;
  style?: typeof NgStyle.prototype.ngStyle;
}
