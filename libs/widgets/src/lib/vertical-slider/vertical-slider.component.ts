import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'dt-vertical-slider',
  templateUrl: './vertical-slider.component.html',
  styleUrls: ['./vertical-slider.component.scss'],
})
export class VerticalSliderComponent {
  @Input() positions: string[];
  @Input() disabledPositions: number[] = [];
  // Tooltips that are shown at a position when it is disabled.
  @Input() tooltipsForDisabled: string[] = [];

  @Input()
  selectedPosition = 0;

  @Output()
  selectedPositionChange = new EventEmitter();

  get gridRows() {
    return `repeat(${this.positions.length}, 1fr)`;
  }

  changeSelectedPosition(newSelectedPosition: number) {
    if (this.selectedPosition === newSelectedPosition) {
      return;
    }

    if (this.disabledPositions.includes(newSelectedPosition)) {
      return;
    }

    this.selectedPosition = newSelectedPosition;
    this.selectedPositionChange.emit(newSelectedPosition);
  }
}
