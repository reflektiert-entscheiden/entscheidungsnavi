import { NgModule } from '@angular/core';
import { DynamicChangeDirective } from './dynamic-change.directive';

@NgModule({
  declarations: [DynamicChangeDirective],
  exports: [DynamicChangeDirective],
})
export class DynamicChangeDirectiveModule {}
