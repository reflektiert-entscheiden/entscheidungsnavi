import { Directive, EventEmitter, HostBinding, HostListener, Input, OnInit, Output } from '@angular/core';
import { OnDestroyObservable } from '@entscheidungsnavi/tools';
import { exhaustMap, merge, Observable, of, repeat, Subject, takeUntil, timer } from 'rxjs';

@Directive({
  selector: '[dtDynamicChange]',
})
export class DynamicChangeDirective implements OnInit {
  @Output()
  dtDynamicChange = new EventEmitter<void>();

  @OnDestroyObservable()
  private onDestroy$: Observable<void>;

  private pointerDown$ = new Subject<void>();
  private pointerUp$ = new Subject<void>();
  private pointerLeave$ = new Subject<void>();
  private hostDisabled$ = new Subject<void>();

  private _disabled = false;

  @Input()
  @HostBinding('disabled')
  get disabled() {
    return this._disabled;
  }

  set disabled(newDisabled: boolean) {
    this._disabled = newDisabled;
    if (newDisabled) this.hostDisabled$.next();
  }

  @HostListener('pointerdown')
  onPointerDown() {
    this.pointerDown$.next();
  }

  @HostListener('pointerup')
  onPointerUp() {
    this.pointerUp$.next();
  }

  @HostListener('pointerleave')
  onPointerLeave() {
    this.pointerLeave$.next();
  }

  ngOnInit() {
    this.pointerDown$
      .pipe(
        exhaustMap(() => {
          return of(undefined).pipe(
            repeat({
              delay: count => {
                let delay = Math.max(50, 200 - (count - 1) * 5);
                if (count === 1) {
                  delay += 500;
                }
                return timer(delay);
              },
            }),
            takeUntil(merge(this.pointerUp$, this.pointerLeave$, this.hostDisabled$, this.onDestroy$)),
          );
        }),
      )
      .subscribe(() => {
        this.dtDynamicChange.emit();
      });
  }
}
