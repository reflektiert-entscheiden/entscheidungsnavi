import { AfterViewInit, ChangeDetectorRef, Directive, ElementRef, EventEmitter, Input, NgZone, OnDestroy, Output } from '@angular/core';
import { minBy } from 'lodash';

/**
 * Dynamically adds css-classes based on the width of the component. Only _one_ WidthTriggerDirective
 * per DOM Element is supported.
 *
 * @example
 * `<div [dtWidthTrigger]="{ 'small-breakpoint': 500, 'large-breakpoint': 1500 }"></div>`
 *
 * This will add the css class 'small-breakpoint' to the div when its width is less or equal 500px. Similarly
 * for class 'large-breakpoint'.
 *
 * (dtWidthTriggerState) will emit a dictionary with the breakpoint names as keys and boolean values indicating
 * whether the breakpoint is met or not. If might for example emit
 * `{ 'small-breakpoint': false, 'large-breakpoint': true }`
 */
@Directive({
  selector: '[dtWidthTrigger]',
  exportAs: 'dtWidthTrigger',
  standalone: true,
})
export class WidthTriggerDirective implements AfterViewInit, OnDestroy {
  // Single global static ResizeObserver
  private static observer = new ResizeObserver(WidthTriggerDirective.observerCallback);
  private static callbacks = new Map<Element, (newWidth: number) => void>();

  static isEntryActive(currentWidth: number, breakpoint: number): boolean {
    return currentWidth <= breakpoint;
  }

  static cssClassOfSmallestActiveBreakpoint(currentWidth: number, dtWidthTriggerEntries: [string, number][]): string | undefined {
    return minBy(
      dtWidthTriggerEntries.filter(([_, breakpoint]) => this.isEntryActive(currentWidth, breakpoint)),
      ([_, breakpoint]) => breakpoint,
    )?.[0];
  }

  private static observerCallback(entries: ResizeObserverEntry[]) {
    for (const entry of entries) {
      const callback = WidthTriggerDirective.callbacks.get(entry.target);
      callback(entry.contentRect.width);
    }
  }

  @Input() dtWidthTrigger: { [key: string]: number };

  @Input() onlySmallestBreakpoint = false;

  @Output() dtWidthTriggerStateChange = new EventEmitter<{ [key: string]: boolean }>();

  public state: { [key: string]: boolean } = {};

  private breakpointStates = new Set<string>();

  constructor(
    private elementRef: ElementRef<HTMLElement>,
    private cdRef: ChangeDetectorRef,
    private zone: NgZone,
  ) {}

  ngAfterViewInit() {
    WidthTriggerDirective.callbacks.set(this.elementRef.nativeElement, newWidth => this.checkWidth(newWidth));
    WidthTriggerDirective.observer.observe(this.elementRef.nativeElement);
  }

  ngOnDestroy() {
    WidthTriggerDirective.observer.unobserve(this.elementRef.nativeElement);
    WidthTriggerDirective.callbacks.delete(this.elementRef.nativeElement);
  }

  private checkWidth(newWidth: number) {
    let hasChange = false;
    const completeState: { [key: string]: boolean } = {};

    const dtWidthTriggerEntries = Object.entries(this.dtWidthTrigger);
    const cssClassOfSmallestActiveBreakpoint = WidthTriggerDirective.cssClassOfSmallestActiveBreakpoint(newWidth, dtWidthTriggerEntries);

    dtWidthTriggerEntries.forEach(([cssClass, breakpoint]) => {
      const currentState = this.onlySmallestBreakpoint
        ? cssClass === cssClassOfSmallestActiveBreakpoint
        : WidthTriggerDirective.isEntryActive(newWidth, breakpoint);

      completeState[cssClass] = currentState;

      if (currentState !== this.breakpointStates.has(cssClass)) {
        if (currentState) {
          this.elementRef.nativeElement.classList.add(cssClass);
          this.breakpointStates.add(cssClass);
        } else {
          this.elementRef.nativeElement.classList.remove(cssClass);
          this.breakpointStates.delete(cssClass);
        }
        hasChange = true;
      }
    });

    if (hasChange) {
      this.zone.run(() => {
        this.state = completeState;
        this.dtWidthTriggerStateChange.emit(this.state);
        this.cdRef.detectChanges();
      });
    }
  }
}
