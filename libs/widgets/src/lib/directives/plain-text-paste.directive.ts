import { Directive, ElementRef, HostListener } from '@angular/core';

/**
 * This directive converts pasted content into plain text.
 *
 * @example
 * `<div contenteditable dtPlainTextPaste></div>`
 */
@Directive({ standalone: true, selector: '[dtPlainTextPaste]' })
export class PlainTextPasteDirective {
  constructor(private elementRef: ElementRef<HTMLElement>) {}

  @HostListener('paste', ['$event'])
  pasteEvent(event: ClipboardEvent) {
    event.preventDefault();

    const text = event.clipboardData.getData('text/plain');
    this.elementRef.nativeElement.textContent += text;
  }
}
