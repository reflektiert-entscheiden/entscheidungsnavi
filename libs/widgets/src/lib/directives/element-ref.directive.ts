import { Directive, ElementRef } from '@angular/core';

/**
 * Export the ElementRef of the selected element for use with template references.
 *
 * @example
 * `<button mat-button #button="dtElementRef" dtElementRef></button>`
 */
@Directive({
  selector: '[dtElementRef]',
  exportAs: 'dtElementRef',
  standalone: true,
})
export class ElementRefDirective<T> extends ElementRef<T> {
  constructor(elementRef: ElementRef<T>) {
    super(elementRef.nativeElement);
  }
}
