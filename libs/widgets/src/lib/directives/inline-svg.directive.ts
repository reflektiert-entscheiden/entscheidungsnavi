import { HttpClient } from '@angular/common/http';
import { Directive, ElementRef, Input, OnInit, Renderer2 } from '@angular/core';

@Directive({
  selector: 'svg[dtInlineSVG]',
})
export class InlineSVGDirective implements OnInit {
  @Input()
  dtInlineSVG: string;

  constructor(
    private renderer: Renderer2,
    private elementRef: ElementRef<HTMLElement>,
    private http: HttpClient,
  ) {}

  ngOnInit() {
    this.http.get(this.dtInlineSVG, { responseType: 'text' }).subscribe(resp => {
      this.renderer.setProperty(this.elementRef.nativeElement, 'innerHTML', resp);
    });
  }
}
