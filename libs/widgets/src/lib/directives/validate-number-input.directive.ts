import { AfterViewInit, Directive, ElementRef, EventEmitter, Input, OnDestroy, Output } from '@angular/core';

@Directive({
  selector: '[dtValidateNumberInput]',
})
export class ValidateNumberInputDirective implements AfterViewInit, OnDestroy {
  @Input() ngModel: number;
  @Input() min: number;
  @Input() max: number;
  @Input() integerOnly = false;

  // If set, will be used as the default value if the input is empty
  @Input() defaultValue: number | null = null;

  @Output() validatedChange = new EventEmitter<number>();

  inputListener: (event: InputEvent) => void;

  constructor(private host: ElementRef<HTMLInputElement>) {
    this.input = this.input.bind(this);
  }

  ngAfterViewInit() {
    this.host.nativeElement.addEventListener('input', this.input, { capture: true });
  }

  ngOnDestroy() {
    this.host.nativeElement.removeEventListener('input', this.inputListener);
  }

  input(event: InputEvent) {
    // Ignore our re-fired event
    if (!event.isTrusted) {
      return;
    }

    let changed = false;

    let newValue = this.host.nativeElement.valueAsNumber;

    if (isNaN(newValue)) {
      if (this.defaultValue != null) {
        this.host.nativeElement.valueAsNumber = this.defaultValue;
      }
      return;
    }

    if (this.integerOnly) {
      if (this.host.nativeElement.valueAsNumber !== Math.floor(newValue)) {
        this.host.nativeElement.valueAsNumber = newValue = Math.floor(newValue);
        changed = true;
      }
    }

    if (newValue > this.max) {
      this.host.nativeElement.valueAsNumber = this.max;
      changed = true;
    } else if (newValue < this.min) {
      this.host.nativeElement.valueAsNumber = this.min;
      changed = true;
    }

    if (changed) {
      // Firefox doesn't call capture Event Listeners before
      // others in AT_EVENT Phase so we have to refire a fake input event to redo other form validation stuff.
      const dummyInputEvent = new InputEvent('input');
      this.host.nativeElement.dispatchEvent(dummyInputEvent);
    }
  }
}
