import { Directive, ElementRef, HostListener, NgZone, OnInit, Renderer2 } from '@angular/core';

type KeyPressDirection = null | 'down' | 'up';

class KeyBind {
  constructor(
    public key: string | null,
    public ctrlKey: boolean | null,
    public shiftKey: boolean | null,
    public metaKey: boolean | null,
    public callback: (event?: KeyboardEvent) => void,
    public callbackOnBlur: boolean,
    public direction: KeyPressDirection,
    public onRepeat: boolean,
    public conditions: Array<(event: KeyboardEvent) => boolean>,
  ) {}
}

/**
 * Utility class to simplify key bindings, example in HierarchyComponent
 */
@Directive({ standalone: true })
export class KeyBindHandlerDirective implements OnInit {
  private bindings: KeyBind[] = [];

  private lastKeyboardEvent: KeyboardEvent | null = null;

  constructor(
    private zone: NgZone,
    private elementRef: ElementRef<HTMLElement>,
    private renderer2: Renderer2,
  ) {}

  ngOnInit() {
    if (this.elementRef.nativeElement.tabIndex === -1) {
      // The element needs to be selectable to capture key events
      this.elementRef.nativeElement.tabIndex = 0;
      this.renderer2.setStyle(this.elementRef.nativeElement, 'outline', 'none');
    }
  }

  @HostListener('keydown.outside-zone', ['$event'])
  protected onKeyDown(event: KeyboardEvent) {
    this.handleKeyPress(event, 'down');
  }

  @HostListener('keyup.outside-zone', ['$event'])
  protected onKeyUp(event: KeyboardEvent) {
    this.handleKeyPress(event, 'up');
  }

  @HostListener('blur.outside-zone', ['$event'])
  protected onBlur(_event: KeyboardEvent) {
    this.handleBlur();
  }

  register({
    callback,
    key = null,
    ctrlKey = null,
    shiftKey = null,
    metaKey = null,
    direction = 'down',
    callbackOnBlur = false,
    onRepeat = true,
    conditions = [],
  }: {
    callback: (event?: KeyboardEvent) => void;
    key?: string;
    ctrlKey?: boolean;
    shiftKey?: boolean;
    metaKey?: boolean;
    direction?: KeyPressDirection;
    callbackOnBlur?: boolean;
    onRepeat?: boolean;
    conditions?: Array<(event: KeyboardEvent) => boolean>;
  }): void {
    this.bindings.push(new KeyBind(key, ctrlKey, shiftKey, metaKey, callback, callbackOnBlur, direction, onRepeat, conditions));
  }

  // ignore direction
  doesKeyBindMatch(keyBind: KeyBind, event: KeyboardEvent) {
    return (
      (keyBind.key === null || keyBind.key === event.key) &&
      (keyBind.ctrlKey == null || keyBind.ctrlKey === event.ctrlKey) &&
      (keyBind.shiftKey == null || keyBind.shiftKey === event.shiftKey) &&
      (keyBind.metaKey == null || keyBind.metaKey === event.metaKey) &&
      keyBind.conditions.every(c => c(event))
    );
  }

  handleKeyPress(event: KeyboardEvent, direction: KeyPressDirection) {
    const matchingBindings = this.bindings.filter((keyBind: KeyBind) => {
      if (event.repeat && !keyBind.onRepeat) return false;

      const matchesLastEvent = () => (this.lastKeyboardEvent ? this.doesKeyBindMatch(keyBind, this.lastKeyboardEvent) : false);
      const matchesCurrentEvent = () => this.doesKeyBindMatch(keyBind, event);

      if (keyBind.direction === null) {
        return matchesLastEvent() || matchesCurrentEvent();
      } else if (direction === 'down' && keyBind.direction === 'down') {
        return matchesCurrentEvent();
      } else if (direction === 'up' && keyBind.direction === 'up') {
        return matchesLastEvent();
      } else {
        return false;
      }
    });

    if (matchingBindings.length > 0) {
      this.zone.run(() => {
        matchingBindings.forEach((triggeredKeyBind: KeyBind) => {
          triggeredKeyBind.callback(event);
        });
      });
    }

    this.lastKeyboardEvent = event;
  }

  handleBlur() {
    this.bindings.filter(binding => binding.callbackOnBlur).forEach(keyUpBinding => this.zone.run(() => keyUpBinding.callback()));
  }
}
