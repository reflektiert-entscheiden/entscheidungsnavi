import { AfterViewInit, Directive, ElementRef, Input } from '@angular/core';
import { BooleanInput, coerceBooleanProperty } from '@angular/cdk/coercion';

@Directive({
  selector: '[dtAutoFocus]',
  standalone: true,
})
export class AutoFocusDirective implements AfterViewInit {
  private _enabled = true;
  @Input('dtAutoFocus') set enabled(value: BooleanInput) {
    this._enabled = coerceBooleanProperty(value);
  }

  @Input('dtAutoFocusSelectAll') selectAll = false;

  constructor(private el: ElementRef<HTMLElement>) {}

  private focus() {
    this.el.nativeElement.focus();

    if (this.selectAll && this.el.nativeElement instanceof HTMLInputElement) {
      this.el.nativeElement.select();
    }
  }

  ngAfterViewInit() {
    if (this._enabled) {
      // Make sure the focus is set after all bindings, in particular ngModel, are loaded
      setTimeout(() => this.focus());
    }
  }
}
