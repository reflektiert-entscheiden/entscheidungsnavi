import { Directive, ElementRef, EventEmitter, OnDestroy, OnInit, Output } from '@angular/core';

@Directive({
  selector: '[dtResizeObserver]',
  standalone: true,
})
export class ResizeObserverDirective implements OnInit, OnDestroy {
  // Single global static ResizeObserver
  private static observer = new ResizeObserver(ResizeObserverDirective.observerCallback);
  private static callbacks = new Map<Element, (newWidth: number) => void>();

  private static observerCallback(entries: ResizeObserverEntry[]) {
    for (const entry of entries) {
      const callback = ResizeObserverDirective.callbacks.get(entry.target);
      callback(entry.contentRect.width);
    }
  }

  // Emits the new width. Is _not_ run in the Angular Zone.
  @Output() dtResizeObserver = new EventEmitter<number>();

  constructor(private elementRef: ElementRef<HTMLElement>) {}

  ngOnInit() {
    // As of now, ResizeObserver callbacks always run outside of the Angular Zone
    ResizeObserverDirective.callbacks.set(this.elementRef.nativeElement, newWidth => this.dtResizeObserver.emit(newWidth));
    ResizeObserverDirective.observer.observe(this.elementRef.nativeElement);
  }

  ngOnDestroy() {
    ResizeObserverDirective.observer.unobserve(this.elementRef.nativeElement);
    ResizeObserverDirective.callbacks.delete(this.elementRef.nativeElement);
  }
}
