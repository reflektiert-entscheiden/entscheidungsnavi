import { AfterViewInit, Directive, ElementRef, EventEmitter, Output } from '@angular/core';
@Directive({
  selector: '[dtViewIntersection]',
})
export class ViewIntersectionDirective implements AfterViewInit {
  @Output()
  enterView: EventEmitter<any> = new EventEmitter();

  @Output()
  leaveView: EventEmitter<any> = new EventEmitter();

  constructor(private elementRef: ElementRef) {}

  ngAfterViewInit(): void {
    const element = this.elementRef.nativeElement;

    const observer = new IntersectionObserver(
      entries => {
        // Not on Screen
        if (entries[0].intersectionRatio === 0) {
          this.leaveView.emit();
        }
        // Fully on Screen
        else if (entries[0].intersectionRatio > 0) {
          this.enterView.emit();
        }
      },
      { threshold: 0 },
    );

    observer.observe(element);
  }
}
