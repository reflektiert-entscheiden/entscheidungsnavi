import { AfterViewInit, ChangeDetectorRef, Directive, ElementRef, Input, OnDestroy } from '@angular/core';

@Directive({
  selector: '[dtScrollbar]',
  exportAs: 'dtScrollbar',
})
export class ScrollbarDirective implements AfterViewInit, OnDestroy {
  @Input('dtScrollbar')
  contentElement: HTMLElement;

  public widthOfVerticalScrollbar = 0;
  public heightOfHorizontalScrollbar = 0;

  private observer: ResizeObserver;

  constructor(
    private cdRef: ChangeDetectorRef,
    private elementRef: ElementRef<HTMLElement>,
  ) {}

  ngAfterViewInit(): void {
    this.checkScrollbar();

    this.observer = new ResizeObserver(() => {
      this.checkScrollbar();
    });

    this.observer.observe(this.contentElement);
  }

  ngOnDestroy() {
    this.observer?.disconnect();
  }

  private checkScrollbar() {
    const element = this.elementRef.nativeElement;
    const newVerticalScrollbarWidth = element.offsetWidth - element.clientWidth;
    const newHorizontalScrollbarHeight = element.offsetHeight - element.clientHeight;

    if (newVerticalScrollbarWidth !== this.widthOfVerticalScrollbar || newHorizontalScrollbarHeight !== this.heightOfHorizontalScrollbar) {
      this.widthOfVerticalScrollbar = newVerticalScrollbarWidth;
      this.heightOfHorizontalScrollbar = newHorizontalScrollbarHeight;

      this.cdRef.detectChanges();
    }
  }
}
