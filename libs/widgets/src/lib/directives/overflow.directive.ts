import { AfterViewInit, ChangeDetectorRef, Directive, ElementRef, Input, NgZone, OnDestroy } from '@angular/core';
import { MatTooltip } from '@angular/material/tooltip';
@Directive({
  selector: '[dtOverflow]',
  exportAs: 'dtOverflow',
  standalone: true,
})
export class OverflowDirective implements AfterViewInit, OnDestroy {
  private static resizeObserver = new ResizeObserver(OverflowDirective.resizeCallback);
  private static resizeCallbacks = new Map<Element, () => void>();

  private static resizeCallback(entries: ResizeObserverEntry[]) {
    for (const entry of entries) {
      const callback = OverflowDirective.resizeCallbacks.get(entry.target);
      callback();
    }
  }

  private mutationObserver: MutationObserver;

  isOverflowing = false;

  @Input('dtOverflow')
  element: Element;

  @Input()
  dtOverflowTooltip: MatTooltip; // If set disables the tooltip when not overflowing

  @Input()
  dtOverflowShow: HTMLElement; // If set, "display: none" when not overflowing

  constructor(
    private elementRef: ElementRef,
    private cdRef: ChangeDetectorRef,
    private zone: NgZone,
  ) {
    this.checkOverflow = this.checkOverflow.bind(this);
  }

  ngAfterViewInit() {
    OverflowDirective.resizeCallbacks.set(this.element ?? this.elementRef.nativeElement, () => this.checkOverflow());
    OverflowDirective.resizeObserver.observe(this.element ?? this.elementRef.nativeElement);

    this.mutationObserver = new MutationObserver(this.checkOverflow);
    this.mutationObserver.observe(this.element ?? this.elementRef.nativeElement, { characterData: true, subtree: true });

    if (this.dtOverflowTooltip) {
      this.dtOverflowTooltip.disabled = true;
      this.cdRef.detectChanges();
    }

    if (this.dtOverflowShow) {
      this.dtOverflowShow.style.display = 'none';
    }
  }

  ngOnDestroy() {
    OverflowDirective.resizeObserver.unobserve(this.element ?? this.elementRef.nativeElement);
    OverflowDirective.resizeCallbacks.delete(this.element ?? this.elementRef.nativeElement);

    this.mutationObserver?.disconnect();
  }

  private checkOverflow() {
    const htmlElement = (this.element ?? this.elementRef.nativeElement) as HTMLElement;

    let shouldBeOverflowing: boolean;
    if (htmlElement.offsetWidth < htmlElement.scrollWidth || htmlElement.offsetHeight < htmlElement.scrollHeight) {
      shouldBeOverflowing = true;
    } else {
      shouldBeOverflowing = false;
    }

    if (this.dtOverflowShow) {
      (this.dtOverflowShow.firstChild as HTMLElement).style.left =
        ((htmlElement.previousElementSibling as HTMLElement).offsetWidth || 0) + 'px';
    }

    if (shouldBeOverflowing !== this.isOverflowing) {
      this.zone.run(() => {
        this.isOverflowing = shouldBeOverflowing;

        if (this.dtOverflowTooltip) {
          this.dtOverflowTooltip.disabled = !this.isOverflowing;
        }

        if (this.dtOverflowShow) {
          this.dtOverflowShow.style.display = this.isOverflowing ? 'block' : 'none';
        }

        this.cdRef.detectChanges();
      });
    }
  }
}
