import { Injectable } from '@angular/core';
import { MatPaginatorIntl } from '@angular/material/paginator';
import { Subject } from 'rxjs';

@Injectable()
export class PaginatorIntlService implements MatPaginatorIntl {
  changes = new Subject<void>();

  firstPageLabel = $localize`Erste Seite`;
  itemsPerPageLabel = $localize`Elemente pro Seite:`;
  lastPageLabel = $localize`Letzte Seite`;

  nextPageLabel = $localize`Nächste Seite`;
  previousPageLabel = $localize`Vorherige Seite`;

  getRangeLabel(page: number, pageSize: number, length: number): string {
    if (length == 0 || pageSize == 0) {
      return $localize`0 von ${length}`;
    }

    const startIndex = page * pageSize;

    // If the start index exceeds the list length, do not try and fix the end index to the end.
    const endIndex = startIndex < length ? Math.min(startIndex + pageSize, length) : startIndex + pageSize;

    return $localize`${startIndex + 1} – ${endIndex} von ${length}`;
  }
}
