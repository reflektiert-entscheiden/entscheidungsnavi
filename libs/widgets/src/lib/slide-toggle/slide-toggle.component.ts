import { Component, EventEmitter, HostBinding, Input, Output } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatSlideToggle } from '@angular/material/slide-toggle';

@Component({
  selector: 'dt-slide-toggle',
  templateUrl: './slide-toggle.component.html',
  styles: [
    `
      :host {
        > div {
          padding: 5px;
          overflow: hidden;
        }

        ::ng-deep .mdc-switch__shadow::before {
          display: flex;
          justify-content: center;
          align-items: center;

          width: 100%;
          height: 100%;

          content: var(--dt-slide-toggle-slider-text);

          color: white;
          font-size: 12px;
          font-weight: 500;
        }
      }
    `,
  ],
  standalone: true,
  imports: [MatIconModule, MatButtonModule, MatTooltipModule, MatSlideToggle],
})
export class SlideToggleComponent {
  @Input() value = false;
  @Input() tooltipTrue: string;
  @Input() tooltipFalse: string;
  @Input() disabled = false;

  @HostBinding('style.--dt-slide-toggle-slider-text')
  // eslint-disable-next-line @angular-eslint/no-input-rename
  @Input({ transform: (value: string) => `'${value}'` })
  sliderTextAbbreviation: string;

  @Output() valueChange = new EventEmitter<boolean>(this.value);
}
