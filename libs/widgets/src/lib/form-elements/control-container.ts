import { Provider, SkipSelf } from '@angular/core';
import { ControlContainer } from '@angular/forms';

export function controlProviderFactory(container: ControlContainer) {
  return container;
}

// This provider creates a connection between the parent component's form and the current component's
// form directives. Should be put in the component decorator's viewProviders array.
// Nested form parts MUST NOT use ChangeDetectionStrategy.OnPush!
export const FORWARD_CONTROL_CONTAINER: Provider = {
  provide: ControlContainer,
  useFactory: controlProviderFactory,
  deps: [[new SkipSelf(), ControlContainer]],
};
