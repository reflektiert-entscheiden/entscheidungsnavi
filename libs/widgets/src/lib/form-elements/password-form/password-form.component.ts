import { Component, Input, OnInit } from '@angular/core';
import { ControlContainer, UntypedFormControl, UntypedFormGroup, Validators } from '@angular/forms';
import { OnDestroyObservable } from '@entscheidungsnavi/tools';
import { Observable } from 'rxjs';
import { FORWARD_CONTROL_CONTAINER, matchOtherValidator } from '..';

/*
  adds password and passwordRepeat to parent form group
  See registrieren.component for Usage Example
**/
@Component({
  selector: 'dt-password-form',
  templateUrl: 'password-form.component.html',
  styleUrls: ['password-form.component.scss'],
  viewProviders: [FORWARD_CONTROL_CONTAINER],
})
export class PasswordFormComponent implements OnInit {
  @OnDestroyObservable()
  private onDestroy$: Observable<void>;

  @Input()
  passwordChange = false;
  @Input()
  newPassword = false;

  hidePassword = true;
  passwordValid = false;

  form: UntypedFormGroup;

  constructor(public controlContainer: ControlContainer) {}

  ngOnInit() {
    this.form = this.controlContainer.control as UntypedFormGroup;

    if (this.passwordChange) {
      this.form.addControl('oldPassword', new UntypedFormControl('', [Validators.required]));
    }
    this.form.addControl('password', new UntypedFormControl('', [Validators.required, Validators.minLength(8)]));
    this.form.addControl(
      'passwordRepeat',
      new UntypedFormControl('', [
        Validators.required,
        Validators.minLength(8),
        matchOtherValidator(this.form.controls.password as UntypedFormControl, this.onDestroy$),
      ]),
    );
  }
}
