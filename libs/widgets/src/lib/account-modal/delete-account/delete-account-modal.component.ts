import { HttpErrorResponse } from '@angular/common/http';
import { Component } from '@angular/core';
import { NonNullableFormBuilder, Validators } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { AuthService, EventsService, OnlineProjectsService, TeamsService } from '@entscheidungsnavi/api-client';
import { OnDestroyObservable } from '@entscheidungsnavi/tools';
import { Observable, first, forkJoin, map, takeUntil } from 'rxjs';

@Component({
  templateUrl: './delete-account-modal.component.html',
  styleUrls: ['./delete-account-modal.component.scss'],
})
export class DeleteAccountModalComponent {
  @OnDestroyObservable()
  private onDestroy$: Observable<any>;

  confirmForm = this.fb.group({
    password: ['', Validators.required],
  });

  userData$ = forkJoin([
    this.onlineProjects.getProjectList(),
    this.events.events$.pipe(first()),
    this.teamsService.getTeamProjects(true),
  ]).pipe(
    map(([projectList, eventList, teamList]) => ({
      projectCount: projectList.length,
      submittedEvents: eventList.filter(event => event.submitted).map(event => event.event.name),
      teamsWhereOwner: teamList.map(team => team.name),
    })),
  );

  readonly email$ = this.authService.user$.pipe(map(user => user?.email));

  constructor(
    private dialogRef: MatDialogRef<DeleteAccountModalComponent>,
    private snackBar: MatSnackBar,
    private authService: AuthService,
    private onlineProjects: OnlineProjectsService,
    private teamsService: TeamsService,
    private events: EventsService,
    private fb: NonNullableFormBuilder,
  ) {
    authService.onLogout$.pipe(takeUntil(this.onDestroy$)).subscribe(() => {
      this.dialogRef.close();
    });
  }

  close() {
    if (this.confirmForm.enabled) this.dialogRef.close();
  }

  delete() {
    if (this.confirmForm.disabled || this.confirmForm.invalid) {
      return;
    }

    const pw = this.confirmForm.value.password;
    this.confirmForm.disable({ emitEvent: false });
    this.authService.deleteUser(pw).subscribe({
      next: () => this.snackBar.open($localize`Dein Account wurde erfolgreich gelöscht`, 'Ok'),
      error: error => {
        this.confirmForm.enable();

        if (error instanceof HttpErrorResponse && error.status === 401) {
          this.confirmForm.controls.password.setErrors({ wrongPassword: true });
        } else {
          this.confirmForm.controls.password.setErrors({ something: true });
        }
      },
    });
  }
}
