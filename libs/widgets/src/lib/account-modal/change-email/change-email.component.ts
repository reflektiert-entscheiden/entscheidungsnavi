import { HttpErrorResponse } from '@angular/common/http';
import { Component, EventEmitter, Output } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { AuthService } from '@entscheidungsnavi/api-client';
import { OnDestroyObservable } from '@entscheidungsnavi/tools';
import { filter, Observable, takeUntil } from 'rxjs';

@Component({
  selector: 'dt-change-email',
  templateUrl: 'change-email.component.html',
})
export class ChangeEmailComponent {
  @OnDestroyObservable()
  private onDestroy$: Observable<void>;

  @Output() emailChanged = new EventEmitter<void>();

  originalEmail: string;

  emailForm = this.fb.group({
    email: this.fb.control<string>('', [Validators.required, Validators.email]),
  });

  constructor(
    private authService: AuthService,
    private fb: FormBuilder,
  ) {
    this.authService.user$.pipe(filter(Boolean), takeUntil(this.onDestroy$)).subscribe(user => {
      this.originalEmail = user.email;
      if (this.emailForm.value.email == null) {
        this.emailForm.controls.email.setValue(this.originalEmail);
      }
    });
  }

  onSubmit() {
    this.emailForm.updateValueAndValidity();

    if (this.emailForm.valid) {
      this.emailForm.disable({ emitEvent: false });

      const email = this.emailForm.controls.email.value;

      this.authService.changeUserMail(email).subscribe({
        next: () => {
          this.emailForm.enable();
          this.originalEmail = email;
          this.emailChanged.emit();
        },
        error: (_err: HttpErrorResponse) => {
          this.emailForm.enable();

          this.emailForm.setErrors({ 'server-error': true });
        },
      });
    }
  }
}
