import { Component } from '@angular/core';
import { MatDialogRef, MatDialog } from '@angular/material/dialog';
import { OnDestroyObservable } from '@entscheidungsnavi/tools';
import { Observable } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { AuthService } from '@entscheidungsnavi/api-client';
import { EmailConfirmationModalComponent } from '../email-confirmation-modal/email-confirmation-modal.component';
import { DeleteAccountModalComponent } from './delete-account/delete-account-modal.component';

@Component({
  templateUrl: './account-modal.component.html',
  styleUrls: ['./account-modal.component.scss'],
})
export class AccountModalComponent {
  screen: 'overview' | 'email' | 'username' | 'password' = 'overview';

  @OnDestroyObservable()
  private onDestroy$: Observable<any>;

  constructor(
    protected authService: AuthService,
    private dialog: MatDialog,
    protected dialogRef: MatDialogRef<AccountModalComponent>,
  ) {
    authService.onLogout$.pipe(takeUntil(this.onDestroy$)).subscribe(() => {
      this.dialogRef.close();
    });
  }

  confirmEmail() {
    this.dialog.open(EmailConfirmationModalComponent);
  }

  deleteAccount() {
    this.dialog.open(DeleteAccountModalComponent);
  }
}
