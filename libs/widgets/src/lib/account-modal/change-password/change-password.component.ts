import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { UntypedFormGroup } from '@angular/forms';
import { AuthService } from '@entscheidungsnavi/api-client';

@Component({
  selector: 'dt-change-password',
  templateUrl: 'change-password.component.html',
  styleUrls: ['./change-password.component.scss'],
})
export class ChangePasswordComponent implements OnInit {
  form: UntypedFormGroup;

  @Output()
  changePasswordSuccess = new EventEmitter();

  constructor(
    private authService: AuthService,
    private snackBarService: MatSnackBar,
  ) {}

  ngOnInit() {
    this.form = new UntypedFormGroup({});
  }

  onSubmit() {
    this.form.updateValueAndValidity();

    if (this.form.valid) {
      this.form.disable({ emitEvent: false });

      const newPassword: string = this.form.controls.password.value;
      const oldPassword: string = this.form.controls.oldPassword.value;
      this.authService.changePassword(oldPassword, newPassword).subscribe({
        next: () => {
          this.snackBarService.open($localize`Passwort erfolgreich geändert!`, undefined, { duration: 4000 });
          this.changePasswordSuccess.emit();
        },
        error: (err: HttpErrorResponse) => {
          this.form.enable();

          if (err.status === 401) {
            this.form.setErrors({ 'wrong-password': true });
          } else {
            this.form.setErrors({ 'server-error': true });
          }
        },
      });
    }
  }
}
