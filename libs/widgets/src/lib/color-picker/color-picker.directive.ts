import { Directive, ElementRef, EventEmitter, HostListener, Input, Output } from '@angular/core';
import { ColorPickerService } from './color-picker.service';

@Directive({
  selector: '[dtColorPicker]',
  standalone: true,
})
export class ColorPickerDirective {
  pickerOpen = false;

  @Input('dtColorPicker')
  initialColor: string;

  @Input()
  dtColorPickerPreset: string[] | (() => string[]);

  @Output('dtColorPicker')
  colorChange = new EventEmitter<string>();

  @Output()
  dtColorPickerOpen = new EventEmitter<void>();

  @Output()
  dtColorPickerClose = new EventEmitter<string | null>();

  @Input()
  disabled = false;

  constructor(
    private colorPickerService: ColorPickerService,
    private elRef: ElementRef<HTMLElement>,
  ) {}

  @HostListener('click')
  hostClick() {
    if (!this.pickerOpen && !this.disabled) {
      this.openPicker();
    }
  }

  openPicker() {
    let finalPreset;

    if (this.dtColorPickerPreset) {
      if (Array.isArray(this.dtColorPickerPreset)) {
        finalPreset = this.dtColorPickerPreset.slice();
      } else {
        finalPreset = this.dtColorPickerPreset();
      }
    }

    this.dtColorPickerOpen.emit();

    let changedColor: string = null;
    this.colorPickerService.openColorPicker(this.elRef.nativeElement, this.initialColor, finalPreset).subscribe({
      next: (newColor: string) => {
        changedColor = newColor;
        this.colorChange.emit(newColor);
      },
      complete: () => {
        this.pickerOpen = false;
        this.dtColorPickerClose.emit(changedColor);
      },
    });

    this.pickerOpen = true;
  }
}
