import { Component, Inject } from '@angular/core';
import { MatSnackBar, MAT_SNACK_BAR_DATA } from '@angular/material/snack-bar';
import { finalize, Observable } from 'rxjs';

@Component({
  templateUrl: './loading-snackbar.component.html',
  styleUrls: ['./loading-snackbar.component.scss'],
})
export class LoadingSnackbarComponent {
  constructor(@Inject(MAT_SNACK_BAR_DATA) public data: { text: string }) {}
}

export function loadingIndicator<T>(snackBar: MatSnackBar, message: string) {
  return (source: Observable<T>) => {
    return new Observable<T>(subscriber => {
      const snackbarRef = snackBar.openFromComponent(LoadingSnackbarComponent, { data: { text: message } });

      source.pipe(finalize(() => snackbarRef.dismiss())).subscribe(subscriber);
    });
  };
}
