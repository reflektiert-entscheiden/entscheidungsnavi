import { ThemePalette } from '@angular/material/core';

export type ButtonColor = ThemePalette | 'accent-less';
