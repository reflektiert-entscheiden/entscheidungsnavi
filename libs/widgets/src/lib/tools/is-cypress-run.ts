export function isCypressRun() {
  let isCypress = false;
  try {
    isCypress = !!(window as any)['Cypress'] || !!(window.parent as any)['Cypress'];
  } catch (e) {
    if (e instanceof DOMException) {
      // We are embedded in an <iframe> and tried to access a DOM element from a different origin (e.g. from the cockpit).
      isCypress = false;
    } else {
      throw e;
    }
  }
  return isCypress;
}
