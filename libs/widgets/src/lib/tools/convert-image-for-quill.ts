/**
 * Convert / Crops a base64 image to a base64 jpeg with a max width and height of 1000px
 * @returns The base64 jpeg image
 */
export async function convertImageForQuill(imageDataURL: string) {
  // If not in a browser context throw
  if (typeof window === 'undefined') {
    throw new Error('convertImageForQuill can only be used in a browser context');
  }

  return new Promise<string>((res, rej) => {
    const canvas = document.createElement('canvas');
    const ctx = canvas.getContext('2d');
    const img = new Image();

    img.src = imageDataURL;

    const imageSizeInBytes = atob(imageDataURL.split(',')[1]).length;

    img.onload = () => {
      if (img.width <= 1000 && img.height <= 1000 && img.src.startsWith('data:image/jpeg') && imageSizeInBytes < 1024 * 1024) {
        // No need to convert the image
        res(imageDataURL);
        return;
      }
      try {
        // Limit the image size to 1000x1000
        if (img.width > 1000 || img.height > 1000) {
          const ratio = Math.min(1000 / img.width, 1000 / img.height);

          canvas.width = img.width * ratio;
          canvas.height = img.height * ratio;
        } else {
          canvas.width = img.width;
          canvas.height = img.height;
        }

        ctx.fillStyle = '#ffffff';
        ctx.fillRect(0, 0, canvas.width, canvas.height);

        ctx.drawImage(img, 0, 0, canvas.width, canvas.height);

        res(canvas.toDataURL('image/jpeg', 0.8));
      } catch (error) {
        rej(error);
      }
    };

    img.onerror = (error: unknown) => rej(error);
  });
}
