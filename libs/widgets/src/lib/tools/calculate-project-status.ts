import { sum } from 'lodash';
import { DecisionData } from '@entscheidungsnavi/decision-data';
import { calculateCurrentProgress } from '@entscheidungsnavi/decision-data/progress';

export function calculateProjectStatusValues(decisionData: DecisionData): string[] {
  const currentProgress = calculateCurrentProgress(decisionData);
  let numberOfObjectivesAspects = 0;
  let numberOfAlternatives = 0;
  let numberOfProcessedOutcomes = 0;
  let numberOfTradeoffs = 0;
  const projectStatusText: string[] = ['', '', '', '', ''];

  projectStatusText[0] =
    decisionData.decisionStatement.statement === null || decisionData.decisionStatement.statement === ''
      ? $localize`Entscheidungsfrage fehlt`
      : $localize`Entscheidungsfrage formuliert`;

  // if there are any objectives get the number of all aspects of the objectives
  if (decisionData.objectives.length > 0) {
    numberOfObjectivesAspects = decisionData.objectives
      .map(obj => obj.getAspectsAsArray())
      .map(asp => asp.length)
      .reduce((acc, curr) => acc + curr);
  }

  if (decisionData.objectives.length === 0) {
    projectStatusText[1] += $localize`Noch nicht bearbeitet`;
  } else if (decisionData.objectives.length === 1) {
    projectStatusText[1] += $localize`1 Fundamentalziel, `;
  } else {
    projectStatusText[1] += $localize`${decisionData.objectives.length} Fundamentalziele, `;
  }

  if (decisionData.objectives.length > 0) {
    projectStatusText[1] += numberOfObjectivesAspects === 1 ? $localize`1 Teilziel` : $localize`${numberOfObjectivesAspects} Teilziele`;
  }

  // count alternatives which do have an entry
  decisionData.alternatives.forEach(alt => {
    if (alt.name !== '') {
      numberOfAlternatives += 1;
    }
  });

  if (numberOfAlternatives === 0) {
    projectStatusText[2] = $localize`Noch nicht bearbeitet`;
  } else if (numberOfAlternatives === 1) {
    projectStatusText[2] = $localize`${numberOfAlternatives} Alternative`;
  } else {
    projectStatusText[2] = $localize`${numberOfAlternatives} Alternativen`;
  }

  // get number of influence factors and forecasts of outcomes
  if (currentProgress.step === 'impactModel' || currentProgress.step === 'results') {
    // go over all impact matrix entries and count all which are processed
    for (let altInd = 0; altInd < decisionData.alternatives.length; altInd++) {
      for (let objInd = 0; objInd < decisionData.objectives.length; objInd++) {
        if (decisionData.outcomes[altInd][objInd].processed) {
          numberOfProcessedOutcomes += 1;

          // consider all impact matrix entries where an influence factor was used
          if (decisionData.outcomes[altInd][objInd].influenceFactor !== undefined) {
            numberOfProcessedOutcomes += decisionData.outcomes[altInd][objInd].influenceFactor.stateCount - 1;
          }
        }
      }
    }

    projectStatusText[3] =
      numberOfProcessedOutcomes === 1 ? $localize`1 Wirkungsprognose` : $localize`${numberOfProcessedOutcomes} Wirkungsprognosen`;

    if (decisionData.projectMode !== 'starter') {
      projectStatusText[3] += ', ';

      projectStatusText[3] +=
        decisionData.influenceFactors.length === 1
          ? $localize`1 Einflussfaktor`
          : $localize`${decisionData.influenceFactors.length} Einflussfaktoren`;

      projectStatusText[3] += ', ';

      const usedIndicators = sum(
        decisionData.objectives.map(objective => (objective.isIndicator ? objective.indicatorData.indicators.length : 0)),
      );
      projectStatusText[3] += usedIndicators === 1 ? $localize`1 Indikator` : $localize`${usedIndicators} Indikatoren`;
    }
  } else {
    projectStatusText[3] = $localize`Noch nicht bearbeitet`;
  }

  // make sure that the user is already at the last step in order to process the last step
  if (currentProgress.step === 'results' && (currentProgress.subStepIndex === undefined || currentProgress.subStepIndex === 1)) {
    projectStatusText[4] +=
      decisionData.objectives.length === 1 ? $localize`Nutzenfunktion definiert und ` : $localize`Nutzenfunktionen definiert und `;

    if (decisionData.projectMode === 'starter') {
      projectStatusText[4] = '';
    } else if (decisionData.projectMode === 'educational') {
      numberOfTradeoffs = countTradeoffs(decisionData);

      projectStatusText[4] += numberOfTradeoffs === 1 ? $localize`1 Trade-Off` : $localize`${numberOfTradeoffs} Trade-Offs`;
    }
  } else if (currentProgress.step === 'results' && currentProgress.subStepIndex === 0) {
    projectStatusText[4] = $localize`Nutzenfunktionen definiert und 0 Trade-Offs`;
  } else {
    projectStatusText[4] = $localize`Noch nicht bearbeitet`;
  }

  return projectStatusText;
}

// only for educational, since we do not have any tradeoffs in starter
function countTradeoffs(decisionData: DecisionData): number {
  let tradeoffs = 0;

  if (decisionData.weights.tradeoffObjectiveIdx === null) {
    // if there is no reference objective set we do not have any tradeoffs
    return tradeoffs;
  } else {
    // in educational mode we count every verified tradeoff if reference objective is set
    decisionData.weights.tradeoffWeights.forEach((tradeoffWeight, index) => {
      if (tradeoffWeight !== null && !decisionData.weights.unverifiedWeights[index]) {
        tradeoffs += 1;
      }
    });

    // subtract one if all weights and therefore the objective reference are set
    if (tradeoffs === decisionData.objectives.length) {
      tradeoffs -= 1;
    }

    return tradeoffs;
  }
}
