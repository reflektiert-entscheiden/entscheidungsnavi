import { PredefinedInfluenceFactorId } from '@entscheidungsnavi/decision-data';

export const PREDEFINED_INFLUENCE_FACTOR_NAMES: Record<
  PredefinedInfluenceFactorId,
  { name: string; shortStateNames: string[]; stateNames: string[] }
> = {
  '3-Scenarios': {
    name: $localize`Worst-Best-Verteilung`,
    shortStateNames: [$localize`Worst`, $localize`Median`, $localize`Best`],
    stateNames: [$localize`Worst (p.10)`, $localize`Median (p.50)`, $localize`Best (p.90)`],
  },
};
