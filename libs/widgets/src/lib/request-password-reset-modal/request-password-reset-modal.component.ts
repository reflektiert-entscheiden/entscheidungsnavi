import { HttpErrorResponse } from '@angular/common/http';
import { Component, Inject } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { AuthService } from '@entscheidungsnavi/api-client';

@Component({
  templateUrl: 'request-password-reset-modal.component.html',
  styleUrls: ['request-password-reset-modal.component.scss'],
})
export class RequestPasswordResetModalComponent {
  resetForm = this.fb.nonNullable.group({
    email: ['', [Validators.required, Validators.email]],
  });

  done = false;

  constructor(
    private authService: AuthService,
    private dialogRef: MatDialogRef<RequestPasswordResetModalComponent>,
    private fb: FormBuilder,
    @Inject(MAT_DIALOG_DATA) presetEmail?: string,
  ) {
    if (presetEmail) {
      this.resetForm.controls.email.setValue(presetEmail);
    }
  }

  close() {
    this.dialogRef.close();
  }

  onSubmit() {
    this.resetForm.updateValueAndValidity();

    if (this.resetForm.valid) {
      this.resetForm.disable({ emitEvent: false });

      const email = this.resetForm.controls.email.value;

      this.authService.requestPasswordReset(email).subscribe({
        next: () => {
          this.done = true;
        },
        error: (err: HttpErrorResponse) => {
          this.resetForm.enable();
          switch (err.status) {
            case 400:
              this.resetForm.controls.email.setErrors({ email: true });
              break;
            case 404:
              this.resetForm.setErrors({
                'server-error': 'account-not-found',
              });
              break;
            case 429:
              this.resetForm.setErrors({
                'server-error': 'rate-limit',
              });
              break;
            default:
              this.resetForm.setErrors({
                'server-error': 'something',
              });
              break;
          }
        },
      });
    }
  }
}
