import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { YoutubeVideoCategoryDto, YoutubeVideoDto } from '@entscheidungsnavi/api-client';
import { NAVI_STEP_ORDER, STEP_NAMES } from '@entscheidungsnavi/decision-data';
import { PersistentSetting, PersistentSettingParent } from '../decorators';

export type YoutubeModalData = {
  videoId?: string;
  startTime?: number;
  stepNumber?: number;
  selectedTabIndex?: number;
  categoryId?: string;
  categoryIdx?: number;
  videos: YoutubeVideoDto[];
  categories: YoutubeVideoCategoryDto[];
};

@PersistentSettingParent('YoutubeModalComponent')
@Component({
  templateUrl: './youtube-modal.component.html',
  styleUrls: ['./youtube-modal.component.scss'],
})
export class YoutubeModalComponent {
  @PersistentSetting()
  consent = false;

  videos: YoutubeVideoDto[] = [];
  categories: YoutubeVideoCategoryDto[] = [];

  protected selectedCategoryIdx: number;
  protected selectedStepIdx: number;
  protected selectedYoutubeId: string;
  private startTime: number; // in seconds

  protected shownSteps: string[];

  protected selectedTabIndex = 0;

  get selectedYoutubeIdWithTime() {
    return this.startTime != null ? `${this.selectedYoutubeId}?start=${this.startTime}` : this.selectedYoutubeId;
  }

  get selectedCategory() {
    return this.categories[this.selectedCategoryIdx];
  }

  get displayedVideos(): YoutubeVideoDto[] {
    if (this.selectedTabIndex === 0 && this.selectedStepIdx != null) {
      return this.videos.filter(video => video.stepNumbers.includes(this.selectedStepIdx));
    } else if (this.selectedTabIndex === 1 && this.selectedCategory?.id != null) {
      return this.videos.filter(
        video => video.categoryIds.filter(categoryId => categoryId.toString() === this.selectedCategory.id).length > 0,
      );
    }
    return [];
  }

  constructor(
    @Inject(MAT_DIALOG_DATA)
    protected modalData: YoutubeModalData,
    protected dialogRef: MatDialogRef<YoutubeModalComponent>,
  ) {
    this.videos = modalData.videos;
    this.startTime = modalData.startTime;
    this.categories = modalData.categories;
    this.selectedTabIndex = modalData.selectedTabIndex ?? 0;
    this.selectedStepIdx = modalData.stepNumber ?? 0;

    if (this.modalData.videoId) {
      // if a videoId is passed, select the corresponding video
      this.selectVideo(modalData.videoId, modalData.startTime);
    } else if (this.modalData.categoryId || this.modalData.categoryIdx != null) {
      this.selectedTabIndex = 1;
      this.selectedCategoryIdx =
        this.modalData.categoryIdx ?? this.categories.findIndex(category => category.id === this.modalData.categoryId);
    }
    this.updateSelectedVideo();

    // the steps and categories are not translated to English, as the videos are only available in German
    // ['Entscheidungsfrage', 'Fundamentalziele', ... 'Evaluation']
    this.shownSteps = [...NAVI_STEP_ORDER.slice(0, -1).map(stepOrder => STEP_NAMES.de[stepOrder].name)];
  }

  // manually select a video from displayedVideos
  public selectVideo(youtubeId: string, startTime?: number) {
    this.selectedYoutubeId = youtubeId;
    this.startTime = startTime ?? null; // reset start time if not provided
    this.updateSelectedCategoryAndStep(youtubeId);
  }

  private updateSelectedVideo() {
    const videos = this.displayedVideos;
    if (!this.selectedYoutubeId && videos.length > 0) {
      this.selectedYoutubeId = videos[0].youtubeId;
    }
    this.updateSelectedCategoryAndStep(this.selectedYoutubeId);
  }

  private updateSelectedCategoryAndStep(youtubeId: string) {
    const currentVideo = this.videos.find(video => video.youtubeId === youtubeId);
    if (!currentVideo) {
      return;
    }

    //switch to categories tab when video has no stepnumbers
    if (currentVideo.stepNumbers.length == 0) {
      this.selectedTabIndex = 1;
    }
    if (!this.selectedStepIdx) {
      this.selectedStepIdx = currentVideo.stepNumbers[0];
    }

    // don't override existing startTime (e.g. when opening a video with a specific startTime from the Assistant)
    this.startTime = this.startTime ?? currentVideo.startTime;

    this.updateSelectionIndexes(currentVideo);
  }

  private updateSelectionIndexes(video: YoutubeVideoDto) {
    if (this.selectedTabIndex === 0 || this.selectedCategoryIdx == null) {
      this.selectedCategoryIdx = this.categories.findIndex(category =>
        video.categoryIds.map(categoryId => categoryId.toString()).includes(category.id),
      );
    }
    if (this.selectedTabIndex === 1 || this.selectedStepIdx == null) {
      this.selectedStepIdx = video.stepNumbers[0];
    }
  }
}
