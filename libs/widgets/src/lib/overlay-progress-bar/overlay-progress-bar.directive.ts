import { ComponentRef, Directive, ElementRef, Input, OnInit, Renderer2, ViewContainerRef } from '@angular/core';
import { ThemePalette } from '@angular/material/core';
import { MatProgressBar } from '@angular/material/progress-bar';
import { BehaviorSubject, delay, distinctUntilChanged, Observable, of, switchMap } from 'rxjs';
import { OnDestroyObservable } from '@entscheidungsnavi/tools';
import { finalize, takeUntil, tap } from 'rxjs/operators';

@Directive({
  selector: '[dtOverlayProgress]',
  standalone: true,
  exportAs: 'dtOverlayProgress',
})
export class OverlayProgressBarDirective implements OnInit {
  private visibleSubject$ = new BehaviorSubject<boolean>(false);
  visible$ = this.visibleSubject$.asObservable();

  @OnDestroyObservable()
  onDestroy$: Observable<void>;

  @Input('dtOverlayProgressColor')
  color: ThemePalette = 'primary';

  @Input('dtOverlayProgressForceRelative')
  forceRelative = true;

  componentRef: ComponentRef<MatProgressBar>;

  @Input('dtOverlayProgress')
  set visible(newValue: boolean) {
    this.visibleSubject$.next(newValue);
  }

  constructor(
    private viewContainer: ViewContainerRef,
    private elementRef: ElementRef,
    private render2: Renderer2,
  ) {}

  ngOnInit() {
    if (this.forceRelative) {
      this.render2.setStyle(this.elementRef.nativeElement, 'position', 'relative');
    }

    // Instantiate Material Progressbar and add it to parent view
    this.componentRef = this.viewContainer.createComponent(MatProgressBar);
    this.componentRef.instance.mode = 'indeterminate';
    this.componentRef.instance.color = this.color;

    // Get HTML Element of the Progress Bar
    const htmlElement = this.componentRef.location.nativeElement as HTMLElement;

    // Apply Styles
    this.render2.setStyle(htmlElement, 'position', 'absolute');
    this.render2.setStyle(htmlElement, 'left', '0');
    this.render2.setStyle(htmlElement, 'right', '0');
    this.render2.setStyle(htmlElement, 'bottom', '0');

    this.render2.setAttribute(htmlElement, 'data-cy', 'dt-overlay-progress');

    // Move Progressbar INSIDE element the directive is attached to
    this.render2.appendChild(this.elementRef.nativeElement, htmlElement);

    this.visibleSubject$
      .pipe(
        distinctUntilChanged(),
        switchMap(visible => of(visible).pipe(delay(visible ? 500 : 0))),
        takeUntil(this.onDestroy$),
      )
      .subscribe(visible => {
        if (this.componentRef) {
          const htmlElement = this.componentRef.location.nativeElement as HTMLElement;
          this.render2.setStyle(htmlElement, 'display', visible ? 'block' : 'none');
        }
      });
  }

  reactivePipe() {
    return <T>(source: Observable<T>) =>
      of({}).pipe(
        tap(() => {
          this.visible = true;
        }),
        switchMap(_ => source.pipe(finalize(() => (this.visible = false)))),
      );
  }
}
