import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'dt-tab',
  styleUrls: ['./tab.component.scss'],
  template: `
    <div [hidden]="!active" [class.flex-tab]="active && flex" [class.padding]="padding">
      <ng-content></ng-content>
    </div>
  `,
})
export class TabComponent {
  @Input() title: string;
  @Input() active = false;
  @Input() flex = false;
  @Input() padding = true;

  @Output() selectTab: EventEmitter<void> = new EventEmitter();
  @Output() activeChange = new EventEmitter<boolean>();
  @Output() tabClick = new EventEmitter();

  activate() {
    if (!this.active) {
      this.active = true;
      this.selectTab.emit();
      this.activeChange.emit(true);
    }
  }
  deactivate() {
    if (this.active) {
      this.active = false;
      this.activeChange.emit(false);
    }
  }
}
