import {
  ChangeDetectionStrategy,
  Component,
  ElementRef,
  EventEmitter,
  Input,
  OnChanges,
  OnInit,
  Output,
  SimpleChanges,
  ViewChild,
} from '@angular/core';
import { clamp, round } from 'lodash';
import '@entscheidungsnavi/tools/number-rounding';
import { Interval } from '@entscheidungsnavi/tools';

// See sensitivity-analysis.component for usage example
@Component({
  selector: 'dt-range-input',
  styleUrls: ['range-input.component.scss'],
  templateUrl: 'range-input.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class RangeInputComponent implements OnInit, OnChanges {
  // required
  @Input() value: number;
  @Input() min: number;
  @Input() max: number;

  // optional
  @Input() step = 1;
  @Input() initialValue: number;
  @Input() interval: Interval;
  @Input() disabled = false;
  @Input() maxValueLength = 7; // Can be overriden to prevent wrapping of the value label
  @Input() showLimits = false; // Whether to show the limits below the slider
  @Input() shownMin: string; // Overrides the limits shown on the input (If shownValue is set this probably has to be overridden as well)
  @Input() shownMax: string;
  @Input() valuePosition: 'left' | 'above' | 'hide' = 'left'; // Where the value should be shown: left or above the thumb or hidden

  // Value limits are additional min / max limitations imposed on the value, WITHOUT changing the scale of the actual input
  @Input() valueLimits: Interval;
  @Input() shownValue: number | string; // show this value as text instead of "value", if set
  @Input() unit: 'none' | 'percent' = 'none';
  @Input() hasResponsiveLabelWidth = false;
  @Input() allowManualInput = false;
  /* If range input has absoluteUpperBound, then the maximum is considered soft.
   * This means that the slider value can exceed the maximum up until absoluteUpperBound.
   * The new maximum is set to absoluteUpperBound.
   */
  @Input() absoluteUpperBound: number;

  @Output() valueChange: EventEmitter<number> = new EventEmitter();
  @Output() maxChange: EventEmitter<number> = new EventEmitter();

  @ViewChild('rangeInput') rangeInput: ElementRef<HTMLInputElement>;
  @ViewChild('numberInput') numberInput: ElementRef<HTMLInputElement>;

  isLabelNarrow = false;

  // Cached Values
  protected _initValueLeft = 0;
  protected _intervalLeft = 0;
  protected _intervalWidth = 0;
  protected _stepDecimals = 0;

  get valueRange(): number {
    return this.max - this.min;
  }

  ngOnInit() {
    this.valueLimits = this.valueLimits || new Interval(this.min, this.max);
    this.calcLayout();
  }

  ngOnChanges(changes: SimpleChanges) {
    if ('max' in changes && this.absoluteUpperBound != null) {
      // only update valueLimits for sliders with soft maximum (that can be subject to change)
      this.valueLimits = new Interval(this.min, this.max);
    }

    if ('step' in changes) {
      this._stepDecimals = this.countDecimals(this.step);
    }

    if ('value' in changes || 'valueLimits' in changes || 'min' in changes || 'max' in changes) {
      const limits = this.valueLimits || { start: this.min, end: this.max };
      this.value = clamp(round(+this.value, this._stepDecimals), limits.start, limits.end);
    }

    this.calcLayout();
  }

  countDecimals(value: number) {
    return value % 1 ? value.toString().split('.')[1].length : 0;
  }

  updateMaximum(newValue: number) {
    if (newValue > this.max) {
      /* Increase maximum but clip it at absoluteUpperBound. */
      this.max = Math.min(newValue, this.absoluteUpperBound);
      /* Update value limits. */
      this.valueLimits = new Interval(this.valueLimits.start, this.max);
      this.maxChange.emit(this.max);
    }
  }

  onInputChange(event: any) {
    this.onSliderChange(event.target.value);
  }

  onSliderChange(newValue: number) {
    if (this.absoluteUpperBound != null) {
      this.updateMaximum(newValue);
    }
    const limits = this.valueLimits || { start: this.min, end: this.max };
    const newClampedValue = clamp(round(newValue, this._stepDecimals), limits.start, limits.end);

    if (newClampedValue !== this.value) {
      this.value = newClampedValue;
      this.valueChange.emit(this.value);
    } else if (newValue !== this.value) {
      // rangeInput already changed its value to newValue.
      // In order to prevent getting out-of-sync, we have to explicitly correct it to the model's value.
      this.rangeInput.nativeElement.valueAsNumber = this.value;
      if (this.numberInput) {
        this.numberInput.nativeElement.valueAsNumber = this.value;
      }
    }
  }

  calculateLabelWidth() {
    if (this.isLabelNarrow && this.hasResponsiveLabelWidth) {
      return '40px';
    } else if (this.hasResponsiveLabelWidth) {
      return '70px';
    } else {
      return 'auto';
    }
  }

  private calcLayout() {
    if (this.initialValue != null) {
      this._initValueLeft = ((this.initialValue - this.min) / this.valueRange) * 100;
    }

    if (this.interval) {
      this._intervalLeft = ((this.interval.start - this.min) / this.valueRange) * 100;
      this._intervalWidth = (this.interval.length / this.valueRange) * 100;
    }
  }
}
