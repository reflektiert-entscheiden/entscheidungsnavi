import { animate, state, style, transition, trigger } from '@angular/animations';
import { Component, EventEmitter, Input, OnChanges, Output, SimpleChanges } from '@angular/core';

@Component({
  selector: 'dt-collapsible',
  templateUrl: 'collapsible.component.html',
  styleUrls: ['collapsible.component.scss'],
  animations: [
    trigger('collapseAnimation', [
      state('false', style({ height: 0, display: 'none' })),
      state('true', style({ height: '*' })),
      transition('false => true', [style({ display: 'block' }), animate('250ms ease-out', style({ height: '*' }))]),
      transition('true => false', animate('250ms ease-out')),
    ]),
  ],
})
export class CollapsibleComponent implements OnChanges {
  @Input()
  open = false;

  @Input()
  forceOpen = false;

  @Input() caption: string;
  @Input() captionOpened: string;
  @Input() captionClosed: string;
  @Input() showHeader = true;

  @Output()
  headerClick = new EventEmitter();

  get trueActive() {
    return this.open || this.forceOpen;
  }

  openByLink() {
    // emit a click event, if the item is not active yet
    if (!this.open) {
      this.headerClick.emit();
    }
  }

  ngOnChanges(changes: SimpleChanges) {
    if ('forceOpen' in changes && changes['forceOpen'].currentValue) {
      this.open = true;
    }
  }

  expand() {
    this.open = true;
  }

  collapse() {
    this.open = false;
  }

  toggle() {
    if (!this.forceOpen) {
      this.open = !this.open;
    }
  }
}
