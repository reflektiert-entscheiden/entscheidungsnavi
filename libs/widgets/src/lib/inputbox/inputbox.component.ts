import { animate, AnimationEvent, state, style, transition, trigger } from '@angular/animations';
import { Component, EventEmitter, Input, Output, ViewChild, TemplateRef } from '@angular/core';
import { ModalComponent } from '../modal';

/*
 Usage:
 <dt-inputbox [deleteButton]='true' [collapsibleButton]='true'>
 <div class='caption-wrapper'>Headline</div>
 <div class='control-wrapper'>
 Option to add certain controls e.g. a delete button
 </div>
 <div class='content-wrapper'>
 Test Content
 </div>
 </dt-inputbox>
 */

@Component({
  selector: 'dt-inputbox',
  templateUrl: 'inputbox.component.html',
  styleUrls: ['inputbox.component.scss'],
  animations: [
    trigger('collapseAnimation', [
      state(
        'expanded',
        style({
          height: '*',
          paddingTop: '5px',
          paddingBottom: '5px',
        }),
      ),
      state(
        'collapsed',
        style({
          height: '0',
          paddingTop: '0',
          paddingBottom: '0',
        }),
      ),
      transition('expanded <=> collapsed', [animate('300ms ease-in-out')]),
    ]),
    trigger('deleteAnimation', [
      state('false', style({ opacity: 1 })),
      state('true', style({ opacity: 0 })),
      transition('false => true', animate('500ms ease')),
    ]),
  ],
})
export class InputboxComponent {
  private _collapsed: boolean;

  get collapsed(): boolean {
    return this._collapsed != null ? this._collapsed : this.collapsedByDefault;
  }

  set collapsed(val: boolean) {
    this._collapsed = val;
  }

  @Input() deleteButton = false;
  @Input() deleteDisabled = false;
  @Input() collapsibleButton = false;
  @Input() draggable = true;
  @Input() isLocked = false;
  @Input() collapsedByDefault = false;
  @Input() margin = false; // sets the CSS class 'margin' (margin: 10px 0;)
  @Input() style: 'default' | 'plain' = 'default';
  @Input() confirmDeleteTemplate: TemplateRef<any>;

  @Output() deleteEvent = new EventEmitter();
  @Output() collapseEvent = new EventEmitter();
  @Output() expandEvent = new EventEmitter();

  @ViewChild('modalConfirmDelete', { static: true }) confirmDeleteModal: ModalComponent;

  deleted = false;

  delete() {
    this.confirmDeleteModal.open();
  }

  confirmDelete() {
    this.confirmDeleteModal.close();
    this.deleted = true;
  }

  finishDelete(event: AnimationEvent) {
    if ((event.toState as any) === true) {
      this.deleteEvent.emit();
    }
  }

  onCollapse() {
    this.collapsed = true;
    this.collapseEvent.emit();
  }

  onExpand() {
    this.collapsed = false;
    this.expandEvent.emit();
  }
}
