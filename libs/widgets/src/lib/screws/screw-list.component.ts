import {
  AfterViewInit,
  ChangeDetectorRef,
  Component,
  ContentChild,
  DoCheck,
  ElementRef,
  EventEmitter,
  HostBinding,
  HostListener,
  inject,
  Input,
  OnInit,
  Output,
  QueryList,
  TemplateRef,
  ViewChild,
  ViewChildren,
} from '@angular/core';
import { CommonModule } from '@angular/common';
import { intersection, some, sortBy } from 'lodash';
import { MatRipple, MatRippleModule } from '@angular/material/core';
import { MatIconModule } from '@angular/material/icon';
import { DragDropModule } from '@angular/cdk/drag-drop';

import { filter, map, Observable, pairwise, startWith, takeUntil } from 'rxjs';
import { Screw, ScrewConfiguration, ScrewConfigurationProvider } from '@entscheidungsnavi/decision-data';
import { OnDestroyObservable } from '@entscheidungsnavi/tools';
import { MatButtonModule } from '@angular/material/button';
import { FormsModule } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatSnackBar } from '@angular/material/snack-bar';
import { toPng } from 'html-to-image';
import { saveAs } from 'file-saver';
import { KeyBindHandlerDirective } from '../directives/keybind-handler.directive';
import {
  AutoFocusDirective,
  ClickOutsideDirective,
  OverflowDirective,
  ResizeObserverDirective,
  WidthTriggerDirective,
} from '../directives';
import { NoteBtnComponent, NoteBtnPresetPipe } from '../note-btn';
import { ToggleButtonComponent } from '../toggle-button/toggle-button.component';
import { FullscreenRef, FullscreenService } from '../services/fullscreen.service';
import { ImageExportSpinnerComponent } from '../spinners/image-export-spinner.component';

type StatePosition = number;

type StateConnection = {
  // Why an array and not separately for each connection. It is for optimization purposes:
  // With many alternatives that are going through the not defined states between screws A and B,
  // we only want to render one connection between A and B.
  configurationIndices: number[];
  start: number;
  end: number;
};

@Component({
  selector: 'dt-screw-list',
  templateUrl: './screw-list.component.html',
  styleUrls: ['./screw-list.component.scss'],
  standalone: true,
  imports: [
    CommonModule,
    AutoFocusDirective,
    ClickOutsideDirective,
    DragDropModule,
    ImageExportSpinnerComponent,
    ResizeObserverDirective,
    FormsModule,
    MatRippleModule,
    MatIconModule,
    NoteBtnComponent,
    OverflowDirective,
    NoteBtnPresetPipe,
    KeyBindHandlerDirective,
    MatButtonModule,
    MatTooltipModule,
    ToggleButtonComponent,
  ],
  hostDirectives: [KeyBindHandlerDirective, { directive: WidthTriggerDirective }],
})
export class ScrewListComponent implements OnInit, DoCheck, AfterViewInit {
  @ContentChild('noteBtnContent', { static: true })
  noteBtnContent: TemplateRef<any>;

  @HostBinding('class.is-dragging-screw')
  isDraggingScrew = false;

  @HostBinding('class.is-pressing-shift')
  isShiftPressed = false;

  @ViewChild('screwContainer') screwContainer: ElementRef<HTMLDivElement>;

  @ViewChild('configurationLabelContainer') configurationLabelsContainer: ElementRef<HTMLDivElement>;

  @ViewChild('configurationLock', { read: MatRipple }) ripple: MatRipple;

  @ViewChildren('configurationLabel') configurationLabels: QueryList<ElementRef<HTMLDivElement>>;

  @ViewChildren('screw') screwElements: QueryList<ElementRef<HTMLDivElement>>;

  @Input()
  screws: Screw[];

  @Input()
  // Dimension: #Configurations x #Screws
  // null => not defined
  screwConfigurationProviders: readonly ScrewConfigurationProvider[];

  @Input()
  canBlur = false;

  // when not editable it enables the dragging of screws/stages, hides empty fields and disables any changes to the data
  // used in the definition modal for verbal indicators
  @Input()
  showScrewsOnly = false;

  @Input()
  readonly = false;

  @Output()
  screwConfigurationChangeAtProvider = new EventEmitter<[configurationIndex: number, changedScrewConfiguration: ScrewConfiguration]>();

  @Input()
  newScrewConfiguration: ScrewConfiguration;

  @Output()
  newScrewConfigurationChange = new EventEmitter<ScrewConfiguration>();

  @Output()
  addNewAlternative = new EventEmitter<void>();

  selectedProvider = -1;

  @Output()
  appendScrew = new EventEmitter<void>();

  @Output()
  moveScrew = new EventEmitter<[number, number]>();

  @Output()
  editOrDeleteScrew = new EventEmitter<number>();

  @Output()
  dblClickedElement = new EventEmitter<[categoryName: string, htmlElement: HTMLElement]>();

  @HostBinding('class.small-screen')
  smallScreen = false;

  readonly connectorStripeWidth = 60;

  highlightedConfigurationIndices: number[] = [];

  ratioHiddenStateLabelsTop = 0;
  ratioHiddenStateLabelsBottom = 0;

  stateConnections: StateConnection[][];

  isSelectedConfigurationUnlocked = false;
  editedNameBoxIdx = -1; // keeps track of the alternative name being edited

  @HostBinding('class.exporting-image')
  exportingImage = false;

  readonly dtWidthTriggerDirective = inject<WidthTriggerDirective>(WidthTriggerDirective);

  @OnDestroyObservable()
  private onDestroy$: Observable<void>;

  fullscreenRef: FullscreenRef | null = null;

  @HostBinding('class.full-screen')
  get isInFullScreen() {
    return this.fullscreenRef != null;
  }

  constructor(
    private cdr: ChangeDetectorRef,
    private fullscreenService: FullscreenService,
    private snackbar: MatSnackBar,
    private dialog: MatDialog,
    private myElementRef: ElementRef<HTMLElement>,
    private keyBindHandler: KeyBindHandlerDirective,
  ) {}

  @HostListener('document:click', ['$event'])
  onClickOutside() {
    if (this.selectedProvider === -1 || !this.canBlur) return;
    if (this.editedNameBoxIdx === -1) {
      this.selectedProvider = -1; // unlock alternative
    } else {
      this.editedNameBoxIdx = -1; // unfocus alternative (stop editing)
    }
  }

  ngOnInit() {
    this.stateConnections = this.screws.map(() => []);
    this.setupKeyBindings();
    this.dtWidthTriggerDirective.dtWidthTrigger = { smallScreen: 400 };
    this.dtWidthTriggerDirective.dtWidthTriggerStateChange
      .asObservable()
      .pipe(takeUntil(this.onDestroy$))
      .subscribe(() => {
        this.smallScreen = this.dtWidthTriggerDirective.state['smallScreen'];
      });
  }

  ngAfterViewInit() {
    this.screwElements.changes
      .pipe(
        map(screws => screws.length),
        startWith(this.screwElements.length),
        pairwise(),
        filter(([oldScrewCount, newScrewCount]) => oldScrewCount < newScrewCount),
      )
      .subscribe(() => {
        this.screwElements.last.nativeElement.scrollIntoView({ behavior: 'smooth', block: 'nearest' });
      });

    this.configurationLabels.changes
      .pipe(
        map(configurationLabels => configurationLabels.length),
        startWith(this.configurationLabels.length),
        pairwise(),
        filter(([oldConfigurationLabelCount, newConfigurationLabelCount]) => oldConfigurationLabelCount < newConfigurationLabelCount),
      )
      .subscribe(([_, newConfigurationLabelCount]) => {
        this.selectConfiguration(newConfigurationLabelCount - 1);
        this.focusConfigurationText(newConfigurationLabelCount - 1);
        this.scrollConfigurationLabelIntoView(newConfigurationLabelCount - 1);
        // To avoid an ExpressionChangedAfterItHasBeenCheckedError.
        this.cdr.detectChanges();
      });
  }

  toggleFullScreen() {
    if (this.fullscreenRef) {
      this.fullscreenRef.close();
    } else {
      this.fullscreenRef = this.fullscreenService.fullscreenElement(this.myElementRef);

      this.fullscreenRef.onClose.subscribe(() => {
        this.fullscreenRef = null;
      });
    }
  }

  exportImage() {
    if (this.exportingImage) return;
    const dialog = this.dialog.open(ImageExportSpinnerComponent, { disableClose: true, hasBackdrop: true });
    this.exportingImage = true;
    setTimeout(() => {
      // timeout gives time for the 'exporting-image' to make changes
      toPng(this.screwContainer.nativeElement)
        .then(dataUrl => {
          saveAs(dataUrl, 'strategy-table.png');
        })
        .catch(error => {
          console.error(error);
          this.snackbar.open($localize`Fehler beim Export des Bildes, versuche einen anderen Browser zu verwenden.`, '', {
            duration: 5000,
          });
        })
        .finally(() => {
          this.exportingImage = false;
          dialog.close();
        });
    });
  }

  setupKeyBindings() {
    this.keyBindHandler.register({
      key: 'Escape',
      callback: () => {
        if (this.isSelectedConfigurationUnlocked) {
          this.toggleLockOfSelectedConfiguration();
        } else {
          if (this.selectedProvider === -1) {
            this.newScrewConfigurationChange.emit(this.newScrewConfiguration.map(() => null));
          } else {
            this.selectedProvider = -1;
            this.editedNameBoxIdx = -1;
          }
        }
      },
    });

    this.keyBindHandler.register({
      callbackOnBlur: true,
      callback: () => {
        this.isShiftPressed = false;
      },
      conditions: [event => event.key !== 'Shift'],
    });

    this.keyBindHandler.register({
      key: 'Shift',
      direction: 'down',
      onRepeat: false,
      callback: () => {
        if (this.isSelectedConfigurationUnlocked) {
          this.ripple.launch({ centered: true });
        } else {
          this.isShiftPressed = true;
        }
      },
    });

    this.keyBindHandler.register({
      key: 'Shift',
      direction: 'up',
      onRepeat: false,
      callbackOnBlur: true,
      callback: () => {
        this.isShiftPressed = false;
      },
    });
  }

  onClickConfiguration(configurationIndex: number) {
    if (this.selectedProvider !== configurationIndex) {
      // select new configuration
      this.selectConfiguration(configurationIndex);
    } else if (this.editedNameBoxIdx !== configurationIndex) {
      // focus the same configuration
      this.focusConfigurationText(configurationIndex);
    }
  }

  selectConfiguration(configurationIndex: number) {
    this.isSelectedConfigurationUnlocked = false;

    if (configurationIndex === -1 && this.selectedProvider !== -1) {
      this.newScrewConfigurationChange.emit(this.newScrewConfiguration.map(() => null));
    }

    this.selectedProvider = configurationIndex;
    this.editedNameBoxIdx = -1; // onfocus current namebox
  }

  focusConfigurationText(configurationIndex: number) {
    this.editedNameBoxIdx = configurationIndex;
  }

  toggleLockOfSelectedConfiguration() {
    if (this.isShiftPressed || this.readonly) return;
    this.isSelectedConfigurationUnlocked = !this.isSelectedConfigurationUnlocked;
  }

  ngDoCheck() {
    // Do not update while dragging because there is some DOM manipulation going on during the dnd process.
    if (this.isDraggingScrew) return;

    const statePositionCache = new Map<string, StatePosition>();
    this.stateConnections = this.screws
      .filter((_, screwIndex) => screwIndex < this.screws.length - 1)
      .map((_, screwIndex) => this.getForwardConnectionsOf(screwIndex, statePositionCache));
  }

  private getForwardConnectionsOf(screwIndex: number, statePositionCache: Map<string, StatePosition>): StateConnection[] | null {
    const getStatePositionOf = (screwIndex: number, stateIndex: number | null): StatePosition | null => {
      if (stateIndex == null) stateIndex = this.screws[screwIndex].states.length;

      const stateKey = [screwIndex, stateIndex].join(';');

      if (statePositionCache.has(stateKey)) {
        return statePositionCache.get(stateKey);
      }

      const stateElement = this.screwElements?.get(screwIndex)?.nativeElement.querySelectorAll<HTMLDivElement>('.state').item(stateIndex);
      if (!stateElement) return null;

      const statePosition = stateElement.offsetTop + stateElement.offsetHeight / 2;

      statePositionCache.set(stateKey, statePosition);

      return statePosition;
    };

    const forwardStateConnections: StateConnection[] = [];
    const statePairToConnection = new Map<string, number>();

    this.screwConfigurationProviders
      .map(screwConfigurationProvider => screwConfigurationProvider.screwConfiguration)
      .forEach((screwConfiguration, configurationIndex) => {
        const fromStateIndex = screwConfiguration[screwIndex];
        const toStateIndex = screwConfiguration[screwIndex + 1];

        const fromStatePosition = getStatePositionOf(screwIndex, fromStateIndex);
        const toStatePosition = getStatePositionOf(screwIndex + 1, toStateIndex);

        if (!(fromStatePosition && toStatePosition)) return;

        const stateConnectionKey = [fromStateIndex, toStateIndex].join('-');

        if (statePairToConnection.has(stateConnectionKey)) {
          forwardStateConnections[statePairToConnection.get(stateConnectionKey)].configurationIndices.push(configurationIndex);
        } else {
          forwardStateConnections.push({
            configurationIndices: [configurationIndex],
            start: fromStatePosition,
            end: toStatePosition,
          });

          statePairToConnection.set(stateConnectionKey, forwardStateConnections.length - 1);
        }
      });

    return forwardStateConnections;
  }

  clickState(screwIndex: number, stateIndex: number | null, event: MouseEvent) {
    if (this.readonly) {
      return;
    }
    let newScrewConfiguration: ScrewConfiguration;

    if (this.selectedProvider === -1) {
      newScrewConfiguration = this.newScrewConfiguration;
    } else {
      newScrewConfiguration = [...this.screwConfigurationProviders[this.selectedProvider].screwConfiguration];
    }

    newScrewConfiguration[screwIndex] = stateIndex;

    const handleConfigurationChange = (force: boolean) => {
      if (this.selectedProvider !== -1) {
        if (force || this.isSelectedConfigurationUnlocked) {
          this.screwConfigurationChangeAtProvider.emit([this.selectedProvider, newScrewConfiguration]);
        } else {
          this.ripple.launch({ centered: true });
        }
      } else {
        this.newScrewConfigurationChange.emit(newScrewConfiguration);
      }
    };

    if (event.detail === 1) {
      handleConfigurationChange(this.isShiftPressed);
    } else if (event.detail === 2) {
      handleConfigurationChange(true);
    }

    event.stopPropagation();
  }

  toggleHighlightOfConfigurationsGoingThroughState(screwIndex: number, stateIndex: number) {
    const configurationsGoingThroughState = this.screwConfigurationProviders
      .map((configurationProvider, configurationIndex) => [configurationProvider.screwConfiguration, configurationIndex] as const)
      .filter(([configuration, _]) => configuration[screwIndex] === stateIndex)
      .map(([_, configurationIndex]) => configurationIndex);

    if (
      configurationsGoingThroughState.length === 0 ||
      intersection(configurationsGoingThroughState, this.highlightedConfigurationIndices).length > 0
    ) {
      this.disableHighlightOnConfigurations();
    } else {
      this.enableHighlightOnConfigurations(configurationsGoingThroughState);
    }
  }

  enableHighlightOnConfigurations(configurationIndices: number[]) {
    if (this.showScrewsOnly) return;
    this.highlightedConfigurationIndices = configurationIndices;
    this.updateHiddenStateLabelRatios();
  }

  updateHiddenStateLabelRatios() {
    const configurationLabelsContainer = this.configurationLabelsContainer.nativeElement;
    const configurationLabelContainerTop = configurationLabelsContainer.scrollTop;
    const configurationLabelContainerBottom = configurationLabelsContainer.scrollTop + configurationLabelsContainer.offsetHeight;

    this.ratioHiddenStateLabelsTop = 0;
    this.ratioHiddenStateLabelsBottom = 0;

    this.configurationLabels.forEach((configurationLabel, configurationIndex) => {
      if (!this.highlightedConfigurationIndices.includes(configurationIndex)) return;
      const configurationLabelTop = configurationLabel.nativeElement.offsetTop;
      const configurationLabelBottom = configurationLabelTop + configurationLabel.nativeElement.offsetHeight;

      if (configurationLabelTop <= configurationLabelContainerTop) {
        this.ratioHiddenStateLabelsTop += Math.min(configurationLabelBottom, configurationLabelContainerTop) - configurationLabelTop;
      } else if (configurationLabelBottom >= configurationLabelContainerBottom) {
        this.ratioHiddenStateLabelsBottom += configurationLabelBottom - Math.max(configurationLabelTop, configurationLabelContainerBottom);
      }
    });

    const sumOfHiddenLengths = this.ratioHiddenStateLabelsTop + this.ratioHiddenStateLabelsBottom;

    if (sumOfHiddenLengths !== 0) {
      this.ratioHiddenStateLabelsTop = this.ratioHiddenStateLabelsTop / sumOfHiddenLengths;
      this.ratioHiddenStateLabelsBottom = this.ratioHiddenStateLabelsBottom / sumOfHiddenLengths;
    } else {
      // this.ratioHiddenStateLabelsTop = this.ratioHiddenStateLabelsBottom = 0 already holds.
    }
  }

  getHiddenStatesIndicatorColor(ratioHiddenStateLabels: number) {
    return `2px solid rgba(248, 147, 30, ${ratioHiddenStateLabels})`;
  }

  disableHighlightOnConfigurations() {
    this.highlightedConfigurationIndices = [];
    this.ratioHiddenStateLabelsTop = this.ratioHiddenStateLabelsBottom = 0;
  }

  scrollConfigurationLabelIntoView(configurationIndex: number) {
    if (configurationIndex === -1) return;

    if (configurationIndex === this.screwConfigurationProviders.length - 1) {
      // scrollIntoView doesn't work properly when the element is already (slightly) visible
      // so we snap to the bottom of the list if it's the last element
      this.configurationLabelsContainer.nativeElement.scrollTo(0, this.configurationLabelsContainer.nativeElement.scrollHeight);
    } else {
      this.configurationLabels.get(configurationIndex).nativeElement.scrollIntoView({
        behavior: 'smooth',
        block: 'nearest',
      });
    }
  }

  isStateHighlighted(screwIndex: number, stateIndex: number) {
    return some(
      this.highlightedConfigurationIndices.map(
        highlightedConfigurationIndex =>
          this.screwConfigurationProviders[highlightedConfigurationIndex].screwConfiguration[screwIndex] === stateIndex,
      ),
    );
  }

  isStateSelected(screwIndex: number, stateIndex: number) {
    return (
      (this.selectedProvider === -1
        ? this.newScrewConfiguration
        : this.screwConfigurationProviders[this.selectedProvider].screwConfiguration)[screwIndex] === stateIndex
    );
  }

  leftRightConnectorToPathDefinition(leftRightConnector: StateConnection) {
    const middleX = this.connectorStripeWidth / 2;
    return `M0 ${leftRightConnector.start} C${middleX} ${leftRightConnector.start},
     ${middleX} ${leftRightConnector.end},
     ${this.connectorStripeWidth} ${leftRightConnector.end}
    `;
  }

  sortStateConnectionsByLevelOfHighlight(stateConnections: StateConnection[]) {
    return sortBy(stateConnections, (stateConnection: StateConnection) => {
      if (this.isStateConnectionSelected(stateConnection)) return 2;
      if (this.isStateConnectionHighlighted(stateConnection)) return 1;
      return 0;
    });
  }

  trackStateConnectionsByEndpoints(_: number, screwStateConnection: StateConnection) {
    return `${screwStateConnection.start};${screwStateConnection.end}`;
  }

  isStateConnectionHighlighted(stateConnection: StateConnection) {
    return intersection(this.highlightedConfigurationIndices, stateConnection.configurationIndices).length > 0;
  }

  isStateConnectionSelected(stateConnection: StateConnection) {
    return stateConnection.configurationIndices.includes(this.selectedProvider);
  }

  // enables dragging of states as text (for non-editable screws)
  dragStart(event: DragEvent, text: string) {
    event.dataTransfer.dropEffect = 'move';
    event.dataTransfer.setData('text', text);
  }

  protected addAlternative() {
    this.addNewAlternative.emit();
  }

  protected shufflePositions() {
    if (this.selectedProvider !== -1) return;
    this.newScrewConfigurationChange.emit(this.screws.map(screw => Math.floor(Math.random() * screw.states.length)));
  }
}
