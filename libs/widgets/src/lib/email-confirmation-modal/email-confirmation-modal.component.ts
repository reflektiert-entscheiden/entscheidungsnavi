import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { UntypedFormControl, UntypedFormGroup, Validators } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { OnDestroyObservable } from '@entscheidungsnavi/tools';
import { Observable, takeUntil } from 'rxjs';
import { AuthService } from '@entscheidungsnavi/api-client';
import { SnackbarComponent, SnackbarData } from '../snackbar';

@Component({
  templateUrl: './email-confirmation-modal.component.html',
  styleUrls: ['./email-confirmation-modal.component.scss'],
})
export class EmailConfirmationModalComponent implements OnInit {
  static isOpen = false;

  confirmForm: UntypedFormGroup;

  @OnDestroyObservable()
  private onDestroy$: Observable<void>;

  constructor(
    private authService: AuthService,
    private dialogRef: MatDialogRef<EmailConfirmationModalComponent>,
    private snackBar: MatSnackBar,
  ) {
    EmailConfirmationModalComponent.isOpen = true;
    dialogRef.disableClose = true;
    dialogRef.afterClosed().subscribe(() => (EmailConfirmationModalComponent.isOpen = false));

    this.authService.user$.pipe(takeUntil(this.onDestroy$)).subscribe(user => {
      if (user == null || user.emailConfirmed) {
        this.dialogRef.close();
      }
    });
  }

  ngOnInit() {
    this.confirmForm = new UntypedFormGroup({
      token: new UntypedFormControl('', [Validators.required, Validators.pattern(/^\d{6}$/)]),
    });
  }

  onSubmit() {
    if (!this.confirmForm.valid) {
      return;
    }

    this.confirmForm.disable({ emitEvent: false });
    this.authService.confirmEmail(this.confirmForm.get('token').value).subscribe({
      next: () => {
        this.snackBar.open($localize`E-Mail bestätigt!`, undefined, { duration: 4000 });
      },
      error: (error: HttpErrorResponse) => {
        this.confirmForm.enable({ emitEvent: false });
        if (error.status === 404) {
          this.confirmForm.controls.token.setErrors({ invalidToken: true });
        } else {
          this.confirmForm.setErrors({ serverError: true });
        }
      },
    });
  }

  requestNewCode() {
    if (!this.confirmForm.enabled) {
      return;
    }

    this.confirmForm.disable({ emitEvent: false });
    this.authService.requestEmailConfirmation().subscribe({
      next: () =>
        this.snackBar.openFromComponent(SnackbarComponent, {
          data: { message: $localize`Verifizierungscode erfolgreich angefordert!` } as SnackbarData,
          duration: 4000,
        }),
      error: () =>
        this.snackBar.openFromComponent(SnackbarComponent, {
          data: {
            message: $localize`Verifizierungscode konnte nicht angefordert werden. Versuche es später noch einmal.`,
            icon: 'error',
          } as SnackbarData,
          duration: 8000,
        }),
      complete: () => this.confirmForm.enable({ emitEvent: false }),
    });
  }

  skip() {
    this.dialogRef.close();
  }

  logout() {
    this.authService.logout().subscribe();
  }
}
