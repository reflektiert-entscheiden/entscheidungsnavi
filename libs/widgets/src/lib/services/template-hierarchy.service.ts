import { Inject, Injectable, LOCALE_ID } from '@angular/core';
import { QuickstartHierarchyNode } from '@entscheidungsnavi/api-types';
import { ITree, Tree } from '@entscheidungsnavi/tools';
import { SimpleHierarchyNode } from '../simple-hierarchy/simple-hierarchy-node';
import { localizeString } from '../pipes';

export interface TemplateHierarchyElement extends SimpleHierarchyNode {
  id: string;
}

@Injectable({ providedIn: 'root' })
export class TemplateHierarchyService {
  constructor(@Inject(LOCALE_ID) private locale: string) {}

  extractHierarchy(tree: ITree<QuickstartHierarchyNode>): Tree<TemplateHierarchyElement> {
    return this._extractHierarchy(tree, 0);
  }

  private _extractHierarchy(tree: ITree<QuickstartHierarchyNode>, level: number): Tree<TemplateHierarchyElement> {
    return new Tree(
      {
        id: tree.value.id,
        name: localizeString(tree.value.name, this.locale),
        comment: localizeString(tree.value.comment, this.locale),
        scaleComment: localizeString(tree.value.scaleComment, this.locale),
        style: this.getStyle(level),
      },
      tree.children.map(child => this._extractHierarchy(child, level + 1)),
    );
  }

  private getStyle(level: number): { [p: string]: any } {
    switch (level) {
      case 0:
        return {
          'background-color': '#d3d3d3',
          border: '1px solid black',
          'min-height.px': 40,
        };
      case 1:
        return {
          'background-color': '#5d666f',
          color: '#fff',
          'min-height.px': 40,
          'font-weight': 500,
        };
      default:
        return {};
    }
  }
}
