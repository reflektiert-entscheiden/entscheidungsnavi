import { Injectable } from '@angular/core';

@Injectable({ providedIn: 'root' })
export class TextWidthService {
  private canvas: HTMLCanvasElement;
  private drawingContext: CanvasRenderingContext2D;

  private document: Document;

  constructor() {
    this.document = document;

    this.initCanvas();
  }

  private initCanvas() {
    this.canvas = this.document.createElement('canvas');
    this.drawingContext = this.canvas.getContext('2d');
  }

  public getTextWidth(text: string, font: string) {
    this.drawingContext.font = font;
    return Math.floor(this.drawingContext.measureText(text).width);
  }

  public getComputedFont(computedStyle: CSSStyleDeclaration) {
    const allInOne = computedStyle.getPropertyValue('font');

    if (allInOne) {
      return allInOne;
    } else {
      // Probably in Firefox here, have to stitch the font together from the seperate properties
      const fontStyle = computedStyle.getPropertyValue('font-style');
      const fontVariant = computedStyle.getPropertyValue('font-variant');
      const fontWeight = computedStyle.getPropertyValue('font-weight');
      const fontSize = computedStyle.getPropertyValue('font-size');
      const fontFamily = computedStyle.getPropertyValue('font-family');

      const resultFont = (fontStyle + ' ' + fontVariant + ' ' + fontWeight + ' ' + fontSize + ' ' + fontFamily).replace(/ +/g, ' ').trim();

      return resultFont;
    }
  }
}
