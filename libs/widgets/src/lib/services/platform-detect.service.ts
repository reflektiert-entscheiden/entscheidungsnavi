import { Platform } from '@angular/cdk/platform';
import { Injectable } from '@angular/core';

@Injectable({ providedIn: 'root' })
export class PlatformDetectService {
  readonly macOS = !!navigator?.userAgent?.includes('Macintosh');
  // eslint-disable-next-line deprecation/deprecation
  readonly iPadOS = navigator.platform === 'MacIntel' && navigator.maxTouchPoints > 1;

  readonly isMobile = this.platform.ANDROID || this.platform.IOS || this.iPadOS;

  constructor(private platform: Platform) {}
}
