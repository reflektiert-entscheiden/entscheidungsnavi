import { Pipe, PipeTransform } from '@angular/core';
import { assertUnreachable } from '@entscheidungsnavi/tools/assert-unreachable';
import { MultiCaption, NoteBtnCaption } from './note-btn.component';

type StringPresets =
  | 'alternative'
  | 'screw'
  | 'objective'
  | 'objectiveWeight'
  | 'utilityFunction'
  | 'influenceFactor'
  | 'verbalOption'
  | 'indicator'
  | 'step';

@Pipe({ name: 'noteBtnPreset', standalone: true })
export class NoteBtnPresetPipe implements PipeTransform {
  transform(value: { alternative: string; objective: string }, type: 'outcome'): MultiCaption;
  transform(value: string, type: 'objective' | 'objectiveWeight' | 'utilityFunction' | 'objectiveWeight'): MultiCaption;
  transform(value: string, type: StringPresets): string;
  transform(value: string | { alternative: string; objective: string }, type: StringPresets | 'outcome'): NoteBtnCaption {
    if (typeof value === 'object' || type === 'outcome') {
      if (type !== 'outcome') {
        throw new Error('Invalid type for object value');
      }

      if (typeof value !== 'object') {
        throw new Error('Invalid value for outcome type');
      }

      return {
        title: $localize`Erläuterung zu einer Wirkungsprognose`,
        subTitle: $localize`Ziel „${value.objective}“, Alternative „${value.alternative}“`,
        secondSubTitle: '',
      };
    }

    switch (type) {
      case 'alternative':
        return $localize`Erläuterung zur Alternative „${value}“`;
      case 'screw':
        return $localize`Erläuterung zum Stellhebel „${value}“`;
      case 'objective':
        return {
          title: $localize`Ziel „${value}“`,
          subTitle: $localize`Definition des Ziels`,
          secondSubTitle: $localize`Beschreibung des Messmodells`,
        };
      case 'objectiveWeight':
        return {
          title: $localize`Ziel „${value}“`,
          subTitle: $localize`Definition des Ziels`,
          secondSubTitle: $localize`Erläuterung zum Gewicht`,
        };
      case 'utilityFunction':
        return {
          title: $localize`Ziel „${value}“`,
          subTitle: $localize`Definition des Ziels`,
          secondSubTitle: $localize`Erläuterung zur Nutzenfunktion`,
        };
      case 'influenceFactor':
        return $localize`Erläuterung zum Einflussfaktor „${value}“`;
      case 'verbalOption':
        return $localize`Erläuterung der Ausprägung „${value}“`;
      case 'indicator':
        return $localize`Erläuterung zum Indikator „${value}“`;
      case 'step':
        return $localize`Notizen zum Schritt „${value}“`;
      default:
        assertUnreachable(type);
    }
  }
}
