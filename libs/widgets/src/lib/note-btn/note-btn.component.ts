import { Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { MatExpansionModule } from '@angular/material/expansion';

import { RichTextEditorComponent } from '../rich-text-editor';
import { ModalModule } from '../modal/modal.module';
import { HoverPopOverDirective } from '../popover';
import { ModalComponent } from '../modal';
import { RichTextEmptyPipe } from '../pipes';

/*
  Usage:
  <div style="position: relative"> <!-- dt-note-btn will position itself relative to this container -->
    <input
      [(ngModel)]="title"
    />
    <dt-note-btn [(comment)]="comment" [caption]="{ title: title, subtitle: 'Static subtitle' }"></dt-note-btn>
  </div>

 Optionally, a custom caption can be provided (default title is "Notiz"/"Note"):
  <dt-note-btn [caption]="'My caption'" i18n-caption></dt-note-btn>
  <!-- or -->
  <dt-note-btn [caption]="{ title: 'abc', subtitle: 'def' }"></dt-note-btn>
  <!-- or -->
  <dt-note-btn [caption]="objective.name | noteBtnPreset: 'objective'"></dt-note-btn>
 */

export type MultiCaption = { title: string; subTitle: string; secondSubTitle: string };
export type NoteBtnCaption = string | MultiCaption;

@Component({
  selector: 'dt-note-btn',
  templateUrl: './note-btn.component.html',
  styleUrls: ['./note-btn.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      multi: true,
      useExisting: NoteBtnComponent,
    },
  ],
  standalone: true,
  imports: [RichTextEditorComponent, ModalModule, HoverPopOverDirective, RichTextEmptyPipe, MatExpansionModule],
})
export class NoteBtnComponent implements OnInit, ControlValueAccessor {
  @Input() twoComments = false;
  @Input() comment: string;
  @Input() secondComment: string;
  @Input() firstOpen = true;
  @Input() secondOpen = false;
  @Input() caption?: NoteBtnCaption;
  @Input() disabled = false; // Disabled opening of the modal via the button
  @Input() readonly = false; // Disabled changing of the note
  @Input() triangle = true; // Whether the triangle should be visible
  /**
   * Set to true whenever the button is placed on a dark background. Otherwise it may be invisible
   * when empty.
   */
  @Input() darkBackground = false;

  @Output() commentChange = new EventEmitter<string>();
  @Output() secondCommentChange = new EventEmitter<string>();
  @Output() noteClose = new EventEmitter<{ comment: string; secondComment: string }>();
  @Output() noteOpen = new EventEmitter();
  @Output() noteModalClick = new EventEmitter();

  @ViewChild('modalLargeCommentField', { static: true }) modalLargeCommentField: ModalComponent;

  isOpen = false;

  // Required for ControlValueAccessor
  onChange: (comment: string) => void;
  onTouched: () => void;
  touched = false;

  // Set by dt-team-comments
  hasTeamComments = false;

  get mainTitle(): string {
    if (this.caption == null) {
      return null;
    } else if (typeof this.caption === 'string') {
      return this.caption;
    } else {
      return this.caption.title;
    }
  }

  get modalCaption(): string {
    if (!this.twoComments) {
      if (this.caption == null || typeof this.caption === 'string') {
        return null;
      } else {
        return this.caption.subTitle;
      }
    }
  }

  get modalSubTitle(): string {
    if (this.caption == null) {
      return null;
    } else if (typeof this.caption === 'string') {
      return this.caption;
    } else {
      return this.caption.subTitle;
    }
  }

  get modalSecondSubTitle(): string {
    if (this.caption == null) {
      return null;
    } else if (typeof this.caption === 'string') {
      return this.caption;
    } else {
      return this.caption.secondSubTitle;
    }
  }

  ngOnInit() {
    if (this.comment === undefined) {
      this.comment = '';
    }
    if (this.secondComment === undefined) {
      this.secondComment = '';
    }
  }

  onTriangleClick(event: MouseEvent) {
    event.stopPropagation();
    if (!this.disabled) this.open();
  }

  open() {
    if (this.isOpen) return;

    this.modalLargeCommentField.open();
    this.isOpen = true;
    this.noteOpen.emit();
  }

  close() {
    this.isOpen = false;
    this.noteClose.emit({ comment: this.comment, secondComment: this.secondComment });
  }

  onCommentChange() {
    this.cleanIfEmpty();
    this.commentChange.emit(this.comment);
    this.secondCommentChange.emit(this.secondComment);
    this.markAsTouched();
    this.onChange?.(this.comment);
  }

  cleanIfEmpty() {
    const clean = (comment: string) => {
      if (
        comment.split('insert').length - 1 == 1 &&
        comment.slice(this.comment.lastIndexOf('insert'), this.comment.length) === `insert":"\\n"}]}`
      ) {
        comment = `{"ops":[{"insert":"\\n"}]}`;
      }
      return comment;
    };
    // if only 1 insert && insert empty (if rich text is completely empty)
    this.comment = clean(this.comment);
    this.secondComment = clean(this.secondComment);
  }

  /////////////////////////////////////////////////////////////////
  // ControlValueAccessor implementation
  /////////////////////////////////////////////////////////////////

  writeValue(comment: string) {
    this.comment = comment;
  }

  registerOnChange(onChange: (comment: string) => void) {
    this.onChange = onChange;
  }

  registerOnTouched(onTouched: () => void) {
    this.onTouched = onTouched;
  }

  setDisabledState(disabled: boolean) {
    // Disabled in the form means that the value can not be changed. This corresponds to readonly
    // in this component.
    this.readonly = disabled;
  }

  private markAsTouched() {
    if (!this.touched) {
      this.onTouched?.();
      this.touched = true;
    }
  }
}
