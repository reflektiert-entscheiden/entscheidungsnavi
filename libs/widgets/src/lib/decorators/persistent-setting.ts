import { debounce } from 'lodash';
import { Subject } from 'rxjs';

const PARENT_KEY = Symbol();

const resetSessionSettingsSubject = new Subject<void>();

/**
 * This decorator must be used on any class that is meant to contain the {@link PersistentSetting}
 * decorator. The given name must be unique.
 */
export function PersistentSettingParent(name: string) {
  return function (target: unknown) {
    Object.defineProperty(target, PARENT_KEY, { value: name, writable: false });
  };
}

export function resetSessionSettings() {
  resetSessionSettingsSubject.next();
}

/**
 * This decorator can be used on properties of objects which should be persisted.
 * It also syncs values across multiple instances of the same class.
 *
 * @remarks
 * Properties with this decorator must be initialized with a default value. Otherwise,
 * the first write to the property will be ignored.
 *
 * The {@link PersistentSettingParent} decorator must be used on the containing class.
 *
 * @example
 * A setting which should be persisted indefinitely in local storage:
 * ```
 *   @PersistentSetting()
 *   colorOutcomes = false;
 * ```
 */
export function PersistentSetting(lifetime: 'indefinite' | 'project' = 'indefinite'): PropertyDecorator {
  const storage = lifetime === 'project' ? sessionStorage : localStorage;

  let currentValue: unknown;

  const firstSetOnInstanceProperty = Symbol();

  return function (target: any, propertyKey: string) {
    const storageKey = () => {
      const parentName = target.constructor[PARENT_KEY];
      if (!parentName) {
        throw new Error(`PersistentSettingParent must be used on the class ${target.constructor.name}`);
      }
      if (lifetime === 'project') {
        return `dt-persistent-lifetime_project-${parentName}-${propertyKey}`;
      }
      return `dt-persistent-${parentName}-${propertyKey}`;
    };
    const saveInStorage = debounce(
      (toSave: unknown) => {
        try {
          storage.setItem(storageKey(), JSON.stringify(toSave));
        } catch (e) {
          // NOOP
        }
      },
      1000,
      { maxWait: 2000 },
    );
    const deleteFromStorage = () => {
      try {
        if (storage.getItem(storageKey()) === null) {
          return;
        }
        storage.removeItem(storageKey());
      } catch (e) {
        // NOOP
      }
    };

    if (lifetime == 'project') {
      resetSessionSettingsSubject.subscribe(() => {
        deleteFromStorage();
      });
    }

    let firstSetOnClass = true;
    Object.defineProperty(target, firstSetOnInstanceProperty, { value: true, writable: true });

    Object.defineProperty(target, propertyKey, {
      get: () => {
        return currentValue;
      },
      set: function (newValue: unknown) {
        if (this[firstSetOnInstanceProperty]) {
          this[firstSetOnInstanceProperty] = false;

          if (firstSetOnClass) {
            firstSetOnClass = false;

            const storageValue = storage.getItem(storageKey());
            if (storageValue !== null) {
              try {
                newValue = JSON.parse(storageValue);
              } catch {
                console.error(`failed to deserialize persistent setting: ${storageKey()}`);
              }
            }

            currentValue = newValue;
          }
        } else {
          saveInStorage(newValue);
          currentValue = newValue;
        }
      },
    });
  };
}
