import { Component } from '@angular/core';

@Component({
  selector: 'dt-scroll-shadow-inner',
  template: `<ng-content></ng-content>`,
  styles: [
    `
      :host {
        display: block;
      }
    `,
  ],
  standalone: true,
})
export class ScrollShadowInnerComponent {}
