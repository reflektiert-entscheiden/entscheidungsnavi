import {
  AfterViewInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ContentChild,
  ElementRef,
  OnDestroy,
  ViewChild,
} from '@angular/core';

import { isEqual } from 'lodash';
import { ScrollShadowInnerComponent } from './scroll-shadow-inner.component';

@Component({
  selector: 'dt-scroll-shadow',
  templateUrl: './scroll-shadow.component.html',
  styleUrls: ['./scroll-shadow.component.scss'],
  standalone: true,
  changeDetection: ChangeDetectionStrategy.OnPush,
  imports: [],
})
export class ScrollShadowComponent implements AfterViewInit, OnDestroy {
  private mutationObserver: MutationObserver;
  private resizeObserver: ResizeObserver;

  private oldChildren: Element[];

  @ViewChild('scrollContainer') scrollContainer: ElementRef<HTMLElement>;
  @ContentChild(ScrollShadowInnerComponent, { read: ElementRef }) innerContainer: ElementRef<HTMLElement>;

  showShadow = {
    top: false,
    bottom: false,
    left: false,
    right: false,
  };

  constructor(private cdRef: ChangeDetectorRef) {}

  ngAfterViewInit() {
    this.resizeObserver = new ResizeObserver(() => this.updateScrollProperties());
    this.mutationObserver = new MutationObserver(() => {
      this.subscribeToChildrenResize();
      this.updateScrollProperties();
    });

    this.mutationObserver.observe(this.innerContainer.nativeElement, { childList: true });
    this.subscribeToChildrenResize();

    this.updateScrollProperties();
  }

  private subscribeToChildrenResize() {
    this.oldChildren?.forEach(child => {
      this.resizeObserver.unobserve(child);
    });

    this.oldChildren = [];
    const children = this.innerContainer.nativeElement.children;
    for (let i = 0; i < children.length; i++) {
      this.resizeObserver.observe(children[i]);
      this.oldChildren.push(children[i]);
    }
  }

  ngOnDestroy() {
    this.resizeObserver.disconnect();
    this.mutationObserver.disconnect();
  }

  updateScrollProperties() {
    const scrollTop = this.scrollContainer.nativeElement.scrollTop;
    const scrollLeft = this.scrollContainer.nativeElement.scrollLeft;
    const offsetHeight = this.scrollContainer.nativeElement.offsetHeight;
    const offsetWidth = this.scrollContainer.nativeElement.offsetWidth;
    const scrollHeight = this.scrollContainer.nativeElement.scrollHeight;
    const scrollWidth = this.scrollContainer.nativeElement.scrollWidth;

    // scrollLeft/scrollTop yield floating point numbers on some devices
    // which means we never fully scroll to the right/bottom.
    // To mitigate that, we add a buffer of 1 pixel.
    const showShadow = {
      top: scrollTop > 0,
      bottom: scrollHeight > offsetHeight + scrollTop + 1,
      left: scrollLeft > 0,
      right: scrollWidth > offsetWidth + scrollLeft + 1,
    };

    if (!isEqual(showShadow, this.showShadow)) {
      this.showShadow = showShadow;
      this.cdRef.detectChanges();
    }
  }
}
