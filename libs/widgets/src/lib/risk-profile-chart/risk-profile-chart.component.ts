import { CommonModule } from '@angular/common';
import { ChangeDetectionStrategy, Component, ContentChild, Input, TemplateRef } from '@angular/core';
import { MatTooltipModule } from '@angular/material/tooltip';
import { pairwiseIterable } from '@entscheidungsnavi/tools';
import { DiagramLegendComponent } from '../diagram-legend';
import { HoverPopOverDirective } from '../popover';

export interface Point {
  x: number;
  y: number;
}

export interface RiskProfileChartSeries {
  seriesLabel: string;
  dataPoints: Point[];
}

@Component({
  selector: 'dt-risk-profile-chart',
  templateUrl: './risk-profile-chart.component.html',
  styleUrls: ['./risk-profile-chart.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  standalone: true,
  imports: [CommonModule, MatTooltipModule, DiagramLegendComponent, HoverPopOverDirective],
})
export class RiskProfileChartComponent {
  @Input() numberOfHorizontalLines = 11;

  @Input() xCaption: string;
  @Input() yCaption: string;

  // Equidistant labels on the x axis
  @Input() xLabels: string[];

  @ContentChild('tooltipTemplate') tooltipTemplate: TemplateRef<{ seriesIndex: number; pointIndex: number }>;

  @Input() dataSeries: RiskProfileChartSeries[];

  readonly lineColors = [
    'hsl(208deg 35% 40%)',
    'hsl(0deg 50% 40%)',
    'hsl(28deg 75% 45%)',
    'hsl(448deg 35% 40%)',
    'hsl(268deg 35% 40%)',
    'hsl(174 50% 40%)',
  ];

  highlightedSeriesIndex = -1;

  get seriesLabels() {
    return this.dataSeries.map(series => series.seriesLabel);
  }

  pairwise(input: Iterable<Point>): Iterable<[Point, Point]> {
    return pairwiseIterable(input);
  }
}
