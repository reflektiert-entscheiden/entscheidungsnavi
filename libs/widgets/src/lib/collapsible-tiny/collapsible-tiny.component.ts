import { Component, ContentChild, EventEmitter, HostBinding, Input, Output, TemplateRef } from '@angular/core';
import { MatDividerModule } from '@angular/material/divider';
import { CommonModule } from '@angular/common';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';

@Component({
  selector: 'dt-collapsible-tiny',
  templateUrl: './collapsible-tiny.component.html',
  styleUrls: ['./collapsible-tiny.component.scss'],
  standalone: true,
  imports: [CommonModule, MatDividerModule, MatIconModule, MatButtonModule],
})
export class CollapsibleTinyComponent {
  @ContentChild('header') headerTemplateRef: TemplateRef<{ $implicit: boolean }>;
  @ContentChild('content') contentTemplateRef: TemplateRef<unknown>;

  @HostBinding('class.open')
  @Input()
  open = true;

  @Output()
  openChange = new EventEmitter<boolean>();
}
