import { Component, EventEmitter, Input, Output } from '@angular/core';

import { CollapsibleTinyComponent } from '../collapsible-tiny/collapsible-tiny.component';

@Component({
  selector: 'dt-diagram-legend',
  templateUrl: './diagram-legend.component.html',
  styleUrls: ['./diagram-legend.component.scss'],
  standalone: true,
  imports: [CollapsibleTinyComponent],
})
export class DiagramLegendComponent {
  /* A list of colors that is by default repeated if the list elements are more than the colors. */
  @Input() itemColors: string[];

  // List of items that are displayed in the legend.
  @Input() items: string[];

  @Output() highlightedItemChange = new EventEmitter<number>();

  @Input() selectedItem = -1;
  @Output() selectedItemChange = new EventEmitter<number>();

  highlightedItem = -1;

  highlightItem(itemIndex: number) {
    this.highlightedItem = itemIndex;
    this.highlightedItemChange.emit(itemIndex);
  }
  getItemColor(itemIndex: number) {
    return this.itemColors[itemIndex % this.itemColors.length];
  }
}
