import { Injectable } from '@angular/core';
import { EventManager } from '@angular/platform-browser';

// eslint-disable-next-line max-len
// TODO This is a duplicate of the definition in https://github.com/angular/angular/blob/fb10abcf3ed66866c05d1de3303480f345c974b7/packages/platform-browser/src/dom/events/event_manager.ts#L85
// because it is not in the public API yet.
// Remove once https://github.com/angular/angular/commit/c5daa6ce776724d44c02cc97f1a349a85cb2a819 is released (Angular 17).
// eslint-disable-next-line @typescript-eslint/naming-convention
abstract class EventManagerPlugin {
  // Using non-null assertion because it's set by EventManager's constructor
  manager!: EventManager;

  /**
   * Should return `true` for every event name that should be supported by this plugin
   */
  abstract supports(eventName: string): boolean;

  /**
   * Implement the behaviour for the supported events
   */
  // eslint-disable-next-line @typescript-eslint/ban-types
  abstract addEventListener(element: HTMLElement, eventName: string, handler: Function): Function;
}

/**
 * This plugin enables the use of native Angular events outside the Angular Zone.
 *
 * @example
 * `(click.outside-zone)="function()"`
 *
 * @example
 * `@HostListener('click.outside-zone')`
 */
@Injectable()
export class OutsideZoneEventManagerPlugin extends EventManagerPlugin {
  readonly modifier = 'outside-zone';

  override supports(eventName: string): boolean {
    return eventName.split('.').includes(this.modifier);
  }

  // eslint-disable-next-line @typescript-eslint/ban-types
  override addEventListener(element: HTMLElement, eventName: string, handler: Function): Function {
    return this.manager.getZone().runOutsideAngular(() =>
      this.manager.addEventListener(
        element,
        eventName
          .split('.')
          .filter(part => part !== this.modifier)
          .join('.'),
        handler,
      ),
    );
  }
}
