import { Component, ElementRef, EventEmitter, Inject, Input, LOCALE_ID, Output, ViewChild } from '@angular/core';
import { QuickstartTagDto } from '@entscheidungsnavi/api-client';
import { COMMA, ENTER } from '@angular/cdk/keycodes';
import { ControlValueAccessor, NG_VALUE_ACCESSOR, ReactiveFormsModule, UntypedFormControl } from '@angular/forms';
import { map, merge, Observable, startWith, Subject } from 'rxjs';
import { MatChipInputEvent, MatChipsModule } from '@angular/material/chips';
import { MatAutocompleteModule, MatAutocompleteSelectedEvent } from '@angular/material/autocomplete';

import { MatIconModule } from '@angular/material/icon';
import { MatFormFieldModule } from '@angular/material/form-field';
import { AsyncPipe } from '@angular/common';
import { LocalizedStringPipe } from '../pipes';

export type Tag = QuickstartTagDto & { isProductive?: boolean };

@Component({
  selector: 'dt-tag-input',
  templateUrl: './tag-input.component.html',
  styleUrls: ['./tag-input.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      multi: true,
      useExisting: TagInputComponent,
    },
  ],
  standalone: true,
  imports: [ReactiveFormsModule, MatAutocompleteModule, MatChipsModule, MatIconModule, LocalizedStringPipe, MatFormFieldModule, AsyncPipe],
})
export class TagInputComponent implements ControlValueAccessor {
  private _allTags: Tag[];

  @Input()
  set allTags(value: Tag[]) {
    this._allTags = value.sort((a, b) => {
      if (a.weight === b.weight) {
        // alphabetical order
        return this.isGerman ? a.name.de.localeCompare(b.name.de) : a.name.en.localeCompare(b.name.en);
      } else {
        // weight based order
        return (b.weight ?? -Infinity) - (a.weight ?? -Infinity);
      }
    });

    this.tagsById = value.reduce(
      (prev, curr) => {
        prev[curr.id] = curr;
        return prev;
      },
      {} as { [key: string]: Tag },
    );

    this.allTagsChanged$.next();
  }
  get allTags() {
    return this._allTags;
  }

  @Input() selectedTagIds: string[];
  @Output() selectedTagIdsChange = new EventEmitter<string[]>();

  separatorKeysCodes = [ENTER, COMMA];
  tagCtrl = new UntypedFormControl('');
  filteredTags$: Observable<Tag[]>;
  @ViewChild('tagInput') tagInput: ElementRef<HTMLInputElement>;

  inputFocus$ = new Subject<void>();

  isGerman: boolean;
  tagsById: { [id: string]: Tag };

  private onChange: any;
  private onTouched: any;

  private allTagsChanged$ = new Subject<void>();

  constructor(@Inject(LOCALE_ID) locale: string) {
    this.isGerman = locale.includes('de');

    this.filteredTags$ = merge(this.allTagsChanged$, this.tagCtrl.valueChanges, this.inputFocus$).pipe(
      startWith(null),
      map(() => this.applyFilter()),
    );
  }

  add(event: MatChipInputEvent) {
    const value = (event.value || '').trim().toLowerCase();
    const tag = this.allTags.find(tag => this.getName(tag).toLowerCase() === value);

    if (tag) {
      this.pushTag(tag.id);
    }

    event.chipInput.clear();
  }

  remove(index: number) {
    this.selectedTagIds.splice(index, 1);
    this.tagCtrl.setValue(null);
    this.valueChanged();
  }

  selected(event: MatAutocompleteSelectedEvent) {
    const tagId = event.option.value;
    this.pushTag(tagId);
    this.tagInput.nativeElement.value = '';
  }

  private pushTag(tagId: string) {
    if (!this.selectedTagIds.includes(tagId)) {
      this.selectedTagIds.push(tagId);
      this.tagCtrl.setValue(null);
      this.valueChanged();
    }
  }

  private applyFilter(): Tag[] {
    const relevantTags = this.allTags.filter(tag => tag.isProductive ?? true).filter(tag => !this.selectedTagIds.includes(tag.id));

    const nameFilter = this.tagCtrl.value as string;
    if (nameFilter) {
      const filterValue = nameFilter.toLowerCase();
      return relevantTags.filter(tag => this.getName(tag).toLowerCase().includes(filterValue));
    } else {
      return relevantTags;
    }
  }

  private getName(tag: Tag) {
    return this.isGerman ? tag.name.de ?? tag.name.en : tag.name.en;
  }

  private valueChanged() {
    this.selectedTagIdsChange.next(this.selectedTagIds);
    this.onChange?.(this.selectedTagIds);
    this.onTouched?.();
  }

  writeValue(tagIds: string[]) {
    this.selectedTagIds = tagIds;
  }
  registerOnChange(fn: any): void {
    this.onChange = fn;
  }
  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }
}
