import { CommonModule } from '@angular/common';
import { ChangeDetectionStrategy, Component, Inject, ViewChild } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { MAT_DIALOG_DATA, MatDialog, MatDialogRef } from '@angular/material/dialog';
import { ConfirmModalComponent, ConfirmModalData, SafeUrlPipe } from '@entscheidungsnavi/widgets';
import { ModalModule } from '@entscheidungsnavi/widgets/modal/modal.module';
import { OverlayProgressBarDirective } from '@entscheidungsnavi/widgets/overlay-progress-bar';
import { catchError, concat, filter, map, Observable, of, Subject, switchMap, tap } from 'rxjs';
import { OnDestroyObservable } from '@entscheidungsnavi/tools';
import { PartialBy } from '@entscheidungsnavi/tools';
import { EmbeddedDecisionToolPortalOutletComponent } from './embedded-decision-tool-portal-outlet.component';
import { EmbeddableProject } from './protocol.types';

export type EmbeddedDecisionToolModalData = {
  // The project to load into the decision tool.
  project: EmbeddableProject;
  decisionToolOrigin: string;
  persist?: (project: EmbeddableProject) => Observable<unknown>;
};

type EmbeddedDecisionToolModalDataWithSavingOptionality = PartialBy<EmbeddedDecisionToolModalData, 'persist'>;

@Component({
  standalone: true,
  imports: [
    CommonModule,
    EmbeddedDecisionToolPortalOutletComponent,
    MatButtonModule,
    ModalModule,
    SafeUrlPipe,
    OverlayProgressBarDirective,
  ],
  template: `
    <dt-modal-content size="giant" [showFooter]="!!data.persist" [flexContent]="true" [contentPadding]="false" (closeClick)="abort()">
      <ng-template #title i18n>Themenprojekt „{{ project.name }}“ bearbeiten</ng-template>
      <ng-template #content>
        <dt-embedded-decision-tool-portal-outlet
          [project]="project"
          [decisionToolOrigin]="decisionToolOrigin"
          (projectDirty)="projectMarkedDirty = true"
        >
        </dt-embedded-decision-tool-portal-outlet>
      </ng-template>
      <ng-template #footer>
        <div class="footer">
          <button
            mat-raised-button
            [dtOverlayProgress]="loading$ | async"
            (click)="save()"
            [disabled]="!projectMarkedDirty"
            type="submit"
            color="primary"
            i18n
            >Speichern</button
          >
          <button mat-stroked-button (click)="abort()" type="reset" i18n>Schließen</button>
        </div>
      </ng-template>
    </dt-modal-content>
  `,
  styles: [
    `
      .footer {
        display: flex;
        gap: 8px;
        padding: 4px 10px;
      }
    `,
  ],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EmbeddedDecisionToolModalComponent {
  @ViewChild(EmbeddedDecisionToolPortalOutletComponent) embeddedDecisionToolComponent: EmbeddedDecisionToolPortalOutletComponent;

  protected project: EmbeddableProject;
  protected projectMarkedDirty = false;

  private save$ = new Subject<void>();

  protected loading$ = this.save$.pipe(
    switchMap(() =>
      this.dialog
        .open<ConfirmModalComponent, ConfirmModalData, boolean>(ConfirmModalComponent, {
          data: {
            title: $localize`Themenprojekt abspeichern`,
            prompt: $localize`Bist Du sicher, dass Du das Themenprojekt abspeichern willst?`,
            buttonConfirm: $localize`Ja, speichern`,
            buttonDeny: $localize`Nein, abbrechen`,
          },
        })
        .afterClosed(),
    ),
    filter(Boolean),
    switchMap(() => {
      return concat(of(true), this.requestAndPersistProject().pipe(map(() => false)));
    }),
    tap(() => (this.projectMarkedDirty = false)),
    catchError(() => of(false)),
  );

  protected readonly decisionToolOrigin: string;

  @OnDestroyObservable()
  private onDestroy$: Observable<void>;

  constructor(
    @Inject(MAT_DIALOG_DATA) protected data: EmbeddedDecisionToolModalDataWithSavingOptionality,
    private dialog: MatDialog,
    private dialogRef: MatDialogRef<EmbeddedDecisionToolModalComponent, void>,
  ) {
    this.project = data.project;
    this.decisionToolOrigin = data.decisionToolOrigin;
  }

  private requestAndPersistProject() {
    return this.embeddedDecisionToolComponent.requestProject().pipe(
      switchMap(project => {
        return this.data.persist(project).pipe(
          tap(() => {
            this.project = project;
          }),
        );
      }),
    );
  }

  protected save() {
    this.save$.next();
  }

  protected abort() {
    if (!!this.data.persist && this.projectMarkedDirty) {
      this.dialog
        .open<ConfirmModalComponent, ConfirmModalData, boolean>(ConfirmModalComponent, {
          data: {
            title: $localize`Bearbeitung des Themenprojekts abschließen`,
            prompt: $localize`Bist Du sicher, dass Du die Bearbeitung des Themenprojekts abschließen willst?
            Ungespeicherte Änderungen gehen verloren.`,
            template: `discard`,
          },
        })
        .afterClosed()
        .pipe(filter(Boolean))
        .subscribe(() => this.close());
    } else {
      this.close();
    }
  }

  protected close() {
    this.dialogRef.close();
  }
}
