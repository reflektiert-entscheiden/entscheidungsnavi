import { ChangeDetectionStrategy, Component, ElementRef, Input, OnDestroy, OnInit, Output, ViewChild } from '@angular/core';
import { plainToInstance } from 'class-transformer';
import { BehaviorSubject, shareReplay, Subject, take } from 'rxjs';
import { SafeUrlPipe } from '@entscheidungsnavi/widgets';
import {
  AbstractDecisionToolToEmbedderMessageBodyDto,
  DecisionToolToEmbedderMessageDto,
  EmbeddableProject,
  EmbedderToDecisionToolMessageDto,
  LoadProjectMessageBodyDto,
  ProjectDirtyMessageBodyDto,
  ProjectRequestMessageBodyDto,
  ProjectResponseMessageBodyDto,
} from './protocol.types';

const STATE = {
  NO_PROJECT_LOADED: 'open-but-no-project-loaded',
  PROJECT_LOADED: 'open-and-project-loaded',
  PROJECT_REQUESTED: 'open-and-project-requested',
  CLOSED: 'closed',
} as const;

type State = (typeof STATE)[keyof typeof STATE];

/**
 * This component maintains an iframe for displaying a project in DT.
 * There, it interacts with `EmbeddedDecisionToolService` to:
 * - _load_ a project into DT,
 * - _fetch_ a project from DT,
 * - and _listen_ to project updates from DT
 */
@Component({
  selector: 'dt-embedded-decision-tool-portal-outlet',
  standalone: true,
  imports: [SafeUrlPipe],
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: ` <iframe #dtIframe (load)="attach()" [attr.src]="url | safeUrl"></iframe>`,
  styles: [
    `
      :host {
        display: flex;
        flex-direction: column;
        min-height: 0;
      }

      iframe {
        height: 100vh;
      }
    `,
  ],
})
export class EmbeddedDecisionToolPortalOutletComponent implements OnInit, OnDestroy {
  @ViewChild('dtIframe') dtIframe: ElementRef<HTMLIFrameElement>;

  @Input() project: EmbeddableProject;
  @Input() decisionToolOrigin: string;
  @Output() projectDirty = new Subject<void>();

  private state$ = new BehaviorSubject<State>(STATE.CLOSED);

  private iFrameWithEmbeddedDecisionTool: HTMLIFrameElement;

  private project$ = new Subject<EmbeddableProject>();

  private messageListener: (message: MessageEvent<unknown>) => void;
  protected url: string;

  ngOnInit() {
    this.url = this.decisionToolOrigin + `/start?embedded`;
  }

  private receiveUnsafeMessage(unsafeMessage: MessageEvent<unknown>) {
    if (unsafeMessage.origin !== this.decisionToolOrigin) return;
    // Prevent processing messages from foreign iframes embedded in this page.
    if (unsafeMessage.source !== this.iFrameWithEmbeddedDecisionTool.contentWindow) return;

    const decisionToolToEmbedderMessage = plainToInstance(DecisionToolToEmbedderMessageDto, unsafeMessage.data);
    this.receiveMessage(decisionToolToEmbedderMessage.body);
  }

  private receiveMessage(messageBody: AbstractDecisionToolToEmbedderMessageBodyDto) {
    if (messageBody instanceof ProjectResponseMessageBodyDto && this.state$.getValue() === STATE.PROJECT_REQUESTED) {
      this.state$.next(STATE.PROJECT_LOADED);
      this.project$.next(messageBody.project);
    } else if (messageBody instanceof ProjectDirtyMessageBodyDto && this.state$.value === STATE.PROJECT_LOADED) {
      this.projectDirty.next();
    } else {
      throw new Error(`Cannot receive message of type '${messageBody.messageType}' in state '${this.state$.value}'.`);
    }
  }

  private sendMessage(messageBody: LoadProjectMessageBodyDto | ProjectRequestMessageBodyDto) {
    const send = () => {
      this.iFrameWithEmbeddedDecisionTool.contentWindow.postMessage(
        { body: messageBody } satisfies EmbedderToDecisionToolMessageDto,
        this.decisionToolOrigin,
      );
    };

    if (messageBody instanceof LoadProjectMessageBodyDto && this.state$.value === STATE.NO_PROJECT_LOADED) {
      send();
      // We could add some acknowledgement message here if we want (and add a state for that).
      // For now, we assume every loading is successful.
      this.state$.next(STATE.PROJECT_LOADED);
    } else if (messageBody instanceof ProjectRequestMessageBodyDto && this.state$.value == STATE.PROJECT_LOADED) {
      send();
      this.state$.next(STATE.PROJECT_REQUESTED);
    } else {
      throw new Error(`Cannot send message of type '${messageBody.messageType}' in state '${this.state$.value}'.`);
    }
  }

  protected attach() {
    if (!this.dtIframe) return;

    this.iFrameWithEmbeddedDecisionTool = this.dtIframe.nativeElement;

    this.messageListener = unsafeMessage => this.receiveUnsafeMessage(unsafeMessage);
    window.addEventListener('message', this.messageListener);

    this.state$.next(STATE.NO_PROJECT_LOADED);
    this.loadProject({ ...this.project });
  }

  requestProject() {
    const requestedProject$ = this.project$.pipe(take(1), shareReplay(1));

    this.sendMessage(
      plainToInstance(ProjectRequestMessageBodyDto, {
        messageType: 'dt-embedded-project-request',
      } satisfies ProjectRequestMessageBodyDto),
    );

    return requestedProject$;
  }

  private loadProject(project: EmbeddableProject) {
    this.sendMessage(
      plainToInstance(LoadProjectMessageBodyDto, {
        messageType: 'dt-embedded-project-load',
        project,
      } satisfies LoadProjectMessageBodyDto),
    );
  }

  private close() {
    window.removeEventListener('message', this.messageListener);
    this.state$.next(STATE.CLOSED);
  }

  ngOnDestroy() {
    this.close();
    this.project$.complete();
  }
}
