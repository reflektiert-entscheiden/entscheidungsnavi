import { cloneDeep } from 'lodash';
import { TeamDto } from '@entscheidungsnavi/api-types';
import { normalizeContinuous } from '@entscheidungsnavi/tools';
import { DecisionData } from '../decision-data';
import { readText } from '../export';
import { Objective } from '../classes/objective/objective';

export function isValidImportance(
  memberCount: number,
  importanceValues: number[][],
  importanceShares: number[],
  importanceShareNames: string[],
): [true] | [false, string] {
  if (importanceShareNames.length !== importanceShares.length) {
    return [false, 'The provided importance share names do not match the number of importance shares'];
  }

  if (importanceValues.length !== memberCount) {
    return [false, 'The provided importance matrix does not match the number of team members'];
  }

  if (importanceValues.some(importances => importances.length !== importanceShares.length)) {
    return [false, 'The provided importance matrix is not rectangular'];
  }

  const firstColumnShare = 100 - importanceShares.slice(1).reduce((sum, column) => sum + column, 0);

  if (firstColumnShare !== importanceShares[0]) {
    return [false, 'The provided importance shares do not add up to 100'];
  }

  if (importanceShares.some(share => share < 0 || share > 100)) {
    return [false, 'The provided importance shares are not between 0 and 100'];
  }

  if (importanceValues.some(importances => importances.some(importance => importance < 0 || importance > 100))) {
    return [false, 'The provided importance values are not between 0 and 100'];
  }

  return [true];
}

export function generateMemberRelevance(importanceValues: number[][], shareValues: number[], membersValid: boolean[]) {
  const normalizedColumnWeights = importanceValues[0].map((_, columnIndex) => {
    const columnWeights = importanceValues.map((weights, memberIndex) => (membersValid[memberIndex] ? weights[columnIndex] : 0));
    normalizeContinuous(columnWeights);

    return columnWeights;
  });

  return importanceValues.map((_, memberIndex) => {
    return normalizedColumnWeights.reduce(
      (sum, columnWeights, weightIndex) => sum + columnWeights[memberIndex] * shareValues[weightIndex],
      0,
    );
  });
}

export function generateMemberProjects(
  baseProject: DecisionData,
  hasAggregatedWeights: boolean,
  weights: readonly Readonly<Record<string, number>>[],
) {
  const memberProjects: DecisionData[] = [];

  for (const weightMap of weights) {
    if (!weightMap || !baseProject.objectives.every(objective => weightMap[objective.uuid])) {
      memberProjects.push(null);
      continue;
    }

    const memberProject = cloneDeep(baseProject);
    memberProject.weights.resetWeights();

    if (!hasAggregatedWeights) {
      memberProjects.push(memberProject);
      continue;
    }

    for (let i = 0; i < memberProject.objectives.length; i++) {
      const objective = memberProject.objectives[i];

      memberProject.weights.changePreliminaryWeightValue(i, weightMap[objective.uuid]);
    }

    memberProject.weights.normalizePreliminaryWeights();

    memberProjects.push(memberProject);
  }

  return memberProjects;
}

export function generateAggregatedProject(
  hasAggregatedWeights: boolean,
  baseDecisionData: DecisionData,
  weights: readonly Readonly<Record<string, number>>[],
  memberImportance: number[],
) {
  const result = cloneDeep(baseDecisionData);
  memberImportance = memberImportance.slice();

  if (hasAggregatedWeights) {
    const memberWeights = turnWeightsRecordIntoArray(result.objectives, weights);

    memberWeights.forEach((weightValues, _memberIndex) => {
      normalizeContinuous(weightValues);
    });

    const weightedAvgObjectiveWeights = memberWeights.reduce((sum, weights, memberIndex) => {
      for (let i = 0; i < weights.length; i++) {
        sum[i] += weights[i] * memberImportance[memberIndex];
      }

      return sum;
    }, new Array<number>(baseDecisionData.objectives.length).fill(0));

    if (weightedAvgObjectiveWeights.some(weight => weight !== 0)) {
      result.weights.resetWeights();

      for (let i = 0; i < weightedAvgObjectiveWeights.length; i++) {
        result.weights.changePreliminaryWeightValue(i, weightedAvgObjectiveWeights[i]);
      }

      result.weights.normalizePreliminaryWeights();
    } else {
      result.weights.resetWeights();
    }
  }

  return result;
}

function turnWeightsRecordIntoArray(objectives: readonly Objective[], weights: readonly (Readonly<Record<string, number>> | null)[]) {
  const weightArray: number[][] = [];

  for (const memberWeightMap of weights) {
    const memberWeights: number[] = [];

    for (const objective of objectives) {
      if (memberWeightMap?.[objective.uuid] != null) {
        memberWeights.push(memberWeightMap[objective.uuid]);
      } else {
        break;
      }
    }

    if (memberWeights.length === objectives.length) {
      weightArray.push(memberWeights);
    } else {
      weightArray.push(objectives.map(_ => 0));
    }
  }

  return weightArray;
}

export class TeamCalc {
  public readonly memberProjects: DecisionData[];
  public readonly aggregatedProject: DecisionData;
  public readonly equalAggregatedProject: DecisionData;
  public readonly memberRelevance: number[];

  constructor(team: TeamDto) {
    const baseProject = readText(team.mainProjectData);

    const teamHasAggregatedWeights = team.hasAggregatedWeights;

    const memberObjectiveWeights = team.members.map(member => member.objectiveWeights);

    this.memberProjects = generateMemberProjects(baseProject, teamHasAggregatedWeights, memberObjectiveWeights);

    this.memberRelevance = generateMemberRelevance(
      team.members.map(member => member.importance),
      team.importanceShares,
      this.memberProjects.map(project => project != null),
    );

    this.aggregatedProject = generateAggregatedProject(teamHasAggregatedWeights, baseProject, memberObjectiveWeights, this.memberRelevance);

    this.equalAggregatedProject = generateAggregatedProject(
      teamHasAggregatedWeights,
      baseProject,
      memberObjectiveWeights,
      team.members.map(() => 100 / team.members.length),
    );
  }
}
