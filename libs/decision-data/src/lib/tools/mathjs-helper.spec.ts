import { replaceSymbols } from './mathjs-helper';

describe('MathJS Helper', () => {
  describe('replaceSymbols', () => {
    it('replaces symbols', () => {
      expect(replaceSymbols('a + b + c', { a: 'd', z: 'a' })).toBe('d + b + c');
    });

    it('handles missing spaces', () => {
      expect(replaceSymbols('a+b-c*45', { b: 'd' })).toBe('a + d - c * 45');
    });

    it('ignores non-symbol replacements', () => {
      expect(replaceSymbols('a + b - c * 45', { '*': 'd', '-': '+' })).toBe('a + b - c * 45');
    });
  });
});
