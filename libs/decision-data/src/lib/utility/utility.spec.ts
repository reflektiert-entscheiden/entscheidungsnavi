import { getIndicatorAggregationFunction } from '../classes/objective/indicator-aggregation';
import { ObjectiveInput } from '../classes/objective/objective-input';
import {
  getIndicatorUtilityFunction,
  getInverseUtilityFunction,
  getNumericalUtilityFunction,
  getOutcomeUtilityFunction,
  getUtilityFunction,
} from './utility';

describe('Utility', () => {
  describe('getUtilityFunction', () => {
    it('works with c=0', () => {
      const f = getUtilityFunction(0);
      expect(f(0)).toBe(0);
      expect(f(1)).toBe(1);
      expect(f(0.5)).toBe(0.5);
    });

    it('works with c=1', () => {
      const f = getUtilityFunction(1);
      expect(f(0)).toBe(0);
      expect(f(1)).toBe(1);
      expect(f(0.3798854930417225)).toBeCloseTo(0.5);
    });
  });

  describe('getInverseUtilityFunction', () => {
    it('works with c=0', () => {
      const f = getInverseUtilityFunction(0);
      expect(f(0)).toBe(0);
      expect(f(1)).toBe(1);
      expect(f(0.5)).toBe(0.5);
    });

    it('works with c=1', () => {
      const f = getInverseUtilityFunction(1);
      expect(f(0)).toBeCloseTo(0);
      expect(f(1)).toBe(1);
      expect(f(0.5)).toBeCloseTo(0.3798854930417225);
    });
  });

  it('utility function <=> inverse utility function, c=3', () => {
    // check if chaining getBasicUtilityFunction with its inverse function returns the original value
    const values = [0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1];
    const f = getUtilityFunction(3);
    const f1 = getInverseUtilityFunction(3);

    values.forEach(value => expect(f1(f(value))).toBeCloseTo(value));
    values.forEach(value => expect(f(f1(value))).toBeCloseTo(value));
  });

  describe('getNumericalUtilityFunction', () => {
    it('Handles c=1', () => {
      const f = getNumericalUtilityFunction(1, 100, 735);
      expect(f([[100]])).toBe(0);
      expect(f([[735]])).toBe(1);
      expect(f([[341.2272880814938]])).toBeCloseTo(0.5);
    });
  });

  describe('getIndicatorUtilityFunction', () => {
    it('Handles ascending scale, c=0', () => {
      const aggregationFunction = getIndicatorAggregationFunction(
        [
          { min: 0, max: 100, coefficient: 3, verbalIndicatorCategories: [] },
          { min: 0, max: 50, coefficient: 1, verbalIndicatorCategories: [] },
        ],
        { worst: 1, best: 7 },
      );
      const f0 = getIndicatorUtilityFunction(0, aggregationFunction, 1, 7);

      expect(f0([[0], [0]])).toBe(0);
      expect(f0([[100], [0]])).toBe(0.75);
      expect(f0([[0], [50]])).toBe(0.25);
      expect(f0([[20], [50]])).toBeCloseTo(0.4);
      expect(f0([[80], [10]])).toBeCloseTo(0.65);
      expect(f0([[100], [50]])).toBe(1);
    });

    it('Handles ascending scale, c=1', () => {
      const aggregationFunction = getIndicatorAggregationFunction(
        [
          { min: 0, max: 100, coefficient: 3, verbalIndicatorCategories: [] },
          { min: 0, max: 50, coefficient: 1, verbalIndicatorCategories: [] },
        ],
        { worst: 1, best: 7 },
      );
      const f1 = getIndicatorUtilityFunction(1, aggregationFunction, 1, 7);

      expect(f1([[0], [0]])).toBe(0);
      expect(f1([[100], [0]])).toBeCloseTo(0.8347);
      expect(f1([[0], [50]])).toBeCloseTo(0.3499);
      expect(f1([[20], [50]])).toBeCloseTo(0.5215);
      expect(f1([[80], [10]])).toBeCloseTo(0.7561);
      expect(f1([[100], [50]])).toBe(1);
    });

    it('Handles descending scale, c=0', () => {
      const aggregationFunction = getIndicatorAggregationFunction(
        [
          { min: 10, max: 0, coefficient: 3, verbalIndicatorCategories: [] },
          { min: 0, max: 100, coefficient: 1, verbalIndicatorCategories: [] },
        ],
        { worst: 1, best: 7 },
      );
      const f0 = getIndicatorUtilityFunction(0, aggregationFunction, 1, 7);

      expect(f0([[10], [0]])).toBe(0);
      expect(f0([[0], [100]])).toBe(1);
      expect(f0([[0], [0]])).toBe(0.75);
      expect(f0([[0], [50]])).toBe(0.875);
      expect(f0([[2], [50]])).toBeCloseTo(0.725);
    });

    it('Handles descending scale, c=1', () => {
      const aggregationFunction = getIndicatorAggregationFunction(
        [
          { min: 10, max: 0, coefficient: 3, verbalIndicatorCategories: [] },
          { min: 0, max: 100, coefficient: 1, verbalIndicatorCategories: [] },
        ],
        { worst: 1, best: 7 },
      );
      const f1 = getIndicatorUtilityFunction(1, aggregationFunction, 1, 7);

      expect(f1([[10], [0]])).toBe(0);
      expect(f1([[0], [100]])).toBe(1);
      expect(f1([[0], [0]])).toBeCloseTo(0.8347);
      expect(f1([[0], [50]])).toBeCloseTo(0.9225);
      expect(f1([[2], [50]])).toBeCloseTo(0.8158);
    });

    it('Handles custom aggregation functions [min=worst, max=best]', () => {
      // 'a * Ind1'
      const aggregationFunction = (values: ObjectiveInput) => 1 * values[0][0];
      const f0 = getIndicatorUtilityFunction(0, aggregationFunction, 0, 10);

      expect(f0([[0], [10]])).toBe(0);
      expect(f0([[5], [5]])).toBe(0.5);
      expect(f0([[10], [0]])).toBe(1);
    });

    it('Handles custom aggregation functions [min=best, max=worst]', () => {
      // Models two indicators with scales [0, 10]
      // '-(a * Ind1 + b * Ind2)'
      const aggregationFunction = (values: ObjectiveInput) => -(values[0][0] + values[1][0]);
      const f0 = getIndicatorUtilityFunction(0, aggregationFunction, 0, -20);

      expect(f0([[0], [0]])).toBe(0);
      expect(f0([[5], [0]])).toBe(0.25);
      expect(f0([[10], [10]])).toBe(1);
    });

    it('Fails with wrong number of values', () => {
      const aggregationFunction = getIndicatorAggregationFunction(
        [
          { min: 0, max: 10, coefficient: -3, verbalIndicatorCategories: [] },
          { min: 0, max: 100, coefficient: 1, verbalIndicatorCategories: [] },
        ],
        { worst: 1, best: 7 },
      );
      const f = getIndicatorUtilityFunction(0, aggregationFunction, 1, 7);

      expect(() => f([[10]])).toThrowError();
    });

    it('returns NaN on null values', () => {
      const aggregationFunction = getIndicatorAggregationFunction(
        [
          { min: 0, max: 10, coefficient: -3, verbalIndicatorCategories: [] },
          { min: 0, max: 100, coefficient: 1, verbalIndicatorCategories: [] },
        ],
        { worst: 1, best: 7 },
      );
      const f = getIndicatorUtilityFunction(0, aggregationFunction, 1, 7);

      expect(f([[null], [null]])).toBe(NaN);
      expect(f([[1], [null]])).toBe(NaN);
    });
  });

  describe('getOutcomeUtilityFunction', () => {
    it('works without influence factor', () => {
      const objectiveUf = jest.fn().mockReturnValue(10.5);
      const outcomeUf = getOutcomeUtilityFunction(objectiveUf);

      expect(outcomeUf([[[0]]])).toBeCloseTo(10.5);
      expect(objectiveUf.mock.calls[0][0][0][0]).toBe(0);
    });

    it('works with influence factor', () => {
      const objectiveUf = jest.fn().mockReturnValueOnce(0.1).mockReturnValueOnce(0.5);
      const scenarioGenerator = jest.fn((callback: (scenario: any) => void, _outcomeValues: ObjectiveInput[]) => {
        callback({ value: 2, stateIndex: 0, probability: 0.4, verbalIndicatorCategories: [] });
        callback({ value: 5, stateIndex: 1, probability: 0.6, verbalIndicatorCategories: [] });
      });

      const outcomeUf = getOutcomeUtilityFunction(objectiveUf, scenarioGenerator);
      expect(outcomeUf([[[2]], [[5]]])).toBeCloseTo(0.1 * 0.4 + 0.5 * 0.6);

      expect(objectiveUf.mock.calls[0][0]).toBe(2);
      expect(objectiveUf.mock.calls[1][0]).toBe(5);
      expect(scenarioGenerator.mock.calls[0][1]).toEqual([[[2]], [[5]]]);
    });
  });
});
