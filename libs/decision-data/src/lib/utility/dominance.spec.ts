import { Alternative } from '../classes/alternative/alternative';
import { Objective, ObjectiveType } from '../classes/objective/objective';
import { Outcome } from '../classes/outcome/outcome';
import { NumericalObjectiveData } from '../classes/objective/numerical-objective-data';
import { UtilityFunction } from '../classes/utility-function';
import { IndicatorObjectiveData } from '../classes/objective/indicator-objective-data';
import { Indicator } from '../classes/objective/indicator';
import { SimpleUserDefinedInfluenceFactor, UserDefinedState } from '../classes/influence-factor/simple-user-defined-influence-factor';
import { calculateDominanceRelations } from './dominance';

function setAllProcessed(outcomes: Outcome[][]) {
  outcomes.forEach(ocs => ocs.forEach(oc => (oc.processed = true)));
}

describe('Dominance', () => {
  describe('calculateDominanceRelations', () => {
    describe('TestSuite [Numerical, Indicator]', () => {
      let objs: Objective[];
      let alts: Alternative[];

      beforeEach(() => {
        objs = [
          new Objective(
            'z1',
            new NumericalObjectiveData(6, 1, '', new UtilityFunction(10, 5)),
            undefined,
            undefined,
            ObjectiveType.Numerical,
          ),
          new Objective(
            'z2',
            undefined,
            undefined,
            new IndicatorObjectiveData([new Indicator('', 0, 10, '', 10), new Indicator('', -1, 9, '', 1)]),
            ObjectiveType.Indicator,
          ),
        ];
        alts = [new Alternative(0, 'a1'), new Alternative(0, 'a2'), new Alternative(0, 'a3')];
      });

      it('recognizes dominance', () => {
        const outcs = [
          [new Outcome([[[6]]]), new Outcome([[[5], [5]]])],
          [new Outcome([[[3]]]), new Outcome([[[5], [5]]])],
          [new Outcome([[[1]]]), new Outcome([[[4], [5]]])],
        ];
        setAllProcessed(outcs);

        const dr = calculateDominanceRelations(objs, alts, outcs);
        expect(dr.length).toBe(1);
        expect(dr[0]).toMatchObject({ dominated: alts[0], dominating: alts[1] });
      });

      it('handles non-processed outcomes', () => {
        const outcs = [
          [new Outcome([[[6]]]), new Outcome([[[5], [5]]])],
          [new Outcome([[[3]]]), new Outcome([[[5], [5]]])],
          [new Outcome([[[1]]]), new Outcome([[[4], [5]]])],
        ];
        setAllProcessed(outcs);
        outcs[1][0].processed = false;

        const dr = calculateDominanceRelations(objs, alts, outcs);
        expect(dr.length).toBe(0);
      });

      it('recognizes non-dominance', () => {
        const outcs = [
          [new Outcome([[[6]]]), new Outcome([[[10], [1]]])],
          [new Outcome([[[3]]]), new Outcome([[[10], [0]]])],
          [new Outcome([[[1]]]), new Outcome([[[10], [-1]]])],
        ];
        setAllProcessed(outcs);

        const dr = calculateDominanceRelations(objs, alts, outcs);
        expect(dr.length).toBe(0);
      });
    });

    it('handles dominance with same influence factor (issue #7)', () => {
      const objs = [
        new Objective('Objective 1', new NumericalObjectiveData(1, 7)),
        new Objective('Objective 2', new NumericalObjectiveData(1, 7)),
      ];
      const alts = [new Alternative(0, 'Option 1'), new Alternative(0, 'Option 2')];
      const uf = new SimpleUserDefinedInfluenceFactor('Weather', [
        new UserDefinedState('', 10),
        new UserDefinedState('', 40),
        new UserDefinedState('', 50),
      ]);

      const outcs = [
        [new Outcome([[[3]], [[3]], [[5]]], undefined, uf), new Outcome([[[3]]])],
        [new Outcome([[[3]]]), new Outcome([[[3]]])],
      ];
      setAllProcessed(outcs);

      const dr = calculateDominanceRelations(objs, alts, outcs);
      expect(dr.length).toBe(1);
      expect(dr[0]).toMatchObject({ dominating: alts[0], dominated: alts[1] });
    });

    describe('handles dominance with different influence factors', () => {
      it('correctly finds no stochastic dominance (overlapping step functions) #1', () => {
        const objs1 = [
          new Objective('Objective 1', new NumericalObjectiveData(5, 1)),
          new Objective('Objective 2', new NumericalObjectiveData(1, 7)),
        ];
        const alts1 = [new Alternative(0, 'Option 1'), new Alternative(0, 'Option 2')];
        const uf11 = new SimpleUserDefinedInfluenceFactor('IF1', [
          new UserDefinedState('2', 20),
          new UserDefinedState('3', 30),
          new UserDefinedState('5', 50),
        ]);
        const uf21 = new SimpleUserDefinedInfluenceFactor('IF2', [
          new UserDefinedState('1', 10),
          new UserDefinedState('4', 40),
          new UserDefinedState('5', 50),
        ]);

        const outcs1 = [
          [new Outcome([[[2]], [[3]], [[5]]], undefined, uf11), new Outcome([[[4]]])],
          [new Outcome([[[1]], [[4]], [[5]]], undefined, uf21), new Outcome([[[3]]])],
        ];
        setAllProcessed(outcs1);

        const dr1 = calculateDominanceRelations(objs1, alts1, outcs1);
        expect(dr1.length).toBe(0);
      });

      it('correctly finds no stochastic dominance (overlapping step functions) #2', () => {
        const objs2 = [
          new Objective('Objective 1', new NumericalObjectiveData(5, 1)),
          new Objective('Objective 2', new NumericalObjectiveData(1, 7)),
        ];
        const alts2 = [new Alternative(0, 'Option 1'), new Alternative(0, 'Option 2')];
        const uf12 = new SimpleUserDefinedInfluenceFactor('IF1', [
          new UserDefinedState('1', 10),
          new UserDefinedState('3', 40),
          new UserDefinedState('5', 50),
        ]);
        const uf22 = new SimpleUserDefinedInfluenceFactor('IF2', [
          new UserDefinedState('2', 20),
          new UserDefinedState('4', 30),
          new UserDefinedState('5', 50),
        ]);

        const outcs2 = [
          [new Outcome([[[1]], [[3]], [[5]]], undefined, uf12), new Outcome([[[4]]])],
          [new Outcome([[[2]], [[4]], [[5]]], undefined, uf22), new Outcome([[[3]]])],
        ];
        setAllProcessed(outcs2);

        const dr2 = calculateDominanceRelations(objs2, alts2, outcs2);
        expect(dr2.length).toBe(0);
      });

      it('correctly identifies a dominance (stochastic dominance + dominance in other columns)', () => {
        const objs3 = [
          new Objective('Objective 1', new NumericalObjectiveData(5, 1)),
          new Objective('Objective 2', new NumericalObjectiveData(1, 7)),
        ];
        const alts3 = [new Alternative(0, 'Option 1'), new Alternative(0, 'Option 2')];
        const uf13 = new SimpleUserDefinedInfluenceFactor('IF1', [
          new UserDefinedState('1', 5),
          new UserDefinedState('2', 10),
          new UserDefinedState('3', 25),
          new UserDefinedState('4', 40),
          new UserDefinedState('5', 20),
        ]);
        const uf23 = new SimpleUserDefinedInfluenceFactor('IF2', [
          new UserDefinedState('1', 1),
          new UserDefinedState('2', 9),
          new UserDefinedState('3', 20),
          new UserDefinedState('4', 20),
          new UserDefinedState('5', 50),
        ]);

        const outcs3 = [
          [new Outcome([[[1]], [[2]], [[3]], [[4]], [[5]]], undefined, uf13), new Outcome([[[4]]])],
          [new Outcome([[[1]], [[2]], [[3]], [[4]], [[5]]], undefined, uf23), new Outcome([[[3]]])],
        ];
        setAllProcessed(outcs3);

        const dr3 = calculateDominanceRelations(objs3, alts3, outcs3);
        expect(dr3.length).toBe(1);
        expect(dr3[0]).toMatchObject({ dominating: alts3[0], dominated: alts3[1] });
      });

      it('correctly identifies a dominance (only stochastic dominance)', () => {
        const objs4 = [new Objective('Objective 1'), new Objective('Objective 2')];
        const alts4 = [new Alternative(0, 'Option 1'), new Alternative(0, 'Option 2')];
        const uf14 = new SimpleUserDefinedInfluenceFactor('IF1', [
          new UserDefinedState('Z1', 10),
          new UserDefinedState('Z2', 30),
          new UserDefinedState('Z3', 60),
        ]);
        const uf24 = new SimpleUserDefinedInfluenceFactor('IF2', [
          new UserDefinedState('Z1', 60),
          new UserDefinedState('Z2', 30),
          new UserDefinedState('Z3', 10),
        ]);

        const outcs4 = [[new Outcome([[[40]], [[60]], [[80]]], undefined, uf14)], [new Outcome([[[40]], [[60]], [[80]]], undefined, uf24)]];
        setAllProcessed(outcs4);

        const dr4 = calculateDominanceRelations(objs4, alts4, outcs4);
        expect(dr4.length).toBe(1);
        expect(dr4[0]).toMatchObject({ dominating: alts4[0], dominated: alts4[1] });
      });
    });

    it('handles dominance with same influence factor', () => {
      const objs = [
        new Objective('Objective 1', new NumericalObjectiveData(1, 7)),
        new Objective('Objective 2', new NumericalObjectiveData(1, 7)),
      ];
      const alts = [new Alternative(0, 'Option 1'), new Alternative(0, 'Option 2')];
      const uf = new SimpleUserDefinedInfluenceFactor('Weather', [
        new UserDefinedState('', 10),
        new UserDefinedState('', 40),
        new UserDefinedState('', 50),
      ]);

      const outcs = [
        [new Outcome([[[3]], [[3]], [[5]]], undefined, uf), new Outcome([[[5]]])],
        [new Outcome([[[3]], [[3]], [[5]]], undefined, uf), new Outcome([[[3]]])],
      ];
      setAllProcessed(outcs);

      const dr = calculateDominanceRelations(objs, alts, outcs);
      expect(dr.length).toBe(1);
      expect(dr[0]).toMatchObject({ dominating: alts[0], dominated: alts[1] });
    });

    describe('indicatorDominance', () => {
      it('handles absence of indicator objectives', () => {
        const objs = [new Objective('z1', new NumericalObjectiveData(6, 1)), new Objective('z1', new NumericalObjectiveData(1, 6))];
        const alts = [new Alternative(0, 'a1'), new Alternative(0, 'a2')];

        const outcs = [
          [new Outcome([[[3]]]), new Outcome([[[3]]])],
          [new Outcome([[[1]]]), new Outcome([[[3]]])],
        ];
        setAllProcessed(outcs);

        const dr = calculateDominanceRelations(objs, alts, outcs);
        expect(dr.length).toBe(1);
        expect(dr[0]).toMatchObject({ dominated: alts[0], dominating: alts[1] });
      });

      it('handles same influence factor', () => {
        const objs = [
          new Objective(
            'Objective 1',
            undefined,
            undefined,
            new IndicatorObjectiveData([new Indicator('Indicator 1', 0, 10, '', 10), new Indicator('Indicator 2', 0, 10, '', 10)]),
            ObjectiveType.Indicator,
          ),
        ];
        const alts = [new Alternative(0, 'Option 1'), new Alternative(0, 'Option 2')];
        const uf = new SimpleUserDefinedInfluenceFactor('Weather', [new UserDefinedState('', 50), new UserDefinedState('', 50)]);

        const outcs = [
          [
            new Outcome(
              [
                [[1], [3]],
                [[1], [4]],
              ],
              undefined,
              uf,
            ),
          ],
          [
            new Outcome(
              [
                [[1], [2]],
                [[1], [4]],
              ],
              undefined,
              uf,
            ),
          ],
        ];
        setAllProcessed(outcs);

        const dr = calculateDominanceRelations(objs, alts, outcs);
        expect(dr.length).toBe(1);
        expect(dr[0]).toMatchObject({ dominating: alts[0], dominated: alts[1] });
      });
    });

    it('handles dominance based on indicator', () => {
      const objs = [
        new Objective('z1', new NumericalObjectiveData(0, 10)),
        new Objective(
          'z2',
          undefined,
          undefined,
          new IndicatorObjectiveData([new Indicator('', 0, 10, '', 10), new Indicator('', 5, -4, '', -1)]),
          ObjectiveType.Indicator,
        ),
      ];
      const alts = [new Alternative(0, 'a1'), new Alternative(0, 'a2'), new Alternative(0, 'a3'), new Alternative(0, 'a4')];

      const outcs = [
        [new Outcome([[[3]]]), new Outcome([[[5], [0]]])],
        [new Outcome([[[5]]]), new Outcome([[[5], [0]]])],
        [new Outcome([[[4]]]), new Outcome([[[6], [1]]])],
        [new Outcome([[[3]]]), new Outcome([[[10], [-1]]])],
      ];
      setAllProcessed(outcs);

      expect(calculateDominanceRelations(objs, alts, outcs)).toEqual([
        { dominated: alts[0], dominating: alts[1] }, // dominated only by number
        { dominated: alts[0], dominating: alts[2] }, // dominated by both
        { dominated: alts[0], dominating: alts[3] }, // dominated only by indicator
      ]);
    });
  });
});
