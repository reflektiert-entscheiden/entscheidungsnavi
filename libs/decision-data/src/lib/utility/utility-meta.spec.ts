import { Indicator } from '../classes/objective/indicator';
import { IndicatorObjectiveData } from '../classes/objective/indicator-objective-data';
import { NumericalObjectiveData } from '../classes/objective/numerical-objective-data';
import { Objective, ObjectiveType } from '../classes/objective/objective';
import { VerbalObjectiveData } from '../classes/objective/verbal-objective-data';
import { Outcome } from '../classes/outcome/outcome';
import { UtilityFunction } from '../classes/utility-function';
import { Weights } from '../classes/weights';
import { SimpleUserDefinedInfluenceFactor, UserDefinedState } from '../classes/influence-factor/simple-user-defined-influence-factor';
import { getAlternativeUtilitiesMeta } from './utility-meta';

/**
 * Helper function that turns an array of numerical weights into a weight object.
 *
 * @param numericalWeights - The weights to embed into the object
 * @returns The weights object
 */
function getWeights(numericalWeights: number[]) {
  const weights = new Weights();
  numericalWeights.forEach((weight, index) => {
    weights.addObjective(index);
    weights.preliminaryWeights[index].value = weight;
  });
  return weights;
}

describe('Utility Calculation Meta', () => {
  describe('getAlternativeUtilitiesMeta', () => {
    let obj1: Objective, obj2: Objective, obj3: Objective;

    beforeAll(() => {
      obj1 = new Objective('Objective 1', new NumericalObjectiveData(100, 735, '', new UtilityFunction(1, 0)), null);
      obj2 = new Objective(
        'Objective 2',
        null,
        new VerbalObjectiveData(['opt1', 'opt2', 'opt3'], [0, 30, 100], undefined, undefined, undefined, undefined, true),
        null,
        ObjectiveType.Verbal,
      );
      obj3 = new Objective(
        'Objective 3',
        null,
        null,
        new IndicatorObjectiveData(
          [new Indicator('Ind 1', 5, 0, '', 3), new Indicator('Ind 2', 0, 100, '', 2), new Indicator('Ind 3', 0, -100, '', 1)],
          new UtilityFunction(0, 0),
        ),
        ObjectiveType.Indicator,
      );
    });

    it('1x [numerical]', () => {
      const utilities = getAlternativeUtilitiesMeta([[new Outcome([[[735]]])]], [obj1], getWeights([100]));

      expect(utilities).toEqual([1]);
    });
    it('3x [numerical]', () => {
      const res = getAlternativeUtilitiesMeta(
        [[new Outcome([[[100]]])], [new Outcome([[[341.2272880814938]]])], [new Outcome([[[735]]])]],
        [obj1],
        getWeights([100]),
      );

      expect(res[0]).toBe(0);
      expect(res[1]).toBeCloseTo(0.5);
      expect(res[2]).toBe(1);
    });

    it('3x [indicator]', () => {
      const res = getAlternativeUtilitiesMeta(
        [[new Outcome([[[0], [100], [-100]]])], [new Outcome([[[2], [30], [-50]]])], [new Outcome([[[5], [0], [0]]])]],
        [obj3],
        getWeights([100]),
      );

      expect(res[0]).toBe(1);
      expect(res[1]).toBeCloseTo(0.4833333333);
      expect(res[2]).toBe(0);
    });

    it('3x [verbal]', () => {
      const res = getAlternativeUtilitiesMeta(
        [[new Outcome([[[1]]])], [new Outcome([[[2]]])], [new Outcome([[[3]]])]],
        [obj2],
        getWeights([100]),
      );

      expect(res[0]).toBe(0);
      expect(res[1]).toBe(0.3);
      expect(res[2]).toBe(1);
    });

    it('1x [numerical, numerical]', () => {
      expect(
        getAlternativeUtilitiesMeta(
          [[new Outcome([[[341.2272880814938]]]), new Outcome([[[233.5226923282041]]])]],
          [obj1, obj1],
          getWeights([100, 200]),
        )[0],
      ).toBeCloseTo(0.5 / 3 + (0.3 * 2) / 3);
    });

    it('1x [numerical, indicator]', () => {
      const res = getAlternativeUtilitiesMeta(
        [
          [new Outcome([[[735]]]), new Outcome([[[0], [100], [-100]]])],
          [new Outcome([[[100]]]), new Outcome([[[5], [0], [0]]])],
        ],
        [obj1, obj3],
        getWeights([100, 200]),
      );
      expect(res[0]).toBe(1);
      expect(res[1]).toBe(0);
    });

    it('3x [numerical, verbal]', () => {
      const res = getAlternativeUtilitiesMeta(
        [
          [new Outcome([[[341.2272880814938]]]), new Outcome([[[2]]])],
          [new Outcome([[[100]]]), new Outcome([[[3]]])],
          [new Outcome([[[735]]]), new Outcome([[[3]]])],
        ],
        [obj1, obj2],
        getWeights([100, 200]),
      );
      expect(res[0]).toBeCloseTo(0.5 / 3 + (0.3 * 2) / 3);
      expect(res[1]).toBeCloseTo(2 / 3);
      expect(res[2]).toBeCloseTo(1);
    });

    describe('with influence factors', () => {
      const uf1 = new SimpleUserDefinedInfluenceFactor('', [new UserDefinedState('', 30), new UserDefinedState('', 70)], 1),
        uf2 = new SimpleUserDefinedInfluenceFactor(
          '',
          [new UserDefinedState('', 10), new UserDefinedState('', 65), new UserDefinedState('', 25)],
          2,
        ),
        ap1 = new Outcome([[[2]], [[1]]], null, uf1),
        ap2 = new Outcome([[[2]], [[3]], [[1]]], null, uf2),
        ap3 = new Outcome([[[341.2272880814938]], [[233.5226923282041]]], null, uf1),
        ap4 = new Outcome([[[341.2272880814938]], [[100]], [[735]]], null, uf2);

      it('simple numerical', () => {
        const res = getAlternativeUtilitiesMeta([[ap3], [ap4]], [obj1], getWeights([100]));
        expect(res[0]).toBe(0.3 * 0.5 + 0.7 * 0.3);
        expect(res[1]).toBe(0.1 * 0.5 + 0.25);
      });

      it('simple verbal', () => {
        const res = getAlternativeUtilitiesMeta([[ap1], [ap2]], [obj2], getWeights([100]));
        expect(res[0]).toBe(0.3 * 0.3);
        expect(res[1]).toBe(0.1 * 0.3 + 0.65);
      });
    });
  });
});
