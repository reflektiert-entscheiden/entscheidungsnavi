import { Weights } from '../classes/weights';
import { Objective, ObjectiveType } from '../classes/objective/objective';
import { NumericalObjectiveData } from '../classes/objective/numerical-objective-data';
import { UtilityFunction } from '../classes/utility-function';
import { VerbalObjectiveData } from '../classes/objective/verbal-objective-data';
import { IndicatorObjectiveData } from '../classes/objective/indicator-objective-data';
import { Indicator } from '../classes/objective/indicator';
import { Outcome } from '../classes/outcome/outcome';
import { SimpleUserDefinedInfluenceFactor, UserDefinedState } from '../classes/influence-factor/simple-user-defined-influence-factor';
import { ObjectiveWeight } from '../classes/objective-weight';
import { getAlternativeUtilitiesMeta } from './utility-meta';
import { randomizedUtilityGenerator, UtilityGeneratorResult } from './uncertainty';

/**
 * Helper function that turns an array of numerical weights into a weight object.
 *
 * @param numericalWeights - The weights to embed into the object
 * @returns The weights object
 */
function getWeights(numericalWeights: number[]) {
  const weights = new Weights();
  numericalWeights.forEach((weight, index) => {
    weights.addObjective(index);
    weights.preliminaryWeights[index].value = weight;
  });
  return weights;
}

describe('Uncertainty', () => {
  describe('randomizedUtilityGenerator()', () => {
    const objectives = [
      new Objective(
        '',
        new NumericalObjectiveData(0, 10, undefined, new UtilityFunction(5, 1)),
        undefined,
        undefined,
        ObjectiveType.Numerical,
      ),
      new Objective('', undefined, new VerbalObjectiveData(['', '', ''], [0, 10, 100], 5), undefined, ObjectiveType.Verbal),
      new Objective(
        '',
        undefined,
        undefined,
        new IndicatorObjectiveData(
          [new Indicator('', 0, 10, undefined, 1), new Indicator('', 5, 1, undefined, 3)],
          new UtilityFunction(3, 2),
        ),
        ObjectiveType.Indicator,
      ),
    ];
    const outcomes = [
      [
        new Outcome([[[2]]]),
        new Outcome(
          [[[1]], [[3]]],
          undefined,
          new SimpleUserDefinedInfluenceFactor('', [new UserDefinedState('', 10), new UserDefinedState('', 90)], 0),
        ),
        new Outcome([[[4], [2]]]),
      ],
    ];
    const weights = [new ObjectiveWeight(10, 5), new ObjectiveWeight(20, 0), new ObjectiveWeight(70, 1)];

    it('matches the default calculation if all uncertainties are disabled', () => {
      const generator = randomizedUtilityGenerator(
        objectives,
        outcomes,
        weights,
        {
          influenceFactorScenarios: { predefined: false, userDefinedIds: [] },
          objectiveWeights: false,
          probabilities: false,
          utilityFunctions: false,
        },
        [0],
      );
      const result = generator.next().value as UtilityGeneratorResult;

      expect(result.alternativeUtilities).toEqual(getAlternativeUtilitiesMeta(outcomes, objectives, getWeights([10, 20, 70])));
    });

    describe('returns appropriate amount of unique utility values', () => {
      /**
       * These tests are specific to the current implementation of randomizedUtilityGenerator.
       * If the implementation changes the mock for Math.random or the expected values might need to be adjusted.
       */

      beforeEach(() => {
        let counter = 0;
        jest.spyOn(global.Math, 'random').mockImplementation(() => {
          // This will return 0, 0.01, 0.02, ..., 0.99, 0, 0.01, ...
          const rng = counter / 100;
          counter = (counter + 1) % 100;
          return rng;
        });
      });

      afterEach(() => {
        jest.spyOn(global.Math, 'random').mockRestore();
      });

      it('if influence factor scenarios are enabled', () => {
        const generator = randomizedUtilityGenerator(
          objectives,
          outcomes,
          weights,
          {
            influenceFactorScenarios: { predefined: false, userDefinedIds: [0] },
            objectiveWeights: false,
            probabilities: false,
            utilityFunctions: false,
          },
          [0],
        );

        const results = Array.from({ length: 100 }, () => generator.next().value as UtilityGeneratorResult);

        const uniqueAlternativeUtilityValues = new Set(results.map(result => result.alternativeUtilities[0]));

        // The influence factor has two states, so there should be two unique utility values
        expect(uniqueAlternativeUtilityValues.size).toBe(2);
      });

      it('if objective weights are enabled', () => {
        const generator = randomizedUtilityGenerator(
          objectives,
          outcomes,
          weights,
          {
            influenceFactorScenarios: { predefined: false, userDefinedIds: [] },
            objectiveWeights: true,
            probabilities: false,
            utilityFunctions: false,
          },
          [0],
        );

        const results = Array.from({ length: 100 }, () => generator.next().value as UtilityGeneratorResult);

        const uniqueAlternativeUtilityValues = new Set(results.map(result => result.alternativeUtilities[0]));

        // Generates 100 unique objective weights so there should be 100 unique utility values
        expect(uniqueAlternativeUtilityValues.size).toBe(100);
      });

      it('if probabilities are enabled', () => {
        const generator = randomizedUtilityGenerator(
          objectives,
          outcomes,
          weights,
          {
            influenceFactorScenarios: { predefined: false, userDefinedIds: [] },
            objectiveWeights: false,
            probabilities: true,
            utilityFunctions: false,
          },
          [0],
        );

        const results = Array.from({ length: 100 }, () => generator.next().value as UtilityGeneratorResult);

        const uniqueAlternativeUtilityValues = new Set(results.map(result => result.alternativeUtilities[0]));

        // Uses 2 random values per run, one for each influence factor state. So there should be 50 unique utility values
        expect(uniqueAlternativeUtilityValues.size).toBe(50);
      });

      it('if utility functions are enabled', () => {
        const generator = randomizedUtilityGenerator(
          objectives,
          outcomes,
          weights,
          {
            influenceFactorScenarios: { predefined: false, userDefinedIds: [] },
            objectiveWeights: false,
            probabilities: false,
            utilityFunctions: true,
          },
          [0],
        );

        const results = Array.from({ length: 100 }, () => generator.next().value as UtilityGeneratorResult);

        const uniqueAlternativeUtilityValues = new Set(results.map(result => result.alternativeUtilities[0]));

        // Generates 100 unique utility functions so there should be 100 unique utility values
        expect(uniqueAlternativeUtilityValues.size).toBe(100);
      });
    });

    it('does not return the same result twice', () => {
      const generator = randomizedUtilityGenerator(
        objectives,
        outcomes,
        weights,
        {
          influenceFactorScenarios: { predefined: false, userDefinedIds: [0] },
          objectiveWeights: true,
          probabilities: true,
          utilityFunctions: true,
        },
        [0],
      );
      const result1 = generator.next().value as UtilityGeneratorResult;
      const result2 = generator.next().value as UtilityGeneratorResult;

      expect(result1.alternativeUtilities).not.toEqual(result2.alternativeUtilities);
    });

    test.each([0, 0.5, 1])('returns values within bounds (Math.random() = %f)', fakeRandom => {
      jest.spyOn(global.Math, 'random').mockReturnValue(fakeRandom);

      const generator = randomizedUtilityGenerator(
        objectives,
        outcomes,
        weights,
        {
          influenceFactorScenarios: { predefined: false, userDefinedIds: [] },
          objectiveWeights: true,
          probabilities: true,
          utilityFunctions: true,
        },
        [0],
      );
      const result1 = generator.next().value as UtilityGeneratorResult;
      for (const utility of result1.alternativeUtilities) {
        expect(utility).toBeGreaterThanOrEqual(0);
        expect(utility).toBeLessThanOrEqual(1);
      }

      jest.spyOn(global.Math, 'random').mockRestore();
    });
  });
});
