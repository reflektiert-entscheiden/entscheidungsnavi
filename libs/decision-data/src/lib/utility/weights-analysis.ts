import { range } from 'lodash';
import { normalizeContinuous } from '@entscheidungsnavi/tools';

export const OWA_HISTOGRAM_BIN_COUNT = 1000;

export class WeightsAnalysis {
  private histogramData: number[][][]; // [alternativeIdx][objectiveIdx][binIdx]
  private combinationsComputed: number;
  private readonly bestAlternativeCounters: number[];
  private randomCombination: number[];

  constructor(
    private numberOfObjectives: number,
    private numberOfAlternatives: number,
    private prefixValuesPerWorker: number[],
    private possibleValues: number[],
    private utilityMatrix: number[][],
  ) {
    this.resetResult();
    this.bestAlternativeCounters = Array.from({ length: numberOfAlternatives }, () => 0);
    this.randomCombination = Array.from({ length: this.numberOfObjectives }, () => null);
  }

  getResult() {
    return {
      histogramData: this.histogramData,
      combinationsComputed: this.combinationsComputed,
      bestAlternativeCounters: this.bestAlternativeCounters,
    };
  }

  resetResult() {
    this.combinationsComputed = 0;
    this.histogramData = Array.from({ length: this.numberOfAlternatives }, () =>
      Array.from({ length: this.numberOfObjectives }, () => Array.from({ length: OWA_HISTOGRAM_BIN_COUNT }, () => 0)),
    );
  }

  // recursion wrapper function
  *generateFixedCombinations() {
    const chosen = Array.from({ length: this.numberOfObjectives }, () => 0);
    for (const prefixValue of this.prefixValuesPerWorker) {
      // every web worker calculates combinations with a pre-defined prefixValue
      // For example when workerCount = 3 and possibleValues = [0, 0.25, 0.5, 0.75, 1]:
      // Worker 1 computes combinations [0, *, *, ..], [0,75, *, *, ..]
      // Worker 2 computes [0.25, *, *, ..], [1, *, *, ..]
      // Worker 3 computes [0.5, *, *, ..]
      yield* this.generateFixedCombinationsRecursively(prefixValue, chosen, 0);
    }
  }

  // recursive function
  private *generateFixedCombinationsRecursively(prefixValue: number, chosen: number[], index: number): Generator<number[]> {
    // combination is ready
    if (index === this.numberOfObjectives - 1) {
      // generate the weight combination in percentages, add prefixValue in front
      const combination = [prefixValue, ...range(this.numberOfObjectives - 1).map(i => this.possibleValues[chosen[i]])];
      this.updateHistogramData(combination);
      this.combinationsComputed++;
      yield combination;
    } else {
      for (let i = 0; i < this.possibleValues.length; i++) {
        chosen[index] = i;
        yield* this.generateFixedCombinationsRecursively(prefixValue, chosen, index + 1);
      }
    }
  }

  *generateRandomCombinations() {
    // eslint-disable-next-line no-constant-condition
    while (true) {
      for (let i = 0; i < this.numberOfObjectives; i++) {
        this.randomCombination[i] = Math.random();
      }
      this.updateHistogramData(this.randomCombination);
      this.combinationsComputed++;
      yield this.randomCombination;
    }
  }

  // compute utility data, find the best alternative, change histogramData
  private updateHistogramData(combination: number[]) {
    normalizeContinuous(combination);

    let bestAlternativeIdx = -1;
    let bestUtility = -Infinity;

    for (let alternativeIdx = 0; alternativeIdx < this.numberOfAlternatives; alternativeIdx++) {
      let alternativeUtility = 0;
      for (let objectiveIdx = 0; objectiveIdx < this.numberOfObjectives; objectiveIdx++) {
        if (!isNaN(combination[objectiveIdx])) {
          alternativeUtility += this.utilityMatrix[alternativeIdx][objectiveIdx] * combination[objectiveIdx];
        }
      }

      if (alternativeUtility > bestUtility) {
        bestUtility = alternativeUtility;
        bestAlternativeIdx = alternativeIdx;
      }
    }

    // find the alternative that is ranking first
    this.bestAlternativeCounters[bestAlternativeIdx]++;
    // add respective utility to histogram (increase respective bin)
    for (let objectiveIdx = 0; objectiveIdx < this.numberOfObjectives; objectiveIdx++) {
      const histogramIdx = Math.round(combination[objectiveIdx] * (OWA_HISTOGRAM_BIN_COUNT - 1));
      this.histogramData[bestAlternativeIdx][objectiveIdx][histogramIdx]++;
    }
  }

  // helper function for finding the index of the best alternative of the current ranking
  private indexOfMax(arr: number[]) {
    if (arr.length === 0) {
      return -1;
    }
    let max = arr[0];
    let maxIndex = 0;
    for (let i = 1; i < arr.length; i++) {
      if (arr[i] > max) {
        maxIndex = i;
        max = arr[i];
      }
    }
    return maxIndex;
  }
}
