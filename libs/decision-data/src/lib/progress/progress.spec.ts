import { readText } from '../export/import';
import { DecisionData } from '../decision-data';
import { ALTERNATIVES_STEPS } from '../classes/alternative/hint-alternatives';
import { DECISION_STATEMENT_STEPS } from '../classes/decision-statement';
import { OBJECTIVES_STEPS } from '../classes/objective/objective-aspects';
import { calculateCurrentProgress, NAVI_SUB_STEP_COUNT, numberToSubStep, subStepToNumber } from './progress';

// eslint-disable-next-line @typescript-eslint/no-var-requires
const testJsonData = require('./progress.spec.json');

describe('progress', () => {
  describe('calculateCurrentProgress', () => {
    let data: { [key: string]: DecisionData };

    beforeAll(() => {
      data = {};
      Object.entries(testJsonData).forEach(([name, dd]) => {
        data[name] = readText(JSON.stringify(dd));
      });
    });

    it('works for decisionStatement/hint1', () => {
      expect(calculateCurrentProgress(data['decisionStatement:hint1'])).toEqual({
        step: 'decisionStatement',
        subStepIndex: 0,
      });
    });

    it('works for objectives/hint4', () => {
      expect(calculateCurrentProgress(data['objectives:hint4'])).toEqual({ step: 'objectives', subStepIndex: 4 });
    });

    it('works for alternatives/main', () => {
      expect(calculateCurrentProgress(data['alternatives:main'])).toEqual({ step: 'alternatives' });
    });

    it('works for impactModel', () => {
      expect(calculateCurrentProgress(data['impactModel'])).toEqual({ step: 'impactModel' });
    });
  });

  it('calculates NAVI_SUB_STEP_COUNT', () => {
    expect(NAVI_SUB_STEP_COUNT).toBe(24);
  });

  describe('subStepToNumber', () => {
    it('works for decisionStatement', () => {
      expect(subStepToNumber({ step: 'decisionStatement', subStepIndex: 0 })).toBe(0);
      expect(subStepToNumber({ step: 'decisionStatement', subStepIndex: 2 })).toBe(2);
      expect(subStepToNumber({ step: 'decisionStatement' })).toBe(4);
    });

    it('works for objectives', () => {
      const offset = DECISION_STATEMENT_STEPS.length;
      expect(subStepToNumber({ step: 'objectives', subStepIndex: 0 })).toBe(offset + 0);
      expect(subStepToNumber({ step: 'objectives', subStepIndex: 2 })).toBe(offset + 2);
      expect(subStepToNumber({ step: 'objectives' })).toBe(offset + 5);
    });

    it('works for alternatives', () => {
      const offset = DECISION_STATEMENT_STEPS.length + OBJECTIVES_STEPS.length;
      expect(subStepToNumber({ step: 'alternatives', subStepIndex: 0 })).toBe(offset + 0);
      expect(subStepToNumber({ step: 'alternatives', subStepIndex: 2 })).toBe(offset + 2);
      expect(subStepToNumber({ step: 'alternatives' })).toBe(offset + 7);
    });

    it('works for impactModel', () => {
      const offset = DECISION_STATEMENT_STEPS.length + OBJECTIVES_STEPS.length + ALTERNATIVES_STEPS.length;
      expect(subStepToNumber({ step: 'impactModel' })).toBe(offset);
      expect(subStepToNumber({ step: 'impactModel', subStepIndex: 2 })).toBe(offset);
    });

    it('works for results', () => {
      const offset = DECISION_STATEMENT_STEPS.length + OBJECTIVES_STEPS.length + ALTERNATIVES_STEPS.length + 1;
      expect(subStepToNumber({ step: 'results', subStepIndex: 0 })).toBe(offset + 0);
      expect(subStepToNumber({ step: 'results', subStepIndex: 1 })).toBe(offset + 1);
      expect(subStepToNumber({ step: 'results' })).toBe(offset + 2);
    });
  });

  describe('numberToSubStep', () => {
    it('works for decisionStatement', () => {
      expect(numberToSubStep(0)).toEqual({ step: 'decisionStatement', subStepIndex: 0 });
      expect(numberToSubStep(2)).toEqual({ step: 'decisionStatement', subStepIndex: 2 });
      expect(numberToSubStep(4)).toEqual({ step: 'decisionStatement' });
    });

    it('works for objectives', () => {
      const offset = DECISION_STATEMENT_STEPS.length;
      expect(numberToSubStep(offset + 0)).toEqual({ step: 'objectives', subStepIndex: 0 });
      expect(numberToSubStep(offset + 2)).toEqual({ step: 'objectives', subStepIndex: 2 });
      expect(numberToSubStep(offset + 5)).toEqual({ step: 'objectives' });
    });

    it('works for alternatives', () => {
      const offset = DECISION_STATEMENT_STEPS.length + OBJECTIVES_STEPS.length;
      expect(numberToSubStep(offset + 0)).toEqual({ step: 'alternatives', subStepIndex: 0 });
      expect(numberToSubStep(offset + 2)).toEqual({ step: 'alternatives', subStepIndex: 2 });
      expect(numberToSubStep(offset + 7)).toEqual({ step: 'alternatives' });
    });

    it('works for impactModel', () => {
      const offset = DECISION_STATEMENT_STEPS.length + OBJECTIVES_STEPS.length + ALTERNATIVES_STEPS.length;
      expect(numberToSubStep(offset)).toEqual({ step: 'impactModel' });
    });

    it('works for results', () => {
      const offset = DECISION_STATEMENT_STEPS.length + OBJECTIVES_STEPS.length + ALTERNATIVES_STEPS.length + 1;
      expect(numberToSubStep(offset)).toEqual({ step: 'results', subStepIndex: 0 });
      expect(numberToSubStep(offset + 1)).toEqual({ step: 'results', subStepIndex: 1 });
      expect(numberToSubStep(offset + 2)).toEqual({ step: 'results' });
    });
  });
});
