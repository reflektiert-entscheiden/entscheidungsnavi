import { DecisionData } from '../decision-data';
import { NAVI_STEP_ORDER, NaviSubStep } from '../classes/navi-step';
import { OBJECTIVES_STEPS } from '../classes/objective/objective-aspects';
import { ALTERNATIVES_STEPS } from '../classes/alternative/hint-alternatives';
import { DECISION_STATEMENT_STEPS } from '../classes/decision-statement';

const RESULTS_SUBSTEP_COUNT = 2;

/**
 * Up until which point the navi has been completed. This tracks the progress up until
 * the objective weighting. The returned step is the last step for which data has been entered.
 */
export function calculateCurrentProgress(data: DecisionData): NaviSubStep {
  if (data.resultSubstepProgress != null) {
    return {
      step: 'results',
      subStepIndex: data.resultSubstepProgress === RESULTS_SUBSTEP_COUNT ? undefined : data.resultSubstepProgress,
    };
  }

  if (data.outcomes.length > 0 && data.outcomes.some(ocForAlt => ocForAlt.length > 0 && ocForAlt.some(oc => !oc.isEmpty))) {
    return { step: 'impactModel' };
  }

  const alternativesSubstepIndex = ALTERNATIVES_STEPS.indexOf(data.hintAlternatives.subStepProgression);
  if (alternativesSubstepIndex > -1) {
    if (alternativesSubstepIndex === ALTERNATIVES_STEPS.length - 1) {
      return { step: 'alternatives' };
    } else {
      return { step: 'alternatives', subStepIndex: alternativesSubstepIndex };
    }
  }

  const objectivesSubstepIndex = OBJECTIVES_STEPS.indexOf(data.objectiveAspects.subStepProgression);
  if (objectivesSubstepIndex > -1) {
    if (objectivesSubstepIndex === OBJECTIVES_STEPS.length - 1) {
      return { step: 'objectives' };
    } else {
      return { step: 'objectives', subStepIndex: objectivesSubstepIndex };
    }
  }

  const decisionStatementSubstepIndex = DECISION_STATEMENT_STEPS.indexOf(data.decisionStatement.subStepProgression);
  if (decisionStatementSubstepIndex > -1) {
    if (decisionStatementSubstepIndex === DECISION_STATEMENT_STEPS.length - 1) {
      return { step: 'decisionStatement' };
    } else {
      return { step: 'decisionStatement', subStepIndex: decisionStatementSubstepIndex };
    }
  }

  return { step: 'decisionStatement', subStepIndex: 0 };
}

export const NAVI_SUB_STEP_COUNT =
  DECISION_STATEMENT_STEPS.length -
  1 +
  OBJECTIVES_STEPS.length -
  1 +
  ALTERNATIVES_STEPS.length -
  1 +
  RESULTS_SUBSTEP_COUNT +
  NAVI_STEP_ORDER.length;

/**
 * Maps navisubsteps to a continuous number range. Main steps are ordered after their respective hints.
 * This might change between releases, so it should not be stored.
 *
 * Example:
 *   decision-statement hints get numbers 0 to 6. decision-statement main gets 7. first objective hint gets 8.
 * @param step - The step to convert to a number
 */
export function subStepToNumber(step: NaviSubStep): number {
  // Main steps lay behind their respective hints
  const mainStepIndex = NAVI_STEP_ORDER.indexOf(step.step);
  const completedHintsIndex = mainStepIndex + (step.subStepIndex == null ? 1 : 0);

  let completedSubSteps = 0;
  if (completedHintsIndex > 0) {
    completedSubSteps += DECISION_STATEMENT_STEPS.length - 1;
  }
  if (completedHintsIndex > 1) {
    completedSubSteps += OBJECTIVES_STEPS.length - 1;
  }
  if (completedHintsIndex > 2) {
    completedSubSteps += ALTERNATIVES_STEPS.length - 1;
  }
  if (completedHintsIndex > 4) {
    completedSubSteps += RESULTS_SUBSTEP_COUNT;
  }

  if (mainStepIndex !== 3) {
    completedSubSteps += step.subStepIndex ?? 0;
  }

  return mainStepIndex + completedSubSteps;
}

/**
 * Reverse of {@link subStepToNumber}. Transforms a step number into the corresponding substep.
 *
 * @param stepNumber - The step number to transform
 */
export function numberToSubStep(stepNumber: number): NaviSubStep {
  stepNumber = Math.min(stepNumber, NAVI_SUB_STEP_COUNT - 1);

  if (stepNumber > DECISION_STATEMENT_STEPS.length - 1) {
    stepNumber -= DECISION_STATEMENT_STEPS.length;
  } else {
    return { step: 'decisionStatement', subStepIndex: stepNumber === DECISION_STATEMENT_STEPS.length - 1 ? undefined : stepNumber };
  }
  if (stepNumber > OBJECTIVES_STEPS.length - 1) {
    stepNumber -= OBJECTIVES_STEPS.length;
  } else {
    return { step: 'objectives', subStepIndex: stepNumber === OBJECTIVES_STEPS.length - 1 ? undefined : stepNumber };
  }
  if (stepNumber > ALTERNATIVES_STEPS.length - 1) {
    stepNumber -= ALTERNATIVES_STEPS.length;
  } else {
    return { step: 'alternatives', subStepIndex: stepNumber === ALTERNATIVES_STEPS.length - 1 ? undefined : stepNumber };
  }
  if (stepNumber > 0) {
    stepNumber--;
  } else {
    return { step: 'impactModel' };
  }

  return { step: 'results', subStepIndex: stepNumber >= RESULTS_SUBSTEP_COUNT ? undefined : stepNumber };
}
