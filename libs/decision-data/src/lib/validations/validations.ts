import { DecisionStatement } from '../classes/decision-statement';
import { ErrorMsg } from '../classes/error';
import { Objective, ObjectiveType } from '../classes/objective/objective';
import { SimpleUserDefinedInfluenceFactor } from '../classes/influence-factor/simple-user-defined-influence-factor';
import { CompositeUserDefinedInfluenceFactor, UserDefinedInfluenceFactor } from '../classes/influence-factor/composite-influence-factor';
import { ObjectiveWeight } from '../classes/objective-weight';
import { Weights } from '../classes/weights';
import { Alternative } from '../classes/alternative/alternative';
import { Outcome } from '../classes/outcome/outcome';
import { ObjectiveInput } from '../classes/objective/objective-input';

export function validateDecisionStatement(decisionStatement: DecisionStatement): [boolean, ErrorMsg[]] {
  if (!decisionStatement.statement) {
    return [false, [{ code: 105 }]]; // A decision statement must be present
  }
  return [true, null];
}

export function validateObjectives(objectives: Objective[]): [boolean, ErrorMsg[]] {
  // require at least one objective
  if (objectives.length < 1) {
    return [false, [{ code: 110 }]]; // 'Es muss mindestens ein Ziel definiert werden.']; // 110
  }

  for (const objective of objectives) {
    if (!objective.name || objective.name.length < 1) {
      return [false, [{ code: 111 }]]; // 'Jedes Ziel muss einen Namen haben.']; // 111
    }
  }
  return [true, null];
}

export function validateObjectiveScales(objectives: Objective[]): [boolean, ErrorMsg[]] {
  for (const objective of objectives) {
    if (objective.objectiveType == null) {
      return [false, [{ code: 112 }]]; // 'Der Typ des Ziels muss gesetzt sein.']; // 112
    }
    switch (objective.objectiveType) {
      case ObjectiveType.Numerical: {
        if (
          objective.numericalData.from == null ||
          objective.numericalData.to == null ||
          isNaN(objective.numericalData.from) ||
          isNaN(objective.numericalData.to)
        ) {
          return [false, [{ code: 113 }]]; // 'Ausprägungen numerischer Ziele müssen Zahlen sein.']; // 113
        }
        break;
      }
      case ObjectiveType.Verbal: {
        if (objective.verbalData.options == null || objective.verbalData.options.some(opt => opt == null || opt === '')) {
          return [false, [{ code: 114 }]]; // 'Alle Ausprägungen eines Ziels müssen benannt werden.']; // 114
        }
        if (objective.verbalData.options.length < 2) {
          return [false, [{ code: 115 }]]; // 'Verbale Ziele müssen mindestens 2 Ausprägungen haben.']; // 115
        }
        break;
      }
      case ObjectiveType.Indicator: {
        if (objective.indicatorData.indicators == null || objective.indicatorData.indicators.length === 0) {
          return [false, [{ code: 117 }]]; // Mindestens ein Indikator muss festgelegt sein
        }
        if (!objective.indicatorData.isScaleValid) {
          return [false, [{ code: 118 }]]; // Die Grenzwerte von Indikatoren müssen ein Intervall aufspannen
        }
        for (const indicator of objective.indicatorData.indicators) {
          if (!indicator.name) {
            return [false, [{ code: 116 }]]; // Indikatoren müssen benannt werden.
          }
          if (
            indicator.min == null ||
            indicator.max == null ||
            isNaN(indicator.min) ||
            isNaN(indicator.max) ||
            indicator.min === indicator.max
          ) {
            return [false, [{ code: 118 }]]; // Die Grenzwerte von Indikatoren müssen ein Intervall aufspannen
          }
          if (indicator.coefficient == null || isNaN(indicator.coefficient) || indicator.coefficient < 0) {
            return [false, [{ code: 119 }]]; // Koeffizienten von Indikatoren müssen nicht-negative Zahlen sein
          }
        }
        break;
      }
    }
  }

  return [true, null];
}

export function validateInfluenceFactor(uf: UserDefinedInfluenceFactor): [boolean, ErrorMsg[]] {
  if (uf == null) {
    return [false, null];
  }
  if (!uf.name || uf.name.length < 1) {
    return [false, [{ code: 120 }]]; // 'Bitte benenne den InfluenceFactor']; // 120
  }
  if (uf instanceof SimpleUserDefinedInfluenceFactor) {
    if (uf.states.length < 2) {
      return [false, [{ code: 121 }]]; // 'Einflussfaktoren müssen mindestens 2 Zustände haben.']; // 121
    }
    for (const state of uf.states) {
      if (!state.name || state.name.length < 1) {
        return [false, [{ code: 123 }]]; // 'Bitte benenne alle Zustände.']; // 123
      }
    }
  }
  if (!(uf instanceof CompositeUserDefinedInfluenceFactor) && !uf.checkProbabilities()) {
    return [false, [{ code: 122 }]]; // The probabilities are invalid
  }
  return [true, null];
}

export function validateInfluenceFactors(influenceFactors: Array<UserDefinedInfluenceFactor>): [boolean, ErrorMsg[], number] {
  for (let i = 0; i < influenceFactors.length; i++) {
    const [valid, errorMsg] = validateInfluenceFactor(influenceFactors[i]);
    if (!valid) {
      return [valid, errorMsg, i];
    }
  }
  return [true, null, null];
}

export function validateAlternatives(alternatives: Alternative[]): [boolean, ErrorMsg[]] {
  if (alternatives.length < 2) {
    return [false, [{ code: 130 }]]; // 'Es müssen mindestens zwei Alternativen definiert werden']; // 130
  }
  for (const alternative of alternatives) {
    if (alternative.name == null || alternative.name.length < 1) {
      return [false, [{ code: 131 }]]; // 'Alle Alternativen müssen benannt werden']; // 131
    }
  }
  return [true, null];
}

export function validateUtilityFunction(objectives: Objective[], i: number): [boolean, ErrorMsg[]] {
  if (objectives.length < i) {
    return [false, [{ code: 140, args: [i + 1] }]]; // `Ziel ${i + 1} existiert nicht.`]; // 140  1
  }
  const objective = objectives[i];
  if (objective == null) {
    return [false, [{ code: 141, args: [i + 1] }]]; // `Das Ziel ${i + 1} ist nicht definiert.`]; // 141  1
  }
  switch (objective.objectiveType) {
    case ObjectiveType.Numerical: {
      const nf: number = +objective.numericalData.utilityfunction.c;
      if (nf == null || isNaN(nf)) {
        return [false, [{ code: 142, args: [objective.name] }]]; // `Die UtilityFunction für Ziel ${ziel.name} ist ungültig.`]; // 142  1
      }
      break;
    }
    case ObjectiveType.Verbal: {
      const v = objective.verbalData;
      if (v.options.length !== v.utilities.length) {
        return [false, [{ code: 143, args: [objective.name] }]]; // `Die Anzahl der Ausprägungen von Ziel ${ziel.name} stimmen
        // nicht mit der Anzahl der Nutzenwerte überein.`]; // 143  1
      }
      if (+v.utilities[0] !== 0) {
        return [false, [{ code: 144 }]]; // `Der Nutzen der schlechtesten Ausprägung muss 0 sein.`]; // 144
      }
      if (+v.utilities[v.utilities.length - 1] !== 100) {
        return [false, [{ code: 145 }]]; // `Der Nutzen der besten Ausprägung muss 100 sein.`]; // 145
      }
      for (let idx = 0; idx < v.utilities.length; idx++) {
        if (idx > 0 && v.utilities[idx] <= v.utilities[idx - 1]) {
          return [false, [{ code: 146 }]]; // `Der Nutzen einer Ausprägung muss größer sein als der Nutzen der
          // schlechteren Ausprägung.`]; // 146
        }
      }
      break;
    }
    case ObjectiveType.Indicator: {
      const nf: number = +objective.indicatorData.utilityfunction.c;
      if (nf == null || isNaN(nf)) {
        return [false, [{ code: 142, args: [objective.name] }]]; // `Die UtilityFunction für Ziel ${ziel.name} ist ungültig.`]; // 142  1
      }
      break;
    }
  }
  return [true, null];
}

export function validateUtilityFunctions(objectives: Objective[]): [boolean, ErrorMsg[], number] {
  for (let idx = 0; idx < objectives.length; idx++) {
    const [valid, errorMsg] = validateUtilityFunction(objectives, idx);
    if (!valid) {
      return [valid, errorMsg, idx];
    }
  }
  return [true, null, null];
}

export function validateTradeoffObjective(weights: Weights, objectives: Objective[]): [boolean, ErrorMsg[]] {
  const idx = weights.tradeoffObjectiveIdx;
  if (idx == null) {
    return [false, [{ code: 150 }]]; // 'Es muss ein Ziel gesetzt werden.']; // 150
  }
  if (idx >= objectives.length || idx < 0) {
    return [false, [{ code: 151 }]]; // 'Das ausgewählte Ziel ist ungültig.']; // 151
  }
  return [true, null];
}

export function validateWeights(weights: ObjectiveWeight[]): [boolean, ErrorMsg[]] {
  for (let i = 0; i < weights.length; i++) {
    if (weights[i].value === undefined) {
      return [false, [{ code: 163 }]];
    }
    if (weights[i].value < 0) {
      return [false, [{ code: 160 }]]; // 'Zielgewichtungen müssen mindestens 0 sein.']; // 160
    }
    if (weights[i].value > Weights.MAX_WEIGHT) {
      return [false, [{ code: 161 }]]; // 'Zielgewichtungen dürfen höchstens 1000 sein.']; // 161
    }
  }
  if (!weights.some(el => el.value > 0)) {
    return [false, [{ code: 164 }]]; // Mindestens ein Zielgewicht muss positiv sein
  }
  return [true, null];
}

export function validateWeightsPost(weights: Weights, objectives: Objective[], ignoreMissing = true): [boolean, ErrorMsg[], number] {
  // does not check for tradeoffPolicy / unverified_gewichtung_post
  for (let i = 0; i < weights.tradeoffWeights.length; i++) {
    if (weights.tradeoffWeights[i]) {
      if (weights.tradeoffWeights[i].value < 0) {
        return [false, [{ code: 160 }], null]; // 'Zielgewichtungen müssen mindestens 0 sein.']; // 160
      }
      if (weights.tradeoffWeights[i].value > Weights.MAX_WEIGHT) {
        return [false, [{ code: 161 }], null]; // 'Zielgewichtungen dürfen höchstens 1000 sein.']; // 161
      }
    } else if (!ignoreMissing) {
      return [false, [{ code: 162, args: [objectives[i].name] }], i];
    }
  }
  return [true, null, null];
}

export function validateValue(objectiveValues: ObjectiveInput, objective: Objective): [boolean, ErrorMsg[]] {
  if (objectiveValues.some(values => values.some(v => v == null))) {
    return [false, [{ code: 170 }]]; // 'Alle Ausprägungen müssen gesetzt sein.']; // 170
  }
  if (objective.objectiveType === ObjectiveType.Indicator) {
    if (objective.indicatorData.indicators.length !== objectiveValues.length) {
      return [false, [{ code: 178 }]]; // Anzahl der Werte muss Anzahl der Indikatoren entsprechen
    }

    const resultValue = objective.indicatorData.aggregationFunction(objectiveValues);

    if (Number.isNaN(resultValue)) {
      return [false, [{ code: 175 }]];
    }

    if (!objective.getRangeInterval().includes(resultValue)) {
      return [false, [{ code: 186 }]]; // Resultierender Wert muss innerhalb der Skala liegen
    }

    let [res, err]: [boolean, ErrorMsg[]] = [true, null];
    const foundError = objectiveValues.find((val: number[], indicatorIdx: number) => {
      const ind = objective.indicatorData.indicators[indicatorIdx],
        von = ind.min,
        bis = ind.max;
      if (!ind.isVerbalized) {
        // validate values if indicator is not verbal (otherwise values are indexes)
        [res, err] = validateValueFromTo(val[0], von, bis);
      } else {
        for (let categoryIdx = 0; categoryIdx < val.length; categoryIdx++) {
          [res, err] = validateValueFromTo(val[categoryIdx], 0, ind.verbalIndicatorCategories[categoryIdx].stages.length - 1);
          if (!res) break; // index out of bounds
        }
      }
      return res === false;
    });
    if (foundError != null) {
      return [res, err];
    }
  } else {
    if (objectiveValues.length !== 1 || objectiveValues[0].length !== 1) {
      return [false, [{ code: 177 }]]; // Mehrere Indikatorwerte, obwohl die Skala keine Indikatorskala ist
    }
    if (objective.objectiveType === ObjectiveType.Numerical) {
      const von = objective.numericalData.from,
        bis = objective.numericalData.to;
      const [res, err] = validateValueFromTo(objectiveValues[0][0], von, bis);
      if (res === false) {
        return [res, err];
      }
    } else if (
      objective.objectiveType === ObjectiveType.Verbal &&
      (!Number.isInteger(objectiveValues[0][0]) || objectiveValues[0][0] < 1 || objectiveValues[0][0] > objective.verbalData.options.length)
    ) {
      return [false, [{ code: 175 }]]; // `Der angegebene Wert ist ungültig`]; // 175
    }
  }
  return [true, null];
}

export function validateValueFromTo(value: number, von: number, bis: number): [boolean, ErrorMsg[]] {
  if (bis > von && value > bis) {
    return [false, [{ code: 171, args: [bis] }]]; // `Werte dürfen nicht größer ${bis} sein.`]; // 171  1
  }
  if (von > bis && value > von) {
    return [false, [{ code: 172, args: [von] }]]; // `Werte dürfen nicht größer ${von} sein.`]; // 172  1
  }
  if (bis > von && value < von) {
    return [false, [{ code: 173, args: [von] }]]; // `Werte dürfen nicht kleiner ${von} sein.`]; // 173  1
  }
  if (von > bis && value < bis) {
    return [false, [{ code: 174, args: [bis] }]]; // `Werte dürfen nicht kleiner ${bis} sein.`]; // 174  1
  }
  return [true, null];
}

export function validateOutcomes(alternatives: Alternative[], objectives: Objective[], outcomes: Outcome[][]): [boolean, ErrorMsg[]] {
  if (outcomes.length < alternatives.length) {
    return [false, [{ code: 180 }]]; // 'Die Anzahl der Ausprägungen stimmt nicht mit der Anzahl an Alternativen
    // überein.']; // 180
  }
  for (let i = 0; i < alternatives.length; i++) {
    if (outcomes[i].length < objectives.length) {
      return [false, [{ code: 181, args: [alternatives[i].name] }]]; // `Die Anzahl der Ausprägungen stimmt nicht
      // mit der Anzahl der Ziele überein.
      // (Alternative ${data.alternatives[i].name})`]; // 181  1
    }
    for (let j = 0; j < objectives.length; j++) {
      if (!outcomes[i][j]) {
        return [false, [{ code: 182, args: [alternatives[i].name, objectives[j].name] }]]; // `Ausprägung
        // existiert nicht.
        // (Alternative
        // ${data.alternatives[i].name}, Ziel ${data.objectives[j].name})`]; // 182 2
      }

      const influenceFactor = outcomes[i][j].influenceFactor;
      if (influenceFactor) {
        if (outcomes[i][j].values.length < influenceFactor.stateCount) {
          return [false, [{ code: 183, args: [alternatives[i].name, objectives[j].name] }]];
          // `Die Anzahl der Werte stimmt nicht mit der Anzahl der Zustände überein. (Alternative
          // ${data.alternatives[i].name}, Ziel ${data.objectives[j].name})`]; // 183  2
        }
        for (let k = 0; k < influenceFactor.stateCount; k++) {
          const [valid, errorMsg] = validateValue(outcomes[i][j].values[k], objectives[j]);
          if (!valid) {
            return [
              false,
              errorMsg.concat({
                code: 184,
                args: [
                  alternatives[i].name,
                  objectives[j].name,
                  influenceFactor instanceof SimpleUserDefinedInfluenceFactor ? influenceFactor.states[k].name : k + '',
                ],
              }),
            ]; // + ` (Alternative ${data.alternatives[i].name}, Ziel ${data.objectives[j].name},
            // Zustand ${data.outcomes[i][j].influenceFactor.states[k].name})`)]; // 184  3
          }
        }
      } else {
        const [valid, errorMsg] = validateValue(outcomes[i][j].values[0], objectives[j]);
        if (!valid) {
          return [false, errorMsg.concat({ code: 185, args: [alternatives[i].name, objectives[j].name] })];
          // + ` (Alternative ${data.alternatives[i].name}, Ziel ${data.objectives[j].name})`)]; // 185 2
        }
      }
    }
  }
  return [true, null];
}

/**
 * combine validation results
 */
export function flattenErrors(errors: Array<[boolean, ErrorMsg[]] | [boolean, ErrorMsg[], number]>): [boolean, ErrorMsg[]] {
  let valid = true;
  const msgs: ErrorMsg[] = [];
  for (const error of errors) {
    valid = valid && error[0];
    if (error[1] != null) {
      msgs.push(...error[1]);
    }
  }
  return [valid, msgs];
}

export function getDefaultValueFor(objective: Objective): ObjectiveInput {
  switch (objective.objectiveType) {
    case ObjectiveType.Numerical:
      return [[objective.numericalData.from]];
    case ObjectiveType.Indicator:
      return objective.indicatorData.indicators.map(indicator => {
        return !indicator.isVerbalized ? [indicator.min] : Array.from({ length: indicator.verbalIndicatorCategories.length }, _ => 0);
      });
    case ObjectiveType.Verbal:
      return [[1]];
  }
}
