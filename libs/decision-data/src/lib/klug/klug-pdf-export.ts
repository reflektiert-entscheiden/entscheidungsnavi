import { KlugPdfExportImages } from '@entscheidungsnavi/api-types';
import { cmToP } from '@entscheidungsnavi/tools';
import { TDocumentDefinitions, Content, Node, Style } from 'pdfmake/interfaces';

export function createKlugPDFExport(token: string, images: KlugPdfExportImages) {
  const content: Content[] = [];

  content.push(
    { text: 'Klug-Projekt', fontSize: 26, bold: true, margin: [0, 50, 0, 0], alignment: 'center' },
    {
      stack: [
        { text: [{ text: 'Projekt Token', bold: true }, ': ', token] },
        { text: [{ text: 'Datum', bold: true }, ': ', new Date().toLocaleDateString('de-DE')] },
      ],
      alignment: 'center',
      margin: [0, 40, 0, 40],
      lineHeight: 1.5,
    },
  );
  content.push({
    text: `Welche Entscheidung möchte ich treffen?`,
    style: 'header',
    margin: [0, 10, 0, 0],
  });
  content.push({ image: images.decisionStatementImage, width: 480, margin: [0, 10, 0, 0] });

  content.push({
    text: `Was sind meine Ziele?`,
    style: 'header',
    margin: [0, 10, 0, 0],
  });
  content.push({ image: images.objectiveListImage, width: 480, margin: [0, 10, 0, 0] });

  content.push({
    text: `Was sind meine Optionen?`,
    style: 'header',
    margin: [0, 10, 0, 0],
  });
  content.push({ image: images.alternativeListImage, width: 480, margin: [0, 10, 0, 0] });

  content.push({
    text: `Wie gut passen meine Optionen zu meinen Zielen?`,
    alignment: 'center',
    pageOrientation: 'landscape',
    pageBreak: 'before',
    style: 'header',
    margin: [0, 10, 0, 0],
  });
  content.push({
    image: images.assessmentGraphImage,
    alignment: 'center',
    fit: [700, 400],
    margin: [0, 10, 0, 0],
  });

  content.push({
    text: `Welches Ziel ist mir wie wichtig?`,
    alignment: 'center',
    style: 'header',
    margin: [0, 10, 0, 0],
  });
  content.push({
    image: images.comparisonGraphImage,
    alignment: 'center',
    fit: [700, 400],
    margin: [0, 10, 0, 0],
    pageOrientation: 'portrait',
    pageBreak: 'after',
  });

  content.push({
    text: `Welche Option ist die Beste?`,
    style: 'header',
    margin: [0, 10, 0, 0],
  });
  content.push({ image: images.evaluationGraphImage, width: 480, margin: [0, 10, 0, 0] });

  const defaultStyle: Style = {
    fontSize: 11,
    lineHeight: 1.08,
    alignment: 'justify',
    color: 'black',
  };

  // The document template
  const document: TDocumentDefinitions = {
    info: {
      title: 'Mein KLUGentscheiden Projekt',
    },
    pageSize: 'A4',
    pageOrientation: 'portrait',
    pageMargins: [cmToP(2.5), cmToP(2.5), cmToP(2.5), cmToP(2.0)],
    content: content,
    footer: currentPage => ({
      text: `${currentPage}`,
      alignment: 'center',
    }),
    pageBreakBefore(currentNode: Node, followingNodesOnPage?: Node[], _nodesOnNextPage?: Node[], _previousNodesOnPage?: Node[]) {
      // If there is no content after a heading break the page
      return currentNode.style == 'header' && followingNodesOnPage.length === 1;
    },
    defaultStyle,
    styles: {
      default: defaultStyle,
      header: {
        fontSize: 16,
        alignment: 'left',
        color: 'black',
      },
      subheader: {
        fontSize: 13,
        alignment: 'left',
        color: '#2e74b5',
      },
    },
  };

  return document;
}
