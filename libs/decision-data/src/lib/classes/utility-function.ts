export class UtilityFunction {
  constructor(
    public c = 0,
    public precision = 1,
    public width?: number,
    public level?: number,
    public explanation = '',
  ) {}
}
