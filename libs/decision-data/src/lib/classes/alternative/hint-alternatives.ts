import { isEqual, omit, range } from 'lodash';
import { assertUnreachable, moveInArray } from '@entscheidungsnavi/tools';
import { v4 as uuidv4 } from 'uuid';
import { DecisionData } from '../../decision-data';
import { NotePage } from '../note-page';
import { ScrewConfiguration } from './screw-configuration-provider';

export class Screw {
  constructor(
    public name: string = '',
    public uuid = uuidv4(),
    public states: string[] = [],
    public comment = '',
  ) {}

  clone(): Screw {
    return new Screw(this.name, this.uuid, [...this.states], this.comment);
  }

  isEqual(otherScrew: Screw): boolean {
    return isEqual(this, otherScrew);
  }
}

export type AlternativesStep = (typeof ALTERNATIVES_STEPS)[number];

export const ALTERNATIVES_STEPS = [
  'KNOWN_ALTERNATIVES',
  'FINDING_WEAK_POINTS',
  'OBJECTIVE_FOCUSED_SEARCH',
  'ASK_OTHER_PEOPLE',
  'IMPORTANT_DESIGN_PARAMETERS',
  'COMBINE_SENSIBLY',
  'ORDER_INTUITIVELY',
  'RESULT',
] as const;

interface ScrewStateAddition {
  opCode: 'addition';
  stateIndex: number;
}

interface ScrewStateMove {
  opCode: 'move';
  fromStateIndex: number;
  toStateIndex: number;
}

interface ScrewStateDeletion {
  opCode: 'deletion';
  stateIndex: number;
}

export type ScrewStateOp = ScrewStateAddition | ScrewStateMove | ScrewStateDeletion;

// step 3 (alternatives) hint data
export class HintAlternatives {
  static adjustScrewConfigurationOnAppendedScrew(screwConfiguration: ScrewConfiguration) {
    screwConfiguration.push(null);
  }

  static adjustScrewConfigurationOnUpdatedScrew(
    screwConfiguration: ScrewConfiguration,
    screwIndex: number,
    operationsForUpdate: ScrewStateOp[],
    currentNumberOfStates: number,
  ) {
    // Not-defined stays not-defined.
    if (screwConfiguration[screwIndex] === null) return;

    const newStateOrdering = range(currentNumberOfStates);

    operationsForUpdate.forEach(op => {
      if (op.opCode === 'addition') {
        // Place dummy that cannot be referenced (must not be a valid state index).
        newStateOrdering.splice(op.stateIndex, 0, -1);
      } else if (op.opCode === 'move') {
        moveInArray(newStateOrdering, op.fromStateIndex, op.toStateIndex);
      } else if (op.opCode === 'deletion') {
        newStateOrdering.splice(op.stateIndex, 1);
      } else {
        assertUnreachable(op);
      }
    });

    // If the state could not be found it means that it was deleted. Make it non-defined.
    const newStateIndex = newStateOrdering.indexOf(screwConfiguration[screwIndex]);
    screwConfiguration[screwIndex] = newStateIndex === -1 ? null : newStateIndex;
  }

  static adjustScrewConfigurationOnScrewMove(screwConfiguration: ScrewConfiguration, screwIndex: number, destinationIndex: number) {
    moveInArray(screwConfiguration, screwIndex, destinationIndex);
  }

  static adjustScrewConfigurationOnScrewDeletion(screwConfiguration: number[], screwIndex: number) {
    screwConfiguration.splice(screwIndex, 1);
  }

  public subStepProgression: AlternativesStep | null = null;

  public ideas: NotePage; // ideas for objective-focused alternatives
  public screws: Screw[] = [];

  constructor(public decisionData: DecisionData) {}

  toJSON() {
    return omit(this, ['decisionData']);
  }

  appendScrew(name?: string, positions?: readonly string[]) {
    const uuid = uuidv4();
    this.screws.push(new Screw(name, uuid, positions?.slice()));
    this.decisionData.alternatives.forEach(alternative =>
      HintAlternatives.adjustScrewConfigurationOnAppendedScrew(alternative.screwConfiguration),
    );
  }

  updateScrew(screwIndex: number, editedScrew: Screw, operationsForUpdate: ScrewStateOp[]) {
    this.decisionData.alternatives.forEach(alternative =>
      HintAlternatives.adjustScrewConfigurationOnUpdatedScrew(
        alternative.screwConfiguration,
        screwIndex,
        operationsForUpdate,
        this.screws[screwIndex].states.length,
      ),
    );

    this.screws[screwIndex].states = editedScrew.states;
    this.screws[screwIndex].name = editedScrew.name;
  }

  moveScrew(screwIndex: number, destinationIndex: number) {
    moveInArray(this.screws, screwIndex, destinationIndex);
    this.decisionData.alternatives.forEach(alternative =>
      HintAlternatives.adjustScrewConfigurationOnScrewMove(alternative.screwConfiguration, screwIndex, destinationIndex),
    );
  }

  deleteScrew(screwIndex: number) {
    this.screws.splice(screwIndex, 1);
    this.decisionData.alternatives.forEach(alternative =>
      HintAlternatives.adjustScrewConfigurationOnScrewDeletion(alternative.screwConfiguration, screwIndex),
    );
  }

  initIdeas() {
    this.ideas = new NotePage();
    this.decisionData.objectives.forEach(() => {
      this.ideas.addNoteGroup([]);
    });
  }

  // called when adding an objective
  addObjective(position: number) {
    if (this.ideas) {
      this.ideas.addNoteGroup([], '', position);
    }
  }

  // called when removing an objective
  removeObjective(position: number) {
    if (this.ideas) {
      this.ideas.removeNoteGroup(position);
    }
  }
}
