import { v4 as uuidv4 } from 'uuid';
import { ScrewConfiguration, ScrewConfigurationProvider } from './screw-configuration-provider';

export class Alternative implements ScrewConfigurationProvider {
  static clone(alternative: Alternative): Alternative {
    return new Alternative(
      alternative.createdInSubStep,
      alternative.name,
      alternative.comment,
      (alternative.children || []).map(a => Alternative.clone(a)),
      alternative.screwConfiguration,
      alternative.uuid,
    );
  }

  constructor(
    public createdInSubStep = 0, // 0 for main step, 1-7 for the corresponding substep
    public name = '',
    public comment?: string,
    public children: Alternative[] = [],
    public screwConfiguration: ScrewConfiguration = [],
    public uuid = uuidv4(),
  ) {}

  clone(): Alternative {
    return Alternative.clone(this);
  }

  isGroup(): boolean {
    return this.children.length > 0;
  }

  addChild(child: Alternative = new Alternative()): Alternative {
    this.children.push(child);
    return child;
  }
  removeChild(idx: number): Alternative {
    const el: Alternative[] = this.children.splice(idx, 1);
    return el.length === 1 ? el[0] : null;
  }
}
