import { Subject } from 'rxjs';

export type GlobalVariablesMap = Map<string, number>;

export class GlobalVariables {
  readonly variablesChanged$ = new Subject<void>();

  constructor(public map: GlobalVariablesMap = new Map()) {}

  toJSON() {
    return { map: Array.from(this.map.entries()) };
  }
}
