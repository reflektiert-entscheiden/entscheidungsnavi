import { DECISION_STATEMENT_STEPS } from './decision-statement';
import { NaviStep } from './navi-step';
import { ALTERNATIVES_STEPS } from './alternative/hint-alternatives';
import { OBJECTIVES_STEPS } from './objective/objective-aspects';

// How many timers are in each navi step
export const TIMER_COUNT: Record<NaviStep, number> = {
  decisionStatement: DECISION_STATEMENT_STEPS.length,
  objectives: OBJECTIVES_STEPS.length,
  alternatives: ALTERNATIVES_STEPS.length,
  impactModel: 2,
  results: 3,
  finishProject: 1,
};

export class TimeRecording {
  timers: Record<NaviStep, { activeTime: number; totalTime: number }[]>;

  constructor(timers: Partial<Record<NaviStep, { activeTime: number; totalTime: number }[]>> = {}) {
    this.timers = {
      decisionStatement: timers.decisionStatement ?? emptyTrackingArray('decisionStatement'),
      objectives: timers.objectives ?? emptyTrackingArray('objectives'),
      alternatives: timers.alternatives ?? emptyTrackingArray('alternatives'),
      impactModel: timers.impactModel ?? emptyTrackingArray('impactModel'),
      results: timers.results ?? emptyTrackingArray('results'),
      finishProject: timers.finishProject ?? emptyTrackingArray('finishProject'),
    };
  }
}

function emptyTrackingArray(step: NaviStep) {
  return new Array(TIMER_COUNT[step]).fill(null).map(() => ({ activeTime: 0, totalTime: 0 }));
}
