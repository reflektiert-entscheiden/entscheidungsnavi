import { Weights } from './weights';

describe('Weights', () => {
  let weights: Weights;

  beforeEach(() => {
    weights = new Weights();
    weights.addObjective(0);
    weights.addObjective(0);
    weights.addObjective(0);
    weights.tradeoffObjectiveIdx = 0;
    weights.manualTradeoffs = [
      null,
      [
        [0, 1],
        [0.5, 0.5],
      ],
      [
        [0.1, 0.4],
        [0.2, 0.2],
      ],
    ];
    weights.explanations = ['comment1', 'comm2', 'c3'];
  });

  describe('changeTradeoffObjective', () => {
    it('clears the manual tradeoffs', () => {
      weights.changeTradeoffObjective(1);
      expect(weights.manualTradeoffs).toEqual([null, null, null]);
    });

    it('clears the explanations', () => {
      weights.changeTradeoffObjective(1);
      expect(weights.explanations).toEqual(['', '', '']);
    });
  });

  describe('addObjective', () => {
    it('updates the manual tradeoffs', () => {
      const before = weights.manualTradeoffs;
      weights.addObjective(1);
      expect(weights.manualTradeoffs.length).toBe(4);

      const newEntry = weights.manualTradeoffs.splice(1, 1);
      expect(newEntry).toEqual([null]);
      expect(weights.manualTradeoffs).toEqual(before);
    });

    it('updates the explanation', () => {
      weights.addObjective(1);
      expect(weights.explanations).toEqual(['comment1', '', 'comm2', 'c3']);
    });
  });

  describe('moveObjective', () => {
    it('updates the manual tradeoffs', () => {
      weights.moveObjective(1, 2);
      expect(weights.manualTradeoffs).toEqual([
        null,
        [
          [0.1, 0.4],
          [0.2, 0.2],
        ],
        [
          [0, 1],
          [0.5, 0.5],
        ],
      ]);
    });

    it('updates the explanations', () => {
      weights.moveObjective(1, 2);
      expect(weights.explanations).toEqual(['comment1', 'c3', 'comm2']);
    });
  });

  describe('removeObjective', () => {
    it('updates the manual tradeoffs', () => {
      weights.removeObjective(1);
      expect(weights.manualTradeoffs).toEqual([
        null,
        [
          [0.1, 0.4],
          [0.2, 0.2],
        ],
      ]);
    });

    it('clears manual tradeoffs when removing tradeoff objective', () => {
      weights.removeObjective(0);
      expect(weights.manualTradeoffs).toEqual([null, null]);
    });

    it('updates the explanations', () => {
      weights.removeObjective(1);
      expect(weights.explanations).toEqual(['comment1', 'c3']);
    });

    it('clears explanation when removing tradeoff objective', () => {
      weights.removeObjective(0);
      expect(weights.explanations).toEqual(['', '']);
    });
  });
});
