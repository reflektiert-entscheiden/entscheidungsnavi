import { Language } from '@entscheidungsnavi/api-types';

export const NAVI_STEP_ORDER = ['decisionStatement', 'objectives', 'alternatives', 'impactModel', 'results', 'finishProject'] as const;
export type NaviStep = (typeof NAVI_STEP_ORDER)[number];

export function isNaviStep(value: string): value is NaviStep {
  return (NAVI_STEP_ORDER as readonly string[]).indexOf(value) !== -1;
}

export interface NaviSubStep {
  step: NaviStep;
  // Set to undefined for the main step
  subStepIndex?: number;
}

export interface NaviStepNames {
  name: string;
  subSteps: string[];
}

export const STEP_NAMES: {
  [key in Language]: {
    [key in NaviStep]: NaviStepNames;
  };
} = {
  de: {
    decisionStatement: {
      name: 'Entscheidungsfrage',
      subSteps: ['Erste Formulierung', 'Grundlegende Werte', 'Impulsfragen', 'Überprüfung'],
    },
    objectives: {
      name: 'Fundamentalziele',
      subSteps: ['Erstes Brainstorming', 'Weitere Überlegungen', 'Erste Zielhierarchie', 'Beispiele und Vorschläge', 'Überprüfung'],
    },
    alternatives: {
      name: 'Alternativen',
      subSteps: [
        'Bekannte Alternativen',
        'Finden von Schwachpunkten',
        'Zielfokussierte Suche',
        'Befragen anderer',
        'Wichtige Stellhebel',
        'Sinnvolles Zusammenfassen',
        'Intuitives Ordnen',
      ],
    },
    impactModel: {
      name: 'Wirkungsmodell',
      subSteps: [],
    },
    results: {
      name: 'Evaluation',
      subSteps: ['Nutzenfunktionen', 'Zielgewichte'],
    },
    finishProject: {
      name: 'Abschlussbetrachtung',
      subSteps: [],
    },
  },
  en: {
    decisionStatement: {
      name: 'Decision Statement',
      subSteps: ['First Draft', 'Fundamental Values', 'Impulse Questions', 'Review'],
    },
    objectives: {
      name: 'Fundamental Objectives',
      subSteps: ['Initial Brainstorming', 'Further Considerations', 'Initial Objective Hierarchy', 'Examples and Suggestions', 'Review'],
    },
    alternatives: {
      name: 'Alternatives',
      subSteps: [
        'Known Alternatives',
        'Finding Weak Points',
        'Objective-focused Search',
        'Questioning others',
        'Strategy Table',
        'Combine Sensibly',
        'Order Intuitively',
      ],
    },
    impactModel: {
      name: 'Consequences Table',
      subSteps: [],
    },
    results: {
      name: 'Evaluation',
      subSteps: ['Utility Functions', 'Objective Weights'],
    },
    finishProject: {
      name: 'Final Review',
      subSteps: [],
    },
  },
};
