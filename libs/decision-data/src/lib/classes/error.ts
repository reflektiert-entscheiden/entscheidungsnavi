/* ***** Decision Data ***** */
// decision statement
type DecisionStatementErrorCodes = 105;

// objectives
type ObjectiveErrorCodes = 110 | 111 | 112 | 113 | 114 | 115 | 116 | 117 | 118 | 119;

// influence factors
type UfErrorCodes = 120 | 121 | 122 | 123;

// alternatives
type AlErrorCodes = 130 | 131;

// utility functions
type UfErrorCodes0 = 144 | 145 | 146;
type UfErrorCodes1num = 140 | 141;
type UfErrorCodes1str = 142 | 143;

// tradeoff objective
type TradeoffObjectiveErrorCodes = 150 | 151;

// weights
type WeightErrorCodes = 160 | 161 | 163 | 164;

type TradeoffWeightErrorCodes = 162;

// outcome value
type ValueErrorCodes0 = 170 | 175 | 176 | 177 | 178 | 186;
type ValueErrorCodes1num = 171 | 172 | 173 | 174;

// outcomes
type OutcomeErrorCodes0 = 180;
type OutcomeErrorCodes1str = 181;
type OutcomeErrorCodes2str = 182 | 183 | 185;
type OutcomeErrorCodes3str = 184;

/* ***** other error codes ***** */
type OtherErrorCodes = 200 | 201;

/* ***** separate interface for different argument types ***** */
// error codes without arguments
export interface EC0 {
  code:
    | DecisionStatementErrorCodes
    | ObjectiveErrorCodes
    | UfErrorCodes
    | AlErrorCodes
    | UfErrorCodes0
    | TradeoffObjectiveErrorCodes
    | WeightErrorCodes
    | ValueErrorCodes0
    | OutcomeErrorCodes0
    | OtherErrorCodes;
}

// error codes with 1 number argument
export interface EC1num {
  code: UfErrorCodes1num | ValueErrorCodes1num;
  args: [number];
}

// error codes with 1 string argument
export interface EC1str {
  code: UfErrorCodes1str | OutcomeErrorCodes1str | TradeoffWeightErrorCodes;
  args: [string];
}

// error codes with 2 string arguments
export interface EC2str {
  code: OutcomeErrorCodes2str;
  args: [string, string];
}

// error codes with 3 string arguments
export interface EC3str {
  code: OutcomeErrorCodes3str;
  args: [string, string, string];
}

// combination of interfaces
export type ErrorMsg = EC0 | EC1num | EC1str | EC2str | EC3str;
