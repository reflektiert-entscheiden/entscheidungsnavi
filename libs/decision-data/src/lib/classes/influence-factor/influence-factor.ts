import { UserDefinedInfluenceFactor } from './composite-influence-factor';
import { PredefinedInfluenceFactor } from './predefined-influence-factor';

export type InfluenceFactor = UserDefinedInfluenceFactor | PredefinedInfluenceFactor;
