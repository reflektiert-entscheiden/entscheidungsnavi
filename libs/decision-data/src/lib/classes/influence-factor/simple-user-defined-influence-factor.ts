import { cloneDeep, sum } from 'lodash';
import { v4 as uuidv4 } from 'uuid';
import { normalizeDiscrete } from '@entscheidungsnavi/tools/math/normalize';
import { ObjectiveInput } from '../objective/objective-input';
import { BaseInfluenceFactor, InfluenceFactorScenario, InfluenceFactorState } from './base-influence-factor';

export class UserDefinedState implements InfluenceFactorState {
  constructor(
    public name: string,
    public probability: number,
    public comment?: string,
  ) {}
}

export interface UserDefinedScenario extends InfluenceFactorScenario {
  value: ObjectiveInput;
  stateIndex: number;
  probability: number;
}

export class SimpleUserDefinedInfluenceFactor implements BaseInfluenceFactor {
  static readonly P_MAX = 50;
  static readonly P_DEFAULT = 10;

  get stateCount() {
    return this.states.length;
  }

  constructor(
    public name = '',
    public states: UserDefinedState[] = [],
    public id = -1,
    public precision = SimpleUserDefinedInfluenceFactor.P_DEFAULT,
    public comment?: string,
    public uuid = uuidv4(),
  ) {
    // there have to be at least 2 states
    for (let i = states.length; i < 2; i++) {
      this.states.push({ name: '', probability: 0 });
    }
  }

  probabilitiesSum(): number {
    return sum(this.states.map(state => state.probability ?? 0));
  }

  checkProbabilities(): boolean {
    return checkProbabilities(this.states.map(state => state.probability ?? 0));
  }

  normalizeProbabilities(): void {
    if (!this.checkProbabilities()) {
      normalizeProbabilities(this.states);
    }
  }

  getStates() {
    return this.states;
  }

  generateScenarios(outcomeValues: ObjectiveInput[]) {
    return generateUserDefinedScenarios(
      outcomeValues,
      this.states.map(state => state.probability / 100),
    );
  }

  iterateScenarios(outcomeValues: ObjectiveInput[], callback: (scenario: InfluenceFactorScenario) => void, _optimizeStates: boolean) {
    iterateUserDefinedScenarios(
      outcomeValues,
      this.states.map(state => state.probability / 100),
      callback,
    );
  }
}

/**
 * Return true, if the sum is 100 and every probability is a whole number.
 *
 * @param probabilities - The array of probabilities
 */
export function checkProbabilities(probabilities: number[]) {
  // We allow the sum to deviate slightly from 100 to alleviate floating point inaccuracy
  return Math.abs(100 - sum(probabilities)) < 1e-10 && probabilities.every(probability => probability >= 0);
}

/**
 * Normalize the probabilities so that they sum up to 100.
 * Keep the round-off error as small as possible.
 *
 * @param states - The array of states. It is modified in place.
 */
export function normalizeProbabilities(states: UserDefinedState[]) {
  const probabilities = states.map(state => state.probability ?? 0);
  const newProbabilities = normalizeDiscrete(probabilities, 100, 1);
  states.forEach((state, idx) => (state.probability = newProbabilities[idx]));
}

/**
 * Generate an array of all possible scenarios for a user-defined influence factor.
 * This corresponds to one scenario per state.
 *
 * @param outcomeValues - Outcome values for every state
 * @param stateProbabilities - Probability for every state in [0, 1]
 */
export function generateUserDefinedScenarios(outcomeValues: ObjectiveInput[], stateProbabilities: number[]) {
  const scenarios: UserDefinedScenario[] = [];
  iterateUserDefinedScenarios(outcomeValues, stateProbabilities, scenario => scenarios.push(cloneDeep(scenario)));
  return scenarios;
}

export function iterateUserDefinedScenarios(
  outcomeValues: ObjectiveInput[],
  stateProbabilities: number[],
  callback: (scenario: UserDefinedScenario) => void,
) {
  for (let stateIndex = 0; stateIndex < stateProbabilities.length; stateIndex++) {
    callback({
      stateIndex,
      value: outcomeValues[stateIndex],
      probability: stateProbabilities[stateIndex],
    });
  }
}
