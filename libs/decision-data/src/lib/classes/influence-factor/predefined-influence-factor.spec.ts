import { generatePredefinedScenarios } from './predefined-influence-factor';

describe('generatePredefinedScenarios()', () => {
  it('converts outcome values into combinations with indexes', () => {
    const outcomeValues = [
      [[1], [3], [5]],
      [[0.1], [0.3], [0.5]],
      [[10], [30], [50]],
    ];
    const stateProbs = [0.1, 0.3, 0.6];

    const combinations = generatePredefinedScenarios(outcomeValues, stateProbs);
    expect(combinations).toEqual([
      expect.objectContaining({ value: [[1], [3], [5]], stateIndices: [0, 0, 0] }),
      expect.objectContaining({ value: [[1], [3], [0.5]], stateIndices: [0, 0, 1] }),
      expect.objectContaining({ value: [[1], [3], [50]], stateIndices: [0, 0, 2] }),
      expect.objectContaining({ value: [[1], [0.3], [5]], stateIndices: [0, 1, 0] }),
      expect.objectContaining({ value: [[1], [0.3], [0.5]], stateIndices: [0, 1, 1] }),
      expect.objectContaining({ value: [[1], [0.3], [50]], stateIndices: [0, 1, 2] }),
      expect.objectContaining({ value: [[1], [30], [5]], stateIndices: [0, 2, 0] }),
      expect.objectContaining({ value: [[1], [30], [0.5]], stateIndices: [0, 2, 1] }),
      expect.objectContaining({ value: [[1], [30], [50]], stateIndices: [0, 2, 2] }),
      expect.objectContaining({ value: [[0.1], [3], [5]], stateIndices: [1, 0, 0] }),
      expect.objectContaining({ value: [[0.1], [3], [0.5]], stateIndices: [1, 0, 1] }),
      expect.objectContaining({ value: [[0.1], [3], [50]], stateIndices: [1, 0, 2] }),
      expect.objectContaining({ value: [[0.1], [0.3], [5]], stateIndices: [1, 1, 0] }),
      expect.objectContaining({ value: [[0.1], [0.3], [0.5]], stateIndices: [1, 1, 1] }),
      expect.objectContaining({ value: [[0.1], [0.3], [50]], stateIndices: [1, 1, 2] }),
      expect.objectContaining({ value: [[0.1], [30], [5]], stateIndices: [1, 2, 0] }),
      expect.objectContaining({ value: [[0.1], [30], [0.5]], stateIndices: [1, 2, 1] }),
      expect.objectContaining({ value: [[0.1], [30], [50]], stateIndices: [1, 2, 2] }),
      expect.objectContaining({ value: [[10], [3], [5]], stateIndices: [2, 0, 0] }),
      expect.objectContaining({ value: [[10], [3], [0.5]], stateIndices: [2, 0, 1] }),
      expect.objectContaining({ value: [[10], [3], [50]], stateIndices: [2, 0, 2] }),
      expect.objectContaining({ value: [[10], [0.3], [5]], stateIndices: [2, 1, 0] }),
      expect.objectContaining({ value: [[10], [0.3], [0.5]], stateIndices: [2, 1, 1] }),
      expect.objectContaining({ value: [[10], [0.3], [50]], stateIndices: [2, 1, 2] }),
      expect.objectContaining({ value: [[10], [30], [5]], stateIndices: [2, 2, 0] }),
      expect.objectContaining({ value: [[10], [30], [0.5]], stateIndices: [2, 2, 1] }),
      expect.objectContaining({ value: [[10], [30], [50]], stateIndices: [2, 2, 2] }),
    ]);
  });
});
