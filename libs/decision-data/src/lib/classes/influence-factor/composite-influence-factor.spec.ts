import { round } from 'lodash';
import { CompositeUserDefinedInfluenceFactor } from './composite-influence-factor';
import { SimpleUserDefinedInfluenceFactor } from './simple-user-defined-influence-factor';

function roundMatrix(matrix: number[][]) {
  // round everything to 4 decimal places
  for (let i = 0; i < matrix.length; i++) {
    for (let j = 0; j < matrix[i].length; j++) {
      matrix[i][j] = round(matrix[i][j], 4);
    }
  }
}

describe('CompositeUserDefinedInfluenceFactor', () => {
  it('returns a default name if the custom name is empty', () => {
    const comb = new CompositeUserDefinedInfluenceFactor(
      0,
      '',
      [new SimpleUserDefinedInfluenceFactor('I1'), new SimpleUserDefinedInfluenceFactor('I2')],
      [
        [3, 3],
        [3, 3],
      ],
      '',
    );
    expect(comb.name).toBe('I1 & I2');
  });

  it('gives state probabilities in [0, 100]', () => {
    const i1 = new SimpleUserDefinedInfluenceFactor('', [
      { name: '', probability: 70 },
      { name: '', probability: 30 },
    ]);
    const i2 = new SimpleUserDefinedInfluenceFactor('', [
      { name: '', probability: 50 },
      { name: '', probability: 50 },
    ]);
    const comb = new CompositeUserDefinedInfluenceFactor(
      0,
      '',
      [i1, i2],
      [
        [3, 3],
        [3, 3],
      ],
      '',
    );

    const stateProbs = comb.getStates().map(state => state.probability);
    expect(stateProbs).toEqual([35, 35, 15, 15]);
  });

  describe('computeProbabilityMatrix()', () => {
    it('works for a stochastically independent combination', () => {
      const i1 = new SimpleUserDefinedInfluenceFactor('', [
        { name: '', probability: 70 },
        { name: '', probability: 30 },
      ]);
      const i2 = new SimpleUserDefinedInfluenceFactor('', [
        { name: '', probability: 50 },
        { name: '', probability: 50 },
      ]);
      const comb = new CompositeUserDefinedInfluenceFactor(
        0,
        '',
        [i1, i2],
        [
          [3, 3],
          [3, 3],
        ],
        '',
      );

      const probs = comb.computeProbabilityMatrix();
      expect(probs).toEqual([
        [0.35, 0.35],
        [0.15, 0.15],
      ]);
    });

    it('adjusts correctly if the sum is too high', () => {
      const i1 = new SimpleUserDefinedInfluenceFactor('', [
        { name: '', probability: 60 },
        { name: '', probability: 40 },
        { name: '', probability: 0 },
      ]);
      const i2 = new SimpleUserDefinedInfluenceFactor('', [
        { name: '', probability: 60 },
        { name: '', probability: 10 },
        { name: '', probability: 30 },
      ]);
      const comb = new CompositeUserDefinedInfluenceFactor(
        0,
        '',
        [i1, i2],
        [
          [4, 5, 3],
          [3, 3, 3],
          [3, 3, 4],
        ],
        '',
      );

      const probs = comb.computeProbabilityMatrix();
      roundMatrix(probs);
      expect(probs).toEqual([
        [0.4333, 0.0833, 0.15],
        [0.2, 0.0333, 0.1],
        [0.0, 0.0, 0.0],
      ]);
    });
  });

  it('adjusts correctly if the sum is too low', () => {
    const i1 = new SimpleUserDefinedInfluenceFactor('', [
      { name: '', probability: 60 },
      { name: '', probability: 40 },
      { name: '', probability: 0 },
    ]);
    const i2 = new SimpleUserDefinedInfluenceFactor('', [
      { name: '', probability: 30 },
      { name: '', probability: 40 },
      { name: '', probability: 30 },
    ]);
    const comb = new CompositeUserDefinedInfluenceFactor(
      0,
      '',
      [i1, i2],
      [
        [1, 2, 3],
        [2, 2, 4],
        [4, 3, 1],
      ],
      '',
    );

    const probs = comb.computeProbabilityMatrix();
    roundMatrix(probs);
    expect(probs).toEqual([
      [0.0778, 0.1926, 0.2111],
      [0.1222, 0.163, 0.2333],
      [0.0, 0.0, 0.0],
    ]);
  });
});
