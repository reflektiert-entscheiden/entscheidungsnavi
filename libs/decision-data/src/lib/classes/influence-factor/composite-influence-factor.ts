import { v4 as uuidv4 } from 'uuid';
import { omit, sum } from 'lodash';
import { assertUnreachable } from '@entscheidungsnavi/tools/assert-unreachable';
import { ObjectiveInput } from '../objective/objective-input';
import {
  generateUserDefinedScenarios,
  iterateUserDefinedScenarios,
  SimpleUserDefinedInfluenceFactor,
  UserDefinedScenario,
} from './simple-user-defined-influence-factor';
import { BaseInfluenceFactor, InfluenceFactorState } from './base-influence-factor';

export type UserDefinedInfluenceFactor = SimpleUserDefinedInfluenceFactor | CompositeUserDefinedInfluenceFactor;
export type CombinedStateInfo = { influenceFactorName: string; stateName: string; probability: number };

export type ActionEntry = {
  action: 'add' | 'delete' | 'move';
  index?: number;
  fromIndex?: number;
  toIndex?: number;
};

export function isCustomInfluenceFactor(influenceFactor: any): influenceFactor is UserDefinedInfluenceFactor {
  return influenceFactor instanceof SimpleUserDefinedInfluenceFactor || influenceFactor instanceof CompositeUserDefinedInfluenceFactor;
}

/**
 * Generates a list of the UserDefinedInfluenceFactors that customIF consists of (in depth).
 * @param customIF - A custom influence factor
 */
export function generateListOfUnderlyingUserDefinedFactors(customIF: UserDefinedInfluenceFactor): SimpleUserDefinedInfluenceFactor[] {
  if (customIF instanceof SimpleUserDefinedInfluenceFactor) {
    return [customIF];
  } else {
    return customIF.baseFactors.map(generateListOfUnderlyingUserDefinedFactors).flat();
  }
}

export type CrossImpact = 1 | 2 | 3 | 4 | 5;

export class CompositeUserDefinedInfluenceFactor implements BaseInfluenceFactor {
  static getDefaultName(baseFactors: [UserDefinedInfluenceFactor, UserDefinedInfluenceFactor]) {
    return `${baseFactors[0].name} & ${baseFactors[1].name}`;
  }

  static getCompositeStateInfos(influenceFactor: UserDefinedInfluenceFactor, compositeStateIndex: number): CombinedStateInfo[] {
    if (influenceFactor instanceof SimpleUserDefinedInfluenceFactor) {
      const state = influenceFactor.getStates()[compositeStateIndex];
      return [{ influenceFactorName: influenceFactor.name, stateName: state.name, probability: state.probability }];
    } else {
      const [firstIndex, secondIndex] = influenceFactor.getBaseStateIndices(compositeStateIndex);
      return [
        ...CompositeUserDefinedInfluenceFactor.getCompositeStateInfos(influenceFactor.baseFactors[0], firstIndex),
        ...CompositeUserDefinedInfluenceFactor.getCompositeStateInfos(influenceFactor.baseFactors[1], secondIndex),
      ];
    }
  }

  get defaultName(): string {
    return CompositeUserDefinedInfluenceFactor.getDefaultName(this.baseFactors);
  }

  get name(): string {
    return this.customName || this.defaultName;
  }

  get stateCount(): number {
    return this.baseFactors[0].stateCount * this.baseFactors[1].stateCount;
  }

  constructor(
    public id: number,
    public customName: string,
    public baseFactors: [UserDefinedInfluenceFactor, UserDefinedInfluenceFactor],
    public crossImpacts: CrossImpact[][], // [#baseFactors[0].states, #baseFactors[1].states]
    public comment: string,
    public uuid = uuidv4(),
  ) {}

  getStates(): InfluenceFactorState[] {
    const probs = this.computeProbabilityMatrix();
    return probs.flatMap(probRow =>
      probRow.map(prob => ({
        probability: prob * 100,
      })),
    );
  }

  /**
   * Adjusts the crossImpact rows & columns for scenarios of depth 1, otherwise
   * it generates a completely new crossImpact matrix.
   * Should be called when the base influence factors change.
   *
   * @param actionList - The list of actions that were performed on the base influence factor
   * @param baseFactorIndex - The index of the base factor that was changed (0 or 1)
   * @param depth - The depth in which the originally changed SimpleUserDefinedIF is located in the current CompositeIF's baseFactors
   */
  // eslint-disable-next-line
  adjustToBaseFactors(actionList: ActionEntry[], baseFactorIndex: number, depth: number) {
    // TODO: Uncomment or delete in the future
    // if (depth === 1) {
    //   // adjust crossImpacts
    //
    //   // handler function for each action
    //   const adjustCrossImpacts = (action: ActionEntry, rowsOrColumnsOfCrossImpacts: CrossImpact[] | CrossImpact[][]) => {
    //     if (action.action === 'add') {
    //       if (Array.isArray(rowsOrColumnsOfCrossImpacts[0])) {
    //         (rowsOrColumnsOfCrossImpacts as CrossImpact[][]).splice(
    //           action.index,
    //           0,
    //           range(this.baseFactors[1].stateCount).map(() => 3),
    //         );
    //       } else {
    //         rowsOrColumnsOfCrossImpacts.splice(action.index, 0, 3);
    //       }
    //     } else if (action.action === 'delete') {
    //       rowsOrColumnsOfCrossImpacts.splice(action.index, 1);
    //     } else if (action.action === 'move') {
    //       const element = rowsOrColumnsOfCrossImpacts.splice(action.fromIndex, 1)[0];
    //       rowsOrColumnsOfCrossImpacts.splice(action.toIndex, 0, element as any);
    //     }
    //   };
    //
    //   actionList.forEach(action => {
    //     if (baseFactorIndex === 0) {
    //       // rows
    //       adjustCrossImpacts(action, this.crossImpacts);
    //     } else if (baseFactorIndex === 1) {
    //       // columns
    //       this.crossImpacts.forEach(row => adjustCrossImpacts(action, row));
    //     }
    //   });
    // } else {
    // completely reset crossImpact
    this.crossImpacts = Array.from({ length: this.baseFactors[0].stateCount }, () =>
      Array.from({ length: this.baseFactors[1].stateCount }, () => 3),
    );
    // }
  }

  computeProbabilityMatrix() {
    const probabilityMatrix = this.getUnadjustedProbabilityMatrix();
    this.adjustProbabilityMatrix(probabilityMatrix);
    return probabilityMatrix;
  }

  /**
   * Make sure the matrix sums up to 1.
   *
   * @param probabilityMatrix - The matrix that should be corrected
   */
  private adjustProbabilityMatrix(probabilityMatrix: number[][]) {
    const probabilitySum = sum(probabilityMatrix.flat());

    if (Math.abs(probabilitySum - 1) <= Number.EPSILON) {
      // The probabilities are normalized
      return;
    }

    // Sum of all probability in excess of the corresponding minimum probability (i.e., cross impact = 1) if the
    // sum is too high, and maximum probability (i.e., cross impact = 5) if the sum is too low
    const minimumProbabilityMatrix = this.getUnadjustedProbabilityMatrix(probabilitySum - 1 > Number.EPSILON ? 1 : 5);
    const excessProbabilitySum = probabilitySum - sum(minimumProbabilityMatrix.flat());

    // How much we need to scale the excess probability to reach 1
    const correctionFactor = (probabilitySum - 1) / excessProbabilitySum;

    for (let i = 0; i < probabilityMatrix.length; i++) {
      for (let j = 0; j < probabilityMatrix[i].length; j++) {
        const correction = (probabilityMatrix[i][j] - minimumProbabilityMatrix[i][j]) * correctionFactor;
        probabilityMatrix[i][j] -= correction;
      }
    }
  }

  private getUnadjustedProbabilityMatrix(overrideCrossImpact?: CrossImpact) {
    return this.baseFactors[0]
      .getStates()
      .map((firstState, firstIndex) =>
        this.baseFactors[1]
          .getStates()
          .map((secondState, secondIndex) =>
            this.getCrossImpactProbability(
              overrideCrossImpact ?? this.crossImpacts[firstIndex][secondIndex],
              firstState.probability / 100,
              secondState.probability / 100,
            ),
          ),
      );
  }

  /**
   * Computes the uncorrected cross impact probability for the given two probabilities.
   */
  private getCrossImpactProbability(crossImpact: CrossImpact, leftProbability: number, rightProbability: number): number {
    switch (crossImpact) {
      case 1:
        return Math.max(leftProbability + rightProbability - 1, 0);
      case 2:
        return (
          (this.getCrossImpactProbability(1, leftProbability, rightProbability) +
            this.getCrossImpactProbability(3, leftProbability, rightProbability)) /
          2
        );
      case 3:
        return leftProbability * rightProbability;
      case 4:
        return (
          (this.getCrossImpactProbability(3, leftProbability, rightProbability) +
            this.getCrossImpactProbability(5, leftProbability, rightProbability)) /
          2
        );
      case 5:
        return Math.min(leftProbability, rightProbability);
      default:
        assertUnreachable(crossImpact);
    }
  }

  generateScenarios(outcomeValues: ObjectiveInput[]) {
    return generateCombinedScenarios(outcomeValues, this.computeProbabilityMatrix().flat());
  }

  iterateScenarios(outcomeValues: ObjectiveInput[], callback: (scenario: UserDefinedScenario) => void, _optimizeStates: boolean) {
    iterateCombinedScenarios(outcomeValues, this.computeProbabilityMatrix().flat(), callback);
  }

  /**
   * Gets the composite state index for a base factor state combination.
   *
   * @param firstFactorIndex - The state index for the first base factor
   * @param secondFactorIndex - The state index for the second base factor
   * @returns The composite state index
   */
  getStateIndex(firstFactorIndex: number, secondFactorIndex: number) {
    return firstFactorIndex * this.baseFactors[1].stateCount + secondFactorIndex;
  }

  /**
   * Computes the state indices for the base influence factors from a composite state index.
   *
   * @param stateIndex - The composite state index to calculate the base indices from
   * @returns The base indices for the two {@link baseFactor}s
   */
  getBaseStateIndices(stateIndex: number): [number, number] {
    return [Math.floor(stateIndex / this.baseFactors[1].stateCount), stateIndex % this.baseFactors[1].stateCount];
  }

  toJSON() {
    return {
      ...omit(this, ['baseFactors']),
      baseFactorIds: this.baseFactors.map(factor => factor.id),
    };
  }

  getCompositeStateInfos(compositeStateIndex: number): CombinedStateInfo[] {
    return CompositeUserDefinedInfluenceFactor.getCompositeStateInfos(this, compositeStateIndex);
  }
}

const generateCombinedScenarios = generateUserDefinedScenarios;
const iterateCombinedScenarios = iterateUserDefinedScenarios;
