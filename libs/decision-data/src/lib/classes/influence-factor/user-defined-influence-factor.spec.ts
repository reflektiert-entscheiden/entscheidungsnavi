import { normalizeProbabilities, UserDefinedState } from './simple-user-defined-influence-factor';

describe('InfluenceFactor', () => {
  describe('normalizeProbabilities', () => {
    it('normalizes too large numbers', () => {
      const states = [
        new UserDefinedState('', 50),
        new UserDefinedState('', 150),
        new UserDefinedState('', 250),
        new UserDefinedState('', 350),
      ];
      normalizeProbabilities(states);
      expect(states.map(state => state.probability)).toEqual([6, 19, 31, 44]);
    });

    it('normalizes too small numbers', () => {
      const states = [
        new UserDefinedState('', 5),
        new UserDefinedState('', 15),
        new UserDefinedState('', 25),
        new UserDefinedState('', 35),
      ];
      normalizeProbabilities(states);
      expect(states.map(state => state.probability)).toEqual([6, 19, 31, 44]);
    });

    it('handles zeros gracefully', () => {
      const states = [new UserDefinedState('', 0), new UserDefinedState('', 0)];
      normalizeProbabilities(states);
      expect(states.map(state => state.probability)).toEqual([50, 50]);
    });
  });
});
