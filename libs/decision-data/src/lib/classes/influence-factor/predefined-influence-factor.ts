import { cloneDeep, range } from 'lodash';
import { ObjectiveInput } from '../objective/objective-input';
import { BaseInfluenceFactor, InfluenceFactorScenario, InfluenceFactorState } from './base-influence-factor';

export const PREDEFINED_INFLUENCE_FACTOR_IDS = ['3-Scenarios'] as const;
export type PredefinedInfluenceFactorId = (typeof PREDEFINED_INFLUENCE_FACTOR_IDS)[number];

export interface PredefinedScenario extends InfluenceFactorScenario {
  value: ObjectiveInput;
  stateIndices: number[];
  probability: number;
}

export class PredefinedInfluenceFactor implements BaseInfluenceFactor {
  get stateCount() {
    return this.states.length;
  }

  constructor(
    public readonly id: PredefinedInfluenceFactorId,
    public readonly states: InfluenceFactorState[] = [],
  ) {}

  getStates() {
    return this.states;
  }

  generateScenarios(outcomeValues: ObjectiveInput[]): PredefinedScenario[] {
    return generatePredefinedScenarios(
      outcomeValues,
      this.states.map(state => state.probability / 100),
    );
  }

  iterateScenarios(outcomeValues: ObjectiveInput[], callback: (scenario: PredefinedScenario) => void, optimizeStates: boolean) {
    iteratePredefinedScenarios(
      outcomeValues,
      this.states.map(state => state.probability / 100),
      callback,
      optimizeStates,
    );
  }
}

/*
  Hardcoded predefined influence factors.
*/
export const PREDEFINED_INFLUENCE_FACTORS: Record<PredefinedInfluenceFactorId, PredefinedInfluenceFactor> = {
  '3-Scenarios': new PredefinedInfluenceFactor('3-Scenarios', [{ probability: 25 }, { probability: 50 }, { probability: 25 }]),
};

/**
 * Compute all possible scenarios for a predefined influence factor in an indicator objective.
 * Since indicators are independent, this is a much longer list than the state list.
 *
 * For example from outcome.values = [[1, 3, 5], [0.1, 0.3, 0.5], [10, 30, 50]] the following combinations are generated:
 * [
 *  (0)  \{ combinations: [1, 3, 5],     indexes: [0, 0, 0] \}
 *  (1)  \{ combinations: [1, 3, 0.5],   indexes: [0, 0, 1] \}
 *  (2)  \{ combinations: [1, 3, 50],    indexes: [0, 0, 2] \}
 *  (3)  \{ combinations: [1, 0.3, 5],   indexes: [0, 1, 0] \}
 *  (4)  \{ combinations: [1, 0.5, 0.5], indexes: [0, 1, 1] \}
 *  (5)  \{ combinations: [1, 0.3, 50],  indexes: [0, 1, 2] \}
 *  ...
 *  (26) \{ combinations: [10, 30, 50],  indexes: [2, 2, 2] \}
 * ]
 *
 * @param indicatorOutcomeValues - The outcome values for every indicator in every state
 * @param stateProbabilities - Probability for every state in [0, 1]
 * @param mergeEqualStates - boolean - whether equal states (e.g. Best = Median) should be merged
 */
export function generatePredefinedScenarios(
  indicatorOutcomeValues: ObjectiveInput[],
  stateProbabilities: number[],
  mergeEqualStates = false,
): PredefinedScenario[] {
  const scenarios: PredefinedScenario[] = [];
  iteratePredefinedScenarios(indicatorOutcomeValues, stateProbabilities, scenario => scenarios.push(cloneDeep(scenario)), mergeEqualStates);
  return scenarios;
}

export function iteratePredefinedScenarios(
  indicatorOutcomeValues: ObjectiveInput[],
  stateProbabilities: number[],
  callback: (scenario: PredefinedScenario) => void,
  mergeEqualStates = false,
) {
  const indicatorOutcomeValuesClone = cloneDeep(indicatorOutcomeValues);
  const stateCount = indicatorOutcomeValuesClone.length; // it's always 3 (Worst, Median, Best)
  const indicatorCount = indicatorOutcomeValuesClone[0].length;
  const indicatorProbabilities: number[][] = range(indicatorCount).map(() => stateProbabilities.slice());

  if (mergeEqualStates) {
    mergeTrivialValuesAndProbabilities(indicatorOutcomeValuesClone, indicatorProbabilities);
  }

  function findCombinations(partialCombination: PredefinedScenario, indicatorIndex: number) {
    for (let stateIndex = 0; stateIndex < stateCount; stateIndex++) {
      if (indicatorOutcomeValuesClone[stateIndex][indicatorIndex] == null) {
        continue;
      }
      partialCombination.value[indicatorIndex] = indicatorOutcomeValuesClone[stateIndex][indicatorIndex];
      partialCombination.stateIndices[indicatorIndex] = stateIndex;

      const oldProbability = partialCombination.probability;
      partialCombination.probability *= indicatorProbabilities[indicatorIndex][stateIndex];

      if (indicatorIndex === indicatorCount - 1) {
        // We have reached the last indicator, so in total we picked a state for every indicator.
        // Thus, we have a complete combination.
        callback(partialCombination);
      } else {
        // We continue building a combination with the other indicators of which we pick states.
        findCombinations(partialCombination, indicatorIndex + 1);
      }

      partialCombination.probability = oldProbability;
    }
  }

  findCombinations({ value: [], stateIndices: [], probability: 1 }, 0);
}

function mergeTrivialValuesAndProbabilities(indicatorOutcomeValues: ObjectiveInput[], indicatorProbabilities: number[][]) {
  const indicatorCount = indicatorOutcomeValues[0].length;
  for (let indicatorIdx = 0; indicatorIdx < indicatorCount; indicatorIdx++) {
    const medianValue = indicatorOutcomeValues[1][indicatorIdx].toString();

    for (const bestOrWorstIndex of [0, 2]) {
      if (indicatorOutcomeValues[bestOrWorstIndex][indicatorIdx].toString() === medianValue) {
        indicatorOutcomeValues[bestOrWorstIndex][indicatorIdx] = null;
        indicatorProbabilities[indicatorIdx][1] += indicatorProbabilities[indicatorIdx][bestOrWorstIndex];
        indicatorProbabilities[indicatorIdx][bestOrWorstIndex] = 0;
      }
    }
  }
}
