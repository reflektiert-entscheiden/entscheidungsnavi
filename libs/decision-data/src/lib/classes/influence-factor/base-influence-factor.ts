import { ObjectiveInput } from '../objective/objective-input';

export interface BaseInfluenceFactor {
  id: string | number;

  /**
   * Returns the number of states of the influence factor.
   */
  get stateCount(): number;

  /**
   * Returns the states of the influence factor.
   */
  getStates(): InfluenceFactorState[];

  /**
   * Given {@link states.length} objective inputs, this function computes scenarios for
   * this influence factor.
   *
   * @param outcomeValues - An array of {@link state.length} inputs.
   */
  generateScenarios(outcomeValues: ObjectiveInput[]): InfluenceFactorScenario[];

  /**
   * Iterates over all scenarios for this influence factor.
   *
   * @param outcomeValues - An array of {@link state.length} inputs.
   * @param callback - The callback to be called for each scenario.
   * @param optimizeStates - If true, scenarios that are guaranteed to result in the same utility value MIGHT be merged together.
   * The probability of the merged scenario is the sum of the probabilities of the merged scenarios.
   *
   *
   * @remarks
   * The objects passed to the callback are reused for performance reasons. They are only valid during the callback.
   * If you want an array of actual scenarios use {@link generateScenarios}.
   */
  iterateScenarios(outcomeValues: ObjectiveInput[], callback: (scenario: InfluenceFactorScenario) => void, optimizeStates: boolean): void;
}

export interface InfluenceFactorState {
  /**
   * The probability is in [0, 100]
   */
  probability: number;
}

export interface InfluenceFactorScenario {
  value: ObjectiveInput;
  probability: number;
}
