import { SimpleUserDefinedInfluenceFactor, UserDefinedState } from '../influence-factor/simple-user-defined-influence-factor';
import { Objective, ObjectiveType } from '../objective/objective';
import { IndicatorObjectiveData } from '../objective/indicator-objective-data';
import { Indicator } from '../objective/indicator';
import { Outcome } from './outcome';

describe('Outcome', () => {
  let uf1: SimpleUserDefinedInfluenceFactor, oc1: Outcome, oc2: Outcome, oc3: Outcome, obj1: Objective, obj2: Objective, obj3: Objective;

  beforeEach(() => {
    uf1 = new SimpleUserDefinedInfluenceFactor('', [
      new UserDefinedState('', 20),
      new UserDefinedState('', 30),
      new UserDefinedState('', 50),
    ]);
    oc1 = new Outcome([[[1]], [[2]], [[3]]], null, uf1);
    oc2 = new Outcome([[[1], [2], [3]]], null);
    oc3 = new Outcome(
      [
        [[1], [2], [3]],
        [[1], [2], [3]],
        [[1], [2], [3]],
      ],
      null,
      uf1,
    );
    obj1 = new Objective('', null, null, null, ObjectiveType.Numerical);
    obj2 = new Objective(
      '',
      null,
      null,
      new IndicatorObjectiveData([new Indicator('', 0, 100, '', 1), new Indicator('', 0, 100, '', 1), new Indicator('', 0, 100, '', 1)]),
      ObjectiveType.Indicator,
    );
    obj3 = new Objective('', null, null, null, ObjectiveType.Verbal);
  });

  describe('constructor', () => {
    it('throws error when without values and objective', () => {
      expect(() => new Outcome()).toThrowError();
      expect(() => new Outcome([])).toThrow();
      expect(new Outcome([[[1]]])).toBeTruthy();
      expect(new Outcome([[[1]], [[2]], [[3]]])).toBeTruthy();
      expect(new Outcome([[[1], [2], [3]]])).toBeTruthy();
      expect(new Outcome(null, obj1)).toBeTruthy();
      expect(new Outcome(null, obj2)).toBeTruthy();
    });

    it('initializes from values', () => {
      expect(oc1.values).toEqual([[[1]], [[2]], [[3]]]);
      expect(oc1.influenceFactor).toBe(uf1);

      let newOc = new Outcome([[[1]], [[2]], [[3]]]);
      expect(newOc.influenceFactor).toBe(undefined);
      expect(newOc.values).toEqual([[[1]]]);

      newOc = new Outcome([[[1]]], null, uf1);
      expect(newOc.influenceFactor).toBe(uf1);
      expect(newOc.values).toEqual([[[1]], [[1]], [[1]]]);

      newOc = new Outcome([[[null]], [[1]]], null, uf1);
      expect(newOc.values).toEqual([[[null]], [[1]], [[undefined]]]);
    });

    it('initializes from objective', () => {
      let newOc = new Outcome(null, obj1);
      expect(newOc.influenceFactor).toBe(undefined);
      expect(newOc.values).toEqual([[[undefined]]]);

      newOc = new Outcome(null, obj1, uf1);
      expect(newOc.influenceFactor).toBe(uf1);
      expect(newOc.values).toEqual([[[undefined]], [[undefined]], [[undefined]]]);

      newOc = new Outcome(null, obj2);
      expect(newOc.influenceFactor).toBe(undefined);
      expect(newOc.values).toEqual([[[undefined], [undefined], [undefined]]]);

      newOc = new Outcome(null, obj2, uf1);
      expect(newOc.influenceFactor).toBe(uf1);
      expect(newOc.values).toEqual([
        [[undefined], [undefined], [undefined]],
        [[undefined], [undefined], [undefined]],
        [[undefined], [undefined], [undefined]],
      ]);
    });
  });

  describe('setInfluenceFactor', () => {
    it('removes values', () => {
      oc1.setInfluenceFactor(undefined);
      expect(oc1.influenceFactor).toBe(undefined);
      expect(oc1.values).toEqual([[[1]]]);
    });

    it('fills with empty values', () => {
      oc2.setInfluenceFactor(uf1);
      expect(oc2.influenceFactor).toBe(uf1);
      expect(oc2.values).toEqual([
        [[1], [2], [3]],
        [[1], [2], [3]],
        [[1], [2], [3]],
      ]);
    });
  });

  describe('addUfState', () => {
    it('preserves number type', () => {
      oc1.addUfState(2);
      expect(oc1.values).toEqual([[[1]], [[2]], [[undefined]], [[3]]]);
      oc1.addUfState(0);
      expect(oc1.values).toEqual([[[undefined]], [[1]], [[2]], [[undefined]], [[3]]]);
      oc1.addUfState(5);
      expect(oc1.values).toEqual([[[undefined]], [[1]], [[2]], [[undefined]], [[3]], [[undefined]]]);
    });

    it('preserves number[] type', () => {
      oc2.addUfState(1);
      expect(oc2.values).toEqual([
        [[1], [2], [3]],
        [[1], [2], [3]],
      ]);
      oc2.addUfState(0);
      expect(oc2.values).toEqual([
        [[1], [2], [3]],
        [[1], [2], [3]],
        [[1], [2], [3]],
      ]);
      oc2.addUfState(2);
      expect(oc2.values).toEqual([
        [[1], [2], [3]],
        [[1], [2], [3]],
        [[1], [2], [3]],
        [[1], [2], [3]],
      ]);
    });
  });

  describe('removeUfState', () => {
    it('should check if the outcome is processed', () => {
      // see issue #315
      oc1.addUfState(0);
      oc1.removeUfState(0);
      expect(oc1.processed).toBe(true);
    });
  });

  describe('initializeValues', () => {
    it('works with indicator', () => {
      oc1.initializeValues(obj2);
      expect(oc1.values).toEqual([
        [[undefined], [undefined], [undefined]],
        [[undefined], [undefined], [undefined]],
        [[undefined], [undefined], [undefined]],
      ]);

      oc2.initializeValues(obj2);
      expect(oc2.values).toEqual([[[undefined], [undefined], [undefined]]]);
    });

    it('works with numerical', () => {
      oc1.initializeValues(obj1);
      expect(oc1.values).toEqual([[[undefined]], [[undefined]], [[undefined]]]);

      oc2.initializeValues(obj1);
      expect(oc2.values).toEqual([[[undefined]]]);
    });

    it('works with verbal', () => {
      oc1.initializeValues(obj3);
      expect(oc1.values).toEqual([[[undefined]], [[undefined]], [[undefined]]]);

      oc2.initializeValues(obj3);
      expect(oc2.values).toEqual([[[undefined]]]);
    });
  });

  describe('addObjectiveIndicator', () => {
    it('adds correctly on indicator', () => {
      oc2.addObjectiveIndicator(0);
      expect(oc2.values).toEqual([[[undefined], [1], [2], [3]]]);
      oc2.addObjectiveIndicator(4);
      expect(oc2.values).toEqual([[[undefined], [1], [2], [3], [undefined]]]);
      oc2.addObjectiveIndicator(2);
      expect(oc2.values).toEqual([[[undefined], [1], [undefined], [2], [3], [undefined]]]);

      oc3.addObjectiveIndicator(2);
      expect(oc3.values).toEqual([
        [[1], [2], [undefined], [3]],
        [[1], [2], [undefined], [3]],
        [[1], [2], [undefined], [3]],
      ]);
    });
  });

  describe('removeObjectiveIndicator', () => {
    it('removes correctly on indicator', () => {
      oc2.removeObjectiveIndicator(0);
      expect(oc2.values).toEqual([[[2], [3]]]);
      oc2.removeObjectiveIndicator(1);
      expect(oc2.values).toEqual([[[2]]]);

      oc3.removeObjectiveIndicator(1);
      expect(oc3.values).toEqual([
        [[1], [3]],
        [[1], [3]],
        [[1], [3]],
      ]);
    });
  });

  describe('clone', () => {
    it('should clone everything', () => {
      expect(oc1.clone()).toEqual(oc1);
      expect(oc3.clone()).toEqual(oc3);
    });

    it('should clone values, not copy', () => {
      const clone = oc2.clone();
      expect(clone.values).toEqual(oc2.values);
      expect(clone.values).not.toBe(oc2.values);
      expect(clone.values[0]).toEqual(oc2.values[0]);
      expect(clone.values[0]).not.toBe(oc2.values[0]);
    });
  });

  describe('copyBack', () => {
    it('should copy back everything', () => {
      oc2.copyBack(oc1);
      expect(oc2).toEqual(oc1);

      oc1.copyBack(oc3);
      expect(oc1).toEqual(oc3);
    });

    it('should clone values, not copy', () => {
      oc1.copyBack(oc2);
      expect(oc1.values).toEqual(oc2.values);
      expect(oc1.values).not.toBe(oc2.values);
      expect(oc1.values[0]).toEqual(oc2.values[0]);
      expect(oc1.values[0]).not.toBe(oc2.values[0]);
    });
  });
});
