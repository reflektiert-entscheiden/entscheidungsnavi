import { v4 as uuidv4 } from 'uuid';
import { cloneDeep, pick, range, sum, zip } from 'lodash';
import { moveInArray } from '@entscheidungsnavi/tools';
import { InfluenceFactor } from '../influence-factor/influence-factor';
import { Objective } from '../objective/objective';
import { getOutcomeUtilityFunction } from '../../utility/utility';
import { ObjectiveInput } from '../objective/objective-input';
import { calculateIndicatorValue } from '../objective/indicator-aggregation';

export class Outcome {
  public processed = false; // the first change of values[] will set this to true

  /**
   * This array contains only one element if no InfluenceFactor is selected or the InfluenceFactor has exactly
   * one condition. Otherwise there will be a value for each condition of the InfluenceFactor.
   * There is always supposed to be at least one value.
   * Null/undefined entries are allowed and indicate not set/processed entries.
   */
  /**
   * This array contains 3 layers:
   * Layer 1 (states): Contains one element for each state. If no InfluenceFactor, it only has one entry.
   * Layer 2 (indicators): Contains one element for each indicator. If no Indicators, it only contains one element.
   * Layer 3 (values): Contains a single value for num./verbal scales and normal indicators. Contains multiple values for verbal indicators.
   * Examples:
   * Numerical/Verbal without IF: [[[1]]]
   * Numerical/Verbal with IF (2 states): [[[1]], [[2]]]
   * Indicator scale (2 normal indicators) without IF: [[[1], [2]]]
   * Indicator scale (2 normal indicators) with IF (2 states): [[[1], [2]], [[3], [4]]]
   * Indicator scale (1 normal indicator, 1 verbal indicator) without IF: [[[1], [0, 1]]]
   * Indicator scale (1 normal indicator, 1 verbal indicator) with IF (2 states): [[[1], [0, 1]], [[2], [0, 1]]]
   * Crazy I know.
   */
  public values: ObjectiveInput[]; // values[stateIdx][indicatorIdx][categoryIdx]
  public influenceFactor: InfluenceFactor;

  get isEmpty() {
    return this.values.flat(2).every(value => value == null);
  }

  /**
   * It is necessary to either input values or an objective in order
   * to initialize the values.
   */
  constructor(
    values?: ObjectiveInput[],
    objective?: Objective,
    uf?: InfluenceFactor,
    processed = false,
    public comment?: string,
    public uuid = uuidv4(),
  ) {
    if (values != null && Array.isArray(values)) {
      this.setValues(values);
    } else if (objective != null) {
      this.initializeValues(objective);
    } else {
      throw new Error('Outcome constructor called with neither values nor an objective');
    }
    this.setInfluenceFactor(uf);
    this.processed = processed;
  }

  private setValues(values: ObjectiveInput[]) {
    if (values.length === 0) {
      throw new Error('Values must never have length zero');
    }
    // Null/undefined values are explicitly permitted here, as they only indicate not processed outcomes.
    this.values = cloneDeep(values);
  }

  /**
   * Set the influence factor and reset the values accordingly
   */
  setInfluenceFactor(uf: InfluenceFactor) {
    let newLength = uf?.stateCount;
    if (newLength == null || newLength < 1) {
      newLength = 1;
    }

    this.influenceFactor = uf;

    if (this.values.length === newLength) {
      // Same number of states, nothing to do
      return;
    }

    if (this.values.length > newLength) {
      // Switched to an influence factor with fewer states / no influence factor, just discard the redundant values
      this.values.splice(newLength, this.values.length - newLength);
      return;
    }

    // We are switching to an influence factor with more states or we are switching to an influence factor from no influence factor
    const statesToAdd = newLength - this.values.length;

    const emptyValue = this.generateEmptyValue();
    for (let i = 0; i < statesToAdd; i++) {
      this.values.push(cloneDeep(emptyValue));
    }
  }

  /**
   * The influence factor has a new state which has to be reflected in the values.
   * @param position - The position of the new state
   */
  addUfState(position: number) {
    // We have to infer whether our values are number[] or number.
    this.values.splice(position, 0, this.generateEmptyValue());
    this.processed = false;
  }

  /**
   * The influence factor had one state removed.
   * @param position - The position of the removed state
   */
  removeUfState(position: number) {
    this.values.splice(position, 1);
    this.checkProcessed();
  }

  /**
   * Adjust values when an influence factor state is moved.
   *
   * @param fromPosition - The source position
   * @param toPosition - The target position
   */
  moveUfState(fromPosition: number, toPosition: number) {
    moveInArray(this.values, fromPosition, toPosition);
  }

  /**
   * Needs to be called whenever the type of the objective changes.
   * @param objective - The objective to initialize the values for
   */
  initializeValues(objective: Objective) {
    // We use a function instead of a fixed value, because for indicators the value
    // is an array which is assigned by reference. We want to assign a new array
    // for each outcome value instead of a reference to the same array.
    const getInitValue: () => ObjectiveInput = () => {
      // We always return undefined as content
      if (objective.isIndicator) {
        return objective.indicatorData.indicators.map(ind => {
          // create an undefined entry for each indicator category or a single entry for a numerical indicator
          if (ind.isVerbalized) {
            return ind.verbalIndicatorCategories.map(() => undefined);
          } else {
            return [undefined];
          }
        });
      } else {
        return [[undefined]];
      }
    };
    this.processed = false;
    this.values = this.values == null ? [getInitValue()] : this.values.map(() => getInitValue());
  }

  /**
   * Called whenever there is a new indicator for the associated objective.
   * @param position - Where in the indicators-array the new indicator is
   */
  addObjectiveIndicator(position: number) {
    this.values.forEach(value => {
      if (!Array.isArray(value)) {
        throw new Error(`Expected array, got ${typeof value}`);
      }

      value.splice(position, 0, [undefined]);
    });
    this.processed = false;
  }

  /**
   * Called whenever an indicator is removed from the associated objective.
   * @param position - Where in the indicators-array the indicator was
   */
  removeObjectiveIndicator(position: number) {
    this.values.forEach(value => {
      value.splice(position, 1);
    });
    // The removed indicator may have been the reason processed was false.
    // It may now be true.
    this.checkProcessed();
  }

  /**
   * Called whenever an indicator changes position in the associated objective.
   */
  moveObjectiveIndicator(fromPosition: number, toPosition: number) {
    this.values.forEach(value => {
      value.splice(toPosition, 0, ...value.splice(fromPosition, 1));
    });
  }

  /**
   * Check if the values are set according to the influence factor.
   * This includes checking whether the length of the values-array matches
   * the number of states of the uf and whether all values are non-null.
   */
  checkProcessed() {
    if (this.values == null || this.values.length === 0) {
      this.processed = false;
    } else {
      // Also account for arrays with null-entries
      this.processed =
        // all values != null
        this.values.every(v1 => v1.every(v2 => v2.every(v3 => v3 != null))) &&
        // states.length = values.length
        (this.influenceFactor == null || this.influenceFactor.stateCount === this.values.length);
    }
  }

  /**
   * Create a clone of this outcome.
   */
  clone(): Outcome {
    return new Outcome(cloneDeep(this.values), null, this.influenceFactor, this.processed, this.comment, this.uuid);
  }

  /**
   * Copy the values of the given outcome into this outcome.
   */
  copyBack(outcome: Outcome) {
    this.values = cloneDeep(outcome.values);
    this.influenceFactor = outcome.influenceFactor;
    this.processed = outcome.processed;
    this.comment = outcome.comment;
    this.uuid = outcome.uuid;
  }

  /**
   * Compute the utility for this outcome.
   *
   * @param objective - The objective corresponding to this outcome
   * @returns The utility value
   */
  getUtility(objective: Objective) {
    return getOutcomeUtilityFunction(
      objective.getUtilityFunction(),
      this.influenceFactor ? (callback, outcomeValues) => this.influenceFactor.iterateScenarios(outcomeValues, callback, true) : undefined,
    )(this.values);
  }

  /**
   * Returns this outcome's values, normalized to [0, 1].
   *
   * @param objective - The objective corresponding to this outcome
   * @returns The normalized value w.r.t. the objective
   */
  getValuePercentages(objective: Objective) {
    const rawValues = objective.isIndicator
      ? this.values.map(value => objective.indicatorData.aggregationFunction(value))
      : this.values.map(value => value[0][0]);
    const interval = objective.getRangeInterval();
    return rawValues.map(value => interval.normalizePoint(value));
  }

  /**
   * Calculates the expected value percentage from the set of value percentages based on this outcome's influence factor.
   *
   * @param percentages - The percentages for each IF state or an array with a single percentage in case no IF is used
   * @returns The expected value percentage
   */
  getExpectedValuePercentage(percentages: number[]) {
    if (this.influenceFactor) {
      return sum(zip(percentages, this.influenceFactor.getStates()).map(([percentage, state]) => (percentage * state.probability) / 100));
    } else {
      return percentages[0];
    }
  }

  toJSON() {
    return {
      ...pick(this, ['processed', 'values', 'comment', 'uuid']),
      // Convert the influence factor to its ID
      ...(this.influenceFactor && { influenceFactorId: this.influenceFactor.id }),
    };
  }

  /**
   * checks whether the outcome includes an actual uncertainty (where not all state values are equal)
   * @param objective - outcome objective
   */
  includesNonTrivialUncertainty(objective: Objective) {
    if (this.influenceFactor == null) return false;
    if (!objective.isIndicator) {
      const values = this.values.map(stateValues => stateValues[0][0]);
      return new Set(values).size > 1;
    }

    return range(Math.max(objective.indicatorData.indicators.length, 1)).some(indicatorIdx => {
      const values = this.values.map(stateValues =>
        calculateIndicatorValue(stateValues[indicatorIdx], objective.indicatorData.indicators[indicatorIdx]),
      );
      return new Set(values).size > 1;
    });
  }

  generateMedianVector(objective: Objective) {
    return range(objective.indicatorData.indicators.length).map(indicatorIdx =>
      calculateIndicatorValue(this.values[1][indicatorIdx], objective.indicatorData.indicators[indicatorIdx]),
    );
  }

  generateExpectedValuesVector(objective: Objective) {
    const states = this.influenceFactor.getStates();
    return range(objective.indicatorData.indicators.length).map(indicatorIdx =>
      sum(
        this.values.map(
          (valuesPerState, stateIdx) =>
            (calculateIndicatorValue(valuesPerState[indicatorIdx], objective.indicatorData.indicators[indicatorIdx]) *
              states[stateIdx].probability) /
            100,
        ),
      ),
    );
  }

  private generateEmptyValue(): ObjectiveInput {
    const emptyValue: ObjectiveInput = [];

    for (let indicatorIndex = 0; indicatorIndex < this.values[0].length; indicatorIndex++) {
      emptyValue[indicatorIndex] = [];

      const indicatorCategoryCount = this.values[0][indicatorIndex].length;

      for (let categoryIndex = 0; categoryIndex < indicatorCategoryCount; categoryIndex++) {
        const isTheSameInAllStates = this.values.every(
          stateValues => stateValues[indicatorIndex][categoryIndex] == this.values[0][indicatorIndex][0],
        );

        if (isTheSameInAllStates) {
          emptyValue[indicatorIndex].push(this.values[0][indicatorIndex][categoryIndex]);
        } else {
          emptyValue[indicatorIndex].push(undefined);
        }
      }
    }

    return emptyValue;
  }
}
