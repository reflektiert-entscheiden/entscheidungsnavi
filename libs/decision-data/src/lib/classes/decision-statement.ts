import { pick } from 'lodash';

export class Value {
  private _name: string;

  get name() {
    return this._name;
  }
  set name(value: string) {
    if (value !== this._name) {
      this._name = value;
      this.valueId = null;
    }
  }

  constructor(
    name: string,
    public val?: number,
    public valueId?: string,
  ) {
    this._name = name;
  }

  toJSON() {
    return pick(this, ['name', 'val', 'valueId']);
  }
}

export type DecisionStatementStep = (typeof DECISION_STATEMENT_STEPS)[number];

export const DECISION_STATEMENT_STEPS = [
  'FIRST_DRAFT',
  'FUNDAMENTAL_VALUES',
  'IMPULSE_QUESTIONS',
  'RECONSIDER_AND_REFORMULATE',
  'RESULT',
] as const;

export class DecisionStatement {
  static get defaultNotes(): [string, string, string, string] {
    return [null, null, null, null];
  }

  constructor(
    public statement: string = null,
    // eslint-disable-next-line @typescript-eslint/naming-convention
    public statement_attempt: string = null,
    // eslint-disable-next-line @typescript-eslint/naming-convention
    public statement_attempt_2: string = null,
    public questions: number[] = [null, null, null],
    public values: Value[] = [],
    public preNotes: string[] = [],
    public afterNotes: string[] = [],
    // eslint-disable-next-line @typescript-eslint/naming-convention
    public preNotes_2: string[] = [],
    // eslint-disable-next-line @typescript-eslint/naming-convention
    public afterNotes_2: string[] = [],
    public preNotesFinal: string[] = [],
    public afterNotesFinal: string[] = [],
    public notes = DecisionStatement.defaultNotes,
    public subStepProgression: DecisionStatementStep | null = null,
  ) {}

  public addValue(index = this.values.length, name = '', val?: number) {
    const newValue = new Value(name, val);
    this.values.splice(index, 0, newValue);
  }

  public removeValue(index: number) {
    this.values.splice(index, 1);
  }

  public sortValues() {
    this.removeEmptyValues();
    this.values.sort(function (a: Value, b: Value) {
      // handle undefined values
      if (a.val == null) {
        if (b.val != null) {
          // a undefined
          return 1;
        } else {
          // both undefined
          return 0;
        }
      } else if (b.val == null) {
        // b undefined
        return -1;
      }
      // sort descending with undefined values at the end
      return b.val - a.val;
    });
  }

  /**
   * returns the top values (up to 5), that are not undefined
   */
  public getImportantValues(): Value[] {
    this.sortValues();
    return this.values.filter(val => val.val != null).slice(0, 5);
  }

  removeEmptyValues() {
    const idxToBeRemoved: number[] = [];
    this.values.forEach((value, idx) => {
      if (value.name === '') {
        idxToBeRemoved.push(idx);
      }
    });

    while (idxToBeRemoved.length) {
      this.values.splice(idxToBeRemoved.pop(), 1);
    }
  }
}
