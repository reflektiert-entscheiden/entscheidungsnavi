import { Note } from './note';

export class NoteGroup {
  constructor(
    public id: number,
    public notes: Note[] = [],
    public name = '',
  ) {}

  clone(): NoteGroup {
    // return a deep copy of the NoteGroup
    return new NoteGroup(this.id, this.cloneToArray(), this.name);
  }

  cloneToArray(): Note[] {
    return this.notes.map((note: Note) => new Note(note.id, note.name));
  }

  // add an existing Note object at the given position or the end of the group
  addNote(note: Note, index?: number) {
    if (index == null) {
      // add note at the end of the group
      this.notes.push(note);
    } else {
      this.notes.splice(index, 0, note);
    }
  }

  // remove a note at the given position and return it
  popNote(index: number) {
    return this.notes.splice(index, 1)[0];
  }

  removeNote(idx: number) {
    this.notes.splice(idx, 1);
  }
}
