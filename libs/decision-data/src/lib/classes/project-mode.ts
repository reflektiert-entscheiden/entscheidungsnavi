export const PROJECT_MODES = ['starter', 'educational', 'professional'] as const;
export type ProjectMode = (typeof PROJECT_MODES)[number];
