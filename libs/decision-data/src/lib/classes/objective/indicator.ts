import { Interval } from '@entscheidungsnavi/tools';

export interface VerbalIndicatorCategory {
  name: string;
  weight: number;
  stages: VerbalIndicatorCategoryStage[];
}

export interface VerbalIndicatorCategoryStage {
  name: string;
  description: string;
}

export const MAX_VERBAL_INDICATOR_CATEGORIES = 5;
export const MAX_STAGES_PER_VERBAL_INDICATOR_CATEGORY = 7;

export class Indicator {
  get isVerbalized() {
    return this.verbalIndicatorCategories.length > 0;
  }

  constructor(
    public name: string = '',
    // min > max is allowed here (e.g. school grades). min is the worst value
    // from a utility perspective if the coefficient is positive. Else it
    // is the best value.
    public min = 1,
    public max = 7,
    public unit = '',
    // We enforce coefficient >= 0
    public coefficient = 1,
    public comment?: string,
    public verbalIndicatorCategories: VerbalIndicatorCategory[] = [], // non-verbal Indicators have verbalIndicatorCategories.length = 0
  ) {}

  getRangeInterval() {
    return new Interval(this.min, this.max);
  }

  clone() {
    return new Indicator(this.name, this.min, this.max, this.unit, this.coefficient, this.comment);
  }
}
