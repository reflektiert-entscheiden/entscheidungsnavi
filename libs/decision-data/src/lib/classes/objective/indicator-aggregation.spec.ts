import { calculateIndicatorValue, getIndicatorAggregationFunction, getIndicatorCoefficientName } from './indicator-aggregation';
import { Indicator } from './indicator';
import { ObjectiveInput } from './objective-input';

describe('getIndicatorCoefficientName', () => {
  it('handles values below 26', () => {
    expect(getIndicatorCoefficientName(0)).toBe('a');
    expect(getIndicatorCoefficientName(10)).toBe('k');
    expect(getIndicatorCoefficientName(25)).toBe('z');
  });

  it('handles values above 25', () => {
    expect(getIndicatorCoefficientName(26)).toBe('aa');
    expect(getIndicatorCoefficientName(260)).toBe('ja');
    expect(getIndicatorCoefficientName(1000)).toBe('alm');
  });
});

describe('getIndicatorAggregationFunction', () => {
  describe('with default aggregation function', () => {
    let fct: (values: ObjectiveInput) => number;
    let fct2: (values: ObjectiveInput) => number;
    let fct3: (values: ObjectiveInput) => number;
    beforeEach(() => {
      fct = getIndicatorAggregationFunction([new Indicator('', 1, 7, '', 2), new Indicator('', 5, 1, '', 1)], { worst: 1, best: 7 });
      fct2 = getIndicatorAggregationFunction([new Indicator('', 1, 7, '', 2), new Indicator('', 5, 1, '', 1)], { worst: 7, best: 1 });
      fct3 = getIndicatorAggregationFunction(
        [
          new Indicator('', 5, 1, '', 1),
          new Indicator('', 0, 10, '', 2, '', [
            {
              name: 'c1',
              weight: 3,
              stages: [
                { name: 's1', description: '' },
                { name: 's2', description: '' },
              ],
            },
            {
              name: 'c2',
              weight: 2,
              stages: [
                { name: 's11', description: '' },
                { name: 's22', description: '' },
                { name: 's33', description: '' },
                { name: 's44', description: '' },
                { name: 's55', description: '' },
              ],
            },
            {
              name: 'c3',
              weight: 5,
              stages: [
                { name: 's111', description: '' },
                { name: 's222', description: '' },
                { name: 's333', description: '' },
              ],
            },
          ]),
        ],
        { worst: 5, best: 1 },
      );
    });

    it('throws error on malformed data', () => {
      expect(() => fct([[1]])).toThrow();
      expect(() => fct([[0]])).toThrow();
    });

    it('returns NaN if a value is null', () => {
      expect(fct([[1], [null]])).toBe(NaN);
      expect(fct([[null], [null]])).toBe(NaN);
      expect(fct3([[null], [null, null, null]])).toBe(NaN);
    });

    it('calculates correct aggregations with min < max', () => {
      expect(fct([[1], [5]])).toBe(1);
      expect(fct([[2], [4]])).toBeCloseTo(2.166);
      expect(fct([[7], [1]])).toBe(7);
    });

    it('calculates correct aggregations with min > max', () => {
      expect(fct2([[1], [5]])).toBe(7);
      expect(fct2([[2], [4]])).toBeCloseTo(5.833);
      expect(fct2([[7], [1]])).toBe(1);
    });

    it('calculates correct aggregations with verbal indicators', () => {
      expect(fct3([[2], [1, 0, 1]])).toBeCloseTo(2.53);
      expect(fct3([[3], [0, 1, 2]])).toBeCloseTo(2.87);
      expect(fct3([[5], [0, 4, 0]])).toBeCloseTo(4.47);
    });
  });

  describe('with custom aggregation function', () => {
    let indicators: Indicator[];
    beforeEach(() => {
      indicators = [new Indicator('', 1, 7, '', 2), new Indicator('', 5, 1, '', 1)];
    });

    it('throws error on malformed data', () => {
      const fct = getIndicatorAggregationFunction(indicators, { formula: 'a * Ind1 + b * Ind2', globalVariables: new Map() });
      expect(() => fct([[1]])).toThrow();
      expect(() => fct([[0]])).toThrow();
    });

    it('returns NaN if a value is null', () => {
      const fct = getIndicatorAggregationFunction(indicators, { formula: 'a * Ind1 + b * Ind2', globalVariables: new Map() });
      expect(fct([[1], [null]])).toBe(NaN);
      expect(fct([[null], [null]])).toBe(NaN);
    });

    it('works with an additive formula', () => {
      const fct = getIndicatorAggregationFunction(indicators, { formula: 'a * Ind1 + b * Ind2', globalVariables: new Map() });
      expect(fct([[1], [1]])).toBe(3);
      expect(fct([[5], [2]])).toBe(12);
    });

    it('works with simple arithmetic operations', () => {
      const fct = getIndicatorAggregationFunction(indicators, { formula: 'a * Ind1 + Ind2 / b - 2', globalVariables: new Map() });
      expect(fct([[1], [1]])).toBe(1);
      expect(fct([[5], [2]])).toBe(10);
    });

    it('works with exp and log', () => {
      const fct = getIndicatorAggregationFunction(indicators, { formula: 'exp(a * Ind1) + log(Ind2 * b)', globalVariables: new Map() });
      expect(fct([[1], [1]])).toBeCloseTo(7.389);
      expect(fct([[5], [2]])).toBeCloseTo(22027.1589);
    });

    it('throws on invalid formula', () => {
      expect(
        getIndicatorAggregationFunction(indicators, { formula: 'a * Ind1 + b * Ind2 + c * Ind3', globalVariables: new Map() })([[1], [1]]),
      ).toBe(NaN);
      expect(
        getIndicatorAggregationFunction(indicators, { formula: 'a * Ind1 + b * Ind2 + Z1', globalVariables: new Map() })([[1], [1]]),
      ).toBe(NaN);
      expect(
        getIndicatorAggregationFunction(indicators, { formula: 'a * Ind1 + b * Ind2 + exp(abc)', globalVariables: new Map() })([[1], [1]]),
      ).toBe(NaN);
    });

    it('handles inverted scale [min=best, max=worst]', () => {
      const fct = getIndicatorAggregationFunction(indicators, { formula: '-(a * Ind1 + b * Ind2)', globalVariables: new Map() });
      expect(fct([[1], [1]])).toBe(-3);
      expect(fct([[5], [2]])).toBe(-12);
    });

    it('handles global variables', () => {
      const fct = getIndicatorAggregationFunction(indicators, {
        formula: 'a * Ind1 + b * Ind2 + Z1',
        globalVariables: new Map([['Z1', 1]]),
      });
      expect(fct([[1], [1]])).toBe(4);
      expect(fct([[5], [2]])).toBe(13);
    });
  });
});

describe('calculateIndicatorValue', () => {
  it('handles values for normal (numerical) indicators', () => {
    const ind = new Indicator('', 0, 10, '', 10);
    expect(calculateIndicatorValue([5], ind)).toBe(5);
  });

  it('handles values for verbalized indicators', () => {
    const ind = new Indicator('', 0, 10, '', 10, '', [
      {
        name: 'c1',
        weight: 3,
        stages: [
          { name: 's1', description: '' },
          { name: 's2', description: '' },
        ],
      },
      {
        name: 'c2',
        weight: 2,
        stages: [
          { name: 's11', description: '' },
          { name: 's22', description: '' },
          { name: 's33', description: '' },
          { name: 's44', description: '' },
          { name: 's55', description: '' },
        ],
      },
      {
        name: 'c3',
        weight: 5,
        stages: [
          { name: 's111', description: '' },
          { name: 's222', description: '' },
          { name: 's333', description: '' },
        ],
      },
    ]);
    expect(calculateIndicatorValue([0, 0, 0], ind)).toBe(0);
    expect(calculateIndicatorValue([0, 0, 1], ind)).toBe(2.5);
    expect(calculateIndicatorValue([0, 0, 2], ind)).toBe(5);
    expect(calculateIndicatorValue([0, 1, 0], ind)).toBe(0.5);
    expect(calculateIndicatorValue([0, 1, 1], ind)).toBe(3);
    expect(calculateIndicatorValue([0, 1, 2], ind)).toBe(5.5);
    expect(calculateIndicatorValue([0, 2, 0], ind)).toBe(1);
    expect(calculateIndicatorValue([0, 2, 1], ind)).toBe(3.5);
    expect(calculateIndicatorValue([0, 2, 2], ind)).toBe(6);
    expect(calculateIndicatorValue([0, 3, 0], ind)).toBe(1.5);
    expect(calculateIndicatorValue([0, 3, 1], ind)).toBe(4);
    expect(calculateIndicatorValue([0, 3, 2], ind)).toBe(6.5);
    expect(calculateIndicatorValue([0, 4, 0], ind)).toBe(2);
    expect(calculateIndicatorValue([0, 4, 1], ind)).toBe(4.5);
    expect(calculateIndicatorValue([0, 4, 2], ind)).toBe(7);
    expect(calculateIndicatorValue([1, 0, 0], ind)).toBe(3);
    expect(calculateIndicatorValue([1, 0, 1], ind)).toBe(5.5);
    expect(calculateIndicatorValue([1, 0, 2], ind)).toBe(8);
    expect(calculateIndicatorValue([1, 1, 0], ind)).toBe(3.5);
    expect(calculateIndicatorValue([1, 1, 1], ind)).toBe(6);
    expect(calculateIndicatorValue([1, 1, 2], ind)).toBe(8.5);
    expect(calculateIndicatorValue([1, 2, 0], ind)).toBe(4);
    expect(calculateIndicatorValue([1, 2, 1], ind)).toBe(6.5);
    expect(calculateIndicatorValue([1, 2, 2], ind)).toBe(9);
    expect(calculateIndicatorValue([1, 3, 0], ind)).toBe(4.5);
    expect(calculateIndicatorValue([1, 3, 1], ind)).toBe(7);
    expect(calculateIndicatorValue([1, 3, 2], ind)).toBe(9.5);
    expect(calculateIndicatorValue([1, 4, 0], ind)).toBe(5);
    expect(calculateIndicatorValue([1, 4, 1], ind)).toBe(7.5);
    expect(calculateIndicatorValue([1, 4, 2], ind)).toBe(10);
  });
});
