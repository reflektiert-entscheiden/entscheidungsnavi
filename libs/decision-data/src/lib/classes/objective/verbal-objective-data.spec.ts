import { Objective, ObjectiveType } from './objective';
import { VerbalObjectiveData } from './verbal-objective-data';

describe('Default Behaviour', () => {
  it('custom insert', () => {
    const objective = new Objective(
      'Objective 2',
      null,
      new VerbalObjectiveData(['opt1', 'opt2', 'opt3'], [0, 30, 100], undefined, undefined, undefined, undefined, true),
      null,
      ObjectiveType.Verbal,
    );

    objective.verbalData.addOption(1, 'opt1.5');

    expect(objective.verbalData.utilities[0]).toBe(0);
    expect(objective.verbalData.utilities[1]).toBe(15);
    expect(objective.verbalData.utilities[2]).toBe(30);
    expect(objective.verbalData.utilities[3]).toBe(100);
  });

  it('function insert', () => {
    const objective = new Objective('Objective 2', null, new VerbalObjectiveData(['opt1', 'opt2', 'opt3']), null, ObjectiveType.Verbal);

    objective.verbalData.addOption(null, 'opt4');
    objective.verbalData.addOption(null, 'opt5');

    expect(objective.verbalData.utilities[0]).toBe(0);
    expect(objective.verbalData.utilities[1]).toBe(25);
    expect(objective.verbalData.utilities[2]).toBe(50);
    expect(objective.verbalData.utilities[3]).toBe(75);
    expect(objective.verbalData.utilities[4]).toBe(100);
  });
});
