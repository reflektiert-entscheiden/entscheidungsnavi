export class VerbalObjectiveDataInitParams {
  get stepNumberIsValid() {
    return this.stepNumber >= 2 && this.stepNumber <= 7;
  }

  // valid if truthy (not empty or undefined)
  get fromIsValid() {
    return !!this.from;
  }

  // valid if truthy (not empty or undefined)
  get toIsValid() {
    return !!this.to;
  }

  get isValid(): boolean {
    return this.stepNumberIsValid && this.fromIsValid && this.toIsValid;
  }

  constructor(
    public stepNumber?: number,
    public from?: string,
    public to?: string,
  ) {
    if (stepNumber === undefined) {
      this.stepNumber = 5;
    }
  }
}
