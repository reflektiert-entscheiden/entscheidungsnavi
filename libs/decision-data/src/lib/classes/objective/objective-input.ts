/**
 * Denotes the input from which the utility can be calculated.
 * It takes the form number[indicatorIdx][stageIdx].
 */
export type ObjectiveInput = number[][];
