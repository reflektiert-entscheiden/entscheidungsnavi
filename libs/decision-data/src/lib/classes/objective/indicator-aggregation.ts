import { clamp } from 'lodash';
import { compileFormula } from '../../tools/mathjs-helper';
import { GlobalVariablesMap } from '../global-variables';
import { Indicator } from './indicator';
import { ObjectiveInput } from './objective-input';

/**
 * Returns the name of the variable in the aggregation function that corresponds to
 * the coefficient belonging to the given indicator.
 */
export function getIndicatorCoefficientName(indicatorIndex: number) {
  let name = '';
  let quotient = indicatorIndex + 1;

  // We basically convert the number to base 26 and display the digits as letters
  // from a to z.
  while (quotient > 0) {
    const remainder = (quotient - 1) % 26;
    name = String.fromCharCode(remainder + 97) + name;

    quotient = Math.floor((quotient - 1) / 26);
  }

  return name;
}

/**
 * Returns the name of the variable in the aggregation function that corresponds to
 * the value of the given indicator.
 */
export function getIndicatorValueName(indicatorIndex: number) {
  return `Ind${indicatorIndex + 1}`;
}

/**
 * Cache existing custom formulas so they don't have to be recompiled everytime
 */
const customFormulaCache = new Map<string, math.EvalFunction>();

/**
 * Either a custom formula or a worst/best combination for the default formula.
 */
export type AggregationSetting = { formula: string; globalVariables: GlobalVariablesMap } | { worst: number; best: number };

/**
 * Maps an indicator objective input to the aggregate.
 * @param values - The input values for the indicators
 * @param skipVerbalTransformation - If true, it is assumed that values for verbal indicators have already been transformed
 *                                   to indicator values.
 */
export type AggregationFunction = (values: ObjectiveInput, skipVerbalTransformation?: boolean) => number;

/**
 * Returns the aggregator function for the given indicator objective. The function calculates the aggregated
 * outcome value from the indicator input values. It uses the custom aggregator function in the objective
 * if available.
 *
 * @param indicators - The coefficients of the indicators
 * @param aggregationSetting - Either a custom aggregation formula as string to use or a worst/best value pair to use
 *                             with the default aggregation formula
 */
export function getIndicatorAggregationFunction(
  indicators: Pick<Indicator, 'min' | 'max' | 'coefficient' | 'verbalIndicatorCategories'>[],
  aggregationSetting: AggregationSetting,
  skipValidation = false,
): AggregationFunction {
  // A function to verify that the input is valid
  const validateInput = (objectiveValues: ObjectiveInput) => {
    for (let indicatorIndex = 0; indicatorIndex < objectiveValues.length; indicatorIndex++) {
      if (objectiveValues[indicatorIndex] == null) return false;
      for (let valueIndex = 0; valueIndex < objectiveValues[indicatorIndex].length; valueIndex++) {
        if (objectiveValues[indicatorIndex][valueIndex] == null) return false;
      }
    }

    return true;
  };

  if ('worst' in aggregationSetting) {
    // Use the default aggregation function if no custom one is specified:
    // (coeff0 * (ind0 - min0) / (max0 - min0) + ...) / (coeff1 + coeff2 + ...)
    // Special consideration is given to when the coefficient is negative.
    const sumOfCoefficients = indicators.reduce((accumulator, currentValue) => accumulator + currentValue.coefficient, 0);

    // Avoid division by zero
    if (sumOfCoefficients === 0) {
      return (_values: ObjectiveInput) => 0;
    } else {
      return (values: ObjectiveInput, skipVerbalTransformation = false) => {
        if (!skipValidation && !validateInput(values)) {
          return NaN;
        }

        let summedValue = 0;
        for (let i = 0; i < indicators.length; i++) {
          if (skipVerbalTransformation && values[i].length > 1) {
            throw new Error('skipVerbalTransformation was set to true, but we have multiple values for verbal categories.');
          }

          const indicatorValue = skipVerbalTransformation ? values[i][0] : calculateIndicatorValue(values[i], indicators[i]);
          summedValue += (indicators[i].coefficient * (indicatorValue - indicators[i].min)) / (indicators[i].max - indicators[i].min);
        }
        const normalizedValue = summedValue / sumOfCoefficients;

        // Stretch the normalized value across the bandwidth set by the aggregationSetting. Clamp to prevent numerical issues.
        if (aggregationSetting.best > aggregationSetting.worst) {
          return clamp(
            normalizedValue * (aggregationSetting.best - aggregationSetting.worst) + aggregationSetting.worst,
            aggregationSetting.worst,
            aggregationSetting.best,
          );
        } else {
          return clamp(
            (1 - normalizedValue) * (aggregationSetting.worst - aggregationSetting.best) + aggregationSetting.best,
            aggregationSetting.best,
            aggregationSetting.worst,
          );
        }
      };
    }
  } else {
    // Use the custom aggregation formula. This may throw an Error if the formula is invalid.
    let calc: math.EvalFunction;
    if (customFormulaCache.has(aggregationSetting.formula)) {
      calc = customFormulaCache.get(aggregationSetting.formula);
    } else {
      calc = compileFormula(aggregationSetting.formula);
      customFormulaCache.set(aggregationSetting.formula, calc);
    }

    // Build the scope consisting of global variables as well as variables for coefficients
    const scope = new Map(aggregationSetting.globalVariables);
    for (let i = 0; i < indicators.length; i++) {
      scope.set(getIndicatorCoefficientName(i), indicators[i].coefficient);
    }

    const indicatorValueNames = indicators.map((_, i) => getIndicatorValueName(i));

    return (values: ObjectiveInput, skipVerbalTransformation = false) => {
      if (!validateInput(values)) {
        return NaN;
      }

      // Add the indicator values to the scope
      for (let i = 0; i < indicators.length; i++) {
        if (skipVerbalTransformation && values[i].length > 1) {
          throw new Error('skipVerbalTransformation was set to true, but we have multiple values for verbal categories.');
        }

        const indicatorValue = skipVerbalTransformation ? values[i][0] : calculateIndicatorValue(values[i], indicators[i]);
        scope.set(indicatorValueNames[i], indicatorValue);
      }
      try {
        const result = calc.evaluate(scope);

        if (isFinite(result)) {
          return result;
        } else {
          return NaN;
        }
      } catch (_) {
        return NaN;
      }
    };
  }
}

// retrieves the indicator value for a single indicator vector outcome.values[stateIdx][indicatorIdx]
// returns the first entry for a STANDARD indicator
// calculates the indicator value based on the additive formula for a VERBAL indicator
export function calculateIndicatorValue(values: number[], indicator: Pick<Indicator, 'min' | 'max' | 'verbalIndicatorCategories'>) {
  if (indicator.verbalIndicatorCategories.length === 0) return values[0]; // no categories
  let sum = 0;
  let divisor = 0; // can't stay 0
  values.forEach((valuePerCategory, categoryIdx) => {
    if (valuePerCategory > indicator.verbalIndicatorCategories[categoryIdx].stages.length - 1) {
      throw new Error('Invalid verbal indicator value');
    }

    // between 0 and 1
    const scalePortion = valuePerCategory / (indicator.verbalIndicatorCategories[categoryIdx].stages.length - 1);

    sum += indicator.verbalIndicatorCategories[categoryIdx].weight * scalePortion;
    divisor += indicator.verbalIndicatorCategories[categoryIdx].weight;
  });
  // do computation, based on value and weight (same as additive formula for indicators)
  return indicator.min + (indicator.max - indicator.min) * (sum / divisor);
}
