import { cloneDeep, flatMapDeep } from 'lodash';
import { v4 as uuidv4 } from 'uuid';
import { assertUnreachable } from '@entscheidungsnavi/tools/assert-unreachable';
import { Tree } from '@entscheidungsnavi/tools';
import { Interval } from '@entscheidungsnavi/tools/interval/interval';
import { getIndicatorUtilityFunction, getNumericalUtilityFunction, getVerbalUtilityFunction } from '../../utility/utility';
import { ObjectiveElement } from './objective-aspects';
import { NumericalObjectiveData } from './numerical-objective-data';
import { VerbalObjectiveData } from './verbal-objective-data';
import { IndicatorObjectiveData } from './indicator-objective-data';

export enum ObjectiveType {
  Numerical,
  Verbal,
  Indicator,
}

export class Objective {
  get isNumerical() {
    return this.objectiveType === ObjectiveType.Numerical;
  }

  get isVerbal() {
    return this.objectiveType === ObjectiveType.Verbal;
  }

  get isIndicator() {
    return this.objectiveType === ObjectiveType.Indicator;
  }

  /**
   * True iff the objective has a linear utility function (and is numerical or indicator)
   */
  get hasLinearUtilityFunction() {
    return (
      (this.isNumerical && this.numericalData.utilityfunction.c === 0) || (this.isIndicator && this.indicatorData.utilityfunction.c === 0)
    );
  }

  get includesVerbalIndicators() {
    return this.isIndicator && this.indicatorData.indicators.some(ind => ind.isVerbalized);
  }

  get isScaleReverse() {
    switch (this.objectiveType) {
      case ObjectiveType.Numerical:
        return this.numericalData.from > this.numericalData.to;
      case ObjectiveType.Verbal:
        return false;
      case ObjectiveType.Indicator:
        return this.indicatorData.worstValue > this.indicatorData.bestValue;
      default:
        assertUnreachable(this.objectiveType);
    }
  }

  constructor(
    public name = '',
    public numericalData: NumericalObjectiveData = new NumericalObjectiveData(),
    public verbalData: VerbalObjectiveData = new VerbalObjectiveData(),
    public indicatorData: IndicatorObjectiveData = new IndicatorObjectiveData(),
    public objectiveType = ObjectiveType.Numerical,
    public displayed = true,
    public comment?: string,
    public scaleComment?: string,
    public aspects?: Tree<ObjectiveElement>,
    public placeholder?: string,
    public uuid = uuidv4(),
  ) {
    if (this.aspects == null) {
      this.resetAspects();
    }

    if (numericalData == null) {
      this.numericalData = new NumericalObjectiveData();
    }
    if (verbalData == null) {
      this.verbalData = new VerbalObjectiveData();
    }
    if (indicatorData == null) {
      this.indicatorData = new IndicatorObjectiveData();
    }
  }

  clone(): Objective {
    return cloneDeep(this);
  }

  getUtilityFunction() {
    switch (this.objectiveType) {
      case ObjectiveType.Numerical:
        return getNumericalUtilityFunction(this.numericalData.utilityfunction.c, this.numericalData.from, this.numericalData.to);
      case ObjectiveType.Verbal:
        return getVerbalUtilityFunction(this.verbalData.utilities);
      case ObjectiveType.Indicator:
        return getIndicatorUtilityFunction(
          this.indicatorData.utilityfunction.c,
          this.indicatorData.aggregationFunction,
          this.indicatorData.worstValue,
          this.indicatorData.bestValue,
        );
    }
  }

  getAspectsAsArray(): ReadonlyArray<string> {
    const f = (ct: Tree<ObjectiveElement>): Array<Tree<ObjectiveElement>> => {
      return [ct].concat(ct.children !== undefined ? flatMapDeep(ct.children, f) : []);
    };

    return flatMapDeep(this.aspects.children, f)
      .filter(t => t.value.name)
      .map(t => t.value.name);
  }

  private resetAspects() {
    this.aspects = new Tree<ObjectiveElement>(new ObjectiveElement(this.name, null, null, null, this.placeholder));
  }

  getRangeInterval(): Interval {
    switch (this.objectiveType) {
      case ObjectiveType.Numerical:
        return new Interval(+this.numericalData.from, +this.numericalData.to);
      case ObjectiveType.Indicator:
        return new Interval(this.indicatorData.worstValue, this.indicatorData.bestValue);
      case ObjectiveType.Verbal:
        return new Interval(1, this.verbalData.optionCount());
      default:
        assertUnreachable(this.objectiveType);
    }
  }

  unit(): string {
    switch (this.objectiveType) {
      case ObjectiveType.Numerical:
        return this.numericalData.unit;
      case ObjectiveType.Indicator:
        return this.indicatorData.aggregatedUnit;
      case ObjectiveType.Verbal:
        return '';
      default:
        assertUnreachable(this.objectiveType);
    }
  }

  hasSameScaleAs(otherObjective: Objective): boolean {
    if (this.objectiveType !== otherObjective.objectiveType) {
      return false;
    }

    switch (this.objectiveType) {
      case ObjectiveType.Numerical:
        return this.numericalData.from === otherObjective.numericalData.from && this.numericalData.to === otherObjective.numericalData.to;
      case ObjectiveType.Indicator:
        return (
          this.indicatorData.indicators.length === otherObjective.indicatorData.indicators.length &&
          this.indicatorData.indicators.every(
            (indicator, index) =>
              indicator.name === otherObjective.indicatorData.indicators[index].name &&
              indicator.min === otherObjective.indicatorData.indicators[index].min &&
              indicator.max === otherObjective.indicatorData.indicators[index].max,
          )
        );
      case ObjectiveType.Verbal:
        return (
          this.verbalData.options.length === otherObjective.verbalData.options.length &&
          this.verbalData.options.every((option, index) => option === otherObjective.verbalData.options[index])
        );
      default:
        assertUnreachable(this.objectiveType);
    }
  }
}
