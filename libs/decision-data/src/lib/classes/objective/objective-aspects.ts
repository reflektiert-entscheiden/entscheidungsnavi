import { omit } from 'lodash';
import { Tree } from '@entscheidungsnavi/tools';
import { DecisionData } from '../../decision-data';

export type ObjectivesStep = (typeof OBJECTIVES_STEPS)[number];

export const OBJECTIVES_STEPS = [
  'FIRST_BRAINSTORMING',
  'FURTHER_CONSIDERATIONS',
  'SUGGESTION_LISTS',
  'FIRST_OBJECTIVE_HIERARCHY',
  'REVIEW',
  'RESULT',
] as const;

export class ObjectiveElement {
  name: string;
  comment?: string;
  scaleComment?: string;
  createdInSubStep?: number;
  placeholder?: string;
  backgroundColor?: string;
  textColor?: string;
  isAspect?: boolean;
  orderIdx?: number; // Index of this objective in the merged array from {listOfAspects, listOfDeletedAspects, aspectsFromTree}

  constructor(
    name = '',
    createdInSubStep = 0,
    backgroundColor?: string,
    textColor?: string,
    placeholder?: string,
    isAspect = false,
    orderIdx?: number,
    comment?: string,
    scaleComment?: string,
  ) {
    this.name = name;
    this.createdInSubStep = createdInSubStep;
    this.backgroundColor = backgroundColor;
    this.textColor = textColor;
    this.placeholder = placeholder;
    this.isAspect = isAspect;
    this.orderIdx = orderIdx;
    this.comment = comment;
    this.scaleComment = scaleComment;
  }
}

// step 3 (alternatives) hint data. the indices are 1-based.
export class ObjectiveAspects {
  public subStepProgression: ObjectivesStep | null = null;

  public listOfAspects: ObjectiveElement[]; // list of aspects
  public listOfDeletedAspects: ObjectiveElement[]; // list of deleted aspects

  constructor(
    public decisionData: DecisionData,
    subStepProgression: ObjectivesStep | null = null,
    listOfAspects: ObjectiveElement[] = [],
    listOfDeletedAspects: ObjectiveElement[] = [],
  ) {
    this.subStepProgression = subStepProgression;
    this.listOfAspects = listOfAspects;
    this.listOfDeletedAspects = listOfDeletedAspects;
  }

  toJSON() {
    return omit(this, ['decisionData']);
  }

  generateInitialAspects() {
    const importantValues = this.decisionData.decisionStatement.getImportantValues();

    if (importantValues) {
      this.decisionData.objectiveAspects.listOfAspects.push(
        ...importantValues.map(v => {
          const objectiveElement = new ObjectiveElement(v.name);
          objectiveElement.createdInSubStep = 1;
          return objectiveElement;
        }),
      );
    }
  }

  getAspectTree(rootName: string): Tree<ObjectiveElement> {
    const mainElement = this.decisionData.objectiveHierarchyMainElement;

    if (mainElement.name === '') {
      mainElement.name = rootName;
    }

    const tree = new Tree<ObjectiveElement>(mainElement);

    tree.children = this.decisionData.objectives.map(o => {
      o.aspects.value.name = o.name;
      o.aspects.value.placeholder = o.placeholder;
      o.aspects.value.comment = o.comment;
      o.aspects.value.scaleComment = o.scaleComment;
      return o.aspects;
    });

    return this.getDraggedTreeWithComments(tree);
  }

  private getCommentForLocationSequence(locationSequence: number[]) {
    if (locationSequence.length < 2) {
      return null;
    } else if (locationSequence.length === 2) {
      return this.decisionData.objectives[locationSequence[1]].comment;
    } else {
      return this.getCommentForLocationSequenceRecursive(this.decisionData.objectives[locationSequence[1]].aspects, locationSequence, 2);
    }
  }

  private getCommentForLocationSequenceRecursive(aspects: Tree<ObjectiveElement>, locationSequence: number[], currentIdx: number): string {
    if (currentIdx === locationSequence.length) {
      return aspects.value.comment;
    } else {
      return this.getCommentForLocationSequenceRecursive(aspects.children[locationSequence[currentIdx]], locationSequence, currentIdx + 1);
    }
  }

  private getDraggedTreeWithComments(draggedTree: Tree<ObjectiveElement>, locationSequence: number[] = [0]) {
    draggedTree.value.comment = this.getCommentForLocationSequence(locationSequence);
    if (draggedTree.children.length > 0) {
      for (let i = 0; i < draggedTree.children.length; i++) {
        const newLocationSequence = [...locationSequence];
        newLocationSequence.push(i);
        draggedTree.children[i] = this.getDraggedTreeWithComments(draggedTree.children[i], newLocationSequence);
      }
    }
    return draggedTree;
  }
}
