import { Alternative } from './classes/alternative/alternative';
import { SimpleUserDefinedInfluenceFactor, UserDefinedState } from './classes/influence-factor/simple-user-defined-influence-factor';
import { Objective, ObjectiveType } from './classes/objective/objective';
import { Outcome } from './classes/outcome/outcome';
import { DecisionData } from './decision-data';
import { Indicator } from './classes/objective/indicator';
import { VerbalObjectiveData } from './classes/objective/verbal-objective-data';
import { IndicatorObjectiveData } from './classes/objective/indicator-objective-data';

describe('DecisionData Functions', () => {
  let data: DecisionData;
  beforeEach(() => {
    data = new DecisionData();
  });

  describe('Testset 1', () => {
    let uf: SimpleUserDefinedInfluenceFactor;
    beforeEach(() => {
      uf = new SimpleUserDefinedInfluenceFactor('', [new UserDefinedState('', 30), new UserDefinedState('', 70)]);
      data.influenceFactors = [uf];
      data.alternatives = [new Alternative(), new Alternative()];
      data.objectives = [
        new Objective('', undefined, undefined, undefined, ObjectiveType.Numerical),
        new Objective('', undefined, undefined, undefined, ObjectiveType.Indicator),
      ];
      data.outcomes = [
        [new Outcome([[[1]]]), new Outcome([[[1], [2]]])],
        [
          new Outcome([[[1]], [[2]]], null, uf),
          new Outcome(
            [
              [[1], [2]],
              [[2], [3]],
            ],
            null,
            uf,
          ),
        ],
      ];
    });

    describe('addObjective', () => {
      it('updates outcomes', () => {
        data.addObjective(new Objective('', null, null, null, ObjectiveType.Verbal), 0);
        data.outcomes.forEach(ocForAlternative => {
          expect(ocForAlternative.length).toBe(3);
          expect(ocForAlternative[0].values).toEqual([[[undefined]]]);
          expect(ocForAlternative[0].processed).toBe(false);
        });
      });
    });

    describe('addAlternative', () => {
      it('updates outcomes', () => {
        data.addAlternative({
          alternative: new Alternative(),
          position: 0,
        });
        expect(data.outcomes.length).toBe(3);
        expect(data.outcomes[0].length).toBe(2);
        expect(data.outcomes[0][0].values).toEqual([[[undefined]]]);
        expect(data.outcomes[0][0].processed).toBe(false);
        expect(data.outcomes[0][1].values).toEqual([[[undefined], [undefined]]]);
        expect(data.outcomes[0][1].processed).toBe(false);
      });
    });

    describe('addState', () => {
      it('updates outcomes', () => {
        data.addState(uf, new UserDefinedState('', 30), 1);
        expect(data.outcomes[1][0].values).toEqual([[[1]], [[undefined]], [[2]]]);
        expect(data.outcomes[1][1].values).toEqual([
          [[1], [2]],
          [[undefined], [undefined]],
          [[2], [3]],
        ]);
      });
    });

    describe('removeState', () => {
      it('updates outcomes', () => {
        data.removeState(uf, 1);
        expect(data.outcomes[1][0].values).toEqual([[[1]]]);
        expect(data.outcomes[1][1].values).toEqual([[[1], [2]]]);
      });
    });

    describe('moveState', () => {
      it('updates outcomes', () => {
        data.moveState(uf, 1, 0);

        expect(data.outcomes[0][0].values).toEqual([[[1]]]);
        expect(data.outcomes[0][1].values).toEqual([[[1], [2]]]);

        expect(data.outcomes[1][0].values).toEqual([[[2]], [[1]]]);
        expect(data.outcomes[1][1].values).toEqual([
          [[2], [3]],
          [[1], [2]],
        ]);
      });
    });

    describe('addObjectiveIndicator', () => {
      it('adds indicator', () => {
        const oldInd = data.objectives[1].indicatorData.indicators[0];
        data.addObjectiveIndicator(1, 0);
        expect(data.objectives[1].indicatorData.indicators.length).toBe(3);
        // Check that it is in fact added to the right position
        expect(data.objectives[1].indicatorData.indicators[0]).not.toBe(oldInd);
        expect(data.objectives[1].indicatorData.indicators[1]).toBe(oldInd);
        expect(data.objectives[1].indicatorData.indicators[0]).toBeInstanceOf(Indicator);
      });

      it('updates outcomes', () => {
        data.addObjectiveIndicator(1, 0);
        data.outcomes[0][1].values.forEach(value => {
          expect(Array.isArray(value)).toBe(true);
          expect(value.length).toBe(3);
        });
        data.outcomes[1][1].values.forEach(value => {
          expect(Array.isArray(value)).toBe(true);
          expect(value.length).toBe(3);
        });
      });
    });

    describe('removeObjectiveIndicator', () => {
      beforeEach(() => {
        data.addObjectiveIndicator(1, 0);
      });

      it('removes indicator', () => {
        const toRemove = data.objectives[1].indicatorData.indicators[0];
        data.removeObjectiveIndicator(1, 0);
        expect(data.objectives[1].indicatorData.indicators.length).toBe(2);
        // Check that the right one was removed
        expect(data.objectives[1].indicatorData.indicators[0]).not.toBe(toRemove);
      });

      it('updates outcomes', () => {
        data.removeObjectiveIndicator(1, 0);
        data.outcomes[0][1].values.forEach(value => {
          expect(Array.isArray(value)).toBe(true);
          expect(value.length).toBe(2);
        });
        data.outcomes[1][1].values.forEach(value => {
          expect(Array.isArray(value)).toBe(true);
          expect(value.length).toBe(2);
        });
      });
    });

    describe('moveObjectiveIndicator', () => {
      it('moves the indicators', () => {
        const [ind1, ind2] = data.objectives[1].indicatorData.indicators;
        data.moveObjectiveIndicator(1, 1, 0);
        expect(data.objectives[1].indicatorData.indicators).toEqual([ind2, ind1]);
      });

      it('updates outcomes', () => {
        data.moveObjectiveIndicator(1, 1, 0);
        expect(data.outcomes[0][1].values).toEqual([[[2], [1]]]);
        expect(data.outcomes[1][1].values).toEqual([
          [[2], [1]],
          [[3], [2]],
        ]);
      });
    });
  });

  describe('changeObjectiveType', () => {
    beforeEach(() => {
      data.addAlternative({ alternative: new Alternative() });
      data.addAlternative({ alternative: new Alternative() });
      data.addObjective(new Objective(undefined, undefined, new VerbalObjectiveData([''], [0]), undefined, ObjectiveType.Numerical));
      data.addObjective(new Objective(undefined, undefined, new VerbalObjectiveData([''], [0]), undefined, ObjectiveType.Verbal));
      data.addObjective(
        new Objective(
          undefined,
          undefined,
          new VerbalObjectiveData([''], [0]),
          new IndicatorObjectiveData([new Indicator(), new Indicator()]),
          ObjectiveType.Indicator,
        ),
      );
      data.outcomes[0][0] = new Outcome([[[0]]], null, null);
      data.outcomes[1][0] = new Outcome(
        [[[0]], [[1]], [[2]]],
        null,
        new SimpleUserDefinedInfluenceFactor(undefined, [
          new UserDefinedState('', 0),
          new UserDefinedState('', 0),
          new UserDefinedState('', 0),
        ]),
      );
      data.outcomes[0][1] = new Outcome([[[1]]], null, null);
      data.outcomes[1][1] = new Outcome(
        [[[1]], [[1]], [[1]]],
        null,
        new SimpleUserDefinedInfluenceFactor(undefined, [
          new UserDefinedState('', 0),
          new UserDefinedState('', 0),
          new UserDefinedState('', 0),
        ]),
      );
      data.outcomes[0][2] = new Outcome([[[0], [0]]], null, null);
      data.outcomes[1][2] = new Outcome(
        [
          [[0], [0]],
          [[1], [1]],
          [[2], [2]],
        ],
        null,
        new SimpleUserDefinedInfluenceFactor(undefined, [
          new UserDefinedState('', 0),
          new UserDefinedState('', 0),
          new UserDefinedState('', 0),
        ]),
      );
    });

    it('changes to numerical', () => {
      data.changeObjectiveType(0, ObjectiveType.Numerical);
      expect(data.objectives[0].objectiveType).toBe(ObjectiveType.Numerical);
      data.changeObjectiveType(1, ObjectiveType.Numerical);
      expect(data.objectives[1].objectiveType).toBe(ObjectiveType.Numerical);
      data.changeObjectiveType(2, ObjectiveType.Numerical);
      expect(data.objectives[2].objectiveType).toBe(ObjectiveType.Numerical);

      expect(data.outcomes.every(ocForAlternative => ocForAlternative[0].values.every(value => typeof value[0][0] === 'number'))).toBe(
        true,
      );
      // expect(validateOutcomes(data)[0]).toBe(true);
    });

    it('changes to verbal', () => {
      data.changeObjectiveType(0, ObjectiveType.Verbal);
      expect(data.objectives[0].objectiveType).toBe(ObjectiveType.Verbal);
      data.changeObjectiveType(1, ObjectiveType.Verbal);
      expect(data.objectives[1].objectiveType).toBe(ObjectiveType.Verbal);
      data.changeObjectiveType(2, ObjectiveType.Verbal);
      expect(data.objectives[2].objectiveType).toBe(ObjectiveType.Verbal);

      expect(data.outcomes.every(ocForAlternative => ocForAlternative[1].values.every(value => typeof value[0][0] === 'number'))).toBe(
        true,
      );
      // expect(validateOutcomes(data)[0]).toBe(true);
    });

    it('changes to indicator', () => {
      data.changeObjectiveType(0, ObjectiveType.Indicator);
      expect(data.objectives[0].objectiveType).toBe(ObjectiveType.Indicator);
      data.changeObjectiveType(1, ObjectiveType.Indicator);
      expect(data.objectives[1].objectiveType).toBe(ObjectiveType.Indicator);
      data.changeObjectiveType(2, ObjectiveType.Indicator);
      expect(data.objectives[2].objectiveType).toBe(ObjectiveType.Indicator);

      expect(
        data.outcomes.every(ocForAlternative =>
          ocForAlternative[2].values.every(
            value =>
              Array.isArray(value) &&
              value.every(nestedValue => Array.isArray(nestedValue) && nestedValue.every(val => typeof val === 'number')),
          ),
        ),
      ).toBe(true);
      // expect(validateOutcomes(data)[0]).toBe(true);
    });
  });

  describe('moveInfluenceFactor', () => {
    let ufs: SimpleUserDefinedInfluenceFactor[];
    beforeEach(() => {
      ufs = [
        new SimpleUserDefinedInfluenceFactor('0', [], 0),
        new SimpleUserDefinedInfluenceFactor('1', [], 1),
        new SimpleUserDefinedInfluenceFactor('2', [], 2),
        new SimpleUserDefinedInfluenceFactor('3', [], 3),
        new SimpleUserDefinedInfluenceFactor('4', [], 4),
      ];
      data.influenceFactors = ufs;
    });

    it('moves the influence factor', () => {
      data.moveInfluenceFactor(0, 2);
      expect(data.influenceFactors.map(uf => uf.name)).toEqual(['1', '2', '0', '3', '4']);
      data.moveInfluenceFactor(0, 4);
      expect(data.influenceFactors.map(uf => uf.name)).toEqual(['2', '0', '3', '4', '1']);
      data.moveInfluenceFactor(2, 3);
      expect(data.influenceFactors.map(uf => uf.name)).toEqual(['2', '0', '4', '3', '1']);
      data.moveInfluenceFactor(1, 4);
      expect(data.influenceFactors.map(uf => uf.name)).toEqual(['2', '4', '3', '1', '0']);
      data.moveInfluenceFactor(2, 2);
      expect(data.influenceFactors.map(uf => uf.name)).toEqual(['2', '4', '3', '1', '0']);
    });

    it('updates the ids', () => {
      data.moveInfluenceFactor(0, 2);
      expect(data.influenceFactors.map(uf => uf.id)).toEqual([0, 1, 2, 3, 4]);
      data.moveInfluenceFactor(0, 4);
      expect(data.influenceFactors.map(uf => uf.id)).toEqual([0, 1, 2, 3, 4]);
      data.moveInfluenceFactor(2, 3);
      expect(data.influenceFactors.map(uf => uf.id)).toEqual([0, 1, 2, 3, 4]);
      data.moveInfluenceFactor(1, 4);
      expect(data.influenceFactors.map(uf => uf.id)).toEqual([0, 1, 2, 3, 4]);
      data.moveInfluenceFactor(2, 2);
      expect(data.influenceFactors.map(uf => uf.id)).toEqual([0, 1, 2, 3, 4]);
    });
  });

  describe('moveObjectiveOption', () => {
    let data: DecisionData;

    beforeEach(() => {
      data = new DecisionData();
      const obj = new Objective();
      obj.objectiveType = ObjectiveType.Verbal;
      obj.verbalData.addOption(0, 'Option 1');
      obj.verbalData.addOption(1, 'Option 2');
      obj.verbalData.addOption(2, 'Option 3');
      data.addObjective(obj);
      data.addAlternative();
      data.addAlternative();
      data.addAlternative();
    });

    it('handles moving an option backwards', () => {
      data.outcomes[0][0].values = [[[null]]];
      data.outcomes[1][0].values = [[[2]]];
      data.outcomes[2][0].values = [[[3]]];

      data.moveObjectiveOption(0, 2, 1);

      expect(data.objectives[0].verbalData.options[0]).toBe('Option 1');
      expect(data.objectives[0].verbalData.options[1]).toBe('Option 3');
      expect(data.objectives[0].verbalData.options[2]).toBe('Option 2');

      expect(data.outcomes[0][0].values[0][0][0]).toBe(null);
      expect(data.outcomes[1][0].values[0][0][0]).toBe(3);
      expect(data.outcomes[2][0].values[0][0][0]).toBe(2);
    });

    it('handles moving an option forwards', () => {
      data.outcomes[0][0].values = [[[1]]];
      data.outcomes[1][0].values = [[[null]]];
      data.outcomes[2][0].values = [[[3]]];

      data.moveObjectiveOption(0, 0, 2);

      expect(data.objectives[0].verbalData.options[0]).toBe('Option 2');
      expect(data.objectives[0].verbalData.options[1]).toBe('Option 3');
      expect(data.objectives[0].verbalData.options[2]).toBe('Option 1');

      expect(data.outcomes[0][0].values[0][0][0]).toBe(3);
      expect(data.outcomes[1][0].values[0][0][0]).toBe(null);
      expect(data.outcomes[2][0].values[0][0][0]).toBe(2);
    });
  });
});
