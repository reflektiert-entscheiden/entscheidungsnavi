import { omit } from 'lodash';
import { DecisionData } from './decision-data';
import { Objective } from './classes/objective/objective';
import { Alternative } from './classes/alternative/alternative';
import { SimpleUserDefinedInfluenceFactor, UserDefinedState } from './classes/influence-factor/simple-user-defined-influence-factor';
import { readText } from './export/import';

describe('DecisionData', () => {
  let data: DecisionData;

  beforeEach(() => {
    data = new DecisionData();
  });

  describe('without data object', () => {
    it('add empty objective', () => {
      expect(data.objectives.length).toBe(0);
      data.addObjective();
      expect(data.objectives.length).toBe(1);
      expect(data.getAttachedObjectiveData(0)).toEqual([]);
      data.addObjective();
      expect(data.objectives.length).toBe(2);

      const objective0WithoutUuid = omit(data.objectives[0], 'uuid');
      const objective1WithoutUuid = omit(data.objectives[1], 'uuid');
      expect(objective0WithoutUuid).not.toBe(objective1WithoutUuid);
      expect(objective0WithoutUuid).toEqual(objective1WithoutUuid);
    });
    it('delete empty objective', () => {
      data.addObjective();
      data.addObjective();
      expect(data.objectives.length).toBe(2);
      data.removeObjective(0);
      expect(data.objectives.length).toBe(1);
    });
    it('add (insert) objective', () => {
      data.addObjective(new Objective('1'));
      data.addObjective(new Objective('2'));
      data.addObjective(new Objective('3'));
      data.addObjective(new Objective('new'), 1);
      expect(data.objectives[1].name).toBe('new');
    });

    it('add empty alternative', () => {
      expect(data.alternatives.length).toBe(0);
      data.addAlternative();
      expect(data.alternatives.length).toBe(1);
      data.addAlternative();
      expect(data.alternatives.length).toBe(2);
    });
    it('delete empty alternative', () => {
      data.addAlternative();
      data.addAlternative();
      expect(data.alternatives.length).toBe(2);
      data.removeAlternative(0);
      expect(data.alternatives.length).toBe(1);
    });
    it('add (insert) Alternative', () => {
      data.addAlternative({ alternative: new Alternative(1, '1') });
      data.addAlternative({ alternative: new Alternative(1, '2') });
      data.addAlternative({ alternative: new Alternative(1, '3') });
      data.addAlternative({ alternative: new Alternative(1, 'new'), position: 1 });
      expect(data.alternatives[1].name).toBe('new');
    });

    it('add empty SimpleUserDefinedInfluenceFactor', () => {
      expect(data.influenceFactors.length).toBe(0);
      data.addInfluenceFactor();
      expect(data.influenceFactors.length).toBe(1);
      data.addInfluenceFactor();
      expect(data.influenceFactors.length).toBe(2);
    });
    it('delete empty SimpleUserDefinedInfluenceFactor', () => {
      data.addInfluenceFactor();
      data.addInfluenceFactor();
      expect(data.influenceFactors.length).toBe(2);
      data.removeInfluenceFactor(0);
      expect(data.influenceFactors.length).toBe(1);
    });
    it('add (insert) SimpleUserDefinedInfluenceFactor', () => {
      data.addInfluenceFactor(new SimpleUserDefinedInfluenceFactor('1'));
      data.addInfluenceFactor(new SimpleUserDefinedInfluenceFactor('2'));
      data.addInfluenceFactor(new SimpleUserDefinedInfluenceFactor('3'));
      data.addInfluenceFactor(new SimpleUserDefinedInfluenceFactor('new'));
      expect(data.influenceFactors[3].name).toBe('new');
      expect((data.influenceFactors[3] as SimpleUserDefinedInfluenceFactor).probabilitiesSum()).toBe(0);
      expect((data.influenceFactors[3] as SimpleUserDefinedInfluenceFactor).checkProbabilities()).toBeFalsy();
    });
  });

  describe('with existing data', () => {
    beforeEach(() => {
      data.addObjective(new Objective('1'));
      data.addObjective(new Objective('2'));
      data.addObjective(new Objective('3'));

      data.addAlternative({ alternative: new Alternative(1, 'a') });
      data.addAlternative({ alternative: new Alternative(1, 'b') });
      data.addAlternative({ alternative: new Alternative(1, 'c') });
      data.addAlternative({ alternative: new Alternative(1, 'd') });

      const states = [new UserDefinedState('u1', 30), new UserDefinedState('u2', 70)];

      data.addInfluenceFactor(new SimpleUserDefinedInfluenceFactor('uf1', states));
    });

    it('outcomes set', () => {
      expect(data.outcomes.length).toBe(4);

      data.outcomes.forEach(a => {
        expect(a).toBeDefined();
        expect(a.length).toBe(3);
        a.forEach(outcome => {
          expect(outcome).toBeDefined();
          expect(outcome.processed).toBeFalsy();
          outcome.checkProcessed();
          expect(outcome.processed).toBeFalsy();
          expect(outcome.values).toBeDefined();
          expect(outcome.values.length).toBe(1);
          expect(outcome.values[0][0][0]).not.toBeDefined();
        });
      });
    });

    it('remove ziel', () => {
      // mark and check outcomes
      data.outcomes.forEach(a => {
        expect(a.length === 3);
        a.forEach((outcome, idx) => (outcome.values[0][0][0] = idx));
        expect(a.map(outcome => outcome.values[0])).toEqual([[[0]], [[1]], [[2]]]);
      });

      // remove objective
      data.removeObjective(1);

      // check outcomes
      data.outcomes.forEach(a => {
        expect(a.length === 2);
        expect(a.map(outcome => outcome.values[0])).toEqual([[[0]], [[2]]]);
      });
    });

    it('remove alternative', () => {
      // mark and check outcomes
      data.outcomes.forEach((a, idx) => {
        expect(a.length === 3);
        a.forEach(outcome => (outcome.values[0][0][0] = idx));
      });
      expect(data.outcomes.length).toBe(4);
      expect(data.outcomes.map(a => a.map(outcome => outcome.values[0]))).toEqual([
        [[[0]], [[0]], [[0]]],
        [[[1]], [[1]], [[1]]],
        [[[2]], [[2]], [[2]]],
        [[[3]], [[3]], [[3]]],
      ]);

      // remove alternative
      data.removeAlternative(1);

      // check outcomes
      expect(data.outcomes.length).toBe(3);
      expect(data.outcomes.map(a => a.map(outcome => outcome.values[0]))).toEqual([
        [[[0]], [[0]], [[0]]],
        [[[2]], [[2]], [[2]]],
        [[[3]], [[3]], [[3]]],
      ]);
    });

    it('influence factor propabilities', () => {
      expect((data.influenceFactors[0] as SimpleUserDefinedInfluenceFactor).probabilitiesSum()).toBe(100);
      expect((data.influenceFactors[0] as SimpleUserDefinedInfluenceFactor).checkProbabilities()).toBeTruthy();
    });

    describe('set influence factor', () => {
      beforeEach(() => {
        // set influence factor for all outcomes
        data.outcomes.forEach(a =>
          a.forEach(outcome => {
            outcome.setInfluenceFactor(data.influenceFactors[0]);
          }),
        );
      });
      it('check length', () => {
        // check values lengths
        data.outcomes.forEach(a =>
          a.forEach(outcome => {
            expect(outcome.values.length).toBe(data.influenceFactors[0].stateCount);
          }),
        );
      });

      it('check default values', () => {
        data.outcomes.forEach(a =>
          a.forEach(outcome => {
            expect(outcome.values).toEqual([[[undefined]], [[undefined]]]);
          }),
        );
      });

      it('add zustand default position', () => {
        // set all values to their index
        data.outcomes.forEach(a =>
          a.forEach(outcome => {
            data.influenceFactors[0].getStates().forEach((v, idx) => {
              outcome.values[idx][0][0] = idx;
            });
          }),
        );
        // check values [0, 1]
        data.outcomes.forEach(a =>
          a.forEach(outcome => {
            expect(outcome.values).toEqual([[[0]], [[1]]]);
          }),
        );
        // add zustand
        data.addState(data.influenceFactors[0] as SimpleUserDefinedInfluenceFactor, new UserDefinedState('test name', 20));
        // check values [0, 1, undefined]
        data.outcomes.forEach(a =>
          a.forEach(outcome => {
            expect(outcome.values).toEqual([[[0]], [[1]], [[undefined]]]);
          }),
        );
        expect((data.influenceFactors[0] as SimpleUserDefinedInfluenceFactor).probabilitiesSum()).toBe(120);
        expect((data.influenceFactors[0] as SimpleUserDefinedInfluenceFactor).checkProbabilities()).toBeFalsy();
      });

      it('add zustand at index 1', () => {
        // set all values to their index
        data.outcomes.forEach(a =>
          a.forEach(outcome => {
            data.influenceFactors[0].getStates().forEach((v, idx) => {
              outcome.values[idx][0][0] = idx;
            });
          }),
        );
        // check values [0, 1]
        data.outcomes.forEach(a =>
          a.forEach(outcome => {
            expect(outcome.values).toEqual([[[0]], [[1]]]);
          }),
        );
        // add zustand
        data.addState(data.influenceFactors[0] as SimpleUserDefinedInfluenceFactor, new UserDefinedState('test name', 30), 1);
        // check values [0, undefined, 1]
        data.outcomes.forEach(a =>
          a.forEach(outcome => {
            expect(outcome.values).toEqual([[[0]], [[undefined]], [[1]]]);
          }),
        );
        expect((data.influenceFactors[0] as SimpleUserDefinedInfluenceFactor).probabilitiesSum()).toBe(130);
      });

      it('remove zustand 0', () => {
        // set all values to their index
        data.outcomes.forEach(a =>
          a.forEach(outcome => {
            data.influenceFactors[0].getStates().forEach((v, idx) => {
              outcome.values[idx][0][0] = idx;
            });
          }),
        );
        // remove zustand 0
        data.removeState(data.influenceFactors[0] as SimpleUserDefinedInfluenceFactor, 0);
        // check values [1]
        data.outcomes.forEach(a =>
          a.forEach(outcome => {
            expect(outcome.values).toEqual([[[1]]]);
          }),
        );
        expect((data.influenceFactors[0] as SimpleUserDefinedInfluenceFactor).probabilitiesSum()).toBe(70);
      });

      it('remove zustand 1', () => {
        // set all values to their index
        data.outcomes.forEach(a =>
          a.forEach(outcome => {
            data.influenceFactors[0].getStates().forEach((v, idx) => {
              outcome.values[idx][0][0] = idx;
            });
          }),
        );
        // remove zustand 1
        data.removeState(data.influenceFactors[0] as SimpleUserDefinedInfluenceFactor, 1);
        // check values [0]
        data.outcomes.forEach(a =>
          a.forEach(outcome => {
            expect(outcome.values).toEqual([[[0]]]);
          }),
        );
        expect((data.influenceFactors[0] as SimpleUserDefinedInfluenceFactor).probabilitiesSum()).toBe(30);
      });
    });

    describe('verbal objective options', () => {
      describe('addObjectiveOption', () => {
        let spy: jest.SpyInstance;
        beforeEach(() => {
          spy = jest.spyOn(data.objectiveOptionAdded$, 'next');
        });

        afterEach(() => {
          spy.mockRestore();
        });

        it('With index', () => {
          data.addObjectiveOption(0, 0, 'o1');
          data.addObjectiveOption(0, 1, 'o2');
          data.addObjectiveOption(0, 0, 'o3');

          expect(data.objectives[0].verbalData.options).toEqual(['o3', 'o1', 'o2']);
          expect(spy).toHaveBeenCalledTimes(3);
        });

        it('Without index', () => {
          data.addObjectiveOption(0, undefined, 'o1');
          data.addObjectiveOption(0, 0, 'o2');
          data.addObjectiveOption(0, undefined, 'o3');

          expect(data.objectives[0].verbalData.options).toEqual(['o2', 'o1', 'o3']);
          expect(spy).toHaveBeenCalledTimes(3);
        });

        describe('adjusts existing outcomes', () => {
          beforeEach(() => {
            data.addObjectiveOption(0, undefined, 'o1');
            data.addObjectiveOption(0, undefined, 'o2');
            data.addObjectiveOption(0, undefined, 'o3');

            spy.mockClear();

            data.outcomes.forEach(alternativeOutcomes => {
              alternativeOutcomes[0].values = [[[1]]];
            });
          });

          it('With index below', () => {
            data.addObjectiveOption(0, 0, 'new');

            expect(spy).toHaveBeenCalledTimes(1);

            data.outcomes.forEach(alternativeOutcomes => {
              expect(alternativeOutcomes[0].values).toEqual([[[2]]]);
            });
          });

          it('With index above', () => {
            data.addObjectiveOption(0, 2, 'new');

            expect(spy).toHaveBeenCalledTimes(1);

            data.outcomes.forEach(alternativeOutcomes => {
              expect(alternativeOutcomes[0].values).toEqual([[[1]]]);
            });
          });

          it('With null index', () => {
            data.addObjectiveOption(0, null, 'new');

            expect(spy).toHaveBeenCalledTimes(1);

            data.outcomes.forEach(alternativeOutcomes => {
              expect(alternativeOutcomes[0].values).toEqual([[[1]]]);
            });
          });

          it('With undefined index', () => {
            data.addObjectiveOption(0, undefined, 'new');

            expect(spy).toHaveBeenCalledTimes(1);

            data.outcomes.forEach(alternativeOutcomes => {
              expect(alternativeOutcomes[0].values).toEqual([[[1]]]);
            });
          });
        });
      });
    });
  });

  describe('test projects', () => {
    /**
     * Reads the test projects from decision-data-spec-test-projects.json
     * and makes sure they can be loaded and have the correct utility values.
     *
     * The code for generating the test file can be found in generate-utility-test-file.ts.
     */
    type TestProject = {
      projectName: string;
      jsonProjectString: string;
      utilityValues: number[];
    };
    // eslint-disable-next-line @typescript-eslint/no-var-requires
    const testProjects: TestProject[] = require('./decision-data-spec-test-projects.json');

    testProjects.forEach((project: any) => {
      it(`should load project "${project.projectName}"`, () => {
        expect(() => readText(project.jsonProjectString)).not.toThrow();
      });

      it(`should have unchanged utility values for project "${project.projectName}"`, () => {
        const decisionData = readText(project.jsonProjectString);

        const alternativeUtilityValues = decisionData.getAlternativeUtilities();
        const testAlternativeUtilityValues = project.utilityValues;

        expect(alternativeUtilityValues.length).toBe(testAlternativeUtilityValues.length);

        for (let i = 0; i < alternativeUtilityValues.length; i++) {
          const alternativeUtilityValue = alternativeUtilityValues[i];
          const testAlternativeUtilityValue = testAlternativeUtilityValues[i];

          expect(alternativeUtilityValue).toBeCloseTo(testAlternativeUtilityValue, 4);
        }
      });
    });
  });
});
