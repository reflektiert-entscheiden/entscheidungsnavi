import { pick } from 'lodash';
import { DecisionData } from '../decision-data';
import { PROPERTY_NAMES } from './properties';

/**
 * Exports the given decision data object into a string.
 *
 * @param data - The object to be exported
 * @param exportVersion - The software version used for this export
 * @param projectName - The name of the project to be included in the export or undefined
 */
export function dataToText(data: DecisionData, exportVersion: string, projectName?: string): string {
  projectName = projectName || undefined; // Convert null and '' to undefined
  const exportData = { exportVersion, projectName, ...pick(data, PROPERTY_NAMES) };
  return JSON.stringify(exportData);
}
