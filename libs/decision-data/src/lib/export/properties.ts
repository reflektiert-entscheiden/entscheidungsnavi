import { ObjectiveAspects, ObjectiveElement } from '../classes/objective/objective-aspects';
import { DecisionStatement } from '../classes/decision-statement';
import { DecisionQuality } from '../classes/decision-quality';
import { ProjectMode } from '../classes/project-mode';
import { Objective } from '../classes/objective/objective';
import { Weights } from '../classes/weights';
import { Alternative } from '../classes/alternative/alternative';
import { HintAlternatives } from '../classes/alternative/hint-alternatives';
import { NaviStep } from '../classes/navi-step';
import { IssueCategory } from '../classes/issue-category';
import { Outcome } from '../classes/outcome/outcome';
import { SavedValues } from '../classes/saved-values';
import { TimeRecording } from '../classes/time-recording';
import { UserDefinedInfluenceFactor } from '../classes/influence-factor/composite-influence-factor';
import { GlobalVariables } from '../classes/global-variables';

export const PROPERTY_NAMES = [
  'projectMode',
  'authorName',
  'globalVariables',
  'decisionQuality',
  'decisionStatement',
  'objectiveHierarchyMainElement',
  'objectives',
  'objectiveAspects',
  'weights',
  'alternatives',
  'hintAlternatives',
  'influenceFactors',
  'projectNotes',
  'stepExplanations',
  'issues',
  'outcomes',
  'version',
  'lastUrl',
  'resultSubstepProgress',
  'savedValues',
  'timeRecording',
  'extraData',
];

// arrays for type checks

// properties of the type Array
export const ARRAY_PROPERTY_NAMES = ['objectives', 'alternatives', 'influenceFactors', 'outcomes'];
// properties of the type Object
export const OBJECT_PROPERTY_NAMES = [
  'globalVariables',
  'decisionQuality',
  'decisionStatement',
  'hintAlternatives',
  'stepExplanations',
  'issues',
  'objectiveAspects',
  'weights',
  'objectiveHierarchyMainElement',
  'savedValues',
  'timeRecording',
  'extraData',
];
// optional properties
export const OPTIONAL_PROPERTY_NAMES = [
  'projectMode',
  'authorName',
  'decisionQuality',
  'globalVariables',
  'decisionStatement',
  'objectiveHierarchyMainElement',
  'outcomes',
  'projectNotes',
  'stepExplanations',
  'issues',
  'objectiveAspects',
  'version',
  'lastUrl',
  'resultSubstepProgress',
  'savedValues',
  'timeRecording',
  'extraData',
];

export interface CommonData {
  projectMode: ProjectMode;
  authorName: string;
  globalVariables: GlobalVariables;
  decisionQuality: DecisionQuality;
  decisionStatement: DecisionStatement;
  objectiveHierarchyMainElement: ObjectiveElement;
  objectives: Objective[];
  objectiveAspects: ObjectiveAspects;
  weights: Weights;
  alternatives: Alternative[];
  hintAlternatives: HintAlternatives;
  influenceFactors: Array<UserDefinedInfluenceFactor>;
  projectNotes: string;
  stepExplanations: Record<NaviStep, string>;
  issues: Record<IssueCategory, string[]>;
  outcomes: Outcome[][];
  version: string;
  lastUrl: string;
  resultSubstepProgress: number;
  savedValues: SavedValues;
  timeRecording: TimeRecording;
  extraData: { [key: string]: unknown };
}

export interface ExportData extends CommonData {
  exportVersion: string;
  projectName?: string;
}
