import { ensureIsRichText, isRichTextEmpty, Tree } from '@entscheidungsnavi/tools';
import { cloneDeep, defaultTo } from 'lodash';
import Delta from 'quill-delta/dist/Delta';
import { DecisionData } from '../decision-data';
import { Weights } from '../classes/weights';
import { ObjectiveAspects, ObjectiveElement } from '../classes/objective/objective-aspects';
import { CompositeUserDefinedInfluenceFactor, UserDefinedInfluenceFactor } from '../classes/influence-factor/composite-influence-factor';
import { DecisionStatement, Value } from '../classes/decision-statement';
import { Objective } from '../classes/objective/objective';
import { NumericalObjectiveData } from '../classes/objective/numerical-objective-data';
import { VerbalObjectiveData } from '../classes/objective/verbal-objective-data';
import { IndicatorObjectiveData } from '../classes/objective/indicator-objective-data';
import { VerbalObjectiveDataInitParams } from '../classes/objective/verbal-objective-data-init-params';
import { UtilityFunction } from '../classes/utility-function';
import { Indicator } from '../classes/objective/indicator';
import { Alternative } from '../classes/alternative/alternative';
import { HintAlternatives, Screw } from '../classes/alternative/hint-alternatives';
import { InfluenceFactor } from '../classes/influence-factor/influence-factor';
import { SimpleUserDefinedInfluenceFactor, UserDefinedState } from '../classes/influence-factor/simple-user-defined-influence-factor';
import { Outcome } from '../classes/outcome/outcome';
import {
  PREDEFINED_INFLUENCE_FACTOR_IDS,
  PREDEFINED_INFLUENCE_FACTORS,
  PredefinedInfluenceFactorId,
} from '../classes/influence-factor/predefined-influence-factor';
import { NotePage } from '../classes/note-page';
import { NoteGroup } from '../classes/note-group';
import { Note } from '../classes/note';
import { ObjectiveWeight } from '../classes/objective-weight';
import { NAVI_STEP_ORDER, NaviStep } from '../classes/navi-step';
import { ISSUE_CATEGORIES, IssueCategory } from '../classes/issue-category';
import { DECISION_QUALITY_CRITERIA, DecisionQuality } from '../classes/decision-quality';
import { SavedValues } from '../classes/saved-values';
import { TimeRecording } from '../classes/time-recording';
import { GlobalVariables } from '../classes/global-variables';

export function loadStatement(ds: DecisionStatement, target: DecisionData) {
  if (ds != null) {
    if (ds.notes && ds.notes.length > 4) {
      ds.notes[3] = ds.notes
        .slice(3)
        .filter(v => v)
        .join('\n');

      ds.notes = ds.notes.slice(0, 4) as [string, string, string, string];
    }

    target.decisionStatement = new DecisionStatement(
      ds.statement,
      ds.statement_attempt,
      ds.statement_attempt_2,
      ds.questions,
      ds.values.map(value => new Value(value.name, value.val, value.valueId)),
      ds.preNotes,
      ds.afterNotes,
      ds.preNotes_2,
      ds.afterNotes_2,
      ds.preNotesFinal,
      ds.afterNotesFinal,
      ds.notes,
      ds.subStepProgression,
    );
  }
}

export function loadObjectives(objectives: Objective[], target: DecisionData) {
  objectives.forEach((objectiveLike: Objective) => {
    const objective = loadObjective(objectiveLike);
    objective.indicatorData.globalVariables = target.globalVariables;
    target.objectives.push(objective);
  });
}

export function loadObjective(objectiveLike: Objective) {
  const numericalData = loadNumericalObjectiveData(objectiveLike.numericalData);
  const verbalData = loadVerbalObjectiveData(objectiveLike.verbalData);
  const indicatorData = loadIndicatorObjectiveData(objectiveLike.indicatorData);

  // Put together the objective object
  return new Objective(
    objectiveLike.name,
    numericalData,
    verbalData,
    indicatorData,
    objectiveLike.objectiveType,
    objectiveLike.displayed,
    objectiveLike.comment,
    objectiveLike.scaleComment,
    objectiveLike.aspects ? Tree.from(objectiveLike.aspects).map(loadObjectiveElement) : undefined,
    objectiveLike.placeholder,
    objectiveLike.uuid,
  );
}

function loadObjectiveElement(objectiveElementLike: ObjectiveElement) {
  return new ObjectiveElement(
    objectiveElementLike.name,
    objectiveElementLike.createdInSubStep,
    objectiveElementLike.backgroundColor,
    objectiveElementLike.textColor,
    objectiveElementLike.placeholder,
    objectiveElementLike.isAspect,
    objectiveElementLike.orderIdx,
    objectiveElementLike.comment,
  );
}

function loadNumericalObjectiveData(nu: NumericalObjectiveData): NumericalObjectiveData {
  return new NumericalObjectiveData(nu.from, nu.to, nu.unit, loadUtilityFunction(nu.utilityfunction), nu.commentFrom, nu.commentTo);
}

function loadVerbalObjectiveData(ve: VerbalObjectiveData): VerbalObjectiveData {
  const initParamsObj = ve.initParams
    ? new VerbalObjectiveDataInitParams(ve.initParams.stepNumber, ve.initParams.from, ve.initParams.to)
    : new VerbalObjectiveDataInitParams();

  return new VerbalObjectiveData(
    ve.options,
    ve.utilities,
    ve.precision,
    ve.comments,
    initParamsObj,
    ve.c,
    ve.hasCustomUtilityValues,
    ve.utilityFunctionExplanation,
  );
}

function loadIndicatorObjectiveData(ind: IndicatorObjectiveData): IndicatorObjectiveData {
  return new IndicatorObjectiveData(
    loadIndicators(ind.indicators),
    loadUtilityFunction(ind.utilityfunction),
    defaultTo(ind.useCustomAggregation, false),
    ind.customAggregationFormula,
    defaultTo(ind.defaultAggregationWorst, 0),
    defaultTo(ind.defaultAggregationBest, 100),
    defaultTo(ind.aggregatedUnit, '%'),
    ind.stages,
    ind.automaticCustomAggregationLimits,
  );
}

function loadUtilityFunction(uf: UtilityFunction): UtilityFunction {
  return new UtilityFunction(uf.c, uf.precision, uf.width, uf.level, uf.explanation);
}

// copy the indicators
function loadIndicators(indicators: Indicator[]): Indicator[] {
  return indicators.map(i => {
    let { min, max, coefficient } = i;

    // In newer versions, we enforce that coefficients are non-negative. If we import an indicator with a negative
    // coefficient, we flip the min/max bounds and take the absolute of the coefficient to achieve the same effect.
    if (coefficient < 0) {
      [min, max] = [max, min];
      coefficient *= -1;
    }

    // Make sure min !== max
    if (min === max) {
      max += 1;
    }

    return new Indicator(i.name, min, max, i.unit, coefficient, i.comment, i.verbalIndicatorCategories);
  });
}

export function loadAlternatives(alternatives: Alternative[], target: DecisionData) {
  alternatives.forEach((a: Alternative) => {
    // Convert old plain text comments into quill json
    if (a.comment) {
      a.comment = ensureIsRichText(a.comment);
    }

    target.alternatives.push(Alternative.clone(a));
  });
}

export function loadHintAlternatives(ha: HintAlternatives, target: DecisionData) {
  if (ha.subStepProgression) {
    target.hintAlternatives.subStepProgression = ha.subStepProgression;
  }

  if (ha.screws) {
    target.hintAlternatives.screws = ha.screws.map(screw => {
      if (typeof screw === 'string') {
        return new Screw(screw);
      }
      return new Screw(screw.name, screw.uuid, screw.states, screw.comment);
    });
  }

  if (ha.ideas) {
    target.hintAlternatives.ideas = loadNotePage(ha.ideas);
  }
}

export function loadInfluenceFactors(ifs: Array<UserDefinedInfluenceFactor>, target: DecisionData) {
  target.influenceFactors = [];

  ifs.forEach(e => {
    if ('crossImpacts' in e) {
      const firstBaseFactor = target.influenceFactors.find(base => base.id === (e as any)['baseFactorIds'][0]),
        secondBaseFactor = target.influenceFactors.find(base => base.id === (e as any)['baseFactorIds'][1]);

      if (firstBaseFactor == null || secondBaseFactor == null) {
        throw new Error('Combined influence factor could not be imported');
      }

      target.influenceFactors.push(
        new CompositeUserDefinedInfluenceFactor(
          e.id,
          e.customName,
          [firstBaseFactor, secondBaseFactor],
          cloneDeep(e.crossImpacts),
          e.comment,
          e.uuid,
        ),
      );
    } else {
      target.influenceFactors[e.id] = new SimpleUserDefinedInfluenceFactor(
        e.name,
        (e.states ?? (e as any)['zustaende']).map(
          (zustand: UserDefinedState) => new UserDefinedState(zustand.name, zustand.probability, zustand.comment),
        ),
        e.id,
        e.precision ?? (e as any)['praezision'],
        ensureIsRichText(e.comment),
        e.uuid,
      );
    }
  });
}

export function loadOutcomes(outcomesData: Outcome[][], target: DecisionData) {
  if (!outcomesData) {
    return;
  }

  const outcomes: Outcome[][] = outcomesData.map((e: any[]) =>
    e.map((a: any, objectiveIdx: number) => {
      let uf: InfluenceFactor;

      // (very) old export version (whole copy of the influence factor object)
      if (a.unsicherheitsfaktor != null) {
        const filteredUf: Array<UserDefinedInfluenceFactor> = target.influenceFactors.filter(u => +u.id === +a.unsicherheitsfaktor.id);
        if (filteredUf.length === 1) {
          uf = filteredUf[0];
        } else {
          console.log(`Could not find UF ${a.unsicherheitsfaktor.id}`);
        }
      }

      // Current version saves the ID
      const ufId = a.influenceFactorId ?? a.unsicherheitsfaktor_id;
      if (ufId != null) {
        /* Get InfluenceFactor from either the hardcoded array or Decision Data. */
        // check if influenceFactorId is a string (predefined IF) or a number (user defined IF)
        if (typeof ufId === 'number') {
          /* Outcome uses a user defined IF. */
          const filteredUf: Array<UserDefinedInfluenceFactor> = target.influenceFactors.filter(u => +u.id === +ufId);
          if (filteredUf.length === 1) {
            uf = filteredUf[0];
          } else {
            console.log(`Could not find UF ${ufId}`);
          }
        } else if (PREDEFINED_INFLUENCE_FACTOR_IDS.includes(ufId)) {
          /* Outcome uses a predefined IF. */
          uf = PREDEFINED_INFLUENCE_FACTORS[ufId as PredefinedInfluenceFactorId];
        }
      }

      // Convert old plain text comments into quill json
      if (a.comment) {
        a.comment = ensureIsRichText(a.comment);
      }

      return new Outcome(a.values, target.objectives[objectiveIdx], uf, a.processed, a.comment, a.uuid);
    }),
  );

  // insert missing fields
  target.outcomes = target.alternatives.map((_a, i) => {
    const row: Outcome[] = i < outcomes.length ? outcomes[i] : [];
    return target.objectives.map((objective, j) => (j < row.length ? row[j] : new Outcome(null, objective)));
  });
}

export function loadObjectiveAspects(objectiveAspects: ObjectiveAspects, target: DecisionData) {
  if (objectiveAspects) {
    target.objectiveAspects = new ObjectiveAspects(
      target,
      objectiveAspects.subStepProgression,
      objectiveAspects.listOfAspects?.map(loadObjectiveElement),
      objectiveAspects.listOfDeletedAspects?.map(loadObjectiveElement),
    );
  }
}

export function loadWeights(weights: Weights, target: DecisionData) {
  target.weights.tradeoffObjectiveIdx = weights.tradeoffObjectiveIdx;

  weights.preliminaryWeights.forEach(weight => {
    target.weights.preliminaryWeights.push(loadWeight(weight));
  });

  if (weights.tradeoffWeights) {
    weights.tradeoffWeights.forEach(weight => {
      if (weight === null) {
        target.weights.tradeoffWeights.push(null);
      } else {
        target.weights.tradeoffWeights.push(loadWeight(weight));
      }
    });
  } else {
    target.objectives.forEach(() => target.weights.tradeoffWeights.push(null));
  }

  if (weights.unverifiedWeights) {
    weights.unverifiedWeights.forEach(weight => target.weights.unverifiedWeights.push(weight));
  } else {
    target.weights.unverifiedWeights.length = target.weights.preliminaryWeights.length;
    target.weights.unverifiedWeights.fill(false);
  }

  if (weights.manualTradeoffs) {
    target.weights.manualTradeoffs = weights.manualTradeoffs;
  } else {
    target.weights.manualTradeoffs = target.objectives.map(() => null);
  }

  target.weights.explanations = weights.explanations ?? target.weights.preliminaryWeights.map(() => '');
}

function loadWeight(weight: number | ObjectiveWeight): ObjectiveWeight {
  if (typeof weight === 'number') {
    return new ObjectiveWeight(weight);
  } else {
    return new ObjectiveWeight(
      weight.value,
      weight.precision ?? (weight as any)['praezision'],
      weight.comparisonPointX,
      weight.activeReferencePointIndex,
    );
  }
}

export function loadNotePage(input: NotePage): NotePage {
  const noteGroups: NoteGroup[] = input.noteGroups ?? (input as any)['notizGroups'];
  return new NotePage(
    noteGroups.map(group => {
      const notes: Note[] = group.notes ?? (group as any)['notizen'];
      return new NoteGroup(
        group.id,
        notes.map(entry => new Note(entry.id, entry.name)),
        group.name,
      );
    }),
  );
}

export function loadStepExplanations(explanations: Record<NaviStep, string>, target: DecisionData) {
  // Very old projects had no explanations associated
  if (explanations == null) {
    for (const step of NAVI_STEP_ORDER) {
      target.stepExplanations[step] = '';
    }
    return;
  }

  // Old explanations from steps objective weighting and utility function should be
  // moved to the results explanation
  if ((explanations as any)['utilityFunctions'] != null && (explanations as any)['weightsOfObjectives'] != null) {
    let delta = loadRichText(explanations.results);

    if (!isRichTextEmpty((explanations as any)['utilityFunctions'])) {
      delta = loadRichText((explanations as any)['utilityFunctions'])
        .insert('\n')
        .concat(delta);
    }
    if (!isRichTextEmpty((explanations as any)['weightsOfObjectives'])) {
      delta = loadRichText((explanations as any)['weightsOfObjectives'])
        .insert('\n')
        .concat(delta);
    }

    explanations.results = JSON.stringify(delta);
  }

  // Copy explanations over
  for (const step of NAVI_STEP_ORDER) {
    target.stepExplanations[step] = explanations[step];
  }
}

export function loadIssues(issues: Record<IssueCategory, string[]>, target: DecisionData) {
  if (issues == null) {
    return;
  }
  for (const category of ISSUE_CATEGORIES) {
    target.issues[category] = issues[category].slice();
  }
}

export function loadDecisionQuality(decisionQuality: DecisionQuality, target: DecisionData) {
  for (const criteria of DECISION_QUALITY_CRITERIA) {
    if (decisionQuality?.criteriaValues[criteria] != null) {
      target.decisionQuality.criteriaValues[criteria] = decisionQuality.criteriaValues[criteria];
    }
  }
}

export function loadSavedValues(savedValues: SavedValues, target: DecisionData) {
  if (savedValues == null || savedValues.projectDescriptionSelectedIndex < 0) {
    target.savedValues.projectDescriptionSelectedIndex = isRichTextEmpty(target.projectNotes) ? 3 : 0;
  } else {
    target.savedValues.projectDescriptionSelectedIndex = savedValues.projectDescriptionSelectedIndex;
  }
}

export function loadTimeRecording(timeRecording: TimeRecording, target: DecisionData) {
  if (timeRecording != null) {
    target.timeRecording.timers = timeRecording.timers;
  } else {
    target.timeRecording = new TimeRecording();
  }
}

export function loadGlobalVariables(globalVariables: GlobalVariables, target: DecisionData) {
  if (globalVariables == null) return;

  target.globalVariables = new GlobalVariables(new Map(globalVariables.map));
}

/**
 * Tries to parse the input as Quill Delta. If that is not possible, falls back to plain text.
 */
function loadRichText(input: string): Delta {
  try {
    return new Delta(JSON.parse(input));
  } catch {
    // Could not parse JSON, assume it is plaintext
    const out = new Delta();
    if (input.length > 0) {
      out.insert(input);
    }
    return out;
  }
}
