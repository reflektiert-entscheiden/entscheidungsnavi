import { flatten } from 'lodash';
import semverGt from 'semver/functions/gt';
import { ObjectiveType } from '../../classes/objective/objective';

export function migrateOutcomesV75(data: any) {
  migrateOutcomeValuesForVerbalObjectives(data);
  migrateObjectiveInputs(data);
}

function migrateOutcomeValuesForVerbalObjectives(data: any) {
  try {
    if (semverGt(data.exportVersion, '7.5.0')) return;
  } catch (e: any) {}

  // In previous versions, the outcome-values for verbal objectives
  // were saved in string form (e.g. "2" instead of 2) which is of
  // course not adhering to the schema. Therefore, we need to parse
  // all (string) values to numbers.
  data.outcomes.forEach((e: any[], alternativeIdx: number) => {
    e.forEach((a: any, objectiveIdx: number) => {
      const newValues: (number | number[])[] = [];
      a.values.forEach((value: any) => {
        // null/undefined values are preserved
        let newVal: number | number[];
        if (Array.isArray(value)) {
          newVal = value.map(entry => (entry ? Number(entry) : entry));
          if (newVal.some(entry => isNaN(entry))) {
            throw new Error(`Failed to parse outcome-value to Array of numbers: ${value}`);
          }
        } else {
          // If we have a number or string (or any other datatype)
          // we try to parse it into a number.
          newVal = value ? Number(value) : value;
          if (isNaN(newVal as number)) {
            throw new Error(`Failed to parse outcome-value to Number: ${value}`);
          }
        }
        newValues.push(newVal);
      });
      data.outcomes[alternativeIdx][objectiveIdx].values = newValues;
    });
  });
}

function migrateObjectiveInputs(data: any) {
  const usesOldObjectiveInput = flatten(data.outcomes).some((o: any) => {
    // check if values are a NOT 3D array
    return (
      o.values != null && !o.values.every((item: any) => Array.isArray(item) && item.every((innerItem: any) => Array.isArray(innerItem)))
    );
  });
  if (!usesOldObjectiveInput) return;

  // convert old ObjectiveInput into the new number[][][]s
  data.outcomes.forEach((outcomesRow: any[], alternativeIdx: number) => {
    outcomesRow.forEach((outcome: any, objectiveIdx) => {
      // encapsulate innermost values in extra array brackets
      // thus converting ObjectiveInput -> number[][][]
      if (data.objectives[objectiveIdx].objectiveType === ObjectiveType.Indicator) {
        data.outcomes[alternativeIdx][objectiveIdx].values = outcome.values.map((stateValues: any) => {
          if (!Array.isArray(stateValues)) {
            return data.objectives[objectiveIdx].indicatorData.indicators.map(() => [undefined as number]);
          }
          return stateValues.map((v: any) => [v]);
        });
      } else {
        data.outcomes[alternativeIdx][objectiveIdx].values = outcome.values.map((v: any) => [[v]]);
      }
    });
  });
}
