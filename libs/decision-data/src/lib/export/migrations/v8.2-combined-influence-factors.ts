/**
 * Up until version 8.2, scenarios (composite influence factors) had a customizable cross impact matrix
 * that could include values between 1 and 5, where 3 was the default (uncorrelated) value.
 * This function converts all cross impact matrices to a default value of 3
 * (it is intended as a temporal solution before the scenario system is reworked).
 * @param data - DecisionData object to be migrated
 */
export function resetAllCrossImpacts(data: any) {
  if (data.influenceFactors) {
    for (const factor of data.influenceFactors) {
      if (factor.crossImpacts != null) {
        factor.crossImpacts = Array.from({ length: countStates(data.influenceFactors[factor.baseFactorIds[0]]) }, () =>
          Array.from({ length: countStates(data.influenceFactors[factor.baseFactorIds[1]]) }, () => 3),
        );
      }
    }
  }

  function countStates(influenceFactor: any): number {
    if (influenceFactor.states != null) {
      return influenceFactor.states.length;
    } else {
      return (
        countStates(data.influenceFactors[influenceFactor.baseFactorIds[0]]) *
        countStates(data.influenceFactors[influenceFactor.baseFactorIds[1]])
      );
    }
  }
}
