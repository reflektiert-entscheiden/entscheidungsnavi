import { isEqual } from 'lodash';
import { Tree } from '@entscheidungsnavi/tools';
import { loadNotePage } from '../import-loaders';
import { ObjectiveElement, OBJECTIVES_STEPS } from '../../classes/objective/objective-aspects';
import { Objective } from '../../classes/objective/objective';
import { NotePage } from '../../classes/note-page';
import { Alternative } from '../../classes/alternative/alternative';
import { ALTERNATIVES_STEPS } from '../../classes/alternative/hint-alternatives';
import { DECISION_STATEMENT_STEPS, DecisionStatement } from '../../classes/decision-statement';

/**
 * Up until version 6.4, every substep of objectives and alternatives had its own dataset, i.e., separate sets of
 * objectives and alternatives. In newer versions, there is only a single dataset across substeps.
 *
 * @param data - The DecisionData to be migrated
 */
export function migrateToCommonSubstepDataV64(data: any) {
  migrateObjectiveAspectsV64(data);
  migrateObjectiveAspectsV2(data);
  calculateProgressionV64(data); // Calculate progress before updating the actual objectives and alternatives
  migrateAlternativesSubstepsV64(data);
  loadObjectivesFromSubstepTreesV64(data);
}

/**
 * Up until version 6.4, the objectives in every substep were separate. If the Result substep has not been reached,
 * we need to copy the objectives from the last completed substep into the results.
 * @param data - DecisionData object to be migrated
 */
function loadObjectivesFromSubstepTreesV64(data: any) {
  const nonEmptyTrees: Tree<ObjectiveElement>[] = data.objectiveAspects?.aspectTrees?.filter((t: any) => t);
  // in case "Ergebnis" hasn't been reached, but there are existing trees in substeps 4 or 5
  if (data.objectives?.length === 0 && nonEmptyTrees?.length > 0) {
    const latestTree = nonEmptyTrees[nonEmptyTrees.length - 1];
    for (const child of latestTree.children) {
      const newObjective = new Objective(child.value.name);
      newObjective.aspects = new Tree<ObjectiveElement>(new ObjectiveElement(newObjective.name), child.children);
      data.objectives.push(newObjective);
    }
  }
}

/**
 * Up until version 6.4, the aspects for each substep were separate and saved in 'objectiveAspects.aspectLists'.
 * @param data - DecisionData object to be migrated
 */
function migrateObjectiveAspectsV64(data: any) {
  if (!data.objectiveAspects) {
    data.objectiveAspects = {};
  }

  if (data.objectiveAspects?.aspectLists) {
    loadObjectiveAspectLists(data, data.objectiveAspects.aspectLists);
  }
}

/**
 * Up until version 2.?, the objective aspects were saved under 'notizen' in the root of DecisionData.
 * @param data - DecisionData object to be migrated
 */
function migrateObjectiveAspectsV2(data: any) {
  if (data.notizen) {
    type Entry = [string, NotePage];
    // copy data to instances of the appropriate classes
    let pageArray: Entry[] = data.notizen
      .map((entry: Entry): Entry => [entry[0], loadNotePage(entry[1])])
      .sort((a: Entry, b: Entry) => a[0].localeCompare(b[0]));

    // The objective substeps/hints had those specific keys in this old version
    const objHintKeyList = ['zh1', 'zh2', 'zh3', 'zh4', 'zh5'];
    // index of step "Identifizieren Sie Ihre Fundamentalziele" in objHintKeyList
    const objFundamentalStartIdx = 3;

    pageArray = pageArray.map((entry: Entry): Entry => {
      if (objHintKeyList.indexOf(entry[0]) < objFundamentalStartIdx) {
        // flatten groups from older versions, since the first steps allow only one note per group
        return [entry[0], entry[1].flattenGroups()];
      } else if (entry[0] === objHintKeyList[objHintKeyList.length - 1] && entry[0] !== pageArray[pageArray.length - 1][0]) {
        // use the groups from the last step for the new last step, if the step count has changed
        const page6 = pageArray.find((e: Entry) => e[0] === 'zh6');
        if (page6) {
          // try to use page 'zh6' explicitly, since some old projects still have an obsolete page 'zh7'
          return [entry[0], page6[1]];
        } else {
          return [entry[0], pageArray[pageArray.length - 1][1]];
        }
      } else if (entry[1].noteGroups.length > 1 && entry[1].noteGroups[1].name === 'Ihre Fundamentalziele') {
        // flatten the second group from versions < 3.2
        return [entry[0], entry[1].flattenSecondGroup()];
      } else {
        return entry;
      }
    });
    // get rid of obsolete pages (:= with a key that is not in objHintKeyList)
    pageArray = pageArray.filter((entry: Entry) => objHintKeyList.indexOf(entry[0]) !== -1);
    // convert old aspect map to simple array
    transformObjectiveAspectsV2(new Map<string, NotePage>(pageArray), data);
  }
}

function transformObjectiveAspectsV2(aspectMap: Map<string, NotePage>, data: any) {
  const singlePages = ['zh1', 'zh2', 'zh3'];
  const multiPages = ['zh4', 'zh5', 'zh6'];

  const lists: string[][] = [];
  const trees: Tree<ObjectiveElement>[] = [];

  aspectMap.forEach((np, key) => {
    if (singlePages.includes(key)) {
      const stepAspects: string[] = [];
      np.noteGroups.forEach(group => {
        if (group.notes[0]) {
          stepAspects.push(group.notes[0].name);
        }
      });
      lists[singlePages.indexOf(key) + 1] = stepAspects;
    } else if (multiPages.includes(key)) {
      if (key === 'zh4') {
        np.removeNoteGroup(0);
      }

      const objectiveTrees: Tree<ObjectiveElement>[] = [];

      np.noteGroups.forEach(group => {
        const objectiveName = group.name;
        const childAspects: ObjectiveElement[] = [];
        group.notes.forEach(n => childAspects.push(new ObjectiveElement(n.name)));

        const tree = new Tree(
          new ObjectiveElement(objectiveName),
          childAspects.map(ca => new Tree(ca)),
        );

        objectiveTrees.push(tree);
      });
      trees[multiPages.indexOf(key) + 4] = new Tree(new ObjectiveElement('Zielhierarchie'), objectiveTrees);
    } else {
      throw Error('Unknown note key: ' + key);
    }
  });

  loadObjectiveAspectLists(data, lists);
}

/**
 * Up until version 6.4, objective aspects were separate lists for each substep. This function
 * merges them.
 * @param data - DecisionData object to be migrated
 */
function loadObjectiveAspectLists(data: any, oldAspectLists: string[][]) {
  const existingAspectNames = [];
  [...data.objectives.map((o: Objective) => o.aspects)]
    .filter(t => t)
    .forEach(tree => {
      const toCheck = [tree];

      while (toCheck.length > 0) {
        const next = toCheck.pop();
        if (oldAspectLists && next.value != null) {
          next.value.createdInSubStep = calculateAspectStepNumber(next.value.name, oldAspectLists);
          existingAspectNames.push(next.value.name);
        }

        // Fix Trees with elements without Children defined
        if (next.children === undefined) {
          next.children = [];
        }

        // Fix Trees with old elements that don't have separate background / text color
        const anyValue = next.value as any;
        if (anyValue && anyValue.color) {
          next.value.backgroundColor = anyValue.color;
          anyValue.color = undefined;
        }

        toCheck.push(...next.children);
      }
    });

  if (data.objectiveAspects.aspectTrees != null) {
    for (let i = 0; i < data.objectiveAspects.aspectTrees.length; i++) {
      if (data.objectiveAspects.aspectTrees[i]) {
        const tempAspectList = [];
        const toCheck = [data.objectiveAspects.aspectTrees[i]];

        while (toCheck.length > 0) {
          const next = toCheck.pop();
          if (oldAspectLists) {
            next.value.createdInSubStep = calculateAspectStepNumber(next.value.name, oldAspectLists);
            if (data.objectiveAspects.aspectTrees[i + 1] == null && data.objectives.length === 0) {
              // take existing aspects from last tree
              existingAspectNames.push(next.value.name);
            }
          }

          // Fix Trees with elements without Children defined
          if (next.children === undefined) {
            next.children = [];
          }

          toCheck.push(...next.children);

          tempAspectList.push(next.value.name);
        }
        tempAspectList.shift(); // remove 'Zielhierarchie'
        oldAspectLists.splice(oldAspectLists.length, 0, tempAspectList);
      }
    }
  }

  // if there are any aspects at all
  if (oldAspectLists?.length > 1) {
    const listOfAspects = [];
    // in older versions there are no aspects in the Brainstorming box
    // this fixes a bug where aspects appear twice (in the Hierarchy and in the Brainstorming box)
    if (data.notizen == null) {
      // use the latest oldAspectList as the current listOfAspects ([4] is the Brainstorming box)
      const latestAspectList = oldAspectLists[Math.min(oldAspectLists.length - 1, 4)];

      if (latestAspectList) {
        for (const oldAspectName of latestAspectList) {
          const newAspect = new ObjectiveElement(oldAspectName);
          newAspect.createdInSubStep = calculateAspectStepNumber(oldAspectName, oldAspectLists);
          listOfAspects.push(newAspect);
          existingAspectNames.push(oldAspectName);
        }
      }
    }
    data.objectiveAspects.listOfAspects = listOfAspects;

    findDeletedAspects(data, existingAspectNames, oldAspectLists);
  }
}

function findDeletedAspects(data: any, existingAspectNames: string[], oldAspectLists: string[][]) {
  const allAspects: string[] = [].concat(...oldAspectLists); // concat all oldAspectLists
  const allUniqueAspects: string[] = [...new Set(allAspects)].filter(a => a); // remove duplicates and nulls

  const missingAspects = allUniqueAspects.filter(a => !existingAspectNames.includes(a));

  const listOfDeletedAspects = [];
  for (const aspectName of missingAspects) {
    const newAspect = new ObjectiveElement(aspectName);
    newAspect.createdInSubStep = calculateAspectStepNumber(aspectName, oldAspectLists);
    listOfDeletedAspects.push(newAspect);
  }
  data.objectiveAspects.listOfDeletedAspects = listOfDeletedAspects;
}

function calculateAspectStepNumber(name: string, aspectLists: string[][]) {
  // aspectLists[0] is (always?) null
  // aspectlists[1-3] are the aspects in the steps 1 to 3
  // aspectlists[4] are the aspects that are left in the Brainstorming box
  // aspectlists[5-6] are artificially created from the trees in substeps 4 and 5
  for (let i = 1; i < Math.min(aspectLists.length, 4); i++) {
    if (aspectLists[i]?.includes(name)) {
      return i;
    }
  }
  return 0;
}

/**
 * Up until version 6.4, there were different sets of alternatives for each substep and the main step.
 * @param data - DecisionData object to be migrated
 */
function migrateAlternativesSubstepsV64(data: any) {
  if (!data.hintAlternatives.alternatives || data.hintAlternatives.alternatives.length === 0) {
    return;
  }

  if (data.alternatives.length === 0) {
    const nonEmptyHintAlternativeLists = data.hintAlternatives.alternatives.filter((a: Alternative) => a);
    const lastHintAlternativeList = nonEmptyHintAlternativeLists[nonEmptyHintAlternativeLists.length - 1];
    for (let i = 0; i < lastHintAlternativeList.length; i++) {
      const createdInSubStep = calculateAlternativeStepNumber(lastHintAlternativeList[i].name, data.hintAlternatives.alternatives);
      for (let j = 0; j < lastHintAlternativeList[i].children.length; j++) {
        const createdInSubStepChild = calculateAlternativeStepNumber(
          lastHintAlternativeList[i].children[j].name,
          data.hintAlternatives.alternatives,
        );
        lastHintAlternativeList[i].children[j].createdInSubStep = createdInSubStepChild;
      }
      data.alternatives.push(
        new Alternative(
          createdInSubStep,
          lastHintAlternativeList[i].name,
          lastHintAlternativeList[i].comment,
          lastHintAlternativeList[i].children,
        ),
      );
    }
  }

  for (let i = 0; i < data.alternatives.length; i++) {
    if (data.alternatives[i].children != null) {
      for (let j = 0; j < data.alternatives[i].children.length; j++) {
        const createdInSubStepChild = calculateAlternativeStepNumber(
          data.alternatives[i].children[j].name,
          data.hintAlternatives.alternatives,
        );
        data.alternatives[i].children[j].createdInSubStep = createdInSubStepChild;
      }
    }
    data.alternatives[i].createdInSubStep = calculateAlternativeStepNumber(data.alternatives[i].name, data.hintAlternatives.alternatives);
  }
}

function calculateAlternativeStepNumber(name: string, alternativeLists: Alternative[][]) {
  for (let i = 0; i < alternativeLists.length; i++) {
    if (alternativeLists[i] == null) {
      // (very) old projects might have undefined hint_alternatives
      continue;
    }
    for (const alternative of alternativeLists[i]) {
      if (alternative.name === name) {
        return i + 1;
      }
    }
  }
  return 0;
}

/**
 * Up until version 6.4, the progress through the substeps was calculated from the data for each substep.
 * @param data - DecisionData object to be migrated
 */
function calculateProgressionV64(data: any) {
  // We know that hintAlternatives and objectiveAspects are not empty at this point
  if (data.decisionStatement?.subStepProgression || data.objectiveAspects.subStepProgression || data.hintAlternatives.subStepProgression) {
    return;
  }

  // step 3
  if (data.alternatives?.length > 0) {
    data.hintAlternatives.subStepProgression = ALTERNATIVES_STEPS[ALTERNATIVES_STEPS.length - 1];
    return;
  }

  if (data.hintAlternatives.alternatives?.filter((a: any) => a).length > 0) {
    data.hintAlternatives.subStepProgression = ALTERNATIVES_STEPS[data.hintAlternatives.alternatives.filter((a: any) => a).length - 1];
    return;
  }

  // step 2
  if (data.objectives?.length > 0) {
    data.objectiveAspects.subStepProgression = OBJECTIVES_STEPS[OBJECTIVES_STEPS.length - 1];
    return;
  }

  if (data.objectiveAspects.aspectTrees?.filter((t: any) => t).length > 0) {
    const nonEmptyTreeCount = data.objectiveAspects.aspectTrees.filter((t: any) => t).length;
    data.objectiveAspects.subStepProgression = OBJECTIVES_STEPS[2 + nonEmptyTreeCount]; // 3 or 4
    return;
  }

  if (data.objectiveAspects.aspectLists?.length > 1) {
    data.objectiveAspects.subStepProgression = OBJECTIVES_STEPS[data.objectiveAspects?.aspectLists.length - 2]; // 3 or 4
    return;
  }

  // step 1
  if (data.decisionStatement?.statement != null) {
    data.decisionStatement.subStepProgression = DECISION_STATEMENT_STEPS[DECISION_STATEMENT_STEPS.length - 1];
    return;
  }
  if (data.decisionStatement?.statement_attempt_2 != null) {
    data.decisionStatement.subStepProgression = DECISION_STATEMENT_STEPS[3];
    return;
  }
  if (!isEqual(data.decisionStatement?.notes, DecisionStatement.defaultNotes)) {
    data.decisionStatement.subStepProgression = DECISION_STATEMENT_STEPS[2];
    return;
  }
  if (!isEqual(data.decisionStatement?.values, { private: [], politics: [], company: [] })) {
    data.decisionStatement.subStepProgression = DECISION_STATEMENT_STEPS[1];
    return;
  }
}
