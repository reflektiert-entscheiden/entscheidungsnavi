// Up until V7.4, decision statement values were saved for every context. After that, contexts were
// removed.
export function migrateDecisionStatementValuesV75(data: any) {
  const values = data.decisionStatement?.values;

  if (values && typeof values === 'object' && data.context && data.context in values) {
    // We set the values by context
    data.decisionStatement.values = data.decisionStatement.values[data.context];
  }
}
