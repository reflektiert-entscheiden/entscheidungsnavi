import { ensureIsRichText, Tree } from '@entscheidungsnavi/tools';
import { ObjectiveElement } from '../../classes/objective/objective-aspects';
import { ObjectiveType } from '../../classes/objective/objective';
import { IndicatorObjectiveData } from '../../classes/objective/indicator-objective-data';
import { UtilityFunction } from '../../classes/utility-function';

/**
 * Up until version 7.5, migrating german properties in objectives was handles in the import loaders and not the compatibility layer.
 *
 * @param data - The imported DecisionData object to be migrated
 */
export function migrateGermanObjectives(data: any) {
  data.objectives.forEach((objective: any) => migrateObjective(objective));
}

function migrateObjective(objective: any) {
  // Older version of objectives which only allowed numerical and verbal objectives.
  if (objective.isNumerisch != null) {
    objective.objectiveType = objective.isNumerisch ? ObjectiveType.Numerical : ObjectiveType.Verbal;
  }

  // Transform string[] aspects into tree
  if (objective.aspects && Array.isArray(objective.aspects)) {
    const stringAspects = <string[]>objective.aspects;
    objective.aspects = new Tree<ObjectiveElement>(
      new ObjectiveElement(objective.name),
      stringAspects.map(aspect => {
        return new Tree<ObjectiveElement>(new ObjectiveElement(aspect));
      }),
    );
  }

  // Convert plain text comments into quill json
  if (objective.comment) {
    objective.comment = ensureIsRichText(objective.comment);
  }

  if (objective.zielNumerisch) {
    objective.numericalData = objective.zielNumerisch;
  }
  migrateNumericalData(objective.numericalData);

  if (objective.zielVerbal) {
    objective.verbalData = objective.zielVerbal;
  }
  migrateVerbalData(objective.verbalData);

  if (objective.zielIndicator) {
    objective.indicatorData = objective.zielIndicator;
  }
  if (objective.indicatorData) {
    migrateIndicatorData(objective.indicatorData);
  } else {
    objective.indicatorData = new IndicatorObjectiveData();
  }
}

function migrateNumericalData(numericalData: any) {
  if ('nutzenFunktion' in numericalData && typeof numericalData.nutzenFunktion === 'number') {
    // Very old formats only saved the c parameter, there was no UtilityFunction class
    numericalData.utilityfunction = new UtilityFunction(numericalData.nutzenFunktion);
  } else {
    if (numericalData.nutzenfunktion) {
      numericalData.utilityfunction = numericalData.nutzenfunktion;
    }

    migrateUtilityFunction(numericalData.utilityfunction);
  }

  if (numericalData.von != null) numericalData.from = numericalData.von;
  if (numericalData.bis != null) numericalData.to = numericalData.bis;
  if (numericalData.einheit != null) numericalData.unit = numericalData.einheit;
  if (numericalData.comment_von != null) numericalData.commentFrom = numericalData.comment_von;
  if (numericalData.comment_bis != null) numericalData.commentTo = numericalData.comment_bis;
  if (numericalData.explanation_von != null) numericalData.explanationFrom = numericalData.explanation_von;
  if (numericalData.explanation_bis != null) numericalData.explanationTo = numericalData.explanation_bis;
}

function migrateVerbalData(verbalData: any) {
  if (verbalData.zielVerbalOptionInitParams) {
    verbalData.initParams = verbalData.zielVerbalOptionInitParams;
  }
  if (verbalData.initParams?.stufen != null) {
    verbalData.initParams.stepNumber = verbalData.initParams.stufen;
  }
  if (verbalData.initParams?.von != null) {
    verbalData.initParams.from = verbalData.initParams.von;
  }
  if (verbalData.initParams?.bis != null) {
    verbalData.initParams.to = verbalData.initParams.bis;
  }

  if (verbalData.optionen) verbalData.options = verbalData.optionen;
  if (!verbalData.comments) verbalData.comments = [];
  if (!verbalData.utilities) {
    verbalData.utilities = verbalData.nutzen ?? [];
  }

  // In previous versions, utilities, comments, and explanations arrays may have been empty.
  if (verbalData.utilities.length === 0)
    verbalData.utilities = verbalData.options.map((_: unknown, index: number) => (index / (verbalData.options.length - 1)) * 100);
  while (verbalData.comments.length < verbalData.options.length) verbalData.comments.push('');

  if (verbalData.praezision != null) verbalData.precision = verbalData.praezision;

  // Old verbal objectives always had custom utility values
  if (verbalData.hasCustomUtilityValues == null) verbalData.hasCustomUtilityValues = true;
}

function migrateIndicatorData(indicatorData: any) {
  if (indicatorData.utilityfunction) {
    migrateUtilityFunction(indicatorData.utilityfunction);
  } else {
    indicatorData.utilityfunction = new UtilityFunction();
  }
}

function migrateUtilityFunction(utilityFunction: any) {
  if (utilityFunction.praezisiongrad != null) utilityFunction.precision = utilityFunction.praezisiongrad;
  if (utilityFunction.breite != null) utilityFunction.width = utilityFunction.breite;
  if (utilityFunction.niveau != null) utilityFunction.level = utilityFunction.niveau;
}
