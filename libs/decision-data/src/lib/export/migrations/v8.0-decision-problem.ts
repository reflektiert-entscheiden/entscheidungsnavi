export function migrateDecisionProblemV80(data: any) {
  if (data.decisionProblem && !data.projectName) {
    data.projectName = data.decisionProblem;
    delete data.decisionProblem;
  }
}
