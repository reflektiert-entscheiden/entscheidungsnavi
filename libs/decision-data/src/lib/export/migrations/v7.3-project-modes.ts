// Project modes were renamed in V7.3
export function migrateNewProjectModeNamesV73(data: any) {
  if (data.projectMode === 'simple') {
    data.projectMode = 'starter';
  } else if (data.projectMode === 'guided') {
    data.projectMode = 'educational';
  } else if (data.projectMode === 'unguided') {
    data.projectMode = 'professional';
  }
}
