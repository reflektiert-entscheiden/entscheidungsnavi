import { DecisionData } from '../decision-data';
import { dataToText } from './export';

describe('dataToText()', () => {
  let decisionData: DecisionData;

  beforeEach(() => {
    decisionData = new DecisionData();
  });

  it('should return correct JSON', () => {
    const text = dataToText(decisionData, '0.0.0');

    expect(text).toBeTruthy();
    expect(() => JSON.parse(text)).not.toThrow();
  });

  it('should include the exportVersion', () => {
    const text = dataToText(decisionData, '1.4.2');
    const parsed = JSON.parse(text);

    expect(parsed).toHaveProperty('exportVersion', '1.4.2');
  });

  it('should include the project name when supplied', () => {
    const text = dataToText(decisionData, '1.4.2', 'My Project');
    const parsed = JSON.parse(text);

    expect(parsed).toHaveProperty('projectName', 'My Project');
  });

  it.each<string>([undefined, null, ''])('should not include the project name when supplied with %p', projectName => {
    const text = dataToText(decisionData, '1.4.2', projectName);
    expect(JSON.parse(text)).not.toHaveProperty('projectName');
  });
});
