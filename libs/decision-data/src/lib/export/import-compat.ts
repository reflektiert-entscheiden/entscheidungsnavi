import { migrateLegacyPropertiesV55 } from './migrations/v5.5-legacy-properties';
import { migrateToCommonSubstepDataV64 } from './migrations/v6.4-common-substep-data';
import { migrateScrewPositionNameChangeV72 } from './migrations/v7.2-new-screw-data';
import { migrateNewProjectModeNamesV73 } from './migrations/v7.3-project-modes';
import { migrateDecisionStatementValuesV75 } from './migrations/v7.4-decision-statement-values';
import { migrateGermanObjectives } from './migrations/v7.5-german-objectives';
import { migrateObjectiveExplanationsV75 } from './migrations/v7.5-objective-explanations';
import { migrateOutcomesV75 } from './migrations/v7.5-outcome-values';
import { migrateDecisionProblemV80 } from './migrations/v8.0-decision-problem';
import { resetAllCrossImpacts } from './migrations/v8.2-combined-influence-factors';

export function migrateDecisionData(data: any) {
  migrateLegacyPropertiesV55(data);
  migrateToCommonSubstepDataV64(data);
  migrateScrewPositionNameChangeV72(data);
  migrateNewProjectModeNamesV73(data);
  migrateDecisionStatementValuesV75(data);
  migrateGermanObjectives(data);
  migrateObjectiveExplanationsV75(data);
  migrateOutcomesV75(data);
  migrateDecisionProblemV80(data);
  resetAllCrossImpacts(data);
}
