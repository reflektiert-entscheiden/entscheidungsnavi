import { richToPlainText } from '@entscheidungsnavi/tools';
import { DecisionData } from '../decision-data';
import { calculateCurrentProgress } from '../progress';
import { ObjectiveType } from '../classes/objective/objective';
import { importText, importTextWithMetadata } from './import';

// eslint-disable-next-line @typescript-eslint/no-var-requires
const testJsonData = require('./import.spec.json');

describe('importText: Compatibility', () => {
  describe('V3.3.0: Objectives with isNumerisch field', () => {
    it('Parses correctly with isNumerisch', () => {
      const text = JSON.stringify(testJsonData['isNumerisch']);
      const target = new DecisionData();
      importText(text, target);
      expect(target.objectives[0].objectiveType).toBe(ObjectiveType.Numerical);
      expect(target.objectives[1].objectiveType).toBe(ObjectiveType.Verbal);
      expect(target.objectives[0].indicatorData).toBeTruthy();
    });
  });

  describe('V4.0.0: DecisionData without manual_tradeoffs', () => {
    it('initialized manual tradeoffs correctly', () => {
      const text = JSON.stringify(testJsonData['withoutManualTradeoffs']);
      const target = new DecisionData();
      importText(text, target);
      expect(target.weights.manualTradeoffs).toEqual([null, null, null]);
    });
  });

  describe('V4.1.0: Utilityfunctions without explanation', () => {
    it('handles explanations missing from utility functions', () => {
      const text = JSON.stringify(testJsonData['withoutUtilityfunctionExplanations']);
      const target = new DecisionData();
      importText(text, target);
      for (let i = 0; i < 3; i++) {
        expect(target.objectives[i].numericalData.utilityfunction.explanation).toBe('');
        expect(target.objectives[i].indicatorData.utilityfunction.explanation).toBe('');
      }
    });

    it('handles explanations missing from verbal objectives', () => {
      const text = JSON.stringify(testJsonData['withoutUtilityfunctionExplanations']);
      const target = new DecisionData();
      importText(text, target);
      for (let i = 0; i < 3; i++) {
        expect(target.objectives[i].verbalData.utilityFunctionExplanation).toBe('');
      }
    });
  });

  describe('V4.4.0: Empty utility functions', () => {
    it('initializes c and precision correctly', () => {
      const text = JSON.stringify(testJsonData['withEmptyUtilityfunctions']);
      const target = new DecisionData();
      importText(text, target);
      expect(target.objectives[0].numericalData.utilityfunction.c).toBe(0);
      expect(target.objectives[0].numericalData.utilityfunction.precision).toBe(1);
      expect(target.objectives[0].indicatorData.utilityfunction.c).toBe(0);
      expect(target.objectives[0].indicatorData.utilityfunction.precision).toBe(1);
    });
  });

  describe('V4.4.0: Without indicator aggregation fields', () => {
    it('Default to a 0 to 100% scale', () => {
      const text = JSON.stringify(testJsonData['withoutIndicatorAggregationScale']);
      const target = new DecisionData();
      importText(text, target);
      expect(target.objectives[0].indicatorData.useCustomAggregation).toBe(false);
      expect(target.objectives[0].indicatorData.defaultAggregationWorst).toBe(0);
      expect(target.objectives[0].indicatorData.defaultAggregationBest).toBe(100);
      expect(target.objectives[0].indicatorData.aggregatedUnit).toBe('%');
    });
  });

  describe('V5.0.0: With negative indicator coefficient', () => {
    it('swaps scale bounds to make coefficient positive', () => {
      const text = JSON.stringify(testJsonData['negativeIndicatorCoefficients']);
      const target = new DecisionData();
      importText(text, target);

      const indicators = target.objectives[0].indicatorData.indicators;
      expect(indicators[0].min).toBe(0);
      expect(indicators[0].max).toBe(100);
      expect(indicators[0].coefficient).toBe(1);
      expect(indicators[1].min).toBe(100);
      expect(indicators[1].max).toBe(0);
      expect(indicators[1].coefficient).toBe(1);
    });
  });

  describe('V5.0.0: String screws', () => {
    // Old versions of Entscheidungsnavi used strings as screws instead of objects
    const text = JSON.stringify(testJsonData['alternativeHintStringScrews']);
    const target = new DecisionData();
    importText(text, target);

    expect(target.hintAlternatives.screws.length).toBe(3);
    expect(target.hintAlternatives.screws[0].name).toBe('Stellschraube 1');
    expect(target.hintAlternatives.screws[1].name).toBe('Screw 2');
    expect(target.hintAlternatives.screws[2].name).toBe('ABC');
  });

  describe('V5.2.0: Old Decision Statement', () => {
    const text = JSON.stringify(testJsonData['oldDecisionStatement']);
    const target = new DecisionData();
    importText(text, target);

    // Removed Notes to be combined into last note
    const lastNote = target.decisionStatement.notes[target.decisionStatement.notes.length - 1];

    expect(lastNote).toBe(
      target.decisionStatement.notes
        .slice(3)
        .filter(v => v)
        .join('\n'),
    );
  });

  // In version 5.5.1, decision data was overhauled to remove legacy german properties and
  // consolidate hintAlternatives and weights. This checks that both the old and the new format
  // can be imported.
  describe.each([
    ['Legacy Attribute Names', 'legacyAttributes_v5.5.1'],
    ['New Attribute Names', 'newAttributes_v5.5.1'],
  ])('V5.5.1: Import legacy (partly german) attribute names: %s', (_, jsonKey) => {
    let target: DecisionData;
    let projectName: string;

    beforeEach(() => {
      target = new DecisionData();
      ({ projectName } = importTextWithMetadata(JSON.stringify(testJsonData[jsonKey]), target));
    });

    it('imports properties correctly', () => {
      expect(projectName).toBe('Alex (Fallstudie zur Job- und Lebensplanung)');
      expect(target.objectives.length).toBe(6);
      expect(target.alternatives.length).toBe(6);
      expect(target.outcomes.length).toBe(6);
      expect(target.outcomes[0].length).toBe(6);
      expect(target.projectNotes.length).toBeGreaterThan(100);
      expect(target.influenceFactors.length).toBe(2);
    });

    it('imports objectives correctly', () => {
      // Numerical specific data
      const nd = target.objectives[5].numericalData;
      expect(nd.from).toBe(0);
      expect(nd.to).toBe(10);
      expect(nd.unit).toBe('Punkte');
      expect(nd.utilityfunction.level).toBe(0.5);
      expect(nd.utilityfunction.precision).toBe(1.5);
      expect(nd.utilityfunction.width).toBe(1);

      // Verbal specific data
      const vd = target.objectives[1].verbalData;
      expect(vd.options).toEqual(['sehr wenig', 'wenig', 'mittel', 'viel', 'sehr viel']);
      expect(vd.utilities).toEqual([0, 25, 50, 75, 100]);
      expect(vd.precision).toBe(1);
      expect(vd.initParams.stepNumber).toBe(6);
      expect(vd.initParams.from).toBe('gering');
      expect(vd.initParams.to).toBe('hoch');

      // Indicator specific data
      const id = target.objectives[0].indicatorData;
      expect(id.utilityfunction.precision).toBe(0.2);
      expect(id.utilityfunction.width).toBe(0.6);
    });

    it('imports influence factors correctly', () => {
      const uf = target.influenceFactors[0];
      expect(uf.stateCount).toBe(3);
    });

    it('imports outcomes with influence factor correctly', () => {
      expect(target.outcomes[2][0].influenceFactor).toBeTruthy();
    });

    it('imports weights correctly', () => {
      expect(target.weights.tradeoffObjectiveIdx).toBe(5);
      expect(target.weights.preliminaryWeights.length).toBe(6);
      expect(target.weights.preliminaryWeights[0].precision).toBe(5);
      expect(target.weights.tradeoffWeights.length).toBe(6);
      expect(target.weights.tradeoffWeights[0].precision).toBe(1);
      expect(target.weights.unverifiedWeights).toEqual([false, false, false, false, false, true]);
      expect(target.weights.manualTradeoffs.length).toBe(6);
      expect(target.weights.explanations.length).toBe(6);
      expect(target.weights.explanations[0].length).toBeGreaterThan(50);
    });

    it('imports hint alternatives correctly', () => {
      expect(target.alternatives.length).toBe(6);
      expect(target.hintAlternatives.ideas.noteGroups.length).toBe(6);
      expect(target.hintAlternatives.screws.length).toBe(3);
    });

    it('imports note pages correctly', () => {
      expect(target.hintAlternatives.ideas.noteGroups.length).toBe(6);
      expect(target.hintAlternatives.ideas.noteGroups[0].id).toBe(6);
      expect(target.hintAlternatives.ideas.noteGroups[0].notes.length).toBe(2);
      expect(target.hintAlternatives.ideas.noteGroups[0].notes[0].name).toBe('Home Office');
    });
  });

  describe('V6.4.0: Old Objective Aspects and Alternative Hints', () => {
    let target: DecisionData;

    beforeEach(() => {
      target = new DecisionData();
      const text = JSON.stringify(testJsonData['oldAspectsAndAlternatives']);
      importText(text, target);
    });

    it('imports objective aspects and alternatives correctly', () => {
      expect(target.alternatives.length).toBe(9);
      expect(target.objectiveAspects.listOfAspects.length).toBe(2);
      expect(target.objectiveAspects.listOfDeletedAspects.length).toBe(3);
      expect(target.objectives.length).toBe(2);
    });

    it('assigns alternatives to the correct substep', () => {
      const a1 = target.alternatives.find(a => a.name === 'a1');
      expect(a1.createdInSubStep).toBe(1);

      const a3 = target.alternatives.find(a => a.name === 'a3');
      expect(a3.createdInSubStep).toBe(2);

      const a10 = target.alternatives.find(a => a.name === 'a10');
      expect(a10.createdInSubStep).toBe(0);
    });

    it('assigns objectives to the correct substep', () => {
      const o1 = target.objectives.find(obj => obj.name === 'Die Welt verbessern wollen');
      expect(o1.aspects.value.createdInSubStep).toBe(1);

      const a3 = o1.aspects.children.find(child => child.value.name === 'a3');
      expect(a3.value.createdInSubStep).toBe(2);
    });

    it('preserves deleted objective aspects in trash', () => {
      const o1 = target.objectiveAspects.listOfDeletedAspects.find(
        aspect => aspect.name === 'Intellektuelle Erfüllung: Lernen, verstehen, wissen',
      );
      expect(o1.createdInSubStep).toBe(1);

      const a2 = target.objectiveAspects.listOfDeletedAspects.find(aspect => aspect.name === 'a2');
      expect(a2.createdInSubStep).toBe(2);
    });
  });

  it('V5.5.0: parses manual_tradeoffs correctly', () => {
    const text = JSON.stringify(testJsonData['withManualTradeoffs']);
    const target = new DecisionData();
    importText(text, target);
    expect(target.weights.manualTradeoffs).toEqual([
      null,
      [
        [0.4, 0.6],
        [0.6, 0.4],
      ],
      [
        [0.04, 0.5],
        [0.05, 0.01],
      ],
    ]);
  });

  it('parses string outcome-values to numbers', () => {
    // (also see the comment in the loadOutcomes method)
    // In previous versions outcome values were sometimes saved as
    // strings instead of as numbers. This test confirms that
    // those strings are parsed to numbers correctly.
    const text = JSON.stringify(testJsonData['outcomeValuesStrings']);
    const target = new DecisionData();
    importText(text, target);
    // Check whether all outcome values are either numbers or arrays of numbers.
    const result = target.outcomes.every(ocForAlt => {
      return ocForAlt.every(outcome => {
        return outcome.values.every(
          objectiveValues =>
            Array.isArray(objectiveValues) &&
            objectiveValues.every(
              values => Array.isArray(values) && values.every(value => typeof value === 'number' || value === undefined),
            ),
        );
      });
    });
    expect(result).toBe(true);
  });

  describe('V4.5.1: Without including aspects twice in the hierarchy and in the brainstorming box', () => {
    it('displays no aspects in the brainstorming box', () => {
      const text = JSON.stringify(testJsonData['withOldNotizen']);
      const target = new DecisionData();
      importText(text, target);
      expect(target.objectiveAspects.listOfAspects.length).toBe(0);
    });
  });

  describe('V6.3.2: Without reaching result in step 2', () => {
    it('detects progress successfully and does not duplicate aspects in Brainstorming and Papierkorb', () => {
      const text = JSON.stringify(testJsonData['exportWithoutReachingStep2Result_v6.3.2']);
      const target = new DecisionData();
      importText(text, target);
      expect(target.objectiveAspects.listOfAspects.length).toBe(0);
      expect(target.objectiveAspects.listOfDeletedAspects.length).toBe(3);
      expect(calculateCurrentProgress(target)).toEqual({
        step: 'objectives',
        subStepIndex: 3,
      });
    });
  });

  describe('V6.3.2: Without reaching result in step 3', () => {
    it('detects progress successfully and displays children alternatvies when grouped', () => {
      const text = JSON.stringify(testJsonData['exportWithoutReachingStep3Result_v6.3.2']);
      const target = new DecisionData();
      importText(text, target);
      expect(target.alternatives.length).toBe(2);
      expect(target.alternatives[0].createdInSubStep).toBe(1);
      expect(target.alternatives[0].children[0].createdInSubStep).toBe(5);
      expect(target.alternatives[1].createdInSubStep).toBe(1);
      expect(calculateCurrentProgress(target)).toEqual({
        step: 'alternatives',
        subStepIndex: 5,
      });
    });
  });

  describe('V7.3: Projects should have new project mode names', () => {
    // with version 7.3 we changed the project mode names 'simple', 'guided', 'unguided' to 'starter', 'educational' and 'professional'
    it('should have project mode educational', () => {
      const text = JSON.stringify(testJsonData['exportWithoutReachingStep3Result_v6.3.2']);
      const target = new DecisionData();
      importText(text, target);
      expect(target.projectMode).toBe('educational');
    });

    it('should have project mode educational', () => {
      const text = JSON.stringify(testJsonData['exportWithoutReachingStep2Result_v6.3.2']);
      const target = new DecisionData();
      importText(text, target);
      expect(target.projectMode).toBe('educational');
    });
  });

  describe('V7.5: Decision statement values no longer have contexts', () => {
    // Before 7.5, decision statement values were dependent on context and there were different values per context.
    // Since then, we just have one set of values.
    it('copies the values from the active context', () => {
      const object = testJsonData['exportWithoutReachingStep2Result_v6.3.2'];
      const text = JSON.stringify(object);

      const target = new DecisionData();
      importText(text, target);

      expect(
        target.decisionStatement.values.every(
          (value, index) =>
            value.name === object.decisionStatement.values['private'][index].name &&
            value.val === object.decisionStatement.values['private'][index].val,
        ),
      ).toBeTruthy();
    });
  });

  describe('V7.5.2: Numerical and verbal objective explanations', () => {
    let target: DecisionData;

    beforeEach(() => {
      const object = testJsonData['withObjectiveExplanationsAndComments_v7.5.2'];
      const text = JSON.stringify(object);

      target = new DecisionData();
      importText(text, target);
    });

    it('parses verbal objective explanations correctly', () => {
      const firstPlainComment = richToPlainText(target.objectives[1].verbalData.comments[0]);
      expect(firstPlainComment).toBe('Eine Kommentar zur ersten Ausprägung\n\n\nEine kurze Beschreibung der ersten Ausprägung');

      const secondPlainComment = richToPlainText(target.objectives[1].verbalData.comments[1]);
      expect(secondPlainComment).toBe('Eine Kommentar zur zweiten Ausprägung\n\n\nEine kurze Beschreibung der zweiten Ausprägung');

      const lastPlainComment = richToPlainText(target.objectives[1].verbalData.comments[4]);
      expect(lastPlainComment).toBe('Eine Kommentar zur letzten Ausprägung\n\n\nEine kurze Beschreibung der letzten Ausprägung');
    });

    it('parses numerical objective explanations correctly', () => {
      const fromComment = richToPlainText(target.objectives[5].numericalData.commentFrom);
      expect(fromComment).toBe('Eine Kommentar zur schlechtesten Ausprägung\n\n\nEine kurze Beschreibung der schlechtesten Ausprägung');

      const toComment = richToPlainText(target.objectives[5].numericalData.commentTo);
      expect(toComment).toBe('Ein Kommentar zur besten Ausprägung\n\n\nEine kurze Beschreibung der besten Ausprägung');
    });
  });
});

describe('2D Outcome Values', () => {
  it('converts 2D ObjectiveInput in 3D ObjectiveInput', () => {
    const text = JSON.stringify(testJsonData['exportWith2DOutcomeValues']);
    const target = new DecisionData();
    importText(text, target);
    // check if all outcome.values are 3D including no nulls and have numbers as inner-most elements
    expect(target.outcomes.flat().map(outcome => outcome.values)).toEqual([
      [[[4]]],
      [[[2]]],
      [[[3], [4]]],
      [[[3]]],
      [[[5]]],
      [[[3]]],
      [[[6], [2]]],
      [[[2]]],
    ]);
  });
});
