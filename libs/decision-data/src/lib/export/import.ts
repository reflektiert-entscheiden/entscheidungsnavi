import { DecisionData } from '../decision-data';
import { ARRAY_PROPERTY_NAMES, ExportData, OBJECT_PROPERTY_NAMES, OPTIONAL_PROPERTY_NAMES, PROPERTY_NAMES } from './properties';
import {
  loadAlternatives,
  loadDecisionQuality,
  loadGlobalVariables,
  loadHintAlternatives,
  loadInfluenceFactors,
  loadIssues,
  loadObjectiveAspects,
  loadObjectives,
  loadOutcomes,
  loadSavedValues,
  loadStatement,
  loadStepExplanations,
  loadTimeRecording,
  loadWeights,
} from './import-loaders';
import { migrateDecisionData } from './import-compat';

export function importText(text: string, target: DecisionData): DecisionData {
  return importTextWithMetadata(text, target).data;
}

// safer than using read_text() directly, because the target will not be modified before the conversion is finished
export function importTextWithMetadata(
  text: string,
  target: DecisionData,
): { data: DecisionData; exportVersion: string; projectName?: string } {
  const { data: tempData, ...remainingResult } = readTextWithMetadata(text);
  return { data: copyProperties(tempData, target), ...remainingResult };
}

export function readText(text: string, target?: DecisionData): DecisionData {
  return readTextWithMetadata(text, target).data;
}

export function readTextWithMetadata(
  text: string,
  target?: DecisionData,
): { data: DecisionData; exportVersion: string; projectName?: string } {
  if (!target) {
    target = new DecisionData();
  }
  let data: ExportData;
  try {
    data = JSON.parse(text);
    if (typeof data === 'string') {
      // TODO: Remove
      // temporary fix for files that have been stringified twice
      data = JSON.parse(data);
    }
  } catch (e) {
    console.error(e);
    throw new Error('The given text is not valid JSON.');
  }

  try {
    // Perform migrations on top-level properties in DecisionData
    migrateDecisionData(data);
  } catch (e) {
    console.error(e);
    throw new Error('Failed to convert the file to the current data format.');
  }

  if (checkData(data)) {
    try {
      return {
        data: convertProperties(data, target),
        exportVersion: data.exportVersion,
        projectName: data.projectName,
      };
    } catch (e) {
      console.error(e);
      throw new Error('Failed to import the given data.');
    }
  } else {
    throw new Error('The given data has the wrong format.');
  }
}

/**
 * @param data - the data to be checked
 * @returns true, if no properties are missing or of the wrong type
 */
export function checkData(data: any): boolean {
  // Iterate over all required properties
  const missing: Array<string> = [];
  const wrongType: Array<string> = [];
  for (const name of PROPERTY_NAMES) {
    if (name in data && data[name] != null) {
      // Check the property types
      if (
        !(
          (!ARRAY_PROPERTY_NAMES.includes(name) &&
            (typeof data[name] === 'string' ||
              typeof data[name] === 'number' ||
              typeof data[name] === 'boolean' ||
              (OBJECT_PROPERTY_NAMES.includes(name) && typeof data[name] === 'object'))) ||
          (ARRAY_PROPERTY_NAMES.includes(name) && Array.isArray(data[name]))
        )
      ) {
        wrongType.push(name);
        console.log(OBJECT_PROPERTY_NAMES.includes(name));
        console.log(OBJECT_PROPERTY_NAMES);
        console.log('property ' + name + ' has the wrong type ' + typeof data[name] + '!');
      }
    } else if (!OPTIONAL_PROPERTY_NAMES.includes(name)) {
      missing.push(name);
      console.log('property ' + name + ' missing!');
    }
  }
  return missing.length === 0 && wrongType.length === 0;
}

/**
 * Copies properties from an imported DecisionData object to a target object.
 */
export function convertProperties(data: ExportData, target?: DecisionData): DecisionData {
  if (!target) {
    target = new DecisionData();
  }
  for (const name of PROPERTY_NAMES) {
    switch (name) {
      case 'globalVariables':
        loadGlobalVariables(data.globalVariables, target);
        break;
      case 'decisionQuality':
        loadDecisionQuality(data.decisionQuality, target);
        break;
      case 'decisionStatement':
        loadStatement(data.decisionStatement, target);
        break;
      case 'objectives':
        loadObjectives(data.objectives, target);
        break;
      case 'objectiveAspects':
        loadObjectiveAspects(data.objectiveAspects, target);
        break;
      case 'alternatives':
        loadAlternatives(data.alternatives, target);
        break;
      case 'hintAlternatives':
        loadHintAlternatives(data.hintAlternatives, target);
        break;
      case 'influenceFactors':
        loadInfluenceFactors(data.influenceFactors, target);
        break;
      case 'outcomes':
        loadOutcomes(data.outcomes, target);
        break;
      case 'weights':
        loadWeights(data.weights, target);
        break;
      case 'stepExplanations':
        loadStepExplanations(data.stepExplanations, target);
        break;
      case 'issues':
        loadIssues(data.issues, target);
        break;
      case 'savedValues':
        loadSavedValues(data.savedValues, target);
        break;
      case 'timeRecording':
        loadTimeRecording(data.timeRecording, target);
        break;
      case 'extraData':
        target.extraData = data.extraData;
        break;
      default:
        if ((data as any)[name] != null) {
          (target as any)[name] = (data as any)[name];
        }
    }
  }

  target.weights.restoreArrayLengths(target.objectives.length);

  return target;
}

export function copyProperties(source: DecisionData, target: DecisionData): DecisionData {
  PROPERTY_NAMES.forEach(name => {
    if ((source as any)[name] != null) {
      (target as any)[name] = (source as any)[name];
    }
  });
  target.restoreInfluenceFactorIDs(); // repair broken IDs in old exports

  // Redo Cyclic DecisionData References
  target.hintAlternatives.decisionData = target;
  target.objectiveAspects.decisionData = target;

  return target;
}
