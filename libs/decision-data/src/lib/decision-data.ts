import { BehaviorSubject, Subject } from 'rxjs';
import { Interval, moveInArray } from '@entscheidungsnavi/tools';
import { pick } from 'lodash';
import * as validations from './validations/validations';
import { flattenErrors } from './validations/validations';
import { Weights } from './classes/weights';
import { HintAlternatives } from './classes/alternative/hint-alternatives';
import { CommonData } from './export/properties';
import { NAVI_STEP_ORDER, NaviStep } from './classes/navi-step';
import { ObjectiveAspects, ObjectiveElement } from './classes/objective/objective-aspects';
import {
  CompositeUserDefinedInfluenceFactor,
  UserDefinedInfluenceFactor,
  isCustomInfluenceFactor,
  ActionEntry,
} from './classes/influence-factor/composite-influence-factor';
import { DecisionStatement } from './classes/decision-statement';
import { ProjectMode } from './classes/project-mode';
import { DecisionQuality } from './classes/decision-quality';
import { IssueCategory } from './classes/issue-category';
import { Outcome } from './classes/outcome/outcome';
import { Alternative } from './classes/alternative/alternative';
import { Objective, ObjectiveType } from './classes/objective/objective';
import { SavedValues } from './classes/saved-values';
import { TimeRecording } from './classes/time-recording';
import { NotePage } from './classes/note-page';
import { SimpleUserDefinedInfluenceFactor, UserDefinedState } from './classes/influence-factor/simple-user-defined-influence-factor';
import { Indicator } from './classes/objective/indicator';
import { ErrorMsg } from './classes/error';
import { getAlternativeUtilitiesMeta, getUtilityMatrixMeta, getWeightedUtilityMatrixMeta } from './utility/utility-meta';
import { NumericalObjectiveData } from './classes/objective/numerical-objective-data';
import { UtilityFunction } from './classes/utility-function';
import { PredefinedInfluenceFactor } from './classes/influence-factor/predefined-influence-factor';
import { GlobalVariables } from './classes/global-variables';

export type AttachedObjectiveData = 'ideas' | 'outcomes' | 'weights' | 'utility' | 'scale';

export class DecisionData implements CommonData {
  objectiveAdded$ = new Subject<number>();
  objectiveRemoved$ = new Subject<number>();
  alternativeAdded$ = new Subject<number>();
  influenceFactorAdded$ = new Subject<number>();
  objectiveAspectAdded$ = new Subject<void>();
  objectiveOptionAdded$ = new Subject<[objectiveIndex: number, optionIndex: number, newOptionCount: number]>();

  authorName: string;

  projectMode$ = new BehaviorSubject<ProjectMode>('educational');

  decisionStatement: DecisionStatement;
  objectiveAspects: ObjectiveAspects;
  objectiveHierarchyMainElement: ObjectiveElement;
  objectives: Objective[];
  hintAlternatives: HintAlternatives;
  alternatives: Alternative[];
  outcomes: Outcome[][]; // [#alternatives][#objectives]
  influenceFactors: Array<UserDefinedInfluenceFactor>;
  weights: Weights;
  decisionQuality: DecisionQuality;
  issues: Record<IssueCategory, string[]>;
  globalVariables: GlobalVariables;

  /**
   * We save one explanation for each step of the entscheidungsnavi.
   */
  stepExplanations: Record<NaviStep, string>;
  projectNotes: string;
  savedValues: SavedValues;
  timeRecording: TimeRecording;
  resultSubstepProgress: number; // Indicates which results substep is currently active (or null if none)
  lastUrl: string;

  // Field that can be used to add additional data to the project that will be saved / loaded.
  extraData: { [key: string]: unknown };

  get projectMode() {
    return this.projectMode$.value;
  }
  set projectMode(newMode: ProjectMode) {
    if (newMode !== this.projectMode) {
      this.projectMode$.next(newMode);
    }
  }

  constructor(public version = '0.0.0') {
    this.reset(version);
  }

  reset(version = '0.0.0') {
    this.authorName = '';
    this.decisionQuality = new DecisionQuality();
    this.decisionStatement = new DecisionStatement();
    this.objectiveAspects = new ObjectiveAspects(this);
    this.objectiveHierarchyMainElement = new ObjectiveElement();
    this.objectives = [];
    this.weights = new Weights();
    this.alternatives = [];
    this.influenceFactors = [];
    this.projectNotes = '';
    this.outcomes = [];
    this.issues = {
      notAssigned: [],
      decisionStatement: [],
      objectives: [],
      alternatives: [],
      influenceFactors: [],
    };
    this.stepExplanations = {
      decisionStatement: '',
      objectives: '',
      alternatives: '',
      impactModel: '',
      results: '',
      finishProject: '',
    };
    this.hintAlternatives = new HintAlternatives(this);
    this.version = version;
    this.lastUrl = null;
    this.resultSubstepProgress = null;
    this.savedValues = new SavedValues();
    this.timeRecording = new TimeRecording();
    NotePage.resetCounters();
    this.extraData = {};
    this.projectMode = 'educational';
    this.globalVariables = new GlobalVariables();
  }

  isEducational() {
    return this.projectMode === 'educational';
  }

  isProfessional() {
    return this.projectMode === 'professional';
  }

  isStarter() {
    return this.projectMode === 'starter';
  }

  addObjective(objective = new Objective(), position = this.objectives.length) {
    objective.indicatorData.globalVariables = this.globalVariables;

    this.objectives.splice(position, 0, objective); // insert objective at the given position
    this.weights.addObjective(position);
    this.hintAlternatives.addObjective(position);

    // add the new outcome objects for the new objective
    this.outcomes.forEach(row => row.splice(position, 0, new Outcome(null, objective)));

    this.objectiveAdded$.next(position);

    return objective;
  }

  moveObjective(from: number, to: number) {
    this.objectives.splice(to, 0, ...this.objectives.splice(from, 1));
    this.weights.moveObjective(from, to);
    this.outcomes.forEach(row => row.splice(to, 0, ...row.splice(from, 1)));
  }

  removeObjective(position: number) {
    this.objectives[position].indicatorData.globalVariables = null;

    this.objectives.splice(position, 1);
    this.weights.removeObjective(position);
    this.hintAlternatives.removeObjective(position);
    this.outcomes.forEach(e => e.splice(position, 1));

    this.objectiveRemoved$.next(position);
  }

  /**
   * Changes the type of an objective.
   *
   * @returns true when a manual tradeoff for this objective was deleted as part of this process
   */
  changeObjectiveType(position: number, newType: ObjectiveType) {
    // Only change when the newType is in fact different
    if (this.objectives[position].objectiveType === newType) return;

    // We change the type of the objective...
    const objective = this.objectives[position];
    objective.objectiveType = newType;

    // ...but also have to update the value entries of the outcomes
    this.outcomes.forEach(ocForAlternative => {
      ocForAlternative[position].initializeValues(objective);
    });

    return this.onUtilityFunctionChange(position);
  }

  /**
   * Called whenever a utility function changes. Deletes manual tradeoffs if required.
   *
   * @param objectivePosition - The position of the objective whose utility function was changed
   * @returns true when a manual tradeoff was deleted
   */
  onUtilityFunctionChange(objectivePosition: number) {
    if (this.weights.manualTradeoffs[objectivePosition]) {
      this.weights.manualTradeoffs[objectivePosition] = null;
      return true;
    }

    return false;
  }

  addAlternative({
    alternative = new Alternative(1),
    position = this.alternatives.length,
  }: { alternative?: Alternative; position?: number } = {}): Alternative {
    if (this.hintAlternatives.screws && alternative.screwConfiguration.length !== this.hintAlternatives.screws.length)
      alternative.screwConfiguration = new Array(this.hintAlternatives.screws.length).fill(null);

    this.alternatives.splice(position, 0, alternative);
    this.outcomes.splice(
      position,
      0,
      this.objectives.map(obj => new Outcome(null, obj)),
    );
    this.alternativeAdded$.next(position);
    return alternative;
  }

  moveAlternative(from: number, to: number) {
    this.alternatives.splice(to, 0, ...this.alternatives.splice(from, 1));
    this.outcomes.splice(to, 0, ...this.outcomes.splice(from, 1));
  }

  removeAlternative(position: number) {
    this.alternatives.splice(position, 1);
    this.outcomes.splice(position, 1);
  }

  addInfluenceFactor(inf: UserDefinedInfluenceFactor = new SimpleUserDefinedInfluenceFactor(), position = this.influenceFactors.length) {
    this.influenceFactors.splice(position, 0, inf);
    this.restoreInfluenceFactorIDs();
    this.influenceFactorAdded$.next(position);
    return inf;
  }

  removeInfluenceFactor(position: number) {
    // reset all outcomes that depend on that uncertainty factor
    this.outcomes.flat().forEach(outcome => {
      if (outcome.influenceFactor === this.influenceFactors[position]) {
        outcome.setInfluenceFactor(undefined);
      }
    });
    this.influenceFactors.splice(position, 1);
    this.restoreInfluenceFactorIDs();
  }

  /**
   * This callback method should be called whenever the state count of an influence factor changes
   * @param changedInfluenceFactor - The influence factor that changed
   * @param actionList - The list of actions that were performed on the influence factor
   * @param depth - The depth in which the originally changed SimpleUserDefinedIF is located in the current CompositeIF's baseFactors
   */
  influenceFactorStateCountChanged(changedInfluenceFactor: UserDefinedInfluenceFactor, actionList: ActionEntry[], depth = 1) {
    this.influenceFactors.forEach(influenceFactor => {
      if (influenceFactor instanceof CompositeUserDefinedInfluenceFactor) {
        const baseFactorIndex = influenceFactor.baseFactors.indexOf(changedInfluenceFactor); // either 0 (row) or 1 (column)
        if (baseFactorIndex !== -1) {
          influenceFactor.adjustToBaseFactors(actionList, baseFactorIndex, depth);
          this.influenceFactorStateCountChanged(influenceFactor, actionList, depth + 1);
        }
      }
    });
  }

  moveInfluenceFactor(from: number, to: number) {
    this.influenceFactors.splice(to, 0, ...this.influenceFactors.splice(from, 1));
    this.restoreInfluenceFactorIDs();
  }

  // reset uncertainty factor IDs to their index
  restoreInfluenceFactorIDs() {
    this.influenceFactors.forEach((uf, idx) => (uf.id = idx));
  }

  addState(uf: SimpleUserDefinedInfluenceFactor, state?: UserDefinedState, position?: number) {
    if (position == null) position = uf.states.length;

    uf.states.splice(position, 0, state);
    this.outcomes.flat().forEach(outcome => {
      if (outcome.influenceFactor === uf) {
        outcome.addUfState(position);
      } else if (
        outcome.influenceFactor instanceof CompositeUserDefinedInfluenceFactor &&
        outcome.influenceFactor.baseFactors.includes(uf)
      ) {
        if (outcome.influenceFactor.baseFactors[0] === uf) {
          for (let i = 0; i < outcome.influenceFactor.baseFactors[1].stateCount; i++)
            outcome.addUfState(outcome.influenceFactor.getStateIndex(position, i));
        } else {
          for (let i = 0; i < outcome.influenceFactor.baseFactors[0].stateCount; i++)
            outcome.addUfState(outcome.influenceFactor.getStateIndex(i, position));
        }
      }
    });
  }

  removeState(uf: SimpleUserDefinedInfluenceFactor, position: number) {
    uf.states.splice(position, 1);
    this.outcomes.flat().forEach(outcome => {
      if (outcome.influenceFactor === uf) {
        outcome.removeUfState(position);
      } else if (
        outcome.influenceFactor instanceof CompositeUserDefinedInfluenceFactor &&
        outcome.influenceFactor.baseFactors.includes(uf)
      ) {
        if (outcome.influenceFactor.baseFactors[0] === uf) {
          for (let i = outcome.influenceFactor.baseFactors[1].stateCount - 1; i >= 0; i--)
            outcome.removeUfState(outcome.influenceFactor.getStateIndex(position, i));
        } else {
          for (let i = outcome.influenceFactor.baseFactors[0].stateCount - 1; i >= 0; i--)
            outcome.removeUfState(outcome.influenceFactor.getStateIndex(i, position));
        }
      }
    });
  }

  moveState(uf: SimpleUserDefinedInfluenceFactor, fromPosition: number, toPosition: number) {
    moveInArray(uf.states, fromPosition, toPosition);

    this.outcomes.flat().forEach(outcome => {
      if (outcome.influenceFactor === uf) {
        outcome.moveUfState(fromPosition, toPosition);
      }
    });
  }

  removeAllStates(uf: SimpleUserDefinedInfluenceFactor) {
    const length = uf.states.length;
    for (let i = 0; i < length; i++) {
      this.removeState(uf, 0);
    }
  }

  getUserDefinedInfluenceFactors() {
    return this.influenceFactors.filter(
      (influenceFactor): influenceFactor is SimpleUserDefinedInfluenceFactor => influenceFactor instanceof SimpleUserDefinedInfluenceFactor,
    );
  }

  getCompositeUserDefinedInfluenceFactors() {
    return this.influenceFactors.filter(
      (influenceFactor): influenceFactor is CompositeUserDefinedInfluenceFactor =>
        influenceFactor instanceof CompositeUserDefinedInfluenceFactor,
    );
  }

  addObjectiveOption(objectivePosition: number, optionPosition?: number, optionName = '') {
    if (optionPosition == null) {
      optionPosition = this.objectives[objectivePosition].verbalData.options.length;
    }
    this.objectives[objectivePosition].verbalData.addOption(optionPosition, optionName);

    this.outcomes
      .map(outcomeRow => outcomeRow[objectivePosition])
      .forEach(outcome => {
        outcome.values.forEach(value => {
          if (value[0][0] != null && value[0][0] >= optionPosition + 1) {
            value[0][0]++;
          }
        });
      });

    this.objectiveOptionAdded$.next([objectivePosition, optionPosition, this.objectives[objectivePosition].verbalData.options.length]);
  }

  removeObjectiveOption(objectivePosition: number, optionPosition: number) {
    this.objectives[objectivePosition].verbalData.removeOption(optionPosition);

    this.outcomes
      .map(outcomeRow => outcomeRow[objectivePosition])
      .forEach(outcome => {
        outcome.values.forEach(value => {
          if (value[0][0] === optionPosition + 1) {
            value[0][0] = undefined;
          } else if (value[0][0] != null && value[0][0] > optionPosition) {
            value[0][0]--;
          }
        });
        outcome.checkProcessed();
      });
  }

  moveObjectiveOption(objectivePosition: number, fromPosition: number, toPosition: number) {
    this.objectives[objectivePosition].verbalData.moveOption(fromPosition, toPosition);

    const affectedValueInterval = new Interval(fromPosition + 1, toPosition + 1);
    const positionShift = fromPosition < toPosition ? -1 : 1;

    this.outcomes
      .map(outcomeRow => outcomeRow[objectivePosition])
      .forEach(outcome => {
        outcome.values.forEach(value => {
          if (value[0][0] === fromPosition + 1) {
            value[0][0] = toPosition + 1;
          } else if (value[0][0] != null && affectedValueInterval.includes(value[0][0])) {
            value[0][0] += positionShift;
          }
        });
      });
  }

  addObjectiveIndicator(objectivePosition: number, indicatorPosition?: number) {
    if (objectivePosition >= 0 && objectivePosition < this.objectives.length) {
      const objInd = this.objectives[objectivePosition].indicatorData;
      // Default to adding it to the end of the list
      if (indicatorPosition == null) {
        indicatorPosition = objInd.indicators.length;
      }
      objInd.addIndicator(indicatorPosition, new Indicator());
      this.outcomes.forEach(ocForAlternative => {
        ocForAlternative[objectivePosition].addObjectiveIndicator(indicatorPosition);
      });
    }
  }

  removeObjectiveIndicator(objectivePosition: number, indicatorPosition: number) {
    if (objectivePosition >= 0 && objectivePosition < this.objectives.length) {
      this.objectives[objectivePosition].indicatorData.removeIndicator(indicatorPosition);
      this.outcomes.forEach(ocForAlternative => {
        ocForAlternative[objectivePosition].removeObjectiveIndicator(indicatorPosition);
      });
    }
  }

  moveObjectiveIndicator(objectivePosition: number, indicatorFromPosition: number, indicatorToPosition: number) {
    if (objectivePosition >= 0 && objectivePosition < this.objectives.length) {
      this.objectives[objectivePosition].indicatorData.moveIndicator(indicatorFromPosition, indicatorToPosition);
      this.outcomes.forEach(ocForAlternative => {
        ocForAlternative[objectivePosition].moveObjectiveIndicator(indicatorFromPosition, indicatorToPosition);
      });
    }
  }

  validateDecisionStatement(): [boolean, ErrorMsg[]] {
    return validations.validateDecisionStatement(this.decisionStatement);
  }

  validateObjectives(): [boolean, ErrorMsg[]] {
    return validations.validateObjectives(this.objectives);
  }

  validateObjectiveScales(): [boolean, ErrorMsg[]] {
    return validations.validateObjectiveScales(this.objectives);
  }

  validateAlternatives(): [boolean, ErrorMsg[]] {
    return validations.validateAlternatives(this.alternatives);
  }

  validateUtilityFunctions(): [boolean, ErrorMsg[], number] {
    return validations.validateUtilityFunctions(this.objectives);
  }

  validateTradeoffObjective(): [boolean, ErrorMsg[]] {
    return validations.validateTradeoffObjective(this.weights, this.objectives);
  }

  validateWeights(): [boolean, ErrorMsg[]] {
    return validations.validateWeights(this.weights.preliminaryWeights);
  }

  validateOutcomes(): [boolean, ErrorMsg[]] {
    return validations.validateOutcomes(this.alternatives, this.objectives, this.outcomes);
  }

  validateInfluenceFactors(): [boolean, ErrorMsg[], number] {
    return validations.validateInfluenceFactors(this.influenceFactors);
  }

  getUtilityMatrix() {
    return getUtilityMatrixMeta(this.outcomes, this.objectives);
  }

  getWeightedUtilityMatrix() {
    return getWeightedUtilityMatrixMeta(this.outcomes, this.objectives, this.weights);
  }

  /**
   * Compute utilities for every alternative.
   *
   * @returns utility for every alternative
   */
  getAlternativeUtilities() {
    return getAlternativeUtilitiesMeta(this.outcomes, this.objectives, this.weights);
  }

  getAttachedObjectiveData(objectiveIndex: number) {
    const data: AttachedObjectiveData[] = [];

    const objective = this.objectives[objectiveIndex];

    // Alternative Ideas
    const group = this.hintAlternatives.ideas ? this.hintAlternatives.ideas.noteGroups[objectiveIndex] : undefined;
    const hasIdeas = (group ? group.notes.length : 0) > 0;

    if (hasIdeas) {
      data.push('ideas');
    }

    // Impact Matrix
    const defaultObjective = new NumericalObjectiveData();
    const hasOutcomes = this.outcomes.some(row => row[objectiveIndex].processed);
    const hasCustomScale =
      !objective.isNumerical ||
      objective.numericalData.from !== defaultObjective.from ||
      objective.numericalData.to !== objective.numericalData.to;

    if (hasOutcomes) {
      data.push('outcomes');
    }

    if (hasCustomScale) {
      data.push('scale');
    }

    // Objective Weighting
    const defaultWeight = Weights.getDefault();
    const objectiveWeight = this.weights.getWeights()[objectiveIndex];
    const hasCustomWeight =
      objectiveWeight && (objectiveWeight.value !== defaultWeight.value || objectiveWeight.precision !== defaultWeight.precision);

    if (hasCustomWeight) {
      data.push('weights');
    }

    // Utility Function
    const defaultUtilityFunction = new UtilityFunction();

    let hasCustomUtilityFunction = false;
    if (objective.isVerbal) {
      hasCustomUtilityFunction =
        objective.verbalData.hasCustomUtilityValues ||
        objective.verbalData.c !== defaultUtilityFunction.c ||
        objective.verbalData.precision !== defaultUtilityFunction.precision;
    } else {
      const utilityFunction = objective.isNumerical
        ? objective.numericalData.utilityfunction
        : objective.isIndicator
          ? objective.indicatorData.utilityfunction
          : undefined;
      hasCustomUtilityFunction =
        utilityFunction &&
        (utilityFunction.c !== defaultUtilityFunction.c || utilityFunction.precision !== defaultUtilityFunction.precision);
    }

    if (hasCustomUtilityFunction) {
      data.push('utility');
    }

    // No Data found
    return data;
  }

  getInaccuracies() {
    const inaccuracies = {
      utilityNumericalIndicator: false,
      utilityVerbal: false,
      weights: false,
      probabilities: false,
      customInfluenceFactors: false,
      predefinedInfluenceFactors: false,
    };

    for (let i = 0; i < this.objectives.length; i++) {
      const objective = this.objectives[i];

      // Utility Functions
      if (objective.isNumerical) {
        if (objective.numericalData.utilityfunction.precision > 0) {
          inaccuracies.utilityNumericalIndicator = true;
        }
      } else if (objective.isVerbal) {
        if (objective.verbalData.precision > 0) {
          inaccuracies.utilityVerbal = true;
        }
      } else if (objective.isIndicator) {
        if (objective.indicatorData.utilityfunction.precision > 0) {
          inaccuracies.utilityNumericalIndicator = true;
        }
      }

      // Weights
      const weight = this.weights.getWeight(i);

      if (weight.precision > 0) {
        inaccuracies.weights = true;
      }
    }

    for (const ocsForAlternative of this.outcomes) {
      for (const outcome of ocsForAlternative) {
        const uF = outcome.influenceFactor;
        if (isCustomInfluenceFactor(uF)) {
          inaccuracies.customInfluenceFactors = true;
          inaccuracies.probabilities ||= uF instanceof CompositeUserDefinedInfluenceFactor ? false : uF.precision > 0;
        } else if (uF instanceof PredefinedInfluenceFactor) {
          inaccuracies.predefinedInfluenceFactors = true;
        }
      }
    }

    return inaccuracies;
  }

  /**
   * Find an Object identified by UUID.
   *
   * @param objectId - The UUID of the object
   * @returns the object
   */
  findObject(objectId: string) {
    const findInCollection = <T extends { uuid: string }>(collection: T[]) => {
      const index = collection.findIndex(object => object.uuid === objectId);

      if (index < 0) {
        return null;
      }

      return { object: collection[index], index };
    };

    return (
      findInCollection(this.objectives) ??
      findInCollection(this.alternatives) ??
      findInCollection(this.influenceFactors) ??
      findInCollection(this.outcomes.flat()) ??
      findInCollection(this.hintAlternatives.screws)
    );
  }

  validateStep(step: NaviStep): [boolean, ErrorMsg[]] {
    switch (step) {
      case 'decisionStatement':
        return this.validateDecisionStatement();
      case 'objectives':
        return this.validateObjectives();
      case 'alternatives':
        return this.validateAlternatives();
      case 'impactModel':
        return flattenErrors([this.validateObjectiveScales(), this.validateOutcomes(), this.validateInfluenceFactors()]);
      case 'results':
        return flattenErrors([this.validateWeights(), this.validateUtilityFunctions()]);
      case 'finishProject':
        return [true, null];
    }
  }

  /**
   * Returns the first navi step which contains an error, or `null` if all steps are valid.
   *
   * @param untilStep - Up until which step to check. Check all steps if omitted.
   * @returns The first step with an error or `null`.
   */
  getFirstStepWithErrors(untilStep: NaviStep = 'finishProject') {
    // The steps are checked in order to ensure that we navigate to the _first_ step with an error
    for (const step of NAVI_STEP_ORDER.slice(0, NAVI_STEP_ORDER.indexOf(untilStep) + 1)) {
      const [isStepValid] = this.validateStep(step);

      if (!isStepValid) return step;
    }
    return null;
  }

  /**
   * Return all errors in the data, as well as the first step with an error.
   *
   * @param untilStep - Up until which step to check. Check all steps if omitted.
   * @returns A tuple consisting of: the first step with an error or null, all accumulated error messages.
   */
  getAllErrors(untilStep: NaviStep = 'finishProject'): [NaviStep | null, [boolean, ErrorMsg[]]] {
    let errorResult: [boolean, ErrorMsg[]] = [true, null];
    let firstErrorStep: NaviStep = null;

    // The steps are checked in order to ensure that we navigate to the _first_ step with an error
    for (const step of NAVI_STEP_ORDER.slice(0, NAVI_STEP_ORDER.indexOf(untilStep) + 1)) {
      const errorResultForThisStep: [boolean, ErrorMsg[]] = this.validateStep(step);

      errorResult = flattenErrors([errorResult, errorResultForThisStep]);

      // For the first error we find, we set firstError
      if (!errorResult[0] && firstErrorStep == null) {
        firstErrorStep = step;
      }
    }

    return [firstErrorStep, errorResult];
  }

  /**
   * Returns a subset of properties of this DecisionData object that is relevant for detecting changes made by the user.   *
   * Among others, this excludes the Observables found in this object, and DecisionData references within children.
   */
  getPropertiesForChangeDetection() {
    const data = pick(this, [
      'authorName',
      'projectMode',
      'globalVariables',
      'decisionQuality',
      'decisionStatement',
      'objectiveHierarchyMainElement',
      'objectives',
      'objectiveAspects',
      'weights',
      'alternatives',
      'hintAlternatives',
      'influenceFactors',
      'projectNotes',
      'stepExplanations',
      'outcomes',
      'resultSubstepProgress',
      'savedValues',
    ]);
    for (const [key, value] of Object.entries(data)) {
      if (value != null && typeof value === 'object' && 'toJSON' in value && typeof value['toJSON'] === 'function') {
        (data as any)[key] = (data as any)[key].toJSON();
      }
    }
    return data;
  }
}
