## DT Examine

First run `npm install` in this folder. Then run `npm run examine` to run the tool.

You will be prompted for two options:

onlyCheckChangedFiles: If set to true the tool will only check files that are different compared to the `main` branch.

projectToCheck: Which project (cockpit, decision-tool, ...) to check.

Note that even if this tool thinks a class is unused, since the checks it performs are pretty basic, there can be a lot of false positives. (Especially with classes "missing" in the scss file)
