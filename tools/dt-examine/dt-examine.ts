import { existsSync, readFileSync } from 'fs';
import { execSync } from 'child_process';
import { glob } from 'glob';
import { start as startPrompt, get as getPrompt } from 'prompt';
import { isString } from 'lodash';
import { basename, dirname, join } from 'path';

type UnusedClass = { name: string; missingIn: 'html' | 'scss' };

const matTypographyClasses = ['mat-caption', 'mat-small', 'material-icons'];
const inputBoxClasses = ['caption-wrapper', 'control-wrapper', 'content-wrapper', 'container-left'];

const safeGlobalClassesInHtml = ['bold', 'italic', 'browser-default', ...matTypographyClasses, ...inputBoxClasses];
const safeGlobalPrefixesInHtml = ['mat-elevation', 'dt-'];

const safeGlobalClassesInScss = [
  // Used in Form Validation
  'ng-submitted',
  'ng-invalid',
];

const dragClasses = ['cdk-drag-preview', 'cdk-drag-placeholder', 'cdk-drag-animating', 'cdk-drop-list-dragging', 'cdk-drag'];

const CONSOLE_RESET = '\x1b[0m';
const CONSOLE_FGGREEN = '\x1b[32m';
const CONSOLE_FGMAGENTA = '\x1b[35m';

const classHtmlAttributeRegex = /class="(.*?)"/gm;
const classHtmlBindingRegex = /\[class.(.+?)\]/gm;
const classHtmlWidthTriggerRegex = /\[dtWidthTrigger\]="{(.*)}"/gm;

const classScssRegex = /(?<!\*.*)(?<!\/\/.*)(?<!:host-context\()\.([_a-zA-Z]+[_a-zA-Z0-9-]*)(?!.*;)/gm;
const importScssRegex = /@import '(.*)';/gm;

run();

async function run() {
  startPrompt();

  const promptResult = await getPrompt({
    properties: {
      onlyCheckChangedFiles: {
        required: true,
        default: false,
        type: 'boolean',
      },
      projectToCheck: {
        required: true,
        default: 'decision-tool',
        type: 'string',
      },
    },
  });

  const onlyCheckChangedFiles = !!promptResult['onlyCheckChangedFiles'];

  if (onlyCheckChangedFiles) {
    const changedFiles = new Set(
      execSync('git diff --name-only origin/main HEAD ../../', { encoding: 'utf-8' })
        .split(/\r?\n/)
        .map(f => f.trim())
        .filter(f => f !== '')
        .map(name => '../../' + name),
    );

    await diff(promptResult['projectToCheck'] + '', true, changedFiles);
  } else {
    await diff(promptResult['projectToCheck'] + '', false);
  }
}

async function diff(project: string, onlyCheckChangedFiles: false): Promise<void>;
async function diff(project: string, onlyCheckChangedFiles: true, changedFiles: Set<string>): Promise<void>;
async function diff(project: string, onlyCheckChangedFiles: boolean, changedFiles?: Set<string>): Promise<void> {
  const totalUnusedClassCount = {
    html: 0,
    scss: 0,
  };

  const scssFiles = new Set(await glob(`../../apps/${project}/src/**/*.scss`));
  const typescriptFiles = new Set(await glob(`../../apps/${project}/src/**/*.ts`));
  const htmlFiles = new Set(await glob(`../../apps/${project}/src/**/*.html`));

  const unused: Record<
    string,
    {
      hasDeep: boolean;
      unusedClasses: UnusedClass[];
    }
  > = {};

  for (const scssFileName of scssFiles) {
    const htmlFileName = scssFileName.replace('.scss', '.html');
    const typescriptFileName = scssFileName.replace('.scss', '.ts');

    if (!htmlFiles.has(htmlFileName)) {
      continue;
    }

    if (onlyCheckChangedFiles && changedFiles) {
      if (![scssFileName, htmlFileName, typescriptFileName].some(file => changedFiles.has(file))) {
        continue;
      }
    }

    const scssFileContent = readFileSync(scssFileName, { encoding: 'utf-8' });
    const htmlFileContent = readFileSync(htmlFileName, { encoding: 'utf-8' });

    const hasDeep = scssFileContent.includes('::ng-deep');

    const classesInScss = getClassesUsedInScss(scssFileContent, scssFileName);
    const classesInHtml = getClassesUsedInHtml(htmlFileContent);

    const unusedClasses: UnusedClass[] = [];

    for (const classDefinedInScss of classesInScss) {
      const hasDrag = htmlFileContent.includes('cdkDrag');

      if (hasDrag && dragClasses.includes(classDefinedInScss)) {
        continue;
      }

      if (safeGlobalClassesInScss.includes(classDefinedInScss)) {
        continue;
      }

      if (classesInHtml.includes(classDefinedInScss)) {
        continue;
      }

      if (typescriptFiles.has(typescriptFileName)) {
        const typescriptFileContent = readFileSync(typescriptFileName, { encoding: 'utf-8' });

        if (typescriptFileContent.includes(`@HostBinding('class.${classDefinedInScss}')`)) {
          continue;
        }
      }

      unusedClasses.push({ name: classDefinedInScss, missingIn: 'html' });
      totalUnusedClassCount.html++;
    }

    for (const classDefinedInHtml of classesInHtml) {
      if (classesInScss.includes(classDefinedInHtml)) {
        continue;
      }

      if (safeGlobalPrefixesInHtml.some(safePrefix => classDefinedInHtml.startsWith(safePrefix))) {
        continue;
      }

      if (safeGlobalClassesInHtml.includes(classDefinedInHtml)) {
        continue;
      }

      unusedClasses.push({ name: classDefinedInHtml, missingIn: 'scss' });
      totalUnusedClassCount.scss++;
    }

    if (unusedClasses.length > 0) {
      unused[scssFileName] = { hasDeep, unusedClasses };
    }
  }

  for (const [scssFileName, result] of Object.entries(unused)) {
    console.log(scssFileName);
    for (const unusedClass of result.unusedClasses) {
      if (unusedClass.missingIn === 'html') {
        process.stdout.write(CONSOLE_FGMAGENTA);
      } else {
        process.stdout.write(CONSOLE_FGGREEN);
      }
      console.log(` - ${unusedClass.name} (missing in ${unusedClass.missingIn})`);

      process.stdout.write(CONSOLE_RESET);
    }

    console.log();
  }

  console.log('Total of ' + (totalUnusedClassCount.html + totalUnusedClassCount.scss) + ' candidates for unused classes');
  console.log(' - ' + totalUnusedClassCount.html + ' missing in html files');
  console.log(' - ' + totalUnusedClassCount.scss + ' missing in scss files');
}

function getClassesUsedInScss(scss: string, scssFilePath: string) {
  const matches = scss.matchAll(classScssRegex);
  const matchesAsArray = [...matches];

  if (!matchesAsArray) {
    return [];
  }

  const cssClassesSet = new Set<string>();

  for (const match of matchesAsArray) {
    let isDeepRule = false;

    const startIndex = match.index;

    if (startIndex == null) {
      continue;
    }

    let depthCounter = 0;

    let leftOurLine = false;

    for (let index = startIndex; index >= 0; index--) {
      if (scss.slice(index - 9, index) === '::ng-deep' && (depthCounter < 0 || !leftOurLine)) {
        isDeepRule = true;
        break;
      }

      if (scss.charAt(index) === '\n') {
        leftOurLine = true;
      }

      if (scss.charAt(index) === '}') {
        depthCounter++;
      }

      if (scss.charAt(index) === '{') {
        depthCounter--;
      }

      if (scss.slice(index - 2, index) === '\n\n' && scss.charAt(index) != ' ') {
        break;
      }
      if (scss.slice(index - 1, index) === '\n' && scss.charAt(index) != ' ' && scss.charAt(index) != '\n') {
        break;
      }
    }

    if (!isDeepRule && match[1]) {
      cssClassesSet.add(match[1]);
    }
  }

  const resultArray = new Array(...cssClassesSet);

  // Check Import Rules
  const importMatches = scss.matchAll(importScssRegex);
  const importMatchesAsArray = [...importMatches];

  const workingDirectory = dirname(scssFilePath);
  for (const importMatch of importMatchesAsArray) {
    const importTarget = importMatch[1];
    if (!importTarget) {
      continue;
    }

    const importTargetFolder = dirname(importTarget);
    let importTargetFilePathCandidates = [basename(importTarget + '.scss'), '_' + basename(importTarget + '.scss')].map(fileName =>
      join(workingDirectory, importTargetFolder, fileName),
    );

    const existingImportTargetPath = importTargetFilePathCandidates.find(path => existsSync(path));

    if (!existingImportTargetPath) {
      continue;
    }

    resultArray.push(...getClassesUsedInScss(readFileSync(existingImportTargetPath, { encoding: 'utf-8' }), existingImportTargetPath));
  }

  return resultArray;
}

function getClassesUsedInHtml(html: string) {
  const classAttributes = html.matchAll(classHtmlAttributeRegex);
  const attributeClasses = [...classAttributes]
    .map(match => match[1])
    .filter(isString)
    .map(value => value.split(' '))
    .flat();

  const classBindings = html.matchAll(classHtmlBindingRegex);
  const boundClasses = new Array(...new Set([...classBindings].map(match => match[1]).filter(isString)));

  const widthTriggers = html.matchAll(classHtmlWidthTriggerRegex);

  const triggerClasses = [...widthTriggers]
    .map(match => match[1])
    .filter(isString)
    .map(triggerObject => triggerObject.split(','))
    .flat()
    .map(trigger => trigger.split(':')[0]?.trim().replaceAll(`'`, ''))
    .filter(isString);

  return new Array(...new Set([...attributeClasses, ...boundClasses, ...triggerClasses]));
}
