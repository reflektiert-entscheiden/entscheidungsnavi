Adblockers look for the name `matomo.js` and `matomo.php`.
Therefore, we name our Matomo tracking script `compass.js` and tunnel all requests through our server.
