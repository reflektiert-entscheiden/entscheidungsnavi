# The following variables can be used to customize the pipeline when manually executing:
# - RESET_NPM_CACHE=1: npm deps are reinstalled
# - FORCE_BUILD_ALL=1: all projects are built, not only affected

workflow:
  rules:
    # Run for MRs (but disable merged result pipelines)
    - if: $CI_PIPELINE_SOURCE == 'merge_request_event' && $CI_MERGE_REQUEST_EVENT_TYPE != 'merged_result'
      variables:
        NX_BASE: $CI_MERGE_REQUEST_DIFF_BASE_SHA
        # Overwrite Docker image tags for MRs
        INPUT_TAGS: mr-$CI_MERGE_REQUEST_IID
    # Run for important branches and tags
    - if: $CI_COMMIT_REF_PROTECTED == 'true'
      variables:
        NX_BASE: $CI_COMMIT_BEFORE_SHA

variables:
  FF_USE_FASTZIP: 'true'
  NX_HEAD: $CI_COMMIT_SHA
  CYPRESS_CACHE_FOLDER: node_modules/.cache/cypress_cache

default:
  image: node:20.11.1-bullseye
  tags:
    - saas-linux-medium-amd64
  # Use npm cache
  cache: &nodemodulescache
    key:
      files:
        - package-lock.json
      prefix: npm-$CI_PROJECT_NAME
    paths:
      - node_modules/
      - apps/decision-tool/src/supported-browsers.ts
    policy: pull
  before_script:
    - source persistent-variables
    - >
      if [[ ! -d node_modules ]]; then
        npm ci --unsafe-perm
      fi
  # Allow jobs to be stopped when they are made redundant by a subsequent pipeline
  interruptible: true

stages:
  - setup
  - build
  - docker
  - test
  - deploy

include: '/.ci/deploy.yml'

cache-node-modules:
  stage: setup
  image: node:20-alpine
  before_script: []
  script:
    - >
      if [ "$RESET_NPM_CACHE" = "1" ]; then
        echo "Resetting npm cache."
        rm -rf node_modules
      fi
    - >
      if [ ! -d node_modules ]; then
        echo "Installing npm dependencies."
        npm ci --unsafe-perm
      else
        echo "Using cached npm dependencies."
        rm -rf node_modules
      fi
  cache:
    <<: *nodemodulescache
    policy: pull-push

check-new-mr:
  stage: setup
  before_script:
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
  script:
    - >
      if ! (docker manifest inspect "$CI_REGISTRY_IMAGE/decision-tool:${INPUT_TAGS:-$CI_COMMIT_REF_NAME}" > /dev/null 2>&1) ||
         [[ "$FORCE_BUILD_ALL" -eq 1 ]] ||
         [[ "$CI_COMMIT_REF_PROTECTED" -eq true ]]; then
        echo "Building all projects."
        echo "export NX_COMMAND=\"npx nx run-many --all\"" > persistent-variables
      else
        echo "Existing docker image found. Using nx affected."
        echo "export NX_COMMAND=\"npx nx affected\"" > persistent-variables
      fi
  cache: []
  tags:
    - docker-preview
  artifacts:
    expire_in: 1 week
    paths:
      - persistent-variables

build:
  stage: build
  needs:
    - cache-node-modules
    - check-new-mr
  script:
    - $NX_COMMAND --target=build --configuration=production --localize --output-style=stream |& tee build.log
    - |
      if [ -n "$SENTRY_AUTH_TOKEN" ]; then
        echo "SENTRY_AUTH_TOKEN is set. Uploading source maps..."
        npm run sentry:sourcemaps:frontend
        npm run sentry:sourcemaps:cockpit
        npm run sentry:sourcemaps:klug
        echo "Regenerating Service Worker hashes for decision tool..."
        npx ngsw-config dist/apps/decision-tool/browser/de apps/decision-tool/ngsw-config.json  /de
        npx ngsw-config dist/apps/decision-tool/browser/en apps/decision-tool/ngsw-config.json  /en
      else
        echo "SENTRY_AUTH_TOKEN is not set. Skipping source map upload."
      fi
  artifacts:
    expire_in: 1 week
    paths:
      - dist/apps
      - build.log

extract-i18n:
  stage: build
  needs:
    - cache-node-modules
    - check-new-mr
  rules:
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
  script:
    - npx nx run-many --target=extract-i18n --all
  artifacts:
    expire_in: 1 week
    paths:
      - i18n/de.*.xlf

docker:
  stage: docker
  image: gperdomor/nx-kaniko:20.11.1-alpine
  needs:
    - cache-node-modules
    - check-new-mr
    - build
  script:
    - echo "{\"auths\":{\"$CI_REGISTRY\":{\"auth\":\"$(echo -n ${CI_REGISTRY_USER}:${CI_REGISTRY_PASSWORD} | base64 | tr -d '\n')\"}}}" > /kaniko/.docker/config.json
    # Kaniko does not support parallel builds
    - $NX_COMMAND --target=container --parallel=1
  variables:
    # Set the build timestamp for the images
    INPUT_BUILD_ARGS: TIMESTAMP=$CI_COMMIT_TIMESTAMP
    # Switch container engine to kaniko and push to registry
    INPUT_ENGINE: 'kaniko'
    INPUT_PUSH: 'true'

lint:
  stage: test
  needs:
    - cache-node-modules
    - check-new-mr
  script:
    - $NX_COMMAND --target=lint
    # Checks format and colors output red for better visibility.
    - npx nx format:check | sed $'s,.*,\e[31m&\e[m,'

test:
  stage: test
  needs:
    - cache-node-modules
    - check-new-mr
  script:
    - $NX_COMMAND --target=test --ci --code-coverage=true
  cache:
    - <<: *nodemodulescache
    - key: jest-$CI_COMMIT_REF_SLUG
      paths:
        - .jest/cache/

check-translations:
  stage: test
  needs:
    - build
  before_script: []
  rules:
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
  script:
    # Check for missing translations
    - '(! cat build.log | grep "No translation found for") || exit 1'
  allow_failure: true
  cache: []

e2e:
  stage: test
  needs:
    - cache-node-modules
    - check-new-mr
  image: cypress/base:20.17.0
  script:
    - 'node -e "new (require(\"mongodb\").MongoClient)(process.env.MONGODB_URI).db().admin().command({ replSetInitiate: {} }).then(() => process.exit())"'
    - npx start-server-and-test --expect 404 'npx nx serve backend --configuration=production' http://localhost:3000/api '$NX_COMMAND --target=e2e --parallel=false'
  variables:
    BASE_URL: http://localhost:4200
    MONGODB_URI: mongodb://mongo:27017/navi?replicaSet=ra0&directConnection=true
    SESSION_SECRET: badpassword
  services:
    - name: mongo:6
      command: ['mongod', '--replSet', 'ra0', '--bind_ip_all']
      alias: mongo
  artifacts:
    expire_in: 2 hrs
    when: always
    paths:
      - dist/cypress/apps
  retry: 2

percy:
  stage: test
  needs:
    - cache-node-modules
    - check-new-mr
  image: cypress/base:20.11.0
  rules:
    - if: $CI_MERGE_REQUEST_SOURCE_BRANCH_NAME =~ /^lokalise-/
      when: never
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH && $CI_PIPELINE_SOURCE == 'push'
      when: always
    - if: $CI_PIPELINE_SOURCE == 'merge_request_event' && $CI_MERGE_REQUEST_EVENT_TYPE == 'detached'
      when: manual
  script:
    - npx percy exec -- npx nx e2e decision-tool-e2e --skip-nx-cache --spec=*/**/click-through-percy.cy.ts
  artifacts:
    expire_in: 2 hrs
    when: always
    paths:
      - dist/cypress/apps
